#!/user/bin/perl
# Name: loadJobs.pl
# Use: Queue-up jobs for data processing
# Author: David Nichols
#
# HISTORY: 
  my $version = "BETA";

###############################################################################
#
#	SUMMARY
#
#	Program will be executed once every minute by the windows task scheduler.
#	Its first action is to look for new files in the "watched" folder. If it 
#	finds a new file or files, it will then execute the rest of the code,
#	else it will do nothing and exit immediately.
#
#	The main body function only renames files and calls one function, 
#	"ReadFile."  ReadFile recursively searches the watched folders for
#	appropriately named files (with .WORKING extensions), and then passes
#	the name of the valid files to another the functin "ParseFileContents,"
#	which is what rads the file contents and passes them as parameters to the
#	back-end processing software (formerly X2H.bat).
#
###############################################################################
# PRAGMA 
###############################################################################
use strict;
use warnings;
no warnings 'uninitialized';

# GLOBALS 
###############################################################################
my $fileFound = ""; # initialized to undef





###############################################################################
# MAIN ROUTINE ################################################################
###############################################################################
 
# searches "watched" folder and returns a value only if a file is found
$fileFound = SearchDir ( );

# $fileFound is defined only if 'SearchDir' finds a file
if ($fileFound ne undef) 
{
	# renames ext
	system "ren O:\\_TempWebProcess\\_in\\*.txt *.WORKING"; 

	# void function to read file names in dir and call ParseFileContents function
	ReadFile ( );	
	
	# temp... will eventually log and delete this file
	system "ren O:\\_TempWebProcess\\_in\\*.WORKING *.txt";
	
}

exit[0];

###############################################################################




###############################################################################
# FUNCTIONS ###################################################################
###############################################################################

sub SearchDir
{ 
	my $found = ""; # initialized to undef
	my $dirToSearch = "O:\\_TempWebProcess\\_in";
	
	print STDOUT "Made it to SeachDir\n";
	
	opendir(DIR, $dirToSearch) or die ("Cannot open directory \"$dirToSearch\n\"");
	
	# passes file names to @files
	my @files = readdir DIR;
	
	# for each file name of @files
	foreach $_ (@files)
	{
		# any number of chars up to a period, and then the first three chars to follow
		$_ =~ m/.+\.(\w{3})/gi;
		
		# if the extension == txt
		if ($1 eq "txt")
		{
			# defines $found
			$found = "YES";
		}
	}
		
	close DIR;
	
	# Returns defined value "YES", though the string "YES" is unimportant.
	# If $found is defined, which is returned and passed to $fileFound in the main
	# routine, the rest of the code will execute.
	return ($found);
}




sub ReadFile
{
	my $addr = shift; # initialized to undef, though will be defined as function recurs
	my $dirToSearch = "O:\\_TempWebProcess\\_in";
	
	
	
	# first pass $addr will be undef, and will not call ParseFileContents
	if ($addr ne undef)
	{
		ParseFileContents ($addr);
	}
	else 
	{	
		opendir(DIR, $dirToSearch) or die ("Cannot open directory \"$dirToSearch\n\"");
		
		# passes file names to @files
		my @files = readdir DIR;
		
		my $fileName = $_;
		
		foreach $fileName (@files)
		{
			$fileName =~ m/.+\.(\w+)/gi;
				
			if ($1 eq "WORKING")
			{
				# Will call itself, this time with a value to shift onto $addr, which will
				# be passed to the function ParseFileContents
				ReadFile ($fileName);
			}
		}
		
		close DIR;
	}	
}




sub ParseFileContents
{
	my $name = shift; 
	my $openFile = "O:\\_TempWebProcess\\_in\\$name";
	my $acctNum = ""; # initialized to undef
	my $eiJobNum = ""; # initialized to undef
	my $updateStr = ""; # initialized to undef
	my $postTime = ""; # initialized to undef
	my $err = 0; 
	my $readErr = "";
	
	open INFILE, "<", $openFile 
	     or die "File \"$openFile\" could not be opened";
	
	while (<INFILE>) 
	{
		$_ =~ m/(\w+)-(.+)/gi;
		
		# Example: ACCT-12345
		#
		# The following conditionals will match
		# the string before a hyphen and then pass the value
		# of the string following the hyphen to and appropriate
		# variable.
		
		if ($1 eq "ACCT")
		{
			$acctNum = $2;
		}
		elsif ($1 eq "EI")
		{
			$eiJobNum = $2;
		}
		elsif ($1 eq "UPDATE")
		{
			$updateStr = $2;
		}
		elsif ($1 eq "POST")
		{
			if ($2 eq "NOW")
			{
				$postTime = $2;
			}
			else
			{
				$postTime = "ERROR";
				$err++; # error count+1 for invalid post option
			}
		}
		elsif ($1 eq "POSTTIME")
			{
				$postTime = $2;
			}
		else
		{
			$err++; # error count+1 for invalid entry type
		}
		
		if ($2 =~ m/\s/)
		{
			$err++; # error count+1 for space in entry
		}
	}
	
	close INFILE;
	
	$readErr = "$err file read error(s)";
	
	# Passes any errors to ErrorReport
	ErrorReport ($acctNum, $readErr);
	
	# Will call back-end process, launcing multiple instances with 'START' for each account to be processed
	system "START \"Processing... $acctNum  \" V:\\SOURCE\\eProcess\\eProcess.bat $acctNum $eiJobNum $updateStr $postTime";
	
	return ($readErr);
}




sub ErrorReport
{
	my $acct = shift;
	my $errors = shift;
	my $todaysDate = "";
	my $logName = ($acct . "_ErrLog.txt");
	
	$todaysDate = GetTime ( );
		
	my $outFile = "O:\\_TempWebProcess\\errors\\$logName";
	
	open OUTFILE, ">>", $outFile 
	     or die "File \"$outFile\" could not be opened";
		 
	print OUTFILE "$errors $todaysDate \n";
	
	close OUTFILE;
}




sub GetTime
{
	my $second = "";
	my $minute = "";
	my $hour = "";
	my $day = "";
	my $month = "";
	my $year = "";
	my $weekDay = "";
	my $dayOfYear = "";
	my $isDST = "";
	my $amPm = "";
	
($second, $minute, $hour, $day, $month, $year, $weekDay, $dayOfYear, $isDST) = localtime(time);

	$year += 1900;

	$month++;
	
	# 12-hr time conversion
	if ($hour > 12)
	{
		$hour = ($hour - 12);
		$amPm = "PM";
	}
	else
	{
		$amPm = "AM";
	}
	# Add zero for minutes less than 10
	if ($minute < 10)
	{
		$minute = ("0" . $minute);
	}
	
	my $date = "on $month/$day/$year @ $hour:$minute $amPm";
	
	return ($date);
}

# EOF
###############################################################################