#!/usr/bin/perl
# FTPHTML.pl
# Used to FTP contents of ~\HTML folders to build/rebuild existing Client web (new format)
# This application is CALLED FROM 'xml2html.bat', which passes in: host, login, pw, and base-folder
#
# Programmer:  Raymond Lillibridge

# Change History:
# 20100921.1 - re-compiled application
# 20100401_1124 (Raymond Lillibridge) change folder structure:
#    ~/download/HTML/#####/#####html.zip
#            to
#    ~/download/HTML/#####html.zip
#================================================
use warnings;
use Net::FTP;
use File::Find;
no warnings 'File::Find';
use Term::InKey;
#=== Win32 ===
use Cwd;
my $CWDSAVED;

my $myVersion = "Version:  2010-09-21.1 (beta)";
my $dashes = "--------------------------------------------------------------------------------";

my $hostname=shift;
my $login=shift;
my $pw=shift;
my $HTMLPATH=shift;
my $JobNo=shift;

my $rtn="";

#================================================
#	MAIN
#================================================
HouseKeeping();
InitFolder($JobNo);
find(\&FTPfile, ".");
PutSelectFiles();
ZipPutHTMLFolder();
$ftp->quit;	

print STDOUT "\n------- DELETING Select XML Files (local)-------\n\n";
find (\&DeleteLocalXMLFile, ".");

print STDOUT "\n\n====================\n FTP FINISHED\n====================\n\n";

print STDOUT "\n\n(Press any key to Exit.)\n";
$x = &ReadKey;

exit 1;

#================================================
#	END MAIN
#================================================



#================================================
#================================================
#	Subroutines...
#================================================

sub DeleteLocalXMLFile {
	my $filename = $_;
	my $fullpath = $File::Find::name;
	#remember that File::Find changes your CWD, so you can call open with just $_
	
	if (-e $filename && -f $filename && /^(.+?)\.xml/ && $filename ne "." && $filename ne ".." && $filename ne "$JobNo.xml") { 
		print "\t...DELETING...$_  IN $File::Find::dir\n"; 
		unlink $_;
	}
}
	

#------------------------------------------------------------
sub FTPfile{
#------------------------------------------------------------
	my $dir="$File::Find::name";
	my $rtn="";
	
	if( (-f $_) && !/(.+?)\.xml/  && !/^SECTION_/  && !/^MCC_SECTION/  &&  !/^indexSec.html/	&& !/(.*)\.tif/i &&!/(.*)\.eps/i)	{
		
		$dir =~ s|^\.\/(.+?)\/(.+?)$|$1|g;

		if($dir =~ /^\./)	{
			$dir=".";
		}
		
		if($dir ne ".")	{
			# CD into subfolder
			$ftp->cwd($dir);
		}
		
		#put the file...
		$rtn=$ftp->pwd();
		print STDOUT "[FTP FILE]    PUT: $_ \tTO: $rtn\n";
		$rtn = $ftp->put($_);
		
		if($dir ne ".")	{
			# back up to parent folder
			$ftp->cdup();
		}
		
				
	} elsif( -d $_  && !/^SECTIONS/  )	{
		# Since original folder was initialized, no FTP subfolders should exist
		$ftp->mkdir($_);
	}
	
}


#------------------------------------------------------------
sub HouseKeeping {
#------------------------------------------------------------
	my $me=whoami();
	my $rtn="";
	$CWDSAVED=cwd();

	&Clear;

	print STDOUT "\n$dashes\n" .
		"Running:  FTPHTML.pl\t\tVersion:  $myVersion\n" .
		"$dashes\n\n";
	
	if (!$JobNo)	{
		print STDOUT "\n\n\tOOPS!  YOU NEED TO PASS THE JOB NUMBER TO THIS APPLICATION!\n\n"; 
		exit 0;
	} else {
		print STDOUT "\$hostname = \t$hostname\n";
#		print STDOUT "\$login = \t$login\n";
#		print STDOUT "\$pw = \t\t$pw\n";
		print STDOUT "\$HTMLPATH = \t$HTMLPATH\n\n\n";
		print STDOUT "JOB NUMBER:  \t$JobNo\n\n";
		
		print STDOUT "\n\n(Press any key to Continue...)\n";
		$x = &ReadKey;
	}
	
	# make FTP connection...
	$ftp = Net::FTP->new($hostname, Debug => 0)	or die "Cannot connect to $hostname: $@";	
		
	$ftp->login($login,$pw);
	$rtn = $ftp->message;
	if($rtn =~ /cannot/)	{
		print STDOUT "\n\t$rtn\n\n\t...Processing cancelled... (Press any key to continue)\n";
		$x = &ReadKey;
		exit 0;
	}

	$ftp->ascii();	
#	$ftp->cwd("/$HTMLPATH") or die "Cannot change working directory to: $HTMLPATH", $ftp->message;
	$ftp->cwd("/$HTMLPATH");
	$rtn = $ftp->message;
	if($rtn =~ /cannot/)	{
		print STDOUT "\n\t$rtn\n\n\t...Processing cancelled... (Press any key to continue)\n";
		$x = &ReadKey;
		exit 0;
	}
	
	$rtn=$ftp->pwd();
	print STDOUT "\nFTP ROOT PATH = $hostname$rtn\n";

	$rtn=$ftp->binary();
}


#------------------------------------------------------------
sub InitFolder {
#------------------------------------------------------------
	my $folder = shift;
	my $me=whoami();
	
	$rtn="";
	# CD to $folder (does it exist?)...
	$rtn = $ftp->cwd("$folder");
	
	if($rtn ne '')	{
		# Folder Exists...
		print STDOUT "\tInitializing FTP folder:  $folder... ";		
		$rtn = $ftp->pwd();
		# CD to parent directory...
		$rtn=$ftp->cdup ();
		$rtn = $ftp->pwd();
		# Remove old directory...
		$rtn = $ftp->rmdir($folder, 1);
		print STDOUT "(Finished)\n";
		
		# Create new directory...
		$ftp->mkdir($folder);			
	} else {	
		# Folder Does Not Exist...
		print STDOUT "\n\tA Folder for '$folder' does NOT EXIST...\n\n" .
		"\t...Press 'Y' to Create Folder $folder\n" .
		"\t   (Any other key to cancel.)\n\n";
		
		$x = &ReadKey;
	
		if($x=~ /[Yy]/) {
			print(STDOUT "\n");	#add a little space before processing the files
			$ftp->mkdir($folder);
		} else {
			print STDOUT "...Processing cancelled... (Press any key to continue)\n";
			$x = &ReadKey;
			exit 0 ;
		}
	}	
	
	# CD to newly created directory
	$rtn = $ftp->cwd("$folder");
	$rtn = $ftp->pwd();
	
	print STDOUT "\n\n\tARE YOU READY TO TRANSFER TO $rtn?\n\n" . 
		"\t...Press:  'Y' to TRANSFER FILES \n\t   (Any other key to cancel.)\n\n";	
	$x = &ReadKey;
	if($x=~ /[Yy]/) {
		# transfering files
	} else {
		print STDOUT "...Processing cancelled... (Press any key to continue)\n";
		$x = &ReadKey;

		exit 0 ;
	}

}


#================================================
#	Utility Subroutines...
#================================================
#-------------------------------------------------------------
sub do_Error {
#-------------------------------------------------------------
	my ($mySub, $myMessage) = @_;
	
	print STDOUT "ERROR:  [$mySub]\n\t$myMessage\n";
}


#------------------------------------------------------------
sub PutSelectFiles {
#------------------------------------------------------------
	#NOTE:  Currently ONLY works properly with 'root' files.
	my $me = whoami();
	my @aSelectFile=("$JobNo.xml");
	
	print STDOUT "\n------- Processing Select Files -------\n\n";
	
	foreach my $s (@aSelectFile)	{
		
		if($s eq "$JobNo.xml"	) {
			# back up to parent folder...
			$rtn = $ftp->cdup();
			$rtn = $ftp->pwd();
			print STDOUT "    PUT: $s \tTO: $rtn\n";
			$rtn = $ftp->put($s);	
			# CD back to original base folder...	
			$ftp->cwd("/$HTMLPATH");
			$rtn = $ftp->message;
			
			if($rtn =~ /cannot/)	{
				print STDOUT "\n\t$rtn\n\n\t...Processing cancelled... (Press any key to continue)\n";
				$x = &ReadKey;
				exit 0;
			}			
			
		}else {
			print STDOUT "    PUT: $s \tTO: $rtn\n";
			$rtn = $ftp->put($s);
		}
	}
}

#------------------------------------------------------------
sub whoami {
#------------------------------------------------------------
	(caller(1))[3];
}



#------------------------------------------------------------
sub ZipPutHTMLFolder {
#------------------------------------------------------------
	my $me = whoami();
	my $FTPDownloadFolder="download/HTML";
	my $zipFileName= "$JobNo" . "HTML.zip";
	my $rtn;
	my @folder=();
	@folder = split(/\//, $FTPDownloadFolder);
	
#	print STDOUT "$dashes\n\tDumping \@folder array...\n$dashes\n";
#	foreach my $i (@folder)	{
#		print STDOUT "\$i=$i\n";	
#	}
	
	print STDOUT "\n\nZipping HTML Folder to \'$zipFileName\'...";
	
	if(-e "$zipFileName")	{
		unlink "$zipFileName";
	}
	
	use Archive::Zip;
   	my $zip = Archive::Zip->new();

	# add all readable plain files below /HTML as HTML/*
	$zip->addTree( '.', 'HTML', sub { -f && -r } );    
	
	# and write them into a file
	$zip->writeToFileNamed("$zipFileName");
	print STDOUT "\(done\)\n\n";

	$rtn = $ftp->cdup();	#CD to parent of 'HTML' folder
	
	foreach my $f (@folder)	{
		$rtn=$ftp->pwd();
#		print STDOUT "\t...In FTP folder $rtn...\n";
		
		#Check FTP Server for download folder(s)...
		$rtn="";
		$rtn=$ftp->cwd("$f");	
		$rtn = $ftp->message;
		if($rtn =~ /cannot/)	{
			print STDOUT "\n\t$rtn\n\n\t...Create download folder \'$f\' on FTP Server?\n" . 
							"\t\t(Press 'Y' = YES \t(any other key = CANCEL)\n\n";
			$x = &ReadKey;
			
			if($x=~ /[Yy]/) {
				#Create download folder(s)...
				$rtn = $ftp->mkdir("$f");
				$rtn = $ftp->cwd("$f");
			} else {
				print STDOUT "...Processing cancelled... (Press any key to continue)\n\n";
				$x = &ReadKey;
				exit 0 ;
			}
		} else {
			$rtn = $ftp->cwd("$f");		
		}
	} #END WHILE
		
	# Download folder exists...
	$rtn=$ftp->pwd();
#	print STDOUT "\tBASE FTP DOWNLOAD PATH = $rtn\n\n";
#	InitFolder($JobNo);  ### COMMENTED OUT [20100401_1251 per Lisa and David] ###
	$rtn=$ftp->pwd();
	print STDOUT "\tPUT:  $zipFileName\tTo:  $rtn\n";

	$rtn=$ftp->binary();
	
	$rtn = $ftp->put("$zipFileName");
	unlink("$zipFileName");	# Remove from local HTML folder.
}