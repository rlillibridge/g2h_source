@echo off
REM	Name: eProcess.bat
REM	BY: David Nichols

SET ACCTNUM=%1
SET EIJOBNUM=%2
SET UPDATESTR=%3
SET POSTTIME=%4

SET DRIVE=R:
SET WEBDRIVE=O:

REM	=======================================================
REM	REQUIREMENTS:  Saxon9.jar (and company) &  java VM installed
REM =======================================================
REM --------------------------------------------------------------------
REM	ENVIRONMENTAL VARIABLES...
REM --------------------------------------------------------------------

SET XSLT=%DRIVE%\XSLT\MCC
SET XSD=%DRIVE%\SCHEMA\MCC\XSD\CODE\code.xsd
SET SAXON=java -Xms128m -Xmx1g net.sf.saxon.Transform
SET ALTOVAXML=%DRIVE%\APPS_3rdPARTY\AltovaXML.exe
SET CWD=%CD%
SET CWDJOB=%CWD%


SET TOPAUSE=-nopause

SET JOB=%1
SET IMAGES=..\HTML\Images
::----- exclude from ..\HTML\Images...
SET ex_TIF=-x!images\*.tif
SET ZIPXML=%DRIVE%\APPS_3rdPARTY\7zip_32\7z.exe a %ACCTNUM%XML.zip -tzip Book_Final.xml %IMAGES% %ex_TIF%
IF DEFINED ProgramFiles(x86) SET ZIPXML=%DRIVE%\APPS_3rdPARTY\7zip_64\7z.exe a %ACCTNUM%XML.zip -tzip Book_Final.xml %IMAGES%

SET WEBFOLDER=%WEBDRIVE%\html\%ACCTNUM%

REM ===== echo local variables =====
echo.
echo LOCAL VARIABLES:
echo DRIVE=%DRIVE%
echo WEBDRIVE=%WEBDRIVE%
echo WEBFOLDER=%WEBFOLDER%
echo	XSLT=%XSLT%
echo	SAXON=%SAXON%
echo	ALTOVAXML=%ALTOVAXML%
echo	CWD=%CWD%
echo	CWDJOB=%CWDJOB%
echo	NP=%NP%
echo	JOB=%ACCTNUM%
echo	IMAGES=%IMAGES%
echo	ZIPXML=%ZIPXML%
echo.

SET INXML=''
SET XSL=''
SET OUTXML=''

SET STEP=
set /p STEP=Type the LETTER of the 'step' you wish to process...

cls
echo.
echo.

echo.
echo.
GOTO STEP%STEP%


RMDIR %DRIVE%\accts\%ACCTNUM%\HTML /s /q
echo.
echo         ...Just removed %DRIVE%\accts\%ACCTNUM%\HTML...
echo.
REM CALL V:\SOURCE\HTML_LocalSetup\HTML_LocalSetup.pl %DRIVE% %CWD% %ACCTNUM% %TOPAUSE%
CALL %DRIVE%\Apps\HTML_LocalSetup.exe %DRIVE% %CWD% %ACCTNUM% %TOPAUSE%



echo.
echo        ...Validating IMAGES (Do the referenced images exist?)...
REM CALL V:\SOURCE\ValidIMGs\ValidIMGs.pl %TOPAUSE%
CALL %DRIVE%\Apps\ValidIMGs.exe %TOPAUSE%


echo.
echo.




:STEPR
echo.
echo.
echo       *******************************************************************
echo       * R - Convert DIVISION (XML) files to HTML and then to RTF
echo       *******************************************************************
echo.
echo.
echo       *		(CTRL-C to ABORT XML2RTF)
echo.
echo.

IF "%TOPAUSE%"=="-nopause" GOTO SKIP03B
:: For PRODUCTION, ALWAYS SKIP PROMPT BELOW AND GENERATE RTF FILES...
IF "%DRIVE%"=="R:" GOTO SKIP03B

SET /P STEP=Press:  'S' to SKIP THIS STEP...  (Any other key to BUILD RTF files.)     
echo.
echo.

IF /I '%STEP%' == 'S' echo ...SKIPPING conversion of XML to HTML to RTF (by division)
IF /I '%STEP%' == 'S' GOTO STEPB


:SKIP03B
IF "%TOPAUSE%"=="-nopause" CALL %DRIVE%\Apps\X2H_DIVS.bat %CWD% %ACCTNUM% %DRIVE% -nopause
IF NOT "%TOPAUSE%"=="-nopause" CALL %DRIVE%\Apps\X2H_DIVS.bat %CWD% %ACCTNUM% %DRIVE% -pause


:STEPB
echo.
echo.
echo       *******************************************************************
echo       * B - Create:  Book.xml?
echo       *******************************************************************
echo.
echo.



::Delete all previously created Book*.xml files so that they are not included in new process.
DEL Book*.xml
REM CALL V:\SOURCE\BookXML\BookXML.pl %TOPAUSE%
CALL %DRIVE%\Apps\BookXML.exe %TOPAUSE%
echo.


:STEPC
echo.
echo.
echo       *******************************************************************
echo       * C - Create:  Book_All.xml?
echo       *******************************************************************
echo.
echo.



SET INXML=Book.xml
SET XSL=%XSLT%\book.xsl
@echo Start time: %time%
echo -------------------------------------------------------------------------
@echo on
CALL %SAXON% -s:%INXML%		-xsl:%XSL%	-o:%OUTXML% -t
@echo off
echo.
echo -------------------------------------------------------------------------
@echo End time: %time%
echo.


:STEPD
echo.
echo.
echo       *******************************************************************
echo       * D - Create: Book_Struct.xml?
echo       *******************************************************************
echo.
echo.



REM CALL V:\SOURCE\XMLStruct\XMLStruct.pl --job=%ACCTNUM% %TOPAUSE%="-nopause"
CALL %DRIVE%\Apps\XMLStruct.exe --job=%ACCTNUM% %TOPAUSE%="-nopause"
echo         Validating:  Book_Struct.xml...    (Please wait)
echo.
CALL %ALTOVAXML% -v Book_Struct.xml
echo.
IF [%ERRORLEVEL%]  =="1" GOTO ERR_INVALIDXML


:STEPE
echo.
echo.
echo       *******************************************************************
echo       * E - Create:  Book_StructIMG.xml?
echo       *******************************************************************
echo.
echo.



REM CALL V:\SOURCE\ImageMeta\ImageMeta.pl %TOPAUSE%="-nopause"
CALL %DRIVE%\Apps\ImageMeta.exe  %TOPAUSE%="-nopause"


:STEPF
echo.
echo.
echo       *******************************************************************
echo       * F - Validating:  Book_StructIMG.xml?
echo       *******************************************************************
echo.
echo.



@echo Start time: %time%
echo -------------------------------------------------------------------------
echo         Validating:  Book_StructIMG.xml...    (Please wait)
echo.
CALL %ALTOVAXML% -v Book_StructIMG.xml
echo.
IF [%ERRORLEVEL%]  ==[1] GOTO INVALIDXML
@echo End time: %time%
echo.
echo -------------------------------------------------------------------------
echo.
echo.


:STEPG
echo.
echo.
echo       *******************************************************************
echo       * G - Create:  Book_Xref.xml
echo       *******************************************************************
echo.
echo.

SET DOXREF=
IF '%DRIVE%'=='V:' SET /p DOXREF=PRESS 'S' to  SKIP XREF processing:  
IF /I '%DOXREF%'=='S' GOTO STEPH


@echo Start time: %time%

SET INXML=Book_StructIMG.xml
SET XSL=%XSLT%\xref.xsl

 
:: Copy XREF Config...
echo.
echo Copying Master xref from %DRIVE%\Master\XrefConfig\XrefConfig.xml...
echo.

XCOPY /Y %DRIVE%\Master\XrefConfig\XrefConfig.xml %DRIVE%\accts\%ACCTNUM%\gc\config\

echo.
echo ...XrefConfig.xml copied from %DRIVE%\Master\XrefConfig\XrefConfig.xml!
echo.
echo.
echo CREATING X-REFS FOR ACCOUNT NO. %ACCTNUM%

%DRIVE%\apps\xref\xreffinder -i: Book_StructIMG.xml -o: Book_Xref.xml -c: %DRIVE%\accts\%ACCTNUM%\gc\config\XrefConfig.xml

@echo End time: %time%
echo.
echo.




:STEPH
echo.
echo.
echo       *******************************************************************
echo       * H - Create:  Book_Meta.xml
echo       *******************************************************************
echo.
echo.

IF /I '%DOXREF%'=='S' SET INXML=Book_StructIMG.xml
IF /I NOT '%DOXREF%'=='S' SET INXML=Book_Xref.xml

echo.
echo INXML = %INXML%
echo.


SET XSL=%XSLT%\meta.xsl
@echo Start time: %time%
echo -------------------------------------------------------------------------
@echo on
CALL %SAXON% -s:%INXML%		-xsl:%XSL%	-o:%OUTXML% -t
@echo off
echo.
echo.


:STEPI
echo.
echo.
echo       *******************************************************************
echo       * I - Create:  Book_Final.xml and %ACCTNUM%XML.zip
echo       *******************************************************************
echo.
echo.



SET INXML=Book_Meta.xml
SET XSL=%XSLT%\breadcrumbs.xsl
@echo Start time: %time%
echo -------------------------------------------------------------------------
@echo on
CALL %SAXON% -s:%INXML%		-xsl:%XSL%	-o:%OUTXML% -t name="%USERNAME%"
@echo off

echo.
echo.
echo       *******************************************************************
echo       * Zipping Book_Final.xml and IMAGES folder to %ACCTNUM%XML.zip
echo       *******************************************************************
echo.
echo.
echo Current Directory:  %CWD%
echo.
echo.



IF NOT EXIST %ACCTNUM%XML.zip echo %ACCTNUM%XML.zip NOT FOUND
IF EXIST %ACCTNUM%XML.zip echo DELETING Existing %ACCTNUM%XML.zip file...
IF EXIST %ACCTNUM%XML.zip del %ACCTNUM%XML.zip

echo ...Zipping the Book_Final.xml and Images folder...
echo.
echo ...Please wait...
echo.
echo COMMAND LINE:  %ZIPXML%
call %ZIPXML%
echo.
echo -------------------------------------------------------------------------
@echo End time: %time%
echo.
echo.


:STEPJ
echo.
echo.
echo       *******************************************************************
echo       * J - Setup ~/HTML for Building Web Pages
echo       *******************************************************************
echo.
echo.



CALL COPY Book_Final.xml ..\HTML
echo.
echo ...BOOK_Final.xml has been copied to ..\HTML
echo.
echo.
echo.




:STEPK
echo.
echo.
echo       *******************************************************************
echo       * K - Build web subfolders and split XML
echo       *******************************************************************
echo.
echo.

CD ..\HTML



CALL %DRIVE%\Apps\BookSplit_XML.bat Book_Final.xml %DRIVE% %TOPAUSE%


:STEPL
echo       *******************************************************************
echo       * L - Build html pages
echo       *******************************************************************
echo.
echo.



CALL %DRIVE%\Apps\BookX2H.bat n%1_Book.xml %DRIVE% %TOPAUSE%


:STEPM
echo.
echo.
echo       *******************************************************************
echo       * M - Create:  MCC_TOC.html
echo       *******************************************************************
echo.
echo.



SET INXML=Book_Final.xml
SET XSL=%XSLT%\TOC_MCC.xsl
@echo Start time: %time%
echo -------------------------------------------------------------------------
@echo on
CALL %SAXON% -s:%INXML%		-xsl:%XSL%	-o:%OUTXML% -t
@echo off
echo.
echo.


:STEPN
echo.
echo.
echo       *******************************************************************
echo       * N - Create:  #####.html (Contegra's TOC)
echo       *******************************************************************
echo.
echo.



SET INXML=Book_Final.xml
SET XSL=%XSLT%\TOC_CONTEGRA_TEMP.xsl
@echo Start time: %time%
echo -------------------------------------------------------------------------
@echo on
CALL %SAXON% -s:%INXML%		-xsl:%XSL%	-o:%OUTXML% -t
@echo off
echo.
echo.
SET INXML=%ACCTNUM%_Contegra_TOC_TEMP.xml
SET XSL=%XSLT%\TOC_CONTEGRA_nodeKey.xsl
@echo Start time: %time%
echo -------------------------------------------------------------------------
@echo on
CALL %SAXON% -s:%INXML%		-xsl:%XSL%	-o:%OUTXML% -t
@echo off
echo.
echo.


:STEPO
CLS
echo.
echo.
echo       *******************************************************************
echo       * O - Copy HTML files to HTML Server       
echo       *******************************************************************
echo.
echo.
echo		Copy HTML files:
echo.
echo            FROM:  %DRIVE%\ACCTS\%ACCTNUM%\HTML    
echo              TO:  %WEBFOLDER%
echo.
echo.
echo.
echo       		!!!	CTRL-C to ABORT	!!!
echo.
echo.



:: Initialize %WEBDRIVE%\html\%ACCTNUM%...
RMDIR /S /Q %WEBFOLDER%
MKDIR %WEBFOLDER%

echo.
echo COPYING HTML TO WEB...
echo        FROM:  %DRIVE%\ACCTS\%ACCTNUM%\HTML
echo        TO:    %WEBFOLDER%
echo.

::For Contegra Indexing...
echo        ...INDEX...
echo         %DRIVE%\ACCTS\%ACCTNUM%\HTML\%ACCTNUM%.xml 
echo                to    %WEBDRIVE%\HTML
COPY /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\%ACCTNUM%.xml %WEBDRIVE%\HTML

:: Copying the content inside the HTML folder to the web...
MD %WEBFOLDER%\images
echo        ...images (jpg, eps, and pdf)...
COPY /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\images\*.jpg %WEBFOLDER%\images
COPY /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\images\*.eps %WEBFOLDER%\images
COPY /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\images\*.pdf %WEBFOLDER%\images

IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level1 echo        ...level1...
IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level1 XCOPY /S /I /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\level1 %WEBFOLDER%\level1

IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level2 echo        ...level2...
IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level2 XCOPY /S /I /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\level2 %WEBFOLDER%\level2

IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level3 echo        ...level3...
IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level3 XCOPY /S /I /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\level3 %WEBFOLDER%\level3

IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level4 echo        ...level4...
IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level4 XCOPY /S /I /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\level4 %WEBFOLDER%\level4

IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level5 echo        ...level5...
IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level5 XCOPY /S /I /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\level5 %WEBFOLDER%\level5

IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level6 echo        ...level6...
IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level6 XCOPY /S /I /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\level6 %WEBFOLDER%\level6

echo.
echo.
echo        WEB ROOT:
echo        ...*.html...
COPY /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\*.html %WEBFOLDER%

echo        ...*.js...
COPY /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\*.js %WEBFOLDER%

echo        ...*css...
COPY /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\*.css %WEBFOLDER%

:: NOTE:  'SECTIONS' folder was never copied to the web, so it doesn't need to be deleted.
:: DELETE 'SECTIONS' HTML files from web...
echo.
echo        REMOVING FROM WEB ROOT:
IF EXIST %WEBFOLDER%\indexSec.html echo        ...indexSec.html...
IF EXIST %WEBFOLDER%\indexSec.html DEL %WEBFOLDER%\indexSec.html

IF EXIST %WEBFOLDER%\MCC_SECTIONS_TOC.html echo        ...MCC_SECTIONS_TOC.html...
IF EXIST %WEBFOLDER%\MCC_SECTIONS_TOC.html DEL %WEBFOLDER%\MCC_SECTIONS_TOC.html

IF EXIST %WEBFOLDER%\MCC_SECTIONS_TOC_DIR.html echo        ...MCC_SECTIONS_TOC_DIR.html...
IF EXIST %WEBFOLDER%\MCC_SECTIONS_TOC_DIR.html DEL %WEBFOLDER%\MCC_SECTIONS_TOC_DIR.html

echo.
echo ...FINISHED COPYING HTML FILES TO %WEBFOLDER%
echo.
echo.




:STEPP
echo.
echo.
echo       *******************************************************************
echo       * P - Index HTML for Search Engine (IDX.exe)
echo       *******************************************************************
echo.
echo.
echo.
echo 		!!!	CTRL-C to ABORT	!!!



REM CALL %DRIVE%\SOURCE\IDX\idx.pl %ACCTNUM% %WEBDRIVE% %TOPAUSE%
CALL %DRIVE%\APPS\idx.exe %ACCTNUM% %WEBDRIVE% %TOPAUSE%


:STEPQ
echo.
echo.
echo       *******************************************************************
echo       * Q - Build Municode Desktop Folder
echo       *******************************************************************
echo.
echo.
echo		Creating Municode Desktop product for Account #%ACCTNUM%
echo.
echo.
echo.
echo       		!!!	CTRL-C to ABORT	!!!
echo.
echo.



::Calls municode_desktop.exe from %DRIVE%\apps
REM CALL %DRIVE%\SOURCE\Municode_Desktop\Municode_Desktop.pl %ACCTNUM% %DRIVE% %WEBDRIVE% %TOPAUSE%
CALL %DRIVE%\APPS\Municode_Desktop.exe %ACCTNUM% %DRIVE% %WEBDRIVE% %TOPAUSE%


:STEPW
echo.
echo.
echo       *******************************************************************
echo       * W - Create Zip files and Copy to ~\download\Folders
echo       *******************************************************************
echo.
echo.
echo		Creating ZIP FILES for product for Account #%ACCTNUM%
echo.
echo.
echo.
echo       		!!!	CTRL-C to ABORT	!!!
echo.
echo.



:: CHDIR TO %DRIVE%\accts\%ACCTNUM%
CD /D %DRIVE%\Accts\%ACCTNUM%

IF EXIST .\XML\BANNER_INFORMATION.txt COPY .\XML\BANNER_INFORMATION.txt .

CALL ZIPALL.bat %ACCTNUM% %TOPAUSE%

echo ======================================================
echo.
echo.
echo Ready to COPY ZIP FILES to %WEBDRIVE%\download Folders (HTML, Municode_Desktop, RTF, XML, ZIPALL)?
echo.
echo        (CTRL+C = ABORT...        an other key to continue...)
echo.
echo.

REM 

echo.
echo SUSTR=%SUSTR%
echo.
PAUSE

::CHECKHTML
IF EXIST %ACCTNUM%HTML.zip echo        ...Copying %ACCTNUM%HTML.zip  to  ~\download\HTML...
IF EXIST %ACCTNUM%HTML.zip COPY /Y %ACCTNUM%HTML.zip %WEBDRIVE%\download\HTML 

::CHECKMCDT
IF EXIST %ACCTNUM%MunicodeDesktop.zip echo        ...Copying %ACCTNUM%MunicodeDesktop.zip  to  ~\download\Municode_Desktop...
IF EXIST %ACCTNUM%MunicodeDesktop.zip COPY /Y %ACCTNUM%MunicodeDesktop.zip %WEBDRIVE%\download\Municode_Desktop 

::CHECKRTF
IF EXIST %ACCTNUM%RTF.zip echo        ...Copying %ACCTNUM%RTF.zip  to  ~\download\RTF...
IF EXIST %ACCTNUM%RTF.zip COPY /Y %ACCTNUM%RTF.zip %WEBDRIVE%\download\RTF

::CHECKXML
IF EXIST %ACCTNUM%XML.zip echo        ...Copying %ACCTNUM%XML.zip  to  ~\download\XML...
IF EXIST %ACCTNUM%XML.zip COPY /Y %ACCTNUM%XML.zip %WEBDRIVE%\download\XML 

::CHECKZIPALL
IF NOT EXIST %WEBDRIVE%\download\ZIP_ALL MKDIR %WEBDRIVE%\download\ZIP_ALL
IF EXIST %ACCTNUM%ZIPALL_%SUSTR%.zip echo        ...Copying %ACCTNUM%ZIPALL_%SUSTR%.zip  to  ~\download\ZIP_ALL...
IF EXIST %ACCTNUM%ZIPALL_%SUSTR%.zip COPY /Y %ACCTNUM%ZIPALL_%SUSTR%.zip %WEBDRIVE%\download\ZIP_ALL

GOTO FIN


REM ===================================================
REM ===================================================
REM = ROUTINES
REM ===================================================

GOTO:EOF

:ERR_NOJOB
echo.
echo.
echo ERROR!!!  YOU MUST ENTER A JOB (Account) 'NUMBER' (to get the banner text).
echo.
echo.    Example:    xml2html 12345 (enter)
echo.
echo.
echo.
echo.
GOTO END

:STEPX
:CANCEL
echo.
echo CANCELLED!
GOTO END


:INVALIDXML
echo.
echo.
echo ERROR!!!  Your XML file ABOVE did  NOT VALIDATE!!!
echo.
echo.
GOTO END


:FIN
echo.
echo.
echo.
echo.
echo.
echo 					    FINISHED!
echo.
echo.
echo.
echo.
echo 				Another professionally built web!
echo.
echo.				Thanks to YOU, %username%!
echo.				     (You are Wonderful!!!)
echo.
echo.
echo.
echo.
echo.

:END
echo.
echo.
