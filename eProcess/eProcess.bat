@echo off
REM	Name: eProcess.bat
REM	BY: David Nichols

SET ACCTNUM=%1
SET EIJOBNUM=%2
SET UPDATESTR=%3
SET POSTTIME=%4

SET DRIVE=V:
SET WEBDRIVE=O:

SET XSLT=%DRIVE%\XSLT\MCC
SET XSD=%DRIVE%\SCHEMA\MCC\XSD\CODE\code.xsd
SET SAXON=java -Xms128m -Xmx1g net.sf.saxon.Transform
SET ALTOVAXML=%DRIVE%\APPS_3rdPARTY\AltovaXML.exe
SET CWD=%CD%
SET CWDJOB=%CWD%

SET TOPAUSE=-nopause
SET IMAGES=..\HTML\Images

SET ex_TIF=-x!images\*.tif

SET ZIPXML=%DRIVE%\APPS_3rdPARTY\7zip_32\7z.exe a %ACCTNUM%XML.zip -tzip Book_Final.xml %IMAGES% %ex_TIF%
IF DEFINED ProgramFiles(x86) SET ZIPXML=%DRIVE%\APPS_3rdPARTY\7zip_64\7z.exe a %ACCTNUM%XML.zip -tzip Book_Final.xml %IMAGES%

SET WEBFOLDER=%WEBDRIVE%\html\%ACCTNUM%

REM ===== echo local variables =====
echo.
echo LOCAL VARIABLES:
echo DRIVE=%DRIVE%
echo WEBDRIVE=%WEBDRIVE%
echo WEBFOLDER=%WEBFOLDER%
echo	XSLT=%XSLT%
echo	SAXON=%SAXON%
echo	ALTOVAXML=%ALTOVAXML%
echo	CWD=%CWD%
echo	CWDJOB=%CWDJOB%
echo	JOB=%ACCTNUM%
echo	IMAGES=%IMAGES%
echo	ZIPXML=%ZIPXML%
echo.

SET INXML=''
SET XSL=''
SET OUTXML=''
SET MISSING_DIR_ERR=TRUE

IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\XML SET MISSING_DIR_ERR=FALSE
IF '%MISSING_DIR_ERR%'=='TRUE' GOTO ERR_NODIR

CD\
CD /D %DRIVE%
CD ACCTS\%ACCTNUM%\XML

:: Temp
pause



::RTF
CALL %DRIVE%\Apps\X2H_DIVS.bat %CWD% %ACCTNUM% %DRIVE% -nopause




:: BookXML
CALL %DRIVE%\Apps\BookXML.exe %TOPAUSE%


:: BookAll
SET INXML=Book.xml
SET XSL=%XSLT%\book.xsl
@echo Start time: %time%
echo -------------------------------------------------------------------------
@echo on
CALL %SAXON% -s:%INXML%		-xsl:%XSL%	-o:%OUTXML% -t
@echo off
echo.


:: XML Struct
CALL %DRIVE%\Apps\XMLStruct.exe --job=%ACCTNUM% %TOPAUSE%="-nopause"
echo         Validating:  Book_Struct.xml...    (Please wait)
echo.
CALL %ALTOVAXML% -v Book_Struct.xml
echo.
IF [%ERRORLEVEL%]  =="1" GOTO ERR_INVALIDXML




:: ImageMeta
CALL %DRIVE%\Apps\ImageMeta.exe  %TOPAUSE%="-nopause"


:: Book Struct IMG
CALL %ALTOVAXML% -v Book_StructIMG.xml
echo.
IF [%ERRORLEVEL%]  ==[1] GOTO INVALIDXML




:: Xref
SET INXML=Book_StructIMG.xml
SET XSL=%XSLT%\xref.xsl

 
:: (Copy XREF Config...)
echo.
echo Copying Master xref from %DRIVE%\Master\XrefConfig\XrefConfig.xml...
echo.

XCOPY /Y %DRIVE%\Master\XrefConfig\XrefConfig.xml %DRIVE%\accts\%ACCTNUM%\gc\config\

echo.
echo ...XrefConfig.xml copied from %DRIVE%\Master\XrefConfig\XrefConfig.xml!
echo.
echo.
echo CREATING X-REFS FOR ACCOUNT NO. %ACCTNUM%

%DRIVE%\apps\xref\xreffinder -i: Book_StructIMG.xml -o: Book_Xref.xml -c: %DRIVE%\accts\%ACCTNUM%\gc\config\XrefConfig.xml





:: Meta
INXML = %INXML%
echo.


SET XSL=%XSLT%\meta.xsl
@echo Start time: %time%

@echo on
CALL %SAXON% -s:%INXML%		-xsl:%XSL%	-o:%OUTXML% -t
@echo off




:: Final

SET INXML=Book_Meta.xml
SET XSL=%XSLT%\breadcrumbs.xsl
@echo Start time: %time%

@echo on
CALL %SAXON% -s:%INXML%		-xsl:%XSL%	-o:%OUTXML% -t name="%USERNAME%"
@echo off



:: Zip XML
call %ZIPXML%



:: Copy XMLFinal to HTML
COPY Book_Final.xml ..\HTML


:: BookSplit.xsl
CD ..\HTML
CALL %DRIVE%\Apps\BookSplit_XML.bat Book_Final.xml %DRIVE% %TOPAUSE%


:: Build HTML
CALL %DRIVE%\Apps\BookX2H.bat n%1_Book.xml %DRIVE% %TOPAUSE%


:: TOC
SET INXML=Book_Final.xml
SET XSL=%XSLT%\TOC_MCC.xsl
@echo Start time: %time%
echo -------------------------------------------------------------------------
@echo on
CALL %SAXON% -s:%INXML%		-xsl:%XSL%	-o:%OUTXML% -t
@echo off




:: Contegra TOC
SET INXML=Book_Final.xml
SET XSL=%XSLT%\TOC_CONTEGRA_TEMP.xsl
@echo Start time: %time%
echo -------------------------------------------------------------------------
@echo on
CALL %SAXON% -s:%INXML%		-xsl:%XSL%	-o:%OUTXML% -t
@echo off
echo.
echo.
SET INXML=%ACCTNUM%_Contegra_TOC_TEMP.xml
SET XSL=%XSLT%\TOC_CONTEGRA_nodeKey.xsl
@echo Start time: %time%
echo -------------------------------------------------------------------------
@echo on
CALL %SAXON% -s:%INXML%		-xsl:%XSL%	-o:%OUTXML% -t
@echo off



::Copy Files


:: Initialize %WEBDRIVE%\html\%ACCTNUM%...
RMDIR /S /Q %WEBFOLDER%
MKDIR %WEBFOLDER%


COPY /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\%ACCTNUM%.xml %WEBDRIVE%\HTML

:: Copying the content inside the HTML folder to the web...
MD %WEBFOLDER%\images

COPY /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\images\*.jpg %WEBFOLDER%\images
COPY /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\images\*.eps %WEBFOLDER%\images
COPY /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\images\*.pdf %WEBFOLDER%\images

IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level1 echo        ...level1...
IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level1 XCOPY /S /I /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\level1 %WEBFOLDER%\level1

IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level2 echo        ...level2...
IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level2 XCOPY /S /I /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\level2 %WEBFOLDER%\level2

IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level3 echo        ...level3...
IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level3 XCOPY /S /I /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\level3 %WEBFOLDER%\level3

IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level4 echo        ...level4...
IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level4 XCOPY /S /I /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\level4 %WEBFOLDER%\level4

IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level5 echo        ...level5...
IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level5 XCOPY /S /I /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\level5 %WEBFOLDER%\level5

IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level6 echo        ...level6...
IF EXIST %DRIVE%\ACCTS\%ACCTNUM%\HTML\level6 XCOPY /S /I /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\level6 %WEBFOLDER%\level6

COPY /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\*.html %WEBFOLDER%

COPY /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\*.js %WEBFOLDER%

COPY /Y %DRIVE%\ACCTS\%ACCTNUM%\HTML\*.css %WEBFOLDER%

:: NOTE:  'SECTIONS' folder was never copied to the web, so it doesn't need to be deleted.
:: DELETE 'SECTIONS' HTML files from web...

IF EXIST %WEBFOLDER%\indexSec.html echo        ...indexSec.html...
IF EXIST %WEBFOLDER%\indexSec.html DEL %WEBFOLDER%\indexSec.html

IF EXIST %WEBFOLDER%\MCC_SECTIONS_TOC.html echo        ...MCC_SECTIONS_TOC.html...
IF EXIST %WEBFOLDER%\MCC_SECTIONS_TOC.html DEL %WEBFOLDER%\MCC_SECTIONS_TOC.html

IF EXIST %WEBFOLDER%\MCC_SECTIONS_TOC_DIR.html echo        ...MCC_SECTIONS_TOC_DIR.html...
IF EXIST %WEBFOLDER%\MCC_SECTIONS_TOC_DIR.html DEL %WEBFOLDER%\MCC_SECTIONS_TOC_DIR.html




:: Index
CALL %DRIVE%\APPS\idx.exe %ACCTNUM% %WEBDRIVE% %TOPAUSE%




:: Municode Desktop
CALL %DRIVE%\APPS\Municode_Desktop.exe %ACCTNUM% %DRIVE% %WEBDRIVE% %TOPAUSE%



:; ZipAll
CD /D %DRIVE%\Accts\%ACCTNUM%
IF EXIST .\XML\BANNER_INFORMATION.txt COPY .\XML\BANNER_INFORMATION.txt .
CALL ZIPALL.bat %ACCTNUM% %TOPAUSE%

echo SUSTR=%SUSTR%
echo.
PAUSE

::CHECKHTML
IF EXIST %ACCTNUM%HTML.zip echo        ...Copying %ACCTNUM%HTML.zip  to  ~\download\HTML...
IF EXIST %ACCTNUM%HTML.zip COPY /Y %ACCTNUM%HTML.zip %WEBDRIVE%\download\HTML 

::CHECKMCDT
IF EXIST %ACCTNUM%MunicodeDesktop.zip echo        ...Copying %ACCTNUM%MunicodeDesktop.zip  to  ~\download\Municode_Desktop...
IF EXIST %ACCTNUM%MunicodeDesktop.zip COPY /Y %ACCTNUM%MunicodeDesktop.zip %WEBDRIVE%\download\Municode_Desktop 

::CHECKRTF
IF EXIST %ACCTNUM%RTF.zip echo        ...Copying %ACCTNUM%RTF.zip  to  ~\download\RTF...
IF EXIST %ACCTNUM%RTF.zip COPY /Y %ACCTNUM%RTF.zip %WEBDRIVE%\download\RTF

::CHECKXML
IF EXIST %ACCTNUM%XML.zip echo        ...Copying %ACCTNUM%XML.zip  to  ~\download\XML...
IF EXIST %ACCTNUM%XML.zip COPY /Y %ACCTNUM%XML.zip %WEBDRIVE%\download\XML 

::CHECKZIPALL
IF NOT EXIST %WEBDRIVE%\download\ZIP_ALL MKDIR %WEBDRIVE%\download\ZIP_ALL
IF EXIST %ACCTNUM%ZIPALL_%SUSTR%.zip echo        ...Copying %ACCTNUM%ZIPALL_%SUSTR%.zip  to  ~\download\ZIP_ALL...
IF EXIST %ACCTNUM%ZIPALL_%SUSTR%.zip COPY /Y %ACCTNUM%ZIPALL_%SUSTR%.zip %WEBDRIVE%\download\ZIP_ALL

GOTO FIN




:FIN
echo.
echo Done?
echo.

pause



REM ////////////////////////////////
REM			ERROR ROUTINES
REM ////////////////////////////////

:ERR_NODIR

REM will eventually pass some parms to the error reporting app
echo.
echo there is a drive and/or account missing!
echo.

pause