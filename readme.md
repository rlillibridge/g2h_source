# IMPORTANT: #
To compile your Perl script files using ActiveState PDK, be sure to add the following to the top of your *.pl file.

Example:

### ActiveState PDK COMPILE TO EXE REQUIRES: 
    
    BEGIN {
    	push (@INC, "c:\/Perl\/lib");
    	push (@INC, "c:\/Perl/site\/lib");
    	push (@INC, "\.");
    }


*NOTE:
ActiveState PDK v9.2 requires Perl v5.16.*