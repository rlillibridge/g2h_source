#!/usr/bin/perl -w
#Banners.pl
#
# Written By:	Raymond Lillibridge
# PURPOSE:
#	Replace folioban.bat

my $myVersion = '2010.11.04.1';

# CHANGE HISTORY:
# 20101104 - Added logic to detect drive from where app is running as to avoid hard-coding paths
# 20100902 - Change old data connection information.

# PARAMETER
my $parmID=shift;

umask 0;
#=============================================	
#	Config Name and Location...
#=============================================	
my $sJobSpecs_ConfigName = "JobSpecs_config.xml";

if (!defined $PerlApp::VERSION) {
		$APPpath = $0;
}	else {
		$APPpath = PerlApp::exe();
}
my $driveletter = substr($APPpath,0,2);
my $sAppPath= ($driveletter . "\\APPS\\CONFIG");					#LOCAL (location of file above)
#my	$sAppPath= "\\\\mcc-pub-05\\MCC_XPP";		#NETWORK


#==============================================================================
# PRAGMA
#==============================================================================
use warnings;
use DBI;
use XML::Simple qw(:strict);
use Data::Dumper;
#use File::Basename;
#use File::chdir;
use Term::InKey;
use Win32;

#=== Win32 ===
use Cwd;
my $CWDSAVED;
my $WinLoginName = getlogin || getpwuid($<) || "Win Login-UNKNOWN-";

# Date and Time vars
my($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime;
$year += 1900;
my $now_string = localtime;
my $nowYYYYMMDD_HHMMSS= dateTime();
my $LogDateTime =	$nowYYYYMMDD_HHMMSS->{'dateNr'} .  "_" . 
					$nowYYYYMMDD_HHMMSS->{'HHMMSS'};


# SQL INFO
my $dsn=q/dbi:ODBC:BIS/;	#system DSN
my $user=q/BIS_dbreader/;
my $pwd=q/ReadOnly1/;
my $dbh;	#database handle
my $sth;	#statement handle
my $query="";

#my $dsn="BIS";	#local DSN
#my $pwd="sa";
#
##my $dsn="BIS";	#system DSN
##my $user="sa";
##my $pwd="concasturbate";
#my $dbh;	#database handle
#my $sth;	#statement handle


my $ProductID="-empty-";	#result set ProductID			TEST WITH:  14139
my $bannerText="-empty-";	#result set bannerText
my @banner=();
my @FFF=();
my @RTF=();
my @HTM=();
my $str1;
my $str2;
my $str3;

#==============================================================================
#Globals
#==============================================================================
my $keyPressed;
my $query="";
my @row;
my $dashes="------------------------------------------------------------";
my $rtn;
my $username=$WinLoginName;
my $Path;
my $FolioPath_acctno;
my $XMLconfig;
my $config_FolioPath;
my $cwd="-empty-";



#==============================================================================
#==============================================================================
# MAIN  MAIN  MAIN  MAIN  MAIN  MAIN  MAIN  MAIN  MAIN  MAIN  MAIN  MAIN  MAIN  MAIN  MAIN  MAIN  MAIN  MAIN  
#==============================================================================
#==============================================================================
	&Housekeeping();
	&Get_JobSpecs_config;
	&FetchRecord();
	
	if($ProductID eq "-empty-")	{
		print "\n\n$dashes\n\nERROR!!!\n" .
			"Account entered:  $parmID IS NOT FOUND in BIS View 'vw_ProductBannerText'\n\n$dashes\n\n";	
		exit 0;
	}
	
	print "\nHERE IS YOUR DATA!\n\n==============================\n" . 
		"\$ProductID=$ProductID\n\n" . 
		"\$bannerText:\n$bannerText\n==============================\n\n";
	
#	print "\$username=$username\n\n";
#	print "\$config_FolioPath=$config_FolioPath\n";
	$FolioPath_acctno = $config_FolioPath . "\\" . $ProductID;
	
	# Verify $FolioPath_acctno...
	if(-e $FolioPath_acctno)	{
#		print "\$FolioPath_acctno=$FolioPath_acctno\n";
		$CWD=$FolioPath_acctno;
		print "\$CWD=$CWD\n";
	} else {
		print "\n\n$dashes\n\nERROR!!!  Path Not Found:\n $FolioPath_acctno\n\n$dashes\n\n";	
		exit 0;
	}
	
	$FolioPath_acctno = $FolioPath_acctno . "\\mag";

	if(-e $FolioPath_acctno)	{
#		print "\$FolioPath_acctno=$FolioPath_acctno\n";
		$CWD=$FolioPath_acctno;
		print "\$CWD=$CWD\n";
	} else {
		print "\n\n$dashes\n\nERROR!!!  MAG Path Not Found:\n $FolioPath_acctno\n\n$dashes\n\n";
		exit 0;
	}
	
	# OK, at this point you are ready to cd to each ~/mag/subfolder and create each banner file...
	@banner = split(/\n/, $bannerText);

	print "\nDUMPING \@banner...\n";
	foreach my $item (@banner)	{
		print "\t$item\n";	
	}
	
	if(-e "$CWD\\FFFLP")	{	&Create_AABANNER_FFF();	}
	if(-e "$CWD\\HTML")	{	&Create_AABANNER_HTM();	}
	if(-e "$CWD\\RTF")	{	&Create_AABANNER_RTF();	}
	
	exit 1;


#==============================================================================
#	SUBROUTINES
#==============================================================================

#-------------------------------------------------------------
sub BadConfigData {
#-------------------------------------------------------------
	my ($parm1, $parm2) = @_;
	my $me = whoami();
	
	$BadConfig=1;
	
	print "ERROR:  \<$parm1\>=$parm2\n";
}


#-------------------------------------------------------------
sub Create_AABANNER_FFF {
#-------------------------------------------------------------
	my $i;
	my $x;
	
	$CWDSAVED=$CWD;
	
	if(-e "$FolioPath_acctno\\FFFLP")	{
		$CWD=$FolioPath_acctno . "\\" . "FFFLP";
	} else {
		print "\n\n$dashes\n\nERROR!!!  Path Not Found:\n $FolioPath_acctno\\FFFLP\n\n$dashes\n\n";
		exit 0;
	}
	
	print "\n\$CWD=$CWD\n";
	print "\nCreating:  AABANNER.FFF ... ";
	
	open(FFFOUT, '>',  "$CWD\\AABANNER.FFF")
		or die "CAN NOT CREATE AABANNER.FFF \n$!\n"; 

	for($i = 0; $i <= $#FFF; $i++)	{
		if($i == 0)	{
			$str1 = $FFF[$i];
			$str1 =~ s|^FFF:||g;
			$str1 =~ s|\+\+\+ACCOUNT\+\+\+|$parmID|g;
			print FFFOUT "$str1";
		} else {
			$str1 = $FFF[$i];
			$str1 =~ s|^FFF:||g;
			
			for(my $x = 0; $x <= $#banner; $x++)	{
				$str2 = $str2 . $banner[$x];
				if($x < $#banner)	{
				 	$str2 = $str2 . "<CR>";
				}
			}
			
			$str2 =~ s|\x{0D}||mg;
			$str2 =~ s|\x{0A}||mg;
			
			$str1 =~ s|\+\+\+BANNER\+\+\+|$str2|g;
			print FFFOUT "$str1";
		}
	}
	
	close FFFOUT;
	print "DONE!\n\n";
	$CWD=$CWDSAVED;
}


#-------------------------------------------------------------
sub Create_AABANNER_HTM {
#-------------------------------------------------------------
	my $i;
	my $x;

	$CWDSAVED=$CWD;

	if(-e "$FolioPath_acctno\\HTML")	{
		$CWD=$FolioPath_acctno . "\\" . "HTML";
	} else {
		print "\n\n$dashes\n\nERROR!!!  Path Not Found:\n $FolioPath_acctno\\HTML\n\n$dashes\n\n";
		exit 0;
	}
	
	print "\n\$CWD=$CWD\n";
	print "\nCreating:  AABANNER - Banner Page.HTM ... ";
	
	open(HTMLOUT, ">",  "$CWD\\AABANNER - Banner Page.HTM")
		or die "CAN NOT CREATE AABANNER - Banner Page.HTM \n$!\n"; 
		
	print HTMLOUT "\n";
		
	for($i = 0; $i <= $#HTM; $i++)	{
		if($HTM[$i] =~ /^HTMbanner:/)	{
			$str1 = $HTM[$i];
			$str1 =~ s|^HTMbanner:||g;
			
			for($x = 0; $x <= $#banner; $x++)	{
				$str2 = $banner[$x];				
				$str2 =~ s|\x{0D}||mg;
				$str2 =~ s|\x{0A}||mg;
				$str3 = $str1;
				$str3 =~ s|\+\+\+BANNER\+\+\+|$str2|g;
				print HTMLOUT "$str3";
			}
			
			# empty para...
			$str3 = $str1;
			$str3 =~ s|\+\+\+BANNER\+\+\+||g;
			print HTMLOUT "$str3";
			
		} elsif($HTM[$i] =~ /^HTManchor:/)	{
			$str1 = $HTM[$i];
			$str1 =~ s|^HTManchor:(.+?)|$1|g;
			opendir(HTMLDIR, "$CWD")	or die "Couldn't open $CWD for reading: $!";
			
			while (defined(my $htmfile = readdir HTMLDIR)) {
				next if(
					($htmfile eq "." || $htmfile eq "..")	|
					($htmfile !~ / - /)					|
					($htmfile =~ /AABANNER/)
				); 
				
				if(!-d $htmfile)	{
					$str2=substr($htmfile, 0, (length($htmfile) - 4));
					$str3=$str1;
					$str3 =~ s|\+\+\+FNAME\+\+\+|$str2|g;
					print HTMLOUT "$str3";
				}
			}
			closedir HTMLDIR	or die "Couldn't CLOSE HTMLDIR file handle: $!";
			
		}
	}
	
	close HTMLOUT;
	print "DONE!\n\n";
	$CWD=$CWDSAVED;
}


#-------------------------------------------------------------
sub Create_AABANNER_RTF {
#-------------------------------------------------------------
	my $i;
	my $x;
	
	$CWDSAVED=$CWD;
	
	if(-e "$FolioPath_acctno\\RTF")	{
		$CWD=$FolioPath_acctno . "\\" . "RTF";
	} else {
		print "\n\n$dashes\n\nERROR!!!  Path Not Found:\n $FolioPath_acctno\\RTF\n\n$dashes\n\n";
		exit 0;
	}
	
	print "\n\$CWD=$CWD\n";
	print "\nCreating:  AABANNER - Banner Page.RTF ... ";
	
	open(RTFOUT, '>',  "$CWD\\AABANNER - Banner Page.RTF")
		or die "CAN NOT CREATE AABANNER - Banner Page.RTF \n$!\n"; 

	for($i = 0; $i <= $#RTF; $i++)	{
		if($RTF[$i] !~ /\+\+\+BANNER\+\+\+/)	{
			$str1 = $RTF[$i];
			$str1 =~ s|^RTF:||g;
			print RTFOUT "$str1";
		} else {
			$str1 = $RTF[$i];
			$str1 =~ s|^RTF:||g;			
#			$str1 =~ s|\x{0D}||mg;
#			$str1 =~ s|\x{0A}||mg;
			
			for(my $x = 0; $x <= $#banner; $x++)	{
				if($banner[$x] !~ /^\x{0D}/)	{
					$str2 = $str1;
					$str2 =~ s|\+\+\+BANNER\+\+\+|$banner[$x]|g;
					print RTFOUT "$str2\n";
				}
			}
		}
	}
	
	close RTFOUT;
	print "DONE!\n\n";
	$CWD=$CWDSAVED;
}


#-------------------------------------------------------------
sub dateTime {
#-------------------------------------------------------------
	my $me = whoami();
	my ($sec,$min,$hour,$mday,$monNr,$year);
	my $now = {};
	#get date and time
	($sec,$min,$hour,$mday,$monNr,$year) = localtime(time());
    	my ($date, $date_nr, $clock);
	my $month=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")[$monNr];
    	$monNr++;
	$monNr = sprintf("%02d",$monNr);
	$mdayNr = sprintf("%02d",$mday);
	$sec = sprintf("%02d",$sec);
	$min = sprintf("%02d",$min);
	$hour = sprintf("%02d",$hour);
	$year= 1900 + $year;
	$now->{'date'} = "$year-$month-$mday";
	$now->{'dateNr'} = "$year$monNr$mdayNr";
	$now->{'time'} = "$hour:$min:$sec";
	$now->{'HHMMSS'} = "$hour$min$sec";
	return($now);		
}


#-------------------------------------------------------------
sub Dump_Config	{
#-------------------------------------------------------------
	my $me = whoami();
	my $i;
	
	 print STDOUT "\n--------------------- Dump_Config: \$XMLconfig ----------------------\n";
#	 my $myDump = Dumper($XMLconfig);
#	 print STDOUT $myDump;
	
#	print STDOUT "\$config_allusers=$config_allusers\n";
	print STDOUT "\$username=$username\n";
#	print STDOUT "\$config_PrintSpecsTemp=$config_PrintSpecsTemp\n";
#	print STDOUT "\$config_LogFile=$config_LogFile\n";
#	print STDOUT "\$config_AccountPath=$config_AccountPath\n";
#	print STDOUT "\$config_BackupPath=$config_BackupPath\n";
	print STDOUT "\$config_FolioPath=$config_FolioPath\n";
#	print STDOUT "\$config_CKListPath=$config_CKListPath\n";
#	
#	print STDOUT "\nInclude Filters (\@config_IncludeFilters):\n";
#	for( $i = 0;  $i <= $#config_IncludeFilters; $i++)	{
#		print STDOUT "\t[$i] = $config_IncludeFilters[$i]\n";
#	
#	}
#	
#	print STDOUT "\nExclude Filters (\@config_ExcludeContainsFilters):\n";
#	for( $i = 0;  $i <= $#config_ExcludeContainsFilters; $i++)	{
#		print STDOUT "\t[$i] = $config_ExcludeContainsFilters[$i]\n";
#	
#	}
	
	print STDOUT "\n-------------------END DUMPER: \$XMLconfig --------------------\n\n";
}



#-------------------------------------------------------------
sub FetchRecord()	{
#-------------------------------------------------------------
	my ($ProdID, $banner);
	my $me = whoami();
	
	# QUERY...
	$query="SELECT ProductID, bannerText FROM vw_ProductBannerText WHERE ProductID = '$parmID' and bannerText IS NOT  NULL";
	 $dbh->{ReadOnly} = 1;
	 $sth = $dbh->prepare($query) or die "Can't prepare query: $query\n$!";
	 $sth->execute;
	
	#Bind the results to local variables...
	$sth->bind_columns(undef, \$ProdID, \$banner );
	
	#retrieve values from the result set...
	while( $sth->fetch() ) {
		#print "$dashes\n\tID=$ProdID\n$dashes\n$banner\n\n\n";
		$ProductID = $ProdID;
		$bannerText = $banner;
	}
	#Close the connection
	$sth->finish();
	
}


#----------------------------------------------------------
sub Get_JobSpecs_config()	{
#----------------------------------------------------------
	my $me = whoami();
	my @config = ();
	
	my $JobSpecsConfigFile = "$sAppPath" . "\\" . "$sJobSpecs_ConfigName";

	eval { # JobSpecs_config.xml exist?...
	     if(!-e ($JobSpecsConfigFile))	{
				do_Error(whoami(), "The $JobSpecsConfigFile file does not exist!");
		}
	};	
	if($@) {
		print STDOUT "\n++++++++++++++++++++\nError:\n$@\n++++++++++++++++++++\n";
		&SpecsExit(1);
	}
			
	my $xs = XML::Simple->new(
		forcearray => 1,
		KeepRoot => 0,
		KeyAttr => [ ]
	);
	
	$XMLconfig = $xs->XMLin($JobSpecsConfigFile);	
	
#	$config_allusers = $XMLconfig->{allusers}->[0];
#	$config_PrintSpecsTemp = $XMLconfig->{printspecsfolder}->[0];
#	$config_JobSpecsFile = $XMLconfig->{jobspecsfile}->[0];
#	$config_LogFile = $XMLconfig->{logfile}->[0];
#	$config_AccountPath = $XMLconfig->{accountfolder}->[0];
#	$config_BackupPath = $XMLconfig->{backupfolder}->[0];
	$config_FolioPath = $XMLconfig->{foliofolder}->[0];
#	$config_OldProcessFolder = $XMLconfig->{oldprocessfolder}->[0];
#	$config_NewProcessFolder = $XMLconfig->{newprocessfolder}->[0];
	
#	my $REF_config_IncludeFilters = $XMLconfig->{inputfilters}[0]->{include};
#	@config_IncludeFilters = @$REF_config_IncludeFilters;

#	my $REF_config_ExcludeEqualsFilters = $XMLconfig->{inputfilters}[0]->{exclude_equals};
#	@config_ExcludeEqualsFilters = @$REF_config_ExcludeEqualsFilters;
	
#	my $REF_config_ExcludeContainsFilters = $XMLconfig->{inputfilters}[0]->{exclude_contains};
#	@config_ExcludeContainsFilters = @$REF_config_ExcludeContainsFilters;
	
#	my $REF_config_ExcludeFilter = $XMLconfig->{inputfilters}[0]->{exclude};
#	@config_ExcludeFilter = @$REF_config_ExcludeFilter;

	#Verify that $config_* variables have data...
	$BadConfig=0;
#	if($config_allusers eq "") {&BadConfigData("config_allusers", $config_allusers); }
#	if($config_PrintSpecsTemp eq "") {&BadConfigData("config_PrintSpecsTemp", $config_PrintSpecsTemp); }
#	if($config_JobSpecsFile eq "") {&BadConfigData("config_JobSpecsFile", $config_JobSpecsFile); }
#	if($config_LogFile eq "") {&BadConfigData("config_LogFile", $config_LogFile); }
#	if($config_AccountPath eq "") {&BadConfigData("config_AccountPath", $config_AccountPath); }
#	if($config_BackupPath eq "") {&BadConfigData("config_BackupPath", $config_BackupPath); }
	if($config_FolioPath eq "") {&BadConfigData("config_FolioPath", $config_FolioPath); }
#	if($config_OldProcessFolder eq "") {&BadConfigData("config_OldProcessFolder", $config_OldProcessFolder); }
#	if($config_NewProcessFolder eq "") {&BadConfigData("config_NewProcessFolder", $config_NewProcessFolder); }
		
	if($BadConfig > 0)	{
		exit(1);
	}
	
#	&Dump_Config;

}


#-------------------------------------------------------------
sub Housekeeping()	{
#-------------------------------------------------------------
	my $me = whoami();
	
	system $^O eq 'MSWin32' ? 'cls' : 'clear';
	print "\nRUNNING:  Banners.exe (pl)\t\t$myVersion\t\t$username\n";
	print "*------------------------------------------------------------------------------*\n";
		
	if(!$parmID)	{
		print STDOUT "\n\n$dashes\n$dashes\n\tERROR!  PLEASE ENTER AN ACCOUNT NUMBER!\n$dashes\n$dashes\n";
		exit(0);
	}
	
	print "\nProcessing ProductID:  $parmID\t\tcontinue...\n";
	
	
	## Process the ProductID or quit?
	print "\n----------------------------------------------------------------\n" . 
		" Y = YES          --OR--          \(blank\) or N = CANCEL\n" . 
		"\n----------------------------------------------------------------\n";
		
	$keyPressed = &ReadKey;
	#$keyPressed = "y";
	
	if($keyPressed=~ /[Yy]/) {
		# do nothing...
		print "\n";	#add a little space before processing the files
	} else {
		print "\n\...Processing cancelled.\n";
		exit 0;
	}
	
##	$dbh = DBI->connect("dbi:ODBC:$dsn", $user, $pwd, { RaiseError => 1, odbc_cursortype => 2})  or die "Can NOT connect to BIS\n$!";
#	$dbh = DBI->connect("dbi:ODBC:$dsn", "", "sa", { RaiseError => 1, odbc_cursortype => 2})  or die "Can NOT connect to BIS\n$!";
	
	# Connect to the data source and get a handle for that connection.
	$dbh = DBI->connect($dsn, $user, $pwd)  
		or die "Can't connect to $dsn: $DBI::errstr\n$!";		
			
	&LoadDataArrays();
		
}


#-------------------------------------------------------------
sub LoadDataArrays()	{
#-------------------------------------------------------------

	# __DATA__ pushed into appropriate arrays...
	while(<DATA>){
		if(/^FFF\:/) {
			push @FFF, $_;
		} elsif(/^HTM(.+?)\:/)	{
			push @HTM, $_;
		} elsif(/^RTF\:/)	{
			push @RTF, $_;
		}
	}
	
#	#DUMP FFF...
#	print "\n\nTEST DUMPING __DATA...\n\n";
#	print "\n\nARRAY FFF:\n";
#	print Dumper(\@FFF);
#
#	#DUMP HTM...
#	print "\n\nARRAY HTM:\n";
#	print Dumper(\@HTM);
#
#	#DUMP RTF...
#	print "\n\nARRAY RTF:\n";
#	print Dumper(\@RTF);
}


#-------------------------------------------------------------
sub whoami {
#-------------------------------------------------------------
	(caller(1))[3];
}

#==============================================================================
# SAMPLES OF COMPLETED BANNER FILES:

#FFF:
#<DI:"16334.DEF">
#<RD:"Heading 1">CODE OF ORDINANCES<CR>EL DORADO, KANSAS<CR><CR>Codified through<CR>Ordinance No. G-1072, passed May 18, 2009.<CR>(Supplement No. 7, Rev.)


#HTML:
#<P ALIGN="CENTER">CODE OF ORDINANCES
#<P ALIGN="CENTER">EL DORADO, KANSAS
#<P ALIGN="CENTER">Codified through
#<P ALIGN="CENTER">Ordinance No. G-1072, passed May 18, 2009.
#<P ALIGN="CENTER">Supplement No. 7, Rev.
#<P ALIGN="CENTER"><P><A HREF="APRELIMS - Preliminaries.HTM">APRELIMS - Preliminaries</A>
#<P><A HREF="T00 - CHARTER ORDINANCES.HTM">T00 - CHARTER ORDINANCES</A>
#<P><A HREF="T01 - Title 1  GENERAL PROVISIONS.HTM">T01 - Title 1  GENERAL PROVISIONS</A>
#<P><A HREF="T02 - Title 2  ADMINISTRATION AND PERSONNEL.HTM">T02 - Title 2  ADMINISTRATION AND PERSONNEL</A>
#<P><A HREF="T03 - Title 3  REVENUE AND FINANCE.HTM">T03 - Title 3  REVENUE AND FINANCE</A>
#<P><A HREF="T04 - Title 4  ALCOHOLIC BEVERAGES.HTM">T04 - Title 4  ALCOHOLIC BEVERAGES</A>
#<P><A HREF="T05 - Title 5  BUSINESS LICENSES.HTM">T05 - Title 5  BUSINESS LICENSES</A>
#<P><A HREF="T06 - Title 6  ANIMALS.HTM">T06 - Title 6  ANIMALS</A>
#<P><A HREF="T07 - Title 7  (Reserved).HTM">T07 - Title 7  (Reserved)</A>
#<P><A HREF="T08 - Title 8  HEALTH AND SAFETY.HTM">T08 - Title 8  HEALTH AND SAFETY</A>
#<P><A HREF="T09 - Title 9  PUBLIC PEACE AND WELFARE.HTM">T09 - Title 9  PUBLIC PEACE AND WELFARE</A>
#<P><A HREF="T10 - Title 10  VEHICLES AND TRAFFIC.HTM">T10 - Title 10  VEHICLES AND TRAFFIC</A>
#<P><A HREF="T11 - Title 11  (Reserved).HTM">T11 - Title 11  (Reserved)</A>


# RTF:
#{\rtf1\ansi \deflang1033\deff0{\fonttbl
# {\f0\fnil \fcharset0 \fprq2 Times New Roman;}}
# \margl720\margr720
#\viewkind4\uc1\pard\f0\fs20
#
#\plain \par \par \pard \qc CODE OF ORDINANCES
#
#\plain \par \par \pard \qc EL DORADO, KANSAS
#
#\plain \par \par \pard \qc Codified through
#
#\plain \par \par \pard \qc Ordinance No. G-1072, passed May 18, 2009.
#
#\plain \par \par \pard \qc Supplement No. 7, Rev.
#
#\plain \par \par \pard \qc \pard }


#==============================================================================
__DATA__
FFF:<DI:"+++ACCOUNT+++.DEF">
FFF:<RD:"Heading 1">+++BANNER+++

HTMbanner:<P ALIGN="CENTER">+++BANNER+++
HTManchor:<P><A HREF="+++FNAME+++.HTM">+++FNAME+++</A>

RTF:{\rtf1\ansi \deflang1033\deff0{\fonttbl
RTF: {\f0\fnil \fcharset0 \fprq2 Times New Roman;}}
RTF: \margl720\margr720
RTF:\viewkind4\uc1\pard\f0\fs20
RTF:
RTF:\plain \par \par \pard \qc +++BANNER+++
RTF:\plain \par \par \pard \qc \pard }


