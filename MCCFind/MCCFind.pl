#!/usr/bin/perl
# MCCFind.pl
# USE:  Search for string (recursively) in G:\accts
#
# Programmer:  Raymond Lillibridge
my $myVersion = "Version:  2011-06-16.1 (WIP)";

use strict;
use warnings;
use File::Find;
no warnings 'File::Find';
use Term::InKey;

my $APPpath="";
my $myDT="";

if (!defined $PerlApp::VERSION) {
	$APPpath = $0;
} else {
	$APPpath = PerlApp::exe();
}

Set_myDT();

my $dashes = "----------------------------------------------------------------------";
my $sum = 0;
my $findStr = "";

my $LogFile="MCCFind_OUTPUT.log";
my @FILEHANDLE = (*STDOUT, *LOG);

&Clear;

# Get input...
if($#ARGV < 1)	{
	print STDOUT "Number of arguments:  $#ARGV\n";
	print STDOUT "Please enter a search string AND one or more Folder Name\(s\) to begin your search.\n";
	print STDOUT "Press any key to continue...\n\n";
	ReadKey();
	exit 1;	
}

&CreateLog;

$findStr = shift(@ARGV);

if($findStr eq "") {
	printOut("\n+++++ ERROR!  Find String must NOT be blank.\n");
	close(LOG);
	exit 1;
}

&Clear;
my $currDir = `CD`;
chomp $currDir;


printOut("$dashes\n");
printOut("  MCCFind.pl\t\t$myVersion\n");
printOut("  Date_Time:  \t\t$myDT\n");
printOut("$dashes\n\n");
printOut("CURRENT DIR:  $currDir\n\n");

foreach my $a (@ARGV)	{
	printOut("Search Location:  $a\n");	
}

printOut("\nSEARCH TERM=$findStr\n\n");

# Perform 'wanted' on each file (recursively) in @ARGV...
find( \&wanted, @ARGV);

Set_myDT();

printOut("\n$dashes\n");
printOut("FINISHED!  FINISHED!  FINISHED!  FINISHED!  FINISHED!\n\n");
printOut("Date_Time:  $myDT\n");
printOut("$dashes\n\n");


#============================================================
# SUB ROUTINES
#============================================================

#----------------------------------------------------------
sub wanted {
#----------------------------------------------------------
	my $fn="$_";
	my $line="";
	my $prnFN="";
	
	# Open Text file and search for input string...	
	printOut("$File::Find::name/\n") if -d;
	return if (-d);
	return if (-x);
	return if (!-T);
	return if(/(.*)\.DOC$/i);
	return if(/(.*)\.LOG$/i);
	return if(/(.*)\.JPG$/i);
	return if(/(.*)\.TIF$/i);
	return if(/(.*)\.EPS$/i);
	return if(/(.*)\.HTML$/i);
	return if(/(.*)\.XML$/i);
	return if(/(.*)\.RTF$/i);
	return if(/(.*)\.PDF$/i);
	return if(/(.*)\.ZIP$/i);
		
	# SKIP non-text files...
	open MF, "<", "$fn"
		or die "Can't OPEN FILE:  $fn \n$!\n";
	
	while (<MF>)	{
		$line = $_;
		chomp $line;
		
		if($line =~ /$findStr/)	{
			if($prnFN eq "")	{
				$prnFN = $fn;
				printOut("FILE:  $prnFN\n");	
			}
			printOut("\t$line\n");	
		}
	}
	
	close(MF);
	
	if($prnFN ne "")	{
		printOut("\n");
	}
} 


#----------------------------------------------------------
sub CreateLog() {
#----------------------------------------------------------	
	open(LOG, ">:utf8",  "$LogFile")
		or die "Can't CREATE LOG file $LogFile: $!\n"; 
		
	printOut("==========================================================================\n");
	printOut("$APPpath\t\tDate_Time:  $myDT\n");
	printOut("NOTE:  Skipping Files with extensions - doc, jpg, tif, eps, html, xml,  or log\n");
	printOut("==========================================================================\n\n");
}


#-------------------------------------------------------------
sub dateTime {
#-------------------------------------------------------------
	my ($sec,$min,$hour,$mday,$monNr,$year);
	my $now = {};
	#get date and time
	($sec,$min,$hour,$mday,$monNr,$year) = localtime(time());
    	my ($date, $date_nr, $clock);
	my $month=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")[$monNr];
    	$monNr++;
	$monNr = sprintf("%02d",$monNr);
	my $mdayNr = sprintf("%02d",$mday);
	$sec = sprintf("%02d",$sec);
	$min = sprintf("%02d",$min);
	$hour = sprintf("%02d",$hour);
	$year= 1900 + $year;
	$now->{'date'} = "$year-$month-$mday";
	$now->{'dateNr'} = "$year$monNr$mdayNr";
	$now->{'time'} = "$hour:$min:$sec";
	$now->{'HHMMSS'} = "$hour$min$sec";
	return($now);		
}


#-------------------------------------------------------------
sub printOut {
#-------------------------------------------------------------
	my $parm = shift;
	
	foreach my $outfile (@FILEHANDLE) {
		print $outfile "$parm";
	}
}


#-------------------------------------------------------------
sub Set_myDT {
#-------------------------------------------------------------
	# Date and Time vars
	my $nowYYYYMMDD_HHMMSS= dateTime();
	my $LogDateTime =	$nowYYYYMMDD_HHMMSS->{'dateNr'} .  "_" . 
						$nowYYYYMMDD_HHMMSS->{'HHMMSS'};
	$myDT=$LogDateTime;
	$myDT =~ s|(\d\d\d\d)(\d\d)(\d\d)_(\d\d)(\d\d)(\d\d)|$1\/$2\/$3 $4:$5:$6|g;
}
