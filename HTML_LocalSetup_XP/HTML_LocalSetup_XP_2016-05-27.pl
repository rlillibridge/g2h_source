#!/usr/bin/perl
# HTML_LocalSetup_XP.pl
# Programmer:  Raymond Lillibridge
my $myVersion = "2013-10-07.1 (nopause)";

#=============================================	
# PASSED PARAMETERS:
# (1) DOS starting location (typically:  (local) C:\..<development folder>\XML
# (2) GENCODEPATH (ex.  G:\accts    or    V:\AcctsG)
# (3) CWD
# (4) Account Number
# (5) pause
# (6) debugging  (if not blank)

# COMMAND LINE EXAMPLE ([DOS] cd V:\AcctsG\15201\XML):
# V:\SOURCE\HTML_LocalSetup_XP\HTML_LocalSetup_XP.pl V: V:\AcctsG V:\AcctsR\15201 15201 -pause debug
# V:\G2h_RLillibridge\g2h_source\HTML_LocalSetup_XP\HTML_LocalSetup_XP.pl V: V:\AcctsG V:\AcctsR\99994 99994 -pause debug
#
# Example using a branch of 'localsetup'...
# V:\Apps_RLillibridge\localsetup\HTML_LocalSetup_XP.pl V: V:\AcctsG V:\AcctsR\99994 99994 -pause debug


#=============================================	
# WIP:
# 2013-10-07 - Raymond Lillibridge - Processing using %TOPAUSE% option

#=================================================================================================
#=================================================================================================
# IMPORTANT NOTES - IMPORTANT NOTES - IMPORTANT NOTES - IMPORTANT NOTES - IMPORTANT NOTES - IMPORTANT NOTES:
#=================================================================================================
#=================================================================================================
# gs DOS:  You must add gs\bin and gs\lib to the PATH, where gs is the top-level Ghostscript directory.
#
# gs EPS: 
#Encapsulated PostScript (EPS) files are intended to be encapsulated in another PostScript document and may not display 
#or print on their own. An EPS file must conform to the Document Structuring Conventions, must include a %%BoundingBox: 
#line to indicate the rectangle in which it will draw, must not use PostScript commands which will interfere with the 
#document importing the EPS, and can have either zero pages or one page. Ghostscript has support for handling EPS 
#files, but requires that the %%BoundingBox: be in the header, not the trailer.


#=============================================	
# CHANGE HISTORY:
# 2013-09-16.1 - Raymond Lillibridge - G:\Accts\#####\*.PDF -> ~\Ancillary\ & appropriate copy to HTML folder
# 2013-09-04.1 - Raymond Lillibridge - Re-compile after Malware issues
# 2013-07-29 - 08-12.1 - Raymond Lillibridge - Adding PNG as images, saving images as PNG rather than JPGs
# 2013-04-23.1 - Raymond Lillibridge - Changing LocalSetup_HTML_images_convert. Everything to JPG.
# 2013-02-11.1 - Raymond Lillibridge - Changed "HOMEPATH" to "GENCODEPATH" due to login HOMEPATH conflicts
# 2012-05-24.1 - Raymond Lillibridge - Added PDF -> JPG conversion for use later in HTML2Word processing
# 2012-05-21.1 - David Nichols - increaed the resolution for jpg thumbnails created from EPS/PDF
# 2012-01-20.2 - Raymond Lillibridge - Added IF() wrapper around "Municode_Title_Page_Logo" (while GCIN...)
# 2012-01-20.1 - David or LaRae - Changes "MCC_Logo" to "Municode_Title_Page_Logo"
# 2011-09-06.1 - Raymond Lillibridge - Passing in WORKPATH, removing "G:\accts" strings
# 2011-08-24.1 - Raymond Lillibridge - [FAILED] Copy images basesd on %ENV{SPLITACCT} value
# 2011-08-10.1 - Raymond Lillibridge - Checking on MCC Logo problems...
# 2011-06-09.1 - Raymond Lillibridge - Get MCC_Logo name(s) and copy same named file from G:\accts\mcc_images.
# 2011-04-05.1 - Raymond Lillibridge - Moved NewHTMLsetupFiles folder to %DRIVE%\Master folder
# 2011-03-06 - Raymond Lillibridge - Adding copying of *.js and *.swf files from NewHTMLsetupFiles
# 2010-12-02 - Raymond Lillibridge - Allow for "-nopause" processing
# 2010-12-01 - David Nichols - Added option to pick-up "Custom" CSS @ ..\NewHTMLsetupFiles\CustomCSS\#####_MCC_style.css
# 2010-11-04 - David Nichols - Added logic to detect where app is running from as to avoid hard-coding paths
# 2010-03-18 - Raymond Lillibridge - Corrected bug when creating $basename from 
#    multip dot file name  (ex.  Raymond.1.2.3.4.tif)
# 2010-01-21 - Raymond Lillibridge - Added conversions:
#       TIF -> JPG
#       EPS -> PDF and create a JPG thumbnail previxed "_TN_"
# 2010-01-15 - Raymond Lillibridge - CREATED APPLICATION
# 

#umask 0;
#=============================================	
# PROCESSING OUTLINE
#=============================================	
# This application is called from G2H.bat\G2H_Core.bat.  The purpose is to 
# a.	Create (local) C:\.. <development folder>\ ~\HTML, ~\HTML\images folders
# b.	Copy G:\accts\<A#>\graphics\*.*  ->  ~\HTML\images 
# c.	Convert (gswin32c.exe) ~\HTML\images\*.*  ->  *.png 
# d.	Copy %DRIVE%:\Master\NEWHTMLsetupfiles\*.*  ->  ~\HTML

#=============================================	
# PRAGMA
#=============================================	
use warnings;
use strict;
use File::Copy;
use File::Copy::Recursive qw(dircopy);
use File::Basename;
use Term::InKey;

use Image::Magick;

#=== Win32 ===
use Cwd;

# Sample Parms:  V: V:\AcctsG V:\AcctsR\99994 99994 pause debug
my $driveletter=shift;
my $GenCodePath=shift;
my $CWD=shift;
my $acctNo=shift;
my $NoPause=shift;
my $CWDACCT=$CWD;

my $mydebug = shift;	# debugging turned on if this is not blank

if(!defined $mydebug)	{
	$mydebug="";	
}

if(!defined $NoPause)	{
	$NoPause="";	
}

my $promptUser;
my $APPpath;

my $CWDSAVED;

if (!defined $PerlApp::VERSION) {
		$APPpath = $0;
}	else {
		$APPpath = PerlApp::exe();
}

my $HTML_LocalSetupFolder= $driveletter . "\\Master\\NewHTMLsetupFiles";
my $HTML_imagesSource=$GenCodePath . "\\$acctNo\\Graphics";
my $MCC_imagesSource=$GenCodePath . "\\MCC_images";
my $GenCodeInputSource=$GenCodePath . "\\$acctNo";

my $myHTML="";
my $myHTMLimages="";

my $dashes = "--------------------------------------------------------------------------------";
my $rtn="";

my $logoFiles="";

#================================================
#	MAIN
#================================================

HouseKeeping();
LocalSetup_HTML();
LocalSetup_HTML_images();
LocalSetup_PDFs();
print STDOUT "\n\n====================\n    FINISHED\n====================\n\n";

#print STDOUT "\n\n(Press any key to Exit.)\n";
#$promptUser = &ReadKey;

exit 1;

#================================================
#	END MAIN
#================================================



#================================================
#================================================
#	Subroutines...
#================================================
#------------------------------------------------------------
sub HouseKeeping {
#------------------------------------------------------------
	my $me=whoami();
	my $rtn="";
	$CWDSAVED=cwd();

	if($NoPause ne "nopause")	{
		&Clear;
	}

	print STDOUT "\n$dashes\n" .
		"Running:  HTML_LocalSetup_XP.exe\t\tVersion:  $myVersion\n" .
		"$dashes\n\n";

print STDOUT "Parameters:\n";
print STDOUT "\$driveletter=$driveletter\n";
print STDOUT "\$GenCodePath=$GenCodePath\n";
print STDOUT "\$CWD=$CWD\n";
print STDOUT "\$CWDACCT=$CWDACCT\n";
print STDOUT "\$acctNo=$acctNo\n";
print STDOUT "\$NoPause=$NoPause\n\n";
print STDOUT "SETUP FOLDER:  $HTML_LocalSetupFolder\n\n";
	
	## Process the files or quit?
	print(STDOUT "\n     continue?...\n" .
		"----------------------------------------------------------------\n" . 
		" Y = YES        --OR--        \(blank\) or N = CANCEL\n" . 
		"----------------------------------------------------------------\n");
	
	if($NoPause ne "nopause")	{	
		$promptUser = &ReadKey;
	} else {
		$promptUser = "Y";
	}

	if($promptUser=~ /[Yy]/) {
		# do nothing...
		print(STDOUT "\n");	#add a little space before processing the files
	} else {
		print(STDOUT "\n\...HTML_LocalSetup_XP.exe cancelled.\n");
		exit 0 ;
	}

}


#------------------------------------------------------------
sub LocalSetup_HTML{
#------------------------------------------------------------
	$CWDSAVED = $CWD;
	
	print STDOUT "\t--------------------------------------------------\n";
	print STDOUT "\t\tCopying Setup Files...\n";
	print STDOUT "\t--------------------------------------------------\n";
	print STDOUT "\tCWDSAVED=$CWDSAVED\n\n";
	print STDOUT "\tCWD=$CWD\n\n";
	
	$rtn=chdir("..");
	$CWD=cwd();
	print STDOUT "\tChanged to Parent DIR \$CWD=$CWD\n\n";
	$myHTML=$CWD . "\\HTML";
	print STDOUT "\t'COPY' Target Folder:  $myHTML\n\n";

	# Create (if necessary) 'HTML' folder...
	if(! -d "HTML")	{
		mkdir("HTML", 0777) or die "+++Can't create $CWD\\HTML\n$!";
		print STDOUT "\tCreated:  HTML\n";
	}
	
	if(-e "$HTML_LocalSetupFolder")	{
		$CWD=$HTML_LocalSetupFolder;
		print STDOUT "\tSetup Folder (Copy INPUT):  $CWD\n\n";
	} else {
		print STDOUT "\n$dashes\n\tERROR!  Setup Folder Missing:  $HTML_LocalSetupFolder\n\n";
		exit 0;
	}
	
	opendir( SETUPH, "$CWD") or die "Can't OPEN SETUPH --> $CWD";
	
	# Process Each File...
	while (defined(my $file = readdir SETUPH)) {
		next if (-d "$CWD\\$file" || $file eq "." || $file eq ".."); 
		
		if($file eq "index.html")	{
			if(! -e "$myHTML\\index.html") {
				$rtn=copy($CWD . "\\$file", $myHTML);
				print STDOUT "\t...$file\n";
				
				if(! $rtn)	{
					print STDOUT "copy \$rtn=$rtn\n\tERROR:  $!\n";	
				}
			} else {
				print STDOUT "\t...$file\t\t[ Customized file exists!  NOT UPDATED ]\n";	
			}	
		} else {
			$rtn=copy($CWD . "\\$file", $myHTML);

			print STDOUT "\t...$file\n";
			
			if(! $rtn)	{
				print STDOUT "copy \$rtn=$rtn\n\tERROR:  $!\n";	
			}
		}
	}
	
	my $CustomCSS = ("$CWD" . "\\CustomCSS" . "\\$acctNo" . "_MCC_style.css");
	
#	print STDOUT "\$CustomCSS is:\t$CustomCSS\n\n";
#	print STDOUT "\$CWD is:\t$CWD\n";
	
	if (-e $CustomCSS) {
			copy("$CustomCSS","$myHTML\\MCC_style.css") or die " CustomCSS copy failed: $!\n";
	} 
	
	closedir SETUPH;
	
	$CWD=$CWDSAVED;
}


#------------------------------------------------------------
sub LocalSetup_HTML_images {
#------------------------------------------------------------
	my $str1="";
	print STDOUT "\n\n\t--------------------------------------------------\n";
	print STDOUT "\t\tCopying Images...\n";
	print STDOUT "\t--------------------------------------------------\n";
	$CWDSAVED = $CWD;
#	print STDOUT "\tCWDSAVED=$CWDSAVED\n";
	
	$rtn=chdir("HTML");
	if(! $rtn)	{
		print STDOUT "copy \$rtn=$rtn\n\tERROR:  $!\n";	
	}
	
	$CWD=cwd();
	# Create (if necessary) 'images' folder...
	if(! -d "images")	{
		mkdir("images", 0777) or die "\n+++Can't create $CWD\\images\n$!\n";
		print STDOUT "\tCreated Folder:  images\n";
	}
	
	$rtn=chdir("images");
	if(! $rtn)	{
		print STDOUT "copy \$rtn=$rtn\n\tERROR:  $!\n";	
	}
	
	$CWD=cwd();
	
	$myHTMLimages=cwd();
	
	if(-e "$HTML_imagesSource")	{
		$CWD=$HTML_imagesSource;
		print STDOUT "\t...'copy'  INPUT Folder:  $CWD\n";
	} else {
		print STDOUT "\n$dashes\n\tERROR!  Images Folder NOT FOUND:  $HTML_imagesSource\n\n";
		exit 0;
	}
	
	$myHTMLimages =~ s|\/|\\|g;
	# Initialize OUTPUT folder...
	print STDOUT "\tINITIALIZING FOLDER:  $myHTMLimages...";
	opendir( INITOUT, "$myHTMLimages") or die "Can't OPEN INITOUT --> $myHTMLimages";
	# Process Each File...
	while (defined(my $file = readdir INITOUT)) {
		next if (-d $file || $file eq "." || $file eq ".."); 
		
		if(-e $file)	{
			unlink($file);
		}
	}
	closedir INITOUT;
	print STDOUT "DONE!\n\n";
		
	print STDOUT "\t('copy' OUTPUT Folder):  $myHTMLimages\n\n\tCopying:\n";

	opendir( SETUPHI, "$CWD") or die "Can't OPEN SETUPHI --> $CWD";
	
	# Process Each File...
	while (defined(my $file = readdir SETUPHI)) {
		next if (-d $file || $file eq "." || $file eq ".."); 
		
		if(	($file =~ /^(.+?)\.tif/i )		||
			($file =~ /^(.+?)\.jpg/i )	||
			($file =~ /^(.+?)\.png/i)	||
			($file =~ /^(.+?)\.eps/i )		)	{

			$str1 = lc($file);
			$str1 =~ s|^(.+?)\.(.+?)$|$2|g;		# get file type...
			$file =~ s|^(.+?)\.(.+?)$|$1\.$str1|g;		# rename value of $file...
				
			$rtn=copy("$CWD\\$file", "$myHTMLimages");
			print STDOUT "\t\t...$file\n";
				
			if(! $rtn)	{
				print STDOUT "copy \$rtn=$rtn\n\tERROR:  $!\n";	
			}
		} else {
			print STDOUT "\t(SKIPPING:  $file)\n";	
		}
	}
	
	closedir SETUPHI;
	
	LocalSetup_MCC_Logo_Files();
	$CWD=$CWDSAVED;
	LocalSetup_HTML_images_convert();	
}


#------------------------------------------------------------
sub LocalSetup_HTML_images_convert {
#------------------------------------------------------------
# NOTES:
#
#(gswin32c.exe) EPS parameters
#-dEPSCrop - Crop an EPS file to the bounding box. This is useful when converting an EPS file to a bitmap.
#
#-dEPSFitPage - Resize an EPS file to fit the page. This is useful for enlarging an EPS file to fit the paper size when printing.
#
#-dNOEPS - Prevent special processing of EPS files. This is useful when EPS files have incorrect Document Structuring Convention comments.


	my $me = whoami();
	my $framewidth = 768;	# 8" at 96px/inch
	my $str1="";
	print STDOUT "\n\n\t--------------------------------------------------\n";
	print STDOUT "\t\tConverting Images...\n";
	print STDOUT "\t--------------------------------------------------\n";
	my $ConvertStatus  = "";
	my $basename = "";
	my $rtn = "";
	my $file = "";
	
	$rtn=chdir("..");
	$CWD=cwd();
	
	$rtn=chdir("HTML");
	$CWD=cwd();	
	$rtn=chdir("images");
	$CWD=cwd();

	print STDOUT "\t('images' Folder):  $CWD\n";
	
	opendir( CIMG, "$CWD") or die "Can't OPEN CIMG --> $CWD";
	
	# Process Each File...
	while (defined($file = readdir CIMG)) {
		next if (-d $file || $file eq "." || $file eq ".."); 
		
		if(	($file =~ /^(.+?)\.tif/i )		||
			($file =~ /^(.+?)\.jpg/i)		||
			($file =~ /^(.+?)\.png/i)	||
			($file =~ /^(.+?)\.eps/i )		)	{

			$basename=$file;
			$basename =~ s|^(.+?)\.(...)$|$1|g;		# get file name without extension...

			#CONVERT FILE...
			#=====
			# TIF or JPG
			#=====
			if( ($file =~ /^(.+?)\.tif/i) || ($file =~ /^(.+?)\.jpg/i) )	{
				print STDOUT "\t$file\n\t\tPNG...";				

				$ConvertStatus =
					`"$driveletter\\APPS_3rdPARTY\\ImageMagick\\convert.exe" $file $basename.png 2>&1`;
										
				if( !-e "$basename.png") {
					print STDOUT "[ ERROR ]\n$dashes\n$ConvertStatus\n$dashes\n";	
				} else {
					print STDOUT "\n";
				}
					
			#=====
			# EPS
			#=====			
			}elsif($file =~ /^(.+?)\.eps/i)	{
				print STDOUT "\t$file\n\t\tPNG...";
				$ConvertStatus =
					#`$driveletter\\APPS_3rdPARTY\\gs\\gs8.64\\bin\\gswin32c.exe -dBATCH -sDEVICE=pdfwrite -dNOSUBSTDEVICECOLORS -dNOPAUSE -dEPSCrop -sOutputFile=$basename.pdf $file 2>&1`;

					# Try DIRECTLY to PNG (NOTE:  r300x300 actually looks better than r600x600)...
# r300x300
					`$driveletter\\APPS_3rdPARTY\\gs\\gs8.64\\bin\\gswin32c.exe -dEPSCrop -dNOSAFER -dBATCH -dNOPAUSE -sDEVICE=png16m -r300x300 -sOutputFile=$basename.png $file 2>&1`;
# r600x600
#					`$driveletter\\APPS_3rdPARTY\\gs\\gs8.64\\bin\\gswin32c.exe -dEPSCrop -dNOSAFER -dBATCH -dNOPAUSE -sDEVICE=png16m -r600x600 -sOutputFile=$basename.png $file 2>&1`;
#print STDOUT "\n[DEBUG] CHECK ON THE NEWLY CREATED PNG FROM EPS INPUT!\n\n";
#$promptUser = &ReadKey;					
					#ps2pdf -sPAPERSIZE#a4 file.ps file.pdf  (#Problem:  rotates PDF, 'cause it's a print format)
					#`$driveletter\\APPS_3rdPARTY\\gs\\gs8.64\\lib\\ps2pdf $basename.eps $basename.pdf 2>&1`;  
					
					#C:\work\misc>gswin32c.exe -o "%1.pdf" -dNOSAFER -sDEVICE=pdfwrite -r720 dCompatibilityLevel=1.5 -dUseFlateCompression=true -dMaxSubsetPct=100 -dSubsetFonts=true -dEmbedAllFonts=true -dNumRenderingThreads=2 -c "60000000 setvmthreshold" -f -dEPSCrop "%1.ps"
					#`$driveletter\\APPS_3rdPARTY\\gs\\gs8.64\\bin\\gswin32c.exe -dNOSAFER -sDEVICE=pdfwrite -r600 dCompatibilityLevel=1.5 $basename.eps -o $basename.pdf 2>&1`;					

					# C:\work\misc>ps2pdf -dNOSAFER -r720 -dCompatibilityLevel#1.5 -dUseFlateCompression#true -dMaxSubsetPct#100 -dSubsetFonts#true -dEmbedAllFonts#true -dEPSCrop "defense.ps" "defense.pdf"
					# STILL ROTATES: `$driveletter\\APPS_3rdPARTY\\gs\\gs8.64\\lib\\ps2pdf -dNOSAFER -r600x600 $basename.eps $basename.pdf 2>&1`;
					
					
#				if( !-e "$basename.pdf") {
#					print STDOUT "\n[ ERROR ]\n$dashes\n$ConvertStatus\n$dashes\n\n";	
#				} else {
#					# PDF -> JPG...
#					print STDOUT "\tJPG...";
#					$ConvertStatus =
#						`$driveletter\\APPS_3rdPARTY\\gs\\gs8.64\\bin\\gswin32c.exe -dBATCH -sDEVICE=jpeg -dNOPAUSE  -r600x600 -sOutputFile=$basename.jpg $basename.pdf 2>&1`;
#	
#					if(!-e "$basename.jpg") {
#						print STDOUT "\n[ ERROR ]\n$dashes\n$ConvertStatus\n$dashes\n\n";	
#					} else {
#						# JPG -> PNG...
#						print STDOUT "\tPNG...";
#						$ConvertStatus =
#							`"$driveletter\\APPS_3rdPARTY\\ImageMagick\\convert.exe" $basename.jpg $basename.png 2>&1`;
#		
#						if( !-e "$basename.png") {
#							print STDOUT "\n[ ERROR ]\n$dashes\n$ConvertStatus\n$dashes\n\n";	
#						} else {
#							print STDOUT "\n";
#							unlink "$file";
#							unlink "$basename.pdf";
#							unlink "$basename.jpg";	
#						}
#					}
#				}

#				if( !-e "$basename.pdf") {
#					print STDOUT "\n[ ERROR ]\n$dashes\n$ConvertStatus\n$dashes\n\n";	
#				} else {
#					# PDF -> PNG...
#					print STDOUT "\tPNG...";
#					$ConvertStatus =
#						`$driveletter\\APPS_3rdPARTY\\gs\\gs8.64\\bin\\gswin32c.exe -dBATCH -sDEVICE=png16m  -dNOPAUSE-r600x600 -sOutputFile=$basename.png  $basename.pdf  2>&1`;

					if(!-e "$basename.png") {
						print STDOUT "\n[ ERROR ]\n$dashes\n$ConvertStatus\n$dashes\n\n";	
					} else {
						print STDOUT "\n";
					}					
			}

		} else {
			print STDOUT "\t (SKIP)........$file\n";	
		}
					
		if(-e "$basename.png") {			
			if(($NoPause ne "nopause")	&& ($mydebug ne "") ){	
				print STDOUT "\n\n\tDELETE the $basename.[eps, jpg, pdf, or tif] file\(s\)?\n";
				print STDOUT "\t([Y] = DELETE;    any other key does NOT DELETE the files.)\n\n";
				$promptUser = &ReadKey;
			} else {
				$promptUser = "Y";		# Defaults to DELETE if using 'nopause' processing
			}
		
			if($promptUser=~ /[Yy]/) {						
				unlink "$basename.eps";
				unlink "$basename.jpg";	
				unlink "$basename.pdf";
				unlink "$basename.tif";	
			}
		}		
	}
	
	closedir CIMG;
	
	$CWD=$CWDSAVED;
}


#------------------------------------------------------------
sub LocalSetup_MCC_Logo_Files	{
#------------------------------------------------------------
	# Copy any MCC Logo Files to $HTML_imagesSource...
	my $me = whoami();
	my $GCfile="";
	my $content="";
	my @imageRefs = ();
	my @tempArray = ();
	
	my $path="";
	my $ext="";
	my $rtn=-1;
	my $copyFrom="";
	my $copyTo="";
	
	if(-e "$GenCodeInputSource")	{
		print STDOUT "\n$dashes\n\tLooking for MCC Logo file references...\n";
		$CWD=$GenCodeInputSource;

		#Open Directory and process every file name...
		opendir(GCDIR, $GenCodeInputSource) or die "Can't opendir $GenCodeInputSource: $!\n";
		
		while(defined($GCfile = readdir(GCDIR)))	{
			next if (-d "$GenCodeInputSource\\$GCfile" || $GCfile eq "." || $GCfile eq ".."); 
			next if( not($GCfile =~ /.*[^\.]/));	#NOT a GenCode file (no extension)
			
			$path = $GenCodeInputSource . "\\" .$GCfile;
			
			if( -d $path) { 
				next;
			}
			
			$ext = extension($path);
			
			next if(	($ext ne "") and ($ext ne "mcc")	);	# skip if not without extension AND not ending .mcc...
			
			print STDOUT "\t...$path\n";
			
			open(GCIN, '<:encoding(utf8)', $path)
				|| die "cannot open input file: $path";
				
			while(<GCIN>)	{
				my($line) = $_;
				chomp($line);			# Strip the trailing newline from the line.

				if($line =~ /Municode_Title_Page_Logo/) {

					if($line =~ /Municode_Title_Page_Logo(.+?)(\.png);/i )	{
						$line =~ s|(.+?)(Municode_Title_Page_Logo)(.+?)(\.png);(.*)|$2$3$4|ig;
						@tempArray = grep (/$line/i, @imageRefs);
						if($#tempArray < 0) { push @imageRefs, $line; }
	
					} elsif($line =~ /Municode_Title_Page_Logo(.+?)(\.jpg);/i)	{
						$line =~ s|(.+?)(Municode_Title_Page_Logo)(.+?)(\.jpg);(.*)|$2$3$4|ig;
						@tempArray = grep (/$line/i, @imageRefs);
						if($#tempArray < 0) { push @imageRefs, $line; }
						
					} elsif($line =~ /Municode_Title_Page_Logo(.+?)(\.tif);/i)	{
						$line =~ s|(.+?)(Municode_Title_Page_Logo)(.+?)(\.tif);(.*)|$2$3$4|ig;
						@tempArray = grep (/$line/i, @imageRefs);
						if($#tempArray < 0) { push @imageRefs, $line; }
						
					} elsif($line =~ /Municode_Title_Page_Logo(.+?)(\.eps);/i )	{
						$line =~ s|(.+?)(Municode_Title_Page_Logo)(.+?)(\.eps);(.*)|$2$3$4|ig;
						@tempArray = grep (/$line/i, @imageRefs);
						if($#tempArray < 0) { push @imageRefs, $line; }
					}
				}

			}

			close(GCIN);
		}
		
		closedir(GCDIR);
	}	
	
	if($#imageRefs > -1)	{
				
		for(my $i = 0; $i <= $#imageRefs; $i++)	{
			$copyFrom="$MCC_imagesSource\\$imageRefs[$i]";
			#$copyTo="$CWD\\HTML\\images";
			$copyTo=$myHTMLimages;	#(2011-09-06)
			print STDOUT "\t\t...COPYING FROM:  $copyFrom  to  $copyTo\n";

			$rtn = copy($copyFrom, $copyTo);
			
			if($rtn < 1)	{
				print STDOUT "\n+++ ERROR!  UNABLE TO COPY:\n" .
					"FROM:  $copyFrom\tto...\n" .
					"TO:  $copyTo\n\n";
			};
		}
	}
}


#------------------------------------------------------------
sub LocalSetup_PDFs	{
#------------------------------------------------------------
	# MOVE all "accounts"\*.pdf files (G-Accounts\<account> and ~\images) to G-Accounts\<account>\Ancillary\ and
	# COPY "Ancillary" folder to R-Accounts\<account>\HTML\
	my $me = whoami();
	
	my $kountPDFs = 0;
	my $PDFfile = "";
	my $rtn = "";
	
	if(-e "$GenCodeInputSource")	{
		print STDOUT "\n$dashes\n\tLooking for PDF files...\n";
		
		#Open Directory and process every file name...
		opendir(GCDIR, $GenCodeInputSource) or die "Can't opendir $GenCodeInputSource: $!\n";
		
		while(defined($PDFfile = readdir(GCDIR)))	{
			next if (-d "$GenCodeInputSource\\$PDFfile" || $PDFfile eq "." || $PDFfile eq ".."); 
			next if( not($PDFfile =~ /.+?[\.]pdf/));	#NOT a PDF file
			
			if(!-d "$GenCodeInputSource\\Ancillary") {
				$rtn = mkdir "$GenCodeInputSource\\Ancillary";
				
				if($rtn)	{
					print STDOUT "\tCreating: $GenCodeInputSource\\Ancillary...\n";
				} else {
					print STDOUT "\t+++\n\tERROR!\n\t+++\n\tCould NOT create folder:  $GenCodeInputSource\\Ancillary\n\n\$rtn=$rtn\n\n";
				}				
			}
			
			if(-d "$GenCodeInputSource\\Ancillary") {
				move("$GenCodeInputSource\\$PDFfile","$GenCodeInputSource\\Ancillary\\$PDFfile"); 
				$kountPDFs++;
				print STDOUT "\tMoved:\t$GenCodeInputSource\\$PDFfile \n\t\tto  $GenCodeInputSource\\Ancillary\\$PDFfile\n";				
			} else {
				print STDOUT "\n\n\n\tTHERE IS SOMETHING STINKY IN DENMARK!\n\n\taka:  HTML_LocalSetup_XP[LocalSetup_PDFs]\n\n\n";
			}
		}

		closedir(GCDIR);
		
		if($kountPDFs > 0) {
			print STDOUT "\n\tTOTAL PDFs moved to $GenCodeInputSource\\Ancillary\\:  $kountPDFs\n\n";
			dircopy("$GenCodeInputSource\\Ancillary", "$CWDACCT\\HTML\\Ancillary") or die("$!\n");
			print STDOUT "\tCopied:\t$GenCodeInputSource\\Ancillary \n\t\tto  $CWDACCT\\HTML\\Ancillary\n\n";
		} else {
			print STDOUT "\n\tNo PDF files were found in $GenCodeInputSource\n\n";	
		}
	}
}

	

#================================================
#	Utility Subroutines...
#================================================
#-------------------------------------------------------------
sub do_Error {
#-------------------------------------------------------------
	my ($mySub, $myMessage) = @_;
	
	print STDOUT "ERROR:  [$mySub]\n\t$myMessage\n";
	
	$promptUser = &ReadKey;
}


#-------------------------------------------------------------
sub extension {
#-------------------------------------------------------------
	my $path = shift; 
	my $ext = (fileparse($path,'\..*'))[2]; 
	$ext =~ s/^\.//;
	return $ext;
}


#------------------------------------------------------------
sub whoami {
#------------------------------------------------------------
	(caller(1))[3];
}
