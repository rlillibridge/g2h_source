#!/perl/bin
#CompareImages.pl
#

use Image::Compare;
use warnings;
use strict;

#Get parms...
my $imageA = shift;
my $imageB = shift;


#============================
# MAIN
#============================
	Housekeeping();
	ValidateInput();
	my $rtn = CompareImages();




#==========================================================
# SUBROUTINES
#==========================================================
#----------------------------------------------------------
sub Housekeeping {
#----------------------------------------------------------
	
}

#----------------------------------------------------------
sub ValidateInput {
#----------------------------------------------------------
	if($imageA == "")	{
		die "First image name missing.\n$!\n";
	}
	
	if($imageB == "")	{
		die "Second image name missing.\n$!\n";
	}
	
	if(! -e "$imageA")	{
		die "ERROR! First input image does NOT EXIST.\n";
	}
	
	if(! -e "$imageB")	{
		die "ERROR! First input image does NOT EXIST.\n";
	}
}


#----------------------------------------------------------
sub CompareImages {
#----------------------------------------------------------
	 my($cmp) = Image::Compare->new();
	 
	 $cmp->set_image1(
	     img  => "$imageA",
	     type => 'jpg',
	 );
	 
	 $cmp->set_image2(
	     img  => "$imageB",
	     type => 'jpg',
	 );
	 
	 $cmp->set_method(
	     method => &Image::Compare::EXACT
	 );
	 
	 if ($cmp->compare()) {
	     # The images are the same...
	     print STDOUT "$imageA IS THE SAME as $imageB\n\n";
	     return "SAME";
	 }
	 else {
	     # The images differ
	     print STDOUT "$imageA is NOT THE SAME as $imageB\n\n";
	     return "DIFFERENT";
	 }
}