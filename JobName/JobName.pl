#!/usr/bin/perl
# JobName.pl
# INPUT:  EIJobID
# OUTPUT:  tempSpeedJobNames->jobName
#
# Programmer:  Raymond Lillibridge
my $myVersion = "Version:  2011-03-24.1 (WIP)";

# CHANGE HISTORY:


#==========================================================
# PRAGMA
#==========================================================
use warnings;
use DBI;
use ENV qw(EIJOBNAME);
use Term::InKey;
use Data::Dumper;
use Tie::File;

use Getopt::Long;
my $opt_eijob="-empty-";	# --eijob		EIJobID from bottom of an account in the Inventory Report

GetOptions(
	"eijob=s" 		=>	\$opt_eijob,
	);

my $eijob="$opt_eijob";

# SQL INFO
my $dsn=q/dbi:ODBC:BIS/;	#system DSN
my $user=q/sa/;
my $pwd=q/concasturbate/;
my $dbh;	#database handle
my $sth;	#statement handle
my $query="";

# RECORD FIELDS RETURNED:
my $EIJobID="";
my $JobID="";
my $EIJobName="";
my $AcctNO="";
my $BannerText="";


#==========================================================
# GLOBALS
#==========================================================
# Date and Time vars
my($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime;
$year += 1900;
my $now_string = localtime;
my $nowYYYYMMDD_HHMMSS= dateTime();
my $LogDateTime =	$nowYYYYMMDD_HHMMSS->{'dateNr'} .  "_" . 
					$nowYYYYMMDD_HHMMSS->{'HHMMSS'};
my $myDT=$LogDateTime;
$myDT =~ s|(\d\d\d\d)(\d\d)(\d\d)_(\d\d)(\d\d)(\d\d)|$1\/$2\/$3 $4:$5:$6|g;

my $dashes="------------------------------------------------------------";


#==========================================================
# MAIN MAIN MAIN MAIN MAIN MAIN MAIN 
#==========================================================

&Housekeeping();

if($opt_eijob eq "-empty-")	{
	printOut("\n\n$dashes\n\nERROR!!!\n" .
		"EIJobID entered:  \"$opt_eijob\" IS NOT going to be a valid number, eh?\n\n$dashes\n\n");	
	exit (1);
}

&FetchBIS;	

print STDOUT "$dashes\n\n";
print STDOUT "PATH=$ENV{'PATH'}\n";
print STDOUT "$dashes\n\n";
print STDOUT "EIJOBNAME=$ENV{'EIJOBNAME'}\n";

## Process the file or quit?
print STDOUT "\n     continue?...\n$dashes\nY = YES                 --OR--     (blank\) or (!Y) = CANCEL\n\n";

$x = &ReadKey;

if($x=~ /[Yy]/) {
	# do nothing...
	print STDOUT "\n";	#add a little space before processing the files
} else {
	print STDOUT "\n\...$0 Processing cancelled.\n";
	exit 0 ;
}





print STDOUT "$0 FINISHED!\n\n";

1;
#==========================================================
#	MAIN END
#==========================================================


#==========================================================
# SUBROUTINES
#==========================================================
#-------------------------------------------------------------
sub FetchBIS()	{
#-------------------------------------------------------------
	my $me = whoami();
	my $sqlErr;
	my $myrtn;
	
	$dbh->{ReadOnly} = 1;
	$dbh->{RaiseError} = 1;

	#------------------------
	# Fetch ProductID, DefaultExpandLevel, ProductName, Client, State, & State-abbreviation...
	#------------------------				
	$query=
#		"SELECT P.ProductID, P.DefaultExpandLevel, N.ProductName, C.Client, S.State, S.Abbreviation\n" .
#		"FROM BIS_PRODUCTS AS P, BIS_PRODUCTNAMES AS N, BIS_CLIENTS AS C, _STATES AS S\n" .
#		"WHERE P.ProductID = '" . $opt_job . "' AND\n" . 
#		"P.ProductNameID = N.ProductNameID AND\n" .
#		"P.ClientID = C.ClientID AND C.StateID = S.StateID";
		
		"SELECT  E.EIJobID, T.jobId, T.jobName, J.ProductID AS AcctNO, J.bannerText\n" .
		"FROM     dbo.tempSpeedJobNames T, dbo.BIS_EIJOBS E, dbo.BIS_JOBS J\n" . 
		"WHERE  T.jobId = E.ParentJobID AND T.jobId = J.JobID AND (E.EIJobID = " . $opt_eijob .")";

		
	$sth = $dbh->prepare($query)
	 	or die "$DBI::errstr\n\nCan't prepare query: $query\n$!";
	 	
	$sqlErr = $sth->execute
	 	or die "$DBI::errstr\n\nCan't execute query:\n$query\n\n$!\n";
	
	#Bind the results to local variables...
	$sth->bind_columns(undef, \$EIJobID, \$JobID, \$EIJobName, \$AcctNO, \$BannerText)
		or die "$DBI::errstr\n\nCan't bind_columns\n$!\n";
	
	#retrieve values from the result set...
	$str1 =	$sth->fetch();
	
	if($str1)	{	
		print STDOUT "\n\n\nSQL Query Results:\n" .
					"$dashes\n" .
					"$EIJobID\n" . 
					"$JobID\n" .
					"$EIJobName\n" .
					"$AcctNO\n" .
					"$BannerText\n" .
					"$dashes\n\n\n";
	}
	
	#Close the connection
	$sth->finish();

	$ENV{'EIJOBNAME'} ="$EIJobName";
	print STDOUT "\$EIJobName=$EIJobName\n";
	print STDOUT "(ENV) EIJOBNAME=$ENV{'EIJOBNAME'}\n";
	#return $EIJobName;
}


#-------------------------------------------------------------
sub dateTime {
#-------------------------------------------------------------
	my $me = whoami();
	my ($sec,$min,$hour,$mday,$monNr,$year);
	my $now = {};
	#get date and time
	($sec,$min,$hour,$mday,$monNr,$year) = localtime(time());
    	my ($date, $date_nr, $clock);
	my $month=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")[$monNr];
    	$monNr++;
	$monNr = sprintf("%02d",$monNr);
	$mdayNr = sprintf("%02d",$mday);
	$sec = sprintf("%02d",$sec);
	$min = sprintf("%02d",$min);
	$hour = sprintf("%02d",$hour);
	$year= 1900 + $year;
	$now->{'date'} = "$year-$month-$mday";
	$now->{'dateNr'} = "$year$monNr$mdayNr";
	$now->{'time'} = "$hour:$min:$sec";
	$now->{'HHMMSS'} = "$hour$min$sec";
	return($now);		
}


#-------------------------------------------------------------
sub DisplayParms()	{
#-------------------------------------------------------------
	my $me=whoami();
	
	&Clear;
	print STDOUT "\nRunning JobName.exe with the following parameters:\n";
	print STDOUT "\$opt_eijob=$opt_eijob\n";
	print STDOUT "\n(Press any key to continue...\n\n";

	$x = &ReadKey;
}
	

#-------------------------------------------------------------
sub Housekeeping	{
#-------------------------------------------------------------
#	DisplayParms();
	
	print STDOUT "\nRUNNING:  $0\t\t$myVersion\n";
		
	if($opt_eijob eq "-empty-")	{
		print STDOUT "\n\n...uh, how about entering an EIJobID Number for me, eh?\n\n";
		print STDOUT "\tExample:  C:\\\>JobName --eijob=123456\n\n";
		exit(1);	
	}		
	
	# Connect to the data source and get a handle for that connection.
		$dbh = DBI->connect($dsn, $user, $pwd)  
			or die "Can't connect to $dsn: $DBI::errstr\n$!";
}


#-------------------------------------------------------------
sub whoami {
#-------------------------------------------------------------
	(caller(1))[3];
}
