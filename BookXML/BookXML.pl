#!/usr/bin/perl
# BookXML.pl
# Reads all files in current folder and creates a single Book.xml file containing entity
# references to the XML files that comprise the book.
#
# Programmer:  Raymond Lillibridge
my $myVersion = "Version:  2010-12-02.1 (STABLE)";

################################################################ 
# Change History:
# 2010-12-02.1 - Raymond Lillibridge - Allow "-nopause" processing

################################################################ 
#    IMPORTANT NOTES:  
################################################################ 
#  SAMPLE OUTPUT:
#
#	Book.xml  (containing the following:)
#		<?xml version="1.0" encoding="UTF-8"?>
#		<!DOCTYPE book [
#		<!ENTITY aprelims SYSTEM "APRELIMS.xml">
#		<!ENTITY ch000 SYSTEM "ch000.xml">
#		<!ENTITY ch001 SYSTEM "ch001.xml">
#		<!ENTITY ch002 SYSTEM "ch002.xml">
#		<!ENTITY ch003 SYSTEM "ch003.xml">
#		...
#		]>
#		<book>
#		&aprelims;
#		&ch000;
#		&ch001;
#		&ch002;
#		&ch003;
#		...
#		</book>



#=============================================	
# PRAGMA
#=============================================	
use warnings;
no warnings 'uninitialized';
use Term::InKey;
use File::Basename;
use XML::Simple qw(:strict);
my $XMLPubMapdriver;
use Data::Dumper;
use Tie::File;

#=============================================	
# GLOBALS
#=============================================
my $NoPause=shift;
if(!defined $NoPause)	{
	$NoPause="";	
}

my $BookXML = "Book.xml";
my $ctr = 0;
my $tot = 0;
my $gtot = 0;
my $str1 = "";
my $str2 = "";
my $Int1 = 0;
my $Int2 = 0;
my $index = 0;
my $go;
my $line = "";
my $lineTemp = "";

#=============================================	
# HOUSEKEEPING
#=============================================	
if($NoPause ne "-nopause") {
	&Clear;
}

print STDOUT "\nRUNNING:  BookXML.exe (pl)\t\t$myVersion\n";

$ctr = 0;

# DELETE EXISTING $BookXML file...
print STDOUT "\n     DELETE EXISTING $BookXML file?...\n\n----------------------------------------------------------------\n" . 
	" Y = YES                 --OR--     (blank\) or (!Y) = CANCEL\n" . 
	"\n----------------------------------------------------------------\n";

if($NoPause ne "-nopause")	{	
	$x = &ReadKey;
} else {
	$x = "Y";
}

if($x=~ /[Yy]/) {
	# DELETE EXISTING $BookXML file...
	$ctr = 0;
	
	if(-e $BookXML)	{	
		$ctr = unlink($BookXML);
		if($ctr > 0) { print STDOUT "\n...DELETED $BookXML\n"; }
	} else {
		print STDOUT "\n...$BookXML NOT FOUND.\n";
	}
	
} else {
	print STDOUT "\n\...BookXML.exe (pl) processing cancelled.\n";
	exit 0 ;
}

## Make empty argument list default to all xml files...
@ARGV = glob("*.xml") unless @ARGV;

if($#ARGV == -1) {
	print STDOUT "------------------------------\n\t XML INPUT FILE(S) NOT FOUND!\n------------------------------\n";
	exit 0;	
}

print STDOUT "*------------------------------------------------------------------------------*\n\n";
print STDOUT "INPUT FILES:\n";

$ctr = 1;

foreach $infile (@ARGV) {
	if(	($infile ne "$BookXML") 	&
		($infile !~ /^_/) 
		)	{
		printf STDOUT "%3s \t%s\n", $ctr, $infile;		#display to user what files are being processed
		$ctr++;
	}
}

## Process the files or quit?
print STDOUT "\n     continue?...\n----------------------------------------------------------------\n" . 
	" Y = YES                 --OR--     (blank\) or (!Y) = CANCEL\n" . 
	"\n----------------------------------------------------------------\n";
	
if($NoPause ne "-nopause")	{	
	$x = &ReadKey;
} else {
	$x = "Y";
}

if($x=~ /[Yy]/) {
	# do nothing...
	print STDOUT "\n";	#add a little space before processing the files
} else {
	print STDOUT "\n\...BookXMLProcessing cancelled.\n";
	exit 0 ;
}

#=============================================	
# HOUSKEEPING - END
#=============================================	


#=============================================	
# MAIN
#=============================================	
# At this point @ARGV should contain all of the files to process (and then some!).

# Create $BookXML file...
open OUT, ">:", $BookXML
	|| die "cannot open file for output:  $BookXML";
	
# XML Header...
print OUT "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";

# DOCTYPE...
print OUT "<!DOCTYPE book [\n";


# create ENTITY entries for each $infile...
foreach $infile (@ARGV) {
	if(	($infile ne "$BookXML") 	&
		($infile !~ /^_/) 
		)	{
		print OUT "<!ENTITY " . substr($infile, 0, -4) . " SYSTEM \"$infile\">\n";
	}
}

print OUT "\]>\n<book>\n";

# create references to entities...
foreach $infile (@ARGV) {
	if(	($infile ne "$BookXML") 	&
		($infile !~ /^_/) 
		)	{
		print OUT "\t\&" . substr($infile, 0, -4) . ";\n";
	}
}

print OUT "</book>\n";
close OUT;
print STDOUT "\n FILE:  $BookXML Created!\n";
print STDOUT "\nBookXML.exe (pl) FINISHED!\n";


1;