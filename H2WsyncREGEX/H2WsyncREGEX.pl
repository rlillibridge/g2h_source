#!/usr/bin/perl
# H2WsyncREGEX.pl
# Applies REGEX transformations to *.html files for HTML2WordDriver.exe (SP)
#
# Programmer:  Raymond Lillibridge
my $myVersion = "Version:  2016-03-29.1 (wip)";

# WIP:

# HISTORY:
# 2016-03-29.1 - Raymond Lillibridge - Convert inline <span/> tags to HTML4, since SharePoint doesn't handle HTML5
#	<span class="bold">BOLD TEXT</span> (ital, bdit, med, ul
# 2014-01-07.1 - Raymond Lillibridge - Added removal of MCC 'external' links from HTML
# 2013-01-30.1 - Raymond Lillibridge - Changed step (12 of 18) to simply remove the anchor since footnotes and title/subtitle are no longer nested.

#=============================================	
# PRAGMA
#=============================================	
use warnings;
no warnings 'once';
use Term::InKey;
use File::Basename;
use File::Copy;
use Data::Dumper;

#=============================================	
# GLOBALS
#=============================================
my $outfile = "";
my $output_buffer = "";
my $key;

#=============================================	
# HOUSEKEEPING
#=============================================	
$num_args = $#ARGV + 1;
if ($num_args != 1) {
  print STDOUT "\nH2WsyncREGEX.exe\tVersion:  $myVersion\nUsage: H2WsyncREGEX.exe(pl) full_path_to_input.html\n\n";
  print STDOUT "Example:  R:\\Accts\\10620\\level1\\CD_COMPARATIVE_TABLEOR.html\n\n";
  exit;
}

my $infile=$ARGV[0];
my($fname,$fpath) = fileparse($infile);
my $fsuffix = substr($fname, -5);

if(uc($fsuffix) ne ".HTML" || length($fname) < 6)
{
	print STDOUT "\nH2WsyncREGEX.exe\tVersion:  $myVersion\nUsage: H2WsyncREGEX.exe(pl) full_path_to_input.html\n\n";
	print STDOUT "Example:  R:\\Accts\\10620\\level1\\CD_COMPARATIVE_TABLEOR.html\n\n";
	print STDOUT "(+++ INPUT MUST END WITH '.html' +++)\n\n";
	print STDOUT "INPUT PATH:\t$fpath\n";
	print STDOUT "INPUT FILE:\t$fname\n";
	exit;
 }
#=============================================	
# HOUSKEEPING - END
#=============================================	


#=============================================	
# MAIN
#=============================================	
#print STDOUT "Reading $infile into a string...\n";
# Read $infile into a scalar...
open INFILE, "<:utf8", $infile or die "Couldn't open file: $!"; 

$filestr = "";
while (<INFILE>){
	$filestr .= $_;
}
close IN;	

$outfile = ($fpath . "\\" . $fname . ".tmp");

open(OUT, ">:utf8", $outfile)		# overwrite
	|| die "cannot open file for output:  $outfile";

my $tmpstr = regex($filestr);
print OUT "$tmpstr";
close OUT;

move($outfile, $infile)  or die(qq{failed to move $outfile -> $infile});;
exit;


#=============================================	
# SUBROUTINES
#=============================================	
#-------------------------------------------------------------
sub regex {
#-------------------------------------------------------------
	my $parm = shift;	#input string

	# Remove CSS reference (single dot)...
	$parm =~ s|<link type=\"text/css\" rel=\"stylesheet\" href=\"./MCC_style.css\">||g;
	
	# Remove CSS reference (double dot)...
	$parm =~ s|<link type=\"text/css\" rel=\"stylesheet\" href=\"../MCC_style.css\">||smg;
	
	# Remove XML IE comments...
	$parm =~ s|^\s<!--\[if IE\]>||smg;
	
	# IE continued...
	$parm =~ s|<link type=\'text/css\' rel\=\'stylesheet\' href\=\'../MCC_style_IE.css\'></link>||smg;
	
	# IE continued...
	$parm =~ s|^\s<!\[endif\]-->||smg;
	
	# Remove XML comments...
	$parm =~ s|<!--(.+?)-->||smg;
	
	# Add newline before </div>...
	$parm =~ s|(</div>)|\n$1\n|smg;
	
	# Remove <script src=" ... </script> 
	$parm =~ s|<script src=\"(.+?)</script>||smg;
	
	# Remove <script type="text/ ... </script>
	$parm =~ s|<script type=\"text/(.+?)</script>||smg;
	
	# Remove <link rel="stylesheet" href=" ... />
	$parm =~ s|<link rel=\"stylesheet\" href=\"(.+?)\/>||smg;
	
	# INSERT \\n\t\t before:  <a class="crumb" ...
	$parm =~ s|(<a class=\"crumb\")|\n\t\t$1|smg;
	
	# Remove:  <a class="showURLs toplink" id="TOPTITLE" href="javascript:void(0)" ></a>...
	$parm =~ s|<a class=\"showURLs(.+?)<\/a>||smg;
	
	# INSERT \r\n before:  <p ...
	$parm =~ s|(<p class=\")|\n\t\t$1|smg;
	
	# <img id="img_1" src="../images/21-29-2.jpg" alt="21-29-2.jpg" width="75%">
	$parm =~ s|<img id=\"(.+?)\" src=\"(.+?)/images/(.+?)\" alt=(.+?)>|<img src=\"$2/images/$1^$3\" alt=$4 />|smg;
		
	$parm =~ s|<a href=\"(.+?).pdf\"|<a href=\"$1.jpg\"|smg;
	
	$parm =~ s|<img id=\"(.+?)\" src=\"../images/_TN_| src=\"../images/_TN_$1\_|smg;
	
	$parm =~ s|src=\"../images/_TN_(.+?) alt=|src=\"../images/$1 alt=|smg;
	
	$parm =~ s| alt=\"(.+?).pdf\"></a>| alt=\"(.+?).jpg\"></a>|smg;
	
	# Remove MCC web 'external' links...
	# Sample:
	#<a href="../level2/VOI_TIT1GEPR.html">Title 1 - GENERAL PROVISIONS</a>
	$parm =~ s|<a href=\"./level\d/(.+?)>(.+?)<\/a>|$2|smg;	
	$parm =~ s|<a href=\"../level\d/(.+?)>(.+?)<\/a>|$2|smg;
	
	$parm = span_to_HTML4($parm);
	
	return $parm;
}


sub span_to_HTML4 {
	#2016-03-28 - Convert inline <span/> elements back to HTML-4 tags, since SharePoint doesn't support HTML5.
	# bold, bdit, italic, & ul

	my $line = shift;
	
	$line =~ s|<span class=\"bold\">(.+?)<\/span>|<b>$1<\/b>|smg;	
	$line =~ s|<span class=\"bdit\">(.+?)<\/span>|<b><i>$1<\/i><\/b>|smg;	
	$line =~ s|<span class=\"ital\">(.+?)<\/span>|<i>$1<\/i>|smg;	
	$line =~ s|<span class=\"med\">(.+?)<\/span>|<span>$1<\/span>|smg;	
	$line =~ s|<span class=\"ul\">(.+?)<\/span>|<u>$1<\/u>|smg;		
	$line =~ s|<span class=\"ulsingle\">(.+?)<\/span>|<u>$1<\/u>|smg;		
	$line =~ s|<span class=\"ulstrike\">(.+?)<\/span>|<s>$1<\/s>|smg;		
	$line =~ s|<span class=\"uloverrule\">(.+?)<\/span>|<span style=\"text-decoration: overline;\">$1<\/span>|smg;			
		
	return $line;
}


#-------------------------------------------------------------
sub whoami {
#-------------------------------------------------------------
	(caller(1))[3];
}
