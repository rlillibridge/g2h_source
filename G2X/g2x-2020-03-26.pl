#!/usr/bin/perl
# g2x.pl
# GenCode to XML (schema:  code.xsd) Converter

my $myVersion = "Version:  2020-03-25.1";

# WIP:
# 2020-03-03.1 - Raymond Lillibridge - <rowbg;_named-color> and <rowheight;_integer_px> or without the 'px'
# 2020-03-02.1 - Raymond Lillibridge - Added: 
	# <tabrules>|<?xpp tabrules;on?>|g;
	# <tabrules;(.+?)>|<?xpp tabrules;$1?>|g;
# 2020-02-27.1 - Raymond Lillibridge - table cell ending inline quad to become cells @style="align-text:*" value
# 2020-02-24.1 - Raymond Lillibridge - <setall;jstyle;*> (l or rr)
# 2020-02-20.1 - Raymond Lillibridge - <tindent;*;b0..9> to properl <?xpp tindent;b0..9?>
# 2020-02-17.1 - Raymond Lillibridge - <com>pi-height;*</com> to <?xpp pi-height;*?>

# HISTORY:
# 2020-03-25.1 - Andrea Gill -		   Changed the <setgtext> rules to prevent entire oh5 tags with <setgtext> at the start and end of the tag are not deleted.
# 2020-03-16.1 - Andrea Gill -		   Added <sup> tag removal to Titl_2_alt	
# 2020-01-30.1 - Raymond Lillibridge - make sure @selectout="" occurs AFTER @id=
# 2020-01-17.1 - Andrea Gill - 		   Moved LL_header fix higher up on list; was causing errors at times.
# 2019-12-23.1 - Andrea Gill - 		   Added conversion for & reg; symbol (previously only had all caps version.)
# 2019-12-03.1 - Andrea Gill -		   Added conversion for &gdot; symbol
# 2019-08-20.1 - Andrea Gill - 		   Amended Capt_2_Para to allow for ulinks
# 2019-08-19.1 - Andrea Gill - 		   @alt -> Added ital and bdit to tag removal
# 2019-08-13.1 - Andrea Gill - 		   @alt -> Added ulink removal
# 2019-08-06.3 - Raymond Lillibridge - @alt -> adding another pattern
# 2019-08-05.2 - Raymond Lillibridge - @alt -> convert special (non-ASCII) characters to utf-8
# 2019-08-05.1 - Raymond Lillibridge - img @alt -> convert " & " to " and "
# 2019-07-30.1 - Andrea Gill -		   Trim trailing spaces from end of line.
# 2019-07-26.1 - Andrea Gill - 		   Added code to fix oh5a footnote counting.
# 2019-07-25.1 - Andrea Gill -		   Added line to check for ;fl; and <mh;0> on different lines and combine into one line.
# 2019-07-24.2 - Andrea Gill -         IGs and IGTs can have customized alt tags using <alt> and </alt> directly following the ig/igt.
# 2019-07-24.1 - Andrea Gill -         Pickups now use Title tags as Alt tag default, with caption as second.
# 2019-04-24.1 - Raymond Lillibridge - Undo Notification of "invalid" image files names
# 2019-04-22.1 - Raymond Lillibridge - Notification of "invalid" image files names. 
# 2019-02-26.1 - Raymond Lillibridge - sfboxt - allow for 'named colors' in second parameter
# 2019-02-20.1 - Raymond Lillibridge - Milpitas, CA ;hg; inside listitems problem on-line
# 2019-02-01.1 - Raymond Lillibridge - <ig;...> being converted to comments during g2h processing
# 2019-01-28.1 - Raymond Lillibridge - 'rules' changed to newly specified widths
# 2019-01-14.1 - Raymond Lillibridge - fix/monetaryblock's to have 
#	1) @monetary="yes"
#	2) wrap content after inline "\" with <monetary>...</monetary> tags
# 2018-09-27.1 - Raymond Lillibridge - ;hl; with content that has an in-line ;hg; - ;hg#;
# 2018-08-31.1 - Raymond Lillibridge - fix conversion of <setwid;-#;#n>  (neg. means "for THIS column and all the rest")
# 2018-08-28.1 - Raymond Lillibridge - remove/process ;hg; inside reference notes
# 2018-08-27.1 - Raymond Lillibridge - remove ;hg; from table cells
# *********************************************************************
# 2018-08-09.1 - Raymond Lillibridge - new: @class="hg#-indent#" to be used whenever there is
#     a GenCode block that has an in-line ;hg; tag in it.
# EXAMPLE:  
# ;b0;
# ;adv=6q;Code adoption;l;;em;;hg;Government Code ;s; 50022.1 et seq.;l;
#
# -- OR --
#
# ;b0;
# <adv;6q><sz;8q>*****;em;;hg;Setbacks shall be a minimum of 50 feet from the normal high water 
#elevation contour on any adjacent natural surface. Especially if you really want it that way. 
#Even if you do not want it that way. So, go with it!
#
#
# (HTML):
# <p class="hg0-indent6">*****;em;Setbacks shall be a minimum of 50 feet from the normal high water 
#elevation contour on any adjacent natural surface. Especially if you really want it that way. 
#Even if you do not want it that way. So, go with it!</p>
#
# -- OR --
#
# <p class="hg0-indent6">*****;em;<br />Setbacks shall be a minimum of 50 feet from the normal high water 
#elevation contour on any adjacent natural surface. Especially if you really want it that way. 
#Even if you do not want it that way. So, go with it!</p>
#
# CSS (for sample above)[see: library-app.css]:
# p.hg0-indent6 {margin: 0.2rem 0rem 0rem 6rem; text-indent: -6rem; }
#
# NOTICE!!! - base.docx will need to be updated for each new CSS class (hg-indent#)
# *********************************************************************
# 2018-07-05__16.1 - Raymond Lillibridge - Insert a custom "new" class "hgem" for the following GenCode:
#	;b0;
#	;adv=6q;Code adoption;l;;em;;hg;Government Code ;s; 50022.1 et seq.;l;
#
#	LOGIC CHANGE:
# 	2018-0-16.1 - Since nested para/para/... is problematic in the XSLT portion of this implementation, I'm 
#		changing this application such that it creates a new GenCode tag and converts it as applicable.
#		The new tag (the content above will be converted to) is: ;hm#; (hang with a 1 em-space indent)
# 2017-07-05.1 - Raymond Lillibridge - Anchors to have @href="https://www.municode.com" rather than ""
# 2017-05-25.1 - Raymond Lillibridge - Harrisonburg, VA #10893 (Andrea: 2017-05-25) - "g2h fractions online"
#	New Fraction entities ADDED: 1/8, 3/8, 5/8, and 7/8.
# 2017-04-04.1 - Raymond Lillibridge - Simplify 'listheader' by prepending to following sibling content instead, wrapped in 'bold' tags
# 2017-03-23.1 - Raymond Lillibridge - <setgtext> and <pregtext> - manage multiple single-line or in-line patterns
# 2017-03-15.1 - Raymond Lillibridge - Re: <setgtext*> and <pregtext*> - Allow for empty parameters.
# 2017-03-14.1 - Raymond Lillibridge - GenCode "rules" have changed for <setgtext*> and <pregtext*> tagging. 
# Now it does NOT occur on a line by itself. Now we need to allow for this to be an in-line tag.
# 2017-02-24.1 - Raymond Lillibridge - Add conversion for <ulink class="section-link".../>
# 2016-09-30.1 - Raymond Lillibridge - Correct creation of pickup (with caption) inside a listitem, so that the <p 'caption'/> contains the following <img/>
# 2016-09-21.1 - Raymond Lillibridge - ;oh5a; (<subsect1/>) not getting proper closing tag, under certain conditions.
# 2016-09-07.2 - Raymond Lillibridge - Convert <tblwidth>, <tblwidth;(.+?)> to PIs.
# 2016-09-07.1 - Raymond Lillibridge - Convert <tblwidth>, <tblwidth;(.+?)> to PIs.
# 2016-08-25.1 - Raymond Lillibridge - "ss" pickup comment div wrapper not working?
# 2016-08-09__11.1 - Raymond Lillibridge - <vstyle/> - table cells vertical position: top, middle, or bottom
#			Also, handle <setall;vstyle;*>  update each cell: <entry><?xpp vstyle;*?>
#			Only if the cell does not already have a <?xpp vstyle;*?> entry
# 2016-08-05.1 - Raymond Lillibridge - Wrap pickups that have "ss", inside a <div style="float: left|center|right">
# 2016-06-06.2 - Raymond Lillibridge - Non ;oh5*; -> ;oh#;  (ex. ;oh3x; -> ;oh3;)
# 2016-06-06.1 - Raymond Lillibridge - FIX ;oh3x; NOT Being converted...
# 2016-05-18_24.1 - Raymond Lillibridge 	- Manage ;oh5, variants as:
#			;oh5a,	- indent as a paragraph and bold Title content, subtitle runs in (no new line)
#			;oh5b; 	- no indent as ;b0; and bold Title content, subtitle runs in (no newline)
#			;oh5l;	- no indent as ;b0; and no bold Title content, subtitle runs in (no newline)
#			;oh5z;	- no indent as ;hg0; and no bold Title content, subtitle runs in (no newline)
# 2016-05-18.1 - Raymond Lillibridge - Allow ;rn0;, that are outside of footnote tags, to keep initial <sup/> tags 
# 2016-04-07.1 - Raymond Lillibridge - ADD: ;#rule; to behave as <#rule>
# 2016-03-09.1 - Raymond Lillibridge - ADD: entities: bullW, circleLgW, checkbox, and asterisk
# 2016-03-08.2 - Raymond Lillibridge - <szpct;###>&entity;</szpct> -> HTML
# 2016-03-08.1 - Raymond Lillibridge - Added: HTML entities -> characters (utf-8)
# 2016-03-01.1 - Raymond Lillibridge - Added: more entities
# 2016-02-19.1 - Raymond Lillibridge - Added:  &circle*;  (entities for &circle[A..Z, a..z, and 0..20];)
# 2016-01-01.1 - Raymond Lillibridge - <check> -> &check; so p1_GenCodeEntities can convert it.
# 2015-10-06.1 - Raymond Lillibridge - Added: fmt="xpp04" -> fmt="double"
# 2015-09-14 - Raymond Lillibridge - Remove <LL_header*></LL_header> (and "footer") empty GenCode tags
# 2015-09-08 - Raymond Lillibridge - Remove <LL_header*>...</LL_header> (and "footer") GenCode tags 
#				(These have to do with Colorado Code header|footer in XPP)
# 2015-02-20.1 - Raymond Lillibridge - allow: ;adv=*;, <adv=*>, and <adv;*> GenCode formats to be converted
#					Also corrected a potential issues with the mix of signatures and "adv" tags.
# 2015-01-30.1 - Raymond Lillibridge - CORRRECT - Place charter, cross, and statelaw refs in elements not comments.
# 2015-02-21.1 - Raymond Lillibridge - All levels for historynotes ;oh[0-9];
# 2014-12-05.1 - Raymond Lillibridge - <ba>, <bx>, and <hp> to PIs
# 2014-10-17.1 - Raymond Lillibridge - Create content wrapper PI's for <co;...> tags
#				<?xpp co;White?>This is my text that should be white.<?xpp coend?>
# 2014-10-07.1 - Raymond Lillibridge - Convert <abrule> to PI:  <?xpp abrule?># 2014-09-12.1 - Raymond Lillibridge - Correct infinite loop when <selectout/> tags are not balanced.
# 2014-08-28.1 - Raymond Lillibridge - Corrected issues with:  ;ul;;en;...;\ul;  tagging
# 2014-08-26_27.1 - Raymond Lillibridge - Added:  p1_XPPmeasurementConversion()
# 2014-08-21.1 - Raymond Lillibridge - Added: </toc> along with existing <\toc> to <toc> while loop.
# 2014-08-14.1  - Raymond Lillibridge - Make <selectout;*>...</selectout> processing FIRST, before _PubMap Levels processing.
# 2014-07-10.1 - Raymond Lillibridge - How to keep "C:\abc\def\myword.docx" from becoming a <monetary></monetary> element.
#				Solution:  convert &bsol; to PI, do "monetary" processing, then convert the PI back to '\'
#				For this solution, the Editor's will need to use the entity '&bsol;' in GenCode where they want an actual '\' character.
# 2014-02-10.1 - Raymond Lillibridge - Commented out p1_SingleTags -> <fv;#>  to PI  (David's work)
#----------------------------------------------------------------------------------
# 2014-01-09.2 - Raymond Lillibridge - TESTING AltovaXML.exe for parsing
# TESTING (results written in minutes:seconds:hundredths):  
# Xerces (16115) 28 files 		= 0:46.96 @10:45am	= 1:15:00 @ 11:04am
# AltovaXML (16115) 28 files	= 1:53:39 @10:50am	= 2:20:94 @ 11:00am
#----------------------------------------------------------------------------------
# 2014-01-16.1 - Raymond Lillibridge - Peoria, IL (10183 custom programming) &squ; -> &#x25A1;
# 2014-01-09.1 - Raymond Lillibridge - <mdit> -> <ital>
# 2013-12-12.1 - Raymond Lillibridge - Under certain conditions the @mark value is incorrect.
#				IF FOOTNOTE @mark is -blank- then there is probably a GenCode tag issue
# 2013-10-03.1 - Raymond Lillibridge - Allow passing in --nopause option
# 2013-09-30.1 - Raymond Lillibridge - <xref;...> to PI is not coded properly
# 2013-09-04.1 - Raymond Lillibridge - RE-compile after malware attack issues
# 2013-07-30.1 - Raymond Lillibridge - Convert all image file extensions to ".png" (except for inputs of *.pdf)
# 2013-06-24.2 - Raymond Lillibridge - When <selectout;...> contains 'none', replace all parms with 'none'.
# 2013-06-24.1 - Raymond Lillibridge - Corrected P1_SingleTags for in-line <selectout/> mixed with <?xpp ...?> PI's
# 2013-06-05.1 - Raymond Lillibridge - IF 'none' memeber of <selectout/> values, then delete all other values for this element
# 2013-05-14.1 - Raymond Lillibridge - Removed $character_entities_declaration stuff because XSLT bombs with the DTD entries (Dang!)
# 2013-05-13.1 - Raymond Lillibridge - Added $character_entities_declaration to XML files
# 2013-05-09.1 - Raymond Lillibridge - Convert select GenCode character "entities" to U+XXXX
# 2013-05-08.1 - Raymond Lillibridge - <setcell...> not being converted to PI properly
# 2013-04-25.1 - Raymond Lillibridge - Fix <img.../> missing value for width/height if image is stacked inside a pickup
# 2013-04-23.1 - Raymond Lillibridge - Making sure there is a value for <img @width and @height /> if possible.
# 2013-04-15.1 - Raymond Lillibridge - Rewrite getImgData() for pickups; PHIL's BIRTHDAY!
# 2013-04-07.1 - Raymond Lillibridge - Perl 5.16.3 - thus Term-ReadKey and NOT Term-InKey
# 2013-03-28.1 - Raymond Lillibridge - Calculation for Image Width
# 2013-03-25.1 - Raymond Lillibridge - Added skipping bold, italic, bdit, & med as containter tags lfor selectout processing
# 2013-03-22.1 - Raymond Lillibridge - Convert to PIs: <tbldepth;n>, <la;n>, <i1;n>
# 2013-03-15.1 - Raymond Lillibridge - <gettext;...> and <settext;...> removed from XML
# 2013-03-08.1 - Raymond Lillibridge - <selectout..>This is ;b;BOLD;\b; text.</selectout>  Allow inline text inside selectout inline tags.
# 2013-03-01.1 - Raymond Lillibridge - Problem:  Extra <!-- --> tags around CAPT of pickup.  (13857 T001 (1-08-010.tif)
# 2013-03-07.1 - Raymond Lillibridge - Change logic to remove ALL content between <toc> and <\toc> GenCode tags
# 2013-02-18.1 - Raymond Lillibridge - <selectout;ebook;html;none;print>...</selectout>
#				USE:
#				(A) As GenCode wrapper around levels, paragraphs, and other block content -
#					These will NOT stay as elements in XML, rather, they will be converted to 
#					become XML Attributes of all enclosed container tags  ex:
#					<para class="sec" selectout="html">...</para>
#				(B) As GenCode inline-element - This will stay as <selectout out="ebook html none print">XYZ</selectout> in XML
# 2013-01-08.1 - Raymond Lillibridge - Adding @mark to <footnoteref/> element; rm initial <sup/> in refs
# 2012-12-18.1 - Raymond Lillibridge - ;oh5z; - LaRae added this tag 2012-12-05.  Lisa just "found" it because the file '42 won't validata
#				Lisa and I decided to simply convert the ;oh5z; tags to ;oh5; for this application and thus the web.
# 2012-11-30.1 - Raymond Lillibridge - (CALS) ConvertTable - problem with 'empty' cells with <fcell...> tag not shading
# 2012-11-29.1 - Raymond Lillibridge - Change new Unicode fraction characters back to PIs 'cause SharePoint can't handle them
# 2012-11-27.1 - Raymond Lillibridge - Moved the <em>, &c. stuff to occur before frac for (ex): <sup><frac;d;;em;></sup> stuff
# 2012-11-21.1 - Raymond Lillibridge - Added:  frax for 1/3, 2/3, 1/5 - 4/5, 1/6, 5/6 Unicode characters
# 2012-11-19.1 - Raymond Lillibridge - Add/check:  &bsol;  "backslash" character  \x{005C}
# 2012-09-04.1 - Raymond Lillibridge - Added:  <ahrule> processing
# 2012-08-30.1 - Raymond Lillibridge - Added:  <circle1> through <circle20> (see p1_GenCodeEntities())
# 2012-08-24.1 - Raymond Lillibridge - Added conversions for ;c;, ;r;, <c>, <r> and corrected <lf> issue
# 2012-07-16.1 - Raymond Lillibridge - Changed: (LEXIS) $line =~ s|;oh3x;|;oh3;|g;  to  $line =~ s|;oh(\d)x;|;oh$1;|g;
# 2012-07-09.1 - Raymond Lillibridge - added logic to process ;oh5l; to become ;oh5; and ;ol0;
# 2012-05-21.1 - David Nichols - added logic to force IGT-based images to resize according to the size of their container
# 2012-05-17.1 - Raymond Lillibridge - Fix problem commenting pickups with text elements that have multiple quad macros.
# 2012-05-16.2 - Raymond Lillibridge - Adding "origin" attribute for elements: book, level3, and section.  Ex.  @origin="CH001"
# 2012-04-11.1 - Raymond Lillibridge - All Font Variants, re-worked
# 2012-01-11.1 - Raymond Lillibridge - ulink processing
# 2012-01-09.1 - Raymond Lillibridge - Finished testing and compiled
# 2012-01-04.2 - Raymond Lillibridge - Convert <setwid;-#;xxx> (negative column numbers)
# 2012-01-04.1 - Raymond Lillibridge - Convert <Tr;;0> and <Tc;;0> (span all/remaining columns)
# 2011-12-12.1 - Raymond Lillibridge - Debugging issues with table cell spanning H & V
# 2011-09-26.1 - David Nichols - added HTML char entity for gencode <check>
# 2011-08-31.1 - David Nichols - added new S&R to make ;xil; and ;i1=#; PI's.
# 2011-08-25.1 - Raymond Lillibridge - skip all content between and including <print_specs>...</print_specs>
# 2011-07-21.1 - Raymond Lillibridge - <ulink @target="_blank"...> as default if not present.
# 2011-06-24.1 - Raymond Lillibridge - Updates to <ulink/> tags
# 2011-06-06.1 - Raymond Lillibridge - Allow GenCode files:  *.mcc
# 2011-05-03.1 - Raymond Lillibridge - Manage new optional attribute to schema, @class of <ulink/> 
# 2011-04-05.1 - Raymond Lillibridge - noprint and ulink issue - David and I decided to simply remove the <noprint> tags for XML.
# 2011-04-01.1 - David Nichols - Added handling for <xref;ignore> and <xref;resume>
# 2011-04-01.2 - Raymond Lillibridge - Problem with <noprint> around a ;oh#; tag...
# 2011-04-01.1 - Raymond Lillibridge - convert ;note; to <rn0;0>
# 2011-03-30.1 - Raymond Lillibridge - convert ;note; to ;b0;\n<sz;8q>...
# 2010-12-01.2 - David Nichols - converts "<noprint>" to "<output doc="*">"
# 2010-11-29.1 - David Nichols - Added into "PASS-ZERO" logic to remove <analysis>...</analysis> from XML
# NOTE:  the <output doc="">...</output> tags can ONLY wrap level 0 paras. (hc, p0, b0, h0)
# 20101105.2 - Raymond Lillibridge - Changed parser from AltovaXML to Xerces
# 20101103.1 - Raymond Lillibridge
#		- Added code to set application path properly from *.pl or *.exe version of the application.
#		- Changed parser from Xerces to AltovaXML
# 20101006.1 - Raymond Lillibridge - $parm =~ s|<AsteriskLarge>|\x{002A}|g;
# 20101004.1 - Raymond Lillibridge - <px/> <pa> single integer parms allowed
# 20100924.1 - Allow multiple "anchor" tags in a given "line" or para, &c.
# 20100923.1 - Removing footnotes for corresponding ;aoh5; ;anchor;'s
# 20100921.1 - Allowing tables inside footnotes


################################################################ 
# Get application path:
################################################################ 
my $AppPath = "";

if(!defined $PerlApp::VERSION)	{
	$AppPath = $0;
} else {
	$AppPath = PerlApp::exe();	
}

# Added to detect drive letter from which apps are currently running
my $mappedDrive = substr($AppPath,0,2);			#ex.  R:
# XML Parsers:
#my $AltovaCMD = $mappedDrive . "\\APPS_3rdPARTY\\AltovaXML.exe";
my $xercesCMD =  $mappedDrive . "\\APPS_3rdPARTY\\xerces\\bin\\StdInParse -n -s";				# 20101105.2 RAL

# LOCAL C:\ version...
my $SchemaLevelBlurb = 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"' .
							' xsi:noNamespaceSchemaLocation="' . $mappedDrive . '\SCHEMA\MCC\XSD\CODE\code.xsd"';						

#---------------------------------------------------------------------------------------------------
# PREQUISITES & ASSUMPTIONS:													
#---------------------------------------------------------------------------------------------------
# (+)  ALL  files need to have a containing GenCode tag  i.e.  ;oh#;																								

#---------------------------------------------------------------------------------------------------
# DISCUSS:																		
#---------------------------------------------------------------------------------------------------

#---------------------------------------------------------------------------------------------------
# NOTES:
#---------------------------------------------------------------------------------------------------

#=============================================	
# PRAGMA
#=============================================	
use warnings;
no warnings 'uninitialized';
use Term::InKey;
use File::Basename;
use File::Copy;
use Math::Round;
use XML::Simple qw(:strict);
$XML::Simple::PREFERRED_PARSER = 'XML::Parser';
my $XMLPubMapdriver;
use Data::Dumper;
#perl2exe_include "Data/Dumper.pm"
use Tie::File;
use Scalar::Util qw(looks_like_number);
use constant { TRUE => 1, FALSE => 0 };

#=============================================	
# GET OPTIONS (not select files)
#=============================================
use Getopt::Long;
my $opt_topause="";	# --topause	pause or nopause 

GetOptions(
	"topause=s"	=>	\$opt_topause
);

#=============================================	
# GLOBALS
#=============================================
my $ParseLog="_ParseLog.txt";
my $PubMap="./CONFIG/_PubMap.xml";
my $ext0 = "_TMP_00.xml";
my $ext1 = "_TMP_01.xml";
my $ext2 = "_TMP_02.xml";
my $ext3 = "_TMP_03.xml";
my $ext4 = "_TMP_04.xml";
my $ext5 = "_TMP_05.xml";
my $ext6 = "_TMP_06.xml";
#my $ext7 = ".xml";
my $ext7 = "_TMP_07.xml";
my $ext8 = ".xml";
my $outfile = "";
my $output_buffer = "";
my $x;
my $key;

my $dashes = "=" x 50;

my %spanRow = ();
my %spanCol = ();
my $activeCol = 0;

my $ctr = 1;
my $seq=0;
my $tot = 0;
my $gtot = 0;
my $myStr1 = "";
my $myStr2 = "";
my $myInt1 = 0;
my $myInt2 = 0;
my %colspecHash = ();
my $myColumnCount;
my $index = 0;
my $go;
my $promptLevels = 0;

my $footnoteIDprefix = "";
my $footnoteSeq = 1;
my $footnotePending = "no";
my $fnLevel2Close = "0";
my $fnLevel= 0;
my @fnStack = ();

my $CloseSubtitle ="";
my $CloseThisTag ="";
my $CloseListitem ="";
my $PreviousLine = "";
my $p4_ContainingElement = "";
my $line = "";
my $lineTemp = "";
my $G_OpenTableFlag = "no";
my @G_TableFootnotes = ();
my $monetaryFlag = FALSE;
my $gclevel= "";
my $cnt;
my $entryCount = 0;
my $parseCtr = 0;

my @interpPart=();
my @T = ();
my @aTemp = ();
my @aTemp2 = ();
my @Astr = ();
#my @aImageNameInvalid = ();
my $OriginalTableAsString = "";
my $str = "";
my $str2 = "";

my $temp8 = "";
my @aX = ();
my $count = "";

#20100923 FLAGS...
my $in_aoh=0;
my $in_aoh_anchor=0;
my $flag_lz = FALSE;
my $in_fn = FALSE;
my $in_rn = FALSE;				  

# STACKS...
my $OHTypeStack = ();	# Head Level Tag Type Stack
my @OHNoStack = ();		#Head Level tag stack LEVEL
my $CTTypeStack = ();		# container Tag Type Stack
my @CTNoStack = ();		#general tag stack LEVEL
my @tagStack = ();		#general tag stack
my @RNStack = ();			# Reference Note Stack
my @removeStack = ();
my @prelimArray = ();		# array that will hold 'aprelim' file, one line per item.
my @prelimNewContent = (";oh0;", "\\");

#=============================================	
# HOUSEKEEPING
#=============================================	
## Make empty argument list default to all files
@ARGV = glob("*") unless @ARGV;
# Filter out all but plain, text files.
#@ARGV = grep { !(/^.*\./) && -f && -T} @ARGV;						#HOW TO SELECT ONLY *. FILES?????
@ARGV = grep { (!(/^.*\./) or /^.+?\.mcc$/gi ) && -f && -T} @ARGV;		#HOW TO SELECT ONLY *. FILES?????

if($#ARGV == -1) {
	print(STDOUT "------------------------------\n     FILE(S) NOT FOUND!\n------------------------------\n");
	exit 0;	
}

&Clear;

print(STDOUT "\nRUNNING:  g2x.exe (pl)\t\t$myVersion\n");
print(STDOUT "Processing the following input files:\n");
print(STDOUT "*------------------------------------------------------------------------------*\n");

foreach $infile (@ARGV) {
		printf(STDOUT "%3s \t%s\n", $ctr, $infile);	#display to user what files are being processed
		$ctr++;
}

# Process the files or quit?
print(STDOUT "\n     continue?...\n----------------------------------------------------------------\n" . 
	" Y = YES                                      --OR--     \n\n K = YES    \(Keep Temp Files\)                 --OR--     \n\n\(blank\) or N = CANCEL\n" . 
	"\n----------------------------------------------------------------\n");
	
if($opt_topause ne "nopause") {
	$x = ReadKey();
} else {
	$x="Y";	
}

if(uc($x) =~ /[YK]/) {
	# do nothing...
	print STDOUT "\n";	#add a little space before processing the files
	
	if(uc($x) =~ /[K]/)	{
		$promptLevels = 1;	
	} else {
		$promptLevels = 0;
	}
	
} else {
	print(STDOUT "\n\...Processing cancelled.\n");
	exit 0 ;
}

#=============================================	
# HOUSKEEPING - END
#=============================================	


#=============================================	
# MAIN
#=============================================	
# At this point @ARGV should contain all of the files to process.

$ctr=1;
$seq=1;
my $Fstring = "";
my $printSpecFileTmp = "";
my $oh5l_title = "";
my $oh5l_subtitle = "";
my $oh5l_listincr = "";
my $oh5l_listcontent = "";
my $oh5l_remainder = "";
my $inSelectout = 0;

my $hgInline = "";
my $hgInline_number = 0;

foreach $infile (@ARGV) {
	
	if(not exists $XMLPubMapdriver->{'PubMap'})	{	
		GetPubMapXML();
	}
	
	# Read $infile into a scalar... (GenCode files should be ASCII;  UTF8 files begin with:  EFBBBF)
	open GCFILE,  $infile ||
		 die "Couldn't open file: $!"; 
		 
	$Fstring = "";
	while (<GCFILE>){
		$Fstring .= $_;
	}
	
	close GCFILE;	
	
	# Remove print-specs tags and content in $Fstring...
	$Fstring =~ s|<print_specs>(.+?)<\/print_specs>||gs;
	
	$printSpecFileTmp = $infile . ".tmp";
	open(MYOUTFILE, ">$printSpecFileTmp"); #open for write, overwrite
	print MYOUTFILE "$Fstring";
	close MYOUTFILE;
	
	open(FILE, '<', "$printSpecFileTmp")
		|| die "cannot open file";
	
	print(STDOUT "$seq\t$infile\.xml\n");
	$seq++;
	$ctr = 1;
	
#---------------------------------------------------------------------------------------------------------- 
# PASS-ZERO PASS-ZERO PASS-ZERO PASS-ZERO PASS-ZERO PASS-ZERO PASS-ZERO  
#----------------------------------------------------------------------------------------------------------	
	$outfile = ($infile . $ext0);		# create PASS-ZERO output file...	
	
	open(OUT, ">:utf8", $outfile)
		|| die "cannot open file for output:  $outfile";

	print OUT "<\?xml version=\"1.0\" encoding=\"UTF-8\"\?>\n<XML>\n";

	while(<FILE>)	{
		my($line) = $_;
		chomp($line);			# Strip the trailing newline from the line.
		
		$line =~ s/\s+$//;
		
		# (JOB=16159) To handle XPP's unique 'hg' tagging to eventually get a Word document with an em-space indented 'hang' format, with appx. requested indent for all wrapped content of the ;b#; paragraph/block.
		
		# First, is it a ;b#; OR ;fl; OR ;hl' tag... if so, then look ahead for the pattern: ;hg;
		$hgInline = "";
		$hgInline_number = 0;
				
		if( ($line =~ /^;b\d;/) or ($line =~ /^;fl;/) or ($line =~ /^;hl;/)) {			
			# NOTE:  may or may not become @class="h#-indent#"
			$hgInline = $line;
			$hgInline_number = $line;
			
			if($line =~ /^;b\d;/) { 
				$hgInline_number =~ s|^;b(\d);$|$1|g;
			} elsif(($line =~ /^;fl;/) or ($line =~ /^;hl;/)) {
				$hgInline_number = 0;			
			}
			
			$line = <FILE>;
			chomp($line);		
			if(	($line =~ /;hg;/ ) and ($hgInline_number < 10)	) {				
				$hgInline = Build_Inline_Hang_Line($hgInline, $line);
				print OUT "$hgInline\n";
				next;
			} else {
				print OUT "$hgInline\n";
				print OUT "$line\n";
			}
			next;
		}		
		if( ($line=~/<LL_[hf]/) ) {
				$line =~ s|<LL_header><\/LL_header>||g;
				$line =~ s|<LL_header>(.+?)<\/LL_header>||g;
				$line =~ s|<LL_header;(.+?)><\/LL_header>||g;
				$line =~ s|<LL_header;(.+?)>(.+?)<\/LL_header>||g;
			#	$line =~ s|<LL_header><\/LL_header>|<?xpp LL_header?>|g;
			#	$line =~ s|<LL_header>(.+?)<\/LL_header>|<?xpp LL_header;$1?>|g;
			#	$line =~ s|<LL_header;(\d+)><\/LL_header>|<?xpp LL_header;$1?>|g;
			#	$line =~ s|<LL_header;(\d+)>(.+?)<\/LL_header>|<?xpp LL_header;$1;$2?>|g;
				
				$line =~ s|<LL_footer><\/LL_footer>||g;
				$line =~ s|<LL_footer>(.+?)<\/LL_footer>||g;
				$line =~ s|<LL_footer;(.+?)><\/LL_footer>||g;
				$line =~ s|<LL_footer;(.+?)>(.+?)<\/LL_footer>||g;
			#	$line =~ s|<LL_footer><\/LL_footer>|<?xpp LL_footer?>|g;
			#	$line =~ s|<LL_footer>(.+?)<\/LL_footer>|<?xpp LL_footer;$1?>|g;
			#	$line =~ s|<LL_footer;(\d+)><\/LL_footer>|<?xpp LL_footer;$1?>|g;
			#	$line =~ s|<LL_footer;(\d+)>(.+?)<\/LL_footer>|<?xpp LL_footer;$1;$2?>|g;
		}

		if( ($line=~/<setgtext/) or ($line=~/<pregtext/) ) {
			$line =~ s|^<setgtext(.+?)>\s+$|-gtext-no-output-1|mg;
			$line =~ s|^<setgtext([^>]+?)>$|-gtext-no-output-2|mg;
			$line =~ s|^<setgtext;>\s+$|-gtext-no-output-3|mg;
			$line =~ s|^<setgtext>\s+$|-gtext-no-output-4|mg;
			$line =~ s|^<setgtext;>$|-gtext-no-output-5|mg;
			$line =~ s|^<setgtext>$|-gtext-no-output-6|g;
			
			$line =~ s|^<pregtext(.+?)>\s+$|-gtext-no-output-7|mg;
			$line =~ s|^<pregtext(.+?)>$|-gtext-no-output-8|mg;
			$line =~ s|^<pregtext;>\s+$|-gtext-no-output-9|mg;
			$line =~ s|^<pregtext>\s+$|-gtext-no-output-10|mg;
			$line =~ s|^<pregtext;>$|-gtext-no-output-11|mg;
			$line =~ s|^<pregtext>$|-gtext-no-output-12|g;
		
			if($line =~ /-gtext-no-output-/) {					
				next;
			}
		}
		
		# Fix ;fl and <mh;0> for pickups on different lines
		if($line =~ /^;fl;/) {
			$str=readline(FILE);
			chomp($str);
			if ($str eq "<mh;0>") {
				$line .= $str;
				print OUT "$line\n";
			} else {
				print OUT "$line\n";
				print OUT "$str\n";
				$line = $str;
			}
		}
		
		# REMOVE ^<selectout;print...>...^</selectout> content.
		if($line =~ /^<selectout;print/) {
			while($line !~ /^<\/selectout>/) {						
				$line=readline(FILE);
				
				if(eof) {	
					die "\n$dashes\n\n\tE R R O R ! \t\t\"selectout\" tag\(s\) not balanced.\n\n$dashes\n\n";					
				}
			
				chomp($line);
				$ctr++;
			}	
			# found </selectout>...
			$line=readline(FILE);
			chomp($line);
			$ctr++;
		}
		
		# strip leading and trailing whitespace
		for ($line) {
			s/^\s+//;
			s/\s+$//;	
		}
		
		# convert <mdit> to <ital>
		$line =~ s|<mdit>|<ital>|g;
		
		# REMOVE <toc>...<\toc>... (2013-03-07 Per LaRae)
		if($line =~ /^<toc>/)
		{
			while( ($line !~/<\\toc>/) and ($line !~/<\/toc>/) ) {	# ADDED 2014-08-21
				$line=readline(FILE);
				chomp($line);
				$ctr++;
			}
			
			# found <\toc>...
			$line=readline(FILE);
			chomp($line);
			$ctr++;
		}	
		
		# ;oh#[^5];  ->  ;oh#;
		if($line !~ /;oh5/) {
			$line =~ s|^;oh(\d)(.+?);|;oh$1;|g;
		}
		
		# Broward  13528 (;oh5z;)...
		$line =~ s|;oh5a;|;oh5;~~~oh5a~~~|g;
		$line =~ s|;oh5b;|;oh5;~~~oh5b~~~|g;
		$line =~ s|;oh5c;|;oh5;~~~oh5c~~~|g;
		$line =~ s|;oh5l;|;oh5;~~~oh5l~~~|g;
		$line =~ s|;oh5x;|;oh5;~~~oh5x~~~|g;
		$line =~ s|;oh5z;|;oh5;~~~oh5z~~~|g;
	
		# ;oh5l; setup to become: ;oh5; & ;ol0;
		if($line =~ /~~~oh\dl~~~/) {
			print(OUT "$line\n"); # echo line read
			$ctr++;
			
			# Read next line:
			$line=readline(FILE);
			chomp($line);
			
			($oh5l_title, $oh5l_subtitle, $oh5l_listincr, $oh5l_listcontent, $oh5l_remainder) = split /\\/, $line;
						
			print(OUT "$oh5l_title\\$oh5l_subtitle\n\n");
			print(OUT ";ol0;\n");
			print(OUT "$oh5l_listincr\\$oh5l_listcontent\n");
			
			# Read next line:
			$line=readline(FILE);
			chomp($line);
		}
		
		#Raymond 2009-11-16
		$line =~ s|<Tc>|\n<Tc>|g;
		$line =~ s|<Tc(.+?)>|\n<Tc$1>|g;

		#Raymond 2010-05-20:
		$line =~ s|^;(.?[po])(\d)off;|;$1$2;|g;
		
		# David 2010-11-30:		; 2012-07-09 Raymond - may clash error corrected:
		#$line =~ s|(<)(analysis>)||gi;	
		#$line =~ s|(<)(/)(analysis>)||gi;
		$line =~ s|<analysis>||gi;	
		$line =~ s|</analysis>||gi;
		
		# Raymond 2010-06-14 - makes life easier in pass-one of this stuff:
		if(
			($line =~ /<\/header\d>/)			|
			($line =~ /<\/FooterL>/)				|
			($line =~ /<\/FooterR>/)				|
			($line =~ /<\/footertext>/)			|
			($line =~ /<\/footer(\d*+)(.?[lr])>/)
			) {
			
			$line =~ s|(<\/header\d>)|$1\n\n|g;
			$line =~ s|(<\/FooterL>)|$1\n\n|g;
			$line =~ s|(<\/FooterR>)|$1\n\n|g;
			$line =~ s|(<\/footertext>)|$1\n\n|g;
			$line =~ s|(<\/footer\d+)(.?[lr]>)|$1$2\n\n|g;
		}
		
		# Raymond 2011-04-01...
		# ;note; or <note> converted to ;rn0;9; and <sz;8q>...
		$line =~ s|;note;|<note>|g;
		$line =~ s|<note>|;rn0;9;|g;
		
		#===========================================
		# Ocoee, FL 14323 <his>, <genref>, and <genref0> stuff...
		#===========================================
		if($line =~ /^<his>/)	{
			$line =~ s|<his>|;fl;|g;
			print(OUT "$line\n"); # echo line read
			$ctr++;
			$lineTemp = $line;
			$line=readline(FILE);
			chomp($line);
			$line =~ s|^(.+?)$|;b;$1;\\b;|g;
		}
		
		if($line =~ /^<genref>/)	{
			$line =~ s|<genref>|;fc;|g;
			print(OUT "$line\n"); # echo line read
			$ctr++;
			$lineTemp = $line;
			$line=readline(FILE);
			chomp($line);
			$line =~ s|^(.+?)$|<sz;8q>;b;$1;\\b;|g;
		}
		
		if($line =~ /^<genref0>/)	{
			$line =~ s|<genref0>|;fl;|g;
			print(OUT "$line\n"); # echo line read
			$ctr++;
			$lineTemp = $line;
			$line=readline(FILE);
			chomp($line);
			$line =~ s|^(.+?)$|<sz;8q>;b;$1;\\b;|g;
		}
		#===========================================
		# END:  Ocoee, FL 14323 <his>, <genref>, and <genref0> stuff...
		#===========================================
		
		print(OUT "$line\n"); # echo line read
		$ctr++;
	}	# end while...
	
	print(OUT "</XML>"); # echo line read
	$ctr++;
	$tot += $ctr;
	$gtot += $tot;
	
# 	print(STDOUT "\t--------------------------------------------------\n");
# 	print(STDOUT "\t\tTotal Lines Processed (This File):  $tot\n\n");
# 	print STDOUT "\(press any key to continue...\)\n\n";
# 	$x = ReadKey();
 	
	$tot=0;
	
	close(FILE);
	close(OUT);
	
		
#---------------------------------------------------------------------------------------------------
# PASS-ONE PASS-ONE PASS-ONE PASS-ONE PASS-ONE PASS-ONE PASS-ONE PASS-ONE
#---------------------------------------------------------------------------------------------------


	if($promptLevels > 0)	{
		print STDOUT "\nPausing Before Pass-One\n";	
		$x = &ReadKey;
	}


	my $infile1 = $outfile;		# PASS-ONE input file name...
	
	open(FILE, '<:encoding(utf8)', $infile1)
		|| die "cannot open input file: $infile1";
	
	$ctr = 1;
	$outfile = ($infile . $ext1);		# create PASS-ONE output file...
	
	open(OUT, ">:utf8", $outfile)
		|| die "cannot open file for output:  $outfile";
	
	$footnoteIDprefix = $infile;

	while(<FILE>) {
		 my($line) = $_;
		chomp($line);			# Strip the trailing newline from the line.
		
		# Remove Word Perfect ending character  0x1A
		$line =~ s|\x1A||g;
		
		#Example:  Forsyth, NC (14363)...
		$line =~ s|;ol(\d*+)def;|;ol$1;|g;
		$line =~ s|;ol(\d*+)note;|;ol$1;|g;		
		$line =~ s|;ol(\d*+)num;|;ol$1;|g;		
		$line =~ s|;ol(\d*+)off;|;ol$1;|g;
		
		# ELKHORN, WISCONSIN (14273) - ;ol0lead;...		# 20100727
		$line =~ s|;ol(\d*+)lead;|;ol$1;|g;
		$line =~ s|;p(\d*+)lead;|;p$1;|g;				# 20100804 (David Nichols)
		$line =~ s|;b(\d*+)lead;|;p$1;|g;				# 20100804 (David Nichols)
		$line =~ s|;hg(\d*+)lead;|;p$1;|g;				# 20100804 (David Nichols)

		# Added:  20100831 per Lisa and David...
		$line =~ s|;p(\d*+)lead;|;p$1;|g;
		$line =~ s|;b(\d*+)lead;|;b$1;|g;
		$line =~ s|;hg(\d*+)lead;|;hg$1;|g;
		$line =~ s|;p(\d*+)off;|;p$1;|g;
		$line =~ s|;b(\d*+)off;|;b$1;|g;
		$line =~ s|;hg(\d*+)off;|;hg$1;|g;
		
		# Convert: ;b#head;  to  ;bhead#;...
		$line =~ s|;b(\d)head;|;bhead$1;|g;

		# Convert:  ;ol#head;  to  ;olhead#;...
		$line =~ s|;ol(\d)head;|;olhead$1;|g;		
		
		#===========================================
		# <ig; name; width; depth; process; scale; rotate; color; priority; cx; cy; cw; cd?>
		#===========================================
		# Check for the <alt> tag after the ig.	
		if($line =~ /<ig;/)	{		
			$line =~ s|<(q)(.?[lcra])>|<?xpp $1$2?>|g;
#			$line =~ s|<(ig;)(.+)$|\n<!-- parms: \<ig; name; width; depth; process; scale; rotate; color; priority; cx; cy; cw; cd\> -->\n<comment note='ig'>\n<!-- <$1$2 -->\n</comment>\n\n|g;	
#			$line =~ s|<(ig;)(.+)$|\n<comment note='ig'>\n<!-- <$1$2 -->\n</comment>\n\n|g;
			if ($line =~ /<ig;.*><alt>/ ) {
				$line =~ s|<(ig;)(.+?><alt>.*</alt>)|<comment note='ig'><!-- <$1$2 --></comment>|g;	
			} else {
				$line =~ s|<(ig;)(.+?)>|<comment note='ig'><!-- <$1$2> --></comment>|g;
			}
		}
			
		#===========================================
		# <igt;...>			Allow <COMMENT/> inside CALS <entry>...</entry> elements!!!
		#===========================================
		# Check for the <alt> tag after the igt.
		if($line =~ /<igt;/)	{
			$line =~ s|<(q)(.?[lcra])>||g;					
#			$line =~ s|<(igt;)(.+)>|\n<!-- parms: \<igt; name; width; depth; process; quad; scale; rotate; color; priority; cx; cy; cw; cd\> -->\n<comment note='igt'>\n<!-- <$1$2> -->\n</comment>\n\n|g;
#			$line =~ s|<(igt;)(.+).?[>]|\n<comment note='igt'>\n<!-- <$1$2> -->\n</comment>\n\n|g;
			if ($line =~ /<igt;.*><alt>/ ) {
				$line =~ s|<(igt;)(.+?><alt>.*</alt>)|<comment note='ig'><!-- <$1$2 --></comment>|g;	
			} else {
				$line =~ s|<(igt;)(.+?)[>]|<comment note='igt'><!-- <$1$2> --></comment>|g;	
			}
		}
				
		#===========================================
		# San Francisco 14139 <interp6/8> stuff...
		#===========================================
		if($line =~ /^<interp\d>/)	{

			$lineTemp = $line;
			$line=readline(FILE);
			chomp($line);
			
			while ( $line =~ /^.[^;]/ )  {
				if(  ($line =~ /^</)		and	(substr($line, 0, 5) ne "<pick")	  ){
					last;
				}
				
				$lineTemp=$lineTemp . $line;
				$line=readline(FILE);
				chomp($line);				
			}
			
			$lineTemp=~ s|<interp(\d)>(.+?)$|<interp$1_0>\n$2\n|g;
			$lineTemp=p1_SingleTags($lineTemp);
			$line = $lineTemp;			
		}	
		
		#===========================================
		# 10183 Peoria, IL
		#===========================================
		#	<ff;53><sz;8q>&squ;<ff;2><sz;6q> to &#x25A1;
		#===========================================
		$line =~ s|<ff;53><sz;8q>&squ;<ff;2><sz;6q>|&#x25A1;|g;
		
		#===========================================
		#  San Francisco <copyright>...</copyright> REMOVAL
		#===========================================
		if($line =~ /^<copyright>/)	{
			$lineTemp = $line;
			$line=readline(FILE);
			chomp($line);
			
			while ($lineTemp !~ /<\/copyright>/)	{
					$lineTemp = $lineTemp . $line;
					$line=readline(FILE);
					chomp($line);
			}
			
			$line = $lineTemp;
		}			

		#===========================================
		#  <FooterR/L/>,  <footertext/>, <footer#[lr]/>, <header\d>
		#===========================================
		# NOTICE!!!  SEE PASS-ZERO, WHICH INSERTS A BLANK LINE AFTER EACH CLOSING TAG (BELOW):
		if(
			($line =~ /^<header\d>/)				|
			($line =~ /^<FooterL>/)					|
			($line =~ /^<FooterR>/)					|
			($line =~ /^<footertext>/)				|
			($line =~ /^<footer(\d*+)(.?[lr])>/)
			) {

			$lineTemp = $line;
			
			$line=readline(FILE);
			chomp($line);
			
			while (
				($lineTemp !~ /<\/header\d>/)			and
				($lineTemp !~ /<\/FooterL>/)			and
				($lineTemp !~ /<\/FooterR>/)			and
				($lineTemp !~ /<\/footertext>/)		and
				($lineTemp !~ /<\/footer(\d*+)(.?[lr])>/)
				) {
				$lineTemp = $lineTemp . $line;
				$line=readline(FILE);
				chomp($line);
			}
			
			$line = $lineTemp . $line;
		}	
		
		#===========================================
		#  sig(lr)   OR   adv= (numeral part, ex.  0.5  or  5) ...;adv=0.5p;
		#===========================================
		if(
			($line =~ /^;adv=(\d+)\.(\d+)([qpcdimknuz]);$/)		|
			($line =~ /^;adv=(\d+)([qpcdimknuz]);$/)				|
			($line =~ /^<adv=(\d+)\.(\d+)([qpcdimknuz])>$/)		|
			($line =~ /^<adv=(\d+)([qpcdimknuz])>$/)			|
			($line =~ /^<adv;(\d+)\.(\d+)([qpcdimknuz])>$/)		|
			($line =~ /^<adv;(\d+)([qpcdimknuz])>$/)				|
			($line =~ /^;sig(.?[lr]);/)			
			) {
			$lineTemp = $line;
			$line=readline(FILE);
			chomp($line);
			$line = $lineTemp . $line;			
		}
		
		# SIGNATURE...
		if($line =~ /^;sig(.?[lr]);/)	{			
			if(substr($line, 4, 1) eq "r")	{
				print OUT "<signature justify='right'>\n";
			} else {
				print OUT "<signature justify='left'>\n";
			}	

			$line = substr($line,6);
			$line = p1_SingleTags($line);	 		
			@part = split(/\\/, $line);	
			print OUT "\t<sig_name>$part[0]</sig_name>\n";
			print OUT "\t<sig_title>$part[1]</sig_title>\n";
			print OUT "</signature>\n";
			$line = "";
		}

			
#		$line =~ s|;([fh][lcr]);|<endtag>\n<$1xxx0>|g;		#fl, fc, fr, hl, hc, & hr...
		$line =~ s|;([fh][lcr]);|;$1$2xxx0;|g;				#fl, fc, fr, hl, hc, & hr...
		$line =~ s|;([fh][lcr])xxx(0);|<$1$2>|g;			#fl, fc, fr, hl, hc, & hr...
		
		$line =~ s|<mh;0>|\n<?xpp mh;0?>|g;				# Will this matter with <pick.../>???
		$line =~ s|<foldout>|\n<foldout0>|g;
		
		#underline stuff AFTER FONT VARIANT STUFF ABOVE...
		$line =~ s|;ul;;nph;(.+);\\nph;;\\ul;|<rule width=\'1\' width_units=\'inches\' weight=\'.5\' weight_units=\'points\'\/>|g;
		
#		LEXIS STUFF...
		$line =~ s|;hnlexis;|;hn0;|g;
#		END LEXIS STUFF.		

		$line =~ s|<setall;(.+)>|<?xpp setall;$1?>|g;
		$line =~ s|<setjstyle;(.+)>|<?xpp setjstyle;$1?>|g;
		
# These are NOT legal GenCode tags...		
#		$line =~ s|;ul;0;|</ul>|g;	
#		$line =~ s|;ul;(\d*+);\-(\d*+)(.?[qpcdimknuz]);|<ul fmt="xpp0$1" offset="-$2$3">|g;	
#		$line =~ s|;ul;(\d*+);(\d*+)(.?[qpcdimknuz]);|<ul fmt="xpp0$1" offset="$2$3">|g;	
#		$line =~ s|;ul;(\d*+);|<ul fmt="xpp0$1">|g;

		$line =~ s|;ul=(\d*+);|<ul;$1>|g;
		$line =~ s|;ul;|<ul fmt="single">|g;
		$line =~ s|;\\ul;|</ul>|g;	

		# XPP:  arg1 is the 'underline-style' of 1..10;  offset is to right by amount or left by (neg) amount
		$line =~ s|<ul;0>|</ul>|g;			
		$line =~ s|<ul;(\d*+);\-(\d*+)(.?[qpcdimknuz])>|<ul fmt="xpp0$1" offset="-$2$3">|g;	
		$line =~ s|<ul;(\d*+);(\d*+)(.?[qpcdimknuz])>|<ul fmt="xpp0$1" offset="$2$3">|g;	
		$line =~ s|<ul;(\d*+)>|<ul fmt="xpp0$1">|g;	
		$line =~ s|<ul>|<ul fmt="single">|g;
		$line =~ s|<\\ul>|</ul>|g;
		
		# fix fmt="xpp010"  ->  fmt="xpp10"...
		$line =~ s|fmt="xpp010"|fmt="xpp10"|g;
		
		# fix fmt="xpp01..03"...
		$line =~ s|fmt="xpp01"|fmt="single"|g;
		$line =~ s|fmt="xpp02"|fmt="strike"|g;
		$line =~ s|fmt="xpp03"|fmt="overrule"|g;
		$line =~ s|fmt="xpp04"|fmt="double"|g;
		
		$line =~ s|<span>|\n<?xpp pc;span?>|g;
		$line =~ s|<xspan>|\n<?xpp pc;normal?>|g;
		$line =~ s|;span;|\n<?xpp pc;1?>|g;
		$line =~ s|;xspan;>|\n<?xpp pc;normal?>|g;

		$line =~ s|<spandc>|\n<?xpp pc;span?>|g;
		$line =~ s|<xspandc>|\n<?xpp pc;normal?>|g;
		$line =~ s|;spandc;|\n<?xpp pc;1?>|g;
		$line =~ s|;xspandc;>|\n<?xpp pc;normal?>|g;
		
		$line =~ s|<(pc);(\d*+)>|\n<?xpp $1;$2?>|g;
		$line =~ s|<(pc;normal)>|\n<?xpp $1?>|g;
		$line =~ s|<(pc;span)>|\n<?xpp $1?>|g;
		
		$line =~ s|;Paoh5;|;aoh5;|g;	
			
		if($line !~ /<\!--(.+)-->/)	{	#Does NOT contain an XML comment...
			$line =~ s|--|\x{2014}|g;	# dash, dash converted to emdash character utf8		
		}

		#20100923...
		#		$line =~ s|;\\fn;|<endtag>\n\n</fn$fnLevel2Close>\n|g;
		
		# Set FLAG if line contains an aoh#...	 20100923
		if($line =~ /aoh\d/)	{
			$in_aoh = 1;	
		}
		
		# Set FLAG if line contains an aoh# AND a footnote anchor...	 20100923
		if(	($in_aoh > 0)  and  ($line =~ /anchor;/)		)	{
			$in_aoh_anchor = 1;
		}
		
		#20100923...
		if(	($in_aoh_anchor > 0) and ($line =~ /;\\fn;/)	)	{
			# aoh# with footnote, so, Do NOT print the \'endtag\' stuff. 
			$line =~ s|;\\fn;|\n</fn$fnLevel2Close>\n|g;	
			$in_aoh=0;
			$in_aoh_anchor=0;
		} else {
			$line =~ s|;\\fn;|<endtag><!-- A -->\n\n</fn$fnLevel2Close>\n|g;	
		}		
		
		$line =~ s|<Trcont;(\d*+);(\d*+);(\d*+)>|<Tr;$1;$2>|g;
		$line =~ s|<Trcont;(\d*+);;(\d*+)>|<Tr;$1>|g;
		$line =~ s|<Trcont;;(\d*+);(\d*+)>|<Tr;;$1>|g;
		$line =~ s|<Trcont;;;(\d*+)>|<Tr>|g;
		$line =~ s|<endcont>|<?xpp endcont?>|g;		
				
		if( $line =~/<headon/ )								{ p1_headon($line); }
		
		elsif(
				($line =~ /^([ \t]*)<frilsize;/)					|				
				($line =~ /^([ \t]*)<layout;/)						|
				($line =~ /^([ \t]*)<mainsize;/)					|
				($line =~ /^([ \t]*)<notesize;/)					|
				($line =~ /^([ \t]*)<prefolio;/)					|	
				($line =~ /^([ \t]*)<setguide;/)					|
				($line =~ /^([ \t]*)<setllpg;/)						|
				($line =~ /^([ \t]*)<tablsize;/)										
																	)	{ p1_Std2XML($line); }
			
		elsif(
				($line =~ /^([ \t]*);aoh\d;/)						|
				($line =~ /^([ \t]*);aaoh\d;/)						|
				($line =~ /^([ \t]*);aoxh\d;/)						|				
				($line =~ /^([ \t]*)<[fh][lcr]\d>/)					|
				($line =~ /^([ \t]*)<foldout\d>/)					|
				($line =~ /^([ \t]*)<interp(\d)_0>/)				|
				($line =~ /^([ \t]*);oh\d;/)						|
				($line =~ /^([ \t]*);oha\d;/)						|
				($line =~ /^([ \t]*);oxh\d;/)						|
				($line =~ /^([ \t]*);hg(\d+)-\d;/)					|
				($line =~ /^([ \t]*);hn\d;/)						|
				($line =~ /^([ \t]*);rn\d;\d;/)						|
				($line =~ /^([ \t]*);rn\d;/)						|
				($line =~ /^([ \t]*);p\d;/)							|
				($line =~ /^([ \t]*);ol\d;/)						|
				($line =~ /^([ \t]*);olhead\d;/)					|
				($line =~ /^([ \t]*);bhead\d;/)						|
				($line =~ /^([ \t]*);ml\d;/)						|
				($line =~ /^([ \t]*);b\d;/)							|
				($line =~ /^([ \t]*);hg\d;/)						|				
				($line =~ /^([ \t]*)<note>/)						|
				($line =~ /^([ \t]*);fn;/)							
																		)	{ p1_OpenContainerTag($line); }
																
		elsif(
				($line =~ /^([ \t]*)\<inset/)						|
				($line =~ /^([ \t]*)\<pick;/)						|
				($line =~ /^([ \t]*)\{\/PICK;/)						|
				($line =~ /^([ \t]*)\{\/GRAPH;/)					|
				($line =~ /^([ \t]*)\{\/CAPT;/)						|
				($line =~ /^([ \t]*)\{\/TITL;/)						|	
				($line =~ /^([ \t]*)\{\/ANNT;/)			
																		)	{	p1_images($line);	}
								
		else {	 			
	 			
				if($PreviousLine =~ /;olhead\d;/) {					
					# Cleanup bad GenCode tagging... (adding missing 2nd '\' and providing ;en; if necessary.)
					if($PreviousLine =~ /;olhead\d/) {				
						if($line =~ /(.*)\\$/) {
							$line .= ";en;";		# Add some data; don't let the line simply end with a back-slash.
							#print STDOUT "$line\n\n";
						}
						
						if($line =~ /(.*)(.[^\\])$/) {
							$lineTemp = $line;	
							@aX = $lineTemp =~ /\\/g;
							$count = @aX;	
							if($count < 2) {
								$line .= "\\;en;\n";	
								#print STDOUT "$line\n\n";
							}			
						}	
					}
				}	
				
				$line = p1_SingleTags($line); 			
	 			
				print(OUT "$line\n"); # echo line read
		}
			
		$ctr++;
		$PreviousLine = $line;
		


	}	# end while...
	
	$ctr++;
	$tot += $ctr;
	$gtot += $tot;
	$tot=0;
	
	close(FILE);
	close(OUT);

	
#---------------------------------------------------------------------------------------------------
# PASS-TWO PASS-TWO PASS-TWO PASS-TWO PASS-TWO PASS-TWO PASS-TWO PASS-TWO
#---------------------------------------------------------------------------------------------------
	if($promptLevels > 0)	{
		print STDOUT "\nPausing Before Pass-Two\n";	
		$x = &ReadKey;
	}

	my $infile2 = $outfile;		# PASS-TWO input file name...
	
	open(FILE, '<:encoding(utf8)', $infile2)
		|| die "cannot open input file: $infile2";
	
	$ctr = 1;
	$outfile = ($infile . $ext2);		# create PASS-TWO output file...
	
	open(OUT, ">:utf8", $outfile)
		|| die "cannot open file for output:  $outfile";

	while(<FILE>) {
		 my($line) = $_;
		chomp($line);			# Strip the trailing newline from the line.	
					
		if(/^<table/)		{ p2_OpenTable(); }			# 20100806.1 

		if (/<endtag>/)	{ 
			p2_CloseContainerTag();	
		} elsif(/^<\/table/) {							# 20100806.1
			
			if($footnotePending ne "yes")	{			
				print(OUT "$line\n\n");
			} 
			
			p2_CloseTable(); 
		} 
							
		elsif(				
		 			(/^<banner/)					| 	
					(/^<begtake/)					| 	
		 			(/^<binder/)					| 	
					(/^<\!-- inset/)				|
					(/^<conthpg/)					|
		 			(/^<cover/)						| 	
		 			(/^<double/)					| 	
		 			(/^<endtake/)					| 	
		 			(/^<foldoutmap/)				| 	
		 			(/^<frilsize/)					|
					(/^<head>/)						|
		 			(/^<ixhead/)					| 	
		 			(/^<ixpage/)					| 	
		 			(/^<layout/)					|
		 			(/^<layout>legal/)				| 	
					(/^<main/)						|
					(/^<maindc>/)					|
					(/^<mainsize/)					|
					(/^<notesize/)					|
		 			(/^<npfnsp/)					| 	
		 			(/^<npxh/)						| 	
		 			(/^<nrhp/)						| 	
		 			(/^<nrhpfnsp/)					| 	
		 			(/^<nrhpR90/)					| 	
		 			(/^<nrhpxh/)					| 	
		 			(/^<prehead/)					| 	
		 			(/^<restore/)					| 	
		 			(/^<rotatepg/)					| 	
		 			(/^<rowgut/)					|
		 			(/^<secanal/)					| 	
		 			(/^<setall/)					|
		 			(/^<setbrul/)					|
					(/^<setbgut/)					|
					(/^<setcol/)					|
		 			(/^<setguide/)					| 	
		 			(/^<setllpg/)					| 	
		 			(/^<setpage/)					| 	
					(/^<setrh;/)					|
		 			(/^<setskipl/)					| 	
		 			(/^<setstyle/)					|
		 			(/^<single/)					| 	
		 			(/^<tabs/)						| 	
		 			(/^<tablsize/)					|
#		 			(/^<toc/)						| 	
		 			(/^<tochead/)					| 	
		 			(/^<tocpage/)					|
		 			(/^<prefolio/)										 	
												)			{ p2_remove($line); } 
 			
			elsif(/^([ \t]*)<interp\d_0>/)					{ p2_CTTag($line); }

			elsif(/^([ \t]*)<oh\d>/)						{ p2_OhTag($line); }
			elsif(/^([ \t]*)<oxh\d>/)						{ $line =~ s|<oxh|<oh|g;		p2_OhTag($line); } #convert to oh first...
 			elsif(/^([ \t]*)<b\d>/)							{ p2_CTTag($line); }
			elsif(/^([\t]*)<[fh][lcr]\d>/)					{ p2_CTTag($line); }
 			elsif(/^([ \t]*)<foldout\d>/)					{ p2_CTTag($line); }	 
 			elsif(/^([ \t]*)<p\d>/)							{ p2_CTTag($line); }	 			
 			elsif(/^([ \t]*)<hg\d>/)						{ p2_CTTag($line); }	 			
 			elsif(/^([ \t]*)<hg(\d+)-\d>/)					{ p2_CTTag($line); }	
 			elsif(/^([ \t]*)<ol\d>/)						{ p2_CTTag($line); }
 			elsif(/^([ \t]*)<olhead\d>/)					{ p2_CTTag($line); } 			
 			elsif(/^([ \t]*)<bhead\d>/)						{ p2_CTTag($line); } 			
 			elsif(/^([ \t]*)<ml\d>/)						{ p2_CTTag($line); }		
 			elsif(/^([ \t]*)<hn\d>/)						{ p2_CTTag($line); }
 			 			
 			elsif(/^([ \t]*)<rn\d;\d>/)		{ 
	 															$line =~ s|<rn(\d);(\d)>|<rn$2;$1>|g;	
 																if($G_OpenTableFlag eq "no")	{	
																	print OUT "$line\n";
																} else {
																	push @G_TableFootnotes, $line;
																}	 															
	 														}	
 			elsif(/^([ \t]*)<note>/)						{ 
	 															$line =~ s|<note>|<rn0;0>|g;
 																if($G_OpenTableFlag eq "no")	{	
																	print OUT "$line\n";
																} else {
																	push @G_TableFootnotes, $line;
																}
															}	
 			elsif(/^([ \t]*)<rn\d>/)							{ 
	 															$line =~ s|<rn(\d)>|<rnm$1>|g;	
 																if($G_OpenTableFlag eq "no")	{	
																	print OUT "$line\n";
																} else {
																	push @G_TableFootnotes, $line;
																}	 															
	 														}	
 			elsif(/^([ \t]*)<fn\d+>/)			{	# Open Footnote tag...
																$footnotePending="yes";
 																if($G_OpenTableFlag eq "no")	{	
																	print OUT "$line\n";
																} else {
																	push @G_TableFootnotes, $line;
																}
	 														}
 			elsif(/^([ \t]*)<\/fn\d+>/)		{	# Close Footnote tag.
	 															p2_ClosePendingFootnoteTags($line); 
																$footnotePending = "no";
	 															
 																if($G_OpenTableFlag eq "no")	{	
		 															print OUT "</footnote>\n"; 
																} else {
																	push @G_TableFootnotes, "\n</footnote>\n";
																}	 															
	 														}
 			elsif(/^([ \t]*)<aoh\d>/)			{ p2_AOHTag($line); }
 			elsif(/^([ \t]*)<aaoh\d>/)		{ p2_AAOHTag($line); }
 			elsif(/^([ \t]*)<aoxh\d>/)		{ p2_AOXHTag($line); }			

			elsif(/^([ \t]*)<\/XML/)			{ CloseOpenStackEntries();		print(OUT "$line\n"); }
			
			else {						
				if(not($footnotePending eq "yes" and $G_OpenTableFlag eq "yes") )	{
					print(OUT "$line\n"); 
				} else {
					push @G_TableFootnotes, "$line\n";
				}
			}	
	
		$ctr++;
	}	# Read next line...
				
	$tot += $ctr;
	$gtot += $tot;
	$tot=0;
	
	close(FILE);
	close(OUT);	
	
#---------------------------------------------------------------------------------------------------
# PASS-THREE PASS-THREE PASS-THREE PASS-THREE PASS-THREE PASS-THREE 
#---------------------------------------------------------------------------------------------------
	if($promptLevels > 0)	{
		print STDOUT "\nPausing Before Pass-Three\n";	
		$x = &ReadKey;
	}

	my $removeFlag = 0;
	my $firstRowFlag = 0;

	my $infile3 = $outfile;			# PASS-THREE input file name...
	open(FILE, '<:encoding(utf8)', $infile3)		# flock(IN, OPERATION) ???
		|| die "cannot open input file: $infile3";
	
	$ctr = 1;
	$outfile = ($infile . $ext3);		# create PASS-THREE output file...
	
	open(OUT, ">:utf8", $outfile)
		|| die "cannot open file for output:  $outfile";

	while(<FILE>) {
		 my($line) = $_;
		chomp($line);			# Strip the trailing newline from the line.
		
	     if	( $line =~ /<REMOVE>/)			{	$removeFlag++;	}
		elsif($line =~ /<\/REMOVE>/)		{	$removeFlag--;	}
		elsif($removeFlag == 0)				{	
			print(OUT "$line\n");		
		} #end elsif
		
		$ctr++;
	}	# Read next line...
					
	close(FILE);
	close(OUT);	
	
	$tot += $ctr;
	$gtot += $tot;
	$tot=0;	
	
#---------------------------------------------------------------------------------------------------
# PASS-FOUR PASS-FOUR PASS-FOUR PASS-FOUR PASS-FOUR PASS-FOUR PASS-FOUR
#---------------------------------------------------------------------------------------------------
	if($promptLevels > 0)	{
		print STDOUT "\nPausing Before Pass-Four\n";	
		$x = &ReadKey;
	}

	my $infile4 = $outfile;				# PASS-FOUR input file name...

	open(FILE, '<:encoding(utf8)', $infile4)		# flock(IN, OPERATION) ???
		|| die "cannot open input file: $infile4";
	
	$ctr = 1;	
	$outfile = ($infile . $ext4);		# create PASS-FOUR output file...
	$footnotePending = "no";
	
	open(OUT, ">:utf8", $outfile)
		|| die "cannot open file for output:  $outfile";

	while(<FILE>) {
		 my($line) = $_;
		chomp($line);			# Strip the trailing newline from the line.
		
		# TABLE...				
		if($line =~ /^<table/) {		# Found a table...
			$line = p4_CALS($line);				
		} 
		
		#---------------------------------------------------
		# BEGIN:  San Francisco 14139 interp6/8 stuff...
		#---------------------------------------------------
		if($line =~ /<interp\d_0/)	{
			$line=readline(FILE);
			chomp($line);
			
			$lineTemp = $line;
			@interpPart=();
			@interpPart = split(/\\/, $lineTemp);

			$line=readline(FILE);
			chomp($line);
			
			while($line !~ /<\/interp\d_0/)	{
				$interpPart[$#interpPart] = $interpPart[$#interpPart] . $line;
				$line=readline(FILE);
				chomp($line);
			}
						
			$lineTemp = "";
			
			for(my $i=0; $i <=$#interpPart; $i++)	{
					
				if($i % 2 == 0)	{	
					$lineTemp = $lineTemp . "<interpA0>\n$interpPart[$i]\n<\/interpA0>\n\n";
				} else {
					if($i == $#interpPart)	{
						$lineTemp = $lineTemp . "<interpC0>\n$interpPart[$i]\n<\/interpC0>\n\n";
					} else {
						$lineTemp = $lineTemp . "<interpB0>\n$interpPart[$i]\n<\/interpB0>\n\n";
					}
				}
			}
			
			$lineTemp =~ s|(<b\d>)|\n\n$1|g;
			$lineTemp =~ s|(<\/b\d>)|\n$1\n\n|g;
			$lineTemp =~ s|(<p\d>)|\n\n$1|g;
			$lineTemp =~ s|(<\/p\d>)|\n$1\n\n|g;
			$lineTemp =~ s|(<hg\d>)|\n\n$1|g;
			$lineTemp =~ s|(<\/hg\d>)|\n$1\n\n|g;
			$lineTemp =~ s|(<hg\d+\-\d)|\n\n$1|g;
			$lineTemp =~ s|(<\/hg\d+\-\d>)|\n$1\n\n|g;
			
			
			$lineTemp =~ s|<comment note='pickup'|\n\n<comment note='pickup'|g;
			$lineTemp =~ s|<\!-- <pick;|\n<!-- <pick;|g;
			$lineTemp =~ s|<\!-- \{\/PICK;|\n<\!-- \{\/PICK;|g;
			$lineTemp =~ s|<\!-- \{\/GRAPH;|\n<\!-- \{\/GRAPH;|g;
			$lineTemp =~ s|<\!-- \{\/CAPT;|\n<\!-- \{\/CAPT;|g;
			$lineTemp =~ s|<\!-- \{\/TITL;|\n<\!-- \{\/TITL;|g;
			$lineTemp =~ s|<\!-- \{\/ANNT;|\n<\!-- \{\/ANNT;|g;
			$lineTemp =~ s|<\/comment\>|\n<\/comment\>\n|g;

			print(OUT "$lineTemp\n");	
		}
		#---------------------------------------------------
		# END:  San Francisco 14139 interp6/8 stuff...
		#---------------------------------------------------
		if( not($line =~ /<\/interp\d_0/) ) {
			print(OUT "$line\n");
		}
		
		$ctr++;
	}	# Read next line...
					
	close(FILE);
	close(OUT);	

	
	$tot += $ctr;
	$gtot += $tot;
	$tot=0;

#---------------------------------------------------------------------------------------------------
# PASS-FIVE PASS-FIVE PASS-FIVE PASS-FIVE PASS-FIVE PASS-FIVE PASS-FIVE PASS-FIVE
#---------------------------------------------------------------------------------------------------
	if($promptLevels > 0)	{
		print STDOUT "\nPausing Before Pass-Five\n";	
		$x = &ReadKey;
	}

	our $infile5 = $outfile;		# PASS-FIVE input file name...
	$p4_ContainingElement = "";
	$footnoteSeq=1;
	
	open(FILE, '<:encoding(utf8)', $infile5)							# flock(IN, OPERATION) ???
		|| die "cannot open input file: $infile5";
	
	$ctr = 1;
	$outfile = ($infile . $ext5);		# create PASS-FIVE output file...
	
	open(OUT, ">:utf8", $outfile)
		|| die "cannot open file for output:  $outfile";

	while(<FILE>) {
		 $line = $_;
		chomp($line);			# Strip the trailing newline from the line.
		
		if($line =~ /<setnc;(\d*+)>/)	{	# no longer need the <setnc;...> line
			$line=readline(FILE);
			chomp($line);
		}
		
		if(!monetary_set_flags($line))	{	
			print STDOUT "+++ ERROR with 'monetary table' flag setting.\n";
		}
		
		if(	($line =~ /^([ \t]*)<p\d>/) |
			($line =~ /^([ \t]*)<b\d>/) |
			($line =~ /^([ \t]*)<hg\d>/) |
			($line =~ /^([ \t]*)<hg\d-\d>/) |
			($line =~ /^([ \t]*)<hm\d>/) |
			($line =~ /^([ \t]*)<bhead\d>/) |
			($line =~ /^([ \t]*)<ol\d>/) |
			($line =~ /^([ \t]*)<olhead\d>/)	
			) {
				$gclevel=$line;
				$gclevel =~ s|^([ \t]*)<(.+?)(\d)>(.+?)$|<$2$3>|g;
				$gclevel =~ s|<(.+?)(\d)>|gclevel=\"$2\"|g;
		}
				
		if($line =~ /<[fh][lcr]0>/)	{
			$line =~ s|<[fh]l0>|<p0 justify=\'left\' block_type=\'block\'>|g;
			$line =~ s|<[fh]c0>|<p0 justify=\'center\' block_type=\'block\'>|g;
			$line =~ s|<[fh]r0>|<p0 justify=\'right\' block_type=\'block\'>|g;
		}

		if($line =~ /<\/[fh][lcr]0>/)	{
			$line =~ s|<\/[fh][lcr]0>|<\/p0>|g;
		}		
		
		$line =~ s|<foldout0>|\n<?xpp foldout?>\n|g;
		$line =~ s|<\/foldout0>|\n|g;	
 			
		if($line =~ /^([ \t]*)<XML>/)				{  }
		elsif($line =~ /^([ \t]*)<book>/)			{ p5_BookTag(); }
		elsif($line =~ /^([ \t]*)<oh\d>/)			{ p5_OHTag($infile, $line); }
		elsif($line =~ /^([ \t]*)<\/oh\d>/)			{ p5_CloseOHTag($line); }
		
		elsif($line =~ /^([ \t]*)<b\d>/)			{
			if($monetaryFlag) {
				$line =~ s|b\d|para block_type=\'block\' monetary=\"yes\" $gclevel|g; 
			} else {
				$line =~ s|b\d|para block_type=\'block\' $gclevel|g; 
			}
			
			print(OUT "$line\n"); 
		}	 			
		
		elsif($line =~ /^([ \t]*)<\/b\d>/)			{ $line =~ s|b\d|para|g;	p5_FootnotePending($line);	}	 	
		elsif($line =~ /^([ \t]*)<interpA0>/)		{ $line =~ s|interpA0|para block_type=\'block\' meta1='interpA'|g;	print(OUT "$line\n"); }	 			
		elsif($line =~ /^([ \t]*)<\/interpA0>/)		{ $line =~ s|interpA0|para|g;	p5_FootnotePending($line);	}
		elsif($line =~ /^([ \t]*)<interpB0>/)		{ $line =~ s|interpB0|para block_type=\'block\' meta1='interpB'|g;	print(OUT "$line\n"); }	 			
		elsif($line =~ /^([ \t]*)<\/interpB0>/)		{ $line =~ s|interpB0|para|g;	p5_FootnotePending($line);	}	 	
		elsif($line =~ /^([ \t]*)<interpC0>/)		{ $line =~ s|interpC0|para block_type=\'block\' meta1='interpC'|g;	print(OUT "$line\n"); }	 			
		elsif($line =~ /^([ \t]*)<\/interpC0>/)		{ $line =~ s|interpC0|para|g;	p5_FootnotePending($line);	}	 	
		elsif($line =~ /^([ \t]*)<hg\d>/)			{ $line =~ s|hg\d|para block_type=\'hang\'  $gclevel|g;	print(OUT "$line\n");  }
		elsif($line =~ /^([ \t]*)<\/hg\d>/)			{ $line =~ s|hg\d|para|g;	  p5_FootnotePending($line);	}
					 	
		elsif($line =~ /^([ \t]*)<hm\d>/)			{ $line =~ s|hm\d|para block_type=\'hangem\'  $gclevel|g;	print(OUT "$line\n");  }
		elsif($line =~ /^([ \t]*)<\/hm\d>/)			{ $line =~ s|hm\d|para|g;	  p5_FootnotePending($line);	}
		
		elsif($line =~ /^([ \t]*)<hg(\d+)-\d>/)		{		
			if($monetaryFlag) {
				$line =~ s|hg(\d+)-\d|para meta3=\"indent$1\" block_type=\'hang\' monetary=\"yes\"  $gclevel|g; 
			} else {
				$line =~ s|hg(\d+)-\d|para meta3=\"indent$1\" block_type=\'hang\'  $gclevel|g; 
			}
			
			print OUT "$line\n";  
		}
		
		elsif($line =~ /^([ \t]*)<\/hg(\d+)-\d>/)	{ $line =~ s|hg(\d+)-\d|para|g;	  p5_FootnotePending($line);	}
		
		elsif($line =~ /^([ \t]*)<p\d/)				{ $line =~ s|p\d|para  $gclevel|g;	print(OUT "$line\n"); }
		elsif($line =~ /^([ \t]*)<\/p\d>/)			{ $line =~ s|p\d|para|g;	p5_FootnotePending($line); }
		elsif($line =~ /^([ \t]*)<ol\d>/)			{ p5_OLTag($line); }
		elsif($line =~ /^([ \t]*)<\/ol\d>/)			{ $line =~ s|<(\/ol\d)>|</listitem>|g;	 p5_FootnotePending($line); }
		elsif($line =~ /^([ \t]*)<olhead\d>/)		{ p5_OLheadTag($line); }
		elsif($line =~ /^([ \t]*)<\/olhead\d>/)		{ $line =~ s|<(\/olhead\d)>|</listitem>|g;	p5_FootnotePending($line);  }
		elsif($line =~ /^([ \t]*)<bhead\d>/)		{ p5_BheadTag($line); }
		elsif($line =~ /^([ \t]*)<\/bhead\d>/)		{ $line =~ s|<(\/bhead\d)>|</para>|g;	p5_FootnotePending($line);  }
		
		elsif($line =~ /^([\t]*)<fn\d+>/)			{ p5_FootnoteTag($line);  }
		elsif(	$line =~ /^([ \t]*)<\/fn\d+>/)  	{ }
		elsif($line =~ /^<\/footnote>/) 			{ $footnotePending = "no";  print OUT "$line\n";}
		elsif($line =~ /^([ \t]*)<ml\d>/)			{ p4_MLTag($line); }
		elsif($line =~ /^([ \t]*)<\/ml\d>/)			{ $line =~ s|ml\d|listitem|g;	p5_FootnotePending($line);	}	
		elsif($line =~ /^([ \t]*)<hn\d>/)			{ p4_HNTag($line); }
		elsif($line =~ /^([ \t]*)<\/hn\d>/)			{  }
		elsif($line =~ /^([ \t]*)<rn\d;\d>/)		{ p4_RNTag($line);  }
		elsif($line =~ /^([ \t]*)<\/rn\d>/)			{  }	
		elsif($line =~ /^([ \t]*)<rnm\d>/)			{  p4_RNTag($line); }
		elsif($line =~ /^([ \t]*)<\/rnm\d>/)		{  }
		elsif($line =~ /^([ \t]*)<\/XML>/)			{  }
		
		else {	
			if($monetaryFlag) {
				$line = monetary_wrap($line);
			}
			
			print OUT "$line\n";
		}	
	
		$ctr++;
	}	# Read next line...

	close(FILE);
	close(OUT);	
					
	$tot += $ctr;
	$gtot += $tot;
	$tot=0;
	
#---------------------------------------------------------------------------------------------------
# PASS-SIX PASS-SIX PASS-SIX PASS-SIX PASS-SIX PASS-SIX PASS-SIX			
#---------------------------------------------------------------------------------------------------

if($promptLevels > 0)	{
	print STDOUT "\nPausing Before Pass-Six\n";	
	$x = &ReadKey;
}


#	MONETARY TABLE PROCESSING for para's...
	my $infile6 = $outfile;				# PASS-SIX input file name...

	open(FILE, '<:encoding(utf8)', $infile6)		# flock(IN, OPERATION) ???
		|| die "cannot open input file: $infile6";
	
	$ctr = 1;	
	$outfile = ($infile . $ext6);		# create PASS-SIX output file...

	open(OUT, ">:utf8", $outfile)
		|| die "cannot open file for output:  $outfile";

	while(<FILE>) {
		 my($line) = $_;
		chomp($line);			# Strip the trailing newline from the line.
		
		# <?xpp ...&amp;...?>	- converting the &amp; to '&'
		$line =~ s|<\?xpp mb;&amp;|<?xpp mb;&|g;
		
		# pass-six: blocks beginning with <?xpp lz?> -> @class="lz"
		# $hg_bodyA = "";
		# $hg_bodyB = "";
		
		# NOTICE: The following BREAKS creation of the <img../> elements for <ig;...> GenCode tags!
		# if($line =~ /^<para /) {
			# $hg_bodyA = $line;			# just saving the "<para ..." line in a scalar
			# $line = <FILE>;
			# chomp($line);
			
			# if($line =~ /^<\?xpp lz\?>/ ) {		
				# $hg_bodyA =~ s|^<para |<para meta5=\"lz\" |g;
				# print OUT $hg_bodyA . "\n";				
				# $hg_bodyB = $line;
				# $hg_bodyB =~ s|^<\?xpp lz\?>(.*?)$|$1|g;
				# print OUT $hg_bodyB . "\n";
			# } else {
				# print OUT $hg_bodyA . "\n";
				# print OUT $line . "\n";
			# }
			
			# next;
		# }		
				
		if($line !~ /<comment note=\'monetary/)	{	#NOT MONETARY TABLE...			
			#==============================
			#  Process images...
			#==============================
			if(		($line =~ /<comment note=\'pickup\'>/)
				|	($line =~ /<comment note=\'ig\'>/)
				|	($line =~ /<comment note=\'igt\'>/)
				) {
					
				if($line =~ /<comment note=\'pickup\'>/)	{	# pickup...
					$str = $line;			# save contents of $line...
					$line=readline(FILE);	#read line after note='pickup'...
					chomp($line);

					while($line !~ /<\/comment>/)	{
						$str = $str . "|" . $line;				# concatenate $line to $str...							
						$line=readline(FILE);
						chomp($line);
					}

					getImgData($str);

					# break out lines from $str...
					@part = split(/\|/, $str);
					 
					foreach my $pickup_line (@part)	{
						print OUT "$pickup_line\n";
					}

				} else {	# ig or igt...				
					$str = $line;			# save contents of $line...
					
					# NOTE:  The $line may have multiple <comment note='ig'>...</comment> elements
					$line = getImgData($str);
					#print OUT "$str\n";					
				}
			}
			#==============================
			#  END Process images...
			#==============================	
						
			print (OUT "$line\n");
		}
		
		$ctr++;
	}	# Read next line...
					
	
	close(FILE);
	close(OUT);
	
	$tot += $ctr;
	$gtot += $tot;
	$tot=0;


#---------------------------------------------------------------------------------------------------
# PASS-SEVEN PASS-SEVEN PASS-SEVEN PASS-SEVEN PASS-SEVEN PASS-SEVEN PASS-SEVEN
#---------------------------------------------------------------------------------------------------
if($promptLevels > 0)	{
	print STDOUT "\nPausing Before Pass-Seven\n";	
	$x = &ReadKey;
}

# if($#aImageNameInvalid > -1) {
	# print "\n$dashes\nERROR in FILE:  $infile\n\nInvalid Image File Names:\n";
	# for($x=0; $x<=$#aImageNameInvalid; $x++) {
		# print STDOUT "\t[" . ($x+1) . "] " . $aImageNameInvalid[$x] . "\n";
	# }	
	
	# print STDOUT "\n\nPlease rename the image(s) and correct the corresponding GenCode to match the new image name.\n";
	# print STDOUT "\nThe ONLY valid characters in an image name are: \n underscore, dash, a-z, A-Z, 0-9, and only a single '.' before the file extension ex. '.png'.\n\n";
	# print STDOUT "$dashes\a\n\a\n\a\n";
	# exit 0;	
# }

#	Building <anchor fnid="???"/> elements...  			(NOT USED after 20100813)
#	Building <footnoteref linkend="???"/> elements...	(added 20100813)

	my $infile7 = $outfile;				# PASS-SEVEN input file name...
	my $buffer = "";
	my $newAnchorLine = "";
	my $fnid = "";
	my $fnMark = "";
	my $fnSeq = 1;
	my $fnSeq_MultiAnchors=0;
	my $myLinkend="";
	my $Rfnid = "";
	my $anchorMark="";
	my @anchorType=();
	my @supAnchor=();
	my $fnSupAnchor="";
	my $sMark = "";

	open(FILE, '<:encoding(utf8)', $infile7)		# flock(IN, OPERATION) ???
		|| die "cannot open input file: $infile7";
	
	$ctr = 1;	
	$outfile = ($infile . $ext7);		# create PASS-SEVEN output file...

	open(OUT, ">:utf8", $outfile)
		|| die "cannot open file for output:  $outfile";

	while(<FILE>) {
		 my($line) = $_;
		chomp($line);			# Strip the trailing newline from the line.
		$ctr++;
		
		#NOTE (2010-09-24):
		# NOW it seems that the following "fact" is no longer true.  Paragraphs MAY have multiple anchors in them.  Makes sense to me!
		
		# NOTE (2009-01-23):  [WRONG!!! AS OF 2010-09-24] For conversion purposes, there should never be a line with two footnote anchors in it.  
		# Therefore, the $line will not be split before processing per design discussion 2009-01-23.
			
		# Locate <?xpp anchor?>...
		if(	($line =~ /<\?xpp anchor\?>/) 		| 
			($line =~ /<\?xpp supanchor\?>/) 	| 
			($line =~ /<\?xpp noanchor\?>/)		
			)	{
				
			# Per discussion with LaRae 20100830_0950...
			# changing supanchor tags to plain anchor tags.
			$line =~ s|xpp supanchor|xpp anchor|g;

			# 2010-09-24 - Allow for MULTIPLE anchors in a given paragraph...
			$fnSeq_MultiAnchors = 0;
			$line =~ s|(<\?xpp anchor\?>)|'~~~'$1|g;
			$line =~ s|(<\?xpp noanchor\?>)|'~~~'$1|g;
			@Astr=();
			@Astr = split /'~~~'/, $line;	
			
			#2010-09-24...
			foreach $str (@Astr)	{
				$anchorMark = $fnSeq + $fnSeq_MultiAnchors -1;
				$myLinkend = $footnoteIDprefix . "_" . ($fnSeq + $fnSeq_MultiAnchors -1);
				$Rfnid = "R" . $myLinkend;
				
				if($str =~ /<\?xpp noanchor\?>/)	{
					$sMark = "";
					$fnMark = "noanchor";
					push @anchorType, $fnMark;
					push @supAnchor, ($fnSeq + $fnSeq_MultiAnchors);

					if($str =~ /(<\?xpp noanchor\?>)<(.+?)><sup>(.+?)<\/sup>(.+?)/) {
						$sMark = $str;
						$sMark =~ s|^(<\?xpp noanchor\?>)<(.+?)><sup>(.+?)<\/sup>(.+?)$|$3|g;
						$str =~ s|^(<\?xpp noanchor\?>)<(.+?)><sup>(.+?)<\/sup>(.+?)$|$1<$2>$4|g; 
					}
					
					elsif($str =~ /(<\?xpp noanchor\?>)<(.+?)><sup>(.+?)<\/sup>/) {
						$sMark = $str;
						$sMark =~ s|^(<\?xpp noanchor\?>)<(.+?)><sup>(.+?)<\/sup>$|$3|g;
						$str =~ s|^(<\?xpp noanchor\?>)<(.+?)><sup>(.+?)<\/sup>$|$1<$2>|g; 
					}
					
					elsif($str =~ /(<\?xpp noanchor\?>)<sup><(.+?)>(.+?)<\/(.+?)><\/sup>(.+?)$/) {
						$sMark = $str;
						$sMark =~ s|^(<\?xpp noanchor\?>)<sup><(.+?)>(.+?)<\/(.+?)><\/sup>(.+?)$|$3|g;
						$str =~ s|^(<\?xpp noanchor\?>)<sup><(.+?)>(.+?)<\/(.+?)><\/sup>(.+?)$|$1$5|g;
					}					

					elsif($str =~ /(<\?xpp noanchor\?>)<sup><(.+?)>(.+?)<\/(.+?)><\/sup>$/) {
						$sMark = $str;
						$sMark =~ s|^(<\?xpp noanchor\?>)<sup><(.+?)>(.+?)<\/(.+?)><\/sup>$|$3|g;
						$str =~ s|^(<\?xpp noanchor\?>)<sup><(.+?)>(.+?)<\/(.+?)><\/sup>$|$1|g;
					}					

					elsif($str =~ /(<\?xpp noanchor\?>)(<sup>)(.+?)<\/sup>(.+?)/) {
						$sMark = $str;
						$sMark =~ s|^(<\?xpp noanchor\?>)(<sup>)(.+?)(<\/sup>)(.+?)$|$3|g;
						$str =~ s|^(<\?xpp noanchor\?>)(<sup>)(.+?)(<\/sup>)(.+?)$|$1$5|g;
					}					

					elsif($str =~ /(<\?xpp noanchor\?>)(<sup>)(.+?)<\/sup>/) {
						$sMark = $str;
						$sMark =~ s|^(<\?xpp noanchor\?>)(<sup>)(.+?)(<\/sup>)$|$3|g;
						$str =~ s|^(<\?xpp noanchor\?>)(<sup>)(.+?)(<\/sup>)$|$1|g;
					}					

					$str =~ s|<\?xpp noanchor\?>|<footnoteref id=\"$Rfnid\" linkend=\"$myLinkend\" mark=\"$sMark\"\/>|g;
					
				} elsif($str =~ /<\?xpp supanchor\?>/) {
					$fnMark = "supanchor";
					push @anchorType, $fnMark;
					push @supAnchor, ($fnSeq + $fnSeq_MultiAnchors);
					$str =~ s|<\?xpp supanchor\?>|<footnoteref id=\"$Rfnid\" linkend=\"$myLinkend\" mark=\"$fnSeq\"\/>|g;
					
				} else {
					$fnMark = "anchor";	
					push @anchorType, $fnMark;
					push @supAnchor, ($fnSeq + $fnSeq_MultiAnchors);
					$str =~ s|<\?xpp anchor\?>|<footnoteref id=\"$Rfnid\" linkend=\"$myLinkend\" mark=\"$anchorMark\"\/>|g;			
				}
				
				$fnSeq_MultiAnchors++;

			}
			
#print OUT "\n\n<!-- DUMP \$fnSeq and \$fnSeq_MultiAnchors Values -->\n";
#print OUT "<!-- \$fnSeq=$fnSeq -->\n";
#print OUT "<!-- \$fnSeq_MultiAnchors=$fnSeq_MultiAnchors -->\n";
#						
#print OUT "\n<!-- AFTER DUMPING \@Astr\(\)... -->\n";					
#foreach $str (@Astr)	{
#	print OUT "<!-- $str -->\n";	
#}

			if($#Astr > -1)	{
				$line = "";
				foreach my $myL (@Astr)	{
					$line = $line . $myL;
				}
			}			
		
#			$fnSeq++;	
			if($fnSeq_MultiAnchors > 1)	{
				$fnSeq = $fnSeq + ($fnSeq_MultiAnchors - 1);
			} else {
				$fnSeq++;				
			}
		}			
		
		if($line =~ /<footnote id/)	{			
			$fnMark = shift @anchorType;
			$fnSupAnchor = shift @supAnchor;
									
			if( $fnMark eq "noanchor")	{		# It's a custom (noanchor) footnote and needs attribute:  mark=""
				$line =~ s|(.+?)(mark\=\")(.)(\">)|$1$2$4|g;	# if it's "noanchor" then the mark should be blank!!!
				
			} elsif( $fnMark eq "supanchor")	{		# It's an integer, sequential (supanchor) footnote and needs attribute:  mark=""
				$line =~ s|(<footnote)(.+?)(mark\=\")(.)(\")(.+?)|$1$2 mark=\"$fnSupAnchor\" $6|g;
				
			} else {
# 20100831	$line =~ s|(<footnote)(.+?)(mark\=\"\*\")(.+?)|$1$2 numbered=\"yes\" $4|g;
				$line =~ s|(<footnote)(.+?)(mark\=\"\*\")(.+?)|$1$2 numbered=\"yes\" $4|g;
# 20100831	$line =~ s|(<footnote)(.+?)(mark\=\"\*\")(.+?)|$1$2 mark\=\"$fnSupAnchor\" $4|g;
			}
			
		}

		# remove empty tags ex.  <bold></bold>...
		#$line =~ s|<(.+?)><\/(\1)>|<$1\/>|g;
		$line =~ s|<(bold)><\/(\1)>||g;
			
		print (OUT "$line\n");
		
	}	# Read next line...
					
	
	close(FILE);
	close(OUT);
	
	$tot += $ctr;
	$gtot += $tot;
	$tot=0;


#END PASS-SEVEN

#---------------------------------------------------------------------------------------------------
# PASS-EIGHT PASS-EIGHT PASS-EIGHT PASS-EIGHT PASS-EIGHT PASS-EIGHT PASS-EIGHT
#---------------------------------------------------------------------------------------------------
	if($promptLevels > 0)	{
		print STDOUT "\nPausing Before Pass-Eight\n";	
		$x = &ReadKey;
	}

	# <selectout;ebook;html;none;print>...</selectout> processing (OPENING TAG)
	my $infile8 = $outfile;				# PASS-EIGHT input file name...

	open(FILE, '<:encoding(utf8)', $infile8)		#
		|| die "cannot open input file: $infile8";
	
	$ctr = 1;	
	$outfile = ($infile . $ext8);		# create PASS-EIGHT output file...
	$temp8 = "";
	$str = "";
	$inSelectout = 0;
	
	open(OUT, ">:utf8", $outfile)
		|| die "cannot open file for output:  $outfile";

	while(<FILE>) {
		 $line = $_;
		chomp($line);			# Strip the trailing newline from the line.
		$ctr++;
		
		
		# Convert <?xpp backslash?> back to the '\' character, now that "monetary table" stuff is Finished
		$line =~ s|<\?xpp backslash\?>|\\|g;
		
		if($line =~ /^<selectout/)	{
			$temp8 = $line;
			$temp8 =~ s|(<selectout;)(.+?)>|$2|g;
			$temp8 =~ s|;| |g;
			
			# IF contains 'none' then delete all other values...
			if($temp8 =~ /none/)  { $temp8 = "none"; }
			
			$inSelectout = 1;
			$str = "";
		}

		if($line =~ /^<\/selectout>/)	{
			$temp8 = "";
			$inSelectout = 0;
		}
		
		if( ($inSelectout > 0) & ($line !~ /^<selectout/) & ($line !~ /^<\/selectout/) )	{
			# Add @selectout to container tag...
			
			if( 	($line !~ /^<\/(.+?)>/) & 		# not a closing tag
				($line !~ /^([ \t]*)<\/(.+?)>/) & 	# not a closing tag
				($line !~ /^<\?/) & 				# not a processing instruction
				($line !~ /^\<\!--/) &				# not an XML comment 
				($line !~ /^([ \t]*)\<\!--/) &		# not an XML comment 
				($line !~ /<tgroup/) &
				($line !~ /<colspec/) &
				($line !~ /<tbody/) &
				($line !~ /<row/) &
				($line !~ /<entry/) &
				($line !~ /<incr/) &
				($line !~ /<content/) &
				($line !~ /<hnitem/) &
				($line !~ /<hnsource/) &
				($line !~ /<hnsourcesect/) &
				($line !~ /<chitem/) &
				($line !~ /<chdescription/) &
				($line !~ /<chsection/) &
				($line !~ /<critem/) &
				($line !~ /<crdescription/) &
				($line !~ /<crsection/) &
				($line !~ /<slitem/) &
				($line !~ /<sldescription/) &
				($line !~ /<slsource/) &
				($line !~ /<slsection/) &
				($line !~ /<bold/) &
				($line !~ /<ital/) &
				($line !~ /<bdit/) &
				($line !~ /<med/) &
				($line !~ /<(.+?[\w])\/>/)		# not self-closing tag
				)	{	
				$str = $line;			
								
				if($str !~ /selectout=/) {
					$str =~ s|^([ \t]*)<signature (.*?)>$|$1<signature selectout=\"$temp8\" $2>|g;
				}
												
				if($str !~ /selectout=/) {
					$str =~ s|^([ \t]*)<sig_name>(.*?)$|$1<sig_name selectout=\"$temp8\">$2|g;
				}

				if($str !~ /selectout=/) {
					$str =~ s|^([ \t]*)<sig_title>(.*?)$|$1<sig_title selectout=\"$temp8\">$2|g;
				}
								
				if($str !~ /selectout=/) {
					$str =~ s|^([ \t]*)<title>(.*?)$|$1<title selectout=\"$temp8\">$2|g;
				}					
								
				if($str !~ /selectout=/) {
					$str =~ s|^([ \t]*)<subtitle>(.*?)$|$1<subtitle selectout=\"$temp8\">$2|g;
				}				

				if($str !~ /selectout=/) {
					$str =~ s|^([ \t]*)<(.+?) (.+?)>(.*?)$|$1<$2 selectout=\"$temp8\" $3>$4|g;
				}
				
				if($str !~ /selectout=/) {
					$str =~ s|^<(.+?) (.+?)>(.*?)$|<$1 selectout=\"$temp8\" $2>$3|g;
				}							
				
				if($str !~ /selectout=/) {
					$str =~ s|^([ \t]*)<(.+?) (.+?)>$|$1<$2 selectout=\"$temp8\" $3>|g;
				}
				
				if($str !~ /selectout=/) {
					$str =~ s|^<(.+?) (.+?)>$|<$1 selectout=\"$temp8\" $2>|g;
				}
				
				if($str !~ /selectout=/) {
					$str =~ s|^([ \t]*)<(.+?)>$|$1<$2 selectout=\"$temp8\">|g;
				}	
				
				if($str !~ /selectout=/) {
					$str =~ s|^<(.+?)>$|$1<$2 selectout=\"$temp8\">|g;
				}	
				
				# make sure that the @id is first attirbute listed...
				$str =~ s|^<(.+?) (.+?) id="(.+?)\"(.*?)$|<$1 id=\"$3\" $2$4|g;
											
				print OUT "$str\n";
			} else {
				print OUT "$line\n";
			}
		} else {
			if( ($inSelectout < 1) & ($line !~ /^<selectout/) & ($line !~ /^<\/selectout/) )	{
				print OUT "$line\n";
			}
		}
	}	# Read next line...
					
	close(FILE);
	close(OUT);
	
	$tot += $ctr;
	$gtot += $tot;
	$tot=0;

#END PASS-EIGHT
		
	if(uc($x) =~/[Y]/) {
		if(-e $printSpecFileTmp) {
			$cnt = unlink($printSpecFileTmp);
		}	
		
		if(-e $infile1) {
			$cnt = unlink($infile1);
		}		
		
		if(-e $infile2) {
			$cnt = unlink($infile2);
		}
		
		if(-e $infile3) {
			$cnt = unlink($infile3);
		}
		if(-e $infile4) {
			$cnt = unlink($infile4);
		}
		
		if(-e $infile5) {
			$cnt = unlink($infile5);
		}

		if(-e $infile6) {
			$cnt = unlink($infile6);
		}

		if(-e $infile7) {
			$cnt = unlink($infile7);
		}
		
		if(-e $infile8) {
			$cnt = unlink($infile8);
		}
	}
		
}	#end foreach $infile...

print(STDOUT "\n============================================================\n");
print(STDOUT "\nGrand Total Lines Processed (All Files):  $gtot\n\n\n");


#============================================================================
#	PARSE THE NEWLY CREATED XML FILES
#============================================================================
#print("Press 'Y' to Clear the screen and Parse all XML files.\n     \(Press any other key to exit.\)\n");
print("Press 'Y' to Parse all XML files.\n     \(Press any other key to exit.\)\n");
if($opt_topause ne "nopause") {
	$x = ReadKey(); 
} else {
	$x="Y";	
}
	
if(uc($x) =~ /Y/)	{
	open STDERR, ">&STDOUT";			# redirect errors to STDOUT
	open STDOUT, ">$ParseLog";		# redirect STDOUT to "_ParseLog.txt" file

	print(STDOUT "============================================================\n" . 
	"Parsing XML Files...\n============================================================\n");
	
	$ctr = 1;
	$parseCtr=0;
	print STDERR "\nRunning:  $xercesCMD on each XML file created...\n";
	#print STDERR "\nRunning:  $AltovaCMD on each XML file created...\n";

#printOut "\n$dashes\nValidating:  $V_xml\t\tPlease wait...\n\n";
#my $validateRTN = `$AltovaCMD -validate $V_xml -schema $V_xsd`;
#printOut "$validateRTN\n";	
	
	foreach $xml (@ARGV) {
		$xml = $xml . ".xml";
		
		$ParseStatus = `$xercesCMD < $xml 2>&1`;
#		$ParseStatus = `$AltovaCMD /v $xml  2>&1`;

		if($ParseStatus =~ /^stdin\:/)	{		# Xerces Validation MSG
		#if($ParseStatus =~ /^The XML data is valid/)	{		# AltovaXML Validation MSG
			# print(STDOUT "$xml - Validation OK!\n");
		} else {
			print(STDOUT "($ctr)    $xml - ERROR!!!\n");
			print(STDOUT "--------------------------------------------------------------------------------\n");
			$ctr++;
		}
		
		if( ($parseCtr%5 == 0) & ($parseCtr > 0) )	{
			print STDERR ".";
		}
		
		if($parseCtr%50 == 0)	{
			print STDERR "\n";
		}
		
		print STDERR "#";
		$parseCtr++;
	}	
	
	print(STDERR "\nParsing XML Files COMPLETE!\n\n");    
	print(STDOUT "\nParsing XML Files COMPLETE!\n");    
	my $NotePadRTN = system("NotePad $ParseLog");
}
#============================================================================
#	FINISHED PARSING
#============================================================================

print(STDOUT "\nFINISHED:  g2x.exe (pl)\n");


#=============================================	
# MAIN - END
#=============================================	

##############################################
##############################################
#	SUBROUTINES
##############################################

##############################################
#    PASS-1 SUBROUTINES
##############################################
#----------------------------------------------------------------------
sub p1_ampersandProcessing	{  
#----------------------------------------------------------------------
	my $ampLine = shift;
	my $newline = "";
	my $i = 0;
	
	
	$ampLine =~ s|;lt;|\&lt;|g;
	$ampLine =~ s|<lt>|\&lt;|g;
	$ampLine =~ s|;gt;|\&gt;|g;
	$ampLine =~ s|<gt>|\&gt;|g;
	
	my @ampArray = split(/\&/, $ampLine);

	for($i = 0; $i <=$#ampArray; $i++)	{
		if(length($ampArray[$i]) == 0)	{
			#do nothing...
		} elsif(substr($ampArray[$i], 0, 3) eq "lt;")	{
			$ampArray[($i - 1)] = $ampArray[($i - 1)] . "\&";		
		} elsif(substr($ampArray[$i], 0, 3) eq "gt;")	{
			$ampArray[($i - 1)] = $ampArray[($i - 1)] . "\&";		
		} elsif(substr($ampArray[$i], 0, 4) eq "amp;")	{
			$ampArray[($i - 1)] = $ampArray[($i - 1)] . "\&";
		} elsif(substr($ampArray[$i], 0, 2) eq "#x")	{			# protect hex entities ex. &#x25A1; (White Square)
			$ampArray[($i - 1)] = $ampArray[($i - 1)] . "\&";
					
		} else {
			if($i > 0)	{
				$ampArray[$i] = "\&amp;" . $ampArray[$i];
			}
		}
	}
	
	for($i = 0; $i <=$#ampArray; $i++)	{
		$newline = $newline . $ampArray[$i];
	}
	
	return($newline);
}


#----------------------------------------------------------------------
sub p1_headon {
#----------------------------------------------------------------------
	my $parm = shift;
	
	while($parm !~ /<headoff>/)	{
		$parm=readline(FILE);
	}	
	
}


#----------------------------------------------------------------------
sub p1_images {
#----------------------------------------------------------------------
	my $parm = shift;
	my $Capt = "";
	my $CaptQuad="";
		
	#===========================================
	#<inset(SN6);80 ft. Building Setback;14;pi;4-82>
	#===========================================
	$parm =~ s|(<inset)(.+?)(>$)||g;


	#Pickups...
	if($parm =~ /(<pick;)/) {
		print(OUT "<comment note='pickup'>\n");
	}
	
	#===========================================
	#Pickups...(For now, wrap them in XML COMMENTS)
	#===========================================
	#;fl;<mh;0>
	#<pick;sample;none;bound;crt/def/def/def>
	#{/PICK;sample;3p;comptd;block;crt;def;ss;no;1;0;0;no;0;cw;no;def;def}
	#{/GRAPH;sample.jpg;2p;comptd;right;top;0;0;cw;normal;0;0;0;edge;edge;best;prior}
	#{/CAPT;1p;comptd;center;stack;1p;0;cw;0;0;0;0}\m<sz;10q><ff;2><bold><ul;0>Sample!<ul;0><qc>\.
	
	$parm =~ s|(<pick;)(.+?)>|<!-- $1$2 -->|g;
	$parm =~ s|(\{\/PICK;)(.+?)(.?[}])|<!-- $1$2$3 -->|g;
	$parm =~ s|(\{\/GRAPH;)(.+?)(.?[}])|<!-- $1$2$3 -->|g;
	
	# 2012-10-30 - Removed the creation of \m...  $4$5
  	$parm =~ s|(\{\/ANNT;)(.+?)(.?[}])(.?[\\])(.?[m])(.*)(\<q)(.?[lcra])\>|$1$2$3$6$7$8>|mg;
  	$parm =~ s|(\{\/CAPT;)(.+?)(.?[}])(.?[\\])(.?[m])(.*)(\<q)(.?[lcra])\>|$1$2$3$6$7$8>|mg;
  	$parm =~ s|(\{\/TITL;)(.+?)(.?[}])(.?[\\])(.?[m])(.*)(\<q)(.?[lcra])\>|$1$2$3$6$7$8>|mg;
  	
  	# 2012-10-30 -  without \m...
  	$parm =~ s|(\{\/ANNT;)(.+?)(.?[}])(.*)(\<q)(.?[lcra])\>|<!-- $1$2$3$4$5$6> -->|mg;
  	$parm =~ s|(\{\/CAPT;)(.+?)(.?[}])(.*)(\<q)(.?[lcra])\>|<!-- $1$2$3$4$5$6> -->|mg;
  	$parm =~ s|(\{\/TITL;)(.+?)(.?[}])(.*)(\<q)(.?[lcra])\>|<!-- $1$2$3$4$5$6> -->|mg;
	
	$parm =~ s|\\\.|\n<\/comment>\n|g;
	print(OUT "$parm\n");
		
}


#----------------------------------------------------------------------
sub p1_SingleTags {
#----------------------------------------------------------------------
	my $parm = shift;
	my $me = whoami();
	my $str="";
	my @aLink=();
	# FOLLOWING used for 'class="section-link"'...
	my $ulink_class = "";
	my $ulink_url = "";
	my $ulink_urlID = "";
	my $ulink_text = "";

	### BEGIN NEW	
	if($parm =~ /<ulink /)	{
		$ulink_class = "";
		$ulink_url = "";
		$ulink_urlID = "";
		$ulink_text = "";
		$str = $parm;
		
		# CAUTION!  There may be more than one <link/> per input line!
		$str =~ s|<|~~~<|g;
		@aLink = split(/~~~/, $str);
		
		for($c = 0;  $c <= $#aLink;  $c++)	{
			
			if($aLink[$c] =~ /<ulink/)	{
				
				# If <ulink...> without Target=, AND without 'class="section-link"' then create default target="_blank"...
				if(
					($aLink[$c] !~ /<ulink (.+?[^>])target=\"(.+?)>/) and 
					($aLink[$c] !~ /class=\"section-link\"/) 
					)	{
					$aLink[$c] =~ s|(<ulink)(.+?)(class=\")(.+?[^\"])(.+?)\"|$1 $3$4$5\" target=\"_blank\" $2|g;
				}
				
				if( 
					($aLink[$c] !~ /<ulink (.+?[^>])print=\"(.+?)>/)	and
					($aLink[$c] !~ /<ulink (.+?[^>])web=\"(.+?)>/)				
					)	{
					$aLink[$c] =~ s|(<ulink)(.+?)(class=\")(.+?[^\"])(.+?)\"|$1 $3$4$5\" print=\"yes\" $2|g;
					$aLink[$c] =~ s|(<ulink)(.+?)(class=\")(.+?[^\"])(.+?)\"|$1 $3$4$5\" web=\"yes\" $2|g;
					
				}
				
				if($aLink[$c] !~ /<ulink (.+?[^>])print=\"(.+?)>/)	{
					$aLink[$c] =~ s|(<ulink)(.+?)(class=\")(.+?[^\"])(.+?)\"|$1 $3$4$5\" print=\"no\" $2|g;
				}	
		
				if($aLink[$c] !~ /<ulink (.+?[^>])web=\"(.+?)>/)	{
					$aLink[$c] =~ s|(<ulink)(.+?)(class=\")(.+?[^\"])(.+?)\"|$1 $3$4$5\" web=\"no\" $2|g;
				}
				
				$aLink[$c] =~ s|(<ulink)(.+?)(print=\")(YES)(\")(.+?)|$1$2$3yes$5$6|ig;
				$aLink[$c] =~ s|(<ulink)(.+?)(print=\")(NO)(\")(.+?)|$1$2$3no$5$6|ig;
				$aLink[$c] =~ s|(<ulink)(.+?)(web=\")(YES)(\")(.+?)|$1$2$3yes$5$6|ig;
				$aLink[$c] =~ s|(<ulink)(.+?)(web=\")(NO)(\")(.+?)|$1$2$3no$5$6|ig;

				if(
					($aLink[$c] =~ /class=\"section-link\"/) and
					($aLink[$c] =~ /web=\"yes\"/)
					) {
#print STDOUT "[debug \$parm: $parm\n\n";
#print STDOUT "A...\$aLink[$c]:\n$aLink[$c]\n\n";
					# FROM:
					#<ulink class="section-link" print="yes" url="https://www.municode.com/library/nc/high_point/codes/development_ordinance?nodeId=CH10MEDE_10.4DE_NONCONFORMITY" web="yes">nonconformity</ulink>
					
					# TO (XML - EDIT SCHEMA):
					# <ulink class="section-link" data-chunk-id="CH10MEDE_10.4DE_NONCONFORMITY" href="">nonconformity</ulink>
					
					# TO (HTML - MODIFY XSLT):
					#<a class="section-link" data-chunk-id="CH10MEDE_10.4DE_NONCONFORMITY" href=" ">nonconformity</a>
					
					$ulink_class = $aLink[$c];
					$ulink_class =~ s|^<ulink class=\"(.+?)\"(.*?)$|class=\"$1\"|g;
#print STDOUT "ulink_class...\$ulink_class:\n$ulink_class\n\n";

					# $ulink_url = $aLink[$c];
					# $ulink_url =~ s|^<ulink (.+?) url=\"(.+?)\" (.*?)$|$2|g;
					
					$ulink_urlID = $aLink[$c];
					$ulink_urlID =~ s|^<ulink (.+?)nodeId=(.+?)\"(.*?)$|$2|g;
#print STDOUT "ulink_urlID...\$ulink_urlID]:\n$ulink_urlID\n\n";
					
					$ulink_text = $aLink[$c];
					$ulink_text =~ s|^<ulink (.+?)>(.*?)$|$2|g;
#print STDOUT "ulink_text...\$ulink_text:\n$ulink_text\n\n";					
					
					 # [Raymond 2017-07-05]:
#					$aLink[$c] =~ s|^<ulink (.*?)$|<ulink $ulink_class data-chunk-id=\"$ulink_urlID\" href=\"\">$ulink_text|g;
					$aLink[$c] =~ s|^<ulink (.*?)$|<ulink $ulink_class data-chunk-id=\"$ulink_urlID\" href=\"https:\/\/www\.municode\.com\">$ulink_text|g;
#print STDOUT "B...\$aLink[$c]:\n$aLink[$c]\n\n$dashes\n";
				}
				
			}				
		}
		
		# Concatenate array elements...
		$str = "";
		for($c = 0;  $c <= $#aLink;  $c++) {
			$str .= $aLink[$c];
		}
			
		$parm = $str;	
	}
		

	#symbol tags...
	if( $parm =~ /<!--.*-->/ ) {
		# do nothing
	} else {
		$parm =~ s|--|\x{2014}|g;
	}

	$parm =~ s|^;\$;|<comment note='monetary_BEGIN'><\/comment>|g;
	$parm =~ s|^;\\\$;|<comment note='monetary_END'><\/comment>|g;
	
	$parm =~ s|<AsteriskLarge>|\x{002A}|g;
	$parm =~ s|\<sbt;(.*?).?[\>]|<?xpp sbt;$1?>|g;
	$parm =~ s|<(ebt)>|<?xpp $1?>|g;
	
	$parm =~ s|;nph;(.*?);\\nph;|<?xpp mwt;$1?>|g;

	# XPP Table and GenCode Table stuff...
	# REMOVED for Phase-I (web):		# FOUND 'CATCH AS CATCH CAN...	
	$parm =~ s|<avrule>||g;
	
#	# ---------------------------------
#	# Protect Character Entities...
#	# ---------------------------------
	#$parm =~ s|\&bsol;|\x{005C}|g;		#
	$parm =~ s|\&bsol;|<?xpp backslash?>|g;	
	$parm =~ s|\&min;|\x{2032}|g;			#
	$parm =~ s|\&sec;|\x{2033}|g;			#
	$parm =~ s|<check>|\&check;|g;	#check mark  Requires "Arial Unicode MS" font to display on the web!

#	# ---------------------------------
#	# END Protect Character Entities
#	# ---------------------------------
	
	#MCC entities...
	$parm = p1_GenCodeEntities($parm);	
	
	$parm =~ s|<szpct;(\d+)>(.+?)<\/szpct>|<\?xpp szpct;$1\?>$2<\?xpp szpctend\?>|g;
	
	$parm =~ s|<ba>|<\?xpp ba\?>|g;
	$parm =~ s|<bx>|<\?xpp bx\?>|g;
	$parm =~ s|<hp>|<\?xpp hp\?>|g;
	
 	$parm =~ s|<abrule>|<\?xpp abrule\?>|g;
	$parm =~ s|;box;|&#x25A1;|g;
	$parm =~ s|<box>|&#x25A1;|g;
	$parm =~ s|<BoxFilled>|\x{25A0}|g;
	$parm =~ s|<boxf>|\x{25A0}|g;
	$parm =~ s|<xbox>|\x{2612}|g;	
	$parm =~ s|<tric>|\x{25B2}|g;				#20100831 - solid black triangle
	$parm =~ s|\&triUpK;|\x{25B2}|g;			#triangle Up Black (2016-03-01)
	$parm =~ s|\&triUpW;|\x{25B3}|g;		#triangle Up White (2016-03-01)
	$parm =~ s|\&triRtK;|\x{25B6}|g;			#triangle Right Black (2016-03-01)
	$parm =~ s|\&triRtW;|\x{25B7}|g;			#triangle Right White (2016-03-01)
	$parm =~ s|\&triDnK;|\x{25BC}|g;			#triangle Down Black (2016-03-01)
	$parm =~ s|\&triDnW;|\x{25BD}|g;		#triangle Down White (2016-03-01)
	$parm =~ s|\&triLtK;|\x{25C0}|g;			#triangle Left Black (2016-03-01)
	$parm =~ s|\&triLtW;|\x{25C1}|g;			#triangle Left White (2016-03-01)
	$parm =~ s|\&diaK;|\x{25C6}|g;			#diamond Black (2016-03-01)
	$parm =~ s|\&diaW;|\x{25C7}|g;			#diamond White (2016-03-01)

	$parm =~ s|<boxrules>|<?xpp boxrules?>|g;
#	boxt; rule-weight; width; depth; text-indent
	$parm =~ s|<boxt;(.+?)>|<?xpp boxt;$1?>|g;
	$parm =~ s|<cent>|\x{00A2}|g;
	$parm =~ s|<cm;(.+?)>|<?xpp cm;$1?>|g;
	
	
	#<chgcell;xvrule>
	$parm =~ s|<(chgcell;)(.*?)>|<?xpp $1$2?>|g;	
	$parm =~ s|<(chgstyle)(.*?)>|<?xpp $1$2?>|g;	
	$parm =~ s|<(chgcol;)(.*?)>|<?xpp $1$2?>|g;	

	$parm =~ s|<(colfit)>|<?xpp $1?>|g;	
	$parm =~ s|<(colfitx)>|<?xpp $1?>|g;	
	$parm =~ s|<(cctguide;)(.+?)>|<?xpp $1$2?>|g;
	$parm =~ s|<caret>|\x{005E}|g;	# circumflex accent
	
	if($parm =~ /<co;/) { $parm = p1_COtagsWrapping($parm); }
#	$parm =~ s|<(co;)(.+?)>|<?xpp co;$2?>|g;
#	$parm =~ s|<(co;)(.+?);(.+?)>|<?xpp co;$2;$3?>|g;
	
	$parm =~ s|<(fcell;)(.+?)>|<?xpp $1$2?>|g;
	
	$parm =~ s|<ff;(\d*+)>|<?xpp ff;$1?>|g;
	$parm =~ s|<FF;(\d*+);(\d*+)>|<?xpp FF;$1;$2?>|g;
#	$parm =~ s|<fv;(\d*+)>|<?xpp fv;$1?>|g;
	
	$parm =~ s|<sfboxt;(\d+);(.+?)>||g;
	$parm =~ s|<efboxt>||g;
	$parm =~ s|<hcolor;(.+?)>||g;

	$parm =~ s|^<FooterL>(.+?)<\/FooterL>|<?xpp FooterL;$1?>|g;
	$parm =~ s|^<FooterR>(.+?)<\/FooterR>|<?xpp FooterR;$1?>|g;
	$parm =~ s|^<footertext>(.+?)<\/footertext>|<?xpp footertext;$1?>|g;
	$parm =~ s|^<footer(\d*+)(.?[lr])>(.+?)<\/footer(\d+?)(.?[lr])>|<?xpp footer$1$2;$3?>|g;
	
	$parm =~ s|<header1><\/header1>|<?xpp header1?>|g;
	$parm =~ s|<header1>(.+?)<\/header1>|<?xpp header1;$1?>|g;
	$parm =~ s|<header1>(.?)<\/header1>|<?xpp header1;$1?>|g;
	$parm =~ s|<header2><\/header2>|<?xpp header2?>|g;
	$parm =~ s|<header2>(.+?)<\/header2>|<?xpp header2;$1?>|g;

#moved to earlier in g2h
# 	$parm =~ s|<LL_header><\/LL_header>||g;
# 	$parm =~ s|<LL_header>(.+?)<\/LL_header>||g;
# 	$parm =~ s|<LL_header;(.+?)><\/LL_header>||g;
# 	$parm =~ s|<LL_header;(.+?)>(.+?)<\/LL_header>||g;
# #	$parm =~ s|<LL_header><\/LL_header>|<?xpp LL_header?>|g;
# #	$parm =~ s|<LL_header>(.+?)<\/LL_header>|<?xpp LL_header;$1?>|g;
# #	$parm =~ s|<LL_header;(\d+)><\/LL_header>|<?xpp LL_header;$1?>|g;
# #	$parm =~ s|<LL_header;(\d+)>(.+?)<\/LL_header>|<?xpp LL_header;$1;$2?>|g;
	
# 	$parm =~ s|<LL_footer><\/LL_footer>||g;
# 	$parm =~ s|<LL_footer>(.+?)<\/LL_footer>||g;
# 	$parm =~ s|<LL_footer;(.+?)><\/LL_footer>||g;
# 	$parm =~ s|<LL_footer;(.+?)>(.+?)<\/LL_footer>||g;
# #	$parm =~ s|<LL_footer><\/LL_footer>|<?xpp LL_footer?>|g;
# #	$parm =~ s|<LL_footer>(.+?)<\/LL_footer>|<?xpp LL_footer;$1?>|g;
# #	$parm =~ s|<LL_footer;(\d+)><\/LL_footer>|<?xpp LL_footer;$1?>|g;
# #	$parm =~ s|<LL_footer;(\d+)>(.+?)<\/LL_footer>|<?xpp LL_footer;$1;$2?>|g;

	$parm =~ s|<(m1;)(.+?)>|<?xpp $1$2?>|g;	
	$parm =~ s|<(me;)(.+?)>|<?xpp $1$2?>|g;	
	$parm =~ s|<(mh;)(.+?)>|<?xpp $1$2?>|g;	
	$parm =~ s|<(ix)>|<?xpp $1?>|g;	
	$parm =~ s|<(xix)>|<?xpp $1?>|g;	


	# San Francisco 14145 - Zoning Maps...
	$parm =~ s|<foldoutm2>|<?xpp foldoutm2?>|g;
	$parm =~ s|<foldoutrotate>|<?xpp foldoutrotate?>|g;
	$parm =~ s|<setzoneheadon>|<?xpp setzoneheadon?>|g;	
	$parm =~ s|<copyright>(.+?)<\/copyright>|<comment meta1='copyright'>\n$1\n<\/comment>|g;	
	$parm =~ s|<setsheetno;(.+?)>|<?xpp setsheetno;$1?>|g;
	# END San Francisco 14145 Zoning Maps.
	
	# Raleigh SC 10312...
	$parm =~ s|<(headrule)>|<?xpp $1?>|g;	
	$parm =~ s|<(begRaleighBox)>|<?xpp $1?>|g;	
	$parm =~ s|<(endRaleighBox)>|<?xpp $1?>|g;	
	$parm =~ s|<(noVerticalBar)>|<?xpp $1?>|g;	
	$parm =~ s|<(yesVerticalBar)>|<?xpp $1?>|g;	
	# END Raleigh SC
	
	$parm =~ s|<genheads>|<?xpp genheads?>|g;
	$parm =~ s|<gentags>|<?xpp gentags?>|g;
	$parm =~ s|<genlists>|<?xpp genlists?>|g;
	$parm =~ s|<guidelists;off>|<?xpp guidelists;off?>|g;
	$parm =~ s|<guidelists;on>|<?xpp guidelists;on?>|g;
	
	$parm =~ s|<setgtext(.+?)>||g;
	$parm =~ s|<pregtext(.+?)>||g;
	
	$parm =~ s|<(headsize);(\d*+);(\d*+)>|<?xpp $1;$2;$3?>|g;
	$parm =~ s|;(headsize);(\d*+);(\d*+);|<?xpp $1;$2;$3?>|g;
	
	# <footer_text;Article I. General Provisions>
	$parm =~ s|<footer_text;(.+?)>|<?xpp footer_text;$1?>|g;
	
	$parm =~ s|<(i1;)(.+?)>|<?xpp $1$2?>|g;
	$parm =~ s|</(i1;10q)>|<?xpp $1?>|g;	
	$parm =~ s|<i>|;i;|g;
	$parm =~ s|<\/i>|;\\i;|g;
	$parm =~ s|<i>|;i;|g;

	#<ibox;.3q;8q;8q> (rule-weight, width, & depth)
	$parm =~ s|<ibox;(.+)>|<?xpp ibox;$1?>|g;

	$parm =~ s|<(il=)(.+?)>|<?xpp $1$2?>|g;
	$parm =~ s|;(il=)(.+?);|<?xpp $1$2?>|g;
	$parm =~ s|;il=(\d*+)\.(\d*+)(.?[qpcdimknuz]);|<?xpp il;$1.$2$3?>|g;
	$parm =~ s|<(il;)(\d*+)\.(\d*+)(.?[qpcdimknuz])>|<?xpp $1.$2$3?>|g;	
	$parm =~ s|;il=(\d*+)(.?[qpcdimknuz]);|<?xpp il;$1$2?>|g;
	$parm =~ s|<(il;)(\d*+)(.?[qpcdimknuz])>|<?xpp $1$2$3?>|g;
	
	$parm =~ s|<(indent;)(.*[\d{1-255}])(.?[qpcdimknuz])>|<?xpp $1$2$3?>|g;
	$parm =~ s|<(ir;)(.+?)>|<?xpp $1$2?>|g;
	$parm =~ s|;ir=(.+?);|<?xpp ir;$1?>|g;
	$parm =~ s|<xir>|<?xpp xir?>|g;
	$parm =~ s|<(in;)(.+?)>|<?xpp $1$2?>|g;
	$parm =~ s|<xin>|<?xpp xin?>|g;
	$parm =~ s|(;)(xil)(;)|<?xpp $2?>|gi;
	
	$parm =~ s|(;)(i1)(=)(\d)(;)|<?xpp $2;$4?>|gi;
	$parm =~ s|<(l1;)(.*?)>|<?xpp $1$2?>|g;
	$parm =~ s|<l>|<?xpp l?>|g;
	
	$parm =~ s|<ks;1>|<?xpp ks;1?>|g;
	$parm =~ s|<ks>|<?xpp ks?>|g;
	$parm =~ s|<ke>|<?xpp ke?>|g;

	$parm =~ s|;vk;|<?xpp vk?>|g;
	$parm =~ s|;\\vk;|<?xpp \/vk?>|g;
	$parm =~ s|<vk>|<?xpp vk?>|g;
	$parm =~ s|<\/vk>|<?xpp \/vk?>|g;
	
	$parm =~ s|<(la;)(.+?)>|<?xpp $1$2?>|g;
	$parm =~ s|<le;(\d*+)(.?[qpcdimknuz])>|<?xpp le;$1$2?>|g;
	$parm =~ s|<logo>|<?xpp logo?>|g;
	$parm =~ s|<plus\/minus>|\x{00B1}|g;
	$parm =~ s|;(me=\d+)(.?[qpcdimknuz]);|<?xpp $1$2?>|g;
	$parm =~ s|<(me;\d+)(.?[qpcdimknuz])>|<?xpp $1$2?>|g;
	
	$parm =~ s|<minrowd;(.*?)>||g;
	$parm =~ s|<ol0rule>|<?xpp ol0rule?>|g;

	$parm =~ s|;(footrule);|<?xpp $1?>|g;
	$parm =~ s|<(headrule;on)>|<?xpp $1?>|g;
	$parm =~ s|<(headrule;off)>|<?xpp $1?>|g;
	
	$parm =~ s|<rv>|<?xpp rv?>|g;
	$parm =~ s|<erv>|<?xpp erv?>|g;
	
	$parm =~ s|<reset>|<?xpp reset?>|g;
	
	$parm =~ s|<(sethead;)(.+?)>|<?xpp $1$2?>|g;
	$parm =~ s|<(setfooter;)(.+?)>|<?xpp $1$2?>|g;	
	
	$parm =~ s|<(setfnnum100;)>|<?xpp $1?>|g;
	$parm =~ s|<(setlldiv)>|<?xpp $1?>|g;
	$parm =~ s|<(setlldiv)(.+?)>|<?xpp $1$2?>|g;
	$parm =~ s|<Sigma>|\x{003A3}|g;
	$parm =~ s|;sigma;|\x{003C3}|g;	
	$parm =~ s|<reg>|\x{00AE}|g;
	$parm =~ s|;(reserve)=(.*[\d{1-255}])(.?[qpcdimknuz]);|<?xpp $1;$2$3?>|g;
	$parm =~ s|<(reserve;)(.*[\d{1-255}])(.?[qpcdimknuz])>|<?xpp $1$2$3?>|g;
	
	$parm =~ s|<(tbldepth;)(.+?)>|<?xpp $1$2?>|g;
	$parm =~ s|<tj;(.+)>|<?xpp tj$1?>|g;
	$parm =~ s|<tj>|<?xpp tj?>|g;

#	$parm =~ s|<pregtext;(.+?)>|<?xpp pregtext;$1?>|g;
	
	$parm =~ s|<ix>|<?xpp ix?>|g;
	$parm =~ s|<xix>|<?xpp xix?>|g;
	$parm =~ s|<(setpline);(.+?)>|<?xpp $1;$2?>|g;
		$parm =~ s|<gettext;(.+?)>||g;
	$parm =~ s|<settext;(.+?)>||g;

	# PX... PA 'dance'
	$parm =~ s|<\/?px;(\d*+);(\d*+)>(.+?)<pa>|<?xpp px;$1;$2?><?xpp pxText;$3?><?xpp pa?>|g;
	$parm =~ s|<\/?px;(\d*+);(\d*+)><pa>|<?xpp px;$1;$2?><?xpp pxText?><?xpp pa?>|g;
	$parm =~ s|<\/?px;(\d*+)>(.+?)<pa>|<?xpp px;$1?><?xpp pxText;2?><?xpp pa?>|g;		#20101004.1
	$parm =~ s|<\/?px;(\d*+)><pa>|<?xpp px;$1?><?xpp pxText?><?xpp pa?>|g;			#20100915.2
	$parm =~ s|<\/?px;;(\d*+)>(.+?)<pa>|<?xpp px;;$1?><?xpp pxText;$2?><?xpp pa?>|g;	#20101004.2
	$parm =~ s|<\/?px;;(\d*+)><pa>|<?xpp px;;$1?><?xpp pxText?><?xpp pa?>|g;			#20100915.2

	$parm =~ s|<cs>||g;	
	$parm =~ s|<ce>||g;	

	#NOTE:  XPP User-defined pattern number range:  165-65535
	$parm =~ s|<(pt;)(\d*+);(\d*+)>|<?xpp $1$2;$3?>|g;		# <pt;0;0> means current fore/background colors as solid
	$parm =~ s|<(pt;)(\d*+)>|<?xpp $1$2?>|g;				# <pt;50;10> means current forground at 50%, background at 10%

	$parm =~ s|<(offset)>|<?xpp $1?>|g;
    
    $parm =~ s|<(rowbg;)(.+?)>|<\?xpp $1$2\?>|g;        # after <T[rc]><?xpp rowbg;Silver?><?xpp rowheight;150?> or <?xpp rowheight;150px?>
    $parm =~ s|<(rowheight;)(.+?)>|<\?xpp $1$2\?>|g;
	
	$parm =~ s|<(rotate;cc90)>|<?xpp $1?>|g;
	$parm =~ s|<(rotate;cw90)>|<?xpp $1?>|g;
	$parm =~ s|<(rotate;cc180)>|<?xpp $1?>|g;
	$parm =~ s|<(rotate;cw180)>|<?xpp $1?>|g;
	$parm =~ s|<(rotate;cc270)>|<?xpp $1?>|g;
	$parm =~ s|<(rotate;cw270)>|<?xpp $1?>|g;
	
	$parm =~ s|<rs>|<?xpp rs?>|g;
	
	$parm =~ s|<(setbgut;)(.+)>|<?xpp $1$2?>|g;
	$parm =~ s|<(setbrul;)(.+)>|<?xpp $1$2?>|g;
	$parm =~ s|<(setrul;)(.+)>|<?xpp $1$2?>|g;
	$parm =~ s|<(setgut;)(.+)>|<?xpp $1$2?>|g;	
	
	$parm =~ s|<setcell;(.+?)>|<?xpp setcell;$1?>|g;
	$parm =~ s|<setcol;(.*?)>|<?xpp setcol;$1?>|g;
	$parm =~ s|<setxgut;.*>||g;
	$parm =~ s|<setrul;.*;\d;\d\.?[.].*[\d].?[qip]>||g;
	$parm =~ s|<setall;(.+)?>|<?xpp setall;$1?>|g;
	$parm =~ s|<(setaln;.*)?>||g;
	$parm =~ s|<setheads;(\d*+)>|<?xpp setheads;$1?>|g;
	$parm =~ s|<(setctr;)(.+)>|<?xpp $1$2?>|g;
	$parm =~ s|<setfolio;(.*?)>||g;
	$parm =~ s|<sum>|<?xpp sum?>|g;
	
	$parm =~ s|<tabrules>|<?xpp tabrules;on?>|g;
	$parm =~ s|<tabrules;(.+?)>|<?xpp tabrules;$1?>|g;
    
	$parm =~ s|<tindent;(.*?)>|<?xpp tindent;$1?>|g;
	$parm =~ s|<theads;(\d{1,2})([^>]*)>|<?xpp theads;$1?>|g;	
	$parm =~ s|<thead(.?[s]);(.*?)>|<?xpp thead$1;$2?>|g;
	$parm =~ s|<pstfolio;(.+?)>||g;
	$parm =~ s|<(setgut;)(.+?)>|<?xpp $1$2?>|g;
	$parm =~ s|<(setrow;)(.+?)>|<?xpp $1$2?>|g;
	$parm =~ s|<(.?[Tt].?[Cc];\d{0,7});>|<$1>|g;
	$parm =~ s|<tblbreak;(.*?)>|<?xpp tblbreak;$1?>|g;
	$parm =~ s|<tblwidth>||g;
	$parm =~ s|<tblwidth(.+?)>|<?xpp tblwidth$1?>|g;
	# NOT A VALID GENCODE, but Editors are using it anyway! ...
	$parm =~ s|<tablwidth;(.*?)>|<?xpp tblwidth;$1?>|g;
	
	$parm =~ s|<(urulez;)(.+?)>|<?xpp $1$2?>|g;
	
	#$parm =~ s|<vstyle;.?[cbt]>||g;
	#$parm =~ s|<vstyle;center>||g;
	$parm =~ s|<vstyle;t>|<\?xpp vstyle;top\?>|g;
	$parm =~ s|<vstyle;c>|<\?xpp vstyle;middle\?>|g;
	$parm =~ s|<vstyle;2>|<\?xpp vstyle;middle\?>|g;
	$parm =~ s|<vstyle;center>|<\?xpp vstyle;middle\?>|g;
	$parm =~ s|<vstyle;b>|<\?xpp vstyle;bottom\?>|g;
	$parm =~ s|<vstyle;(.+?)>|<\?xpp vstyle;$1\?>|g;
	
	$parm =~ s|<chgrow;xalign>|<?xpp chgrow;xalign?>|g;	
	$parm =~ s|<chgrow;ahrule>|<?xpp chgrow;ahrule?>|g;
	$parm =~ s|<chgrow;xhrule>|<?xpp chgrow;xhrule?>|g;
	$parm =~ s|<chgrow;xvrule>|<?xpp chgrow;xvrule?>|g;
	$parm =~ s|<chgrow;bold>|<?xpp chgrow;bold?>|g;
	$parm =~ s|<chgrow;(.*;.?[-]\d.?[qip])>|<?xpp chgrow;$1?>|g;
	$parm =~ s|<chgrow;(.*?);(.*?)>|<?xpp chgrow;$1;$2?>|g;
	$parm =~ s|<chgrow;(.*?)>|<?xpp chgrow;$1?>|g;
	$parm =~ s|<(rowrule)>|<?xpp chgrow;hrulez;1?>|g;
	$parm =~ s|<(hrulez;)(.+?)>|<?xpp $1$2?>|g;

	$parm =~ s|<(ahrule)>|<?xpp $1?>|g;
	$parm =~ s|<(xalign)>|<?xpp $1?>|g;
	$parm =~ s|<(xhrule)>|<?xpp $1?>|g;
	$parm =~ s|<(xrule)>|<?xpp $1?>|g;
	$parm =~ s|<(xvrule)>|<?xpp $1?>|g;
	$parm =~ s|<(xbrule)>|<?xpp $1?>|g;

	$parm =~ s|<(setrh1;)(.*?)>|<?xpp $1$2?>|g;
				
	#Atlanta <rn#> junk (bad markup?)...
	$parm =~ s|<rn0>|<?xpp rn0?>|g;
	$parm =~ s|<rn1>|<?xpp rn1?>|g;
	$parm =~ s|<rn2>|<?xpp rn2?>|g;
	$parm =~ s|<rn3>|<?xpp rn3?>|g;
	$parm =~ s|<rn4>|<?xpp rn4?>|g;
	$parm =~ s|<rn5>|<?xpp rn5?>|g;
	$parm =~ s|<rns0>|<?xpp rns0?>|g;
	$parm =~ s|<rns1>|<?xpp rns1?>|g;
	$parm =~ s|<rns2>|<?xpp rns2?>|g;
	$parm =~ s|<rns3>|<?xpp rns3?>|g;
	$parm =~ s|<rns4>|<?xpp rns4?>|g;
	$parm =~ s|<rns5>|<?xpp rns5?>|g;	
	
	#Oklahoma Junk...
	$parm =~ s|<beg>||g;	
	$parm =~ s|<end>||g;		
	
	$parm =~ s|<title>|<?xpp pg_title?>|g;
	$parm =~ s|<official>|<?xpp pg_official?>|g;
	$parm =~ s|<ord>|<?xpp pg_ord?>|g;
	$parm =~ s|<preface>|<?xpp pg_preface?>|g;
	#$parm =~ s|<foldout>|\n<?xpp foldout?>\n|g;
			
	
	# Some test items...
	$parm =~ s|<hArr>|\x{DB}|g;	
	$parm =~ s|<infin>|\x{221E}|g;	
	$parm =~ s|<capital-N-Tilde>|\x{00D1}|g;	
			
	# Convert to Unicode utf8...
	$parm =~ s|[\|]Z1|\x{2460}|g;
	$parm =~ s|[\|]Z2|\x{2461}|g;
	$parm =~ s|[\|]Z3|\x{2462}|g;
	$parm =~ s|[\|]Z4|\x{2463}|g;
	$parm =~ s|[\|]Z5|\x{2464}|g;
	$parm =~ s|<micron>|\x{00B5}|g;		
	$parm =~ s|<\'A>|\x{00C1}|g;
	$parm =~ s|<\'a>|\x{00E1}|g;
	$parm =~ s|<at>|\x{0040}|g;
	$parm =~ s|<bullet>|\x{2022}|g;	
	$parm =~ s|;bullet;|\x{2022}|g;	
	$parm =~ s|<circle>|\x{25CB}|gi;
	$parm =~ s|\&circleWhite;|\x{25CB}|g;		# circle White (2016-03-01)
	$parm =~ s|\&circleBlack;|\x{25CF}|g;		# circle Black (2016-03-01)
	
	$parm =~ s|\&cirLtK;|\x{25D0}|g;			# circle Left Black (2016-03-01)
	$parm =~ s|\&cirRtK;|\x{25D1}|g;			# circle Right Black (2016-03-01)
	$parm =~ s|\&cirBtmK;|\x{25D2}|g;			# circle Bottom Black (2016-03-01)
	$parm =~ s|\&cirTopK;|\x{25D3}|g;			# circle Top Black (2016-03-01)

	$parm =~ s|\&circleLtK;|\x{25D0}|g;			# circle Left Black (2016-03-02)
	$parm =~ s|\&circleRtK;|\x{25D1}|g;			# circle Right Black (2016-03-02)
	$parm =~ s|\&circleBtmK;|\x{25D2}|g;			# circle Bottom Black (2016-03-02)
	$parm =~ s|\&circleTopK;|\x{25D3}|g;			# circle Top Black (2016-03-02)


	
	# integers 1-20 inside a circle: Added: 2012-08-30 Raymond Lillibridge
	$parm =~ s|<circle0>|\x{24EA}|g;	
	$parm =~ s|<circle1>|\x{2460}|g;	
	$parm =~ s|<circle2>|\x{2461}|g;	
	$parm =~ s|<circle3>|\x{2462}|g;	
	$parm =~ s|<circle4>|\x{2463}|g;	
	$parm =~ s|<circle5>|\x{2464}|g;	
	$parm =~ s|<circle6>|\x{2465}|g;	
	$parm =~ s|<circle7>|\x{2466}|g;	
	$parm =~ s|<circle8>|\x{2467}|g;	
	$parm =~ s|<circle9>|\x{2468}|g;	
	$parm =~ s|<circle10>|\x{2469}|g;	
	$parm =~ s|<circle11>|\x{246A}|g;	
	$parm =~ s|<circle12>|\x{246B}|g;	
	$parm =~ s|<circle13>|\x{246C}|g;	
	$parm =~ s|<circle14>|\x{246D}|g;	
	$parm =~ s|<circle15>|\x{246E}|g;	
	$parm =~ s|<circle16>|\x{246F}|g;	
	$parm =~ s|<circle17>|\x{2470}|g;	
	$parm =~ s|<circle18>|\x{2471}|g;	
	$parm =~ s|<circle19>|\x{2472}|g;	
	$parm =~ s|<circle20>|\x{2473}|g;	
	
	$parm =~ s|<copy>|\x{00A9}|g;	
	$parm =~ s|;copy;|\x{00A9}|g;	
	
	$parm =~ s|<[Dd]elta>|<?xpp Delta?>|g;
	$parm =~ s|<ds>|\x{00B0}|g;	
	$parm =~ s|<\'E>|\x{00C9}|g;
	$parm =~ s|<\'e>|\x{00E9}|g;
	$parm =~ s|;ellipsis;|\x{2026}|g;
	$parm =~ s|<ellipsis>|\x{2026}|g;
	$parm =~ s|<lte>|\x{2264}|g;
	$parm =~ s|<gte>|\x{2265}|g;
	$parm =~ s|;para;|\x{00B6}|g;
	$parm =~ s|<para>|\x{00B6}|g;
	$parm =~ s|<s>|\x{00A7}|g;
	$parm =~ s|;s;|\x{00A7}|g;
	$parm =~ s|<min>|\x{2032}|g;
	$parm =~ s|<sec>|\x{2033}|g;
	$parm =~ s|<minus>|-|g;
	$parm =~ s|<times>|\x{00d7}|g;
	$parm =~ s|<deg>|\x{00b0}|g;
	#$parm =~ s|<ft>|\x{02032}|g;			# prime?
	#$parm =~ s|<inch>|\x{02033}|g;
	$parm =~ s|<ft>|\x{2032}|g;			# prime?
	$parm =~ s|<inch>|\x{2033}|g;
	$parm =~ s|<carot>|\x{005E}|g;
	$parm =~ s|<sd>|\x{2020}|g;				#single dagger
	$parm =~ s|<dd>|\x{2021}|g;				#double dagger
	
	$parm =~ s|<eoc>|<?xpp eoc?>|g;			#end of column
	$parm =~ s|<pm>|<?xpp pm?>|g;

	
	
	#$parm =~ s|<thin>|\x{2009}|g;	#DOESN'T SEEM TO WORK...		
	$parm =~ s|<thin>|<?xpp thin?>|g;	
	$parm =~ s|;thin;|<?xpp thin?>|g;	
	
	# Remove Word Perfect ending character  0x1A
	$parm =~ s|\x1A||g;
		

	
	#eol stuff...
	$parm =~ s|;eol;|<?xpp qa?>|g;	
	$parm =~ s|<eol>|<?xpp qa?>|g;	
	
	$parm =~ s|(.)(<Tr)|$1\n$2|g;			
	$parm = fv($parm);		# manage font variant stuff
	
    # tindent... (already a PI)
    if($parm =~ /<\?xpp tindent;/) {
        $str = $parm;
        $str =~ s|^(.+?);(.*?)\?>$|$2|g;    # get everything after 'tindent;'
        $str =~ s|^(.+?);$|$1|g;            # remove ending ';'
        if($str !~ /;/) { $str = "empty"; } # remove content if only XPP value (single)
        $str =~ s|^(.+?);(.+?)$|$2|g;       # get only the intended web indent 'b0..9'
        $parm = "<\?xpp tindent;$str\?>";      
    }
    
	# Convert GenCode comments to standard XML comments...
    if($parm =~ /<com>pi-/) {
        $parm =~ s|<com>pi-(.+?)<\/com>|<\?xpp $1\?>|g;
    } else {
        $parm =~ s|;com;|<comment>\n<!--|g;
        $parm =~ s|;\\com;|-->\n</comment>|g;
        $parm =~ s|<com>|<comment>\n<!--|g;
        $parm =~ s|<\/com>|-->\n</comment>|g;
        $parm =~ s|<\\com>|-->\n</comment>|g;
    }
    
#	$parm =~ s|<analysis>||g;
#	$parm =~ s|</analysis>||g;
	
	$parm =~ s|;comment;|<comment>\n<\!--|g;
	$parm =~ s|;\\comment;|-->\n<\/comment>|g;
	
#	$parm =~ s|<([\/]?)noprint>||g;
#	$parm =~ s|<noprint>|<output doc=\"web\">|g;
#	$parm =~ s|</noprint>|</output>|g;
	$parm =~ s|<noprint>||g;
	$parm =~ s|</noprint>||g;	

	#David created this tag...
	if($parm =~ /<output/)	{
		$parm =~ s|<output doc=\"(.+?)\">|<\?xpp output;$1\?>|g;
#		$output_buffer = $parm;	# save for <endtag> game later...
#		$parm = "";
	}
	$parm =~ s|<\/output>|<\?xpp output;close\?>|g;

	# MISC	
	$parm =~ s|<q(.?[lcrja])>|<?xpp q$1?>|g;
	$parm =~ s|;q(.?[lcrja]);|<?xpp q$1?>|g;
	$parm =~ s|;l;|<?xpp ql?>|g;
	$parm =~ s|<l>|<?xpp ql?>|g;
	$parm =~ s|;c;|<?xpp qc?>|g;
	$parm =~ s|<c>|<?xpp qc?>|g;
	$parm =~ s|;r;|<?xpp qr?>|g;
	$parm =~ s|<r>|<?xpp qr?>|g;
	$parm =~ s|;j;|<?xpp qj?>|g;
	$parm =~ s|<j>|<?xpp qj?>|g;
	$parm =~ s|<(lead;)(\d*+)>|<?xpp $1$2?>|g;
	$parm =~ s|<ip;(\d*+)>?|<?xpp ip;$1?>|g;
	
	$parm =~ s|<(check)>|\x{25D8}|g;	# "reverse bullet"
	$parm =~ s|<(ckbox)>|\x{2611}|g;	# checked box (unicode)	

	$parm =~ s|<eop>|<?xpp ep?>|g;
	$parm =~ s|<ep>|<?xpp ep?>|g;
	$parm =~ s|<(eb;)(.+?)>|<?xpp $1$2?>|g;	
	$parm =~ s|<ebnormal>|<?xpp ebnormal?>|g;

	$parm =~ s|<(hp)>|<?xpp $1?>|g;
	$parm =~ s|<mk>|<?xpp mk?>|g;
	$parm =~ s|<rmk>|<?xpp rmk?>|g;
	$parm =~ s|<xmk>|<?xpp xmk?>|g;
	$parm =~ s|<\/vs>|<?xpp vs?>|g;
	$parm =~ s|<vs>|<?xpp vs?>|g;
	
	# $parm =~ s|;hg;| |g;

	$parm = p1_XPPmeasurementConversion($parm);
		
# boxed text...	
	#<sbt;0.5q;0;2q>		[+-]?(\d+\.\d+|\d+\.|\.\d+)
#	$parm =~ s|<(sbt);(\d+\.\d+|\d+)(.?[qpcdimknuz]);(\d+\.\d+|\d+);(\d+\.\d+|\d+)(.?[qpcdimknuz])>|<?xpp $1;$2$3;$4;$5$6?>|g;
	$parm =~ s|<(sbt);(\d+\.\d+)(.?[qpcdimknuz]);(\d+\.\d+);(\d+\.\d+)(.?[qpcdimknuz])>|<?xpp $1;$2$3;$4;$5$6?>|g;
	$parm =~ s|<(sbt);(\d*+)(.?[qpcdimknuz]);(\d*+);(\d*+)(.?[qpcdimknuz])>|<?xpp $1;$2$3;$4;$5$6?>|g;
	$parm =~ s|<(sbt);(\d+\.\d+)(.?[qpcdimknuz]);(\d*+);(\d*+)(.?[qpcdimknuz])>|<?xpp $1;$2$3;$4;$5$6?>|g;
	$parm =~ s|<(sbt);(\d+\.\d+)(.?[qpcdimknuz]);(\d*+);(\d+\.\d+)(.?[qpcdimknuz])>|<?xpp $1;$2$3;$4;$5$6?>|g;
	$parm =~ s|<(sbt);(\d*+)(.?[qpcdimknuz]);(\d*+);(\d+\.\d+)(.?[qpcdimknuz])>|<?xpp $1;$2$3;$4;$5$6?>|g;
	
	$parm =~ s|<(ebt)>|<?xpp $1?>|g;
# END boxed text.
	
	$parm =~ s|;(le)(\=)(\d*+)(.?[ipq]);|<?xpp $1;$3$4?>|g;
	

# XPP-Macro:  mb {n, -n, &n, &-n}{[qpcdimknuz]}  ex:  <mb;&-2.44p>
	$parm =~ s|<(mb;0)>|<?xpp $1?>|g;
	$parm =~ s|;(mb)(\=)(.?[-])(\d*+)(.?[qpcdimknuz]);|<?xpp $1;$3$4$5?>|g;
	$parm =~ s|<(mb;)([\&]?)([+-]?)(\d*+)(\.)(\d*+)(.?[qpcdimknuz])>|<?xpp $1$2$3$4$5$6$7?>|g;
	$parm =~ s|<(mb;)([\&]?)([+-]?)([\.]?)(\d*+)(.?[qpcdimknuz])>|<?xpp $1$2$3$4$5$6?>|g;

	$parm =~ s|<lf>|<?xpp lf?>|g;
	$parm =~ s|;lf;|<?xpp lf?>|g;	
	$parm =~ s|;lf\=(.+?);|<?xpp lf;$1?>|g;
	$parm =~ s|<lf\=(.+?)>|<?xpp lf;$1?>|g;
	$parm =~ s|<lf(.+?)>|<?xpp lf;$1?>|g;
	
	$parm =~ s|<lp;0>|<?xpp lp;0?>|g;	
	$parm =~ s|<lp;(\d*+)(.?[\.])(\d*+)(.?[qpcdimknuz]);1>|<?xpp lp;$1$2$3$4$5;1?>|g;
	$parm =~ s|<lp;(.?[-])(\d*+)(.?[\.])(\d*+)(.?[qpcdimknuz])>|<?xpp lp;$1$2$3$4$5?>|g;	
	$parm =~ s|<lp;(\d*+)(.?[\.])(\d*+)(.?[qpcdimknuz])>|<?xpp lp;$1$2$3$4?>|g;	
	$parm =~ s|<lp;(\d*+)(.?[qpcdimknuz])>|<?xpp lp;$1$2?>|g;
	$parm =~ s|<lp;(.?[-])(\d*+)(.?[qpcdimknuz])>|<?xpp lp;$1$2$3?>|g;
	$parm =~ s|<lp;(\d*+)(.?[qpcdimknuz]);(\d*+)>|<?xpp lp;$1$2;$3?>|g;
	$parm =~ s|<\?xpp lp;(.+?)>||g;
		
	$parm =~ s|;(i1)(\=)(\d.?[qpcdimknuz]);|<?xpp $1;$3?>|g;
	$parm =~ s|<(in;)(\d*+)(.?[qpcdimknuz])>|<?xpp $1$2$3?>|g;		#  CONVERT SIMILAR \d MARKUP!!!
	$parm =~ s|;(il)(\=)(\d*+)(.?[qpcdimknuz]);|<?xpp in;$3$4?>|g;
	$parm =~ s|;lz;|<?xpp lz?>|g;
	$parm =~ s|<lz>|<?xpp lz?>|g;
	$parm =~ s|^[<;]restore[;>]|<?xpp restore?>|g;
	$parm =~ s|^[<;]rotatepg[;>]|<?xpp rotatepg?>|g;
	$parm =~ s|<xin>|<?xpp xin?>|g;
		
	$parm =~ s|;me=(.*?[\d])(.?)[qpcdimknuz];||g;
	$parm =~ s|<mh;0>|<?xpp mh;0?>|g;
	$parm =~ s|<mh;(.*[0-9\.])(.?[qpcdimknuz])>|<?xpp mh;$1$2?>|g;
	
	# footnote anchor tags...
 	$parm =~ s|;(anchor);|<?xpp $1?>|g;
 	$parm =~ s|;(noanchor);|<?xpp $1?>|g;
 	$parm =~ s|;(supanchor);|<?xpp $1?>|g;
 		
 	$parm =~ s|<begtab>|\n<table tabstyle=\'sys\'>\n|g;
 	$parm =~ s|<endtab>|<\/table>\n|g;
 	$parm =~ s|<begtab;(.+?);.+?>|\n<table tabstyle=\'$1\'>|g;		#prelead is responsibility of XPP Style
 	$parm =~ s|<begtab;(.+?)>|\n<table tabstyle=\'$1\'>|g;

	$parm =~ s|;(rev\=)(\d*+)(\w);|<?xpp rev;$2\.$3?>|g;
	
	$parm =~ s|;adv=(\d+)\.(\d+)([qpcdimknuz]);|<?xpp lp;$1\.$2$3?>|g;
	$parm =~ s|;adv=(\d+)([qpcdimknuz]);|<?xpp lp;$1$2?>|g;
	$parm =~ s|<adv=(\d+)\.(\d+)([qpcdimknuz])>|<?xpp lp;$1\.$2$3?>|g;
	$parm =~ s|<adv=(\d+)([qpcdimknuz])>|<?xpp lp;$1$2?>|g;
	$parm =~ s|<adv;(\d+)\.(\d+)([qpcdimknuz])>|<?xpp lp;$1\.$2$3?>|g;
	$parm =~ s|<adv;(\d+)([qpcdimknuz])>|<?xpp lp;$1$2?>|g;
	$parm =~ s|<\?xpp lp;(.+?)>||g;
	
	$parm =~ s|<city_name>|\n<REMOVE>\n<comment>\n<city_name>|g;
	$parm =~ s|</city_name>|</city_name>\n</comment>\n</REMOVE>\n|g;	
	
	# Using "Character Map (Windows Utility) to get U+#### values: 
	$parm =~ s|;(frax;\d+;\d+);|<$1>|g;		# Raymond Lillibridge - convert legacy Gencode to have chevron delimiters
	# ASCII Code Ext.
	$parm =~ s|<frax;1;4>|\x{00BC}|g;
	$parm =~ s|<frax;1;2>|\x{00BD}|g;
	$parm =~ s|<frax;3;4>|\x{00BE}|g;
	
#	# NON-ASCII Code Ext. (This block added 2012-11-21 - Raymond Lillibridge)
#	$parm =~ s|<frax;1;3>|\x{2153}|g;	
#	$parm =~ s|<frax;2;3>|\x{2154}|g;	
#	$parm =~ s|<frax;1;5>|\x{2155}|g;	
#	$parm =~ s|<frax;2;5>|\x{2156}|g;	
#	$parm =~ s|<frax;3;5>|\x{2157}|g;	
#	$parm =~ s|<frax;4;5>|\x{2158}|g;	
#	$parm =~ s|<frax;1;6>|\x{2159}|g;	
#	$parm =~ s|<frax;5;6>|\x{215A}|g;		
#	$parm =~ s|<frax;1;8>|\x{215B}|g;
#	$parm =~ s|<frax;3;8>|\x{215C}|g;
#	$parm =~ s|<frax;5;8>|\x{215D}|g;
#	$parm =~ s|<frax;7;8>|\x{215E}|g;
	
	$parm =~ s|<(frax;\d+;\d+)>|<?xpp $1?>|g;
	
	# and finally (catch all)...
	$parm =~ s|<(frax;)(.+?);(.+?)>|<?xpp $1$2;$3?>|g;
	$parm =~ s|;(frax;)(.+?);(.+?);|<?xpp $1$2;$3?>|g;
	
	$parm =~ s|;en;|\x{2002}|g;
	$parm =~ s|<en>|\x{2002}|g;
	$parm =~ s|;em;|\x{2003}|g;
	$parm =~ s|<em>|\x{2003}|g;
	
	#<frac;NDD-ADD;ADD>
	$parm =~ s|<(frac;)(.+?);(.+?)>|<?xpp $1$2;$3?>|g;
	$parm =~ s|;(frac;)(.+?);(.+?);|<?xpp $1$2;$3?>|g;

	#rules...
	# regex to match numbers:  /^[+-]?(\d+\.\d+|\d+\.|\.\d+|\d+)([eE][+-]?\d+)?$/
	#<ru; width; depth; x location; y location>  </ru;60q;0.5q>	NOTE:  Currently, NOT using x or y locations!!!!!	
	$parm =~ s|<(.?[/])ru;(.*[\d{0-255}])q;(.*[\d{0-255}])q>|<rule width="$2" width_units="points" weight="$3" weight_units="points"\/>|g;
	$parm =~ s|<(.?[/])ru;(.*[\d{0-255}])p;(.*[\d{0-255}])q>|<rule width="$2" width_units="picas" weight="$3" weight_units="points"\/>|g;
	$parm =~ s|<(.?[/])ru;(.*[\d{0-255}])i;(.*[\d{0-255}])q>|<rule width="$2" width_units="inches" weight="$3" weight_units="points"\/>|g;
	$parm =~ s|<ru;(.*[\d{0-255}])q;(.*[\d{0-255}])q>|<rule width="$1" width_units="points" weight="$2" weight_units="points"\/>|g;
	$parm =~ s|<ru;(.*[\d{0-255}])p;(.*[\d{0-255}])q>|<rule width="$1" width_units="picas" weight="$2" weight_units="points"\/>|g;
	$parm =~ s|<ru;(.*[\d{0-255}])i;(.*[\d{0-255}])q>|<rule width="$1" width_units="inches" weight="$2" weight_units="points"\/>|g;

	# 2019-01-28 Per Andrea, make appropriate changes to convert the following GenCode tags (using 'chevron' or ';' delimiters):
	# <namerule>       11 em-spaces
	# <daterule>        8 em-spaces
	# <rule>            7 em-spaces
	# <$rule>           5 em-spaces
	# <#rule>           5 em-spaces
	# <yrrule>          3 em-spaces

	$parm =~ s|<namerule>|___________|g;
	$parm =~ s|;namerule;|___________|g;
	# $parm =~ s|<daterule>|<rule width=".3" width_units="inches" weight=".5" weight_units="points"\/>\/<rule width=".3" width_units="inches" weight=".5" weight_units="points"\/>\/<rule width=".3" width_units="inches" weight=".5" weight_units="points"\/>|g;
	$parm =~ s|<daterule>|________|g;
	$parm =~ s|;daterule;|________|g;
 	$parm =~ s|<rule>|_______|g;
 	$parm =~ s|;rule;|_______|g;
 	$parm =~ s|<\$rule>|_____|g;
 	$parm =~ s|;\$rule;|_____|g;
	$parm =~ s|<\#rule>|_____|g;
	$parm =~ s|;\#rule;|_____|g;
	$parm =~ s|<yrrule>|___|g;
	$parm =~ s|;yrrule;|___|g;	
	# END 2019-01-28 changes Per Andrea
	
	# $parm =~ s|<namerule>|<rule width="1\.826" width_units="inches" weight=".5" weight_units="points"\/>|g;
	# $parm =~ s|;namerule;|<rule width="1\.826" width_units="inches" weight=".5" weight_units="points"\/>|g;
	# # $parm =~ s|<daterule>|<rule width=".3" width_units="inches" weight=".5" weight_units="points"\/>\/<rule width=".3" width_units="inches" weight=".5" weight_units="points"\/>\/<rule width=".3" width_units="inches" weight=".5" weight_units="points"\/>|g;
	# $parm =~ s|<daterule>|<rule width=\"1\.33\" width_units=\"inches\" weight=\"\.5\" weight_units=\"points\"\/>|g;
	# $parm =~ s|;daterule;|<rule width=\"1\.33\" width_units=\"inches\" weight=\"\.5\" weight_units=\"points\"\/>|g;
 	# $parm =~ s|<rule>|<rule width=\"1\.16\" width_units=\"inches\" weight=\"\.5\" weight_units=\"points\"\/>|g;
 	# $parm =~ s|;rule;|<rule width=\"1\.16\" width_units=\"inches\" weight=\"\.5\" weight_units=\"points\"\/>|g;
 	# $parm =~ s|<\$rule>|<rule width=\"\.83\" width_units=\"inches\" weight=\"\.5\" weight_units=\"points\"\/>|g;
 	# $parm =~ s|;\$rule;|<rule width=\"\.83\" width_units=\"inches\" weight=\"\.5\" weight_units=\"points\"\/>|g;
	# $parm =~ s|<\#rule>|<rule width="\.83" width_units="inches" weight=".5" weight_units="points"\/>|g;
	# $parm =~ s|;\#rule;|<rule width="\.83" width_units="inches" weight=".5" weight_units="points"\/>|g;
	# $parm =~ s|<yrrule>|<rule width="\.5" width_units="inches" weight=".5" weight_units="points"\/>|g;
	# $parm =~ s|;yrrule;|<rule width="\.5" width_units="inches" weight=".5" weight_units="points"\/>|g;	
	# # END 2019-01-28 changes Per Andrea
	
	$parm =~ s|<rum>|<rule width="1" width_units="inches" weight=".5" weight_units="points"\/>|g;
	$parm =~ s|<rum;(.*[\d])(.?[q])>|<rule width="1" width_units="inches" weight="$1" weight_units="points"\/>|g;
	$parm =~ s|.?[;<]xpdrule.?[;>]|<rule width="2" width_units="inches" weight=".5" weight_units="points"\/>|g;

	$parm =~ s|<(setrecto)(.+?)>|<?xpp $1$2?>|g;	 	
	$parm =~ s|<(setverso)(.+?)>|<?xpp $1$2?>|g;	 	
	$parm =~ s|<(setrecto)>|<?xpp $1?>|g;	 	
	$parm =~ s|<(setverso)>|<?xpp $1?>|g;
	$parm =~ s|<(setrev)(.+?)>|<?xpp $1$2?>|g;
	$parm =~ s|<(setrev)>|<?xpp $1?>|g;

	$parm =~ s|<(xref);(.+?)>|<?$1 $2?>|gi;	# Kind of non standard, but this works.  Better:  <?g2h xref;ignore/resume?>

	#Obsolete Page Layouts 
#	$parm =~ s|<(analysis)>|<$1\/>|g;	 	
	$parm =~ s|<(banner)>|<$1\/>|g; 	
	$parm =~ s|<begtake>||g;	 	
	$parm =~ s|<(binder)>|<$1\/>|g; 	
	$parm =~ s|<(cover)>|<$1\/>|g; 	
	$parm =~ s|<(double)>|<$1\/>|g; 	
	$parm =~ s|<endtake>||g;	 	
	$parm =~ s|<(foldout)>|<$1\/>|g; 	
	$parm =~ s|<(head>)>|<$1\/>|g;
	$parm =~ s|<(ixhead)>|<$1\/>|g; 	
	$parm =~ s|<(ixpage)>|<$1\/>|g; 
	$parm =~ s|<jstyle(.*?)>||g;	
	$parm =~ s|<(layout>legal)>|<$1\/>|g;	 	
	$parm =~ s|<(main)>|<$1\/>|g;	
	$parm =~ s|<(maindc)>|<?xpp $1?>|g;	
	$parm =~ s|<(npfnsp)>|<$1\/>|g; 	
	$parm =~ s|<(npxh)>|<$1\/>|g;	 	
	$parm =~ s|<(nrhp)>|<$1\/>|g;	 	
	$parm =~ s|<(nrhpfnsp)>|<$1\/>|g;	 	
	$parm =~ s|<(nrhpR90)>|<$1\/>|g;	 	
	$parm =~ s|<(nrhpxh)>|<$1\/>|g; 	
	$parm =~ s|<(official)>|<$1\/>|g;
	$parm =~ s|<(officials)>|<$1\/>|g;
	$parm =~ s|<(ord)>|<$1\/>|g;	 	
	$parm =~ s|<(preface)>|<$1\/>|g; 	
	$parm =~ s|<(prehead)>|<$1\/>|g;	 
	$parm =~ s|<(prelead);(.+?)>|<?xpp $1;$2?>|g;	
	$parm =~ s|<(restore)>|<$1\/>|g; 	
	$parm =~ s|<(rotatepg)>|<$1\/>|g;	 	
	$parm =~ s|<(setguide)>|<$1\/>|g;	 	
	$parm =~ s|<(setllpg)>|<$1\/>|g; 	
	$parm =~ s|<(setrev)>|<$1\/>|g; 	
	$parm =~ s|<(single)>|<$1\/>|g; 	
	$parm =~ s|<(tabs)>|<$1\/>|g;	 	
#	$parm =~ s|<(title)>|<$1\/>|g;	
	$parm =~ s|<(toc)>|<$1\/>|g; 	
	$parm =~ s|<(tochead)>|<$1\/>|g;	 	
	$parm =~ s|<(tocpage)>|<$1\/>|g;	 	
	$parm =~ s|<(prefolio)>|<$1\/>|g;	
	$parm =~ s|<(setskipl;\d+)>|<?xpp $1?>|g;	
	$parm =~ s|<(setl;\d+;\d+)>|<?xpp $1?>|g;
	
	# xml &...(amp, apos, gt, lt, quot)
	
	if(
		($parm =~ /\&/)			|
		($parm =~ /<lt>/)		|
		($parm	 =~ /<gt>/)
	)	{
		$parm = p1_ampersandProcessing($parm);
	}
	
	# BEGINS :  ;ebook;html;none;print>...</selectout>		2013-05-29
	if($parm =~ /(.+?)<selectout/) { 
		$parm =~ s|(<selectout)|~~~$1|g;	
		$parm =~ s|(<\?xpp )|~~~$1|g;
		@part = ();
		@part = split "~~~", $parm;		
		
		for(my $pt = 0; $pt <= $#part; $pt++) {
			$part[$pt] =~ s|^<selectout;(.+?);(.+?);(.+?);(.+?)>|<selectout out=\"$1 $2 $3 $4\">|g;
			$part[$pt] =~ s|^<selectout;(.+?[^>]);(.+?[^>]);(.+?[^>])>|<selectout out=\"$1 $2 $3\">|g;
			$part[$pt] =~ s|^<selectout;(.+?);(.+?)>|<selectout out=\"$1 $2\">|g;
			$part[$pt] =~ s|^<selectout;(.+?)>|<selectout out=\"$1\">|g;

			# if 'none' exists in parameters, then replace all parameters with 'none'...
			if($part[$pt] =~ /none/) {
				$part[$pt] =~ s|(<selectout out\=)\"(.+?)\">|$1\"none\">|g;
			}
		}
		
		$parm = join "", @part;			
	}
	
	return($parm);
}


#----------------------------------------------------------------------
sub fv {
#----------------------------------------------------------------------
	# manage conversion of font variant tags...
	my $parm = shift;

	$parm =~ s|<fv;0>|<med>|g;
	$parm =~ s|<fv;1>|;b;|g;
	$parm =~ s|<fv;2>|;i;|g;
	$parm =~ s|<fv;3>|;bi;|g;
	
	#Convert GenCode legacy to standard XyMacro font variants...
	# BOLD...
	$parm =~ s|;b;|<bold>|g;
	$parm =~ s|;\\b;|<\/bold>|g;
	$parm =~ s|;\/b;|<\/bold>|g;
	$parm =~ s|<b>|<bold>|g;
	$parm =~ s|<\\b>|<\/bold>|g;
	$parm =~ s|<\/b>|<\/bold>|g;
	
	# ITALIC...
	$parm =~ s|;i;|<ital>|g;
	$parm =~ s|;\\i;|<\/ital>|g;
	$parm =~ s|;\/i;|<\/ital>|g;
	$parm =~ s|<i>|<ital>|g;
	$parm =~ s|<\\i>|<\/ital>|g;
	$parm =~ s|<\/i>|<\/ital>|g;	
	
	# BOLD-ITALIC...
	$parm =~ s|;bi;|<bdit>|g;
	$parm =~ s|;\\bi;|<\/bdit>|g;
	$parm =~ s|;\/bi;|<\/bdit>|g;
	$parm =~ s|<bi>|<bdit>|g;
	$parm =~ s|<\\bi>|<\/bdit>|g;
	$parm =~ s|<\/bi>|<\/bdit>|g;	
	
	# SUBSCRIPT (inferior)...
	$parm =~ s|;sub;|<inf>|g;
	$parm =~ s|;\\sub;|<\/inf>|g;
	$parm =~ s|;\/sub;|<\/inf>|g;
	$parm =~ s|<sub>|<inf>|g;
	$parm =~ s|<\\sub>|<\/inf>|g;
	$parm =~ s|<\/sub>|<\/inf>|g;	
	
	# SUPERSCRIPT...
	$parm =~ s|;sup;|<sup>|g;
	$parm =~ s|;\\sup;|<\/sup>|g;
	$parm =~ s|;\/sup;|<\/sup>|g;
	$parm =~ s|<sup>|<sup>|g;
	$parm =~ s|<\\sup>|<\/sup>|g;
	$parm =~ s|<\/sup>|<\/sup>|g;	
	
	# UNDERLINE...
	$parm =~ s|;ul;|<ul>|g;
	$parm =~ s|;\\ul;|</ul>|g;
	$parm =~ s|;\/ul;|</ul>|g;
	
	if(
		($parm =~ /<bold>/)		|
		($parm =~ /<\/bold>/)	|
		($parm =~ /<ital>/)		|
		($parm =~ /<\/ital>/)	|
		($parm =~ /<bdit>/)		|
		($parm =~ /<\/bdit>/)	|
		($parm =~ /<med>/)		|
		($parm =~ /<\/med>/)	|
		($parm =~ /<inf>/)		|
		($parm =~ /<\/inf>/)	|
		($parm =~ /<sup>/)		|
		($parm =~ /<\/sup>/)	|
		($parm =~ /<ul>/)		|
		($parm =~ /<\/ul>/)
		
		)	{
			$parm = p1_FontVariants($parm);		# Process Font Variants, balance missing closing tags, etc.
	}
	
	return $parm;
}


#----------------------------------------------------------------------
sub p1_FontVariants {
#----------------------------------------------------------------------
	my $myParm = shift;	

#print OUT "<!-- DEBUG [p1_FontVariants]\n$myParm\n -->\n";
	
	my $newParm = undef;
	my $tmpParm = "";
	my @pgraph = ();				# Used to hold multiple pgraphs of $myParm
	my $myPgraph = "";
	my $sHold = "";
	my @FV_openStack = ();
	my $tagOffset = 0;
	my $frontIndex = 0;
	my $str = "";
	my $ctr = 0;
	my $key = "";
	my $keyCount = 0;
	my $keysProcessed = 0;
	my $sub1 = 0;
	my $sub2 = 0;
	my $minTagName = "";
	my $fin = "not finished";
	my $myDump = "";
	my %variant;
			
	# NOTE:  Number of possible occurances for a given $key in a given $myPara, may be modified.
	my $minTagValue = 8192;			
			
	@pgraph = split(/(\\)|(;eol;)|(<\?xpp.q[alcr]\?>)|(<[T]r)|(<Tc)/, "$myParm");

	#work on a "pgraph" at a time...
	foreach $myPgraph (@pgraph)	{
		
		# Variant HASH (init)...
		%variant = (
			"<bold>"	=> [],
			"</bold>"	=> [],
			"<bdit>"	=> [],
			"</bdit>"	=> [],
			"<ital>"	=> [],
			"</ital>"	=> [],
			"<med>"	=> [],
			"</med>" => [],
			"<inf>"	=> [],
			"</inf>"	=> [],
			"<sup>"	=> [],
			"</sup>"	=> []			
		);

		# Load the %variant hash...		
		foreach $key (keys %variant)	{
			($ctr, $sub1) = 0;
			
			while($ctr > -1)	{
				$ctr = index($myPgraph, $key, $sub1);
				
				if($ctr > -1) {
					push @{$variant{$key}}, $ctr;
					$sub1 = $ctr + length($key);
				}
			}
		}
		
		# Set $keyCount to number of variants...
		foreach $key (keys %variant)	{
			if($#{$variant{$key}} > -1)	{	# has array elements...
				$keyCount++;		# The number of keys
			}
		}
	
		#Build $newParm string...
		$sHold = "";
		$fin = "NOTFIN";
		$frontIndex = 0;
		
		while( $fin ne "FIN")	{
			$keysProcessed = 0;
			$minTagName = "";
			$minTagValue = 8192;			
			
			#locate next variant (smallest subscript) to process...
			foreach $key (keys %variant)	{
				# Find minTagName and minTagValue...
				if( ($#{$variant{$key}} > -1) & (${$variant{$key}}[0] < $minTagValue )	)	{
					$minTagName = $key;
					$minTagValue = @{$variant{$key}}[0];
					$keysProcessed++;
				}
			}
						
			$tagOffset = shift @{$variant{$minTagName}};		# $tagOffset will = the offset/index to $minTagName...

			if($minTagName ne "") {
				#------------------			
				#  HAVE A TAG!
				#------------------
				if(substr($minTagName, 1, 1) ne "\/")	{
					#------------------			
					# OPEN TAG
					#------------------			
	 				push @FV_openStack, "$minTagName";
					$sHold = $sHold . substr($myPgraph, $frontIndex, ($tagOffset - $frontIndex));
					$sHold = $sHold . $minTagName;
					$frontIndex = $tagOffset + length($minTagName);
	 				
				} else {
					#------------------			
					# CLOSE TAG
					#------------------			
	 				# Does $minTagName have matching open tag on the stack?...
	 				if(	(@FV_openStack) && (substr($FV_openStack[$#FV_openStack], 1)	eq substr($minTagName, 2))	)	{
						pop @FV_openStack;
					} else {
						$sHold = $sHold . "<" . substr($minTagName, 2);
					}
					
 					$sHold = $sHold . substr($myPgraph, $frontIndex, ($tagOffset - $frontIndex)) . $minTagName;
					$frontIndex = $tagOffset + length($minTagName);
				}
				
				if($#{$variant{$minTagName}} < 0)	{	# NO array elements...
	  				delete $variant{$minTagName};	
				}
			} else {
				#------------------			
				#  NO MORE TAGS!
				#------------------
				$sHold = $sHold . substr($myPgraph, $frontIndex);

				while (@FV_openStack)	{
					$str = pop @FV_openStack;
					$sHold = $sHold . "<\/" . substr($str,1, (length($str) - 1));	
				}
			
			}	# END if($minTagName ne "")...

			if($keysProcessed == 0)	{
				$fin = "FIN";
			}	
						
		} # END While()  - Building $newParm string
	
		$newParm = $newParm . $sHold;

	}	# END foreach pgraph...
	
	# strip off last backslash...
	if(	substr($newParm, length($newParm) - 1, 1)	eq "\\")	{
		$newParm = substr($newParm, 0, -1);
	}

	return($newParm);
}


#----------------------------------------------------------------------
sub p1_OpenContainerTag {
#----------------------------------------------------------------------
	my $parm = shift;
	my $me = whoami();

	if($parm !~ /;[fr]n[;\d]/)	{		# NOT ;fn#  ;rn# 
# 2016-05-20		$parm =~ s|^([ \t]*)([<;])(.+?)([;>])$|<$3>|g;	
		if($parm =~ /~/) {
			$parm =~ s|^([ \t]*)([<;])(.+?)([;>])~(.*?)$|<$3>~$5|g;	
		} else {
			$parm =~ s|^([ \t]*)([<;])(.+?)([;>])$|<$3>|g;	
		}
		
		print(OUT "<endtag><!-- NOT fn or rn -->\n$parm\n");	# echo line read	
		
		if($output_buffer ne "")	{
			print(OUT "$output_buffer\n");
			$output_buffer="";
		}
		
		if ($parm !~ /~~~oh5a/ ) {
			$fnLevel = substr($parm, -2, 1);		# container level of this tag
		} else {
			$fnLevel = 0;
		}
				
	} elsif($parm =~ /;rn\d/)	{			
		$parm =~ s|(^([ \t]*)[<;])(.+)([;>]$)|<$3>|g;	
		print(OUT "$parm\n");	# echo line read		
				
	} else {
		# ;fn;
		
		if($parm =~ /<[fh][lcr]\d>/)	{
			$fnLevel = 0;	
		}
		
		$fnLevel++;
		$parm =~ s|(^([ \t]*)[<;])(.+?)([;>]$)|<$3$fnLevel>|g;
		print OUT "$parm\n";
		$fnLevel2Close = $fnLevel;
		@fnStack = ();
	} 
		
	return(0);
}


##----------------------------------------------------------------------
#sub p1_endTag {
##----------------------------------------------------------------------
#	my $parm = shift;
#	my $me = whoami();
#	
#	print(OUT "<endtag>\n");
#	
#	return(0);
#}


#----------------------------------------------------------------------
sub p1_Std2XML{
#----------------------------------------------------------------------
	$parm = shift;
	$parm =~ s|(^[<;])(.+?)(;)(.*)([;>]$)|<$2>$4</$2>|;	
	print(OUT "$parm\n"); # echo line read	
	
	return(0);
}


#----------------------------------------------------------------------
sub p1_XPPmeasurementConversion {
#----------------------------------------------------------------------
	my $parm = shift;

	#----------------------------------
	# 'sz' - Font Size and Extra Leading
	#----------------------------------
	# After day of research, only two ways of entering the 'sz' tag/macro work:
	# (1) GenCode tag - ;sz=(\d*+)\.(\d*+)(.?[qpcdimknuz]);  (EXTRA LEADING IS ALWAYS REPLACED WITH '2q') NO internal ';' char allowed.
	# (2) XyMacro - <sz;(\d*+)\.(\d*+)(.?[qpcdimknuz]);(\d*+)\.(\d*+)(.?[qpcdimknuz])> BEHAVES as intended.
	#
	# GenCode sz= (only one parameter is legal)
	$parm =~ s/;sz\=(\d+\.\d+|\d+)(.?[qpcdimknuz]);/<?xpp sz;$1$2?>/g;			# DECIMAL OR INTEGER

	# XyMacro sz
	$parm =~ s|<sz;(.+?)>|<\?xpp sz;$1\?>|g;
	
	#----------------------------------
	# 'adv' - (GenCode) Advance Vertically
	#----------------------------------
	# GenCode adv
#	$parm =~ s/;adv\=(\d+\.\d+|\d+)(.?[qpcdimknuz]);/<\?xpp adv;$1$2\?>/g;		# DECIMAL OR INTEGER		
	
	#----------------------------------
	# 'le' - Interline Leading:
	#----------------------------------
	$parm =~ s/;le\=(\d+\.\d+|\d+)(.?[qpcdimknuz]);/<\?xpp le;$1$2\?>/g;			# DECIMAL OR INTEGER	
	$parm =~ s/;le\=0;/<\?xpp le;0\?>/g;	# ZERO

	# XyMacro le
	$parm =~ s|<le;(.+?)>|<\?xpp le;$1\?>|g;	
	
	
	#----------------------------------
	# 'lp' - Preleading:
	#----------------------------------
	$parm =~ s/;lp\=(\d+\.\d+|\d+)(.?[qpcdimknuz]);/<\?xpp lp;$1$2\?>/g;		# DECIMAL OR INTEGER	
	$parm =~ s/;lp\=&(\d+\.\d+|\d+)(.?[qpcdimknuz]);/<\?xpp lp;&$1$2\?>/g;		# DECIMAL OR INTEGER	
	$parm =~ s/;lp\=&-(\d+\.\d+|\d+)(.?[qpcdimknuz]);/<\?xpp lp;&-$1$2\?>/g;	# DECIMAL OR INTEGER	

	# XyMacro lp
	$parm =~ s|<lp;(.+?)>|<\?xpp lp;$1\?>|g;		
	$parm =~ s|<\?xpp lp;(.+?)>||g;
	
	#----------------------------------
	# 'me' - Line Measure:
	#----------------------------------
	$parm =~ s/;me\=(\d+\.\d+|\d+)(.?[qpcdimknuz]);/<\?xpp me;$1$2\?>/g;		# DECIMAL OR INTEGER	
	$parm =~ s/;me\=&(\d+\.\d+|\d+)(.?[qpcdimknuz]);/<\?xpp me;&$1$2\?>/g;		# DECIMAL OR INTEGER	
	$parm =~ s/;me\=&-(\d+\.\d+|\d+)(.?[qpcdimknuz]);/<\?xpp me;&-$1$2\?>/g;	# DECIMAL OR INTEGER
	$parm =~ s/;me\=0;/<\?xpp me;0\?>/g;	# ZERO

	# XyMacro me
	$parm =~ s|<me;(.+?)>|<\?xpp me;$1\?>|g;		
	
	return $parm;
}

#----------------------------------------------------------------------
sub p1_CloseContainerTag {
#----------------------------------------------------------------------
	my $parm = shift;
	$parm =~ s|(^[<;])(\\)(.+?)([;>]$)|</$3>|;	
	print(OUT "$parm\n"); # echo line read	
	
	return(0);
}


#----------------------------------------------------------------------
sub p1_COtagsWrapping {
#----------------------------------------------------------------------
	my $line = shift;
	my $NotFirstCO = 0;
	my $str = "";
	
	$line =~ s|(<co;)|~~~$1|g;	
	my @Aco = split(/~~~/, $line);	

#print STDOUT "\n\n++++++++++++\n\nBEFORE...$#Aco\n++++++++++++\n";					
#for(my $cox=0; $cox <= $#Aco; $cox++) {
#	print STDOUT "\t[$cox] $Aco[$cox]\n";
#}
	
	for(my $cox=0; $cox <= $#Aco; $cox++) {
		
		if($Aco[$cox] =~ /^<co;/) {
			$NotFirstCO++;
			$Aco[$cox] =~ s|<co;(.+?)>|<\?xpp co;$1\?>|g;
			
			if($NotFirstCO > 1) {
				$Aco[$cox] = "<\?xpp coend\?>" . $Aco[$cox];	
			}
			
			if(($cox == $#Aco) and ($Aco[$cox] =~ /^<\?xpp coend/)) {
				$Aco[$cox] = $Aco[$cox] . "<\?xpp coend\?>";
			}
		}	
	}

#print STDOUT "\n\n++++++++++++\n...AFTER $#Aco\n++++++++++++\n";					
#for(my $cox=0; $cox <= $#Aco; $cox++) {
#	print STDOUT "\t[$cox] $Aco[$cox]\n";	
#}	

	# Put everything back together
	$str = "";
	for(my $cox=0; $cox <= $#Aco; $cox++) {
		$str = $str . $Aco[$cox];
	}

#print STDOUT "\n$str\n\n";
	
	return $str;
}


##############################################
#    PASS-2 SUBROUTINES
##############################################
#----------------------------------------------------------------------
sub p2_blankLine {
#----------------------------------------------------------------------
	my $parm = "--empty--";
	print(OUT "$parm\n"); # echo line read	

	return(0);
}


#----------------------------------------------------------------------
sub p2_remove {
#----------------------------------------------------------------------
	my $parm = shift;
	print(OUT "<REMOVE>\n");
	print(OUT "$parm\n"); # echo line read	
	print(OUT "</REMOVE>\n");
	
	return(0);
}


#----------------------------------------------------------------------
sub p2_AOHTag {
#----------------------------------------------------------------------
	my $parm = shift;
	
	if	($parm =~ /^[ \t]*<aoh0>/)	{ push @tagStack, "aoh0";  }
	elsif($parm =~ /^[ \t]*<aoh1>/)	{ push @tagStack, "aoh1";  }
	elsif($parm =~ /^[ \t]*<aoh2>/)	{ push @tagStack, "aoh2";  }
	elsif($parm =~ /^[ \t]*<aoh3>/)	{ push @tagStack, "aoh3";  }
	elsif($parm =~ /^[ \t]*<aoh4>/)	{ push @tagStack, "aoh4";  }
	elsif($parm =~ /^[ \t]*<aoh5>/)	{ push @tagStack, "aoh5";  }
	elsif($parm =~ /^[ \t]*<aoh6>/)	{ push @tagStack, "aoh6";  }
	elsif($parm =~ /^[ \t]*<aoh7>/)	{ push @tagStack, "aoh7";  }

	print(OUT "<REMOVE>\n");
	print(OUT "$parm\n"); # echo line read	

	return(0);
}

#----------------------------------------------------------------------
sub p2_AAOHTag {
#----------------------------------------------------------------------
	my $parm = shift;
	
	if	($parm =~ /^[ \t]*<aaoh0>/)	{ push @tagStack, "aaoh0";  }
	elsif($parm =~ /^[ \t]*<aaoh1>/)	{ push @tagStack, "aaoh1";  }
	elsif($parm =~ /^[ \t]*<aaoh2>/)	{ push @tagStack, "aaoh2";  }
	elsif($parm =~ /^[ \t]*<aaoh3>/)	{ push @tagStack, "aaoh3";  }
	elsif($parm =~ /^[ \t]*<aaoh4>/)	{ push @tagStack, "aaoh4";  }
	elsif($parm =~ /^[ \t]*<aaoh5>/)	{ push @tagStack, "aaoh5";  }
	elsif($parm =~ /^[ \t]*<aaoh6>/)	{ push @tagStack, "aaoh6";  }
	elsif($parm =~ /^[ \t]*<aaoh7>/)	{ push @tagStack, "aaoh7";  }

	print(OUT "<REMOVE>\n");
	print(OUT "$parm\n"); # echo line read	

	return(0);
}

#----------------------------------------------------------------------
sub p2_AOXHTag {
#----------------------------------------------------------------------
	my $parm = shift;
	
	if	($parm =~ /^[ \t]*<aoxh0>/)	{ push @tagStack, "aoxh0";  }
	elsif($parm =~ /^[ \t]*<aoxh1>/)	{ push @tagStack, "aoxh1";  }
	elsif($parm =~ /^[ \t]*<aoxh2>/)	{ push @tagStack, "aoxh2";  }
	elsif($parm =~ /^[ \t]*<aoxh3>/)	{ push @tagStack, "aoxh3";  }
	elsif($parm =~ /^[ \t]*<aoxh4>/)	{ push @tagStack, "aoxh4";  }
	elsif($parm =~ /^[ \t]*<aoxh5>/)	{ push @tagStack, "aoxh5";  }
	elsif($parm =~ /^[ \t]*<aoxh6>/)	{ push @tagStack, "aoxh6";  }
	elsif($parm =~ /^[ \t]*<aoxh7>/)	{ push @tagStack, "aoxh7";  }

	print(OUT "<REMOVE>\n");
	print(OUT "$parm\n"); # echo line read	

	return(0);
}

#----------------------------------------------------------------------
sub p2_OhTag {
#----------------------------------------------------------------------
	my $parm = shift;
	my $p2_OHvariant = "";

	if($parm =~ /^(.+?)\~(.*?)$/) {
		$p2_OHvariant = $parm;
		$p2_OHvariant =~ s|^(.+?)\~(.*?)$|~$2|g;
		$parm =~ s|^(.+?)\~(.*?)$|$1|g;
	} else {
		$p2_OHvariant = "";
	}

 	my $tagLength=length($parm);
	my $tagNo = substr($parm, ($tagLength - 2), 1);
	my @aTag = split(/<|>|\d/, $parm);
	my $tagType = $aTag[1];
 	my $popTagNo = -1;
 	my $popTagType = "";
	
	my @theTagNo = $tagNo;		#single item array
	my @theTagType = $tagType;
	
	CloseOpenTagStackEntries();
		
	if(@OHNoStack)	{		#any array elements?
		
			if($tagNo gt $OHNoStack[$#OHNoStack])	{				# Greater than
				push @OHTypeStack, @theTagType;	
				push @OHNoStack, @theTagNo;
				
				if($p2_OHvariant ne "") {
					print(OUT "\n$parm $p2_OHvariant\n"); # echo line read
				} else {
					print(OUT "\n$parm\n"); # echo line read
				}
			
			} elsif($tagNo eq $OHNoStack[$#OHNoStack]) {		# Equal to
				$popTagNo = pop @OHNoStack;
				$popTagType = pop @OHTypeStack;
				print(OUT "</" . $popTagType . $popTagNo . ">\n\n");
				push @OHTypeStack, @theTagType;
				push @OHNoStack, @theTagNo;

				if($p2_OHvariant ne "") {
					print(OUT "\n$parm $p2_OHvariant\n"); # echo line read
				} else {
					print(OUT "\n$parm\n"); # echo line read
				}
				
			} elsif($tagNo lt $OHNoStack[$#OHNoStack]) {			# Less than

				while($tagNo le $OHNoStack[$#OHNoStack] ) {
						$popTagNo = pop @OHNoStack;
						$popTagType = pop @OHTypeStack;
						
						if($tagNo le $popTagNo)	{
							if($p2_OHvariant ne "") {
								print(OUT "</" . $popTagType . $popTagNo . "> $p2_OHvariant\n\n");
							} else {
								print(OUT "</" . $popTagType . $popTagNo . ">\n\n");
							}
						}
				}
				
				push @OHTypeStack, @theTagType;
				push @OHNoStack, @theTagNo;

				if($p2_OHvariant ne "") {
					print(OUT "\n$parm $p2_OHvariant\n"); # echo line read
				} else {
					print(OUT "\n$parm\n"); # echo line read
				}
				
			} else {
				print(STDOUT "\n\n\nERROR IN p2_OhTag()\n");
			}
		
	} else {
		push @OHNoStack, @theTagNo;
		push @OHTypeStack, @theTagType;

				if($p2_OHvariant ne "") {
					print(OUT "\n$parm $p2_OHvariant\n"); # echo line read
				} else {
					print(OUT "\n$parm\n"); # echo line read
				}
	}
				
	return(0);
}


#----------------------------------------------------------------------
sub p2_OpenTable {
#----------------------------------------------------------------------
	$G_OpenTableFlag = "yes";
	@G_TableFootnotes = ();
}


#----------------------------------------------------------------------
sub p2_CloseTable {
#----------------------------------------------------------------------
	if($#G_TableFootnotes > -1)	{
	
		foreach my $tfn (@G_TableFootnotes)	{
			print(OUT "$tfn\n");	
		}
		
		# 20100921...
		if($footnotePending eq "yes")	{
			print OUT "<\/table>\n";	
		}		
	}
	
	$G_OpenTableFlag = "no";
	$footnotePending = "no";
}


#----------------------------------------------------------------------
sub p2_NoteTag {
#----------------------------------------------------------------------
	my $parm = shift;
	
	push @tagStack, "note";
	print(OUT "$parm\n"); # echo line read	
		
	return(0);
}


#----------------------------------------------------------------------
sub p2_CloseContainerTag {
#----------------------------------------------------------------------
	my $me = whoami();
	my $LastOpenTag = "";

	if($#tagStack == -1) {
		# do nothing...		
	} else {
		
			$LastOpenTag = pop @tagStack;

			if( $LastOpenTag 		=~ /^note/)				{
				print(OUT "<\/$LastOpenTag>\n");
			}
			elsif($LastOpenTag 	=~ /^aoh(\d)/)		{
				print(OUT "<\/$LastOpenTag>\n"); 
				print(OUT "<\/REMOVE>\n");	
			} 
			elsif($LastOpenTag 	=~ /^aaoh(\d)/)		{
				print(OUT "<\/$LastOpenTag>\n"); 
				print(OUT "<\/REMOVE>\n");	
			}
			elsif($LastOpenTag 	=~ /^aoxh(\d)/)		{
				print(OUT "<\/$LastOpenTag>\n");
				print(OUT "<\/REMOVE>\n");	
			}
			elsif($LastOpenTag 	=~ /^historynote/)	{
				print(OUT "<\/$LastOpenTag>\n"); 
				print(OUT "<\/REMOVE>\n");	
			}
			elsif($LastOpenTag 	=~ /^main/)				{
				print(OUT "<\/$LastOpenTag>\n");
			}
			elsif($LastOpenTag 	=~ /^maindc/)			{
				print(OUT "<\/$LastOpenTag>\n"); 
				print(OUT "<\/REMOVE>\n");	
			}
			else	{ print(OUT "+++ No closing for:  $LastOpenTag +++\n"); }
	
	}	#end of else...

	return(0);
}

#----------------------------------------------------------------------
sub CloseOpenStackEntries {
#----------------------------------------------------------------------
	my $popTagNo = -1;
	my $popTagType = "";
	
	CloseOpenTagStackEntries();
		
#  	print(OUT "\n...inside CloseOpenStackEntries...\$#OHNoStack=$#OHNoStack\n");
# 	dumpPop(@OHTypeStack);
	
	while($#OHNoStack > -1)	{
		$popTagNo = pop @OHNoStack;
		$popTagType = pop @OHTypeStack;
		print(OUT "</" . $popTagType . $popTagNo . ">\n");
	}

	return(0);
}


#----------------------------------------------------------------------
sub CloseOpenTagStackEntries {
#----------------------------------------------------------------------
	my $popTagNo = -1;
	my $popTagType = "";
		
	while($#CTNoStack > -1)	{
		$popTagNo = pop @CTNoStack;
		$popTagType = pop @CTTypeStack;
		print(OUT "</" . $popTagType . $popTagNo . ">\n");
	}
	
	#init Tag Stack & fnStack arrays:
	@tagStack = ();
	@fnStack = ();
	
	return(0);
}


#----------------------------------------------------------------------
sub p2_ClosePendingFootnoteTags {
#----------------------------------------------------------------------
	my $parm = shift;
	
#print OUT "\n<!-- [p2_ClosePendingFootnoteTags]  CTNoStack: -->\n";	
#for(my $x = 0; $x <= $#CTNoStack; $x++)	{
#	print OUT	"<!-- CTNoStack[$x] = $CTNoStack[$x] -->\n";
#}
#
#print OUT "\n<!-- [p2_ClosePendingFootnoteTags] fnStack: -->\n";	
#for(my $x = 0; $x <= $#fnStack; $x++)	{
#	print OUT	"<!-- fnStack[$x] = $fnStack[$x] -->\n";
#}
		

	if($#CTNoStack > -1)	{
		
		for(my $i=$#fnStack; $i > -1; $i--)	{
			# print OUT "<\/$fnStack[$i]>\n";			#20100806
			# IF...added 20100806
			if($G_OpenTableFlag eq "no")	{
				print OUT "<\/$fnStack[$i]>\n"; # echo line read
			}else{
				push @G_TableFootnotes, "<\/$fnStack[$i]>";	
			}
			
			#Hmmm...  getting rid of CTNoStack and CTTypeStack (duplicate) entries...
			pop @CTNoStack;
			pop @CTTypeStack;
		}	
		
	} else {
		@fnStack = ();
	}
	
}


# Process Container Tags...
#----------------------------------------------------------------------
sub p2_CTTag {
#----------------------------------------------------------------------
	my $parm = shift;	
	my $me = whoami();
 	my $tagLength=length($parm);
	my $tagNo = substr($parm, ($tagLength - 2), 1);
	my $tagType = substr($parm, 1, -2);
#	my @aTag = split(/<|>|\d/, $parm);
#	my $tagType = $aTag[1];
	
 	my $popTagNo = -1;
 	my $popTagType = "";
 	
 	my $TableFN=0;
 	my $fnstring="";
 	

 	if($footnotePending eq "yes"  and  $G_OpenTableFlag eq "yes")	{
 		$TableFN=1;
	}
	
	my @theTagNo = $tagNo;		#single item array
	my @theTagType = $tagType;

	if($#CTNoStack > -1)	{		#any array elements?	
			if($tagNo gt $CTNoStack[$#CTNoStack])	{			# Greater than
				push @CTTypeStack, @theTagType;	
				push @CTNoStack, @theTagNo;

				# print(OUT "$parm\n"); # echo line read				#20100806
				# IF... 20100806
				if(not($TableFN))	{
					print(OUT "$parm\n"); # echo line read
				}else{
					push @G_TableFootnotes, "$parm\n";	
				}
				
				if( $footnotePending eq "yes")  { push @fnStack, "$tagType$tagNo";}				
			
			} elsif($tagNo eq $CTNoStack[$#CTNoStack]) {			# Equal to
				$popTagNo = pop @CTNoStack;
				$popTagType = pop @CTTypeStack;

				#print(OUT "</" . $popTagType . $popTagNo . ">\n");			#20100806
				# IF... 20100806
				if(not($TableFN)	)	{
					print(OUT "</" . $popTagType . $popTagNo . ">\n");
				}else{
					$fnstring="</" . $popTagType . $popTagNo . ">\n";
					push @G_TableFootnotes, $fnstring;	
				}
								
				if( $footnotePending eq "yes")  { 
					pop @fnStack;	
				}				

				push @CTTypeStack, @theTagType;
				push @CTNoStack, @theTagNo;
				# print(OUT "$parm\n"); # echo line read		#20100806
				# IF... 20100806
				if(not($TableFN))	{
					print(OUT "$parm\n"); # echo line read
				}else{
					push @G_TableFootnotes, "$parm\n";	
				}				
				
				if( ($footnotePending eq "yes") and ($tagType !~ /[fh][lrc]\d/) ) { push @fnStack, "$tagType$tagNo"; }	
				
			} elsif($tagNo lt $CTNoStack[$#CTNoStack]) {			# Less than
				
				while($tagNo le $CTNoStack[$#CTNoStack]) {
						$popTagNo = pop @CTNoStack;
						$popTagType = pop @CTTypeStack;
						if( $footnotePending eq "yes")  { pop @fnStack;	}				
						
						if($tagNo le $popTagNo)	{
							#print(OUT "</" . $popTagType . $popTagNo . ">\n");		#20100806
							
							# IF... 20100806
							if(not($TableFN)	)	{
								print(OUT "</" . $popTagType . $popTagNo . ">\n");
							}else{
								$fnstring="</" . $popTagType . $popTagNo . ">\n";
								push @G_TableFootnotes, $fnstring;	
							}							
						}
				}
				
				push @CTTypeStack, @theTagType;
				push @CTNoStack, @theTagNo;
				# print(OUT "$parm\n"); # echo line read		#20100806
				# IF... 20100806
				if(not($TableFN))	{
					print(OUT "$parm\n"); # echo line read
				}else{
					push @G_TableFootnotes, "$parm\n";	
				}				
				
				if( $footnotePending eq "yes")  { push @fnStack, "$tagType$tagNo";}				
				
			} else {
				print(STDOUT "\n\n\nERROR IN p2_Tag()\n");
			}
		
	} else {
		
		push @CTNoStack, @theTagNo;
		push @CTTypeStack, @theTagType;
		# print(OUT "$parm\n"); # echo line read		#20100806
		# IF... 20100806
		if(not($TableFN))	{
			print(OUT "$parm\n"); # echo line read
		}else{
			push @G_TableFootnotes, "$parm\n";	
		}	
			
		if( ($footnotePending eq "yes") and ($tagType !~ /[fh][lrc]/) )  { push @fnStack, "$tagType$tagNo"; }				
	}

	return(0);
}

#----------------------------------------------------------------------
sub p4_CALS	{
#----------------------------------------------------------------------
#	NOTE:  PASS-TWO converted the <begtab -> <table (incl. closing tag)
	#REMOVED	non-xymacro(s); should be in "sys" definition
	my $parm = shift;		# initial "<table" line...
	my $sCALS = "";
	my $index = 0;
	my @temp = ();
	my $tempStr = "";
	my @T = ();
	
		
	push @T, "$parm";
	
	while($parm !~ /<\/table>/)	{
		
		$parm=readline(FILE);						# read next line in FILE...
		chomp($parm);
		
		if(length($parm) > 0) {
			@temp = ();
			@temp = split(/(<Tr)|(<Tc)/, "$parm");
			
			if( $#temp > 1)	{
			
				for($index=0; $index<=$#temp; $index++ ) {
				
					if( defined($temp[$index])  )	{
						
						if($index > 0)	{
							
							if( $temp[$index] !~ /<T[rc]/ )	{
								
								if($index == $#temp)	{		    # last in table...
									$tempStr .= $temp[$index];
									push @T, $tempStr	;
								} else {
									$tempStr .= $temp[$index];
								}
								
							} else {	#<Tr or <Tc...
									push @T, $tempStr	;			# push previous contents of $tempStr...
									$tempStr = $temp[$index];
							}
							
						} else {	# $index == 0
							$tempStr = $temp[$index];
						}
						
					}
				} #END for(...)
				
			} else {
				push @T, "$parm";
			}
		} # END if length
	} # END while not </table>
	
	# remove ;hg; from all table cell content...
	for(my $h=0; $h<=$#T; $h++) {
		$T[$h] =~ s|;hg;||g;
	}
				
	$sCALS = ConvertTable(@T);
	
	my $rtn_p4_CALS = "\n\n$sCALS\n\n";
	return($rtn_p4_CALS);
}


#----------------------------------------------------------------------
sub p5_BookTag {
#----------------------------------------------------------------------
	$p4_ContainingElement="book";
	print(OUT "<book $SchemaLevelBlurb origin=\"$infile\">\n");		# Containing element... 
	print(OUT "\t<bookinfo>\n");
	print(OUT "\t\t<title/>\n");
	print(OUT "\t\t<subtitle/>\n");
	print(OUT "\t</bookinfo>\n\n");
}
		
		
#----------------------------------------------------------------------
sub p5_FootnotePending {
#----------------------------------------------------------------------
	my $line = shift;
	
	if( $footnotePending eq "yes")  {		
		$CloseThisTag = $line;

		if($line =~ /<\/listitem>/ )	{			
			$CloseListitem = "</content>\n</listitem>";
		}
		
	} else {  																				
		print OUT "$line\n"; 
	}
}


# Convert Pseudo Container tags to Schema Container tags and
# Create Title and Subtitle elements
#----------------------------------------------------------------------
sub p5_OHTag {
#----------------------------------------------------------------------
	my $GenCodeFile = shift;
	my $line = shift;
	
	my $OHvariant = "";
	my $OHvariantOut = "";
	
#print STDOUT "\n[debug-A 5_OHTag \$line]: $line\n";

	if($line =~ /^(.+?) \~(.*?)$/) {	# If $line ends with the '~' character... it's an ;oh5*; variant
		$OHvariant = $line;
		$line =~ s|^(.+?) \~(.*?)$|$1|g;
		$OHvariant =~ s|^(.+?) ~~~(.*?)~~~$|$2|g;
		$OHvariantOut = "<!-- " . $OHvariant . " -->";
	} else {
		$OHvariant = "";
		$OHvariantOut = "";
	}

#print STDOUT "[debug-B 5_OHTag new \$line]: $line\n";
#print STDOUT "[debug-C 5_OHTag \$OHvariant]: $OHvariant\n";

	my @part = ();
 	my $tagLength=length($line);
	my $ohNo = substr($line, ($tagLength - 2), 1);
	my $element = GetPubMapLevel("oh$ohNo");
	my $origin =  " origin=\"$GenCodeFile\"";

	if($p4_ContainingElement ne "")	{												# NOT First Containing element...
		if($OHvariant ne "") {
			print(OUT "<$element meta5=\"$OHvariant\" $origin>\n");		# Containing element... 
		} else {
			print(OUT "<$element $origin>\n");		# Containing element... 
		}
	} else {
		$p4_ContainingElement="oh$ohNo";
		if($OHvariant ne "") {
			print(OUT "<$element $SchemaLevelBlurb meta5=\"$OHvariant\" $origin>\n");		# Containing element... 
		} else {
			print(OUT "<$element $SchemaLevelBlurb $origin>\n");		# Containing element... 
		}
	}
		
	$line=readline(FILE);
	chomp($line);
	@part = split(/\\/, $line);
	
	if(defined $part[0])	{
			# the following replacements are for creation of abbrev_txt and abbrev_nbr 
			# w/o ensp, emsp, endash, emdash characters
			$part[0] =~ s|\x{2002}| |g;
			$part[0] =~ s|\x{2003}| |g;
			print(OUT "<title>$part[0]<\/title>\n");
	} else {
			print(OUT "<title/>\n");	
	}
	
	if(defined $part[1]) {	
		# the following replacements are for creation of abbrev_txt and abbrev_nbr 
		# w/o ensp, emsp, endash, emdash characters
		$part[0] =~ s|\x{2002}| |g;
		$part[0] =~ s|\x{2003}| |g;
		print(OUT "<subtitle>" . $part[1] . "</subtitle>\n");	
	} else {
		print(OUT "<subtitle/>\n");	
	}
		
	if(defined $part[2])	{
		print(OUT "<para block_type=\'runin\'>" . $part[2] . "</para>\n");	
	}
}  			

# Convert Pseudo Container tags to Schema Container tags and
# Create Title and Subtitle elements
#----------------------------------------------------------------------
sub p5_CloseOHTag {
#----------------------------------------------------------------------
	my $line = shift;

	$line =~ s|^(.+?)\s+~~~(.*?)$|$1|g;
 	my $tagLength=length($line);
	my $ohNo = substr($line, ($tagLength - 2), 1);
	my $element = GetPubMapLevel("oh$ohNo");
	my @part = split(/ /, $element);

	print(OUT "<\/$part[0]>\n");		# Containing element...  
}

# Convert Pseudo Container tags to Schema Container tags and
# ;ol#;  tags become listitem
#----------------------------------------------------------------------
sub p5_OLTag {
#----------------------------------------------------------------------
	my $line = shift;
	my @part = ();
	my $idx = 0;
	
	my $gclevel=$line;
	$gclevel =~ s|<(.+?)(\d)>|gclevel=\"$2\"|g;	

	$line=readline(FILE);
	chomp($line);
	
	@part = split(/\\/, $line);		# split on the '\' character
	
#	# DUMP DATA:
#	if($#part > 0) {
#	 	print(OUT "\n<!-- =========== DUMPER: \$p5_OLTag =========== -->\n");
#	 	my $myDump = Dumper(@part);
#	 	print OUT "<!--\n";
#	 	print(OUT $myDump);	
#	 	print OUT "-->\n";
#	}
#	# END DUMP DATA
	
	
	if($#part < 2) {
		print(OUT "<listitem>\n<incr $gclevel>$part[0]</incr>\n");
	} else {
		print(OUT "<listitem monetary='yes'>\n<incr $gclevel>$part[0]</incr>\n");
		print(OUT "<content>$part[1]");
	}
		
	if($#part > 1) {
		print(OUT "<monetary>$part[2]</monetary>");		
	} else {
		print(OUT "<content>$part[1]");		
	}
	
	print(OUT "</content>\n");
}  			

#----------------------------------------------------------------------
sub p5_OLheadTag {
#----------------------------------------------------------------------
	my $line = shift;
	my @part = ();
	my $idx = 0;

	my $gclevel=$line;
	$gclevel =~ s|<(.+?)(\d)>|gclevel=\"$2\"|g;	

	$line=readline(FILE);
	chomp($line);
	
	@part = split(/\\/, $line);
	
#	# DUMP DATA:
	# if($#part > 0) {
	 	# print(OUT "\n<!-- =========== DUMPER: \$p5_OLheaderTag =========== -->\n");
	 	# my $myDump = Dumper(@part);
	 	# print OUT "<!--\n";
	 	# print(OUT $myDump);	
	 	# print OUT "-->\n";
	# }
#	# END DUMP DATA
	
	if($#part < 3) {
		# print(OUT "<listitem>\n<incr $gclevel>$part[0]</incr>\n");
		# print(OUT "<listheader>$part[1]");		
		# print(OUT "</listheader>\n");
		# print(OUT "<content>$part[2]");
		
		# 2017-04-04 - Raymond - Simplify 'listheader' conversion...
		print(OUT "<listitem>\n<incr $gclevel>$part[0]</incr>\n");
		print(OUT "<content><bold>$part[1]<\/bold> $part[2]");
	}
	
	print(OUT "</content>\n");
}  			

#----------------------------------------------------------------------
sub p5_BheadTag {
#----------------------------------------------------------------------
	my $line = shift;
	my @part = ();
	my $idx = 0;
	
	my $gclevel=$line;
	$gclevel =~ s|<(.+?)(\d)>|gclevel=\"$2\"|g;	

	$line=readline(FILE);
	chomp($line);
	
	@part = split(/\\/, $line);
		
	print(OUT "<para block_type=\'block\' $gclevel>\n");
	print(OUT "\t<blockheader>$part[0]</blockheader>\n");
	print OUT "$part[1]\n";

#	# DUMP DATA:
#	if($#part > 0) {
#	 	print(OUT "\n<!-- =========== DUMPER: \$p5_BheaderTag =========== -->\n");
#	 	my $myDump = Dumper(@part);
#	 	print OUT "<!--\n";
#	 	print(OUT $myDump);	
#	 	print OUT "-->\n";
#	}
#	# END DUMP DATA
	
}  	

# Convert Pseudo Container tags to Schema Container tags and
# ;ml#;  tags become listitem
#----------------------------------------------------------------------
sub p4_MLTag {
#----------------------------------------------------------------------
	my $line = shift;
	my $me = whoami();
	my @part = ();
	
	# BEGIN:  2010-05-11
	my $gclevel=$line;
	$gclevel =~ s|<(.+?)(\d)>|gclevel=\"$2\"|g;	
	# END:  2010-05-11
	
	$line=readline(FILE);
	chomp($line);
	@part = split(/\\/, $line);	

#	# DUMP DATA:
#	if($#part > 0) {
#	 	print(OUT "\n<!-- =========== DUMPER: \$me =========== -->\n");
#	 	my $myDump = Dumper(@part);
#	 	print OUT "<!--\n";
#	 	print(OUT $myDump);	
#	 	print OUT "-->\n";
#	}
#	# END DUMP DATA
	


# ORIGINAL VERSION PRIOR TO 20010506...	
#	if($#part < 3) {
#		print(OUT "<listitem>\n<incr>$part[0] $part[1]</incr>\n<content>$part[2]</content>");
#	} else {
#		print(OUT "<listitem>\n<incr>$part[0] $part[1]</incr>\n<content>$part[2]<monetary>$part[3]</monetary></content>");		
#	}

	if($#part < 3) {
		print(OUT "<listitem>\n<incr $gclevel>$part[0]</incr>\n" .
			"<incr_ml>$part[1]</incr_ml>\n<content>$part[2]</content>");
	} else {
		print(OUT 
		"<listitem>\n<incr $gclevel>$part[0]</incr>\n" .
			"<incr_ml>$part[1]</incr_ml>\n<content>$part[2]<monetary>$part[3]</monetary></content>");
	}

}

# Convert Pseudo Container tags to Schema Container tags 
#----------------------------------------------------------------------
sub p4_HNTag {
#----------------------------------------------------------------------
	my $line = shift;
	my $commentLine="";
	my $hnlevel = $line;
	
	# ^([ \t]*)<hn\d>
	$hnlevel =~ s|^([ \t]*)<hn(\d)>|$2|g;
	
	$line=readline(FILE);
	chomp($line);
	
	$commentLine = $line;
	$commentLine =~ s|--|&dash;&dash;|g;
	
	print(OUT "<comment gclevel=\"$hnlevel\" note=\"historynote\">$commentLine</comment>\n");
	print(OUT "<historynote>\n\t<hnitem>\n\t\t<hnsource><\/hnsource>\n\t\t<hnsourcesect><\/hnsourcesect>\n\t</hnitem>\n");
	print OUT "</historynote>\n";
}  			


# Convert Pseudo Container tags to Schema Container tags
#----------------------------------------------------------------------
sub p5_FootnoteTag {
#----------------------------------------------------------------------
	my $line = shift;	
	
	print(OUT "\n<footnote id=\"" . $infile . "_" . $footnoteSeq++ . "\" location=\"end_page\" mark=\"*\">\n");
	$footnotePending = "yes";
}  			


# Convert Pseudo Container tags to Schema Container tags:  reference notes 
#----------------------------------------------------------------------
sub p4_RNTag {
#----------------------------------------------------------------------
	my $line = shift;
	# NOTE:  PASS-TWO reverses the indent & type because nesting requires indent to be last.
	# INPUT:  <rn[type];[indent]>
	my $refIndent = $line;
	
	if( $line =~ /<rnm\d>/)	{
		$refIndent =~ s|<rnm(\d)>|$1|g;		
		$line =~ s|<rnm(\d)>|<rn$1;m>|g;
	} else {		
		$refIndent =~ s|<rn(\d);(\d)>|$2|g;
		$line =~ s|<rn(\d);(\d)>|<rn$2;$1>|g;
	}	

 	my $tagLength=length($line);
	my $refType = substr($line, ($tagLength - 2), 1);	# values:  .?[m0-9]	

	if( $refType eq "m")	{ p4_refManual($refIndent);	}
	elsif($refType eq "0")	{ p4_ref0($refIndent);	}		#refeditor
	elsif($refType eq "1")	{ p4_ref1($refIndent);	}		#refcharter
	elsif($refType eq "2")	{ p4_ref2($refIndent);	}		#refcross
	elsif($refType eq "3")	{ p4_ref3($refIndent);	}		#refstatelaw
	elsif($refType eq "4")	{ p4_ref4($refIndent);	}		#refstateconst **
	elsif($refType eq "5")	{ p4_ref5($refIndent);	}		#refcaselaw
	elsif($refType eq "6")	{ p4_ref6($refIndent);	}		#refcaselawanno **
	elsif($refType eq "7")	{ p4_ref7($refIndent);	}		#refnote
	elsif($refType eq "9")	{ p4_ref9($refIndent);	}		#;note; or <note>

	else	{
		print(OUT "\n<!-- +++ Bad RefNote +++-->\n");	
	}
	
}  			

# refmanual
#----------------------------------------------------------------------
sub p4_refManual {
#----------------------------------------------------------------------
	my $refIndent = shift;
	
	my $line;
# 	<refmanual>
#			<para/>
# 	</refmanual>
	$line=readline(FILE);
	chomp($line);
	
	if($footnotePending eq "yes")	{	
		$line = p4_RemoveInitialSupTag($line);
	}
	
	$line=Remove_hg($line);							 
	print(OUT "<refmanual>\n\t<para block_type=\"hang\" gclevel=\"$refIndent\">$line</para>\n</refmanual>\n");	
}

# refeditor
#----------------------------------------------------------------------
sub p4_ref0 {
#----------------------------------------------------------------------
	my $refIndent = shift;
	
	my $line;
# 	<refeditor>
#			<para/>
# 	</refeditor>
	$line=readline(FILE);
	chomp($line);

	if($footnotePending eq "yes")	{	
		$line = p4_RemoveInitialSupTag($line);
	}
	
	$line=Remove_hg($line);
	print(OUT "<refeditor>\n\t<para block_type=\"hang\" gclevel=\"$refIndent\">$line</para>\n</refeditor>\n");	
}

# refcharter
#----------------------------------------------------------------------
sub p4_ref1 {
#----------------------------------------------------------------------
	my $refIndent = shift;
	
	my $line;
	$line=readline(FILE);
	chomp($line);
	
	if($footnotePending eq "yes")	{	
		$line = p4_RemoveInitialSupTag($line);
	}
	
	my $commentLine = $line;
	$commentLine =~ s|--|&dash;&dash;|g;
	
	$line=Remove_hg($line);
	print(OUT "<refcharter>\n\t<para block_type=\"hang\" gclevel=\"$refIndent\">$line</para>\n</refcharter>\n");	
	
# Raymond - 2015-01-30	
# 	<refcharter>
# 		<chitem>
# 			<chdescription/>
# 			<chsection/>
# 		</chitem>
# 	</refcharter>
#	print(OUT "<comment note='refcharter'>$commentLine</comment>\n");
#	print(OUT "<refcharter>\n\t<chitem>\n\t\t<chdescription/>\n\t\t\t<chsection/>\n");
#	print(OUT "\t</chitem>\n</refcharter>\n");
}

#refcross
#----------------------------------------------------------------------
sub p4_ref2{
#----------------------------------------------------------------------
	my $refIndent = shift;
	
	my $line;
	$line=readline(FILE);
	chomp($line);
	
	if($footnotePending eq "yes")	{	
		$line = p4_RemoveInitialSupTag($line);
	}
	
	my $commentLine = $line;
	$commentLine =~ s|--|&dash;&dash;|g;
	$line=Remove_hg($line);
	print(OUT "<refcross>\n\t<para block_type=\"hang\" gclevel=\"$refIndent\">$line</para>\n</refcross>\n");

# Raymond - 2015-01-30		
# 	<refcross>
# 		<critem>
# 			<crdescription/>
# 			<crsection/>
# 		</critem>
# 	</refcross>	
#	print(OUT "<comment note='refcross'>$commentLine</comment>\n");
#	print(OUT "<refcross>\n\t<critem>\n\t\t<crdescription/>\n\t\t\t<crsection/>\n");
#	print(OUT "\t</critem>\n</refcross>\n");
}

# refstatelaw
#----------------------------------------------------------------------
sub p4_ref3 {
#----------------------------------------------------------------------
	my $refIndent = shift;
	
	my $line;

	$line=readline(FILE);
	chomp($line);
	#$line = p4_RemoveInitialSupTag($line);
	
	my $commentLine = $line;
	$commentLine =~ s|--|&dash;&dash;|g;
	$line=Remove_hg($line);
	print(OUT "<refstatelaw>\n\t<para block_type=\"hang\" gclevel=\"$refIndent\">$line</para>\n</refstatelaw>\n");
	
# Raymond - 2015-01-30		
# 	<refstatelaw>
# 		<slitem>
# 			<sldescription/>
# 			<slsource/>
# 			<slsection/>	<!-- multiple slsections allowed -->
# 			<slsection/>
# 			<slsection/>
# 		</slitem>
# 	</refstatelaw>
#	print(OUT "<comment note='refstatelaw'>$commentLine</comment>\n");
#	print(OUT "<refstatelaw>\n\t<slitem>\n\t\t<sldescription/>\n\t\t\t<slsource/>\n\t\t\t<slsection/>\n\t\t\t<slsection/>\n\t\t\t<slsection/>\n");
#	print(OUT "\t</slitem>\n</refstatelaw>\n");
}

# refstateconst
#----------------------------------------------------------------------
sub p4_ref4 {
#----------------------------------------------------------------------
	my $refIndent = shift;
	
	my $line;
# 	<refstateconst>
#			<para/>
# 	</refstateconst>
	$line=readline(FILE);
	chomp($line);
	
	if($footnotePending eq "yes")	{	
		$line = p4_RemoveInitialSupTag($line);
	}
	
	$line=Remove_hg($line);
	print(OUT "<refstateconst>\n\t<para block_type=\"hang\" gclevel=\"$refIndent\">$line</para>\n</refstateconst>\n");
}

# refcaselaw
#----------------------------------------------------------------------
sub p4_ref5{
#----------------------------------------------------------------------
	my $refIndent = shift;
	
	my $line;
# 	<refcaselaw>
# 		<para/>
# 	</refcaselaw>
	$line=readline(FILE);
	chomp($line);
	
	if($footnotePending eq "yes")	{	
		$line = p4_RemoveInitialSupTag($line);
	}
	
	$line=Remove_hg($line);
	print(OUT "<refcaselaw>\n\t<para block_type=\"hang\" gclevel=\"$refIndent\">$line</para>\n</refcaselaw>\n");
}

# refcaselawanno
#----------------------------------------------------------------------
sub p4_ref6 {
#----------------------------------------------------------------------
	my $refIndent = shift;
	
	my $line;
# 	<refcaselawanno>
#			<para/>
# 	</refcaselawanno>
	$line=readline(FILE);
	chomp($line);

	if($footnotePending eq "yes")	{	
		$line = p4_RemoveInitialSupTag($line);
	}
	
	$line=Remove_hg($line);
	print(OUT "<refcaselawanno>\n\t<para block_type=\"hang\" gclevel=\"$refIndent\">$line</para>\n</refcaselawanno>\n");
}


# refnote
#----------------------------------------------------------------------
sub p4_ref7 {
#----------------------------------------------------------------------
	my $refIndent = shift;
	
	my $line;
# 	<refnote>
#			<para/>
# 	</refnote>
	$line=readline(FILE);
	chomp($line);

	if($footnotePending eq "yes")	{	
		$line = p4_RemoveInitialSupTag($line);
	}
	
	$line=Remove_hg($line);
	print(OUT "<refnote>\n\t<para block_type=\"hang\" gclevel=\"$refIndent\">$line</para>\n</refnote>\n");
}


# ;note; or <note>
#----------------------------------------------------------------------
sub p4_ref9 {
#----------------------------------------------------------------------
	my $refIndent = shift;
	
	my $line;
# 	<refgeneric>
#			<para/>
# 	</refgeneric>

	$line=readline(FILE);
	chomp($line);

	if($footnotePending eq "yes")	{	
		$line = p4_RemoveInitialSupTag($line);
	}
	
	$line=Remove_hg($line);
	print(OUT "<refgeneric>\n\t<para block_type=\"block\" gclevel=\"$refIndent\">$line</para>\n</refgeneric>\n");
}


#----------------------------------------------------------------------
sub p4_RemoveInitialSupTag{
#----------------------------------------------------------------------
	my $line = shift;
	my $sTemp = $line;
	
	
	if(	($sTemp =~ /^<sup>/)  |  ($sTemp =~ /^<.+?><sup>/) ) {
		$sTemp =~ s|^<sup>(.+?)<\/sup>(.+?)|$2|g;
		$sTemp =~ s|^<sup>(<.+?>)(.+?)(<.+?>)<\/sup>(.+?)|$4|g;
		$sTemp =~ s|^(<.+?>)<sup>(.+?)<\/sup>(.+?)|$1$3|g;
		$sTemp =~ s|^(<.+?>)<sup>(<.+?>)(.+?)(<.+?>)<\/sup>(.+?)|$1$5|g;
	}
	
	return $sTemp;
}


#----------------------------------------------------------------------
sub Remove_hg {
#----------------------------------------------------------------------
	my $line = shift;
	
	$line =~ s|;hg;||g;
	
	return $line;
}


#----------------------------------------------------------------------
sub monetary_set_flags {
#----------------------------------------------------------------------
	my $line = shift;
	
	if($line =~ /monetary_BEGIN/)	{ $monetaryFlag = TRUE; 	}	
	if($line =~ /monetary_END/)		{ $monetaryFlag = FALSE;	}
	
	return TRUE;
}


#----------------------------------------------------------------------
sub monetary_wrap {
#----------------------------------------------------------------------
	my $line = shift;
	my $rtn = "";
			
	@part = split(/\\/, $line);

	if($#part > 0) {	
		# print(OUT "\n--------------------- DUMPER: PASS-SIX ----------------------");
		# my $myDump = Dumper(@part);
		# print OUT "\n<!--\n";
		# print(OUT $myDump);	
		# print OUT "\n-->\n";
		
		$rtn = "$part[0]<monetary>$part[1]</monetary>";
	} else {
		$rtn = $line;
	}
	
	return $rtn;
}
		

#----------------------------------------------------------------------
sub GetPubMapXML {
#----------------------------------------------------------------------
	
	if(!-e $PubMap)	{		
		CreatePubMapXML();
	}
	
	my $xsPubMap = XML::Simple->new(
		ForceArray => 0,
		KeepRoot => 1,
		KeyAttr => [ ],
		NoAttr=>1
	);
		
	# Load $XMLdriver from PubMap.xml...
	$XMLPubMapdriver = $xsPubMap->XMLin($PubMap);		
}

	
#----------------------------------------------------------------------
sub GetPubMapLevel	{ # Lookup $infile in PubMap.xml...
#----------------------------------------------------------------------
	my $level=shift;
	my $rtnLevel="";
	
	if( not $XMLPubMapdriver->{'PubMap'}->{lc($infile)} ) {
			#default...
			$rtnLevel=$XMLPubMapdriver->{'PubMap'}->{'default'}->{$level};
	} else {
			#custom...
			$rtnLevel=$XMLPubMapdriver->{'PubMap'}->{lc($infile)}->{$level};
	}
		
	return($rtnLevel);
}


#----------------------------------------------------------------------
sub CreatePubMapXML {
#----------------------------------------------------------------------

	if(!-d "./CONFIG")	{
		mkdir("CONFIG");
	}

	if(-e "_PubMap.xml") {
		move("_PubMap.xml", "./CONFIG/_PubMap.xml");		
		return;	
	}

	$PubMapDefault = "" . 
"<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<PubMap>
  <default>
    <oh0>level1</oh0>
    <oh1>level2</oh1>
    <oh2>level3</oh2>
    <oh3>level4</oh3>
    <oh4>level5</oh4>
    <oh5>section</oh5>
    <oh6>subsec1</oh6>
    <oh7>subsec2</oh7>
    <oh8>subsec3</oh8>
  </default>
</PubMap>
";

	open(PMAP, ">./CONFIG/_PubMap.xml") or die "cannot open file for writing: $!";
	print PMAP "$PubMapDefault";
	close PMAP;
}


#----------------------------------------------------------------------
sub DecrementAllSpanRowValues {
#----------------------------------------------------------------------
	#If resulting value equals zero, then delete that hash.
	foreach my $key (keys %spanRow)	{
		$spanRow{$key} = $spanRow{$key} - 1;
		
		if($spanRow{$key} < 1 )	{
			delete $spanRow{$key};
			delete $spanCol{$key};
		}
	}
}


#----------------------------------------------------------------------
sub Build_Inline_Hang_Line {
#----------------------------------------------------------------------
	my $block_tag = shift;			# ;b#; or ;hl;
	my $block_content = shift;		# should contain the in-line tag: ;hg;
	
	my $block_Number = $block_tag;
	my $c = 0;						# counter
	my $float = 0.0;
	my $left_string = "";
	my $right_string = "";
	my $rtn = "";
	
	if($block_Number =~ /;b(\d);/) {
		$block_Number =~ s|;b(\d);|$1|g;
	} elsif($block_Number =~ /;hl;/) {
		$block_Number = 0;
	} else {
		$block_Number = 0;
	}
	
	# first, get a count of characters before the ;hg; tag...
	# if this is greater than 100, then simply convert the ;hg; to a space
	
	$left_string = $block_content;
	$right_string = $block_content;
	$left_string =~ s|^(.+?);hg;(.*?)$|$1|g;
	$right_string =~ s|^(.+?);hg;(.*?)$|$2|g;
	
	$c = InlineHGCharacterCount($left_string);
	
	if($left_string !~ /<q([alcr])>/) {
		$float = $c;
		$c = int(round($float / 2));
	}
	
	$block_tag = ";hg" . $c . "-" . $block_Number . ";";
			
	$left_string =~ s|^(.*?)<q([alcr])>(.*?)$|$1<q$2>|g;
	# $left_string =~ s|<q([alcr])> |<q$1>|g;
	# $left_string =~ s|<q([alcr])>(.+?)|<q$1>|g;
	$left_string =~ s|(\x{2003})|\x{2002}|g;		# hex em-space to en-space
	$left_string =~ s|<em>|<en>|g;					# using 'ex' measurements in CSS, NOT 'rem' or 'em'
	$left_string =~ s|;em;|;en;|g;					# using 'ex' measurements in CSS, NOT 'rem' or 'em'

	$block_content = $left_string . $right_string;
	
	$rtn = $block_tag . "\n" . $block_content;	
	
	# print STDOUT "[debug rtn]: $rtn\n";
	# $x = ReadKey();
	
	return $rtn;
}


#----------------------------------------------------------------------
sub InlineHGCharacterCount {
#----------------------------------------------------------------------
	my $str = shift;
	my $kount = 0;
	
	# print STDOUT "[debug-A Inline str]: $str\n";
	$str =~ s|;copy;| |g;					# convert ;copy; to a space
	$str =~ s|<en>| |g;						# convert ;en; to a space
	$str =~ s|<em>| |g;						# convert ;em; to a space
	$str =~ s|;en;| |g;						# convert ;en; to a space
	$str =~ s|;em;| |g;						# convert ;em; to a space
	$str = fv($str);						# manage font variant stuff
	$str =~ s|;(.[lcr]);|<q$1>|g;			# convert ;l; ;c; and ;r; to quad tags <ql>...	
	$str =~ s|<(.[lcr])>|<q$1>|g;			# convert <l> <c> and <r> to quad tags <ql>...	
	$str =~ s|<q([alcr])>|\~q$1\+|g;		# convert <l> <c> and <r> to quad tags <ql>...	
	# print STDOUT "[debug-B Inline str]: |$str|\n";
	
	$str =~ s|;adv(.+?);||g;				# strip out ;adv...;
	$str =~ s|<(.+?)>||g;					# strip out elements
	# print STDOUT "[debug-C Inline str]: |$str|\n";
	
	if($str =~ /\~(.+?)\+/) {
		$str =~ s|^(.+?)\~q([alcr])\+(.*?)$|$3|g;
		$kount = length($str);
	} else {
		$kount = length($str);
	}
	
	# print STDOUT "[debug kount]: $kount\n\n";
	return $kount;
}


#----------------------------------------------------------------------
sub CAPT_2_Para {
#----------------------------------------------------------------------
	my $parm = shift;
	my $me = whoami();
	my $printCaption="";
		
	if($parm =~ /\{\/CAPT;/)	{		
		$CaptQuad = $parm;
#		$CaptQuad =~ s|<!-- (\{\/CAPT;)(.+?)(.?[}])(.?[\\])(.?[m])(.*)(\<q)(.?[lcra])(\>)([\\]?[\.]?) -->|$8|g;	# old school
		$CaptQuad =~ s|<!-- (\{\/CAPT;)(.+?)(.?[}])(.*)(\<q)(.?[lcra])(\>)([\\]?[\.]?) -->|$6|g;
		$CaptQuad =~ s|^l$|justify=\"left\"|g;		
		$CaptQuad =~ s|^c$|justify=\"center\"|g;		
		$CaptQuad =~ s|^r$|justify=\"right\"|g;		
		$CaptQuad =~ s|^a$|justify=\"center\"|g;		
		
		$Capt = $parm;
#		$Capt =~ s|<!-- (\{\/CAPT;)(.+?)(.?[}])(.?[\\])(.?[m])(.*)(\<q)(.?[lcra])(\>)([\\]?[\.]?) -->|$6$7$8$9|g;
		$Capt =~ s|<!-- (\{\/CAPT;)(.+?)(.?[}])(.*)(\<q)(.?[lcra])(\>)([\\]?[\.]?) -->|$4$5$6$7|g;
		$Capt =~ s|<\/ul>(.+?)<\/ul>|$1|g;
		$Capt = p1_SingleTags($Capt);
		$Capt ="<para $CaptQuad block_type=\"block\">\n<?xpp CAPT?>\n$Capt\n"; 
	}
	
	$printCaption = $Capt;
	
	return $printCaption;
}



#----------------------------------------------------------------------
sub TITL_2_Alt {
#----------------------------------------------------------------------
	my $parm = shift;
	my $me = whoami();
	
	my $alt = $parm;	# don't modify the original input variable
	my $rtn="";
	
	# SAMPLE INPUT:
	#{/TITL;17p;comptd;left;stack;1p;0;cw;0;0;0;0}<sz;10q><ff;2><bold></ul>Scenario 1: single-car garage: 21' fa&ccedil;ade <times> 0.5 = 10'6" 10'6" &lt; 13', maximum garage width = 13' Scenario 2: 2-car garage: 40' fa&ccedil;ade <times> 0.5 = 20' 20' &gt; 13', maximum garage width = 20'</ul>
	
	#Strip out ulinks
	$alt =~ s|<ulink.*?>(.*?)<\/ulink>|$1|g;
	
	# stubborn characters...
	$alt =~ s|<times>|x|g;
	$alt =~ s|\\m||g;

	# strip off front content...
	$alt =~ s|^(.+?)\}(.*?)$|$2|g;
	$alt =~ s|^(.*?)--\>|$1|g;

	# strip off bold, med, & underline, sup and sub
	$alt =~ s|<bold>||g;
	$alt =~ s|<\/bold>||g;
	$alt =~ s|<ital>||g;
	$alt =~ s|<\/ital>||g;
	$alt =~ s|<bdit>||g;
	$alt =~ s|<\/bdit>||g;
	$alt =~ s|<b>||g;
	$alt =~ s|<\/b>||g;
	$alt =~ s|<i>||g;
	$alt =~ s|<\/i>||g;	
	$alt =~ s|<med>||g;
	$alt =~ s|<\/med>||g;
	$alt =~ s|<ul>||g;
	$alt =~ s|<\/ul>||g;
	$alt =~ s|<ul (.+?)>||g;
	$alt =~ s|<sup>||g;
	$alt =~ s|<\/sup>||g;
	$alt =~ s|<sub>||g;
	$alt =~ s|<\/sub>||g;
	
	# convert all inline quads...
	$alt =~ s|<q.>| |g;
	
	$alt = p1_SingleTags($alt);
	$alt = p1_GenCodeEntities($alt);	

	$alt =~ s|"|&quot;|g;	
	
	# strip off the PIs...
	$alt =~ s|<\?(.+?)\?>||g;

	$alt =~ s|^\s+(.*?)$|$1|g;	
	$alt =~ s|^(.*?)\s+$|$1|g;
		
	$rtn = $alt;
	
	return $rtn;
}


#----------------------------------------------------------------------
sub Dump_colspecHash {
#----------------------------------------------------------------------
	my $header = shift;
	my $key;
	my $value;
	
	print(OUT "\n<!-- ================================== -->\n");
	print(OUT "<!-- \%colspecHash ($header)-->\n");
	for $key ( sort keys %colspecHash ) {
		#$value = $colspecHash{$key};
		#print(OUT "<!-- \t$key => $value -->\n");
		if($key gt '0') {
			printf(OUT "%4d %s\n", $key, $colspecHash{$key});
		} else {
			printf(OUT "(NAN) %s %s\n", $key, $colspecHash{$key});			
		}
	}
	print(OUT "<!-- \%colspecHash FIN -->\n");
	print(OUT "<!-- ================================== -->\n");
}


#----------------------------------------------------------------------
sub Dump_SOtagLines {
#----------------------------------------------------------------------
	my $header = shift;
	my $index;
	print(OUT "\n<!-- ================================== -->\n");
	print(OUT "<!-- \@SOtagLines ($header)-->\n");
	for($index=0;		$index <= $#SOtagLines;		$index++ ) {
		print(OUT "<!-- [$index] = |$SOtagLines[$index]| -->\n");	
	}
	print(OUT "<!-- \@SOtagLines (FINISHED) -->\n");
	print(OUT "<!-- ================================== -->\n");
}


#----------------------------------------------------------------------
sub Dump_SOelementStack {
#----------------------------------------------------------------------
	my $header = shift;
	my $index;
	print(OUT "\n<!-- ================================== -->\n");
	print(OUT "<!-- \@SOelementStack ($header)-->\n");
	for($index=0;		$index <= $#SOelementStack;		$index++ ) {
		print(OUT "<!-- [$index] = |$SOelementStack[$index]| -->\n");	
	}
	print(OUT "<!-- \@SOelementStack (FINISHED) -->\n");
	print(OUT "<!-- ================================== -->\n");
}


#----------------------------------------------------------------------
sub Dump_aTemp {
#----------------------------------------------------------------------
	my $header = shift;
	my $index;
	print(OUT "\n<!-- ================================== -->\n");
	print(OUT "<!-- \@aTemp ($header)-->\n");
	for($index=0;		$index <= $#aTemp;		$index++ ) {
		print(OUT "<!-- \@aTemp[$index] = |$aTemp[$index]| -->\n");	
	}
	print(OUT "<!-- \@aTemp FIN-->\n");
	print(OUT "<!-- ================================== -->\n");
}

#----------------------------------------------------------------------
sub Dump_interpPart {
#----------------------------------------------------------------------
	my $header = shift;

	my $index;
	print(OUT "\n<!-- ================================== -->\n");
	print(OUT "<!-- \@interpPart ($header)-->\n");
	for($index=0;		$index <= $#interpPart;		$index++ ) {
		print(OUT "<!-- \@interpPart[$index] = $interpPart[$index] -->\n");	
	}
	print(OUT "<!-- \@interpPart FIN-->\n");
	print(OUT "<!-- ================================== -->\n");
}

#----------------------------------------------------------------------
sub Dump_CTNoStack {
#----------------------------------------------------------------------
	my $header = shift;
	my $index;
	print(OUT "\n<!-- ================================== -->\n");
	print(OUT "<!-- \@CTNoStack ($header)-->\n");
	for($index=0;		$index <= $#CTNoStack;		$index++ ) {
		print(OUT "<!-- \@CTNoStack[$index] = |$CTNoStack[$index]| -->\n");	
	}
	print(OUT "<!-- \@CTNoStack FIN-->\n");
	print(OUT "<!-- ================================== -->\n");
}
		
#----------------------------------------------------------------------
sub  dumpPop	{
#----------------------------------------------------------------------
	my $aRef = \@_;
	my @Stack = @_;
	my $item = "";
	
	print(OUT "========================== DUMP POP: Stack BEGIN ============================\n");
	print(OUT "\$aRef=$aRef\n");
	
	while($#Stack > -1) {
		$item = pop @Stack;	
		print(OUT "\t\t\tStack Item \=$item\n");
	}
	
	print(OUT "========================== DUMP POP: Stack END =============================\n");
	
	return(0);	
}

#----------------------------------------------------------------------
sub ConvertTable	{
#----------------------------------------------------------------------
	my $refTable = \@_;
	my @myT = @$refTable;

	my $x = "";
	
	my $sTable = "";
	my $OriginalTableAsString = "";
	my @cals = ();
	my $index;
	my $str = "";
	my $str2 = "";
	my $myStr1 = "";
	my $myStr2 = "";
	
	my $namest = 0;
	my $namest_str = "";
	my $nameend = 0;
	my $nameend_str = "";
	
	my $entrycount = 0;
	my $myInt1 = 0;
	my $myInt2 = 0;
	my $myDump = "";
	my $saveIndex = 0;
	
	my $setwidFlag = "no";
	my @aTemp = ();
	my @Astr = ();
	my $firstRow = "yes";
	my $openRow = "-";
	my $openEntry = "-";
	
	my @RC = ();
	my $rowCount = 0;
	my $colCount = 0;

 	# normalize <Tr> to not have imbedded newline characters...
 	$str = "";
 			
	for($index=0;		$index <= $#myT;		$index++ ) {
		if($myT[$index]	!~	/^</)	{						# Doesn't begin with "<"
			$saveIndex = $index - 1;							# last good index beginning with "<..."
			
			while(	($index <= $#myT) & ($myT[$index]	!~	/^</)	)	{
				$str .=	$myT[$index];							# Add on to temp string $str
				splice @myT, $index, 1;						# Remove array element
			}

			$myT[$saveIndex] .=	$str;						# Add on to last item beginning with "<
			$str="";
		}		
	}
		
	#Build $OriginalTableAsString... 
	$OriginalTableAsString = "";
	for($index=0;		$index <= $#myT;		$index++ ) {
		$OriginalTableAsString .= "$myT[$index]\n";
	}
	
#print OUT "\n\n<!-- \$OriginalTableAsString: -->\n<!-- \n$OriginalTableAsString-->\n\n";
 	
	#==============================
 	# CALS-ONE Convert the table to CALS...
	#==============================
 	
	# Insert <title/>
	$str = $myT[0] . "\|\|" . qq(<title></title>) ;
	@Astr = split /\|\|/, $str;		
	splice @myT, 0, 1, @Astr;
	
	# Set </row>'s stuff...
 	$firstRow = "yes";
 	$index = 0;
 	
	for($index=0; $index<=$#myT; $index++ ) {
		if($myT[$index] =~ /<Tr/)	{
			if($firstRow ne "yes")	{
				$str = "</row>" . "\|\|" . $myT[$index];
				@Astr = split /\|\|/, $str;						
				splice @myT, $index++, 1, @Astr;
			} else {
				$firstRow = "no";
			}
		}
	}

	# NOTE:
	# Span columns <T[cr];rows;columns>  convert to  <spanspec...>  
	#    - OR  -
	# Use the <entry> element with attributes to span rows or columns in a CALS table:
	# *	Use the "morerows" attribute to specify the number of ADDITIONAL rows
	#	or vertical spanning. For example, <entry morerows="1"> is
	# 	equivalent to <Tc;2> in XPP. 
	# *	Use the "namest" and "nameend" attributes to specify the start and
	# 	end columns for horizontal spanning. 
	
# print(OUT "\n\n<!-- ==========  [ConvertTable - BEGINNING CALS-ONE]: \@myT ========== -->\n\n<!-- \n");
# $myDump = Dumper(@myT);
# print(OUT $myDump);
# print(OUT "\n-->\n<!-- ========== [ConvertTable - END DUMP:  BEGINNING CALS-ONE]: \@myT ========== -->\n\n");

	if($OriginalTableAsString =~ /<setnc;(\d*+)>/)		{
		
		# find index of setnc...
		$index = 0;
		while( ($myT[$index] !~ /<setnc;/g) & ($index <= $#myT) )	{
			$index++;
		}
		
		$str2 = $myT[$index];										        # get value of <setnc;...
		$str2 =~	s|(.*)<setnc;(\d*+)>(.*)|$2|g;						    # value $str = number of columns					
		$str = $myT[1] . "\|\|" . qq(<tgroup cols=\") . $str2 . qq(\">);	#<title/> . <tgroup...>		
		$myColumnCount = $str2;
		@Astr = split /\|\|/, $str;		
		splice @myT, 1, 2, @Astr;
		
	} else {														    # not a "<setnc;...>"  so...
	
		$str = $myT[1] . "\|\|" . qq(<tgroup cols=\"2\">);				# set number of columns to 2...
		$myColumnCount = 2;
		@Astr = split /\|\|/, $str;		
		splice @myT, 1, 1, @Astr;
	}
	

 	$activeCol = 1;
 	$namest = 0;
 	$nameend = 0;
 	%spanRow = ();
 	%spanCol = ();

	
	for($index=0;		$index <= $#myT;		$index++ ) {
		
		if($myT[$index] =~ /^<\/row>/) {
		 	$activeCol = 1;	 	
		 	$namest = 0;
		 	$nameend = 0;
		 	
		 	DecrementAllSpanRowValues();
		 	
		 	if(exists $spanCol{$activeCol}) {
		 		if(defined $spanCol{$activeCol}) {	 	
			 		$activeCol += GetSpanColValue($activeCol);
			 	}
		 	}
		}

		if($myT[$index] =~ /^<Tr/) {
		
			# <Tr;#;#>...span rows and columns...
			if($myT[$index] =~ /^<Tr;(\d+);(\d+)>/) {
				$spanRows_str = $myT[$index];
				$spanCols_str = $myT[$index];
				
				$spanRows_str =~ s|^<Tr;(\d+);(\d+)>(.*?)$|$1|g;
				$spanRow{$activeCol} = int($spanRows_str);

				$spanCols_str =~ s|^<Tr;(\d+);(\d+)>(.*?)$|$2|g;
				$spanCol{$activeCol} = int($spanCols_str);
				
				$namest = $activeCol;
				$nameend = ($namest + GetSpanColValue($activeCol)) - 1;

				$myT[$index]  =~	s|^<Tr;(\d+);(\d+)>|<row>\n<entry morerows=\'$1\' namest=\'c$namest\' nameend=\'c$nameend\'>|mg;
				
				if(exists $spanCol{$activeCol})	{
					$activeCol += GetSpanColValue($activeCol);	
				}
			}

			# <Tr;;#>...span columns...
			if($myT[$index] =~ /^<Tr;;(\d+)>/) {
				$spanRow{$activeCol} = 1;
				$spanCols_str = $myT[$index];

				$spanCols_str =~ s|^<Tr;;(\d+)>(.*?)$|$1|g;
				$spanCol{$activeCol} = int($spanCols_str);
				
				$namest = $activeCol;
				$nameend = ($namest + GetSpanColValue($activeCol)) - 1;

				$myT[$index]  =~	s|^<Tr;;(\d+)>|<row>\n<entry namest=\'c$namest\' nameend=\'c$nameend\'>|mg;
				
				if(exists $spanCol{$activeCol})	{
					$activeCol += GetSpanColValue($activeCol);	
				}
			}
				
			# <Tr;#>...span rows...
			if($myT[$index] =~ /^<Tr;(\d+)>/) {
				$spanRows_str = $myT[$index];
				
				$spanRows_str =~ s|^<Tr;(\d+)>(.*?)$|$1|g;
				$spanRow{$activeCol} = int($spanRows_str);

				$spanCol{$activeCol} = 1;
				
				$namest = $activeCol;
				$nameend = ($namest + GetSpanColValue($activeCol)) - 1;

				$myT[$index]  =~	s|^<Tr;(\d+)>|<row>\n<entry morerows=\'$1\' namest=\'c$namest\' nameend=\'c$nameend\'>|mg;
				
				if(exists $spanCol{$activeCol})	{
					$activeCol += GetSpanColValue($activeCol);	
				}
			}
			
			# <Tr>
			if($myT[$index] =~ /^<Tr>/) {
				$myT[$index] =~	s|^<Tr>|<row>\n<entry>|mg;
				$activeCol++;
				
				if(exists $spanCol{$activeCol})	{
					$activeCol += GetSpanColValue($activeCol);	
				}
			}
		}
	
		
		if($myT[$index] =~ /^<Tc/) {
			# Tc span rows and columns...(NOTE:  if columns=0, then span all remaining columns)
			if($myT[$index] =~ /<Tc;(\d+);(\d+)>/) {
				$spanRows_str = $myT[$index];
				$spanCols_str = $myT[$index];
				
				$spanRows_str =~ s|^<Tc;(\d+);(\d+)>(.*?)$|$1|g;
				$spanRow{$activeCol} = int($spanRows_str);

				$spanCols_str =~ s|^<Tc;(\d+);(\d+)>(.*?)$|$2|g;
				$spanCol{$activeCol} = int($spanCols_str);
				
				$namest = $activeCol;
				
				if($spanCols_str > 0) {
					$nameend = ($namest + GetSpanColValue($activeCol)) - 1;
				} else {
					$nameend = $activeCol - $namest;
				}
								
				$myT[$index] =~ s|^<Tc;(\d+);(\d+)>|<entry morerows=\'$1\' namest=\'c$namest\' nameend=\'c$nameend\'>|mg;
				
				if(exists $spanCol{$activeCol})	{
					$activeCol += GetSpanColValue($activeCol);	
				}
			}
			
			# columns only...
			if($myT[$index] =~ /^<Tc;;(\d+)>/) {
				$spanRow{$activeCol} = 1;
				$spanCols_str = $myT[$index];

				$spanCols_str =~ s|^<Tc;;(\d+)>(.*?)$|$1|g;
				$spanCol{$activeCol} = int($spanCols_str);
						
				$namest = $activeCol;
				
				if($spanCols_str > 0) {
					$nameend = ($namest + GetSpanColValue($activeCol)) - 1;
				} else {
					$nameend = $activeCol - $namest;
				}
			
				$myT[$index] =~ s|^<Tc;;(\d+)>|<entry namest=\'c$namest\' nameend=\'c$nameend\'>|mg;
						
				if(exists $spanCol{$activeCol})	{
					$activeCol += GetSpanColValue($activeCol);	
				}				
			}
					
			# rows only...
			if($myT[$index] =~ /^<Tc;(\d+)>/) {
				$spanRows_str = $myT[$index];
				
				$spanRows_str =~ s|^<Tc;(\d+)>(.*?)$|$1|g;
				$spanRow{$activeCol} = int($spanRows_str);

				$spanCol{$activeCol} = 1;
				
				$namest = $activeCol;
				$nameend = ($namest + GetSpanColValue($activeCol)) - 1;

				$myT[$index] =~ s|^<Tc;(\d+)>|<entry morerows=\"$1\">|mg;
				
				if(exists $spanCol{$activeCol})	{
					$activeCol += GetSpanColValue($activeCol);	
				}				
			}
			
			# <Tc>
			if($myT[$index] =~ /^<Tc>/) {
				$myT[$index] =~ s|^<Tc>|<entry>|mg;
				$activeCol++;
				
				if(exists $spanCol{$activeCol})	{
					$activeCol += GetSpanColValue($activeCol);	
				}				
			}
		}
		
		# Of course XPP rows and CALS don't jibe...(so fix it by subtracting 1 from each morerows)...		
		if($myT[$index] =~ /morerows=/)	{	
			$myInt1 = $myT[$index];
			$myInt1 =~ s|^(.+?)(morerows=)(.)(\d+)(.*?)$|$4|smg;		# assign value of morerows to $myStr1...
			$myInt1--;
			$myT[$index] =~ s|^(.+?)(morerows=)(.)(\d+)(.*?)$|$1$2$3$myInt1$5|smg;
		}
				
	}
	
	#==============================
 	# CALS-TWO Convert the table to CALS...
	#==============================
	
 	
	#--------------------------------
	# setnc ->  <tgroup cols=#...>
	#--------------------------------
	
# print(STDERR "\n<!-- --------------------- [ConvertTable - BEGINNING CALS-TWO]: \@myT ---------------------- -->\n\n");
# $myDump = Dumper(@myT);
# print(STDERR $myDump);
# print(STDERR "\n<!-- --------------------- [ConvertTable - END DUMP:  BEGINNING CALS-TWO]: \@myT ---------------------- -->\n\n");
	
	# Now that you have the $myColumnCount variable...fix the nameend='c0' entries to match...
	for($index=0;		$index <= $#myT;		$index++ ) {
		if($myT[$index] =~ /nameend=\'c0/mg )		{
			$myInt1 =	$myT[$index];
			$myT[$index] =~ s|nameend=\'c0\'|nameend=\'c$myColumnCount\'|mg;	
		}
	}

	#--------------------------------
	# setwid -->  <colspec...
	#--------------------------------
	#  "<setwid;#;#> Xy  <setwid; column#(0-+), column-width(.*)
	# <colspec colwidth=11.6pc colnum=1 colname=c1>
	# <colspec colwidth=* colnum=2 colname=c2>
	# <colspec colwidth=* colnum=3 colname=c3>
	# XPP Tags:
	#  <setwid> or <setwid;0;0>
	#		Sets the width of every column equal to the width of the widest text in the column.
	#	<setwid;2;2n>
	#		Sets the width of column 2 to two times the proportional column width.
	
	$str = $OriginalTableAsString;
	
	if($str =~ /<setwid;/)	{
		$setwidFlag = "yes";
		$str =~ s|(<setwid;)|~~~$1|g;		# prefix all <setwid...> with "~~~" (removed during 'split' command...
		@aTemp = split(/~~~/, $str);
		$str = $aTemp[$#aTemp];
		push(@aTemp, $str);		# duplicate last array item
		$index = index($aTemp[($#aTemp - 1)], ">");
		$aTemp[($#aTemp - 1)] = substr($aTemp[($#aTemp - 1)], 0, ($index + 1));
		$aTemp[$#aTemp] = substr($aTemp[$#aTemp], ($index + 1));
		shift @aTemp;	# remove first item
		pop @aTemp;		# remove last item
	} else {
		$setwidFlag = "no";
	}
	
	if($#aTemp < 0) {		# IF there are no <setwid...> entries, create a default one...
		push(@aTemp, "<setwid;1;1*>");	
		push(@aTemp, "<setwid;2;1*>");	
	}
				
	#******************************************************************	
		# NOTE:  At this point, if @aTemp has any items ($#aTemp > -1), 
		# then all elements will be <setwid;...> elements
	#******************************************************************	
	
# print(OUT "\n<!-- --------------------- [ConvertTable - BEGINNING CALS-TWO]: \@aTemp ---------------------- -->\n\n");
# $myDump = Dumper(@aTemp);
# print(OUT $myDump);
# print(OUT "\n<!-- --------------------- [ConvertTable - END DUMP:  BEGINNING CALS-TWO]: \@aTemp ---------------------- -->\n\n");

	%colspecHash = ();
	
	# Initialize colspecHash...
	for($index = 1;	$index <= $myColumnCount;		$index++)	{
		$colspecHash{$index} = "\<colspec colnum=\"$index\" colname=\"c$index\" colwidth=\"1\*\"\/>";
	}		
	
	# Process all 'spanning' setwid tags first...  (<setwid;-#;...>)
 	for($index=0;		$index <= $#aTemp;		$index++ ) {		# CALS q-meas:  pc, pt, in
 		
 		if($aTemp[$index] =~ /<setwid;-(\d+);(.+?)(.?[pqin\*])>/) {
#print STDOUT "[debug-A]: $aTemp[$index]\n"; 			
 		  	$myStr1 = $aTemp[$index];
			$myStr1 =~ s|<setwid;-(\d+);(.+?)(.?[pqin\*])>|<colspec colnum="$1" colname="c$1" colwidth="$2$3" \/>|g;
#print STDOUT "[debug-B \$myStr1]: $myStr1\n";			
			$myStr2 = $aTemp[$index];
			# Get column...
			$myStr2 =~  s|<setwid;-(\d+);(.+?)(.?[pqin\*])>|$1|g;
			
			if(looks_like_number($myStr2)) {
				$myInt1 = int($myStr2);		
			} else {
				$myInt1 = 0;
				print(OUT "<!-- ERROR! Not numeric [\$myStr2]: $myStr2	 -->\n");
			}
#print STDOUT "[debug \$myInt1]: $myInt1\n";									
			# Change remaining columns to match...
 			for(my $x=$myInt1;  $x<=$myColumnCount;  $x++) {
				$myStr1 =~ s|<colspec colnum="(\d+)" colname="c(\d+)"|<colspec colnum="$x" colname="c$x"|g; 				
#print STDOUT "[debug \$myStr1]: $myStr1\n";
				$colspecHash{$x} = $myStr1;
			}
 		} 		
 	}
 	
#print(OUT "\n<!-- [A] colspecHash Updates [Dump_colspecHash]-->\n");	
#	Dump_colspecHash();
	
	# Process all 'normal' (non-spanning) setwid tags...
 	for($index=0;		$index <= $#aTemp;		$index++ ) {		# CALS q-meas:  pc, pt, in
 	
 		if($aTemp[$index] =~ /<setwid;(\d+);(.+?)(.?[pqin\*])>/) {
	 		$myStr1 = $aTemp[$index];
			$myStr1 =~ s|<setwid;(\d+);(.+?)(.?[pqin\*])>|<colspec colnum=\"$1\" colname=\"c$1\" colwidth=\"$2$3\" \/>|g;
			$myStr2 = $aTemp[$index];
			
			# Get column...
			$myStr2 =~  s|<setwid;(\d+);(.+?)(.?[pqin\*])>|$1|g;
			
			if(looks_like_number($myStr2)) {
				$myInt1 = int($myStr2);		
			} else {
				print(OUT "<!-- ERROR! Not numeric [\$myStr2]: $myStr2	 -->\n");
			}			
			
			$colspecHash{ $myInt1} = $myStr1;
		}
	}
	
	# convert XPP qualified measure to CALS qualified measure...
 	foreach $key (%colspecHash) {		# CALS q-meas:  pc, pt, in	
		$colspecHash{$key} =~ s|(colwidth=\")(.+?)p(\") |$1$2pc$3 |g;
		$colspecHash{$key} =~ s|q\" |pt\" |g;
		$colspecHash{$key} =~ s|i\" |in\" |g;
		$colspecHash{$key} =~ s|colwidth\=\"(.+?)n\"|colwidth\=\"$1\*\" |g;
		$colspecHash{$key} =~ s|\n||g;
	}			
			
	# Recreate @aTemp from colspecHash...
	@aTemp = ();
	push @aTemp, "--empty--";
	for($index = 1; $index <= $myColumnCount; $index++)	{
		push @aTemp, $colspecHash{$index};
	}

	# Remove the '--empty--' first element...
	shift(@aTemp);

	if($#aTemp > -1)	{
		# Now, insert the @aTemp array into the @myT array, after the <tgroup...> item...
		# find index of <setwid...
		
		$index = 0;
		
		if($setwidFlag eq "yes") {
			while( ($myT[$index] !~ /<setwid;/) & ($index <= $#myT) )	{
				$index++;
			}
			
			push @aTemp, "<tbody>";
			splice @myT, $index, 1, @aTemp;		# insert the <colspec...> lines into @myT...
			
		} else	{
			while( ($myT[$index] !~ /<row/) & ($index <= $#myT) )	{
				$index++;
			}
			push @aTemp, "<tbody>";
			push @aTemp, "$myT[$index]";
		
			splice @myT, $index, 1, @aTemp;		# insert the <colspec...> lines into @myT...
		}

	} else {
				
		# No setwid but still need the <tbody> element before the first <row>...
		$index = 0;
		while( ($myT[$index] !~ /<row>/) & ($index <= $#myT) )	{
			$index++;
		}
		push @aTemp, "<tbody><!-- aTemp EMPTY -->";		
		splice @myT, $index, $#aTemp, @aTemp;		
	}

#????????????????????????????????????????????????
# ******************************************
#		NOTE:  Deal with the following Phase-II
# ******************************************
#<chgrow;xrule>
# <setcol...> not used by WEB  Phase-I
# <setctr;ctr>
#  "\<setall;(*)\> Xy <setall;macro-to-process;arg>
#  "\<setgut;(*)\> Xy <setgut;[row|col|style1|style2];row#/col#[0|-+];0 or qualified numeric>	
# ******************************************


# print(OUT "\n<!-- [ConvertTable - BEFORE Closing /entry tags]: \@myT -->\n<!-- \n");
# no warnings 'utf8';
# for($index=0;		$index <= $#myT;		$index++ ) {
	# print OUT "[$index] $myT[$index]\n"; 	
# }
# use warnings;
# print(OUT "-->\n<!-- [ConvertTable - DONE BEFORE Closing /entry tags] \@myT  -->\n\n");


	($openRow, $openEntry) = "no";
	
	# Close all open <entry> tags in @myT items...
 	for($index=0;		$index <= $#myT;		$index++ ) {
 		
	# quad stuff...  ex.  <?xpp qc?>  (center, char, justify, left, or right)
#print OUT "\n<!-- INPUT: entry-align=$myT[$index] -->\n";				
		
		if($myT[$index] =~ /<\?xpp q(.?[clrja])\?>/)	{			#NOTE:  DOES NOT ACCOUNT FOR EXISTING 'style' ATTRIBUTE...
			if($myT[$index] =~ /<entry>/)	{					    # WITHOUT ATTRIBUTES...
                $myT[$index] =~ s|<entry>(.*?)(<\?xpp qa\?>)$|<entry>$1|g;
                $myT[$index] =~ s|<entry>(.*?)(<\?xpp qc\?>)$|<entry align=\"center\">$1|g;
                $myT[$index] =~ s|<entry>(.*?)(<\?xpp qj\?>)$|<entry align=\"justify\">$1|g;
                $myT[$index] =~ s|<entry>(.*?)(<\?xpp ql\?>)$|<entry align=\"left\">$1|g;
                $myT[$index] =~ s|<entry>(.*?)(<\?xpp qr\?>)$|<entry align=\"right\">$1|g;
            } elsif($myT[$index] =~ /<entry /) {					# WITH ATTRIBUTES...
                $myT[$index] =~ s|<entry(.+?)>(.*?)(<\?xpp qc\?>)$|<entry align=\"center\" $1>$2|g;
                $myT[$index] =~ s|<entry(.+?)>(.*?)(<\?xpp qj\?>)$|<entry align=\"justify\" $1>$2|g;
                $myT[$index] =~ s|<entry(.+?)>(.*?)(<\?xpp ql\?>)$|<entry align=\"left\" $1>$2|g;
                $myT[$index] =~ s|<entry(.+?)>(.*?)(<\?xpp qr\?>)$|<entry align=\"right\" $1>$2|g;
			}       
		}		 	
		
		if($myT[$index] =~ /<setwid/)		{
			splice @myT, $index, 1, "";
 		}
 		
 		if($#myT != $index)	{									# not last item...
			if($myT[$index] =~ /<entry/)  {							# contains an <entry tag...
				
				# fcell stuff...  ex.  <?xpp fcell;15?>
				if($myT[$index] =~ /<\?xpp fcell;(\d*+)\?>/)	{		#NOTE:  DOES NOT ACCOUNT FOR EXISTING 'style' ATTRIBUTE...
					if($myT[$index] =~ /<entry>/)	{				# WITHOUT ATTRIBUTES...
						$myT[$index] =~ s|<entry>(<\?xpp fcell;)(\d+)\?>(.*?)|<entry style=\"bg_$2\">$3|g;
						$myT[$index] =~ s|<\?xpp fcell;(\d+)\?>||g;
					} else {											# WITH ATTRIBUTES...
						$myT[$index] =~ s|<entry(.+)>(<\?xpp fcell;)(\d+)\?>(.+?)|<entry style=\"bg_$3\" $1>$4|g;
						$myT[$index] =~ s|<\?xpp fcell;(\d+)\?>||g;
					}
				}
								
				if( $myT[($index + 1)] =~ /(^<entry)|(^<\/row)|(^<\/table)/) 		{	# next item?
					$myT[$index] =~ s|(.*)$|$1<\/entry>|s;			# ADD LAST CLOSING </entry>		
						
				} elsif($myT[($index + 1)] !~ /(^<entry)|(^<\/row)|(^<\/table)/)    	{	# ! entry/row/table?
					
					while($myT[($index + 1)] !~ /(^<entry)|(^<\/row)|(^<\/table)/)	{	# $index++ until entry/row/table
						$index++;					
					}		
					
					$myT[$index] =~ s|(.*)$|$1\n<\/entry>|s;			# ADD LAST CLOSING </entry>		
					
				} else {
					print OUT "<!-- Other ... -->\n+++\$index=$index \t\t$myT[$index]";
				}
			}	
		}
	}
	
	# close tgroup...
	$str = qq(<\/row>) . "\|\|" . qq(<\/tbody>) . "\|\|" . qq(<\/tgroup>) . "\|\|" . $myT[($#myT)];
	@Astr = split /\|\|/, $str;		
	splice @myT, $#myT, 3, @Astr;
	    
    # Move all PIs found between <table...<row> to position right after the table element...
    $str = "";
    for($index=0; $index<=$#myT; $index++) {
        while($myT[$index] !~ /<row/) {
            if($myT[$index] =~ /^<\?xpp/) {
                $str .= $myT[$index] . "\n";
                $myT[$index] = "";          # get rid of duplicate entry
            }
            $index++;
        }
        
        $str =~ s|^(.*?)\s+$|$1|mg;     # trim ending whitespace
        $myT[0] .= "\n$str";  # append to end of <table...> element
        $index = $#myT + 1;
    }
    
# 	load @myT into $sTable and return it from this subroutine...
	$str = "";
 	for($index=0; $index <= $#myT; $index++ ) {
 		if($myT[$index] !~ /^$/) {
		
			if($myT[$index] =~ /<\?xpp setall;vstyle;(.+?)\?>/) {
				$str = $1;
			}
			
			if( (length $str > 0) 
                and ($myT[$index] !~ /<\?xpp vstyle;/) 
                and ($myT[$index] =~ /<entry/)  
                ) {
                
                $myT[$index] =~ s|<entry>|<entry><\?xpp vstyle;$str\?>|g;					
                
                if($myT[$index] !~ /<\?xpp vstyle;/) {
                    $myT[$index] =~ s|<entry(.+?[^>])>|<entry$1><\?xpp vstyle;$str\?>|g;
                }
                
                $myT[$index] =~ s|<\?xpp vstyle;t\?>|<\?xpp vstyle;top\?>|g;
                $myT[$index] =~ s|<\?xpp vstyle;1\?>|<\?xpp vstyle;top\?>|g;
                $myT[$index] =~ s|<\?xpp vstyle;c\?>|<\?xpp vstyle;middle\?>|g;
                $myT[$index] =~ s|<\?xpp vstyle;2\?>|<\?xpp vstyle;middle\?>|g;
                $myT[$index] =~ s|<\?xpp vstyle;center\?>|<\?xpp vstyle;middle\?>|g;
                $myT[$index] =~ s|<\?xpp vstyle;b\?>|<\?xpp vstyle;bottom\?>|g;
                $myT[$index] =~ s|<\?xpp vstyle;3\?>|<\?xpp vstyle;bottom\?>|g;				
			}
			
			$sTable .= "$myT[$index]\n";	
		}
	}	
# print(OUT "\n<!-- --------------------- [ConvertTable - END]: \@myT ---------------------- -->\n");
# $myDump = Dumper(@myT);
# print(OUT $myDump);
	
 	return($sTable);
}


#----------------------------------------------------------------------
sub find_nth {
#----------------------------------------------------------------------
	# example:  print find_nth($str,$c,$n), "\n";
	my ($s, $c, $n) = @_;
	my $pos = -1;

	while ($n--) {
		$pos = index($s, $c, $pos+1);
		return -1 if $pos == -1;
	}
	
	return $pos;
}


#----------------------------------------------------------------------
sub getImgData	{
#----------------------------------------------------------------------
# NOTE:  $line may contain multiple <comment note='ig..>...</comment> elements
	my $line = shift;	# $str
	
	my $idx1 = -1;
	my $idx2 = -1;
	my $position = -1;
	my $idx_GRAPH = -1;
	my $idx_CAPT = -1;
	my $idx_TITL = -1;
	my $printCaption="";
	my $holdCaption="";
	my $holdTitle="";
	my $ss = "";
	my $pickup_position = "";
	my $i;
	my $idx = 0;
	my $ig_ctr = 0;
	
	my @a_graphtrim = ();
	my $graphstr = "";
	
	my @a_igtag = ();
	my @a_igtrim = ();
	my $igstr = "";

	my $igtrimstr = "";
	my $imgTrim = "";
	my $imgFile = "";
	my $imgAlt = "";
	my $imgSrc = "";
	my $imgHeight = "";
	my $imgWidth = "";
	my $imgProcess = "";
	my $imgScale = "";
	my $rtnString = "";
	
	if($str =~ /<comment note=\'pickup\'>/)		{		
		# break out lines from $str...
		@part = split(/\|/, $str);
		
		# load $ss...
		#<!-- {/PICK;4-3-C-1;14p;comptd;block;crt;def;ss;no;1;0;0;no;0;cw;no;def;def} -->
		foreach (@part) {
		
			if($_ =~ /\/PICK;/) {
				# get position (crt for example)...
				$idx = find_nth($_, ";", 5);	# get index of 5th occurrence of the ";" character
				$pickup_position = substr($_, $idx + 2, 1);	# get only the middle character (left, right, or center);
				
				if($pickup_position eq "l") {
					$pickup_position = "left";
				} elsif($pickup_position eq "c") {
					$pickup_position = "center";
				} elsif($pickup_position eq "r") {
					$pickup_position = "right";
				} else {
					$pickup_position = "center";	# otherwise
				}			
				
				# Is keepout an "ss" format?
				$idx = find_nth($_, ";", 7);	# get index of 7th occurance of the ";" character
				$ss = substr($_, $idx + 1, 2);
				
				if($ss eq "ss") {
					print OUT "<comment note=\"div\">style=\"float:$pickup_position\"<\/comment>\n";
				}
			}
		}
		
		# Get before or after CAPT...
		# Decide if ALT title text is present
		for($i = 0; $i <= $#part; $i++) {
			if($part[$i] =~ /\{\/CAPT;/)	{ 
				$idx_CAPT=$i; 
				$holdCaption = $part[$i];
			}
			
			if($part[$i] =~ /\{\/GRAPH;/)	{
				 $idx_GRAPH=$i;
			}

			if($part[$i] =~/\{\/TITL;/)		{
				$idx_TITL=$i;
				$holdTitle = $part[$i];
			}			
		}
		 
		# CAPTION before GRAPHIC?	 
		if(($idx_CAPT > -1) && ($idx_CAPT < $idx_GRAPH) )	{
			$printCaption = CAPT_2_Para($holdCaption, $pickup_position); 
		 	print OUT "$printCaption\n";
		}
		
		foreach $pickup_line (@part)	{
			if($pickup_line =~ /\/GRAPH;/)	{		
				$idx1 = index($pickup_line, "\/GRAPH;");		# (returns -1 if not found)
						
				if($idx1 ne -1)	{
					$imgFile = "";
					$imgWidth = "";
					$imgHeight = "";
					$imgProcess = "";
					$imgScale = "";
					
					$rtnString = "";
					@a_graphtrim = ();
					
					#<!-- {/GRAPH;2-70word.jpg;6.67i;7.37i;center;top;0;0;cw;normal;0;0;0;edge;edge;best;prior} -->
					#print STDOUT "\t[GRAPH] \$pickup_line=$pickup_line\n";	
	
					$imgTrim = $pickup_line;
					$imgTrim =~ s|^(.+?)\{\/GRAPH;(.+?)\}(.*?)$|$2|g;
					
					@a_graphtrim = ();
					@a_graphtrim = split /;/, $imgTrim;
										
					if($#a_graphtrim > -1) { $imgFile = $a_graphtrim[0]; }
					if($#a_graphtrim > 0) { $imgWidth = $a_graphtrim[1]; }
					if($#a_graphtrim > 1) { $imgHeight = $a_graphtrim[2]; }
					if($#a_graphtrim > 13) { $imgScale = $a_graphtrim[14]; }		#best, width, or depth
					$imgFile =~ s|(.+?)\.eps$|$1\.png|ig;				
					$imgFile =~ s|(.+?)\.tif$|$1\.png|ig;				
					$imgFile =~ s|(.+?)\.jpg$|$1\.png|ig;			
					$imgSrc = ".\/images/$imgFile";				
					
					# Assign alt text. If TITLE is present, it takes priority. Else, Caption. Else, imgfile name.								
					if($idx_TITL > -1 )		{
						$findAlt = TITL_2_Alt($holdTitle);
						$imgAlt = $findAlt;	
						
					} elsif(($idx_TITL == -1) && ($idx_CAPT > -1))	{				
						$findAlt = TITL_2_Alt($holdCaption);	
						$imgAlt = $findAlt;
						
					} elsif (($idx_TITL == -1) && ($idx_CAPT == -1))	{
						$imgAlt = $imgFile;
						$imgAlt =~ s|\.png||g;
					}

# --- START DEBUGGING ---		
#if($imgFile =~ /24H\.208\.2\.B/) {			
#	print STDOUT "\n\t\$imgFile=$imgFile\n";
#	print STDOUT "\t\$imgWidth=$imgWidth\n";
#	print STDOUT "\t\$imgHeight=$imgHeight\n";
#	print STDOUT "\t\$imgScale=$imgScale\n";
#	print STDOUT "\t\$imgSrc=$imgSrc\n============\n";
#}
# --- END DEBUGGING ---					

					# NOTE:  HTML5 DOES NOT ALLOW FOR WIDTH OR HEIGHT PERCENTAGES, ONLY PIXELS (96/inch)
					#           PERCENTAGES FOR WIDTH AND HEIGHT WILL BE CONVERTED TO PIXELS IN ImageInfo.pl "StepE" of G2H.
					if(($imgScale eq "best") | ($imgScale eq "none") ) {
						
						if( ($imgWidth  =~ /^[0-9]/) and ($imgHeight =~ /^[0-9]/) ) {
							$rtnString = "<img alt=\"$imgAlt\" src=\"$imgSrc\" width=\"$imgWidth\" height=\"$imgHeight\" \/>";
							
						} elsif( ($imgWidth =~ /^[0-9]/) and ($imgHeight !~ /^[0-9]/) ) {					# width
							$rtnString = "<img alt=\"$imgAlt\" src=\"$imgSrc\" width=\"$imgWidth\" height=\"\" \/>";
							
						} elsif( ($imgWidth !~ /^[0-9]/) and ($imgHeight  =~ /^[0-9]/) ) {				# height
							$rtnString = "<img alt=\"$imgAlt\" src=\"$imgSrc\" width=\"\" height=\"$imgHeight\" \/>";
							
						} elsif( ($imgWidth !~ /^[0-9]/) and ($imgHeight !~ /^[0-9]/) ) {					# neither
							$rtnString = "<img alt=\"$imgAlt\" src=\"$imgSrc\" width=\"\" height=\"\" \/>";
						}
						
					} elsif($imgScale eq "width") {
						
						if( $imgWidth  =~ /^[0-9]/ )	 {												# width
							$rtnString = "<img alt=\"$imgAlt\" src=\"$imgSrc\" width=\"$imgWidth\" height=\"\" \/>";
							
						} elsif( $imgWidth !~ /^[0-9]/ ) {											# NO width
							$rtnString = "<img alt=\"$imgAlt\" src=\"$imgSrc\" width=\"\" height=\"\" \/>";
						}					
						
					} elsif ($imgScale eq "depth") {
						
						if( $imgHeight  =~ /^[0-9]/ ) {												# height
							$rtnString = "<img alt=\"$imgAlt\" src=\"$imgSrc\" width=\"\" height=\"$imgHeight\" \/>";	
									
						} elsif( $imgHeight !~ /^[0-9]/ ) {											# NO width
							$rtnString = "<img alt=\"$imgAlt\" src=\"$imgSrc\" width=\"\" height=\"$imgHeight\" \/>";
						}
						
					} elsif ($imgScale =~ /^[0-9]/) {	# percentage, so clear out width and height					
						$rtnString = "<img alt=\"$imgAlt\" src=\"$imgSrc\" width=\"$imgScale\%\" height=\"\" \/>";
						
					} else {
						# $imgScale height blank or wrong data...
						$rtnString = "<img alt=\"$imgAlt\" src=\"$imgSrc\" width=\"100%\" height=\"\" \/>";
					}					
						
					#print STDOUT "[debug getImgData OUT]:\n$rtnString\n";
					print OUT "$rtnString\n";
					
				}	#if($idx1 ne -1
			} # if(.../GRAPH;
		} # foreach...
		
		if(($idx_CAPT > -1) && ($idx_CAPT < $idx_GRAPH) ) {
			# close the CAPT paragraph, thus including the graphic content.
			print OUT "</para>\n";
		}
		
		# GRAPHIC before CAPTION?
		if(($idx_CAPT > -1) && ($idx_CAPT > $idx_GRAPH) )	{	 
		 	$printCaption = CAPT_2_Para($holdCaption, $pickup_position); 
		 	print OUT "$printCaption\n";
			print OUT "</para>\n";
		}
		
		if($ss =~ /ss/) {
			print OUT "<comment note=\"div_close\"\/>\n";
		}

	} elsif ($line =~ /<comment note=\'ig/)		{
		# <img alt(short-desc), src(URL), height(px/%), ismap(URL), longdesc(URL), usemap(URL), & width(px/%).
		# GenCode:  <ig; name; width; depth; process; scale; rotate; color; priority; cx; cy; cw; cd>
		
		# 2013-03-28 - Allow for MULTIPLE '<ig*' in a given paragraph...
		$line =~ s|(<comment note=\'ig)|'~~~'$1|g;
		@a_igtag=();
		@a_igtag = split /'~~~'/, $line;	

		foreach $igstr (@a_igtag)	{
			if($igstr =~ /^<comment note=\'ig/)	{
				#  Sample:  <comment note='ig'><!-- <ig;ClosedCircle.tif;1p;10q;opaque;width; --></comment>
				#print STDOUT "-----\n[A]$igstr\n\n";
				
				$imgFile = "";
				$imgAlt = "";
				$imgWidth = "";
				$imgHeight = "";
				$imgProcess = "";
				$imgScale = "";

				#if alt is present just set it now, why not.
				if ($igstr =~ /<alt>/) {
					$imgAlt = $igstr;
					$imgAlt =~ s|^.*<alt>(.*)<\/alt>.*$|$1|g;
				}

				#continue with deciphering the IG
				$imgTrim = $igstr;
				$imgTrim =~ s|^(.+?)<ig;(.+?)>(.*?)$|$2|g;					
				$imgTrim =~ s|^(.+?)<igt;(.+?)>(.*?)$|$2|g;					
				$imgTrim =~ s|^(.+?);$|$1|g;
				#print STDOUT "\t[trim] \$imgTrim=$imgTrim\n";	
				
				@a_igtrim = ();
				@a_igtrim = split /;/, $imgTrim;
				
				#print STDOUT "+++ \$#a_igtrim=$#a_igtrim +++\n\n";
				
				if($#a_igtrim > -1) { $imgFile = $a_igtrim[0]; }
				if($#a_igtrim > 0) { $imgWidth = $a_igtrim[1]; }
				if($#a_igtrim > 1) { $imgHeight = $a_igtrim[2]; }
				if($#a_igtrim > 2) { $imgProcess = $a_igtrim[3]; }
				if($#a_igtrim > 3) { $imgScale = $a_igtrim[4]; }
				$imgFile =~ s|(.+?)\.eps$|$1\.png|ig;				
				$imgFile =~ s|(.+?)\.tif$|$1\.png|ig;				
				$imgFile =~ s|(.+?)\.jpg$|$1\.png|ig;				
				$imgSrc = ".\/images/$imgFile";

				#set imgalt if there wasn't optional tag
				if ($imgAlt eq "" ) {
					$imgAlt = $imgFile;
					$imgAlt =~ s|\.png||g;
				}

#print STDOUT "[debug getImgData(pick new)] \$imgSrc=$imgSrc\n\n";		
	
					
# --- DEBUGGING: ---										
#				print STDOUT "----- \@a_igtrim DUMP -----\n";
#				foreach $igtrim (@a_igtrim) {
#					print STDOUT "\t$igtrim\n";	
#				}
#				print STDOUT "----- END \@a_igtrim DUMP -----\n\n";
#					
#				print STDOUT "\t\$imgFile=$imgFile\n";
#				print STDOUT "\t\$imgWidth=$imgWidth\n";
#				print STDOUT "\t\$imgHeight=$imgHeight\n";
#				print STDOUT "\t\$imgProcess=$imgProcess\n";
#				print STDOUT "\t\$imgScale=$imgScale\n";
#				print STDOUT "\t\$imgSrc=$imgSrc\n\n";
# --- END DEBUGGING ---

				# Converting XPP's qualified measurments to pixel values...
				if( $imgWidth ne "" and $imgHeight ne "")	{
					$rtnString .= "<img alt=\"$imgAlt\" src=\"$imgSrc\" width=\"$imgWidth\" height=\"$imgHeight\" \/>\n";
					
				} elsif( $imgWidth ne "" and $imgHeight eq "") {
					$rtnString .= "<img alt=\"$imgAlt\" src=\"$imgSrc\" width=\"$imgWidth\" height=\"\"\/>\n";
					
				} elsif( $imgWidth eq "" and $imgHeight ne "") {
					$rtnString .= "<img alt=\"$imgAlt\" src=\"$imgSrc\" width=\"\" height=\"$imgHeight\" \/>\n";
					
				} elsif($imgWidth eq "" and $imgHeight eq "" and $imgScale =~ /^[0-9]/ ) {
					$rtnString .= "<img alt=\"$imgAlt\" src=\"$imgSrc\" width=\"$imgScale%\" height=\"$imgScale%\" \/>\n";
					
				} else {
					$rtnString .= "<img alt=\"$imgAlt\" src=\"$imgSrc\" width=\"100%\" height=\"100%\" \/>\n";
				}
			} # END ...if($igstr =~  /^<comment note=\'ig/)
			
			$rtnString .= $igstr;
			
		} # END ...foreach $igstr (@a_igtag)
		
		return $rtnString;
			
	} else {
		return -1;		#bad data
	}
	
}


#----------------------------------------------------------------------
#sub ValidateImageName {
#----------------------------------------------------------------------
	# my $imageName = (shift);
	# my $imageBaseName = $imageName;
	
	# $imageBaseName =~ s|^(.*?)\.png$|$1|g;
	
	# if($imageBaseName =~ /^[0-9a-zA-Z\-\_]+$/) {
		##do nothing
	# } else {
		# push @aImageNameInvalid, $imageName;
	# }
# }


#----------------------------------------------------------------------
sub GetSpanColValue	{
#----------------------------------------------------------------------
	my $SCkey = int(shift);

	if(exists $spanCol{$SCkey}) {
		return $spanCol{$SCkey};
	} else {
		return 0;	
	}
}


##-------------------------------------------------------------
sub p1_GenCodeEntities {
##-------------------------------------------------------------
	my $parm = shift;
	my $flag=0;
		
	$parm =~ s|;times;|\x{00D7}|g;			# GenCode
	$parm =~ s|\&Aacute;|\x{00C1}|g;	
	$parm =~ s|\&aacute;|\x{00E1}|g;	
	$parm =~ s|\&Acirc;|\x{00C2}|g;			
	$parm =~ s|\&acirc;|\x{00E2}|g;		
	$parm =~ s|\&acute;|\x{00B4}|g;		
	$parm =~ s|\&AElig;|\x{00C6}|g;		
	$parm =~ s|\&aelig;|\x{00E6}|g;		
	$parm =~ s|\&Agrave;|\x{00C0}|g;		
	$parm =~ s|\&agrave;|\x{00E0}|g;	
	$parm =~ s|\&alef;|\x{05D0}|g;			
	$parm =~ s|\&alefsym;|\x{2135}|g;		
	$parm =~ s|\&aleph;|\x{2135}|g;
	$parm =~ s|\&Alpha;|\x{0391}|g;		
	$parm =~ s|\&alpha;|\x{03B1}|g;		
	$parm =~ s|\&amalg;|\x{2201}|g;
	$parm =~ s|\&and;|\x{2227}|g;
	$parm =~ s|\&ang;|\x{2220}|g;
	$parm =~ s|\&ang90;|\x{221F}|g;
	$parm =~ s|\&angmsd;|\x{2221}|g;
	$parm =~ s|\&angsph;|\x{2222}|g;
	$parm =~ s|\&angst;|\x{212B}|g;
	$parm =~ s|\&ap;|\x{2248}|g;
	$parm =~ s|\&ape;|\x{224A}|g;
	$parm =~ s|\&apos;|\x{0027}|g;		
	$parm =~ s|\&Aring;|\x{00C5}|g;		
	$parm =~ s|\&aring;|\x{00E5}|g;		
	$parm =~ s|\&asterisk;|\x{273D}|g;		
	$parm =~ s|\&asymp;|\x{2248}|g;		
	$parm =~ s|\&asymp;|\x{2248}|g;
	$parm =~ s|\&Atilde;|\x{00C3}|g;		
	$parm =~ s|\&atilde;|\x{00E3}|g;		
	$parm =~ s|\&Auml;|\x{00C4}|g;		
	$parm =~ s|\&auml;|\x{00E4}|g;		
	$parm =~ s|\&b\.alpha;|\x{03B1}|g;
	$parm =~ s|\&b\.beta;|\x{03B2}|g;
	$parm =~ s|\&b\.chi;|\x{03C7}|g;
	$parm =~ s|\&b\.Delta;|\x{0394}|g;
	$parm =~ s|\&b\.delta;|\x{03B4}|g;
	$parm =~ s|\&b\.epsi;|\x{03B5}|g;
	$parm =~ s|\&b\.epsis;|\x{03B5}|g;
	$parm =~ s|\&b\.epsiv;|\x{025B}|g;
	$parm =~ s|\&b\.eta;|\x{03B7}|g;
	$parm =~ s|\&b\.Gamma;|\x{0393}|g;
	$parm =~ s|\&b\.gamma;|\x{03B3}|g;
	$parm =~ s|\&b\.gammad;|\x{03DC}|g;
	$parm =~ s|\&b\.iota;|\x{03B9}|g;
	$parm =~ s|\&b\.kappa;|\x{03BA}|g;
	$parm =~ s|\&b\.kappav;|\x{03F0}|g;
	$parm =~ s|\&b\.Lambda;|\x{039B}|g;
	$parm =~ s|\&b\.lambda;|\x{03BB}|g;
	$parm =~ s|\&b\.mu;|\x{03BC}|g;
	$parm =~ s|\&b\.nu;|\x{03BD}|g;
	$parm =~ s|\&b\.Omega;|\x{03A9}|g;
	$parm =~ s|\&b\.omega;|\x{03C9}|g;
	$parm =~ s|\&b\.Phi;|\x{03A6}|g;
	$parm =~ s|\&b\.phis;|\x{03C6}|g;
	$parm =~ s|\&b\.phiv;|\x{03D5}|g;
	$parm =~ s|\&b\.Pi;|\x{03A0}|g;
	$parm =~ s|\&b\.pi;|\x{03C0}|g;
	$parm =~ s|\&b\.piv;|\x{03D6}|g;
	$parm =~ s|\&b\.Psi;|\x{03A8}|g;
	$parm =~ s|\&b\.psi;|\x{03C8}|g;
	$parm =~ s|\&b\.rho;|\x{03C1}|g;
	$parm =~ s|\&b\.rhov;|\x{03F1}|g;
	$parm =~ s|\&b\.Sigma;|\x{03A3}|g;
	$parm =~ s|\&b\.sigma;|\x{03C3}|g;
	$parm =~ s|\&b\.sigmav;|\x{03C2}|g;
	$parm =~ s|\&b\.tau;|\x{03C4}|g;
	$parm =~ s|\&b\.Theta;|\x{0398}|g;
	$parm =~ s|\&b\.thetas;|\x{03B8}|g;
	$parm =~ s|\&b\.thetav;|\x{03D1}|g;
	$parm =~ s|\&b\.upsi;|\x{03C5}|g;
	$parm =~ s|\&b\.Upsi;|\x{03D2}|g;
	$parm =~ s|\&b\.Xi;|\x{039E}|g;
	$parm =~ s|\&b\.xi;|\x{03BE}|g;
	$parm =~ s|\&b\.zeta;|\x{03B6}|g;
	$parm =~ s|\&barwed;|\x{22BC}|g;
	$parm =~ s|\&Barwed;|\x{2306}|g;
	$parm =~ s|\&bcong;|\x{224C}|g;
	$parm =~ s|\&bdquo;|\x{201E}|g;		
	$parm =~ s|\&becaus;|\x{2235}|g;
	$parm =~ s|\&bepsi;|\x{220D}|g;
	$parm =~ s|\&bernou;|\x{212C}|g;
	$parm =~ s|\&Beta;|\x{0392}|g;		
	$parm =~ s|\&beta;|\x{03B2}|g;		
	$parm =~ s|\&beth;|\x{2136}|g;
	$parm =~ s|\&blk12;|\x{2592}|g;
	$parm =~ s|\&blk14;|\x{2591}|g;
	$parm =~ s|\&blk34;|\x{2593}|g;
	$parm =~ s|\&block;|\x{2588}|g;
	$parm =~ s|\&bottom;|\x{22A5}|g;
	$parm =~ s|\&bowtie;|\x{22C8}|g;
	$parm =~ s|\&bprime;|\x{2035}|g;
	$parm =~ s|\&brvbar;|\x{00A6}|g;		
	$parm =~ s|\&bsim;|\x{223D}|g;
	$parm =~ s|\&bsime;|\x{22CD}|g;
	$parm =~ s|\&bull;|\x{2022}|g;			
	$parm =~ s|\&bullet;|\x{2219}|g;
	$parm =~ s|\&bullW;|\x{25E6}|g;
	$parm =~ s|\&bump;|\x{224E}|g;
	$parm =~ s|\&bumpe;|\x{224F}|g;
	$parm =~ s|\&cap;|\x{2229}|g;
	$parm =~ s|\&Cap;|\x{22D2}|g;
	$parm =~ s|\&caret;|\x{2041}|g;
	$parm =~ s|\&Ccedil;|\x{00C7}|g;		
	$parm =~ s|\&ccedil;|\x{00E7}|g;		
	$parm =~ s|\&cedil;|\x{00B8}|g;		
	$parm =~ s|\&cent;|\x{00A2}|g;		
	$parm =~ s|\&check;|\&#x2713;|g;
	$parm =~ s|\&check;|\x{2713}|g;
	$parm =~ s|\&checkbox;|\x{2611}|g;
	$parm =~ s|\&Chi;|\x{03A7}|g;			
	$parm =~ s|\&chi;|\x{03C7}|g;
	$parm =~ s|\&cir;|\x{25CB}|g;
	$parm =~ s|\&circ;|\x{02C6}|g;			
	$parm =~ s|\&circle0;|\x{24EA}|g;
	$parm =~ s|\&circle1;|\x{2460}|g;
	$parm =~ s|\&circle10;|\x{2469}|g;
	$parm =~ s|\&circle11;|\x{246A}|g;
	$parm =~ s|\&circle12;|\x{246B}|g;
	$parm =~ s|\&circle13;|\x{246C}|g;
	$parm =~ s|\&circle14;|\x{246D}|g;
	$parm =~ s|\&circle15;|\x{246E}|g;
	$parm =~ s|\&circle16;|\x{246F}|g;
	$parm =~ s|\&circle17;|\x{2470}|g;
	$parm =~ s|\&circle18;|\x{2471}|g;
	$parm =~ s|\&circle19;|\x{2472}|g;
	$parm =~ s|\&circle19;|\x{2472}|g;
	$parm =~ s|\&circle2;|\x{2461}|g;
	$parm =~ s|\&circle20;|\x{2473}|g;
	$parm =~ s|\&circle3;|\x{2462}|g;
	$parm =~ s|\&circle4;|\x{2463}|g;
	$parm =~ s|\&circle5;|\x{2464}|g;
	$parm =~ s|\&circle6;|\x{2465}|g;
	$parm =~ s|\&circle7;|\x{2466}|g;
	$parm =~ s|\&circle8;|\x{2467}|g;
	$parm =~ s|\&circle9;|\x{2468}|g;
	$parm =~ s|\&circleA;|\x{24b6}|g;
	$parm =~ s|\&circlea;|\x{24D0}|g;
	$parm =~ s|\&circleB;|\x{24b7}|g;
	$parm =~ s|\&circleb;|\x{24D1}|g;
	$parm =~ s|\&circleBlack;|\x{25cf}|g;
	$parm =~ s|\&circleBtmK;|\x{25d2}|g;
	$parm =~ s|\&circleC;|\x{24b8}|g;
	$parm =~ s|\&circlec;|\x{24D2}|g;
	$parm =~ s|\&circleD;|\x{24b9}|g;
	$parm =~ s|\&circled;|\x{24D3}|g;
	$parm =~ s|\&circleE;|\x{24bA}|g;
	$parm =~ s|\&circlee;|\x{24D4}|g;
	$parm =~ s|\&circleF;|\x{24bB}|g;
	$parm =~ s|\&circlef;|\x{24D5}|g;
	$parm =~ s|\&circleG;|\x{24bC}|g;
	$parm =~ s|\&circleg;|\x{24D6}|g;
	$parm =~ s|\&circleH;|\x{24bD}|g;
	$parm =~ s|\&circleh;|\x{24D7}|g;
	$parm =~ s|\&circleI;|\x{24BE}|g;
	$parm =~ s|\&circlei;|\x{24D8}|g;
	$parm =~ s|\&circleJ;|\x{24BF}|g;
	$parm =~ s|\&circlej;|\x{24D9}|g;
	$parm =~ s|\&circleK;|\x{24C0}|g;
	$parm =~ s|\&circlek;|\x{24DA}|g;
	$parm =~ s|\&circleL;|\x{24C1}|g;
	$parm =~ s|\&circlel;|\x{24DB}|g;
	$parm =~ s|\&circleLgW;|\x{25EF}|g;
	$parm =~ s|\&circleLtK;|\x{25d0}|g;
	$parm =~ s|\&circleM;|\x{24C2}|g;
	$parm =~ s|\&circlem;|\x{24DC}|g;
	$parm =~ s|\&circleN;|\x{24C3}|g;
	$parm =~ s|\&circlen;|\x{24DD}|g;
	$parm =~ s|\&circleO;|\x{24C4}|g;
	$parm =~ s|\&circleo;|\x{24DE}|g;
	$parm =~ s|\&circleP;|\x{24C5}|g;
	$parm =~ s|\&circlep;|\x{24DF}|g;
	$parm =~ s|\&circleQ;|\x{24C6}|g;
	$parm =~ s|\&circleq;|\x{24E0}|g;
	$parm =~ s|\&circleR;|\x{24C7}|g;
	$parm =~ s|\&circler;|\x{24E1}|g;
	$parm =~ s|\&circleRtK;|\x{25d1}|g;
	$parm =~ s|\&circleS;|\x{24C8}|g;
	$parm =~ s|\&circles;|\x{24E2}|g;
	$parm =~ s|\&circleT;|\x{24C9}|g;
	$parm =~ s|\&circlet;|\x{24E3}|g;
	$parm =~ s|\&circleTopK;|\x{25d3}|g;
	$parm =~ s|\&circleU;|\x{24CA}|g;
	$parm =~ s|\&circleu;|\x{24E4}|g;
	$parm =~ s|\&circleV;|\x{24CB}|g;
	$parm =~ s|\&circlev;|\x{24E5}|g;
	$parm =~ s|\&circleW;|\x{24CC}|g;
	$parm =~ s|\&circlew;|\x{24E6}|g;
	$parm =~ s|\&circleWhite;|\x{25cb}|g;
	$parm =~ s|\&circleX;|\x{24CD}|g;
	$parm =~ s|\&circlex;|\x{24E7}|g;
	$parm =~ s|\&circleY;|\x{24CE}|g;
	$parm =~ s|\&circley;|\x{24E8}|g;
	$parm =~ s|\&circleZ;|\x{24CF}|g;
	$parm =~ s|\&circlez;|\x{24E9}|g;
	$parm =~ s|\&cire;|\x{2257}|g;
	$parm =~ s|\&clubs;|\x{2663}|g;
	$parm =~ s|\&colone;|\x{2254}|g;
	$parm =~ s|\&commat;|\x{0040}|g;
	$parm =~ s|\&comp;|\x{2201}|g;
	$parm =~ s|\&compfn;|\x{2218}|g;
	$parm =~ s|\&cong;|\x{2245}|g;
	$parm =~ s|\&conint;|\x{222E}|g;
	$parm =~ s|\&coprod;|\x{2210}|g;
	$parm =~ s|\&copy;|\x{00A9}|g;		
	$parm =~ s|\&copy;|\x{00A9}|g;
	$parm =~ s|\&copysr;|\x{2117}|g;
	$parm =~ s|\&crarr;|\x{21B5}|g;		
	$parm =~ s|\&cross;|\x{2717}|g;
	$parm =~ s|\&cuepr;|\x{22DE}|g;
	$parm =~ s|\&cuesc;|\x{22DF}|g;
	$parm =~ s|\&cularr;|\x{21B6}|g;
	$parm =~ s|\&cup;|\x{222A}|g;
	$parm =~ s|\&Cup;|\x{22D3}|g;
	$parm =~ s|\&cupre;|\x{227C}|g;
	$parm =~ s|\&curarr;|\x{21B7}|g;
	$parm =~ s|\&curren;|\x{00A4}|g;		
	$parm =~ s|\&cuvee;|\x{22CE}|g;
	$parm =~ s|\&cuwed;|\x{22CF}|g;
	$parm =~ s|\&dagger;|\x{2020}|g;		
	$parm =~ s|\&Dagger;|\x{2021}|g;
	$parm =~ s|\&daleth;|\x{2138}|g;
	$parm =~ s|\&darr;|\x{2193}|g;			
	$parm =~ s|\&dArr;|\x{21D3}|g;
	$parm =~ s|\&darr2;|\x{21CA}|g;
	$parm =~ s|\&dash;|\x{2010}|g;
	$parm =~ s|\&dashv;|\x{22A3}|g;
	$parm =~ s|\&deg;|\x{00B0}|g;			
	$parm =~ s|\&Delta;|\x{0394}|g;
	$parm =~ s|\&delta;|\x{03B4}|g;
	$parm =~ s|\&dharl;|\x{21C3}|g;
	$parm =~ s|\&dharr;|\x{21C2}|g;
	$parm =~ s|\&diaK;|\x{25C6}|g;
	$parm =~ s|\&diam;|\x{22C4}|g;
	$parm =~ s|\&diamf;|\x{2666}|g;		# diamond solid
	$parm =~ s|\&diamond;|\x{22C4}|g;	# diamond no fill
	$parm =~ s|\&diams;|\x{2666}|g;		# diamond solid
	$parm =~ s|\&diaW;|\x{25C7}|g;
	$parm =~ s|\&divide;|\x{00F7}|g;		
	$parm =~ s|\&divonx;|\x{22C7}|g;
	$parm =~ s|\&dlarr;|\x{2199}|g;
	$parm =~ s|\&dlcorn;|\x{231E}|g;
	$parm =~ s|\&dlcorn;|\x{231E}|g;
	$parm =~ s|\&Dot;|\x{00A8}|g;
	$parm =~ s|\&DotDot;|\x{20DC}|g;
	$parm =~ s|\&drarr;|\x{2198}|g;
	$parm =~ s|\&drcorn;|\x{231F}|g;
	$parm =~ s|\&dtri;|\x{25BF}|g;
	$parm =~ s|\&dtrif;|\x{25BE}|g;
	$parm =~ s|\&Eacute;|\x{00C9}|g;		
	$parm =~ s|\&eacute;|\x{00E9}|g;		
	$parm =~ s|\&ecir;|\x{2256}|g;
	$parm =~ s|\&Ecirc;|\x{00CA}|g;		
	$parm =~ s|\&ecirc;|\x{00EA}|g;		
	$parm =~ s|\&ecolon;|\x{2255}|g;
	$parm =~ s|\&eDot;|\x{2251}|g;
	$parm =~ s|\&efDot;|\x{2252}|g;
	$parm =~ s|\&Egrave;|\x{00C8}|g;		
	$parm =~ s|\&egrave;|\x{00E8}|g;		
	$parm =~ s|\&egs;|\x{22DD}|g;
	$parm =~ s|\&ell;|\x{2113}|g;
	$parm =~ s|\&els;|\x{22DC}|g;
	$parm =~ s|\&empty;|\x{2205}|g;		
	$parm =~ s|\&empty;|\x{2205}|g;
	$parm =~ s|\&emsp;|\x{2003}|g;		
	$parm =~ s|\&ensp;|\x{2002}|g;		
	$parm =~ s|\&epsi;|\x{03B5}|g;
	$parm =~ s|\&Epsilon;|\x{0395}|g;		
	$parm =~ s|\&epsilon;|\x{03B5}|g;		
	$parm =~ s|\&epsis;|\x{03B5}|g;
	$parm =~ s|\&epsiv;|\x{025B}|g;
	$parm =~ s|\&equiv;|\x{2261}|g;
	$parm =~ s|\&erDot;|\x{2253}|g;
	$parm =~ s|\&esdot;|\x{2250}|g;
	$parm =~ s|\&Eta;|\x{0397}|g;			
	$parm =~ s|\&eta;|\x{03B7}|g;
	$parm =~ s|\&ETH;|\x{00D0}|g;			
	$parm =~ s|\&eth;|\x{00F0}|g;			
	$parm =~ s|\&Euml;|\x{00CB}|g;		
	$parm =~ s|\&euml;|\x{00EB}|g;		
	$parm =~ s|\&euro;|\x{20AC}|g;		
	$parm =~ s|\&exist;|\x{2203}|g;
	$parm =~ s|\&female;|\x{2640}|g;
	$parm =~ s|\&flat;|\x{266D}|g;
	$parm =~ s|\&fnof;|\x{0192}|g;
	$parm =~ s|\&forall;|\x{2200}|g;
	$parm =~ s|\&fork;|\x{22D4}|g;
	
	# $parm =~ s|\&frac12;|\x{00BD}|g;		
	# $parm =~ s|\&frac13;|\x{2153}|g;
	# $parm =~ s|\&frac14;|\x{00BC}|g;		
	# $parm =~ s|\&frac15;|\x{2155}|g;
	# $parm =~ s|\&frac16;|\x{2159}|g;
	# $parm =~ s|\&frac23;|\x{2154}|g;
	# $parm =~ s|\&frac25;|\x{2156}|g;
	# $parm =~ s|\&frac34;|\x{00BE}|g;
	# $parm =~ s|\&frac35;|\x{2157}|g;
	# $parm =~ s|\&frac45;|\x{2158}|g;
	# $parm =~ s|\&frac56;|\x{215A}|g;
	
	$parm =~ s|\&frac14;|\x{00BC}|g;	
	$parm =~ s|\&frac12;|\x{00BD}|g;	
	$parm =~ s|\&frac34;|\x{00BE}|g;	
	$parm =~ s|\&frac13;|\x{2153}|g;	
	$parm =~ s|\&frac23;|\x{2154}|g;
	$parm =~ s|\&frac15;|\x{2155}|g;
	$parm =~ s|\&frac25;|\x{2156}|g;
	$parm =~ s|\&frac35;|\x{2157}|g;
	$parm =~ s|\&frac45;|\x{2158}|g;
	$parm =~ s|\&frac16;|\x{2159}|g;
	$parm =~ s|\&frac56;|\x{215A}|g;
	$parm =~ s|\&frac18;|\x{215B}|g;
	$parm =~ s|\&frac38;|\x{215C}|g;
	$parm =~ s|\&frac58;|\x{215D}|g;
	$parm =~ s|\&frac78;|\x{215E}|g;	
	
	$parm =~ s|\&frasl;|\x{2044}|g;			
	$parm =~ s|\&frown;|\x{2322}|g;
	$parm =~ s|\&Gamma;|\x{0393}|g;
	$parm =~ s|\&gamma;|\x{03B3}|g;
	$parm =~ s|\&gammad;|\x{03DC}|g;
	$parm =~ s|\&gap;|\x{2273}|g;
	$parm =~ s|\&gdot;|\x{0121}|g;
	$parm =~ s|\&ge;|\x{2265}|g;
	$parm =~ s|\&gE;|\x{2267}|g;
	$parm =~ s|\&gel;|\x{22DB}|g;
	$parm =~ s|\&Gg;|\x{22D9}|g;
	$parm =~ s|\&gimel;|\x{2137}|g;
	$parm =~ s|\&gl;|\x{2277}|g;
	$parm =~ s|\&gne;|\x{2269}|g;
	$parm =~ s|\&gnE;|\x{2269}|g;
	$parm =~ s|\&gnsim;|\x{22E7}|g;
	$parm =~ s|\&gsdot;|\x{22D7}|g;
	$parm =~ s|\&gsim;|\x{2273}|g;
	$parm =~ s|\&gt;|\x{003E}|g;			
	$parm =~ s|\&Gt;|\x{226B}|g;
	$parm =~ s|\&gvnE;|\x{2269}|g;
	$parm =~ s|\&half;|\x{00BD}|g;
	$parm =~ s|\&hamilt;|\x{210B}|g;
	$parm =~ s|\&harr;|\x{2194}|g;
	$parm =~ s|\&hArr;|\x{21D4}|g;
	$parm =~ s|\&harrw;|\x{21AD}|g;
	$parm =~ s|\&heart;|\x{2661}|g;
	$parm =~ s|\&hearts;|\x{2665}|g;
	$parm =~ s|\&hellip;|\x{2026}|g;
	$parm =~ s|\&Iacute;|\x{00CD}|g;		
	$parm =~ s|\&iacute;|\x{00ED}|g;		
	$parm =~ s|\&Icirc;|\x{00CE}|g;		
	$parm =~ s|\&icirc;|\x{00EE}|g;			
	$parm =~ s|\&iexcl;|\x{00A1}|g;		
	$parm =~ s|\&iff;|\x{21D4}|g;
	$parm =~ s|\&Igrave;|\x{00CC}|g;		
	$parm =~ s|\&igrave;|\x{00EC}|g;		
	$parm =~ s|\&image;|\x{2111}|g;
	$parm =~ s|\&incare;|\x{2105}|g;
	$parm =~ s|\&infin;|\x{221E}|g;
	$parm =~ s|\&inodot;|\x{0131}|g;
	$parm =~ s|\&int;|\x{222B}|g;
	$parm =~ s|\&intcal;|\x{22BA}|g;
	$parm =~ s|\&Iota;|\x{0399}|g;			
	$parm =~ s|\&iota;|\x{03B9}|g;
	$parm =~ s|\&iquest;|\x{00BF}|g;		
	$parm =~ s|\&isin;|\x{2208}|g;
	$parm =~ s|\&Iuml;|\x{00CF}|g;			
	$parm =~ s|\&iuml;|\x{00EF}|g;			
	$parm =~ s|\&Kappa;|\x{039A}|g;		
	$parm =~ s|\&kappa;|\x{03BA}|g;
	$parm =~ s|\&kappav;|\x{03F0}|g;
	$parm =~ s|\&lAarr;|\x{21DA}|g;
	$parm =~ s|\&lagran;|\x{2112}|g;
	$parm =~ s|\&Lambda;|\x{039B}|g;		
	$parm =~ s|\&lambda;|\x{03BB}|g;
	$parm =~ s|\&lang;|\x{2329}|g;
	$parm =~ s|\&lap;|\x{2272}|g;
	$parm =~ s|\&laquo;|\x{00AB}|g;		
	$parm =~ s|\&larr;|\x{2190}|g;			
	$parm =~ s|\&Larr;|\x{219E}|g;
	$parm =~ s|\&lArr;|\x{21D0}|g;			
	$parm =~ s|\&lArr;|\x{21D0}|g;
	$parm =~ s|\&larr2;|\x{21C7}|g;
	$parm =~ s|\&larrhk;|\x{21A9}|g;
	$parm =~ s|\&larrlp;|\x{21AB}|g;
	$parm =~ s|\&larrtl;|\x{21A2}|g;
	$parm =~ s|\&lceil;|\x{2308}|g;
	$parm =~ s|\&ldot;|\x{22D6}|g;
	$parm =~ s|\&ldquo;|\x{201C}|g;		
	$parm =~ s|\&le;|\x{2264}|g;
	$parm =~ s|\&lE;|\x{2266}|g;
	$parm =~ s|\&leg;|\x{22DA}|g;
	$parm =~ s|\&lfloor;|\x{230A}|g;
	$parm =~ s|\&lg;|\x{2276}|g;
	$parm =~ s|\&lhard;|\x{21BD}|g;
	$parm =~ s|\&lharu;|\x{21BC}|g;
	$parm =~ s|\&lhblk;|\x{2584}|g;
	$parm =~ s|\&Ll;|\x{22D8}|g;
	$parm =~ s|\&lnE;|\x{2268}|g;
	$parm =~ s|\&lne;|\x{2268}|g;
	$parm =~ s|\&lnsim;|\x{22E6}|g;
	$parm =~ s|\&lowast;|\x{2217}|g;
	$parm =~ s|\&loz;|\x{25CA}|g;			
	$parm =~ s|\&lrarr2;|\x{21C6}|g;
	$parm =~ s|\&lrhar2;|\x{21CB}|g;
	$parm =~ s|\&lrm;|\x{200E}|g;			
	$parm =~ s|\&lsaquo;|\x{2039}|g;		# proposed
	$parm =~ s|\&lsh;|\x{21B0}|g;
	$parm =~ s|\&lsim;|\x{2272}|g;
	$parm =~ s|\&lsqb;|\x{005B}|g;
	$parm =~ s|\&lsquo;|\x{2018}|g;		
	$parm =~ s|\&lt;|\&lt;|g;				
	$parm =~ s|\&Lt;|\x{226A}|g;
	$parm =~ s|\&lthree;|\x{22CB}|g;
	$parm =~ s|\&ltimes;|\x{22C9}|g;
	$parm =~ s|\&ltrie;|\x{22B4}|g;
	$parm =~ s|\&ltrif;|\x{25C2}|g;
	$parm =~ s|\&lvnE;|\x{2268}|g;
	$parm =~ s|\&macr;|\x{00AF}|g;		
	$parm =~ s|\&male;|\x{2642}|g;
	$parm =~ s|\&malt;|\x{2720}|g;
	$parm =~ s|\&map;|\x{21A6}|g;
	$parm =~ s|\&marker;|\x{25AE}|g;
	$parm =~ s|\&mdash;|\x{2014}|g;		
	$parm =~ s|\&mdash;|\x{2014}|g;
	$parm =~ s|\&micro;|\x{00B5}|g;		
	$parm =~ s|\&middot;|\x{00B7}|g;		
	$parm =~ s|\&minus;|\x{2212}|g;
	$parm =~ s|\&minusb;|\x{229F}|g;
	$parm =~ s|\&mnplus;|\x{2213}|g;
	$parm =~ s|\&models;|\x{22A7}|g;
	$parm =~ s|\&Mu;|\x{039C}|g;			
	$parm =~ s|\&mu;|\x{03BC}|g;
	$parm =~ s|\&mumap;|\x{22B8}|g;
	$parm =~ s|\&nabla;|\x{2207}|g;
	$parm =~ s|\&nap;|\x{2249}|g;
	$parm =~ s|\&nbsp;|\x{00A0}|g;		
	$parm =~ s|\&ncong;|\x{2247}|g;
	$parm =~ s|\&ndash;|\x{2013}|g;
	$parm =~ s|\&ne;|\x{2260}|g;
	$parm =~ s|\&nearr;|\x{2197}|g;
	$parm =~ s|\&nequiv;|\x{2262}|g;
	$parm =~ s|\&nexist;|\x{2204}|g;
	$parm =~ s|\&ngE;|\x{2271}|g;
	$parm =~ s|\&nge;|\x{2271}|g;
	$parm =~ s|\&nges;|\x{2271}|g;
	$parm =~ s|\&ngt;|\x{226F}|g;
	$parm =~ s|\&nharr;|\x{21AE}|g;
	$parm =~ s|\&nhArr;|\x{21CE}|g;
	$parm =~ s|\&ni;|\x{220B}|g;
	$parm =~ s|\&nlarr;|\x{219A}|g;
	$parm =~ s|\&nlArr;|\x{21CD}|g;
	$parm =~ s|\&nldr;|\x{2025}|g;
	$parm =~ s|\&nle;|\x{2270}|g;
	$parm =~ s|\&nlE;|\x{2270}|g;
	$parm =~ s|\&nles;|\x{2270}|g;
	$parm =~ s|\&nlt;|\x{226E}|g;
	$parm =~ s|\&nltri;|\x{22EA}|g;
	$parm =~ s|\&nltrie;|\x{22EC}|g;
	$parm =~ s|\&nmid;|\x{2224}|g;
	$parm =~ s|\&not;|\x{00AC}|g;			
	$parm =~ s|\&notin;|\x{2209}|g;
	$parm =~ s|\&npar;|\x{2226}|g;
	$parm =~ s|\&npr;|\x{2280}|g;
	$parm =~ s|\&npre;|\x{22E0}|g;
	$parm =~ s|\&nrarr;|\x{219B}|g;
	$parm =~ s|\&nrArr;|\x{21CF}|g;
	$parm =~ s|\&nrtri;|\x{22EB}|g;
	$parm =~ s|\&nrtrie;|\x{22ED}|g;
	$parm =~ s|\&nsc;|\x{2281}|g;
	$parm =~ s|\&nsce;|\x{22E1}|g;
	$parm =~ s|\&nsim;|\x{2241}|g;
	$parm =~ s|\&nsime;|\x{2244}|g;
	$parm =~ s|\&nsmid;|\x{2224}|g;
	$parm =~ s|\&nspar;|\x{2226}|g;
	$parm =~ s|\&nsub;|\x{2284}|g;
	$parm =~ s|\&nsube;|\x{2288}|g;
	$parm =~ s|\&nsup;|\x{2285}|g;
	$parm =~ s|\&nsupe;|\x{2289}|g;
	$parm =~ s|\&Ntilde;|\x{00D1}|g;		
	$parm =~ s|\&ntilde;|\x{00F1}|g;		
	$parm =~ s|\&Nu;|\x{039D}|g;			
	$parm =~ s|\&nu;|\x{03BD}|g;
	$parm =~ s|\&nvdash;|\x{22AC}|g;
	$parm =~ s|\&nvDash;|\x{22AD}|g;
	$parm =~ s|\&nVdash;|\x{22AE}|g;
	$parm =~ s|\&nVDash;|\x{22AF}|g;
	$parm =~ s|\&nwarr;|\x{2196}|g;
	$parm =~ s|\&Oacute;|\x{00D3}|g;		
	$parm =~ s|\&oacute;|\x{00F3}|g;		
	$parm =~ s|\&oast;|\x{229B}|g;
	$parm =~ s|\&ocir;|\x{229A}|g;
	$parm =~ s|\&Ocirc;|\x{00D4}|g;		
	$parm =~ s|\&ocirc;|\x{00F4}|g;		
	$parm =~ s|\&odash;|\x{229D}|g;
	$parm =~ s|\&odot;|\x{2299}|g;
	$parm =~ s|\&OElig;|\x{0152}|g;		
	$parm =~ s|\&oelig;|\x{0153}|g;		
	$parm =~ s|\&Ograve;|\x{00D2}|g;		
	$parm =~ s|\&ograve;|\x{00F2}|g;		
	$parm =~ s|\&olarr;|\x{21BB}|g;
	$parm =~ s|\&oline;|\x{203E}|g;		
	$parm =~ s|\&Omega;|\x{03A9}|g;
	$parm =~ s|\&omega;|\x{03C9}|g;		
	$parm =~ s|\&Omicron;|\x{039F}|g;		
	$parm =~ s|\&omicron;|\x{03BF}|g;		
	$parm =~ s|\&ominus;|\x{2296}|g;
	$parm =~ s|\&oplus;|\x{2295}|g;
	$parm =~ s|\&or;|\x{2228}|g;
	$parm =~ s|\&orarr;|\x{21BA}|g;
	$parm =~ s|\&order;|\x{2134}|g;
	$parm =~ s|\&ordf;|\x{00AA}|g;			
	$parm =~ s|\&ordm;|\x{00BA}|g;		
	$parm =~ s|\&oS;|\x{24C8}|g;
	$parm =~ s|\&Oslash;|\x{00D8}|g;		
	$parm =~ s|\&oslash;|\x{00F8}|g;		
	$parm =~ s|\&osol;|\x{2298}|g;
	$parm =~ s|\&Otilde;|\x{00D5}|g;		
	$parm =~ s|\&otilde;|\x{00F5}|g;		
	$parm =~ s|\&otimes;|\x{2297}|g;
	$parm =~ s|\&Otimes;|\x{2A37}|g;
	$parm =~ s|\&Ouml;|\x{00D6}|g;
	$parm =~ s|\&ouml;|\x{00F6}|g;		
	$parm =~ s|\&par;|\x{2225}|g;
	$parm =~ s|\&para;|\x{00B6}|g;			
	$parm =~ s|\&part;|\x{2202}|g;
	$parm =~ s|\&permil;|\x{2030}|g;
	$parm =~ s|\&perp;|\x{22A5}|g;
	$parm =~ s|\&Phi;|\x{03A6}|g;
	$parm =~ s|\&phi;|\x{03C6}|g;			
	$parm =~ s|\&phis;|\x{03C6}|g;
	$parm =~ s|\&phisym;|\x{03D5}|g;
	$parm =~ s|\&phiv;|\x{03D5}|g;
	$parm =~ s|\&phmmat;|\x{2133}|g;
	$parm =~ s|\&phone;|\x{260E}|g;
	$parm =~ s|\&Pi;|\x{03A0}|g;			
	$parm =~ s|\&pi;|\x{03C0}|g;			
	$parm =~ s|\&piv;|\x{03D6}|g;
	$parm =~ s|\&planck;|\x{0127}|g;
	$parm =~ s|\&plusb;|\x{229E}|g;
	$parm =~ s|\&plusdo;|\x{2214}|g;
	$parm =~ s|\&plusmn;|\x{00B1}|g;		
	$parm =~ s|\&pound;|\x{00A3}|g;
	$parm =~ s|\&pr;|\x{227A}|g;
	$parm =~ s|\&prap;|\x{227E}|g;
	$parm =~ s|\&pre;|\x{227C}|g;
	$parm =~ s|\&prime;|\x{2032}|g;
	$parm =~ s|\&Prime;|\x{2033}|g;
	$parm =~ s|\&prnap;|\x{22E8}|g;
	$parm =~ s|\&prnsim;|\x{22E8}|g;
	$parm =~ s|\&prod;|\x{220F}|g;
	$parm =~ s|\&prop;|\x{221D}|g;
	$parm =~ s|\&prsim;|\x{227E}|g;
	$parm =~ s|\&Psi;|\x{03A8}|g;
	$parm =~ s|\&psi;|\x{03C8}|g;
	$parm =~ s|\&quot;|\x{0022}|g;		
	$parm =~ s|\&rAarr;|\x{21DB}|g;
	$parm =~ s|\&radic;|\x{221A}|g;
	$parm =~ s|\&rang;|\x{232A}|g;
	$parm =~ s|\&Rang;|\x{27EB}|g;
	$parm =~ s|\&raquo;|\x{00BB}|g;		
	$parm =~ s|\&rarr;|\x{2192}|g;			
	$parm =~ s|\&Rarr;|\x{21A0}|g;
	$parm =~ s|\&rArr;|\x{21D2}|g;
	$parm =~ s|\&rarr2;|\x{21C9}|g;
	$parm =~ s|\&rarrhk;|\x{21AA}|g;
	$parm =~ s|\&rarrlp;|\x{21AC}|g;
	$parm =~ s|\&rarrtl;|\x{21A3}|g;
	$parm =~ s|\&rarrw;|\x{21DD}|g;
	$parm =~ s|\&rceil;|\x{2309}|g;
	$parm =~ s|\&rdquo;|\x{201D}|g;		
	$parm =~ s|\&real;|\x{211C}|g;
	$parm =~ s|\&rect;|\x{25AD}|g;
	$parm =~ s|\&reg;|\x{00AE}|g;
	$parm =~ s|\&REG;|\x{00AE}|g;			# GenCode
	$parm =~ s|\&rfloor;|\x{230B}|g;
	$parm =~ s|\&rhard;|\x{21C1}|g;
	$parm =~ s|\&rharu;|\x{21C0}|g;
	$parm =~ s|\&Rho;|\x{03A1}|g;			
	$parm =~ s|\&rho;|\x{03C1}|g;
	$parm =~ s|\&rhov;|\x{03F1}|g;
	$parm =~ s|\&rlarr2;|\x{21C4}|g;
	$parm =~ s|\&rlhar2;|\x{21CC}|g;
	$parm =~ s|\&rlm;|\x{200F}|g;			
	$parm =~ s|\&rsaquo;|\x{203A}|g;		# proposed
	$parm =~ s|\&rsh;|\x{21B1}|g;
	$parm =~ s|\&rsqb;|\x{005D}|g;
	$parm =~ s|\&rsquo;|\x{2019}|g;		
	$parm =~ s|\&rthree;|\x{22CC}|g;
	$parm =~ s|\&rtimes;|\x{22CA}|g;
	$parm =~ s|\&rtrie;|\x{22B5}|g;
	$parm =~ s|\&rtrif;|\x{25B8}|g;
	$parm =~ s|\&samalg;|\x{2210}|g;
	$parm =~ s|\&sbquo;|\x{201A}|g;		
	$parm =~ s|\&sbsol;|\x{FE68}|g;
	$parm =~ s|\&sc;|\x{227B}|g;
	$parm =~ s|\&scap;|\x{227F}|g;
	$parm =~ s|\&Scaron;|\x{0160}|g;
	$parm =~ s|\&scaron;|\x{0161}|g;		
	$parm =~ s|\&sccue;|\x{227D}|g;
	$parm =~ s|\&sce;|\x{227D}|g;
	$parm =~ s|\&scnap;|\x{22E9}|g;
	$parm =~ s|\&scnsim;|\x{22E9}|g;
	$parm =~ s|\&scsim;|\x{227F}|g;
	$parm =~ s|\&sdot;|\x{22C5}|g;
	$parm =~ s|\&sdotb;|\x{22A1}|g;
	$parm =~ s|\&sect;|\x{00A7}|g;		
	$parm =~ s|\&setmn;|\x{2216}|g;
	$parm =~ s|\&sfrown;|\x{2322}|g;
	$parm =~ s|\&sharp;|\x{266F}|g;
	$parm =~ s|\&shy;|\x{00AD}|g;			
	$parm =~ s|\&Sigma;|\x{03A3}|g;
	$parm =~ s|\&sigma;|\x{03C3}|g;
	$parm =~ s|\&sigmaf;|\x{03C2}|g;		
	$parm =~ s|\&sigmav;|\x{03C2}|g;
	$parm =~ s|\&sim;|\x{223C}|g;
	$parm =~ s|\&sime;|\x{2243}|g;
	$parm =~ s|\&smile;|\x{2323}|g;
	$parm =~ s|\&spades;|\x{2660}|g;
	$parm =~ s|\&spar;|\x{2225}|g;
	$parm =~ s|\&sqcap;|\x{2293}|g;
	$parm =~ s|\&sqcup;|\x{2294}|g;
	$parm =~ s|\&sqsub;|\x{228F}|g;
	$parm =~ s|\&sqsube;|\x{2291}|g;
	$parm =~ s|\&sqsup;|\x{2290}|g;
	$parm =~ s|\&sqsupe;|\x{2292}|g;
	$parm =~ s|\&squ;|\x{25A1};|g;		# White Square (hollow)
	$parm =~ s|\&square;|\x{25A1}|g;
	$parm =~ s|\&squf;|\x{25A0}|g;			# Black Square (filled)
	$parm =~ s|\&squK;|\x{25A0}|g;		# Black Square (filled) 2016-03-01
	$parm =~ s|\&squLtK;|\x{25E7}|g;		# Square Left Black (2016-03-01)
	$parm =~ s|\&squRtK;|\x{25E8}|g;		# Square Left Black (2016-03-01)
	$parm =~ s|\&squW;|\x{25A1}|g;		# White Square (filled) 2016-03-01
	$parm =~ s|\&ssetmn;|\x{2216}|g;
	$parm =~ s|\&ssmile;|\x{2323}|g;
	$parm =~ s|\&ssquf;|\x{25AA}|g;
	$parm =~ s|\&sstarf;|\x{22C6}|g;
	$parm =~ s|\&star;|\x{2606}|g;
	$parm =~ s|\&starf;|\x{2605}|g;
	$parm =~ s|\&sub;|\x{2282}|g;			
	$parm =~ s|\&sub;|\x{2282}|g;
	$parm =~ s|\&Sub;|\x{22D0}|g;
	$parm =~ s|\&sube;|\x{2286}|g;
	$parm =~ s|\&subE;|\x{2AC5}|g;
	$parm =~ s|\&subne;|\x{228A}|g;
	$parm =~ s|\&sum;|\x{2211}|g;			
	$parm =~ s|\&sup;|\x{2283}|g;
	$parm =~ s|\&Sup;|\x{22D1}|g;
	$parm =~ s|\&sup1;|\x{00B9}|g;		
	$parm =~ s|\&sup2;|\x{00B2}|g;		
	$parm =~ s|\&sup3;|\x{00B3}|g;		
	$parm =~ s|\&supe;|\x{2287}|g;
	$parm =~ s|\&supE;|\x{2AC6}|g;
	$parm =~ s|\&supne;|\x{228B}|g;
	$parm =~ s|\&szlig;|\x{00DF}|g;			
	$parm =~ s|\&Tau;|\x{03A4}|g;			
	$parm =~ s|\&tau;|\x{03C4}|g;
	$parm =~ s|\&tdot;|\x{20DB}|g;
	$parm =~ s|\&telrec;|\x{2315}|g;
	$parm =~ s|\&there4;|\x{2234}|g;
	$parm =~ s|\&Theta;|\x{0398}|g;
	$parm =~ s|\&theta;|\x{03B8}|g;		
	$parm =~ s|\&thetas;|\x{03B8}|g;
	$parm =~ s|\&thetasym;|\x{03D1}|g;
	$parm =~ s|\&thetav;|\x{03D1}|g;
	$parm =~ s|\&thinsp;|\x{2009}|g;		
	$parm =~ s|\&THORN;|\x{00DE}|g;		
	$parm =~ s|\&thorn;|\x{00FE}|g;		
	$parm =~ s|\&tilde;|\x{02DC}|g;		
	$parm =~ s|\&times;|\x{00D7}|g;		
	$parm =~ s|\&timesb;|\x{22A0}|g;
	$parm =~ s|\&top;|\x{22A4}|g;
	$parm =~ s|\&tprime;|\x{2034}|g;
	$parm =~ s|\&trade;|\x{2122}|g;		
	$parm =~ s|\&TRADE;|\x{2122}|g;
	$parm =~ s|\&triDnK;|\x{25BC}|g;
	$parm =~ s|\&triDnW;|\x{25BD}|g;
	$parm =~ s|\&trie;|\x{225C}|g;
	$parm =~ s|\&triLtK;|\x{25C0}|g;
	$parm =~ s|\&triLtW;|\x{25C1}|g;
	$parm =~ s|\&triRtK;|\x{25B6}|g;
	$parm =~ s|\&triRtW;|\x{25B7}|g;
	$parm =~ s|\&triUpK;|\x{25B2}|g;
	$parm =~ s|\&triUpW;|\x{25B3}|g;
	$parm =~ s|\&twixt;|\x{226C}|g;
	$parm =~ s|\&Uacute;|\x{00DA}|g;		
	$parm =~ s|\&uacute;|\x{00FA}|g;		
	$parm =~ s|\&uarr;|\x{2191}|g;			
	$parm =~ s|\&uArr;|\x{21D1}|g;
	$parm =~ s|\&uarr2;|\x{21C8}|g;
	$parm =~ s|\&Ucirc;|\x{00DB}|g;		
	$parm =~ s|\&ucirc;|\x{00FB}|g;		
	$parm =~ s|\&Ugrave;|\x{00D9}|g;		
	$parm =~ s|\&ugrave;|\x{00F9}|g;		
	$parm =~ s|\&uharl;|\x{21BF}|g;
	$parm =~ s|\&uharr;|\x{21BE}|g;
	$parm =~ s|\&uhblk;|\x{2580}|g;
	$parm =~ s|\&ulcorn;|\x{231C}|g;
	$parm =~ s|\&uml;|\x{00A8}|g;			
	$parm =~ s|\&uplus;|\x{228E}|g;
	$parm =~ s|\&upsi;|\x{03C5}|g;
	$parm =~ s|\&Upsi;|\x{03D2}|g;
	$parm =~ s|\&upsih;|\x{03D2}|g;		
	$parm =~ s|\&Upsilon;|\x{03A5}|g;		
	$parm =~ s|\&upsilon;|\x{03C5}|g;		
	$parm =~ s|\&urcorn;|\x{231D}|g;
	$parm =~ s|\&utri;|\x{25B5}|g;
	$parm =~ s|\&utrif;|\x{25B4}|g;
	$parm =~ s|\&Uuml;|\x{00DC}|g;		
	$parm =~ s|\&uuml;|\x{00FC}|g;		
	$parm =~ s|\&varr;|\x{2195}|g;
	$parm =~ s|\&vArr;|\x{21D5}|g;
	$parm =~ s|\&vdash;|\x{22A2}|g;
	$parm =~ s|\&Vdash;|\x{22A9}|g;
	$parm =~ s|\&vDash;|\x{255E}|g;
	$parm =~ s|\&veebar;|\x{22BB}|g;
	$parm =~ s|\&verbar;|\x{007C}|g;
	$parm =~ s|\&Verbar;|\x{2016}|g;
	$parm =~ s|\&verbar;|\x{2223}|g;
	$parm =~ s|\&vltri;|\x{22B2}|g;
	$parm =~ s|\&vprime;|\x{2032}|g;
	$parm =~ s|\&vprop;|\x{221D}|g;
	$parm =~ s|\&vrtri;|\x{22B3}|g;
	$parm =~ s|\&vsubne;|\x{228A}|g;
	$parm =~ s|\&vsupne;|\x{228B}|g;
	$parm =~ s|\&Vvdash;|\x{22AA}|g;
	$parm =~ s|\&wedgeq;|\x{2259}|g;
	$parm =~ s|\&weierp;|\x{2118}|g;
	$parm =~ s|\&wreath;|\x{2240}|g;
	$parm =~ s|\&xcirc;|\x{25EF}|g;
	$parm =~ s|\&xharr;|\x{2194}|g;
	$parm =~ s|\&Xi;|\x{039E}|g;
	$parm =~ s|\&xi;|\x{03BE}|g;
	$parm =~ s|\&xlArr;|\x{21D0}|g;
	$parm =~ s|\&xrArr;|\x{21D2}|g;
	$parm =~ s|\&xutri;|\x{25B3}|g;
	$parm =~ s|\&Yacute;|\x{00DD}|g;		
	$parm =~ s|\&yacute;|\x{00FD}|g;		
	$parm =~ s|\&yen;|\x{00A5}|g;			
	$parm =~ s|\&yuml;|\x{00FF}|g;		
	$parm =~ s|\&Yuml;|\x{0178}|g;		
	$parm =~ s|\&Zcaron;|\x{017D}|g;
	$parm =~ s|\&zcaron;|\x{017E}|g;
	$parm =~ s|\&Zeta;|\x{0396}|g;		
	$parm =~ s|\&zeta;|\x{03B6}|g;
	$parm =~ s|\&zwj;|\x{200D}|g;	
	$parm =~ s|\&zwnj;|\x{200C}|g;			

# -------------------------
# legacy
#--------------------------
#	#See: V:\SCHEMA\xhtml-*.ent files
#	$parm =~ s|\&acute;|\x{00B4}|g;		#
#	$parm =~ s|\&aacute;|\x{00E1}|g;		#
#	$parm =~ s|\&Aacute;|\x{00C1}|g;		#
#	$parm =~ s|\&acirc;|\x{00E2}|g;		#
#	$parm =~ s|\&Acirc;|\x{00C2}|g;		#	
#	$parm =~ s|\&aelig;|\x{00E6}|g;		#
#	$parm =~ s|\&AElig;|\x{00C6}|g;		#
#	$parm =~ s|\&agrave;|\x{00E0}|g;		#
#	$parm =~ s|\&Agrave;|\x{00C0}|g;		#
#	$parm =~ s|\&alef;|\x{05D0}|g;			#
#	$parm =~ s|\&alefsym;|\x{2135}|g;		#
#	$parm =~ s|\&alpha;|\x{03B1}|g;		#
#	$parm =~ s|\&Alpha;|\x{0391}|g;		#
#	$parm =~ s|\&and;|\x{2227}|g;			#
#	$parm =~ s|\&ang;|\x{2220}|g;			#
#	$parm =~ s|\&apos;|\x{0027}|g;		#
#	$parm =~ s|\&aring;|\x{00E5}|g;		#
#	$parm =~ s|\&Aring;|\x{00C5}|g;		#
#	$parm =~ s|\&asymp;|\x{2248}|g;		#
#	$parm =~ s|\&atilde;|\x{00E3}|g;		#
#	$parm =~ s|\&Atilde;|\x{00C3}|g;		#
#	$parm =~ s|\&auml;|\x{00E4}|g;		#
#	$parm =~ s|\&Auml;|\x{00C4}|g;		#
#	
#	$parm =~ s|\&beta;|\x{03B2}|g;		#
#	$parm =~ s|\&Beta;|\x{0392}|g;		#
#	$parm =~ s|\&bdquo;|\x{201E}|g;		#
#	$parm =~ s|\&brvbar;|\x{00A6}|g;		#
#	$parm =~ s|\&bull;|\x{2022}|g;			#
#	$parm =~ s|\&bullet;|\x{2219}|g;		#
#	
#	$parm =~ s|\&cap;|\x{2229}|g;			#
#	$parm =~ s|\&check;|\&#x2713;|g;
#	$parm =~ s|\&cedil;|\x{00B8}|g;		#
#	$parm =~ s|\&ccedil;|\x{00E7}|g;		#
#	$parm =~ s|\&Ccedil;|\x{00C7}|g;		#
#	$parm =~ s|\&cent;|\x{00A2}|g;		#
#	$parm =~ s|\&chi;|\x{03C7}|g;			#
#	$parm =~ s|\&Chi;|\x{03A7}|g;			#
#	$parm =~ s|\&circ;|\x{02C6}|g;			#
#	$parm =~ s|\&clubs;|\x{02663}|g;		#
#	$parm =~ s|\&cong;|\x{2245}|g;		#
#	$parm =~ s|\&copy;|\x{00A9}|g;		#
#	$parm =~ s|\&crarr;|\x{21B5}|g;		#
#	$parm =~ s|\&cup;|\x{222A}|g;			#
#	$parm =~ s|\&curren;|\x{00A4}|g;		#
#	$parm =~ s|\&commat;|\x{0040}|g;
#	$parm =~ s|\&copy;|\x{00A9}|g;
#	
#	$parm =~ s|\&dagger;|\x{2020}|g;		#
#	$parm =~ s|\&Dagger;|\x{2021}|g;		#
#	$parm =~ s|\&darr;|\x{2193}|g;			#
#	$parm =~ s|\&dArr;|\x{21D3}|g;			#
#	$parm =~ s|\&deg;|\x{00B0}|g;			#
#	$parm =~ s|\&delta;|\x{03B4}|g;		#
#	$parm =~ s|\&Delta;|\x{0394}|g;
#	$parm =~ s|\&diam;|\x{022C4}|g;		# diamond no fill
#	$parm =~ s|\&diamond;|\x{022C4}|g;	# diamond no fill
#	$parm =~ s|\&diamf;|\x{2666}|g;		# diamond solid
#	$parm =~ s|\&diams;|\x{2666}|g;		# diamond solid
#	$parm =~ s|\&divide;|\x{00F7}|g;		#
#	$parm =~ s|\&dlcorn;|\x{231E}|g;
#	$parm =~ s|\&drcorn;|\x{231F}|g;
#	$parm =~ s|\&dtrif;|\x{25BE}|g;
#	
#	$parm =~ s|\&eacute;|\x{00E9}|g;		#
#	$parm =~ s|\&Eacute;|\x{00C9}|g;		#
#	$parm =~ s|\&ecirc;|\x{00EA}|g;		#
#	$parm =~ s|\&Ecirc;|\x{00CA}|g;		#
#	$parm =~ s|\&egrave;|\x{00E8}|g;		#
#	$parm =~ s|\&Egrave;|\x{00C8}|g;		#
#	$parm =~ s|\&empty;|\x{2205}|g;		#
#	$parm =~ s|\&emsp;|\x{2003}|g;		#
#	$parm =~ s|\&ensp;|\x{2002}|g;		#
#	$parm =~ s|\&epsilon;|\x{03B5}|g;		#
#	$parm =~ s|\&Epsilon;|\x{0395}|g;		#
#	$parm =~ s|\&equiv;|\x{2261}|g;		#
#	$parm =~ s|\&eta;|\x{03B7}|g;			#
#	$parm =~ s|\&Eta;|\x{0397}|g;			#
#	$parm =~ s|\&eth;|\x{00F0}|g;			#
#	$parm =~ s|\&ETH;|\x{00D0}|g;			#
#	$parm =~ s|\&euml;|\x{00EB}|g;		#
#	$parm =~ s|\&Euml;|\x{00CB}|g;		#
#	$parm =~ s|\&euro;|\x{20AC}|g;		#
#	$parm =~ s|\&exist;|\x{2203}|g;		#
#	
#	$parm =~ s|\&fnof;|\x{0192}|g;			#
#	$parm =~ s|\&forall;|\x{2200}|g;		#
#	$parm =~ s|\&frac12;|\x{00BD}|g;		#
#	$parm =~ s|\&frac14;|\x{00BC}|g;		#
#	$parm =~ s|\&frac34;|\x{00BE}|g;
#	$parm =~ s|\&frasl;|\x{2044}|g;			#
#	
#	$parm =~ s|\&gamma;|\x{03B3}|g;		#
#	$parm =~ s|\&Gamma;|\x{0393}|g;
#	$parm =~ s|\&ge;|\x{2265}|g;			#
#	$parm =~ s|\&gt;|\x{003E}|g;			#
#	
#	$parm =~ s|\&half;|\x{00BD}|g;
#	$parm =~ s|\&harr;|\x{2194}|g;			#
#	$parm =~ s|\&hArr;|\x{21D4}|g;			#
#	$parm =~ s|\&hearts;|\x{2665}|g;		#
#	$parm =~ s|\&hellip;|\x{2026}|g;		#
#	
#	$parm =~ s|\&iacute;|\x{00ED}|g;		#
#	$parm =~ s|\&Iacute;|\x{00CD}|g;		#
#	$parm =~ s|\&icirc;|\x{00EE}|g;			#
#	$parm =~ s|\&Icirc;|\x{00CE}|g;		#
#	$parm =~ s|\&iexcl;|\x{00A1}|g;		#
#	$parm =~ s|\&igrave;|\x{00EC}|g;		#
#	$parm =~ s|\&Igrave;|\x{00CC}|g;		#
#	$parm =~ s|\&image;|\x{2111}|g;		#
#	$parm =~ s|\&infin;|\x{221E}|g;			#
#	$parm =~ s|\&int;|\x{222B}|g;			#
#	$parm =~ s|\&iota;|\x{03B9}|g;			#
#	$parm =~ s|\&Iota;|\x{0399}|g;			#
#	$parm =~ s|\&iquest;|\x{00BF}|g;		#
#	$parm =~ s|\&isin;|\x{2208}|g;			#
#	$parm =~ s|\&iuml;|\x{00EF}|g;			#
#	$parm =~ s|\&Iuml;|\x{00CF}|g;			#
#	
#	$parm =~ s|\&kappa;|\x{03BA}|g;		#
#	$parm =~ s|\&Kappa;|\x{039A}|g;		#
#	
#	$parm =~ s|\&lambda;|\x{03BB}|g;		#	
#	$parm =~ s|\&Lambda;|\x{039B}|g;		#
#	$parm =~ s|\&lang;|\x{2329}|g;			#
#	$parm =~ s|\&laquo;|\x{00AB}|g;		#
#	$parm =~ s|\&larr;|\x{2190}|g;			#
#	$parm =~ s|\&lArr;|\x{21D0}|g;			#
#	$parm =~ s|\&lceil;|\x{2308}|g;			#
#	$parm =~ s|\&ldquo;|\x{201C}|g;		#
#	$parm =~ s|\&le;|\x{2264}|g;			#
#	$parm =~ s|\&lfloor;|\x{230A}|g;		#
#	$parm =~ s|\&lowast;|\x{2217}|g;		#
#	$parm =~ s|\&loz;|\x{25CA}|g;			#
##	$parm =~ s|\&lstroke;|\x{}|g;
##	$parm =~ s|\&Lstroke;|\x{}|g;
#	$parm =~ s|\&lsqb;|\x{005B}|g;
#	
##	$parm =~ s|\&lt;|\x{003C}|g;			#
#	$parm =~ s|\&lt;|\&lt;|g;				#
#	
##	if($flag > 0) {
##		print STDOUT "\t\$parm(AFTER)=$parm\n";
##		print STDOUT "(Press any key to continue...\n)";
##		$flag = 0;
##		$key = ReadKey;
##	}
#	
#	$parm =~ s|\&lrm;|\x{200E}|g;			#
#	$parm =~ s|\&lsaquo;|\x{2039}|g;		# proposed
#	$parm =~ s|\&lsquo;|\x{2018}|g;		#
#	
#	$parm =~ s|\&macr;|\x{00AF}|g;		#
#	$parm =~ s|\&mdash;|\x{2014}|g;		#
#	$parm =~ s|\&micro;|\x{00B5}|g;		#
#	$parm =~ s|\&middot;|\x{00B7}|g;		#
#	$parm =~ s|\&minus;|\x{2212}|g;		#
#	$parm =~ s|\&mu;|\x{03BC}|g;			#
#	$parm =~ s|\&Mu;|\x{039C}|g;			#
#	
#	$parm =~ s|\&nabla;|\x{2207}|g;		#
#	$parm =~ s|\&nbsp;|\x{00A0}|g;		#
##	$parm =~ s|\&nbsp;|\&nbsp;|g;			# ?????
#	$parm =~ s|\&ndash;|\x{2013}|g;		#
#	$parm =~ s|\&ne;|\x{2260}|g;			#
#	$parm =~ s|\&ni;|\x{220B}|g;			#
#	$parm =~ s|\&not;|\x{00AC}|g;			#
#	$parm =~ s|\&notin;|\x{2209}|g;		#
#	$parm =~ s|\&nsub;|\x{2284}|g;		#
#	$parm =~ s|\&ntilde;|\x{00F1}|g;		#
#	$parm =~ s|\&Ntilde;|\x{00D1}|g;		#
#	$parm =~ s|\&nu;|\x{03BD}|g;			#
#	$parm =~ s|\&Nu;|\x{039D}|g;			#
#	
#	$parm =~ s|\&oacute;|\x{00F3}|g;		#
#	$parm =~ s|\&Oacute;|\x{00D3}|g;		#
#	$parm =~ s|\&ocirc;|\x{00F4}|g;		#
#	$parm =~ s|\&Ocirc;|\x{00D4}|g;		#
#	$parm =~ s|\&OElig;|\x{0152}|g;		#
#	$parm =~ s|\&oelig;|\x{0153}|g;		#
#	$parm =~ s|\&ograve;|\x{00F2}|g;		#
#	$parm =~ s|\&Ograve;|\x{00D2}|g;		#
#	$parm =~ s|\&oline;|\x{203E}|g;		#
#	$parm =~ s|\&omega;|\x{03C9}|g;		#
#	$parm =~ s|\&Omega;|\x{03A9}|g;		#
#	$parm =~ s|\&omicron;|\x{03BF}|g;		#
#	$parm =~ s|\&Omicron;|\x{039F}|g;		#
#	$parm =~ s|\&oplus;|\x{2295}|g;		#
#	$parm =~ s|\&or;|\x{2228}|g;			#
#	$parm =~ s|\&ordf;|\x{00AA}|g;			#
#	$parm =~ s|\&ordm;|\x{00BA}|g;		#
#	$parm =~ s|\&oslash;|\x{00F8}|g;		#
#	$parm =~ s|\&Oslash;|\x{00D8}|g;		#
#	$parm =~ s|\&otilde;|\x{00F5}|g;		#
#	$parm =~ s|\&Otilde;|\x{00D5}|g;		#
#	$parm =~ s|\&otimes;|\x{2297}|g;		#
#	$parm =~ s|\&Otimes;|\x{2A37}|g;
#	$parm =~ s|\&ouml;|\x{00F6}|g;		#
#	$parm =~ s|\&Ouml;|\x{00D6}|g;		#		
#	
#	$parm =~ s|\&para;|\x{00B6}|g;			#
#	$parm =~ s|\&part;|\x{2202}|g;			#
#	$parm =~ s|\&permil;|\x{2030}|g;		#
#	$parm =~ s|\&perp;|\x{22A5}|g;		#	
#	$parm =~ s|\&phi;|\x{03C6}|g;			#
#	$parm =~ s|\&Phi;|\x{03A6}|g;			#
#	$parm =~ s|\&phisym;|\x{03D5}|g;		# GenCode
#	$parm =~ s|\&pi;|\x{03C0}|g;			#
#	$parm =~ s|\&Pi;|\x{03A0}|g;			#
#	$parm =~ s|\&piv;|\x{03D6}|g;			#
#	$parm =~ s|\&plusmn;|\x{00B1}|g;		#
#	$parm =~ s|\&pound;|\x{00A3}|g;		#
#	$parm =~ s|\&prime;|\x{2032}|g;		#
#	$parm =~ s|\&Prime;|\x{2033}|g;		#
#	$parm =~ s|\&prod;|\x{220F}|g;			#
#	$parm =~ s|\&prop;|\x{221D}|g;		#
#	$parm =~ s|\&psi;|\x{03C8}|g;			#
#	$parm =~ s|\&Psi;|\x{03A8}|g;			#
#	$parm =~ s|\&pound;|\x{00A3}|g;	
#
#	$parm =~ s|\&quot;|\x{0022}|g;		#
#	
#	$parm =~ s|\&radic;|\x{221A}|g;		#
#	$parm =~ s|\&rang;|\x{232A}|g;		#
##	$parm =~ s|\&rang;|\x{3009}|g;			# GenCode ???
#	$parm =~ s|\&Rang;|\x{27EB}|g;
#	$parm =~ s|\&raquo;|\x{00BB}|g;		#
#	$parm =~ s|\&rarr;|\x{2192}|g;			#
#	$parm =~ s|\&rArr;|\x{21D2}|g;			#
#	$parm =~ s|\&rceil;|\x{2309}|g;			#
#	$parm =~ s|\&real;|\x{211C}|g;			#
#	$parm =~ s|\&reg;|\x{00AE}|g;			#
#	$parm =~ s|\&REG;|\x{00AE}|g;			# GenCode
#	$parm =~ s|\&rdquo;|\x{201D}|g;		#
#	$parm =~ s|\&rfloor;|\x{230B}|g;		#
#	$parm =~ s|\&rho;|\x{03C1}|g;			#
#	$parm =~ s|\&Rho;|\x{03A1}|g;			#
#	$parm =~ s|\&rlm;|\x{200F}|g;			#
#	$parm =~ s|\&rsqb;|\x{005D}|g;
#	$parm =~ s|\&rsaquo;|\x{203A}|g;		# proposed
#	$parm =~ s|\&rsquo;|\x{2019}|g;		#
#	
#	$parm =~ s|\&sbquo;|\x{201A}|g;		#
#	$parm =~ s|\&Scaron;|\x{0160}|g;		#	
#	$parm =~ s|\&scaron;|\x{0161}|g;		#
#	$parm =~ s|\&sdot;|\x{22C5}|g;		#
#	$parm =~ s|\&sect;|\x{00A7}|g;		#
#	$parm =~ s|\&shy;|\x{00AD}|g;			#
#	$parm =~ s|\&sigma;|\x{03C3}|g;		#
#	$parm =~ s|\&Sigma;|\x{03A3}|g;		#
#	$parm =~ s|\&sigmaf;|\x{03C2}|g;		#
#	$parm =~ s|\&sim;|\x{223C}|g;			#
#	$parm =~ s|\&spades;|\x{2660}|g;		#
#	$parm =~ s|\&squ;|\&#x25A1;|g;		# White Square (hollow)
#	$parm =~ s|\&squf;|\x{25A0}|g;			# Black Square (filled)
#	$parm =~ s|\&squK;|\x{25A0}|g;		# Black Square (filled) 2016-03-01
#	$parm =~ s|\&squW;|\x{25A1}|g;		# White Square (filled) 2016-03-01
#	$parm =~ s|\&squLtK;|\x{25E7}|g;		# Square Left Black (2016-03-01)
#	$parm =~ s|\&squRtK;|\x{25E8}|g;		# Square Left Black (2016-03-01)
#	$parm =~ s|\&sub;|\x{2282}|g;			#
#	$parm =~ s|\&Sub;|\x{22D0}|g;
#	$parm =~ s|\&sube;|\x{2286}|g;		#
#	$parm =~ s|\&subE;|\x{2AC5}|g;
#	$parm =~ s|\&sum;|\x{2211}|g;			#
#	$parm =~ s|\&sup;|\x{2283}|g;			#
#	$parm =~ s|\&Sup;|\x{22D1}|g;
#	$parm =~ s|\&sup1;|\x{00B9}|g;		#
#	$parm =~ s|\&sup2;|\x{00B2}|g;		#
#	$parm =~ s|\&sup3;|\x{00B3}|g;		#
#	$parm =~ s|\&supe;|\x{2287}|g;		#
#	$parm =~ s|\&supE;|\x{2AC6}|g;
#	$parm =~ s|\&szlig;|\x{00DF}|g;			#
#	
#	$parm =~ s|\&tau;|\x{03C4}|g;			#
#	$parm =~ s|\&Tau;|\x{03A4}|g;			#
#	$parm =~ s|\&there4;|\x{2234}|g;		#
#	$parm =~ s|\&theta;|\x{03B8}|g;		#
#	$parm =~ s|\&Theta;|\x{0398}|g;		#
#	$parm =~ s|\&thetasym;|\x{03D1}|g;	#
#	$parm =~ s|\&thinsp;|\x{2009}|g;		#
#	$parm =~ s|\&thorn;|\x{00FE}|g;		#
#	$parm =~ s|\&THORN;|\x{00DE}|g;		#
#	$parm =~ s|\&tilde;|\x{02DC}|g;		#
#	$parm =~ s|\&times;|\x{00D7}|g;		#
#	$parm =~ s|;times;|\x{00D7}|g;			# GenCode
#	$parm =~ s|\&trade;|\x{2122}|g;		#
#	$parm =~ s|\&TRADE;|\x{2122}|g;
#	
#	$parm =~ s|\&uacute;|\x{00FA}|g;		#
#	$parm =~ s|\&Uacute;|\x{00DA}|g;		#
#	$parm =~ s|\&uarr;|\x{2191}|g;			#
#	$parm =~ s|\&uArr;|\x{21D1}|g;			#
#	$parm =~ s|\&ucirc;|\x{00FB}|g;		#
#	$parm =~ s|\&Ucirc;|\x{00DB}|g;		#
#	$parm =~ s|\&ugrave;|\x{00F9}|g;		#
#	$parm =~ s|\&Ugrave;|\x{00D9}|g;		#
#	$parm =~ s|\&ulcorn;|\x{231C}|g;
#	$parm =~ s|\&upsih;|\x{03D2}|g;		#
#	$parm =~ s|\&upsilon;|\x{03C5}|g;		#
#	$parm =~ s|\&Upsilon;|\x{03A5}|g;		#
#	$parm =~ s|\&urcorn;|\x{231D}|g;
#	$parm =~ s|\&utrif;|\x{25B4}|g;
#	$parm =~ s|\&uml;|\x{00A8}|g;			#
#	$parm =~ s|\&uuml;|\x{00FC}|g;		#
#	$parm =~ s|\&Uuml;|\x{00DC}|g;		#
#	
#	$parm =~ s|\&verbar;|\x{007C}|g;
#	$parm =~ s|\&Verbar;|\x{2016}|g;
#	
#	$parm =~ s|\&weierp;|\x{2118}|g;		#
#	
#	$parm =~ s|\&xi;|\x{03BE}|g;			#
#	$parm =~ s|\&Xi;|\x{039E}|g;			#
#	
#	$parm =~ s|\&yacute;|\x{00FD}|g;		#
#	$parm =~ s|\&Yacute;|\x{00DD}|g;		#
#	$parm =~ s|\&yen;|\x{00A5}|g;			#
#	$parm =~ s|\&Yuml;|\x{0178}|g;		#
#	$parm =~ s|\&yuml;|\x{00FF}|g;		#
#	
#	$parm =~ s|\&zcaron;|\x{017E}|g;
#	$parm =~ s|\&Zcaron;|\x{017D}|g;
#	$parm =~ s|\&zeta;|\x{03B6}|g;			#
#	$parm =~ s|\&Zeta;|\x{0396}|g;		#
#	$parm =~ s|\&zwj;|\x{200D}|g;			#
#	$parm =~ s|&zwnj;|\x{200C}|g;			#
#	
#	# integers 1-20 inside a circle:
#	$parm =~ s|\&circle0;|\x{24EA}|g;	
#	$parm =~ s|\&circle1;|\x{2460}|g;	
#	$parm =~ s|\&circle2;|\x{2461}|g;	
#	$parm =~ s|\&circle3;|\x{2462}|g;	
#	$parm =~ s|\&circle4;|\x{2463}|g;	
#	$parm =~ s|\&circle5;|\x{2464}|g;	
#	$parm =~ s|\&circle6;|\x{2465}|g;	
#	$parm =~ s|\&circle7;|\x{2466}|g;	
#	$parm =~ s|\&circle8;|\x{2467}|g;	
#	$parm =~ s|\&circle9;|\x{2468}|g;	
#	$parm =~ s|\&circle10;|\x{2469}|g;	
#	$parm =~ s|\&circle11;|\x{246A}|g;	
#	$parm =~ s|\&circle12;|\x{246B}|g;	
#	$parm =~ s|\&circle13;|\x{246C}|g;	
#	$parm =~ s|\&circle14;|\x{246D}|g;	
#	$parm =~ s|\&circle15;|\x{246E}|g;	
#	$parm =~ s|\&circle16;|\x{246F}|g;	
#	$parm =~ s|\&circle17;|\x{2470}|g;	
#	$parm =~ s|\&circle18;|\x{2471}|g;	
#	$parm =~ s|\&circle19;|\x{2472}|g;	
#	$parm =~ s|\&circle20;|\x{2473}|g;
#	
#	# enclosed alpha CAPITAL:	
#	$parm =~ s|\&circleA;|\x{24B6}|g;
#	$parm =~ s|\&circleB;|\x{24B7}|g;
#	$parm =~ s|\&circleC;|\x{24B8}|g;
#	$parm =~ s|\&circleD;|\x{24B9}|g;
#	$parm =~ s|\&circleE;|\x{24BA}|g;
#	$parm =~ s|\&circleF;|\x{24BB}|g;
#	$parm =~ s|\&circleG;|\x{24BC}|g;
#	$parm =~ s|\&circleH;|\x{24BD}|g;
#	$parm =~ s|\&circleI;|\x{24BE}|g;
#	$parm =~ s|\&circleJ;|\x{24BF}|g;
#	$parm =~ s|\&circleK;|\x{24C0}|g;
#	$parm =~ s|\&circleL;|\x{24C1}|g;
#	$parm =~ s|\&circleM;|\x{24C2}|g;
#	$parm =~ s|\&circleN;|\x{24C3}|g;
#	$parm =~ s|\&circleO;|\x{24C4}|g;
#	$parm =~ s|\&circleP;|\x{24C5}|g;
#	$parm =~ s|\&circleQ;|\x{24C6}|g;
#	$parm =~ s|\&circleR;|\x{24C7}|g;
#	$parm =~ s|\&circleS;|\x{24C8}|g;
#	$parm =~ s|\&circleT;|\x{24C9}|g;
#	$parm =~ s|\&circleU;|\x{24CA}|g;
#	$parm =~ s|\&circleV;|\x{24CB}|g;
#	$parm =~ s|\&circleW;|\x{24CC}|g;
#	$parm =~ s|\&circleX;|\x{24CD}|g;
#	$parm =~ s|\&circleY;|\x{24CE}|g;
#	$parm =~ s|\&circleZ;|\x{24CF}|g;
#	
#	# enclosed alpha lowercase:
#	$parm =~ s|\&circlea;|\x{24D0}|g;
#	$parm =~ s|\&circleb;|\x{24D1}|g;
#	$parm =~ s|\&circlec;|\x{24D2}|g;
#	$parm =~ s|\&circled;|\x{24D3}|g;
#	$parm =~ s|\&circlee;|\x{24D4}|g;
#	$parm =~ s|\&circlef;|\x{24D5}|g;
#	$parm =~ s|\&circleg;|\x{24D6}|g;
#	$parm =~ s|\&circleh;|\x{24D7}|g;
#	$parm =~ s|\&circlei;|\x{24D8}|g;
#	$parm =~ s|\&circlej;|\x{24D9}|g;
#	$parm =~ s|\&circlek;|\x{24DA}|g;
#	$parm =~ s|\&circlel;|\x{24DB}|g;
#	$parm =~ s|\&circlem;|\x{24DC}|g;
#	$parm =~ s|\&circlen;|\x{24DD}|g;
#	$parm =~ s|\&circleo;|\x{24DE}|g;
#	$parm =~ s|\&circlep;|\x{24DF}|g;
#	$parm =~ s|\&circleq;|\x{24E0}|g;
#	$parm =~ s|\&circler;|\x{24E1}|g;
#	$parm =~ s|\&circles;|\x{24E2}|g;
#	$parm =~ s|\&circlet;|\x{24E3}|g;
#	$parm =~ s|\&circleu;|\x{24E4}|g;
#	$parm =~ s|\&circlev;|\x{24E5}|g;
#	$parm =~ s|\&circlew;|\x{24E6}|g;
#	$parm =~ s|\&circlex;|\x{24E7}|g;
#	$parm =~ s|\&circley;|\x{24E8}|g;
#	$parm =~ s|\&circlez;|\x{24E9}|g;
#		
#
##	$parm =~ s|\&|\x{0026}|g;				#
##	$parm =~ s|\&amp;|\x{0026}|g;			#
##	$parm =~ s|\&amp;|\&amp;|g;			#	
	
	return $parm;
}


#-------------------------------------------------------------
sub whoami {
#-------------------------------------------------------------
	(caller(1))[3];
}
