#!/usr/bin/perl

use DBI;
use DBD::mysql;

my $acct = $ARGV[0]; #temp
my @acctsToRun = "";
my $chgAccts = "false";
my $count;

$count = QueryDb ($acct, \$chgAccts, \@acctsToRun);

print STDOUT "Acct# entered: $acct\n";
print STDOUT "#of accounts: $count\n\n";
print STDOUT "Accounts to run:\n\n@acctsToRun\n";

if ($chgAccts eq "true")
{	
	print STDOUT "Oh shit! You typed in a child account #.\nSwitching to @acctsToRun[0]...";
}



#----------------------------------------------------------
sub QueryDb {
#----------------------------------------------------------

	my $acctNum = shift;
	my $switch = shift;
	my $list = shift;
	
	# QUERY CONFIG VARIABLES
	my $user = "sa";
	my $pw = "concasturbate";
	my $connect;
	my $query;
	my $queryHandle;
	my $ct = 0;
	

	# DATA SOURCE NAME
	my $dsn = "dbi:ODBC:BIS";

	# PERL DBI CONNECT
	$connect = DBI->connect($dsn, $user, $pw) || die $!;

	# PREPARE THE QUERY
	$query = "SELECT [AssociateProductID], (SELECT SplitWebPublishProductID FROM BIS_Products WHERE ProductID = $acctNum)
		 AS PublishProductID
	     FROM [BIS].[dbo].[SplitWebPubAssociations]
		 WHERE PrimaryProductID = $acctNum"; 

	$queryHandle = $connect->prepare($query);

	# EXECUTE THE QUERY
	$queryHandle->execute() or die $!;
	
	# BIND TABLE COLUMNS TO VARIABLES
	$queryHandle->bind_columns(undef, \$procIds{AssociateID}, \$procIds{PostProductID}) or die $!;

	# LOOP THROUGH RESULTS
	while ($queryHandle->fetch())
	{
		${$list}[$ct] = $procIds{AssociateID};
						
		if ($procIds{AssociateID} ne $procIds{PostProductID})
		{
			if ($ct lt 1)
			{	
				${$list}[$ct] = $procIds{PostProductID};
				$ct = $ct + 1;
				${$list}[$ct] = $procIds{AssociateID};
				${$switch} = "true";
			}
		}
		
		$ct++;	
	}
	
	return ($ct);
}