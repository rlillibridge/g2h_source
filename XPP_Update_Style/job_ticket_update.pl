#!/usr/bin/perl
#Author: LaRae Chasteen

#=============================================	
# PURPOSE OF THIS PROGRAM
#=============================================	
# Update all XPP job tickets by adding preprocess call to MCCtran if not already there. 
# Adjust position of remaining input translations if necessary.


#=============================================	
# PROCESSING OUTLINE
#=============================================	
# To be run on \\mcc-xpp-01\xpp\std_jobz\alljobz\CLS_pubdocs\GRP_alt\JOB_*\_jt_job.sde (every job ticket in all styles)
# Export every XPP job ticket to XML format in c:\temp.
# Update each XML file (add MCCtran, adjust other lines)
# Import every updated XML file, overwriting existing job tickets.
#=============================================	
# PROCESSING DETAILS
#=============================================	



#out: C:\XPP\std_jobz\alljobz\CLS_pubdocs\GRP_alt\JOB_#99995>sdedit _jt_job.sde -aout c:\temp\99995-jt.xml -xml
#in: C:\XPP\std_jobz\alljobz\CLS_pubdocs\GRP_alt\JOB_#99995>sdedit _jt_job.sde -ain "c:\temp\99995-jt.xml"


# excerpt from _jt_job.XML:
    # <ttx1inp>MCCtran</ttx1inp>
    # <ttx2inp>iso2gen</ttx2inp>
    # <ttx3inp>gentoxy1</ttx3inp>
    # <ttx1out/>
    # <pagesize>colossal</pagesize>
    # <maxpages>4000</maxpages>
    # <maxpickups>65535</maxpickups>
    # <archive>none</archive>
    # <fvfil2/>
    # <fvfil3/>
    # <fvfil4/>
    # <fvfil5/>
    # <imglib1>#99995</imglib1>
    # <imglib2>MCC_Images</imglib2>
    # <imglib3/>
    # <imglib4/>
    # <imglib5/>
    # <imglib6/>
    # <imglib7/>
    # <imglib8/>
    # <imglib9/>
    # <imglib10/>
    # <imglib11/>
    # <imglib12/>
    # <kernlib/>
    # <ttx2out/>
    # <ttx3out/>
    # <ttx4inp>gentoxy2</ttx4inp>
    # <ttx5inp>gentoxy3</ttx5inp>
    # <ttx6inp/>
    # <ttx4out/>
    # <ttx5out/>
    # <ttx6out/>
    # <ml>off</ml>

#=============================================	
# PRAGMA 
#=============================================	
use strict;
use warnings;
use Data::Dumper;
use Term::InKey;
# use Switch; # EXE gives switch error. Perlmonks say too many caveats with Switch.pm; use if / elsif instead.

#=============================================
# MAIN
#=============================================

my $dir = shift;
my $program_name = "TOCfromGC";
my $program_message = "\n=====================================\n\t$program_name\n=====================================\n\n";
