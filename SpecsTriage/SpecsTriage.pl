#!/usr/bin/perl
# SpecsTriage.pl
# Programmer:  Raymond Lillibridge
my $myVersion = "Version:  2011-08-04.1 (beta)";

#=============================================	
# CHANGE HISTORY:
# 2011-08-04 - Raymond Lillibridge - CKLIST zip to NOT include any subfolders
# 20110603 - Raymond Lillibridge - Added:  use strict (corr. all errors), allow *.mcc as GenCode file.
# 20101104 - David Nichols - Added logic to detect drive app is running from as to avoid hard-coding paths.
# 20100920 - Raymond Lillibridge - No longer running CleanHardReturns();
# 20100810 - Raymond Lillibridge - manage GenCode:  oxh5, aoxh5, oh3x, & (next line)
#     xtra added space after clearing hard return following ";adv=6q;", ";il=4q;" and ";qa;;lz;" when contained in listitem.
# 2010-08-03 - Raymond Lillibridge - Lisa found problem with ;fn;...
#	I also added missing stuff for pickup tags and <main>, &c.
# 2010-06-02 - Raymond Lillibridge - Phil asked that I add logic to remove all "imbedded" hard-returns.
# 2010-02-22 - Raymond Lillibridge - John asked to have NOT TAKES REMOVAL on files called:  spec or specstoc
# 2010-02-19 - Raymond Lillibridge - David asked to have NOT TAKES REMOVAL on files called:  specs or specsix
# 2010-02-05 - Raymond Lillibridge - David asked to have the filtering AFTER removal of "takes" from all files.
# 2010-01-22 - Raymond Lillibridge - BUG:  'new' excludes works but 'old' doesn't - so fixed it.
# 2010-01-15 - Raymond Lillibridge - Added logic to create/init 'Folio'\<A#>\RTF and ~\XML folders
# 2010-01-12 - Raymond Lillibridge - Added logic to use exclude list in jobspecs_config.xml before copying to folio...
# 2009-09-09 - Reworking logic to remove print_specs GenCode tagging.  Must allow for multiple pairs of tags in a single line and all 
#	options for partial tagging that breaks across lines.

#=============================================	
#	Config Name and Location...
#=============================================	
if (!defined $PerlApp::VERSION) {
		$APPpath = $0;
}	else {
		$APPpath = PerlApp::exe();
}
my $driveletter = substr($APPpath,0,2);
my $sJobSpecs_ConfigName = "JobSpecs_config.xml";
my $sAppPath= ($driveletter . "\\APPS\\CONFIG");					#LOCAL (location of file above)
#my	$sAppPath= "\\\\mcc-pub-05\\MCC_XPP";		#NETWORK

umask 0;
#=============================================	
# PROCESSING OUTLINE
#=============================================	
#	Reads "Job List" (JobSpecsTriage.xml) as input.
#	Creates two output Reports "JobSpecs_Takes.txt" and "JobSpecs_PrintSpecs.txt".
#	JobSpecs_Takes.txt - Reports on Jobs that have NO <print_specs/> tags			(must read ALL Job's GenCode files to determine)
#	JobSpecs_PrintSpecs.txt - Reports on Jobs that have <print_specs/> tags		(must read ALL Job's GenCode files to determine)
#
#	NOTES:
#	(OUTPUT) JobSpecs_Takes.txt will report (for each JOB):
#		- balanced <begtake>..<endtake>
#		- unbalanced <begtake>..<endtake>
#		- count of <begtake>..<endtake>
#
#	(OUTPUT) JobSpecs_PrintSpecs.txt will report (for each JOB):
#		- balanced <begtake>..<endtake>
#		- unbalanced <begtake>..<endtake>
#		- count of <begtake>..<endtake>
#		- balanced <print_specs>..</print_specs>
#		- unbalanced <print_specs>..</print_specs>
#		- count of <print_specs>..</print_specs>
#

#=============================================	
# PRAGMA
#=============================================	
use strict;
use warnings;
no warnings 'uninitialized';

use XML::Simple qw(:strict);
use Data::Dumper;
use File::Basename;
use File::Copy;
use File::Copy::Recursive;
use File::Listing qw(parse_dir);
use File::chdir;
use Win32;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );

#use Getopt::Std;
use Getopt::Long;

my $opt_d;			#-d			debug switch
my $opt_job="-none-";	# --job		job or account_number
my $opt_new;			# --new		new_process
my $opt_old;			# --old		old_process
my $opt_update;		# --update	update_original_account_files
my $opt_noupdate;		# --noupdate	no update_original_account_files

my $opt_h;
my $opt_help;

my $OverrideJobSpecsTriage = 0;	#flag set to 1 if command line options other than -d

GetOptions(
	"d"			=>	\$opt_d,
	"job=s" 		=>	\$opt_job,
	"new"		=>	\$opt_new,
	"old"			=>	\$opt_old,
	"update"		=>	\$opt_update,
	"noupdate"	=>	\$opt_noupdate,
	"h"			=>	\$opt_h,
	"help"		=>	\$opt_help
	);

#=== Win32 ===
use Cwd;
my $CWDSAVED;
my $WinLoginName = getlogin || getpwuid($<) || "Win Login-UNKNOWN-";

# Date and Time vars
my($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime;
$year += 1900;
my $now_string = localtime;
my $nowYYYYMMDD_HHMMSS= dateTime();
my $LogDateTime =	$nowYYYYMMDD_HHMMSS->{'dateNr'} .  "_" . 
					$nowYYYYMMDD_HHMMSS->{'HHMMSS'};

#for development purposes...
use Term::InKey;


#=============================================	
# GLOBALS
#=============================================
my $username=$WinLoginName;
my $UserPath="";
my $UserPathTemp="";
my $contents;
my $PrintSpecsExists=0;

my $Path;
my $BackupPath;
my $AccountPath_acctno;
my $FolioPath_acctno;
my $myFolder;
my $AcctConfigFile;
my $str1;
my $str2;
my $zip;
my $file_member;
my $dir_member;
my $FoundSpecsHeader="0";

my $XMLconfig;
my $XMLJobs;

# config values:
my $config_allusers;
my $config_PrintSpecsTemp;
my $config_JobSpecsFile;
my $config_LogFile;
my $config_AccountPath;
my $config_BackupPath;
my $config_FolioPath;
my $config_OldProcessFolder;
my $config_NewProcessFolder;
my $config_CKListPath;
#my @config_IncludeFilters=();
#my @config_ExcludeEqualsFilters=();
#my @config_ExcludeContainsFilters=();
my @config_ExcludeFilter=();

my $BadConfig=0;
my $FileFilterFlag="skip";

my @account=();
my @newprocess=();
my @update_accounts=();
my @ErrorFile=();

my $JobCount;
my $job;
my $PrintKounter;

my $cwd="-empty-";

my @FILEHANDLE = (*STDOUT, *LOG);
my $outfile;
my @files=();
my $filename;
my $x;
my $rtn;
my $dashes = "----------------------------------------------------------------------";

#=============================================	
# MAIN
#=============================================	
# Clear screen
#	print "Press any key to clear the screen: ";
#	$x = &ReadKey;
#  or...
#	chomp($go=<STDIN>);
	&Clear;
#	print "You pressed $x\n";

if( ($opt_h) or ($opt_help) )	{
#	print STDOUT "\nFor HELP please enter:  'pod2text specstriage.pl' at the command prompt.\n";
	system `SpecsTriage.html`;
	exit 0;
}

print(STDOUT "\nRUNNING:  SpecsTriage.exe (pl)\t\t$myVersion\n");
print(STDOUT "*------------------------------------------------------------------------------*\n");

&Housekeeping;
$JobCount = $#account + 1;

if($JobCount gt 0 ) {		
	#Process All Accounts..
	for( $job=0; $job<$JobCount; $job++)	{
		
		if($OverrideJobSpecsTriage > 0) {
			printOut("\tCOMMAND LINE: ");
			&print_opts();
			printOut("\n*------------------------------------------------------------------------------*\n");
		}
		
		printOut("\n\n$dashes\n\t$account[$job]\n");
		printOut("\t=====\n");
		
		$UserPathTemp = $UserPath . "\\temp";
		
		if(! -e "$UserPathTemp")	{
			chdir($UserPath);
			mkdir("$UserPath\\temp", 0777) or die "+++Can't create $UserPath\\temp\n$!";
		}
		
		# DELETE Existing Files in $UserPath/temp folder...
		if(-e "$UserPathTemp")	{
			$CWD="$UserPathTemp";
			if($opt_d) {printOut("\n\tDELETING TEMP FILES in:\n\t$UserPathTemp\n\n");}
			opendir( USERH, "$CWD") or die "Can't OPEN USERH --> $CWD";
			
			# Process Each File...
			while (defined($filename = readdir USERH)) {
				next if ($filename eq "." || $filename eq ".."); 
				unlink $filename;
			}
			closedir USERH;
		}
		
		$CWD =  "$config_BackupPath";
		$AccountPath_acctno=$config_AccountPath . "\\" . $account[$job];
		
		#Backup Job...
		$BackupPath = "$config_BackupPath\\" . "$account[$job]";
				
		if( -e ($BackupPath))	{
			if($opt_d) {printOut("\tBackup Path:\n\t$BackupPath\n");}
		} else {
			if($opt_d) {printOut("\tBackupPath NOT FOUND!\n\t$BackupPath\n\n");}
			&CreatePath("$BackupPath");
		}
	
		$CWD=$BackupPath;
	
		# Create backup subfolder name from date and time...
		&Update_LogDateTime;
		$myFolder=$LogDateTime;
		mkdir( "$myFolder", 0777) 	or die "Could NOT Make BACKUP DATE-TIME-FOLDER:  ~\\$myFolder\n$!";
					
		$CWD=$BackupPath . "\\" . $myFolder;
		
		if($opt_d) {
			printOut("\n\tCreated BACKUP FOLDER:\n");
			printOut("\t$CWD\n");
			printOut("\n\tCopying FROM ('G-Accts' Acct):\n\t$AccountPath_acctno\n");
		}
		
		$zip = Archive::Zip->new();
		
		# Copy $account file to backup folder...
		opendir(ACCTH, "$AccountPath_acctno")        or die "Couldn't open $AccountPath_acctno for reading: $!";

		# ADDING FILES TO TEMP FOLDER FOR BACKUP...	
		if($opt_d) {printOut("\t COPYING FILES TO FOLDER FOR BACKUP...\n");}
		
		while (defined(my $acctfile = readdir ACCTH)) {
			next if ($acctfile eq "." || $acctfile eq ".."); 
			
			$filename = "$AccountPath_acctno\\$acctfile";
			
			# Copy 'cklist'...
#			if((-d $filename) and (uc($acctfile) eq "CKLIST")	)	{			
#				if($opt_d) {printOut("\t\t$acctfile - FOLDER\n");}
#				# $filename IS a SELECT DIRECTORY...
#				$dir_member = $zip->addDirectory( "$filename/" );
#
#				opendir(CKLISTH, "$filename")	or die "Couldn't open ? for reading: $!";
#					while (defined(my $ckfile = readdir CKLISTH)) {
#						next if ($ckfile eq "." || $ckfile eq ".."); 
#						
#						if(!-d $ckfile)	{
#							$file_member = $zip->addFile("$filename/$ckfile");
#							if($opt_d) {printOut("\t\t\t---\$ckfile=$ckfile\n");}
#						}
#					}
#				closedir CKLISTH	or die "Couldn't CLOSE CKLISTH file handle: $!";
#			}

				# Copy 'cklist'...
				if((-d $filename) and (uc($acctfile) eq "CKLIST")	)	{			
					if($opt_d) {printOut("\t\t$acctfile - FOLDER\n");}
					$dir_member = $zip->addDirectory( "$filename/" );
						
					opendir(CKLISTH, "$filename")	or die "Couldn't open ? for reading: $!";
						while (defined(my $ckfile = readdir CKLISTH)) {
							next if($ckfile eq "." || $ckfile eq "..");
							
							if(-d "$filename/$ckfile") {
								printOut("\t\t\t---\$ckfile=$ckfile \(DIRECTORY SKIPPED\)\n");	
								next;
							}
														
							$file_member = $zip->addFile("$filename/$ckfile");
							if($opt_d) {printOut("\t\t\t---\$ckfile=$ckfile \(ADDED TO ZIP\)\n");}
						}
					closedir CKLISTH	or die "Couldn't CLOSE CKLISTH file handle: $!";
				}

			#Copy all files NOT folders...
			# NOTE:  $filename includes the full path, too!
			if( (!-d $filename))	{
				# $filename is NOT a DIRECTORY...
				$file_member = $zip->addFile( "$filename");
				copy($filename, "$UserPath/temp")
					or die "+++COPY Failed \n$filename  --\>  $UserPath/temp/\n\t$!";
				if($opt_d) {printOut("\t\t$acctfile\n");}
			}
		}
		
		# Save the Zip file
		unless ( $zip->writeToFileNamed("$CWD\\$account[$job].zip") == AZ_OK ) {
		   die "Write Zip Error:  $CWD\\$account[$job].zip";		
		}
		
   		if($opt_d) {printOut("\n\tZIP FILE:\n\t$CWD\\$account[$job].zip\n");}

		closedir ACCTH	or die "Couldn't close ACCTH $AccountPath_acctno";
		
		#-----------------------------------------------------------------------
		# TRIAGE:  Process each file in 'temp' looking for <print_specs> tag...
		#-----------------------------------------------------------------------
#printOut("\n[DEBUG] B4 Creating 'temp' folder...\n\t\$UserPath=$UserPath\n\n");		
#		
#		if(! -e "$UserPathTemp")	{
#			mkdir("$UserPath\\temp", 0777) or die "+++Can't create $UserPath\\temp\n$!";
#		}
		
		$CWD=$UserPathTemp;
		
#printOut("\n[DEBUG] AFTER Creating 'temp' folder...\n\t\$UserPath=$UserPath\n\$CWD=$CWD\n\n");		
		
		
		opendir(TEMPH, "$UserPathTemp")        or die "Couldn't open $AccountPath_acctno for reading: $!";
   		if($opt_d) {
   			printOut("\n\tTEMP FILES (print_specs SEARCH):\n\t$CWD\n");
   			printOut("\n\tPROCESSING FILES:\n");
   		}
		# Process Each TEMP File...
		$PrintSpecsExists=0;
		$FoundSpecsHeader=0;
		
		#Do any of the files contain '<print_specs>'???
		while (defined($filename = readdir TEMPH)) {
			next if ($filename eq "." || $filename eq ".."); 
			
# Raymond:  2010-02-22  The following filter asked for by David:			
			next if (	
				uc($filename) eq "SPEC"		|| 
				uc($filename) eq "SPECS"		||
				uc($filename) eq "SPECSIX"		||
				uc($filename) eq "SPECSTOC"
			); 

#printOut("[DEBUG] \$triageFile=$triageFile\n");   				
			   			
   			&LoadContentsVAR("$filename");
   			
   			if($contents =~ /\<print_specs\>/m )	{
   				if($FoundSpecsHeader == 0)	{
					$FoundSpecsHeader=1;
   					printOut("\n===\>\tREMOVING \<print_spec\/\> content from:\n");
   				}
   				
   				$PrintSpecsExists=1;
   				#REMOVE <print_specs>...
				$rtn = &ProcessMyFile($filename);   				

   			} else {
#   				printOut("\t(-------------): $filename\n");
   			}
   			
		}
				
		if(!$PrintSpecsExists)	{
			if($opt_d) {
	   			printOut("\n===\>\tNO FILES HAVE \<print_specs>\n");
	   		} else {
	   			printOut("\tNO FILES HAVE \<print_specs>");
	   		}
		}
		
		closedir TEMPH	or die "Couldn't close TEMPH ($UserPathTemp)";
		
		#Check that file Does NOT Have any <begtake>  or  <endtake> tags...
		@ErrorFile=();
		opendir(TEMPH, "$UserPathTemp")        or die "Couldn't open $AccountPath_acctno for reading: $!";
   		if($opt_d) {printOut("\n\tTEMP FILES (<begtake> & <endtake> SEARCH):\n\t$CWD\n");}
		
		# Process Each TEMP File...
		
		#Do any of the files contain '<begtake>'  ||  '<endtake>'???
		while (defined($filename = readdir TEMPH)) {
			next if ($filename eq "." || $filename eq ".."); 
			next if (! -f $filename);
			next if( ! -T $filename);

# Raymond:  2010-02-22 The following added to filter files names:  specs or specsix:
			next if (	
				uc($filename) eq "SPEC"		|| 
				uc($filename) eq "SPECS"		||
				uc($filename) eq "SPECSIX"		||
				uc($filename) eq "SPECSTOC"
			);
						   			
   			&LoadContentsVAR("$filename");
   			
   			if( ($contents =~ /\<begtake\>/m) ||  ($contents =~ /\<endtake\>/m) )	{
   				push @ErrorFile, $filename;
   			}
		}
		
		closedir TEMPH	or die "Couldn't close TEMPH ($UserPathTemp)";

		
		if($#ErrorFile > -1)	{
			printOut("\n\n\t$dashes\n");
#			print "\a";	#beep!!!
			printOut("===\>\tERROR!  ERROR!  ERROR!  ERROR!  ERROR!  ERROR!  ERROR!\n");
			printOut("\t$dashes\n");
			printOut("\tPLEASE CORRECT THESE FILES!\n\t(All 'TAKES' MUST be wrapped in <print_specs/> tags.)\n\n");
			
			for my $i (0..$#ErrorFile)	{
				printOut("\t\t- $ErrorFile[$i]\n");		
   			}
   			
		} else {
			# All files have been converted and there are NO 'takes'  (Yea!)
			# COPY modified files TO appropriate FOLIO folder...
   			if($opt_d) {printOut("\n===\>\tNO FILES HAVE \<begtake>  or  <endtake>\n");}
   		   		
			#Does $config_FolioPath folder exist?
			if(!-e $config_FolioPath )	{
				printOut("\n\n\n$dashes\n");
				printOut("$dashes\n");
				printOut("$dashes\n");
#					print "\a";	#beep!!!
				printOut("\tERROR!  ERROR!  ERROR!  ERROR!  ERROR!  ERROR!  ERROR!\n");
				printOut("$dashes\n");
				printOut("$dashes\n");
				printOut("$dashes\n\n");
				printOut("\tFOLIO BASE PATH NOT FOUND\:  $config_FolioPath\n\tSEE SysAdmin!\n\n");
				closedir TEMPH;
				do_Error(whoami(), "\...FOLIO PATH MUST EXIST - Processing cancelled.\n");
				SpecsExit(1);
			}
			
			$str1 = $config_FolioPath . "\\" . $account[$job];
			
						
			#CREATE ACCOUNT PATH IF NECESSARY...
			if(! -e $str1)	{
				if($opt_d){printOut("\n===>\tCREATING FOLIO ACCOUNT PATH $str1\n\n");}
				chdir $config_FolioPath;
				mkdir($account[$job], 0777)	or die "+++ Can't mkdir $account[$job]\n$!";
			}   	
				   		
			$CWDSAVED = $CWD;
			$CWD = $str1;		# FOLIO + ACCOUNT#
			
			if(! -e "XML")	{
				if($opt_d){printOut("\n===>\tCREATING FOLIO ACCOUNT FOLDER:  XML\n");}
				chdir "XML";
				mkdir("XML", 0777)	or die "+++ Can't mkdir $account[$job]\\XML\n$!";
			} else {
				$CWD=$str1 . "/XML";
				if($opt_d){printOut("\n===>\tFOLIO ACCOUNT FOLDER EXISTS:  XML\n");}
				if($opt_d) {printOut("\n\tINITIALIZING:  $CWD\n\n");}
				opendir( XMLH, "$CWD") or die "Can't OPEN XMLH --> $CWD";
				
				# Process Each File...
				while (defined($filename = readdir XMLH)) {
					next if ($filename eq "." || $filename eq ".."); 
					unlink $filename;
				}
				closedir XMLH;
			}
			
			$CWD = $str1;

			if(! -e "RTF")	{
				if($opt_d){printOut("\n===>\tCREATING FOLIO ACCOUNT FOLDER:  RTF\n");}
				chdir "RTF";
				mkdir("RTF", 0777)	or die "+++ Can't mkdir $account[$job]\\RTF\n$!";
			} else {
				$CWD=$str1 . "/RTF";
				if($opt_d){printOut("\n===>\tFOLIO ACCOUNT FOLDER EXISTS:  RTF\n");}
				if($opt_d) {printOut("\n\tINITIALIZING:  $CWD\n\n");}
				opendir( RTFH, "$CWD") or die "Can't OPEN RTFH --> $CWD";
				
				# Process Each File...
				while (defined($filename = readdir RTFH)) {
					next if ($filename eq "." || $filename eq ".."); 
					unlink $filename;
				}
				closedir RTFH;			}			

			$CWD = $str1;
			
			# DELETE PRIOR CONTENTS OF 'old' and 'new' Process Folders...
			$str1 = $CWD . "\\" . $config_NewProcessFolder;
			
			#NEW PROCESS FOLDER CLEANUP...
			if(-e $str1)	{
				$CWD=$str1;
				if($opt_d) {printOut("\n\tNew Process Folder Cleanup...\n");}
				opendir(TEMPNEW, $CWD)
						or die "Couldn't open $CWD for reading: \n\t$!";
						
				#DELETE EACH FILE...
				while (defined($filename = readdir TEMPNEW)) {
					next if ($filename eq "." || $filename eq ".."); 
					
					if(! -d $filename)	{
						unlink $filename	or die "NewProcess Cleanup - Can't unlink $CWD/$filename \n $!";
						if($opt_d) {printOut("\t\t$filename has been DELETED\n");}
					}
				}
				closedir TEMPNEW	or die "Couldn't close TEMPNEW ($CWD)";
			}
			
   		   	#OLD PROCESS FOLDER CLEANUP...
			$str1 = $CWD . "\\" . $config_OldProcessFolder;
			if(-e $str1)	{
				$CWD=$str1;
				if($opt_d) {printOut("\n\tOld Process Folder Cleanup...\n");}
				opendir(TEMPOLD, $CWD)
						or die "Couldn't open $CWD for reading: \n\t$!";
						
				#DELETE EACH FILE...
				while (defined($filename = readdir TEMPOLD)) {
					next if ($filename eq "." || $filename eq ".."); 

					if(! -d $filename)	{					
						unlink $filename	or die "OldProcess Cleanup - Can't unlink $CWD/$filename \n $!";
						if($opt_d) {printOut("\t\t$filename has been DELETED\n");}
					}
				}
				closedir TEMPOLD	or die "Couldn't close TEMPOLD ($CWD)";
			}
								
			$str1=$config_FolioPath . "\\" . $account[$job] . "\\" . $config_NewProcessFolder;	
			
			if(uc($newprocess[$job]) eq "YES")	{
				# NEW PROCESS...
				$FolioPath_acctno=$str1;		
				printOut("\n\n***\t\$FolioPath_acctno (NEW):\t$FolioPath_acctno\n\n");

				#Create FOLIO account folder and 'gc' folder if necessary...
				if(! -e $FolioPath_acctno)	{
					if($opt_d){
						printOut("===>\tCREATING FOLIO ACCOUNT NEWPROCESS PATH - $config_NewProcessFolder\n\n");
					}
					chdir $config_FolioPath . "\\" . $account[$job];
					mkdir( $config_NewProcessFolder, 0777)	 or die "+++Can't mkdir $config_NewProcessFolder\n$!";
				}
				
				$CWD=$UserPathTemp;
				#COPY files to $str1  (FOLIO ACCOUNT NEWPROCESS PATH...
				printOut("\tCOPYING FILES TO FOLIO FOLDER\n\t\t\($str1\)\n");
				
				opendir(TEMPH, "$UserPathTemp")        or die "Couldn't open $AccountPath_acctno for reading: $!";

				while (defined($filename = readdir TEMPH)) {
					next if ($filename eq "." || $filename eq ".."); 
					
					if( ! -d $filename)	{	# $filename is NOT a DIRECTORY...

						$FileFilterFlag = FilterFile($filename);
						if( $FileFilterFlag ne "Approved")	{
							next;
						}

						copy($filename, $str1 . "\\" . $filename)
							or die "\n\[FOLIO NEW\] +++\n\tCOPY FAILED\n" .
								"\t$filename  --\>  $str1\\$filename\n$!";
						if($opt_d eq '')	{		
							printOut("\t\t$filename\n");
						}
					}
				}
				
				closedir TEMPH	or die "Couldn't close TEMPH ($UserPathTemp)";
				
			} else {
				# OLD PROCESS...
				$str1=$config_FolioPath . "\\" . $account[$job] . "\\" . $config_OldProcessFolder;	
				$FolioPath_acctno=$str1;
				printOut("\n\n***\t\$FolioPath_acctno (OLD):\t$FolioPath_acctno\n\n");
				
				#Create FOLIO account folder and 'mag' folder if necessary...
				if(! -e $FolioPath_acctno)	{
					if($opt_d){
						printOut("===>\tCREATING FOLIO ACCOUNT OLDPROCESS PATH - $config_OldProcessFolder\n\n");
					}
					chdir $config_FolioPath . "\\" . $account[$job];
					mkdir( $config_OldProcessFolder, 0777)	 or die "+++Can't mkdir $config_OldProcessFolder\n$!";
				}
				
				$CWD=$str1;

				#COPY files to $CWD  (FOLIO ACCOUNT OLDPROCESS PATH...
				printOut("\tCOPYING FILES TO FOLIO FOLDER\n\t\t\($str1\)\n");
				
				opendir(TEMPHO, "$UserPathTemp")        or die "Couldn't open $AccountPath_acctno for reading: $!";
				
				while (defined($filename = readdir TEMPHO)) {
					next if ($filename eq "." || $filename eq ".."); 
					
					if( ! -d $filename)	{	# $filename is NOT a DIRECTORY...
						
						$FileFilterFlag = FilterFile($filename);
						if( $FileFilterFlag ne "Approved")	{
							next;
						}
						
						copy($UserPathTemp ."\\" . $filename, $CWD . "\\" . $filename)
							or die "\n\[FOLIO OLD\] +++\n\tCOPY FAILED\n" .
								"\t$filename  --\>  $CWD\\$filename\n$!";
																
						if($opt_d eq '')	{		
							printOut("\t\t$filename\n");
						}
					}
				}
				
				closedir TEMPHO	or die "Couldn't close TEMPH ($UserPathTemp)";
			}   		
			   			
		}
		

	}
	
	
} else {
	printOut("\n[processing all accounts]\n\tNo accounts to process!\n");
}



&SpecsExit(0);	#true
#=============================================	
# MAIN END
#=============================================	


#=============================================================================	
# 					S U B R O U T I N E S	
#=============================================================================	

#----------------------------------------------------------
sub Housekeeping	{
#----------------------------------------------------------
	my $me = whoami();
	
	#====================
	# Get User Input...
	#====================
	&Clear;	
	
	
	print(STDOUT "\nRUNNING:  SpecsTriage.exe (pl)\t\t$myVersion\n");
	print(STDOUT "\n\tNeed Help?\tEnter:  SpecsTriage --help\n\n\n");
	print(STDOUT "Ready to processing the following input files:\n");
	print(STDOUT "*------------------------------------------------------------------------------*\n");
	
	if($opt_d) {
		print(STDOUT "\tDEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG \n");
		print(STDOUT "*------------------------------------------------------------------------------*\n");
	} else {
		print(STDOUT "\t(Process with '-d' switch to turn on debugging.)\n");
	}
	
	if($opt_job ne "-none-") {
	 	if(($opt_new or $opt_old)	and	($opt_update or $opt_noupdate)	)	{
#			print(STDOUT "\tCOMMAND LINE: ");
#			&print_opts();
#			print(STDOUT "\n*------------------------------------------------------------------------------*\n");
			$OverrideJobSpecsTriage = 1;
		} else {
			print(STDOUT "\tCOMMAND LINE: ERROR! ERROR! ERROR! ERROR! ERROR!\n");
			print STDOUT "\nDumping GetOptions Data (overrides JobSpecsTriage.xml):\n\n";
			print STDOUT "\(job number) = $opt_job\n";	
			print STDOUT "ONE OF THE FOLLOWING REQUIRED:\n";
			print STDOUT "\t'--new'\t(new folio process) = $opt_new\n";	
			print STDOUT "\t'--old'\t(old folio process) = $opt_old\n";	
			print STDOUT "\nONE OF THE FOLLOWING REQUIRED:\n";
			print STDOUT "\t'--update'\t(update original account) = $opt_update\n";	
			print STDOUT "\t'--noupdate'\t(don't update original account) = $opt_noupdate\n\n";
			print(STDOUT "\n*------------------------------------------------------------------------------*\n");
			exit 1;
		}
	}
		
	## Process the files or quit?
	print(STDOUT "\n     continue?...\n----------------------------------------------------------------\n" . 
		" Y = YES\t\t--OR--\t\t(blank\) or N = CANCEL\n" . 
		"\n----------------------------------------------------------------\n");
		
	$x = &ReadKey;
	
	if($x=~ /[Yy]/) {
		# do nothing...
		print(STDOUT "\n");	#add a little space before processing the files
	} else {
		do_Error(whoami(), "\...Processing cancelled.\n");
		exit 0 ;
	}

	if($opt_d and $opt_job)	{
		print STDOUT "\nDumping GetOptions Data (overrides JobSpecsTriage.xml):\n";
		print STDOUT "\$opt_d (debugging) = $opt_d\n";
		print STDOUT "\$opt_job (job number) = $opt_job\n";	
		print STDOUT "\$opt_new (new folio process) = $opt_new\n";	
		print STDOUT "\$opt_old (old folio process) = $opt_old\n";	
		print STDOUT "\$opt_update (update original account) = $opt_update\n";	
		print STDOUT "\$opt_noupdate (don't update original account) = $opt_noupdate\n\n";	
	}
		
	#====================
	# Read JobSpecs_config.xml file...
	#====================
	&Get_JobSpecs_config;
#	&Dump_Config;
	
	#====================
	# Setup PrintSpecsTemp folder...
	#====================
	# Set $CWD to $config_PrintSpecsTemp location...
	$Path = "$config_allusers\\$username\\$config_PrintSpecsTemp";
	
	if( -e "$Path")	{
		print STDOUT "[$me]\n\tPath Exists!\n\t$Path\n\n";
	} else {
		print STDOUT "[$me]\n\tNOT FOUND!\n\t$Path\n\n";
		&CreatePath("$Path");
	}
			
	$CWD=$Path;
	$UserPath=$Path;

	#====================
	# Setup LOGS folder...
	#====================
	$Path = $Path . "\\LOGS";
	
	if(! -e "$Path")	{
		mkdir("LOGS", 0777) or die "Could NOT Make Directory:  ~\\LOGS\n$!\n";
	}
	
	$CWD=$Path;
	if($opt_d) {print STDOUT "[$me]\n\t\$CWD=$CWD\n\n";}

	&CreateLog();
	if($opt_d) {print LOG "\$CWD=$CWD\n";	}

	#====================
	# Read jobspecsfile ...
	#====================	
	$Path = substr($Path, 0, -5);
	$CWD=$Path;
	
	if($opt_d) {printOut("[$me]\n\t\$CWD=$CWD\n\n");}
	
	if(!$OverrideJobSpecsTriage)	{
		if( 	($opt_job eq "-none-") 		and 
			(!$opt_new) 		and
			(!$opt_old) 		and
			(!$opt_noupdate)	and
			(!$opt_noupdate)
			)	{
			
			&Get_JobSpecsFile;
		} else {
			print STDOUT "\nThere is something stinky in Denmark, eh?!!\n\n...Check your parameters.\n";
			exit 1;
		}
	} else {
		# OverrideJobSpecsTriage...
		my $REF_account_single;
		my $REF_newprocess_single;
		my $REF_update_accounts_single;
		
		# load @account...		
		$REF_account_single = $opt_job;
		push @account, $REF_account_single;
	
		# load @newprocess...
		if($opt_new)	{ $REF_newprocess_single="yes"; }
		if($opt_old)	{ $REF_newprocess_single="no"; }
		push @newprocess, $REF_newprocess_single;
	
		# load @update_accounts...
		if($opt_update)	{ $REF_update_accounts_single="yes"; }
		if($opt_noupdate)	{ $REF_update_accounts_single="no"; }
		push @update_accounts, $REF_update_accounts_single;		
	}
}

#----------------------------------------------------------
sub SpecsExit  {
#----------------------------------------------------------
	 my $status = shift;
	 my $me = whoami();
	 
	 
	 if($status == 0) {
	 	printOut("\n\n============================================================\n");
	 	if($opt_d) {
			printOut("[$me]\n\t----- FINISHED!  [\$status=$status] -----\n");
		} else {
			printOut("\t----- FINISHED! -----\n");
		}
	 } else {
		do_Error(whoami(), "----- ERROR!  [\$status=$status]-----\n");
	}
	 
 	printOut("============================================================\n");
	close(LOG);
		
	my $NotePadRTN = system("NotePad $Path\\LOGS\\$config_LogFile");

	exit($status);
}

#----------------------------------------------------------
sub Get_JobSpecs_config()	{
#----------------------------------------------------------
	my $me = whoami();
	my @config = ();
	
	my $JobSpecsConfigFile = "$sAppPath" . "\\" . "$sJobSpecs_ConfigName";

	eval { # JobSpecs_config.xml exist?...
	     if(!-e ($JobSpecsConfigFile))	{
				do_Error(whoami(), "The $JobSpecsConfigFile file does not exist!");
		}
	};	
	if($@) {
		print STDOUT "\n++++++++++++++++++++\nError:\n$@\n++++++++++++++++++++\n";
		&SpecsExit(1);
	}
			
	my $xs = XML::Simple->new(
		forcearray => 1,
		KeepRoot => 0,
		KeyAttr => [ ]
	);
	
	$XMLconfig = $xs->XMLin($JobSpecsConfigFile);	
	
	$config_allusers = $XMLconfig->{allusers}->[0];
	$config_PrintSpecsTemp = $XMLconfig->{printspecsfolder}->[0];
	$config_JobSpecsFile = $XMLconfig->{jobspecsfile}->[0];
	$config_LogFile = $XMLconfig->{logfile}->[0];
	$config_AccountPath = $XMLconfig->{accountfolder}->[0];
	$config_BackupPath = $XMLconfig->{backupfolder}->[0];
	$config_FolioPath = $XMLconfig->{foliofolder}->[0];
	$config_OldProcessFolder = $XMLconfig->{oldprocessfolder}->[0];
	$config_NewProcessFolder = $XMLconfig->{newprocessfolder}->[0];
	
#	my $REF_config_IncludeFilters = $XMLconfig->{inputfilters}[0]->{include};
#	@config_IncludeFilters = @$REF_config_IncludeFilters;

#	my $REF_config_ExcludeEqualsFilters = $XMLconfig->{inputfilters}[0]->{exclude_equals};
#	@config_ExcludeEqualsFilters = @$REF_config_ExcludeEqualsFilters;
	
#	my $REF_config_ExcludeContainsFilters = $XMLconfig->{inputfilters}[0]->{exclude_contains};
#	@config_ExcludeContainsFilters = @$REF_config_ExcludeContainsFilters;
	
	my $REF_config_ExcludeFilter = $XMLconfig->{inputfilters}[0]->{exclude};
	@config_ExcludeFilter = @$REF_config_ExcludeFilter;

	#Verify that $config_* variables have data...
	$BadConfig=0;
	if($config_allusers eq "") {&BadConfigData("config_allusers", $config_allusers); }
	if($config_PrintSpecsTemp eq "") {&BadConfigData("config_PrintSpecsTemp", $config_PrintSpecsTemp); }
	if($config_JobSpecsFile eq "") {&BadConfigData("config_JobSpecsFile", $config_JobSpecsFile); }
	if($config_LogFile eq "") {&BadConfigData("config_LogFile", $config_LogFile); }
	if($config_AccountPath eq "") {&BadConfigData("config_AccountPath", $config_AccountPath); }
	if($config_BackupPath eq "") {&BadConfigData("config_BackupPath", $config_BackupPath); }
	if($config_FolioPath eq "") {&BadConfigData("config_FolioPath", $config_FolioPath); }
	if($config_OldProcessFolder eq "") {&BadConfigData("config_OldProcessFolder", $config_OldProcessFolder); }
	if($config_NewProcessFolder eq "") {&BadConfigData("config_NewProcessFolder", $config_NewProcessFolder); }
		
	if($BadConfig > 0)	{
		exit(1);
	}
}


#----------------------------------------------------------
sub Get_JobSpecsFile()	{
#----------------------------------------------------------
	my $me=whoami();
	
	if($opt_d) {
		printOut("[$me]\n\tBEGIN:\n");
		printOut("\t\$CWD=$CWD\n");
	}

	eval { # Job Specs File exist?...
	    	if(!-e ($config_JobSpecsFile))	{
	    		die "\tThe $config_JobSpecsFile file does not exist!\n";
	    		print LOG "\tThe $config_JobSpecsFile file does not exist!\n";
			do_Error(whoami(), "The $config_JobSpecsFile file does not exist!\n");
		}
	};	
	if($@) {
		printOut("\n++++++++++++++++++++\nError:\n$@\n++++++++++++++++++++\n");
		&SpecsExit(1);
	}
			
	my $xs = XML::Simple->new(
		ForceArray => 1,
		KeepRoot => 0,
		KeyAttr => []
	);
	
	$XMLJobs = $xs->XMLin($config_JobSpecsFile);	

#	&Dump_Jobs;

	# load @account...
	my $REF_account;
	
	foreach my $i (0..$#{ $XMLJobs->{'job'} })	{
		$REF_account = $XMLJobs->{'job'}[$i]->{'account'};
		push @account, @$REF_account;
	}

	# load @newprocess...
	my $REF_newprocess;
	
	foreach my $i (0..$#{ $XMLJobs->{'job'} })	{
		$REF_newprocess = $XMLJobs->{'job'}[$i]->{'newprocess'};
		push @newprocess, @$REF_newprocess;
	}

	# load @update_accounts...
	my $REF_update_accounts;
	
	foreach my $i (0..$#{ $XMLJobs->{'job'} })	{
		$REF_update_accounts = $XMLJobs->{'job'}[$i]->{'update_accounts'};
		push @update_accounts, @$REF_update_accounts;
	}

	# Dump Arrays...
	$PrintKounter=0;
	for my $i (0..$#account)	{
		$PrintKounter = $i + 1;
		printOut("\n[$PrintKounter]\taccount=\t$account[$i]\n");
		printOut("\tnewprocess=\t$newprocess[$i]\n");
		printOut("\tupdate_accounts=$update_accounts[$i]\n");
	}
}


#----------------------------------------------------------
sub CreateLog() {
#----------------------------------------------------------
	my $myDT=$LogDateTime;

	$myDT =~ s|(\d\d\d\d)(\d\d)(\d\d)_(\d\d)(\d\d)(\d\d)|$1\/$2\/$3 $4:$5:$6|g;
	
	open(LOG, ">:utf8",  "$CWD\\$config_LogFile")
		or die "Can't CREATE LOG file $config_LogFile: $!\n"; 
		
	print LOG "($config_LogFile)\n\nApplication:\tSpecsTriage.exe (pl)\t\t$myVersion\n";
	print LOG "Date_Time:\t\t$myDT\n";
	print LOG "====================================================================================\n";

	if($opt_d) {
		print LOG "\tDEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG \n";
		print LOG "*------------------------------------------------------------------------------*\n";
		
		print LOG "\n\n================================\n" .
			"[DEBUG] EXCLUDE:\n================================\n";
		foreach my $exc (@config_ExcludeFilter)	{
			print LOG "$exc\n";	
		}
		print LOG "================================\n\n";
	} else {
		print LOG "\t(Process with '-d' switch to turn on debugging.)\n";
	}
}

# NOT USED 2011-06-06
##----------------------------------------------------------
#sub CreateBackupFolder()	{
##----------------------------------------------------------
#	# COPY FILES TO BACKUP Directory if it exists.
#	if (-e "$backupFolder") {
#		rmtree([$backupFolder]);
#		mkdir("$backupFolder", 0777) or die "Can't CREATE $backupFolder:\n$!\n";		
#	} else {
#		# $backupFolder DOES NOT EXIST...
#		mkdir("$backupFolder", 0777) or die "Can't CREATE $backupFolder:\n$!\n";
#	}
#	
#	print LOG "Created $backupFolder subfolder...\n\n";
#	print LOG "------------------------------------------------------------\n\n";
#}


#----------------------------------------------------------
sub ProcessMyFile()	{
#----------------------------------------------------------
	my ($infile) = @_;
	my $me = whoami();
	my $outputfile;
	my $takes;
	my $foundSpec=0;
	
	my $startIndex=0;
	my $endIndex=0;
	my $newLine="";
	my $holdLine="";
		
	printOut("\t\t$infile");
			
	$outputfile = $infile;		# create PASS-ONE output file...	
	move($infile, "$infile.temp") or die "\n\tRENAMING FAILED for $infile: $!\n";
		
	# INPUT FILE...
	open(FILE, '<:utf8', "$infile.temp")
		|| die "\n\tOPEN INPUT FAILED: \n\t$CWD\\$infile\n $!";
		
	# OUTPUT FILE...
	open(OUT, '>', "$outputfile")
		|| die "OPEN OUTPUT FAILED: \n\t$CWD\\$outputfile\n $!";
		
	if($contents =~ /<print_specs>/) {
		$takes = 1;
	} else {
		$takes = 0; 
	}
	
	# Since $contents contains the whole file...
	$contents =~ s|(<print_specs>)(.+?)(<\/print_specs>)||smg;
	
#	# 2010-06-02 - Removing inline extra hard-returns ...
#	$contents =~ s|(.?[^>;])\n(.?[^<;\n])(.+?)\n|$1$2$3|g;	#doesn't work quite right...
## !!! NO LONGER RUNNING THE FOLLOWING TWO LINES (2010-09-20)
#	print STDOUT "...Cleaning Hard Returns...\n";
#	$contents = CleanHardReturns($contents);

	
	print OUT "$contents";
	
	close(OUT);
	close(FILE);
	unlink "$infile.temp";	# delete INPUT file...

	#Copy the file back to Accounts if $update_accounts[$job]='yes'...
	if($takes != 0) {
		if(uc($update_accounts[$job]) eq "YES")	{
			printOut("... (Copy Back to Accounts)\n");
			copy($infile, "$AccountPath_acctno/$infile")
				or die "[$me] +++COPY FAILED \n$infile  --\>  $AccountPath_acctno/$infile";		
		} else {
			printOut("... (NOT COPIED BACK TO ACCOUNTS!)\n");
		}
	}
		
}


#-------------------------------------------------------------
sub dateTime {
#-------------------------------------------------------------
	my ($sec,$min,$hour,$mday,$monNr,$year);
	my $now = {};
	#get date and time
	($sec,$min,$hour,$mday,$monNr,$year) = localtime(time());
    	my ($date, $date_nr, $clock);
	my $month=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")[$monNr];
    	$monNr++;
	$monNr = sprintf("%02d",$monNr);
	my $mdayNr = sprintf("%02d",$mday);
	$sec = sprintf("%02d",$sec);
	$min = sprintf("%02d",$min);
	$hour = sprintf("%02d",$hour);
	$year= 1900 + $year;
	$now->{'date'} = "$year-$month-$mday";
	$now->{'dateNr'} = "$year$monNr$mdayNr";
	$now->{'time'} = "$hour:$min:$sec";
	$now->{'HHMMSS'} = "$hour$min$sec";
	return($now);		
}


#-------------------------------------------------------------
sub Dump_Config	{
#-------------------------------------------------------------
	my $i;
	
	 print STDOUT "\n--------------------- Dump_Config: \$XMLconfig ----------------------\n";
#	 my $myDump = Dumper($XMLconfig);
#	 print STDOUT $myDump;
	
	print STDOUT "\$config_allusers=$config_allusers\n";
	print STDOUT "\$username=$username\n";
	print STDOUT "\$config_PrintSpecsTemp=$config_PrintSpecsTemp\n";
	print STDOUT "\$config_LogFile=$config_LogFile\n";
	print STDOUT "\$config_AccountPath=$config_AccountPath\n";
	print STDOUT "\$config_BackupPath=$config_BackupPath\n";
	print STDOUT "\$config_FolioPath=$config_FolioPath\n";
	print STDOUT "\$config_CKListPath=$config_CKListPath\n";
	
#	print STDOUT "\nInclude Filters (\@config_IncludeFilters):\n";
#	for( $i = 0;  $i <= $#config_IncludeFilters; $i++)	{
#		print STDOUT "\t[$i] = $config_IncludeFilters[$i]\n";
#	
#	}
	
#	print STDOUT "\nExclude Filters (\@config_ExcludeContainsFilters):\n";
#	for( $i = 0;  $i <= $#config_ExcludeContainsFilters; $i++)	{
#		print STDOUT "\t[$i] = $config_ExcludeContainsFilters[$i]\n";
#	
#	}
	
	print STDOUT "\n-------------------END DUMPER: \$XMLconfig --------------------\n\n";
}


#-------------------------------------------------------------
sub Dump_Jobs	{
#-------------------------------------------------------------
	my $me=whoami();
	my $i;
	
	printOut("\n--------------------- $me: \$XMLJobs ----------------------\n");
	my $myDump = Dumper($XMLJobs);
	printOut("$myDump");
	printOut("\n-------------------END $me: \$XMLJobs --------------------\n\n");
}


#----------------------------------------------------------
sub CreatePath {
#----------------------------------------------------------
	my ($Path) = @_;
	my $me = whoami();
	my $UserShare="";
	my $mappedDrive="";
	my $folderString="";
	my @folders=();
	my $currPath="";
	
	
	print STDOUT "[$me]...\n";
# THE FOLLOWING REMOVED PER PHIL 2009-08-31...
#	print STDOUT "====================================================\n";
#	print STDOUT "Press 'C' to CREATE THE FOLLOWING USER PATH...\n";
#	print STDOUT "CREATING THE FOLLOWING USER PATH...\n";
#	print STDOUT "====================================================\n\n";
#	print STDOUT "    $Path\n\n\t\t-or-\n\n\tAny other key to EXIT\n\n";
	
#	$x = &ReadKey;
	
#	if($x =~ /[Cc]/) {
		#Create DIR...		
#		print(STDOUT "\n");
		print STDOUT "CREATING DIRECTORY(S):  $Path...  \n";
		
		
		if($Path =~ /^\\\\/)	{
			$UserShare = $Path;
			$UserShare =~ s|^\\\\(.+?)\\(.+?)\\(.+?)$|\\\\$1\\$2\\|g;		#"\\server\share\"
			print STDOUT "\t\$UserShare=$UserShare\n";
			$currPath=$UserShare;
			
			#Load @folders...
			$folderString = $Path;
			$folderString =~ s|^\\\\(.+?)\\(.+?)\\(.+?)\\(.+?)$|$3\\$4|g;	# ~\folders
			print STDOUT "\t\$folderString=$folderString\n";
		} else {
			$mappedDrive = $Path;			
			$mappedDrive =~ s|^(.+?)(\:\\)(.+?)$|$1$2|g;
			print STDOUT "\t\$mappedDrive=$mappedDrive\n";
			$currPath=$mappedDrive;
			
			#Load @folders...
			$folderString = $Path;
			$folderString =~ s|^(.+?)(\:\\)(.+?)$|$3|g;
			print STDOUT "\t\$folderString=$folderString\n";
		}	
		
		@folders = split(/\\/, $folderString);
		foreach my $mysplit (@folders)	{
			print STDOUT "\t\t$mysplit\n";	
		}
		
		print STDOUT "\n ok...Actually creating the missing directories...\n";

		if(-e $currPath)	{		# either "\\server\share\"  or  mapped drive ex. "C:\"
			local $CWD=$currPath;
			
			#create folders if they don't exist...
			foreach my $myFolder (@folders)	{
				
				print STDOUT "\t===\> \$myFolder=$myFolder\n";
				
				if( -e $myFolder)	{
					$CWD = $CWD . "\\" . $myFolder;
					print STDOUT "\t\tUpdated \$CWD to:  $CWD\n";
				} else {
					mkdir("$myFolder", 0777) or die "+++ Can't mkdir $myFolder\n$!\n";
					$CWD = $CWD . "\\" . $myFolder;
					print STDOUT "\t\tCreated folder $myFolder and ...\n" . 
								"\t\tUpdated \$CWD to:  $CWD\n";
				}
			}	
			
			print STDOUT "CREATED DIRECTORY(S)!\n\n";
			
		} else {
			do_Error(whoami(), "Dang!  Something is stinky in Denmark!");
		}
				
		
#	} else {
#		print(STDOUT "\n\...SpecsTriage processing cancelled.\n");
#		exit 0 ;
#	}
}

##remove the content of a folder and then the folder itself
##-------------------------------------------------------------
#sub folderRemove {
##-------------------------------------------------------------
#	my ($folder) = @_;
#	my $file;
#	message(" -removing folder and content: $folder\n", 8);
#	opendir DIR, $folder or return;
#	while ($file = readdir DIR) {
#		next if ($file =~m#^\.$#);
#		next if ($file =~ m#^\.\.$#);
#		message ("  -deleting file <$file>\n", 9);
#		unlink "$folder$FS$file";
#	}
#	rmdir $folder or badExit("could not remove export folder\nsee $folder");
#}


#list the contents of a folder
#-------------------------------------------------------------
sub listDir {
#-------------------------------------------------------------
	my ($folder) = @_;
	my $file;
	
	print STDOUT "listing folder and content: $folder\n";
	
	opendir DIR, $folder or die "*** ERROR ***\nCouldn't open folder $folder\n$!";

	print STDOUT "============================================================================\n";
	print STDOUT "     FOLDER LISTING FOR:  $folder\n";	
	print STDOUT "============================================================================\n";

	while ($file = readdir DIR) {
		next if ($file =~m#^\.$#);
		next if ($file =~ m#^\.\.$#);
		print STDOUT "$file\n";
	}
}

#-------------------------------------------------------------
sub do_Error {
#-------------------------------------------------------------
	my ($mySub, $myMessage) = @_;
	
	printOut("ERROR:  [$mySub]\n\t$myMessage\n");
}

#-------------------------------------------------------------
sub whoami {
#-------------------------------------------------------------
	(caller(1))[3];
}

#-------------------------------------------------------------
sub Update_LogDateTime{
#-------------------------------------------------------------
	$nowYYYYMMDD_HHMMSS= dateTime();
	$LogDateTime =	$nowYYYYMMDD_HHMMSS->{'dateNr'} .  "_" . $nowYYYYMMDD_HHMMSS->{'HHMMSS'};
}

##-------------------------------------------------------------
#sub ZipDir {
##-------------------------------------------------------------
# # USE:  Create a Zip file
#	@parms = @_;
#	my $myDir = $parms[0];		# the Directory to zip
#	my $myName = $parms[1];	# the Zip File Name
#	my $me = whoami();
#	
##	   use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
#	   my $zip = Archive::Zip->new();
#	   
#	   # Add a directory
#	   my $dir_member = $zip->addDirectory( "$myDir/" );
#	   
##	   # Add a file from a string with compression
##	   my $string_member = $zip->addString( 'This is a test', 'stringMember.txt' );
##	   $string_member->desiredCompressionMethod( COMPRESSION_DEFLATED );
#	   
##	   # Add a file from disk
##	   my $file_member = $zip->addFile( 'xyz.pl', 'AnotherName.pl' );
#	   
#	   # Save the Zip file
#	   unless ( $zip->writeToFileNamed("$myName.zip") == AZ_OK ) {
#	       die '[$me] write error';
#	   }
#	   
##	   # Read a Zip file
##	   my $somezip = Archive::Zip->new();
##	   unless ( $somezip->read( 'someZip.zip' ) == AZ_OK ) {
##	       die 'read error';
##	   }
##	   
##	   # Change the compression type for a file in the Zip
##	   my $member = $somezip->memberNamed( 'stringMember.txt' );
##	   $member->desiredCompressionMethod( COMPRESSION_STORED );
##	   unless ( $zip->writeToFileNamed( 'someOtherZip.zip' ) == AZ_OK ) {
##	       die 'write error';
##	   }
#}

#-------------------------------------------------------------
sub LoadContentsVAR {
#-------------------------------------------------------------
	my ($infile) = @_;
	my $me=whoami();
	
	open INPUT, "<$infile" or die "[$me] Can't open $infile \n$!";
	undef $/;
	$contents = <INPUT>;
	close INPUT;	
	$/ = "\n";     #Restore for normal behaviour later in script
}

#-------------------------------------------------------------
sub printOut {
#-------------------------------------------------------------
	my $parm = shift;
	
	foreach $outfile (@FILEHANDLE) {
		print $outfile "$parm";
	}
}

#-------------------------------------------------------------
sub BadConfigData {
#-------------------------------------------------------------
	my ($parm1, $parm2) = @_;
	my $me = whoami();
	
	$BadConfig=1;
	
	if($opt_d) { print STDOUT "\[$me\] "; }
	print STDOUT "ERROR:  \<$parm1\>=$parm2\n";
}

#-------------------------------------------------------------
sub FilterFile {
#-------------------------------------------------------------
	my $parm = shift;
	my $me = whoami();
	my $FilteredParm=0;
	
#	#SELECT ONLY *. FILES (due to legacy processing on XPP)...
	if($parm !~ /\./)	{$FilteredParm = 1;}
	#$FilteredParm = grep { !/\./} $parm;
	if($parm =~ /(.+?)\.mcc$/)	{$FilteredParm = 1;}
		
	if($opt_d) {
		printOut("\t\t$parm");
		if((length $parm) > 7)	{
			printOut("\t[\$FilteredParm=$FilteredParm]\t"); 
		} else {
			printOut("\t\t[\$FilteredParm=$FilteredParm]\t"); 
		}
	}
			
	if( $FilteredParm < 1)	{		
		if($opt_d) { printOut("--- EXCLUDED (via \$FilteredParm) ---\n"); }
		return "FILTERED";
	}
	
	#EXCLUDE FILTER ENTRIES TO TEST?...
	if($#config_ExcludeFilter > -1)	{	
		foreach my $ff (@config_ExcludeFilter)	{
			if($parm =~ /^$ff$/i)	{	#case insensitive
				if($opt_d) { printOut("--- EXCLUDED (via \@config_ExcludeFilter)---\n"); }
				return "FILTERED";
			}
		}
	}

	
	if($opt_d) { printOut("\(APPROVED\)\n"); }
	
	
	return "Approved";
}

#-------------------------------------------------------------
sub print_opts ()	{
#-------------------------------------------------------------
	my $me = whoami();
	
	if($opt_job)		{ printOut(" --job=$opt_job"); }
	if($opt_new)		{ printOut(" --new"); }
	if($opt_old)		{ printOut(" --old"); }
	if($opt_update)	{ printOut(" --update"); }
	if($opt_noupdate)	{ printOut(" --noupdate"); }
	
}


#-------------------------------------------------------------
sub CleanHardReturns()	{
#-------------------------------------------------------------
	my $strFile = shift;
	my $me = whoami();
	my @line = split(/([\n])/, $strFile);
	my $x;
	my $rtn = "";
	my $myLen=-1;
	my $firstFlag=1;
	
#	for( my $w=0; $w <300; $w++)	{
#print STDOUT "\n--------------------- Dump_Config: \@line ----------------------\n";
#		print STDOUT "[$w] $line[$w]";
#	}
#print STDOUT "\n--------------------- END DUMP ----------------------\n";
		
	for($x=0; $x < $#line; $x++)	{	
		
		if(	($line[$x]  =~ /^;oh\d;/)			|
			($line[$x]  =~ /^;aoh\d;/)			|
			($line[$x]  =~ /^;aaoh\d;/)			|
			($line[$x]  =~ /^;aoxh\d;/)			|
			($line[$x]  =~ /^;oxh\d;/)			|
			($line[$x]  =~ /^;oh\dx;/)			|
			($line[$x]  =~ /^;ol\d;/)			|
			($line[$x]  =~ /^;p\d;/)				|
			($line[$x]  =~ /^;b\d;/)				|
			($line[$x]  =~ /^;hg\d;/)			|
			($line[$x]  =~ /^;f(.?[lcr]);/)			|
			($line[$x]  =~ /^;fn;/)				|
			($line[$x]  =~ /^;\\fn;/)			|
			($line[$x]  =~ /^;h(.?[lcr]);/)		|
			($line[$x]  =~ /^;hn\d;/)			|
			($line[$x]  =~ /^;hnlexis/)			|			
			($line[$x]  =~ /^;rn\d;\d;/)			|
			($line[$x]  =~ /^;rn\d;/)	
												)	{																								
			$line[$x] =~ s|^(.*)\s+$|$1|g;
			$myLen = length($line[$x]);			
			$rtn .= $line[$x++] . "\n";
			$firstFlag=1;
			
#			while( ($line[$x]  !~ /^(.?[<;])/)  and  ($x < $#line) )	{	#Just a whitespace (blank) line...
			while(	($line[$x]  !~ /^;oh\d;/)			and
					($line[$x]  !~ /^;aoh\d;/)			and
					($line[$x]  !~ /^;aaoh\d;/)			and
					($line[$x]  !~ /^;aoxh\d;/)			and
					($line[$x]  !~ /^;oxh\d;/)			and
					($line[$x]  !~ /^;oh\dx;/)			and
					($line[$x]  !~ /^;ol\d;/)				and
					($line[$x]  !~ /^;p\d;/)				and
					($line[$x]  !~ /^;b\d;/)				and
					($line[$x]  !~ /^;hg\d;/)			and
					($line[$x]  !~ /^;f(.?[lcr]);/)			and
					($line[$x]  !~ /^;fn;/)				and
					($line[$x]  !~ /^;\\fn;/)				and
					($line[$x]  !~ /^;h(.?[lcr]);/)			and
					($line[$x]  !~ /^;hn\d;/)			and					
					($line[$x]  !~ /^;hnlexis/)			and
					($line[$x]  !~ /^;rn\d;\d;/)			and
					($line[$x]  !~ /^;rn\d;/)				and
					
					# it appears that I need the following, too!
					($line[$x]  !~ /^<begtab/)			and
					($line[$x]  !~ /^<main/)			and
					($line[$x]  !~ /^<pick/)				and
					($line[$x]  !~ /^\{\/PICK/)			and
					($line[$x]  !~ /^\{\/GRAPH/)		and
					($line[$x]  !~ /^\{\/CAPT/)			and
					($line[$x]  !~ /^\{\/ANNT/)			and
					($line[$x]  !~ /^\{\/TITL/)			and
					($line[$x]  !~ /^<set/)				and
					($line[$x]  !~ /^<thead/)			and
					($line[$x]  !~ /^<T(.?[rc])/)			and
					
					($x < $#line) 							)	{	#Just a whitespace (blank) line...
						
						
		if($line[$x] =~ /^\<com\>/)	{
			$rtn .= "\n";
	
			while($line[$x] !~ /<\/com\>/)	{
				$rtn .=  "$line[$x++]";
			}	
		}
									
				$line[$x] =~ s|^\\|__NoTitleNoSubtitle__|g;		# Protect needed GenCode backslash between title\subtitle
				$myLen = length($line[$x]);
								
				if($myLen > 1)	{
					$line[$x] =~ s|(.+?)\s+$|$1|g;
					if($firstFlag==1)	{
						$rtn .= $line[$x];
						$firstFlag=0;
					} else {
						$rtn .= " " . $line[$x];						
					}
				}
				$x++;
			} # end while...
			
			#Found select (if statement) tag...
			$rtn .= "\n\n";
			$x--;
			
		} else {
			$rtn .=  "$line[$x]";
		}
	}
	
	# Now, cleanup the blank lines after the 'container' tags...
	@line = ();
	@line = split(/([\n])/, $rtn);
	$str1="";
	
#print STDOUT "\n--------------------- Dump_Config: \@line ----------------------\n";
#	for(my $w=0; $w <121 && $w<$#line ; $w++)	{
#		print STDOUT "[$w]=$line[$w]\n";
#	}
#print STDOUT "\n--------------------- END DUMP ----------------------\n";
		
	for($x=0; $x < $#line; $x++)	{
		
		if(	($line[$x]  =~ /^;oh\d;/)			|
			($line[$x]  =~ /^;p\d;/)				|
			($line[$x]  =~ /^;b\d;/)				|
			($line[$x]  =~ /^;hg\d;/)			|
			($line[$x]  =~ /^;f(.?[lcr]);/)			|
			($line[$x]  =~ /^;h(.?[lcr]);/)		|
			($line[$x]  =~ /^;hn\d;/)			|
			($line[$x]  =~ /^;rn\d;\d;/)	
											)	{										
			$str1 .= $line[$x] . "\n";
			$x++;
			
			# remove blank lines...
			$firstFlag=1;
			
			while( ( $line[$x] =~ /^\n/s ) or  ($line[$x] =~ /^$/) )	{
				$x++;
			}
			
			$str1 .= "$line[$x]";
		} else {
			$str1 .= $line[$x];
		}
	}
	
	$rtn = $str1;
	
	$rtn =~ s|__NoTitleNoSubtitle__|\\|g;
	
	return $rtn;
}



#============================
#POD Documentation
#
# To convert this documentation to an html file, type "C:\>pod2html SpecsTriage.pl > SpecsTriage.html" <enter>
# You will probably want to tweak the output a little.
#============================
=head1

=head1

=head1 SpecsTriage.exe Documentation

=head2

=head2 The SpecsTriage.exe is used to do the following:

=head1

=over 4

=item 1.
Read the JobSpecs_config.xml file for UNC paths and settings, \&c.

=head2

=item 2.
Zip all of the files for each Job specified, named as "JobNumber".zip and place it in the config files Backup Path/JobNumber/Date_Time/ folder.



=head2

=item 3.
Based on config filter settings, remove all content from, a given jobs GenCoded files, that is between (and including) the <print_specs>...</print_specs> tags.!

=back

=head2

=head2 Command-line:

=head3

=head3 --------------------------------------------------------------

=head3   WITHOUT OPTIONS

=head3 --------------------------------------------------------------

=head3

=over 8

C:\>SpecsTriage <enter>

The SpecsTriage.exe application will look for the job specification file (JobSpecsTriage.xml) generated via BIS and saved in the location specified in the JobSpecs_config.xml file (<allusers/> element).

Here is part of a JobSpecs_config.xml file:

/*=================================================*/

<?xml version="1.0" encoding="UTF-8"?>

<!-- This file is auto-generated via BIS - DO NOT EDIT! -->

<JobSpecsTriage> 

    <job>
        <account>14131</account>
        <newprocess>yes</newprocess>
        <update_accounts>yes</update_accounts>
    </job>
    <job>
        <account>#99994</account>
        <newprocess>yes</newprocess>
        <update_accounts>no</update_accounts>
    </job>

/*=================================================*/
    
=back

=head3

=head3 --------------------------------------------------------------

=head3   WITH OPTIONS (for a select Job)

=head3 --------------------------------------------------------------

=head3

=over 8

At your DOS prompt, enter one of the following examples, changing the Job number, of course:

C:\>SpecsTriage --job=12345 --new --update


C:\>SpecsTriage --job=12345 --new --noupdate


C:\>SpecsTriage --job=12345 --old --update


C:\>SpecsTriage --job=12345 --old --noupdate

(NOTE:  Remember that you can also add the -d switch to aid debugging.)

=back

=cut
