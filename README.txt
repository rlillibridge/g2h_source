FILE:  README.txt
USE:  G2H Project Folder Information File
Created:  2013-08-19 14:39
By:  Raymond Lillibridge (git version 1.8.3.msysgit.0)
# BitBucket.org 
# MODIFIED:  2013-08-29 11:24
#------------------------------------------------------------------------------
STATUS:  TESTING PURPOSES ONLY!!!
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
NOTE:  
~\<some-folder>\git init  
    Shall be run for the individual folders, NOT at the root of G2H
	per multiple suggestions on using GIT, found on the web

#------------------------------------------------------------------------------
# Semantic Versioning: 
#------------------------------------------------------------------------------
Given a version number MAJOR.MINOR.PATCH.(build), increment the:

MAJOR - version when you make incompatible API changes,
MINOR - version when you add functionality in a backwards-compatible manner, and
PATCH - version when you make backwards-compatible bug fixes.

NOTE:  Additional labels for pre-release and build metadata are available as extensions to the MAJOR.MINOR.PATCH format.
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
G2H_Core.bat (aka:  G2H_Core_RLillibridge.bat)

O U T L I N E
--------------
Calls (about 40 different applications)*.pl (or exe), batch files, XSL, or PowerShell scripts in this order:

GetBannerInfo_xp
SetJobName.bat
XMLPrep
ReviewStructure
G2x
ValidIDs
GCHomeAccountUpdate (live version, not called via 'DEV' version)
HTML_LocalSetup_XP
ValidIMGs
BookXML
book.xsl
XMLStruct2
ImageInfo
(xref\xreffinder (using :  Master\XrefConfig\XrefConfig.xml)     !!!MANAGED VIA DIFFERENT GIT REPOSITORY!!!)
meta.xsl
breadcrumbs.xsl
BookSplit_XML.bat...
    Split_Book_XML.xsl
	Split_Level1_XML.xsl
	Split_Level2_XML.xsl
	Split_Level3_XML.xsl
	Split_Level4_XML.xsl
	Split_Level5_XML.xsl
	Split_Level6_XML.xsl
BookX2H.bat...
    code_BOOK_html.xsl
    code_L1_html.xsl
    code_L2_html.xsl
    code_L3_html.xsl
    code_L4_html.xsl
    code_L5_html.xsl
    code_L6_html.xsl
TOC_MCC.xsl
H2W_BATCH.bat...
    RebuildHTML.exe
TOC_CONTEGRA_TEMP.xsl
TOC_CONTEGRA_nodeKey.xsl
G2H_StepP-chunking.ps1
G2H_StepT-indexing.ps1
G2H_StepT-indexing.ps1 (yes, twice in a row???)
G2H_StepU-Archive.ps1
Municode_Desktop.exe
ZIPALL2.bat




















