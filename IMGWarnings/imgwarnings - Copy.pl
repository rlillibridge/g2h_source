#!/usr/bin/perl
# imgwarnings.pl
# Image Warnings 
#
# Programmer:  David Nichols
my $version = "12-14-2010.1 [BETA]";

######################## VERSION HISTORY #
##########################################

##########################################

############################## PROCEDURE #
##########################################
# 1.)  XPP.bat makes a call to this program
# 1a.) Perl gets username
# 2.)  Read graphic2.txt from User's xpp.xyz folder
# 3.)  Create a lookup array with that info
# 4.)  Call in images from that account's Graphics folder based on array
# 5.)  For each image, gather file data and place into an array)
# 6.)  For each value the @array, check to make sure images are within certain standards
# 7.)  Report to screen any "warnings" about images...
# 7a.) ...This should happen after each image, i.e. one-at-a-time, each image will be called-in, evaluated, and reported upon
# 8.)  insert a pause in XPP.bat so that user must stop and look at any reports
##########################################

################################# PRAGMA #
##########################################

use strict;
use warnings;
no warnings "uninitialized";
use Bundle::Image::Info::Everything;
use Image::Info qw(image_info dim);
use Term::InKey;
use File::Path;

##########################################

################################ GLOBALS #
##########################################

my $acctnum = $ARGV[0];
my $user = getlogin();
my $imgloc = "G:\\Accts\\$acctnum\\Graphics\\";
my $xycache = ($imgloc . ".xycache");
my $myVersion = "Version: (BETA)";
my $graphic2 = "\\\\mcc-file-01\\users\\$user\\xpp.xyz\\graphic2.txt";
my $IMG_out = "\\\\mcc-file-01\\users\\$user\\xpp.xyz\\IMGINFO.temp";
my @imginfo = "";
my @checkfiles = "";
my $info;
my $count = "0";
my $imgwarnings = "0";
my $warnfile = "";
my $warnres = "";
my $filenameRES = "";
my $filenameDIMW = "";
my $filenameDIMH = "";
my $warndimW = "";
my $warndimH = "";
my $result;

##########################################


########################### MAIN ROUTINE #
##########################################

print STDOUT "========================================================\n";
print STDOUT "IMAGE WARNIGS(.pl) V\.$version\n";
print STDOUT "========================================================\n\n";



open (GRAPHICS, $graphic2)
		|| die "cannot open \"GRAPHICS2.txt\" file for output $!\n";

while (<GRAPHICS>) {

#	REMOVES ANY NEEDLESS SPACES IN LOOKUP FILE #
	$_ =~ s|\s||g;
	my $imgfile = ($imgloc . $_);
	
	getinfo($imgfile);
	
	print STDOUT "Preparing images... $count [$_]\n";
}
close (GRAPHICS);


open (IMGINFO, ">$IMG_out");
print IMGINFO "@imginfo";
close (IMGINFO);

open (IMGINFO, "<$IMG_out");

while (<IMGINFO>) {
		
	$_ =~ m/(IMG\[)(.+\..+)(\]=>)(\w)(Resolution=>\t)(\d+)/gi;
	$filenameRES = "$2";
	$warnres = "$6";

	if ($warnres =~ /^[+-]?\d+$/ ) {
	
			if ($warnres > 300 ) {
			compileRESwarnings($filenameRES,$warnres);
		} else {next;}
	}
	
	$_ =~ m/(IMG\[)(.+\..+)(\]=>)(width=>\t)(\d+)/gi;
	$filenameDIMW = "$2";
	$warndimW = "$5";

	if ($warndimW =~ /^[+-]?\d+$/ ) {
		
		if ($warndimW > 2100 ) {
			compileDIMWwarnings($filenameDIMW,$warndimW);
		} else {next;}
	}	

	$_ =~ m/(IMG\[)(.+\..+)(\]=>)(height=>\t)(\d+)/gi;
	$filenameDIMH = "$2";
	$warndimH = "$5";
	
	if ($warndimH =~ /^[+-]?\d+$/ ) {
		
		if ($warndimH > 2700 ) {
			compileDIMHwarnings($filenameDIMH,$warndimH);
		} else {next;}
	}
}

close IMGINFO;


print STDOUT "========================================================\n";
print STDOUT "Image count total:\t$count\n";
print STDOUT "========================================================\n\n";


if ($imgwarnings > 0) {
	
	system("cls");
	
	print STDOUT "\n\nImage warnings:\t[$imgwarnings]\n";
		
	print STDOUT "\n========================================================\n";
	print STDOUT "\t  PRESS ANY KEY TO SEE WARNINGS\n";
	print STDOUT "========================================================\n";
	
	my $x = &ReadKey;
	
	system("cls");
	
	print STDOUT "\n\n========================================================\n";
	print STDOUT "\t\t   IMAGE WARNINGS\n";
	print STDOUT "========================================================\n";	
	
	print STDOUT "\nNOTE:\tThe following images were flagged for any of the following reasons...\n\n";
	print STDOUT "\tResolution more than\t300 dpi\n";
	print STDOUT "\tHeight more than\t2700 pixels\n";
	print STDOUT "\tWidth more than\t\t2100 pixels\n\n";
	print STDOUT "\tEach \"flagged\" image has the potential to adversely affect your processing time.\n\n";
	
	$x = &ReadKey;
	
	print STDOUT "@checkfiles\n\n";
	
	#print STDOUT "\n\tPress \"x\" to cancel processing.\n";
	#print STDOUT "\t(Press any other key to continue...)\n\n\n";
	print STDOUT "\tPress any key to continue processing...\n\n\n";
	$x = &ReadKey;
	
	#if ($x eq "x") {
		
		#print STDOUT "It worked!\n";
		
		#$x = &ReadKey;
		#my $hemlock = killmenow();
	#}

} else {
	print STDOUT "There are ZERO warnings.\n\n\n";
}




############################ SUBROUTINES #
##########################################

sub getinfo {
	
	my $addr = shift;
	
	if ($addr eq $xycache) { 
  	return $addr;
	}
					
	my $reverseaddr = reverse($addr);
	my $extreversed = substr($reverseaddr, 0, 3);
	my $ext = reverse($extreversed);
	
	
	if ($ext eq "tif") {
			$count++;
			$info = image_info($addr);
				$ext ="";
				$result = img_tif_info($info);
				if (my $error = $info->{error}) {
					print STDOUT "Can't parse image info: $error\n";
				} 
		return $addr;
	} elsif ($ext eq "jpg") {
			$count++;
			$info = image_info($addr);
				$ext ="";
				$result = img_jpg_info($info);
				if (my $error = $info->{error}) {
					print STDOUT "Can't parse image info: $error\n";
				}
			return $addr;
	} elsif ($ext eq "JPG") {
			$count++;
			$info = image_info($addr);
				$ext ="";
				$result = img_jpg_info($info);
				if (my $error = $info->{error}) {
					print STDOUT "Can't parse image info: $error\n";
			}
			return $addr;
	} elsif ($ext eq "TIF") {
			$count++;
			$info = image_info($addr);
				$ext ="";
				$result = img_tif_info($info);
				if (my $error = $info->{error}) {
					print STDOUT "Can't parse image info: $error\n";
				}
	} elsif ($ext eq "EPS") {
			$count++;
			return $addr;
	} elsif ($ext eq "eps") {
			$count++;
			return $addr;
  } elsif ($ext eq "gif") {
			$count++;
			return $addr;
	} elsif ($ext eq "GIF") {
			$count++;
			return $addr;
	}
	return $addr;
}


sub img_tif_info {
	
	my $parm1 = shift;
	while( my ($k, $v) = each %$parm1 ) {
		push (@imginfo, "IMG[$_]=>$k=>\t$v\n");
	}
	return $parm1;
}


sub img_jpg_info {
	
	my $parm1 = shift;
	while( my ($k, $v) = each %$parm1 ) {
		push (@imginfo, "IMG[$_]=>$k=>\t$v\n");
	}
	return $parm1;
}


sub compileRESwarnings {
	my $parm1 = shift;
	my $parm2 = shift;
	
	$warnfile = ("WARNING: \"$parm1\":\t" . "the resolution is currently:  " .  $parm2 . "dpi\n");
  $imgwarnings++;
  push (@checkfiles, $warnfile);
  
	return $parm1;
	return $parm2;
}


sub compileDIMWwarnings {
	my $parm1 = shift;
	my $parm2 = shift;
 
  $warnfile = ("WARNING:  \"$parm1\":\t" . "width is currently:   " .  $parm2 . " pixels\n");
  $imgwarnings++;
  push (@checkfiles, $warnfile);

	return $parm1;
	return $parm2;
}


sub compileDIMHwarnings {
	my $parm1 = shift;
	my $parm2 = shift;
		
  $warnfile = ("WARNING:  \"$parm1\":\t" . "height is currently:  " .  $parm2 . " pixels\n");
  $imgwarnings++;
  push (@checkfiles, $warnfile);
  
	return $parm1;
	return $parm2;
}


sub killmenow {
	
	print STDOUT "\n";
	
}