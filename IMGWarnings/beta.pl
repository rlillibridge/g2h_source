#!/usr/bin/perl
# test.pl
# GenCode to XML (schema:  code.xsd) Converter
#
# Programmer:  David Nichols

############################## PROCEDURE #
##########################################
# 1.)  XPP.bat makes a call to this program
# 1a.) Perl gets username
# 2.)  Read graphic1.txt from User's xpp.xyz folder
# 3.)  Create a lookup array with that info
# 4.)  Call in images from that account's Graphics folder based on array
# 5.)  For each image, gather file data and place into another hash (or maybe just an array)
# 6.)  For each Value of each key in image data hash, check to make sure images are within certain standards
# 7.)  Report to screen any conerns about images...
# 7a.) ...This should happen after each image, i.e. one-at-a-time, each image will be called-in, evaluated, and reported upon
# 8.)  insert a pause in XPP.bat so that user must stop and look at any reports
##########################################


 



################################# PRAGMA #
##########################################
use Bundle::Image::Info::Everything;
use Image::Info qw(image_info dim);
##########################################

################################ GLOBALS #
##########################################

$acctnum = $ARGV[0];
$user = getlogin();
$imgloc = "\\\\mcc-file-01\\accounts\\Accts\\$acctnum\\Graphics\\";
my $myVersion = "Version: (BETA)";
$graphic2 = "\\\\mcc-file-01\\users\\$user\\TESTxpp.xyz\\graphic2.txt";
@imginfo;

##########################################



open (GRAPHICS, "$graphic2");
@graphlookup = <GRAPHICS>;
close (GRAPHICS);





foreach $line (@graphlookup) {
	
		$line =~ s|\s||; # removes whitespace from GRAPHIS2.txt

		my $imgfile = ($imgloc . $line);
	
	getinfo($imgfile);
	
}


print STDOUT "@imginfo";


foreach $line (@imginfo) {
	
	$line=~ m|(IMG[)(.+)(])|g;
		$name = $2;
		
}

print STDOUT "\n$name\n";






############################ SUBROUTINES #
##########################################

sub getinfo($imgfile) {

	my $addr = shift;
		
	my $info = image_info($addr);
		if (my $error = $info->{error}) {
  	  	die "Can't parse image info: $error\n";
	}
		push (@imginfo, "\nIMG[$line]");
		
		while( my ($k, $v) = each %$info ) {
			push (@imginfo, $k, $v);

		}
	
	return $addr;
}