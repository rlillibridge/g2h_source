#!/usr/bin/perl
# ValidIDs.pl
# Programmer:  Raymond Lillibridge
my $myVersion = "Version:  2015-04-23.1 (nopause)";
#=============================================	
# WIP:
# 2015-04-23.1 - Raymond Lillibridge - &theta; special character filtered from @ID creation

# CHANGE HISTORY:
# 2013-10-03.1 - Raymond Lillibridge - Allow passing in --nopause option
# 2013-06-04.1 - Raymond Lillibridge - Manage <selectout;ebook;html;none;print> and <selectout out="ebook;html;none;print> tags
# 2013-05-01.1 - Raymond Lillibridge - Build @id for in-line <img.../> elements.
# 20121203.2 - Raymond Lillibridge - frax stuff in title/subtitle -> @ID
# 20121203.1 - Raymond Lillibridge - Convert PIs in title and/or subtitle to "aaa-bbb" for @id
# 20120516.1 - Raymond Lillibridge - Allow for new book, level#, & section @origin attribute
# 20110418.1 - Raymond Lillibridge - For 64bit machines, change use of 'deltree' to 'rmdir /S /Q'
# 20110408.1 - Raymond Lillibridge - brackets in title/subtitle issue
# 20110207.1 - Raymond Lillibridge - adding logic to BuildID_Title() to remove square brackets i.e. "see [Sec.];en;#.\Text."... 
# 20101105.1 - David Nichols - added logic to detect app location as to avoid hard-coding in paths
# 20101001.1 - Raymond Lillibridge - Added creation of ids for table, listitem, img, and para elements
# 20100831.1 - Raymond Lillibridge - added:  <footnoteref .../> 
# 20100730.1 - Raymond Lillibridge - manage fraction chars in titles as decimal numbers?
# 20100726.1 - Raymond Lillibridge - Update POD info and re-generated ValidIDs.html help file
# 20100721.1 - Raymond Lillibridge - Update POD info at the end of this file and regenerated ValidIDs.html
# 20100721.1 - Raymond Lillibridge - Minneapolis, MN T21 (585) has VERY long catch-lines, had to truncate IDs to not exceed 200 char.
# 20100720.1 - Raymond Lillibridge - Corrected: <title>Sections 4-a and 4b.</title>  getting: CHTR_ARTIICOGEPO_S2	-->-A_4BRE
# 20101716.1 - Raymond Lillibridge - Handling duplicate Titles + SubTitles for differently named files:
#		Adding command line switches:  --fnameStart=#  --fnameLength=#  and  "--idposition="#"
#		This will allow the addition of a character(s) from the originating filename to be a prefix, suffix, or unique position in the creation
#		of the <myID/> element.
#
#		Example 1 (Juneau, AK):
#			ValidIDs.exe --fnameStart=-1  --fnameLength=1  --idposition=last  (Negative starts n chars from end)
#
#			filename:  ZCCT1
#			<myID>WHAT_EVER_ENDING_WITH_1</MYID>
# 		---------------------------------------------------
#		Example 2 (Anchorage, AK):
#			ValidIDs.exe --fnameStart=1  --fnameLength=1 --idposition=1
#
#			filename: C02_025
#			<myID>CWHAT_EVER_BEGINNING_WITH_FIRST_CHAR_OF_FILENAME</myID>
# 		---------------------------------------------------
#		Example 3:
#			ValidIDs.exe --fnameStart=2  --fnameLength=2  --idposition=3

#
#			filename:  AXXBCDEF
#			<myID>MYXXWHAT_EVER_WITH_XX_BEGINNING_AT_POSITION_3</MYID>
# 		---------------------------------------------------

# ALSO:  Stubbed out some help POD info at the end of this file.  It needs to be Edited & Generated appropriately.


umask 0;
#=============================================	
# PROCESSING OUTLINE
#=============================================	
#	Makes 'backup' of all *.xml files to ~/BackupXML subfolder
#   Reads $driveletter\apps\CONFIG\ValidIDs.xml (regex patterns) file...
#	Reads "*.xml" files, collecting filename, all title/subtitle pairs
#	Creates ValidIDs.xml file (template in DATA of this application)
#	Builds initial 'IDs' for each entry in the 'ValidIDs.xml' file
#	Validates the 'ValidIDs.xml' file
#	IF NON_VALID 'ValidIDs.xml' file, cycle for changes, &c.
#	Update all input.xml files with newly created IDs.

#=============================================	
# PRAGMA
#=============================================	
use warnings;
# no warnings 'uninitialized';
use Data::Dumper;
use Encode;
use File::Basename;
use File::Listing qw(parse_dir);
use File::chdir;
use File::Copy;
use File::Slurp;

#use Getopt::Std;
use Getopt::Long;

#substr(Filename, $opt_fnameStart, $opt_fnameLength) to use in <myID/> (placed at $idposition)
my $opt_fnameStart=undef;		# --fnamestart=#
my $opt_fnameLength=undef;	# --fnameLength=#

my $opt_idposition=undef;		# --idposition=#		#offset in <myID/> to place the substring above.
my $opt_topause=undef;		# --topause=#
my $opt_h;
my $opt_help;

my $OverrideJobSpecsTriage = 0;	#flag set to 1 if command line options other than -d

GetOptions(
	"fnameStart=s"	=>	\$opt_fnameStart,		
	"fnameLength=s" 	=>	\$opt_fnameLength,
	"idposition=s"		=>	\$opt_idposition,
	"topause=s"		=>	\$opt_topause,
	"h"				=>	\$opt_h,
	"help"			=>	\$opt_help
	);

#=== Win32 ===
use Cwd;
my $CWDSAVED=cwd();
my $WinLoginName = getlogin || getpwuid($<) || "Win Login-UNKNOWN-";

# Date and Time vars
my($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime;
$year += 1900;
my $now_string = localtime;
my $nowYYYYMMDD_HHMMSS= dateTime();
my $LogDateTime =	$nowYYYYMMDD_HHMMSS->{'dateNr'} .  "_" . 
					$nowYYYYMMDD_HHMMSS->{'HHMMSS'};

#for development purposes...
use Term::InKey;


#=============================================	
# GLOBALS
#=============================================
my $username=$WinLoginName;
my $Path=cwd();
my $BackupFolder="BackupXML";
my $BackupPath=$Path . "/" . $BackupFolder;

#	Using location relative to where app is installed -dmn
# my $ValidIDs_config="C:/apps/CONFIG/ValidIDs_config.xml";	# 20101105
my $APPpath = "";
if (!defined $PerlApp::VERSION) {
		$APPpath = $0;
}	else {
		$APPpath = PerlApp::exe();
}
my $myreverse = reverse($APPpath);
my $myoffset = index($myreverse, "\\");
my $string = substr($myreverse, $myoffset);
my $ValidIDs_config = reverse($string) . "CONFIG\\ValidIDs_config.xml";	

# Parent Level State...
my $myLevel1Prefix="";
my $myLevel2Prefix="";
my $myLevel3Prefix="";
my $myLevel4Prefix="";
my $myLevel5Prefix="";
my $mySectionPrefix="";
my $mySubsec1Prefix="";
my $mySubsec2Prefix="";
my $mySubsec3Prefix="";

# 20100930 - counters for @id creation
my $ctr_table=1;
my $ctr_listitem=1;
my $ctr_img=1;
my $ctr_para=1;

my $str1;
my $str2;
my $ctr;
my $debugFlag = 0;

my @FILEHANDLE = (*STDOUT, *LOG);
my $config_LogFile="_ValidIDs_LOG.txt";
my $ValidIDsXML="_ValidIDs.xml";

# Getting drive letter of where app is running from
# my $ValidIDschema = "C:/MCC_XSD/MCC-ValidIDs/validids.xsd"; 20101105
# my $ValidID_Name_ID_xsd="C:/MCC_XSLT/ValidIDs_Name_IDs.xsl"; 20101105
my $driveletter = substr($APPpath,0,2);
my $ValidIDschema = ($driveletter . "/SCHEMA/MCC/XSD/VALIDIDS/validids.xsd"); 
my $ValidID_Name_ID_xsd= ($driveletter . "/XSLT/MCC/ValidIDs_Name_IDs.xsl");	

my $ValidID_Name_IDs_OUTFILE="_ValidIDs_Name_IDs.xml";
my @files=();
my @rxPatterns=();
my $G_infilename = "";

my $rtn;
my $dashes = "----------------------------------------------------------------------";

#=============================================	
# MAIN
#=============================================	
Housekeeping();
Build_ValidIDs_XML();
ValidateFile($ValidIDsXML, $ValidIDschema);
$rtn = XSLTransform($ValidIDsXML, $ValidID_Name_ID_xsd, $ValidID_Name_IDs_OUTFILE);

if($rtn)	{
	printOut("\n");
	UpdateIDsInFiles();
} else {
	printOut("\nWell, something DIDN'T work properly!\n");	
}

ValidIDsExit(0);	#true

#=============================================	
# MAIN END
#=============================================	


#=============================================================================	
# 					S U B R O U T I N E S	
#=============================================================================	
#-------------------------------------------------------------
sub BackupXMLFiles {
#-------------------------------------------------------------
	# global @files should now contain list of *.xml files to backup
	my $me = whoami();
		
	printOut("============================================================================\n");
	printOut("     FILES TO BACKUP:  $CWD\n");	
	printOut("============================================================================\n");

	# print all the filenames in our array
	$ctr=1;
	foreach my $file (@files) {
		if($file =~ /^Book/) {next;}
		if($file =~ /^_/) {next;};
	  	printOut("[" . $ctr++ . "]\t$file\n");
	}

	## Process the files or quit?
	printOut("\n     continue?...\n$dashes\n" . 
		" Y = YES\t\t--OR--\t\t(blank\) or N = CANCEL\n" . 
		"\n$dashes\n");
		
	if($opt_topause ne "nopause") {
		$x = ReadKey();
	} else {
		$x="Y";	
	}
	
	if($x=~ /[Yy]/) {
		# do nothing...
		printOut("\n");	#add a little space before processing the files
	} else {
		do_Error(whoami(), "\...Backup Processing cancelled.\n");
		exit 0 ;
	}

	#Does $BackupFolder exist already, if so, delete it before creating new, empty folder...
	if(-e $BackupFolder)	{
#		$rtn = `deltree /Y $BackupFolder`;	#Changed to 'rmdir' on 20110418.1 for 64 bit machines...
		$rtn = `rmdir /S /Q $BackupFolder`;
		printOut("... $rtn\n");
	}

	if(!-e $BackupFolder)	{
		printOut("Creating $BackupFolder...\n");
		chdir($Path);
		mkdir("$BackupFolder", 0777) or die "+++Can't create $BackupFolder\n$!";
	}

	## Copy the files or quit?
#	printOut("\n     Copy the files to $BackupFolder?...\n----------------------------------------------------------------\n" . 
#		" Y = YES\t\t--OR--\t\t(blank\) or N = CANCEL\n" . 
#		"\n----------------------------------------------------------------\n");
#	$x = &ReadKey;
#	
#	if($x=~ /[Yy]/) {
#		# do nothing...
#		printOut("\n");	#add a little space before processing the files
#	} else {
#		do_Error(whoami(), "\...Backup Processing cancelled.\n");
#		exit 0 ;
#	}

	# Copy files to backup folder...
	$ctr=1;
	foreach my $filebk (@files) {
		if($filebk =~ /^Book/) {next;}
		if($filebk =~ /^_/) {next;};
#		printOut("... copying $filebk to $BackupFolder\n");
		copy("$filebk", "./$BackupFolder") or die "Backup Copy Failed:  $!";
	}
	
	
}


#----------------------------------------------------------
sub BuildID_Title {
#----------------------------------------------------------
	my $IDtitle = shift;
	my $rxT="";
	my $myRTN="";
	my $flagged=0;
			
	if($IDtitle ne "")	{
		# Using titles_regex array entries...
		# @rxPatterns contains entries from $ValidIDs_config
		
		#print STDOUT "\t[DEBUG-title] $IDtitle\n";
		
		# remove square brackets...					#2011-04-08.1
		$IDtitle =~ s|\[||g;
		$IDtitle =~ s|\]||g;

		# remove trailing periods...
		$IDtitle =~ s|^(.*?)(\.{1,99})$|$1|g;
		
		# remove trailing whitespace...
		$IDtitle =~ s|^(.*?)(\s)$|$1|g;
		
		# remove trailing dash(es)
		$IDtitle =~ s|^(.+?)(-{1,99})$|$1|g;

		foreach my $rxp (@rxPatterns)	{
			($rxT, $rxS) = split(/\|/, $rxp, 2);
			$IDtitle = uc($IDtitle);
			$IDtitle =~ s|$rxT|$rxS|g;
		}
		
		$myRTN =$IDtitle;
	}

	return $myRTN;
}


#----------------------------------------------------------
sub BuildID_Subtitle {
#----------------------------------------------------------
	my $IDsubtitle = shift;
	my $rxS="";
	my @subtitleChars = ();
	my $myRTN="";
	
	#...Trim RIGHT FROM �...
	if($IDsubtitle =~ /�/)	{
		$IDsubtitle =~ s|^(.+?)(�)(.+)$|$1|g;		
	}
	
	if($IDsubtitle =~ /-/)	{
		$IDsubtitle =~ s|(.?[^\d])-(.?[^\d])|_|g;		# Sec. 154-67.1.   ???  Trying to KEEP the dash
	}
		
	if($IDsubtitle ne "")	{
		# split subtitle on underscore and then grab first TWO chars of each word, then, concatenate
		@subtitleChars = split(/_/, $IDsubtitle);
		
		foreach my $stchars (@subtitleChars)	{
			if($stchars =~ /^(\d+)/)	{
				$myRTN .= $stchars;
			}else {
				$myRTN .= substr($stchars, 0, 2);	
			}
		}
	}
		
	return $myRTN;
}


#----------------------------------------------------------
sub Build_ValidIDs_XML {
#----------------------------------------------------------
	# The ValidIDs.xml file is used as a workfile that can be validated using an XSLT parser, &c.
	my $me = whoami();
	my $linetemp="";
	my $ctr=0;
	my $prev=0;
	my $tagLevel="";
	my $myTitle="";
	my $mySubtitle="";
	my $myIDtemp="";
	my $myID="";
	my $allContent = "";
	my $ParentPrefix="";
		
	printOut("Creating NEW $ValidIDsXML file:\n");
	open(WORK, ">:utf8", "$ValidIDsXML")	or die "Can't CREATE $ValidIDsXML: $!\n";
	
	print WORK "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	print WORK "<ValidIDs xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:noNamespaceSchemaLocation=\"$driveletter\\SCHEMA\\MCC\\XSD\\VALIDIDS\\validids.xsd\">\n";
	
	foreach my $vid (@files)	{	
		next if($vid eq $ValidIDsXML);
		next if($vid eq $ValidID_Name_IDs_OUTFILE);
		next if(uc(substr($vid , 0, 4)) eq "BOOK");
		next if(uc(substr($vid , 0, 1)) eq "_");
		$linetemp="";
		$tagLevel="";
		
		# initial	my $myTitle="";
		$myTitle = "";
		$mySubtitle = "";
		$myIDtemp = "";
		$myID = "";
		$allContent = "";	
			
		printOut("\n...Processing XML file:  $vid...\n");	
				
		# Check that there are levels, if not, close file.
		$allContent = read_file($vid);
		if($allContent !~ /<level\d/) {
			printOut("\tFILE DOES NOT HAVE ANY LEVELS!  SKIPPING TO NEXT FILE.\n");
			#print STDOUT "\n\n$dashes\n[DEBUG \$allContent]\n$dashes\n$allContent\n$dashes\n\n";
			$allContent = "";
			close(INFIL);
			next;
		}
		
		$G_infilename = $vid;
					
		open INFIL, "<:utf8", "$vid"	or die "Can't OPEN input file:  $vid  $!\n";
		
		print WORK "\t<file>\n\t\t";
		print WORK "<fname>$Path\/$vid<\/fname>";
		

		
		while(<INFIL>)	{
			my($line) = $_;
			chomp($line);			# Strip the trailing newline from the line.
			
			$line =~ s|<title (.+?)>|<title>|g;				
			$line =~ s|<subtitle (.+?)>|<subtitle>|g;				
			
			if(	($line =~ /<level\d/)		|
				($line =~ /<section/)		|
				($line =~ /<subsec\d/)			)	{
									
				$tagLevel = $line;

				# remove PIs...
				$tagLevel =~ s|(<\?)(.+?)(\?>)||g;
				$tagLevel =~ s|^([ \t]*)<(level\d) xmlns:xsi(.+?)$|$2|g;		
				$tagLevel =~ s|^([ \t]*)<(section) xmlns:xsi(.+?)$|$2|g;		
				$tagLevel =~ s|^([ \t]*)<(subsec\d) xmlns:xsi(.+?)$|$2|g;		
				$tagLevel =~ s|<(level\d) (.+?)$|$1|g;		
				$tagLevel =~ s|<(section) (.+?)$|$1|g;		
				$tagLevel =~ s|<(subsec\d) (.+?)$|$1|g;		
				$tagLevel =~ s|^.*([\W])(.+?)|$2|g;
				$tagLevel =~ s|^([ \t\s]*)<(.+?) (.*?)>$|$2|g;
				
				Init_ChildIDPrefix($tagLevel);
			}

			if($line =~ /<table/)	{ 
				$prev = 1; 
			}
			
			if($line =~ /<title\/>/)	{
				$line =	"<title><\/title>";
			}

			# title...
			if( ($line =~ /<title>/) and ($prev > 0) )	{	# IF previous line was a <table...> element
				$prev = 0;
				next;	
			}
			
			if($line =~ /<title>/)	{				
				$lineTemp = "\n\t\t<titleinfo>\n\t\t\t";
				$lineTemp .= "<parent>$tagLevel<\/parent>\n\t\t\t";
				$myTitle = $line;
				
				while($line !~ /<\/title>/)	{
					$line=readline(INFIL);
					chomp($line);
					$myTitle .=  $line;
				}
				
				$lineTemp .= $myTitle;	
													
				# Remove all <anchor.../> tags...
				$lineTemp =~ s|<anchor(.+?)\/>||g;

				# Remove all <footnoteref.../> tags...			#20100831
				$lineTemp =~ s|<footnoteref(.+?)\/>||g;

				print WORK "\t\t\t$lineTemp\n";
				$myTitle = CleanMetadataAndPIs($myTitle);	
			}
			
			# subtitle...
			if($line =~ /<subtitle\/>/)	{
				$line =	"<subtitle><\/subtitle>";
			}
			
			if($line =~ /<subtitle>/)	{
				$mySubtitle = $line;
				
				while($line !~ /<\/subtitle>/)	{
					$line=readline(INFIL);
					chomp($line);
					$mySubtitle .=  $line;
				}
				
				# Remove all <anchor.../> tags...
				$mySubtitle =~ s|<anchor(.+?)\/>||g;

				# Remove all <footnoteref.../> tags...			# 20100831
				$mySubtitle =~ s|<footnoteref(.+?)\/>||g;
				
				print WORK "\t\t\t$mySubtitle\n";
				$mySubtitle = CleanMetadataAndPIs($mySubtitle);				

				# <TitleID>...
				$myIDtemp = BuildID_Title($myTitle);				
				$myID = $myIDtemp;
				# <SubtitleID>...
				$myIDtemp = BuildID_Subtitle($mySubtitle);

				$myID .= "$myIDtemp";				# 2010-07-15

				#=========================================
				# <myID> element...
				#=========================================
				# IMPORTANT!!!  Need to prepend parent IDs!!!
				$myID = PrependParentIDs($tagLevel, $myID);
				
				if($myID =~ /^(\d+)/)  	{	  					# Begins with an integer
					$myID = "n" . $myID;						# IDs must begin with alpha
				}
				
				$myID = FinalCleanupID($myID);
				print WORK "\t\t\t<myID>$myID<\/myID>\n";
				print WORK "\t\t</titleinfo>\n";
			}
						
		} # END... while(<INFIL>)	
		print WORK "\n\t<\/file>\n";
		close(INFIL);
	}
	
	print WORK "<\/ValidIDs>\n";
	close(WORK);
	printOut("DONE!\n");
}


#-------------------------------------------------------------
sub CleanMetadataAndPIs	{
#-------------------------------------------------------------
	#==================================================
	#Cleanup Metacharacters, whitespace trim, and remove 'wrapping element tags':		
	#==================================================
	my $parm = shift;
	my $me = whoami();
	my @aFrax = ();
	my $sFraxc = "";
	my $iFrax = 0;
	my $sTop = "";
	my $sBottom = "";
	my $sResult = "";
	my $ictr = 0;
		
	# strip off element tags
	$parm =~ s|^([ \t\s]*)<title>(.+?)<\/title>$|$2|g;
	$parm =~ s|^([ \t\s]*)<subtitle>(.+?)<\/subtitle>$|$2|g;
	
	# convert 1/4, 1/2, and 3/4 fraction characters to _#-#_
	$parm =~ s|\x{00BC}|_1-4_|g;
	$parm =~ s|\x{00BD}|_1-2_|g;
	$parm =~ s|\x{00BE}|_3-4_|g;
	
#print STDOUT "\t[debug] CleanMetadataAndPIs (???): $parm\n";	
	
	if($parm =~ /<\?xpp fra(.?[cx]);/)
	{
		$sFraxc = $parm;
		$sFraxc =~ s|(<\?xpp fra)(.?[cx])(;)(.+?)>|~~~$1$2$3$4>|g;
#print STDOUT "\t\t(a) [DEBUG] \$sFraxc = $sFraxc\n";		
		@aFrax = split("~~~", $sFraxc);
				
		for(my $i=0; $i<=$#aFrax;$i++)
		{		
			#print STDOUT "\t\t[$ictr]\t$aFrax[$i]\n";
			$ictr++;
			
			if($aFrax[$i] =~ /<\?xpp fra(.?[cx]);(.+?);(.+?)\?>/)	{
				$sTop = $aFrax[$i];
				$sTop =~ s|<\?xpp fra(.?[cx]);(.+?);(.+?)\?>(.*?)$|$2|g;
				$sBottom = $aFrax[$i];
				$sBottom =~ s|<\?xpp fra(.?[cx]);(.+?);(.+?)\?>(.*?)$|$3|g;
#print STDOUT "\t\t(b)[debug] sTop=$sTop\tsBottom=$sBottom\n";				
				$sResult = "_" . $sTop . "-" . $sBottom . "_";
				$aFrax[$i] =~ s|<\?xpp fra(.?[cx]);(.+?);(.+?)\?>(.*?)$|$sResult$4|g;
			}
		}
		
		$sFraxc = "";
		for(my $i=0; $i<= $#aFrax;$i++)
		{
			$sFraxc .= $aFrax[$i];
		}
		
		$parm = $sFraxc;
		
	}
	
#print STDOUT "\t\t-----\n\t$parm\n\n";		
	
	#...<?xpp qa?>...
	$parm =~ s|<\?xpp q[lca]\?>|_|g;

	# Remove in-line elements...
	while($parm =~ /<(.+?)>/)	{
		$parm =~ s|<(.+?)>||g;
	}
	
	# Remove '&amp; ' ...
	while($parm =~ /\&amp; /)	{
		$parm =~ s|\&amp; ||g;
	}
	
	#...Trim LEFT...
	$parm =~ s|^\s+||g;
	
	#...Trim RIGHT...
	$parm =~ s|\s+$||g;
		
	while($parm =~ /\s/)	{	
		$parm =~ s|\s|+|g;
	}
	
	#en-dash, em-dash, quotes, &c.
	$parm =~ s|\x{2014}|--|g;
	
	
	while($parm =~ /\x{2013} | \x{2022} | \x{2018} | \x{2019} | \x{201A} | \x{201B} | \x{201C} | \x{201D}/)	{
		$parm =~ s|.?[\x{2013}\x{2022}\x{2018}\x{2019}\x{201A}\x{201B}\x{201C}\x{201D}]|_|g;
	}
	
	
	#...General Meta character cleanup...
	$str1="\~\!@#\$%\^\*\(\)+=\|;:\?'\/\"";
#	$parm =~ s|.?[~!@\#\$%\^\*\(\)\+=\\\|\;\:\?]|_|g;
	$parm =~ s|[$str1]|_|g;

	#...Space to underscore...
	$parm =~ s|\x{20}|_|g;

	#...Comma to underscore...
	$parm =~ s|,||g;
	
	#...Trim trailing special char...
	$parm =~ s|_+$||g;
	
	# Remove underscore underscore...
	while($parm =~ /__/)	{
		$parm =~ s|__|_|g;
	}

	$parm = uc($parm);

	while($parm =~ /_A_/)	{
		$parm =~ s|_A_|_|g;
	}
	
	while($parm =~ /_S_/)	{
		$parm =~ s|_S_|_|g;
	}
	
	while($parm =~ /_AN_/)	{
		$parm =~ s|_AN_|_|g;
	}
	
	while($parm =~ /_AS_/)	{
		$parm =~ s|_AS_|_|g;
	}

	while($parm =~ /_AT_/)	{
		$parm =~ s|_AT_|_|g;
	}

	while($parm =~ /_BY_/)	{
		$parm =~ s|_BY_|_|g;
	}

	while($parm =~ /_IT_/)	{
		$parm =~ s|_IT_|_|g;
	}
	while($parm =~ /_ITS_/)	{
		$parm =~ s|_ITS_|_|g;
	}
	
	while($parm =~ /_AND_/)	{
		$parm =~ s|_AND_|_|g;
	}

	while($parm =~ /_FOR_/)	{
		$parm =~ s|_FOR_|_|g;
	}

	while($parm =~ /_THE_/)	{
		$parm =~ s|_THE_|_|g;
	}

	while($parm =~ /_TO_/)	{
		$parm =~ s|_TO_|_|g;
	}
	
	while($parm =~ /_ON_/)	{
		$parm =~ s|_ON_|_|g;
	}
	
	while($parm =~ /_OF_/)	{
		$parm =~ s|_OF_|_|g;
	}
	
	while($parm =~ /_OR_/)	{
		$parm =~ s|_OR_|_|g;
	}

	while($parm =~ /_IN_/)	{
		$parm =~ s|_IN_|_|g;
	}
	
	while($parm =~ /_CITY_/)	{
		$parm =~ s|_CITY_|_|g;
	}

	while($parm =~ /_FROM_/)	{
		$parm =~ s|_FROM_|_|g;
	}

	while($parm =~ /_THAN_/)	{
		$parm =~ s|_THAN_|_|g;
	}

	while($parm =~ /_THIS_/)	{
		$parm =~ s|_THIS_|_|g;
	}

	while($parm =~ /_THAT_/)	{
		$parm =~ s|_THAT_|_|g;
	}

	while($parm =~ /_WITH_/)	{
		$parm =~ s|_WITH_|_|g;
	}

	# Remove [ from beginning...
	while($parm =~ /^\[(.+?)$/)	{
		$parm =~ s|^\[(.+?)$|$1|g;
	}

	# Remove ]  from end...
	while($parm =~ /^(.+?)\]$/)	{
		$parm =~ s|^(.+?)\]$|$1|g;
	}

	# Remove underscore from beginning...
	while($parm =~ /^_(.+?)$/)	{
		$parm =~ s|^_(.+?)$|$1|g;
	}

	# Remove underscore  from end...
	while($parm =~ /^(.+?)_$/)	{
		$parm =~ s|^(.+?)_$|$1|g;
	}

	return $parm;
}


#----------------------------------------------------------
sub CreateBackupFolder()	{
#----------------------------------------------------------
	# COPY FILES TO BACKUP Directory if it exist.
	if (-e "$backupFolder") {
		rmtree([$backupFolder]);
		mkdir("$backupFolder", 0777) or die "Can't CREATE $backupFolder:\n$!\n";		
	} else {
		# $backupFolder DOES NOT EXIST...
		mkdir("$backupFolder", 0777) or die "Can't CREATE $backupFolder:\n$!\n";
	}
	
	print LOG "Created $backupFolder subfolder...\n\n";
	print LOG "------------------------------------------------------------\n\n";
}


#-------------------------------------------------------------
sub dateTime {
#-------------------------------------------------------------
	my ($sec,$min,$hour,$mday,$monNr,$year);
	my $now = {};
	#get date and time
	($sec,$min,$hour,$mday,$monNr,$year) = localtime(time());
    	my ($date, $date_nr, $clock);
	my $month=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")[$monNr];
    	$monNr++;
	$monNr = sprintf("%02d",$monNr);
	$mdayNr = sprintf("%02d",$mday);
	$sec = sprintf("%02d",$sec);
	$min = sprintf("%02d",$min);
	$hour = sprintf("%02d",$hour);
	$year= 1900 + $year;
	$now->{'date'} = "$year-$month-$mday";
	$now->{'dateNr'} = "$year$monNr$mdayNr";
	$now->{'time'} = "$hour:$min:$sec";
	$now->{'HHMMSS'} = "$hour$min$sec";
	return($now);		
}


#-------------------------------------------------------------
sub do_Error {
#-------------------------------------------------------------
	my ($mySub, $myMessage) = @_;
	
	printOut("ERROR:  [$mySub]\n\t$myMessage\n");
}


#----------------------------------------------------------
sub FinalCleanupID  {
#----------------------------------------------------------
	my $me = whoami();
	my $myCleanID = shift;
	my $myKey;
	my $metaStr="\~\!@#\$%\^\*\(\)+=\|;:\?\'\/\"�\x{0398}\x{03B8}\x{03D1}\x{03F4}\x{1DBF}"; #The last 5 HEX values are for the theta char vars
	my $fn = "";
	
	my $one_quart=".25";
	my $one_half=".5";
	my $three_quarts=".75";
	my $one_eighth=".125";
	my $three_eighths=".375";
	my $five_eighths=".625";
	my $seven_eighths=".875";
	
#	my $flagged=0;
	
#if($myCleanID =~ /S154-67.1/)	{
#	#PTII_COORENOR_CH154_UT_ARTIII_WA_DIV1_GE_S154-67.1_RAASPRWAMAFIHYIN
#	print STDOUT "[A-debug $me] - \$myCleanID\n$myCleanID\n";
#	$flagged=1;
#}
#	
	#en-dash, em-dash, quotes, &c.
	$myCleanID =~ s|\x{2014}|--|g;
	
	#Remove '�'...
	while($myCleanID =~ /\x{2021}/)	{
		$myCleanID =~ s|\x{2021}||g;
	}	

	#Remove '&'...
	while($myCleanID =~ /\x{0026}/)	{
		$myCleanID =~ s|\x{0026}||g;
	}	
	
	#em-dash, different quote marks
#	$myCleanID =~ s|.?[\x{2013}\x{2022}\x{2018}\x{2019}\x{201A}\x{201B}\x{201C}\x{201D}]|_|g;	#2010-07-15
	$myCleanID =~ s|.?[\x{2013}\x{2022}\x{2018}\x{2019}\x{201A}\x{201B}\x{201C}\x{201D}]||g;	#2010-07-15

	# fraction characters not allowed in @id...
#	$myCleanID =~ s|.?[\x{00BC}\x{00BD}\x{00BE}\x{215B}\x{215C}\x{215D}\x{215E}]||g;	#20100730
	$myCleanID =~ s|\x{00BC}|$one_quart|g;
	$myCleanID =~ s|\x{00BD}|$one_half|g;
	$myCleanID =~ s|\x{00BE}|$three_quarts|g;
	$myCleanID =~ s|\x{215B}|$one_eighth|g;
	$myCleanID =~ s|\x{215C}|$three_eighths|g;
	$myCleanID =~ s|\x{215D}|$five_eighths|g;
	$myCleanID =~ s|\x{215E}|$seven_eighths|g;
		
	#...General Meta character cleanup...
#	$myCleanID =~ s|[$metaStr]|_|g;		#2010-07-15
	$myCleanID =~ s|[$metaStr]||g;		#2010-07-15
	
	#...Space to underscore...
	$myCleanID =~ s|\x{20}||g;		#2010-07-15
	
	#...Comma to underscore...
	$myCleanID =~ s|,||g;
	
	#...Trim trailing special char...
#	$myCleanID =~ s|_+$||g;		#2010-07-15
	$myCleanID =~ s|\x{20}+$||g;
	
#	$myCleanID =~ s|([\x{005B}\x{005D}])|_|g;		# clean open/close square brackets		#2010-07-15
	$myCleanID =~ s|([\x{005B}\x{005D}])||g;		# clean open/close square brackets		#2010-07-15

	# Remove underscore underscore...
	while($myCleanID =~ /__/)	{
		$myCleanID =~ s|__|_|g;
	}

#	$myCleanID =~ s|.?[^0-9a-zA-Z\.-_]|+|g;

#if($flagged)	{
#	print STDOUT "[B-debug $me] - \$myCleanID\n$myCleanID\n";
#}

	# Remove trailing underscores...
	$myCleanID =~ s|(.+?)_+$|$1|g;		# 2010-07-15

#	# "Special" characters from filename?
#	if($opt_fnameStart gt '0'  and  $opt_fnameLength gt '0'  and  $opt_idposition gt '0')	{	
#		print STDOUT "[debug $me] - \$G_infilename=$G_infilename\n";		
#	}
	
	# NOTICE!!!  Due to a problem with very long catch-lines in Minneapolis, MN (11490),
	# the next steps truncate all IDs to a max length of 200 characters.
	if(length($myCleanID) > 200)	{
		$myCleanID = substr($myCleanID, 0, 200);	
	}
	
	$myCleanID =~ s/(.*?)\.$/$1/g;
		
	return $myCleanID;
}


#----------------------------------------------------------
sub Housekeeping	{
#----------------------------------------------------------
	my $me = whoami();
	
	&Clear;		
	$myDT=$LogDateTime;
	$myDT =~ s|(\d\d\d\d)(\d\d)(\d\d)_(\d\d)(\d\d)(\d\d)|$1\/$2\/$3 $4:$5:$6|g;
	
	open(LOG, ">:utf8",  "$CWD\\$config_LogFile")
		or die "Can't CREATE LOG file $config_LogFile: $!\n"; 

	printOut("\nRUNNING:  ValidIDs.exe (pl)\t\t$myVersion\n");
	printOut("Date_Time:\t\t$myDT\n");
	printOut("USER:  $username\n");
	printOut("[debug \$ValidIDs_config=$ValidIDs_config\n");

	printOut("====================================================================================\n");
	
	ValidateParms();
		
	printOut("Ready to process the following folder:\n");
	printOut("*------------------------------------------------------------------------------*\n");
	printOut("\n\$Path=$Path\n\n");

	# create a list of all *.xml files in the current directory
	opendir(DIR, ".");
	@files = grep(/\.xml$/,readdir(DIR));
	closedir(DIR);

	# DEBUG...
	#printOut("$dashes\n$dashes\n$dashes\n\n\n\t[DEBUG]. . . NOT BACKING UP ! ! !  \n\n\n$dashes\n$dashes\n$dashes\n\n\n\n");
	BackupXMLFiles();
	Read_ValidIDs_Config();
}


#-------------------------------------------------------------
sub printOut {
#-------------------------------------------------------------
	my $parm = shift;
	
	foreach $outfile (@FILEHANDLE) {
		print $outfile "$parm";
	}
}


#----------------------------------------------------------
sub Read_ValidIDs_Config()	{
#----------------------------------------------------------
	my $me = whoami();
	my @config = ();
	
	printOut("\nDeleting OLD $ValidIDsXML file...");
	if(-e $ValidIDsXML)	{ unlink $ValidIDsXML; }
	printOut("DONE!\n\n");
		
	eval { # ValidIDs_config.xml exist?...
	     if(!-e ($ValidIDs_config))	{
				do_Error(whoami(), "The $ValidIDs_config file does not exist!");
		}
	};	
	if($@) {
		printOut("\n++++++++++++++++++++\nError:\n$@\n++++++++++++++++++++\n");
		ValidIDsExit(1);
	}

	#========================================================================
	# NOTE:  If the patterns require escaping characters using the '\' character, XML-Simple GETS WACKY!!! 			
	#========================================================================

	open(REGX, "<:utf8", $ValidIDs_config)
		|| die "cannot open file for input:  $ValidIDs_config";

	while(<REGX>)	{
		my($line) = $_;
		chomp($line);			# Strip the trailing newline from the line.

		if(/^(.+?)<rx>/)	{
			$line =~ s|(.+?)<rx>(.+?)<\/rx>|$2|g;	
			push @rxPatterns, $line;
		}
	}

	close(REGX);

#	 printOut("\n--------------------- Dump:  \@rxPatterns ----------------------\n");
#	 my $myDumpRX = Dumper(@rxPatterns);
#	 printOut($myDumpRX);
#	 printOut("\n--------------------- Finished Dump ----------------------\n\n\n");
}


#-------------------------------------------------------------
sub Init_ChildIDPrefix {
#-------------------------------------------------------------
	my $me = whoami();
	my $CurrLevel = shift;
	
	if($CurrLevel eq "level1")	{
		$myLevel1Prefix="";
		$myLevel2Prefix="";
		$myLevel3Prefix="";
		$myLevel4Prefix="";
		$myLevel5Prefix="";
		$mySectionPrefix="";
		$mySubsec1Prefix="";
		$mySubsec2Prefix="";
		$mySubsec3Prefix="";	
	} elsif($CurrLevel eq "level2")	{
		$myLevel2Prefix="";
		$myLevel3Prefix="";
		$myLevel4Prefix="";
		$myLevel5Prefix="";
		$mySectionPrefix="";
		$mySubsec1Prefix="";
		$mySubsec2Prefix="";
		$mySubsec3Prefix="";
	} elsif($CurrLevel eq "level3")	{
		$myLevel3Prefix="";
		$myLevel4Prefix="";
		$myLevel5Prefix="";
		$mySectionPrefix="";
		$mySubsec1Prefix="";
		$mySubsec2Prefix="";
		$mySubsec3Prefix="";		
	} elsif($CurrLevel eq "level4")	{
		$myLevel4Prefix="";
		$myLevel5Prefix="";
		$mySectionPrefix="";
		$mySubsec1Prefix="";
		$mySubsec2Prefix="";
		$mySubsec3Prefix="";		
	} elsif($CurrLevel eq "level5")	{
		$myLevel5Prefix="";
		$mySectionPrefix="";
		$mySubsec1Prefix="";
		$mySubsec2Prefix="";
		$mySubsec3Prefix="";		
	} elsif($CurrLevel eq "section")	{
		$mySectionPrefix="";
		$mySubsec1Prefix="";
		$mySubsec2Prefix="";
		$mySubsec3Prefix="";		
	} elsif($CurrLevel eq "subsec1")	{
		$mySubsec1Prefix="";
		$mySubsec2Prefix="";
		$mySubsec3Prefix="";		
	} elsif($CurrLevel eq "subsec2")	{
		$mySubsec2Prefix="";
		$mySubsec3Prefix="";		
	} elsif($CurrLevel eq "subsec3")	{	
		$mySubsec3Prefix="";		
	} else {
		# do nothing?
	}
}


#-------------------------------------------------------------
sub PrependParentIDs	{
#-------------------------------------------------------------
	my $me = whoami();	
	my ($myTagLevel, $passedID) = @_;
	my $prefixID="";
	
	if($myTagLevel eq "level1")	{
		$myLevel1Prefix = $passedID . "_";
	} elsif($myTagLevel eq "level2")	{
		$myLevel2Prefix = $passedID . "_";
	} elsif($myTagLevel eq "level3")	{
		$myLevel3Prefix = $passedID . "_";
	} elsif($myTagLevel eq "level4")	{
		$myLevel4Prefix = $passedID . "_";
	} elsif($myTagLevel eq "level5")	{
		$myLevel5Prefix = $passedID . "_";
	} elsif($myTagLevel eq "section")	{
		$mySectionPrefix = $passedID . "_";
	} elsif($myTagLevel eq "subsec1")	{
		$mySubsec1Prefix = $passedID . "_";
	} elsif($myTagLevel eq "subsec2")	{
		$mySubsec2Prefix = $passedID . "_";
	} elsif($myTagLevel eq "subsec3")	{
		$mySubsec3Prefix = $passedID;
	} else {
		#do nothing?	
	}
	
	$prefixID = $myLevel1Prefix . $myLevel2Prefix . $myLevel3Prefix . $myLevel4Prefix . $myLevel5Prefix . $mySectionPrefix . 
					$mySubsec1Prefix . $mySubsec2Prefix . $mySubsec3Prefix;
	
	$prefixID =~ s|(.+?)\+$|$1|g;
	return $prefixID;
}


#-------------------------------------------------------------
sub printDebugInfo {
#-------------------------------------------------------------
	my $sText = shift;
	my $parm = shift;
	
	print STDOUT "\n$dashes\n\tDEBUG INFO:  ($sText)\n$dashes\n";
	print STDOUT "$parm\n$dashes\n\n";	
}


#-------------------------------------------------------------
sub TableListImgPara_id_creation {
#-------------------------------------------------------------
	my $item = shift;
	no warnings 'uninitialized';
	my @a_img = ();
	
	if($item =~ /<table/)	{
		$item =~ s|^([ \t]*)<table(.+?)|$1<table id\=\"tbl_$ctr_table\" $2|g;	
		$ctr_table++;
	} elsif($item =~ /<listitem/)	{
		$item =~ s|^([ \t]*)<listitem(.+?)|$1<listitem id\=\"list_$ctr_listitem\" $2|g;	
		$ctr_listitem++;
	} elsif($item =~ /<img/)	{
#		print STDOUT "\n$dashes\n[debug img in \$item:\n$item\n-----\n";
#		$item =~ s|<img|~~~<img|g;
#		@a_img = split('~~~', $item);
#		
#		foreach my $img (@a_img) {
#			print STDOUT "(*) $img\n";
#			
#			if($img =~ /^<img /) {
#				$img =~ s|<img(.+?)|$1<img id\=\"img_$ctr_img\" $2|g;
#			}
#		}
#		
		#$item =~ s|^([ \t]*)<img(.+?)|$1<img id\=\"img_$ctr_img\" $2|g;	
		$item =~ s|<img(.+?)|$1<img id\=\"img_$ctr_img\" $2|g;	
		$ctr_img++;		
	} elsif($item =~ /<para/)	{
		$item =~ s|^([ \t]*)<para(.+?)|$1<para id\=\"para_$ctr_para\" $2|g;	
		$ctr_para++;
	} else {
		# do nothing
	}
	
	use warnings;
	return $item;
}


#-------------------------------------------------------------
sub UpdateIDsInFiles {
#-------------------------------------------------------------
	my $me = whoami();
	my $infileName="";
	my $outfile="";
	my $newID="";
	my $name="";
	my $fpath="";
	my @IDline=();
	my @inputLine=();
	my $lineIN="";
	my $newline="";
	my $d=0;
	my $x=0;

	open(NAMEID, "<:utf8", $ValidID_Name_IDs_OUTFILE)
		|| die "cannot open file for input:  $ValidID_Name_IDs_OUTFILE";

	@IDline = <NAMEID>;
	close(NAMEID);
	
	for($d=0; $d <=$#IDline; $d++)	{
		chomp($IDline[$d]);			# Strip the trailing newline from the line.
		
		# For each file name to process...
		if($IDline[$d] =~ /<file name=/)	{
			@inputLine=();
			close(FOUT);
			
			if(-e $ infileName && -e $outfile)	{
				print STDOUT "\trenamed to: $name\n\n";
				move($outfile, $infileName);
			}

			# Create $output file name...
			$infileName = $IDline[$d];
			$infileName =~ s|(.+?)<file name=\"(.+?)\"(.+?)|$2|g;
			
			($name, $fpath) = fileparse($infileName);
			#Create $outfile name from $infile...
			$outfile=$fpath . "_ID_" . $name;
												
			# Open INPUT file (original XML file)...
			open(IN, "<:utf8", $infileName)
				|| die "cannot open file for input:  $infileName";

			@inXML = <IN>;
			close(IN);

			# Open OUTPUT file (XML file to create)...
			if(-e $outfile)	{ unlink($outfile); }			# delete existing file
			open(FOUT, ">>:utf8", $outfile)
				|| die "cannot open file for output:  $outfile";

			print STDOUT "...Creating:  $outfile...\t";
			
			#Read and write until FIRST <level#/>, <section/>,  or <subsec#> element...		
			$x=0;
			no warnings 'uninitialized';
			until(	($x > $#inXML)				|
					($inXML[$x] =~ /<level\d/) 		|
					($inXML[$x] =~ /<section/)		|
					($inXML[$x] =~ /<subsec\d/)	
					)	{
					chomp($inXML[$x]);
					
					if($inXML[$x] =~ /<table|<listitem|<img|<para/)	{
						$inXML[$x] = TableListImgPara_id_creation($inXML[$x]);	
					}
					print FOUT "$inXML[$x++]\n";
			}
			use warnings;
			
		} elsif ( $IDline[$d] =~ /<myID>/)	{
			$newID = $IDline[$d];
#printDebugInfo("\$newID A", $newID);			
			$newID =~ s|^([ \t]*)<myID>(.+?)<\/myID>|$2|g;
#printDebugInfo("\$newID B", $newID);			
			
			chomp($inXML[$x]);
#printDebugInfo("\$inXML[$x] A", $inXML[$x]);			
#			$inXML[$x] =~ s|^([ \t]*)<level(\d)|<level$2 id=\"$newID\"|g;
#			$inXML[$x] =~ s|^([ \t]*)<section|<section id=\"$newID\"|g;
#			$inXML[$x] =~ s|^([ \t]*)<subsec(\d)|<subsec$2 id=\"$newID\"|g;
#			$inXML[$x] =~ s|<level(\d)|<level$1 id=\"$newID\"|g;
#			$inXML[$x] =~ s|<section|<section id=\"$newID\"|g;
#			$inXML[$x] =~ s|<subsec(\d)|<subsec$1 id=\"$newID\"|g;
			
			$inXML[$x] =~ s|^([ \t]*)<level(\d)|<level$2 id=\"$newID\"|g;
			$inXML[$x] =~ s|^([ \t]*)<section|<section id=\"$newID\"|g;
			$inXML[$x] =~ s|^([ \t]*)<subsec(\d)|<subsec$2 id=\"$newID\"|g;
#printDebugInfo("\$inXML[$x] B", $inXML[$x]);			
			print FOUT "$inXML[$x++]\n";
			
			#Read and write until next <level#/>, <section/>,  or <subsec#> element...
			
			no warnings 'uninitialized';
			until(	($x > $#inXML)					|
					($inXML[$x] =~ /<level\d/) 		|
					($inXML[$x] =~ /<section/)		|
					($inXML[$x] =~ /<subsec\d/)	
					)	{
					chomp($inXML[$x]);
					
					if($inXML[$x] =~ /<table|<listitem|<img|<para/)	{
						$inXML[$x] = TableListImgPara_id_creation($inXML[$x]);	
					}
					print FOUT "$inXML[$x++]\n";
			}
			use warnings;
			 # END if <myID>
		} else {
			# do nothing; read next
		}

	} # END while(<NAMEID>)
	
	close(FOUT);
	
	if(-e $ infileName && -e $outfile)	{
		print STDOUT "\trenamed to: $name\n\n";
		move($outfile, $infileName);
	}
}


#-------------------------------------------------------------
sub Update_LogDateTime{
#-------------------------------------------------------------
	$nowYYYYMMDD_HHMMSS= dateTime();
	$LogDateTime =	$nowYYYYMMDD_HHMMSS->{'dateNr'} .  "_" . $nowYYYYMMDD_HHMMSS->{'HHMMSS'};
}


#-------------------------------------------------------------
sub UpdateXMLfiles {
#-------------------------------------------------------------
	my $me = whoami();	
	
	### VALIDATED ###
	printOut "Congratulations!\n\nDo you want to update all of the XML files' IDs?\n";
	
	## Process the files or quit?
	printOut("\n     continue?...\n$dashes\n" . 
		" Y = YES\t\t--OR--\t\t(any other key = CANCEL)\n" . 
		"\n$dashes\n");
		
	if($opt_topause ne "nopause") {
		$x = ReadKey();
	} else {
		$x="Y";	
	}	

	if($x=~ /[Yy]/) {
		# do nothing...
		printOut("\n");	#add a little space before processing the files
	} else {
		do_Error(whoami(), "\...UpdateXMLfiles Processing cancelled.\n");
		exit 0 ;
	}
		
	# Ok, so update the files...
	
}


#----------------------------------------------------------
sub ValidIDsExit  {
#----------------------------------------------------------
	 $status = shift;
	 my $me = whoami();
	 
	 if($status == 0) {
	 	printOut("\n\n============================================================\n");
		printOut("\t----- FINISHED! -----\n");
	 } else {
		do_Error(whoami(), "----- ERROR!  [\$status=$status]-----\n");
	}
	 
 	printOut("============================================================\n");
	close(LOG);

	exit($status);
}


#----------------------------------------------------------
sub ValidateFile {
#----------------------------------------------------------
	my $V_xml = shift;
	my $V_xsd = shift;
	my $me = whoami();
	
	printOut "\n$dashes\nValidating:  $V_xml\t\tPlease wait...\n\n";
	my $validateRTN = `AltovaXML -validate $V_xml -schema $V_xsd`;
	printOut "$validateRTN\n";
	printOut "$dashes\n";
		
	if($validateRTN =~ /The XML data is valid./i)		{
		UpdateXMLfiles();
	} else {
		### DID NOT VALIDATE ###
		# Need to modify $ValidIDsXML file and then run this routine again...
		printOut "\nOOPS!  VALIDATION FAILED!\n\nPlease edit $V_xml and then\n";
		
		## Process the files or quit?
		printOut("\n     re-validate?...\n$dashes\n" . 
			" Y = YES\t\t--OR--\t\t(any other key = CANCEL)\n" . 
			"\n$dashes\n");
			
		$x = &ReadKey;
		
		if($x=~ /[Yy]/) {
			printOut("\n");	#add a little space before processing the files
			ValidateFile($V_xml, $V_xsd);
		} else {
			do_Error(whoami(), "\...Validation Processing cancelled.\n");
			exit 0 ;
		}
		
	}
	
}


#----------------------------------------------------------
sub ValidateParms {
#----------------------------------------------------------
	if( ($opt_h) or ($opt_help) )	{
	#	print STDOUT "\nFor HELP please enter:  'pod2text ValidIDs.pl' at the command prompt.\n";
		system `ValidIDs.html`;
		exit 0;
	}
	
	# Validate command-line entries:
	# Are ANY of the switches defined?...
	
#	if(
#		(defined $opt_fnameStart) 		|
#		(defined $opt_fnameLength)		|
#		(defined $opt_idposition)				)	{
#			
#		print STDOUT "\nAt least ONE OF THE 3 command line parameters are defined.\n\n";
#		no warnings;
#		
#		if(
#			!(defined $opt_fnameStart) 		and
#			!(defined $opt_fnameLength)	and
#			!(defined $opt_idposition)			) {
#			# ALL THREE are NOT set...
#			print STDOUT "\nERROR:  At least one parameter is defined so ALL 3 NEED TO BE DEFINED.\n" .
#							"\n\t--fnameStart=#  --fnameLength=#  --idposition=#\n\n";
#			
#			system `ValidIDs.html`;
#			exit 0;
#		}
#		use warnings;
#		
##		print STDOUT "\nALL parameters are defined.  (Are they Valid?).\n";
#		
#		# OK, so ALL THREE have some DATA, better check it out...
#		#Validate $opt_fnameStart...
#		if(
#			($opt_fnameStart 	!~ /^\d+/)		|
#			($opt_fnameLength 	!~ /^\d+/)		|
#			($opt_idposition 		!~ /^\d+/)			)	{
#			#	print STDOUT "\nFor HELP please enter:  'pod2text ValidIDs.pl' at the command prompt.\n";
#			print STDOUT "\nALL INPUT parameters are NOT VALID.\n";
#			print STDOUT 	"\n\tfnameStart=$opt_fnameStart\n" .
#							"\n\tfnameLength=$opt_fnameLength\n" .
#							"\n\tidposition=$opt_idposition\n\n";
#							
#				system `ValidIDs.html`;
#				exit 0;
#		}
#		
#		print STDOUT "Input Parameters Seem To Be OK.\n\nPress 'Y' or 'y' to continue processing...\n\n";
#		$x = &ReadKey;
#		
#		if($x=~ /[Yy]/) {
#			# do nothing...
#			printOut("\n");	#add a little space before processing the files
#		} else {
#			do_Error(whoami(), "\... ValidateParms cancelled.\n");
#			exit 0 ;
#		}
#	}	
}


#-------------------------------------------------------------
sub whoami {
#-------------------------------------------------------------
	(caller(1))[3];
}


#-------------------------------------------------------------
sub XSLTransform {
#-------------------------------------------------------------
	my $infile = shift;	
	my $xslfile = shift;
	my $outfile = shift;
	my $err = 0;
		
	if(! -e $infile) {
		printOut "\n\tERROR! ERROR!  ERROR!\t\n\tFile:  $infile\tDOES NOT EXIST!\n\n";	
		$err++;
	};
	
	if(! -e $xslfile) {
		printOut "\n\tERROR! ERROR!  ERROR!\t\n\tFile:  $xslfile\tDOES NOT EXIST!\n\n";	
		$err++;
	};
	
	if($err < 1)	{
		printOut "\n$dashes\nTransforming:  $infile\twith:  $xslfile\n\nPlease wait...\n\n";
		my $XSLTRTN = `AltovaXML -xslt2 $xslfile -in $infile -out $outfile`;
		
		
		printOut "$XSLTRTN\n";
		printOut "$dashes\n";
		return 1;
	} else {
		printOut "\n$outfile NOT CREATED!\n";
		return 0;
	}
}

#POD Documentation
#
# To convert this documentation to an html file, RUN:  $driveletter\>pod2html --title="ValidIDs Documentation"  ValidIDs.pl > ValidIDs.html
# You will probably want to tweak the output a little.
#============================
#=begin html
#
#<div align="center">
#	<img src="MCC_Logo_Contact_Info.jpg" width="256px" />
#</div>
#=end html

__END__

=pod

=head1 ValidIDs.exe Documentation

=head2 DESCRIPTION:

=over 4

This application is used to do generate unique @id='' attributes for each <level#/>, <section/>, or <subsect#/>  in a job's XML files. This is typically run right after all of the GenCode files have been converted via G2x.exe (and they all validate.)


=back

--------------------------------------------------------------

=head2 Command-line WITHOUT OPTIONS:

--------------------------------------------------------------


=over 4

B<C:\>B<ValidIDs.exe> <enter>

The ValidIDs.exe application will do the following:

=item 1.
Some I<Housekeeping>

=over 8

=begin html

<ol type="A">
<li>Open a log file (_ValidIDs_LOG.txt)</li>
<li>Backup original XML files (in the curent folder) to a subfolder called <b>~/BackupXML</b></li>
<li>Read the <b>C:\Apps\CONFIG\ValidIDs_config.xml</b> file.  
This file is used to add/maintain User modifiable regex patterns.  These patterns are of the 'search and replace' type, used to modify a copy of the contents of the &lt;title/&gt; element.<br/>
<br/>
Example:<br/><b>&lt;rx&gt;</b>Chapter|CH<b>&lt;/rx&gt;</b> could be used to convert the copy of a &lt;title/&gt; strings text 'Chapter' to 'CH'<br/><br/></li>
</ol>

=end html

=back 

=item 2.
Build a work file called '_ValidIDs.xml' that is 'validatable' and contains unique id='' attributes.  This file must 'validate' for the processing to continue.

=item 3.
Build a work file called '_ValidIDs_Name_IDs.xml' from the previously validated work file '_ValidIDs.xml' (above).

=item 4.
Update all of the XML job files id='' attributes from the contents of the '_ValidIDs_Name_IDs.xml' file.


=back

--------------------------------------------------------------

=head2 Command-line WITH OPTIONS:

--------------------------------------------------------------

=over 4

B<C:\>B<ValidIDs.exe -help>  or B<-h> <enter>

Displays this 'help' file in the default browser.

=back

=cut