#!usr/bin/perl
#phoenix.pl (1.1)
#Programmer: David Nichols

my $version = "2010-11-02 v1.1";
print STDOUT "Application:  phx (Phoenix)    Version:  $version\n\n";
#######################
##### 	UPDATES		#####
#######################
#
#	V.1.1 - Added FTP step and updated script to no longer include CSS/Jscript.  
#			Also, images are now located at the root of the folder.
#
#######################################################################################


########################
####### PRAGMA #########
########################
use warnings;
use File::Copy::Recursive qw(fcopy rcopy dircopy fmove rmove dirmove);
use File::Path;
use File::Copy;
use Term::InKey;
use File::Find;
#use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
#use Net::FTP;

no warnings 'uninitialized';
########################
####### GLOBALS ########
########################


$acctnum = $ARGV[0];

if (!defined $PerlApp::VERSION) {
		$APPpath = $0;
}	else {
		$APPpath = PerlApp::exe();
}

#my $driveletter = "R:";
my $driveletter = substr($APPpath,0,2);


$path_home = ($driveletter . "\\accts\\$acctnum");
$path_HTML = ($driveletter . "\\accts\\$acctnum\\HTML");
$path_HTMLCust = ($driveletter . "\\accts\\$acctnum\\HTML_Custom");
#$path_CSS = ($driveletter . "\\Master\\Custom\\13485\\MCC_style\mcc.css");
$path_images = ($driveletter . "\\accts\\$acctnum\\HTML_Custom\\images");
$path_index = ($driveletter . "\\accts\\$acctnum\\HTML_Custom\\index\.html");

$path_level_all = ($driveletter . "\\accts\\$acctnum\\HTML_Custom\\level");
$path_level1 = ($driveletter . "\\accts\\$acctnum\\HTML_Custom\\level1");
$path_level2 = ($driveletter . "\\accts\\$acctnum\\HTML_Custom\\level2");
$path_level3 = ($driveletter . "\\accts\\$acctnum\\HTML_Custom\\level3");
$path_level4 = ($driveletter . "\\accts\\$acctnum\\HTML_Custom\\level4");
$path_level5 = ($driveletter . "\\accts\\$acctnum\\HTML_Custom\\level5");

$acctXML = ($path_HTMLCust . "\\" . $acctnum . ".xml");
$Blank = $path_HTMLCust . "\\Blank.html";
$Book = $path_HTMLCust . "\\Book.html";
$ClientID = $path_HTMLCust . "\\ClientID.txt";
$CodeStyle = $path_HTMLCust . "\\Code_style_html.txt";
$indexHTML = $path_HTMLCust . "\\indexSec.html";
$indexSec = $path_HTMLCust . "\\index.html";
$Stylesheet = $path_HTMLCust . "\\MCC_style.css";
$StylesheetOLD = $path_HTMLCust . "\\MCC_style_old.css";
$js = $path_HTMLCust . "\\mcc.js";
$temp = ($driveletter . "\\accts\\$acctnum\\HTML_Custom\\OUT.txt");
my $uploadfile = $acctnum . "-CustomHTML.zip";
$oldzip = ($driveletter . "\\accts\\$acctnum\\$uploadfile");

my @delfiles = ($acctXML, $Blank, $Book, $ClientID, $CodeStyle, $indexHTML, $Stylesheet, $js, $temp, $oldzip, $indexSec, $StylesheetOLD);

my $levellevel="";


#print STDOUT "@delfiles";


########################
######## MAIN ##########
########################


print STDOUT "\n\nBuilding Custom HTML for Phoenix, AZ ($acctnum)!\n\n";
print STDOUT "Assembling files & folders... (may take a moment)\n";


if (-e, $path_HTMLCust) {
	my $remove;
	$remove = rmtree($path_HTMLCust);
	
	print STDOUT "Removing old files... ($remove)\n";
	
	dircopy($path_HTML,$path_HTMLCust);
} else {
	dircopy($path_HTML,$path_HTMLCust);
}

mkdir("$path_home\\HTML_Custom");

unlink(@delfiles);

rename("$path_HTMLCust\\MCC_TOC.html", "$path_HTMLCust\\index.html");

#copy($path_CSS,$path_HTMLCust) or die $!;

dircopy($path_images,$path_HTMLCust);
rmtree($path_images);

#Building temp file with path to each file in level folders...
if (-e, $path_level1) {
	$levellevel = "1";
	ParseLevel();
}

if (-e, $path_level2) {
	$levellevel = "2";
	ParseLevel();
}

if (-e, $path_level3) {
	$levellevel = "3";
	ParseLevel();
}

if (-e, $path_level4) {
	$levellevel = "4";
	ParseLevel();
}

if (-e, $path_level5) {
	$levellevel = "5";
	ParseLevel();
}

open (TEMP, "\\\\mcc-file-01\\folio\\accts\\$acctnum\\HTML_Custom\\TEMP.txt");
@temp = <TEMP>;
close (TEMP);

print STDOUT "\nExecuting Search \& Replace\n\n";

#Opening each level file from TEMP.txt and calling Search and Replace subroutine...
foreach $line (@temp) {
	SandR ($line);
}

#Deleting temp file...
unlink("\\\\mcc-file-01\\folio\\accts\\$acctnum\\HTML_Custom\\TEMP.txt");


#Relocating level files (html)...
if (-e, $path_level1) {
	print STDOUT "	...Movin' junk...(level1)\n";
	dirmove($path_level1,$path_HTMLCust);
}

if (-e, $path_level2) {
	print STDOUT "	...Movin' junk...(level2)\n";
	dirmove($path_level2,$path_HTMLCust);
}

if (-e, $path_level3) {
	print STDOUT "	...Movin' junk...(level3)\n";
	dirmove($path_level3,$path_HTMLCust);
}

if (-e, $path_level4) {
	print STDOUT "	...Movin' junk...(level4)\n";
	dirmove($path_level4,$path_HTMLCust);
}

if (-e, $path_level5) {
	print STDOUT "	...Movin' junk...(level5)\n";
	dirmove($path_level5,$path_HTMLCust);
}

#S&R on index.html...
open (INDEX, "$path_index");
@ind = <INDEX>;
close (INDEX);

foreach $line (@ind) {
		$line=~ s|/level1||g;
		$line=~ s|/level2||g;
		$line=~ s|/level3||g;
		$line=~ s|/level4||g;
		$line=~ s|/level5||g;
		$line=~ s|\.\.\/mcc\.js|\.\/mcc\.js|g;
		$line=~ s|\.\.\/MCC_style\.css|\.\/MCC_style\.css|g;
		$line=~ s|\</head\>\n|<\/head\>\n\<br\><\!--\#include virtual\=\"/SSI/3menustop\.html\"--\>\<br\>|g;
}

open (INDEX, ">$path_index");
print INDEX "@ind";
close (INDEX);

print STDOUT "\n\nDone!\n\n";

#############################
#####	Zipping up files	#####
#############################

#print STDOUT "\nZipping...";

#my $zip = Archive::Zip->new();
#$zip->addTree( "$path_HTMLCust" );
#$zip->writeToFileNamed("$uploadfile");
#print STDOUT "...DONE!!\n\n";


#############################
#####	FTP to phoenix.gov ####
#############################

# THIS IS SSSLLLLLLOOOWWWWWWW......

#print STDOUT "FTP'ing to \"copftp.phoenix.gov\"\n\n";


#$ftp = Net::FTP->new("copftp.phoenix.gov", Debug => 0)
#	or die "Dayum... can't connect to copftp.phoenix.gov: $@";

#$ftp->login("citycode","dwayitis")
#	or die "C'mon, mayyynnn!  Cannot login!", $ftp->message;

#$ftp->cwd("$path_home")
#	or die "Eff... Cannot change working directory ", $ftp->message;

#$ftp->put("$uploadfile")
#	or die "Awww snap!  Put failed ", $ftp->message;

#$ftp->quit;


########################
##### SUBROUTINES ######
########################

sub ParseLevel {
	opendir(LEVEL, "$path_level_all$levellevel");
	my @leveladdr = grep {"/.*+?\.html/"} readdir LEVEL;
	close LEVEL;
	
	foreach $line (@leveladdr) {
		open (OUT, ">>\\\\mcc-file-01\\folio\\accts\\$acctnum\\HTML_Custom\\TEMP.txt");
		print OUT "$path_level_all$levellevel\\$line\n";
		close OUT;
	}
}



sub SandR	{	
	my $line = shift;
	open (ADDR1, "$line");
	@addr1 = <ADDR1>;
	close (ADDR1);

	foreach $line (@addr1) {
		$line =~ s|\.\./level1|\.|g;
		$line =~ s|\.\./level2|\.|g;
		$line =~ s|\.\./level3|\.|g;
		$line =~ s|\.\./level4|\.|g;
		$line =~ s|\.\./level5|\.|g;
		$line =~ s|\.\./images/|\.|g;
		$line =~ s|\.\.\/mcc\.js|\.\/mcc\.js|g;
		$line =~ s|\.\.\/MCC_style\.css|\.\/MCC_style\.css|g;
		$line =~ s|\</head\>\n|<\/head\>\n\<br\><\!--\#include virtual\=\"/SSI/3menustop\.html\"--\>\<br\>|g;
		$line =~ s|\<a class\=\"showURL\" href\=\"javascript:void\(0\)\" onclick\=\"mcc_TrimStringAfter\(location\.href, '#', this\.name\)\"\>||gi;
		$line =~ s|\<a class\=\"showURLs\" href\=\"javascript:void\(0\)\" onclick\=\"mcc_TrimStringAfter\(location\.href, \'\#\', \'.+\)\"\>||gi;
		$line =~ s|\<a class\=\"showURLs\" href\=\"javascript:void\(0\)\" onclick\=\"mcc_TrimStringAfter\(location\.href, '#', this\.name\)\"\>||gi;
		$line =~ s|\<a class\=\"showURL\" href\=\"javascript:void\(0\)\" onclick\=\"mcc_TrimStringAfter\(location\.href, \'\#\', \'.+\)\"\>||gi;
		$line =~ s|\<a class\=\"showURLs\" href\=\"javascript:void\(0\)\" >||gi;
		$line =~ s|\<script type\=\"text/javascript\" src\=\"\./mcc\.js\"\>\</script\>||gi;
		$line =~ s|\<script type\=\"text\/javascript\" src\=\"/scripts\/\w+\.js\"\>\<\/script\>||gi;
		$line =~ s|\<a class\=\"showURLs toplink\" id\=\"(\w+)\" href\=\"java(.+)\>||gi;	
		$line =~ s|\<a class\=\"(\w+)\" href\=\"java(.+)\>||gi;
	}
	
	open (ADDR2, ">$line");
	print ADDR2 "@addr1";
	close (ADDR2);
}