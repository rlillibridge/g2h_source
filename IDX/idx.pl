#!usr/bin/perl
#
#idx.pl [$version]
#
#Programmer: David Nichols
my $version = "2013-03-27.1 (BETA)";

############################################################################
# Change History:
# 2013-03-27.1 Raymond Lillibridge - Removed Term::InKey; replace it with Term::ReadKey; 
# 20110304.1 Raymond Lillibridge - Adding -nopause option
# 20110302.1 Raymond Lillibridge - Setup for passed-in drive parameters
# 20101203.1 D & R - Mapped network drive and swapped-out foreach loop for a while loop on FILEHANDLE
# 20101202.1 Raymond Lillibridge - Allow for -nopause processing

############################################################################
# "Idx.pl" circumvents the need for a user to manually use the dtSearch indexer desktop utility      	 
# to index a recently processed website for the "new" search engine. A user enters three arguments,       
# an account number, the mapped web drive, & a NoPause value, via command line. Idx.pl will then use this
# information to:
# 1.) Determine if this is a new account or if it's simply updating an existing one, and 
# 2.) Query the SQL database for relevant information (e.g. City Name/State) to insert into the dtIndexer data 
#		files located on "web drive"\Indexes. Once that is done it simply calls the dtInderer utility via command line  
# 		and takes the appropriate actions based on whether it is a new index or an existing one. 						 
############################################################################

########################
#    PRAGMA 
########################
use warnings;
use DBI;
#use Term::InKey;
use Term::ReadKey;

########################
#    GLOBALS
########################
# INPUT PARAMETERS...
my $acctnum = shift;
my $webDrive = shift;	# example:  "L:" (live) or "O:" (devlibrary)
my $NoPause = shift;

my $key = q{};

if(!defined $acctnum)	{
	die "ERROR! account number \(first parameter\) is missing.\n $!\n";
}

if(!defined $NoPause)	{
	$NoPause="";	
}

if(!-e $webDrive)	{
	die "ERROR!  web drive \(second parameter\): $webDrive does NOT EXIST.\n $!\n";
}

my $APPpath = "";

if (!defined $PerlApp::VERSION) {
		$APPpath = $0;
}	else {
		$APPpath = PerlApp::exe();
}

my $appDriveLetter = substr($APPpath,0,2);

my $dtindexer = ("\"$appDriveLetter\\APPS_3rdPARTY\\dtIndexer\\dtIndexer\" /i $webDrive\\Indexes\\$acctnum /c /a /o");
my $ixlib = "$webDrive\\Indexes\\ixlib.ilb";
my $match = "<IndexPath>$acctnum</IndexPath>";
my $makenew = "yes";
my $newdir = "$webDrive\\Indexes\\$acctnum";
my $datfile = "$webDrive\\Indexes\\$acctnum\\index_o.ix";
my $typocheck = length($acctnum);


########################
#    MAIN
########################

if($NoPause ne "-nopause")	{
	#&Clear;
	system('cls');
}

print STDOUT "\n\n			[IDX.PL version $version]			\n\n";

#Checking if user entered a valid, 5-digit account#.
if ($typocheck != 5) {
	print STDOUT "\nYou entered $typocheck characters for your account#...\n\nLast I checked you need 5, homie.\n";

#	$x = &ReadKey;
	#  Term::ReadKey  (Perl 5.14.4)...
	print STDOUT "(Press ENTER to continue...)\n";
	ReadMode 0;
	$key = ReadKey(0);
	#print "Key is ", ord($key), "\n";
	ReadMode 0;
	#print "hit enter to exit...\n";
	exit[0];
} else {
	print STDOUT "\n\nProcessing:\n\taccount#\t= $acctnum\n";
	print STDOUT "\tweb drive\t= $webDrive\n";
	print STDOUT "\tNoPause\t\t= $NoPause\n\n";
	print STDOUT "Press any key to continue...\n\n";

	if($NoPause ne "-nopause")	{
#		$x = &ReadKey;
		#  Term::ReadKey  (Perl 5.14.4)...
		print STDOUT "(Press ENTER to continue...)\n";
		ReadMode 0;
		$key = ReadKey(0);
		#print "Key is ", ord($key), "\n";
		ReadMode 0;
		#print "hit enter to exit...\n";
	} 
}


###########################################
# CHECK FOR EXISTING ENTRY IN "ixlib.ilb"
###########################################
#Opening/reading the index lib file to check for existing entry for account#

open (IXLIB, '<', $ixlib) 
		 or die "Cannot open $ixlib $!\n";

while(<IXLIB>) {
	if($_ =~ /$match/) {
		$makenew = "no";
		last;		
	} 
}

close IXLIB;

if ($makenew =~ "yes") {
	&newentry();
} 


########################
#### UPDATING INDEX ####
########################
print STDOUT "Updating index for $acctnum...";
my $indexing = system($dtindexer);
print STDOUT "Finished!\n\n";

exit[0];



########################
##### SUBROUTINES #########
########################
sub newentry() {
	print STDOUT "\nNo index found!\n";
	print STDOUT "Creating new entry for $acctnum...";

	#Querying the database#
	
	my $dsn="dbi:ODBC:BIS";	#system DSN
	my $user="BIS_dbreader";
	my $pw="ReadOnly1";
	my $query="";
	
	#Not all of this info is currently being used...
	$ProductID = "";
	$ProductName = "";
	$Client = "";
	$Address = "";
	$City = "";
	$Zip = "";
	$PhoneAreaCode = "";
	$Phone = "";
	$URL = "";
	$StateID = "";
	$State = "";
	$ClientST = "";
	$BannerInfo = "";
	#...............................................
	
	#PERL DBI CONNECT#
	$connect = DBI->connect($dsn, $user, $pw);
	$connect->{ReadOnly} = 1;
	$connect->{RaiseError} = 1;

	#PREPARE THE QUERY#
	$query = 
			"SELECT P.ProductID, N.ProductName, C.Client, Address, City, Zip, PhoneAreaCode, Phone, URL, S.StateID, S.State, S.Abbreviation, T.bannertext\n" .
			"FROM BIS_PRODUCTS AS P, BIS_PRODUCTNAMES AS N, BIS_CLIENTS AS C, _STATES AS S, vw_ProductBannerText AS T\n" .
			"WHERE P.ProductID = '" . $acctnum . "' AND\n" . 
			"P.ProductNameID = N.ProductNameID AND\n" .
			"P.ClientID = C.ClientID AND C.StateID = S.StateID AND P.ProductID = T.productid";
	
			
	$myqueryresults = $connect->prepare($query)
		or die "$DBI::errstr\n\nCan't prepare query: $query\n$!";
	
	#EXECUTE THE QUERY#
	$myqueryresults->execute()
		or die "$DBI::errstr\n\nCan't execute query: $query\n$!";
		
	#Binding the query results to scalars to be passed back into file contents later#
	$myqueryresults->bind_columns(undef, \$ProductID, \$ProductName, \$Client, \$Address, \$City, \$Zip, \$PhoneAreaCode, \$Phone, \$URL, \$StateID, \$State, \$ClientST, \$BannerInfo)
		or die $!;
	
	my $queryreturn = $myqueryresults->fetch();
	
	#Editing entry in index lib file for new account#
	my $entry = "	<Item>\n		<IndexName>$acctnum - $City $ClientST</IndexName>\n		<IndexPath>$acctnum</IndexPath>\n	</Item>\n</dtSearchIndexLibrary>";
	open (IXLIBNEW, "$ixlib");
	my @ixlibnew = <IXLIBNEW>;
	close (IXLIBNEW);

	foreach $line (@ixlibnew) {
	$line =~ s|</dtSearchIndexLibrary>|$entry|g;
	}

	open (IXLIBNEW, ">$ixlib");
	print IXLIBNEW "@ixlibnew";
	close (IXLIBNEW);

	#Creating new folder in $webDrive\Indexes for account#
	mkdir ($newdir);
	print STDOUT "Finished!\n";
	
	#Editing config file to include proper filters which will be reused by dtindexer#
	open (DATFILE, ">$datfile");
	print DATFILE "\<\?xml version=\"1\.0\" encoding=\"UTF-8\" \?\>\n
	\<dtSearchIndexSettings\>\n
		\<IndexName\>$acctnum - $City $ClientST\</IndexName\>\n
		\<BaseFolderForRelativePaths\>\</BaseFolderForRelativePaths\>\n
		\<Filters\>\</Filters\>\n
		\<ExcludeFilters\>\*\\images\\\* \*\.xml \*\.css book\.html index\.html mcc_toc\.html \*\.js\</ExcludeFilters\>\n
		\<ExtraFiles\>Index_LastUpdate\*\.htm\</ExtraFiles\>\n
		\<DetailedLogging\>0\</DetailedLogging\>\n
		\<CdDocumentsFolder\>\</CdDocumentsFolder\>\n
		\<StoredFields\>\*\</StoredFields\>\n
		\<EnumerableFields\>\</EnumerableFields\>\n
		\<Item\>\n
			\<Name\>\.\.\\\.\.\\HTML\\$acctnum\</Name\>\n
			\<Recursive\>1\</Recursive\>\n
		\</Item\>\n
	\</dtSearchIndexSettings\>\n";
	close DATFILE;
	
	print STDOUT "\nAdding files to index...";
	
		my $dtindexernew = "\"$appDriveLetter\\APPS_3rdPARTY\\dtIndexer\\dtIndexer\" 
										 ##/i $webDrive\\Indexes\\$acctnum /c /a /o";
	## my $dtindexernew = "\C:\\Program Files (x86)\\dtSearch\\bin\\dtIndexer\" ## 
	##									 /i $webDrive\\Indexes\\$acctnum /c /a /o"; ##
		
	print STDOUT "Finished!\n\n";
	print STDOUT "Updating $acctnum...";
	my $indexing = system($dtindexernew);
	print STDOUT "Finished!";
	
	exit[0];	
}
