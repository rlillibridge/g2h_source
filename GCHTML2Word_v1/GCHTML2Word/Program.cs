﻿//************************************************************
// GCHTML2Word.exe
//
//  Programmer:  Raymond Lillibridge
//	DateVersion:  2011-09-12.1 (WIP)
//
// INPUTS:
//	1) HTML file name to convert to Word
//
// USE:
//************************************************************

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace GCHTML2Word {
	class Program {
		static void Main(string[] args) {
			string infile = args[0];	// full path file name  (V:\acctR\#####\RTF\abc.html)
			//string infile = @"V:\_temp\RTF\abc.HTML";
			if (infile == "") {
				Console.WriteLine("\n\n+++ YOU MUST SUPPLY A FULL PATH AND FILE NAME!\\n\n" +
					"(example:  V:\\_temp\\RTF\\abc.html)\n");
			} else {

				if (!ConvertHTML2Word(infile)) {
					Console.WriteLine("\nDANG! Converting " + infile + " Failed!");
				}
			}
		}


		//************************************************************
		// S U B   R O U T I N E S
		//************************************************************

		private static Boolean ConvertHTML2Word(object fileName) {
			object read_only = false;
			object visible = true;
			object isFalse = false;

			string sFN = fileName.ToString();

			//provide a date format for the date we are inserting.
			object datetime = @"DATE \@ ""yyyy/MM/dd hh:mm:ss""";
			object fieldtype = Microsoft.Office.Interop.Word.WdFieldType.wdFieldDate;

			//  the way to handle parameters you don't care about in .NET 
			object missing = System.Reflection.Missing.Value;

			//   Open the document that was chosen by the dialog 
			Microsoft.Office.Interop.Word.ApplicationClass wordApp =
					new Microsoft.Office.Interop.Word.ApplicationClass();

			wordApp.Visible = false;

			Microsoft.Office.Interop.Word.Document aDoc =
			wordApp.Documents.Open(
				ref fileName, ref missing, ref read_only,
				ref missing, ref missing,
				ref missing, ref missing, ref missing, ref missing,
				ref missing, ref missing, ref visible, ref missing,
					ref missing, ref missing, ref missing);

			//wordApp.Documents.Open(
			//    [In] ref object FileName, 
			//    [In, Optional] ref object ConfirmConversions, 
			//    [In, Optional] ref object ReadOnly, 
			//    [In, Optional] ref object AddToRecentFiles, 
			//    [In, Optional] ref object PasswordDocument, 
			//    [In, Optional] ref object PasswordTemplate, 
			//    [In, Optional] ref object Revert, 
			//    [In, Optional] ref object WritePasswordDocument, 
			//    [In, Optional] ref object WritePasswordTemplate, 
			//    [In, Optional] ref object Format, 
			//    [In, Optional] ref object Encoding, 
			//    [In, Optional] ref object Visible, 
			//    [In, Optional] ref object OpenAndRepair, 
			//    [In, Optional] ref object DocumentDirection, 
			//    [In, Optional] ref object NoEncodingDialog, 
			//    [In, Optional] ref object XMLTransform
			//);


			((Microsoft.Office.Interop.Word.Document)aDoc).Activate();

			try {
				//wordApp.ActiveWindow.ActivePane.View.SeekView =
				//    Microsoft.Office.Interop.Word.WdSeekView.wdSeekCurrentPageFooter;

				//wordApp.Selection.Fields.Add(wordApp.Selection.Range,
				//    ref fieldtype, ref datetime, ref isFalse);

				aDoc.SaveAs(sFN + ".doc","doc");
			}
			finally {
				aDoc.Close(ref isFalse, ref missing, ref missing);
				wordApp.Quit(ref isFalse, ref missing, ref missing);
			}

			return true;
		}

		//************************************************************
		private static Boolean H2W(string infile) {
			//************************************************************
			object oMissing = System.Reflection.Missing.Value;
			object oEndOfDoc = "\\endofdoc"; /* \endofdoc is a predefined bookmark */





			//Start Word and create a new document.
			



			/*
			Word._Application oWord;
			Word._Document oDoc;
			oWord = new Word.Application();
			oWord.Visible = true;
			oDoc = oWord.Documents.Add(ref oMissing, ref oMissing,
				ref oMissing, ref oMissing);

			//Insert a paragraph at the beginning of the document.
			Word.Paragraph oPara1;
			oPara1 = oDoc.Content.Paragraphs.Add(ref oMissing);
			oPara1.Range.Text = "Heading 1";
			oPara1.Range.Font.Bold = 1;
			oPara1.Format.SpaceAfter = 24;    //24 pt spacing after paragraph.
			oPara1.Range.InsertParagraphAfter();

			//Insert a paragraph at the end of the document.
			Word.Paragraph oPara2;
			object oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
			oPara2 = oDoc.Content.Paragraphs.Add(ref oRng);
			oPara2.Range.Text = "Heading 2";
			oPara2.Format.SpaceAfter = 6;
			oPara2.Range.InsertParagraphAfter();

			//Insert another paragraph.
			Word.Paragraph oPara3;
			oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
			oPara3 = oDoc.Content.Paragraphs.Add(ref oRng);
			oPara3.Range.Text = "This is a sentence of normal text. Now here is a table:";
			oPara3.Range.Font.Bold = 0;
			oPara3.Format.SpaceAfter = 24;
			oPara3.Range.InsertParagraphAfter();

			//Insert a 3 x 5 table, fill it with data, and make the first row
			//bold and italic.
			Word.Table oTable;
			Word.Range wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
			oTable = oDoc.Tables.Add(wrdRng, 3, 5, ref oMissing, ref oMissing);
			oTable.Range.ParagraphFormat.SpaceAfter = 6;
			int r, c;
			string strText;
			for (r = 1; r <= 3; r++)
				for (c = 1; c <= 5; c++) {
					strText = "r" + r + "c" + c;
					oTable.Cell(r, c).Range.Text = strText;
				}
			oTable.Rows[1].Range.Font.Bold = 1;
			oTable.Rows[1].Range.Font.Italic = 1;

			//Add some text after the table.
			Word.Paragraph oPara4;
			oRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
			oPara4 = oDoc.Content.Paragraphs.Add(ref oRng);
			oPara4.Range.InsertParagraphBefore();
			oPara4.Range.Text = "And here's another table:";
			oPara4.Format.SpaceAfter = 24;
			oPara4.Range.InsertParagraphAfter();

			//Insert a 5 x 2 table, fill it with data, and change the column widths.
			wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
			oTable = oDoc.Tables.Add(wrdRng, 5, 2, ref oMissing, ref oMissing);
			oTable.Range.ParagraphFormat.SpaceAfter = 6;
			for (r = 1; r <= 5; r++)
				for (c = 1; c <= 2; c++) {
					strText = "r" + r + "c" + c;
					oTable.Cell(r, c).Range.Text = strText;
				}
			oTable.Columns[1].Width = oWord.InchesToPoints(2); //Change width of columns 1 & 2
			oTable.Columns[2].Width = oWord.InchesToPoints(3);

			//Keep inserting text. When you get to 7 inches from top of the
			//document, insert a hard page break.
			object oPos;
			double dPos = oWord.InchesToPoints(7);
			oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range.InsertParagraphAfter();
			do {
				wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
				wrdRng.ParagraphFormat.SpaceAfter = 6;
				wrdRng.InsertAfter("A line of text");
				wrdRng.InsertParagraphAfter();
				oPos = wrdRng.get_Information
							   (Word.WdInformation.wdVerticalPositionRelativeToPage);
			}
			while (dPos >= Convert.ToDouble(oPos));
			object oCollapseEnd = Word.WdCollapseDirection.wdCollapseEnd;
			object oPageBreak = Word.WdBreakType.wdPageBreak;
			wrdRng.Collapse(ref oCollapseEnd);
			wrdRng.InsertBreak(ref oPageBreak);
			wrdRng.Collapse(ref oCollapseEnd);
			wrdRng.InsertAfter("We're now on page 2. Here's my chart:");
			wrdRng.InsertParagraphAfter();

			//Insert a chart.
			Word.InlineShape oShape;
			object oClassType = "MSGraph.Chart.8";
			wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
			oShape = wrdRng.InlineShapes.AddOLEObject(ref oClassType, ref oMissing,
				ref oMissing, ref oMissing, ref oMissing,
				ref oMissing, ref oMissing, ref oMissing);

			//Demonstrate use of late bound oChart and oChartApp objects to
			//manipulate the chart object with MSGraph.
			object oChart;
			object oChartApp;
			oChart = oShape.OLEFormat.Object;
			oChartApp = oChart.GetType().InvokeMember("Application",
				BindingFlags.GetProperty, null, oChart, null);

			//Change the chart type to Line.
			object[] Parameters = new Object[1];
			Parameters[0] = 4; //xlLine = 4
			oChart.GetType().InvokeMember("ChartType", BindingFlags.SetProperty,
				null, oChart, Parameters);

			//Update the chart image and quit MSGraph.
			oChartApp.GetType().InvokeMember("Update",
				BindingFlags.InvokeMethod, null, oChartApp, null);
			oChartApp.GetType().InvokeMember("Quit",
				BindingFlags.InvokeMethod, null, oChartApp, null);
			//... If desired, you can proceed from here using the Microsoft Graph 
			//Object model on the oChart and oChartApp objects to make additional
			//changes to the chart.

			//Set the width of the chart.
			oShape.Width = oWord.InchesToPoints(6.25f);
			oShape.Height = oWord.InchesToPoints(3.57f);

			//Add text after the chart.
			wrdRng = oDoc.Bookmarks.get_Item(ref oEndOfDoc).Range;
			wrdRng.InsertParagraphAfter();
			wrdRng.InsertAfter("THE END.");

			//Close this form.
			this.Close();
			*/
			return 1;
		}
	}
}