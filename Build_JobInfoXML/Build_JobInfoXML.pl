#!/usr/bin/perl
# Build_JobInfoXML.pl
# Reads BIS to create the JobInfo.xml file which is used by CodeBank and other apps.
#
# Programmer:  Raymond Lillibridge
my $myVersion = "Version:  2014-01-09.1 (Beta)";

#==========================================================
# WIP:
#==========================================================
# 2014-01-09.1 - Raymond Lillibridge - Changing code to write output to ~/HTML, not ~/XML


#==========================================================
# HISTORY:
#==========================================================
# 2013-09-24.2 - Raymond Lillibridge - Added missing space in output XML
# 2013-09-24.1 - Raymond Lillibridge - Initial application

#==========================================================
# PRAGMA
#==========================================================
use warnings;
use DBI;
use Term::InKey;

use Getopt::Long;
my $opt_job="-empty-";	# --job	job or account_number (product number?)
my $opt_outpath="-empty-";	# --outpath for output file
my $opt_topause="-empty-";# --topause switch value:
my $opt_d="";				#-d	debug switch

GetOptions(
	"job=s" 		=>	\$opt_job,
	"outpath=s"	=>	\$opt_outpath,
	"topause=s"	=>	\$opt_topause,
	"d"			=>	\$opt_d
);

my $job="$opt_job";
my $outpath="$opt_outpath";
my $ToPause="$opt_topause";

# SQL INFO
my $dsn=q/dbi:ODBC:BIS/;		#system DSN
my $user=q/BIS_dbreader/;
my $pwd=q/ReadOnly1/;
my $dbh;	#database handle
my $sth;	#statement handle
my $query="";
my $LatestJobID="-empty-";
my $JobName="-empty-";
my $MaxStartDate="-empty-";
my $LatestAdoptionDate="-empty-";
my $maxTrackingDate="-empty-";


#==========================================================
# GLOBALS
#==========================================================
my $APPpath = "";
if (!defined $PerlApp::VERSION) {
		$APPpath = $0;
}	else {
		$APPpath = PerlApp::exe();
}

my $dashes = "--------------------------------------------------------------------------------";
my $str1 = "";
my $myKey = "";

# Date and Time vars
my($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime;
$year += 1900;
my $now_string = localtime;
my $nowYYYYMMDD_HHMMSS= dateTime();
my $LogDateTime =	$nowYYYYMMDD_HHMMSS->{'dateNr'} .  "_" . 
					$nowYYYYMMDD_HHMMSS->{'HHMMSS'};
my $myDT=$LogDateTime;
$myDT =~ s|(\d\d\d\d)(\d\d)(\d\d)_(\d\d)(\d\d)(\d\d)|$1\/$2\/$3 $4:$5:$6|g;


#==========================================================
# MAIN
#==========================================================
	Housekeeping();
	Build_JobInfoXML_file();	
	print STDOUT "\tFinished building:  $outpath\\JobInfo.xml\n\n";

1;
#==========================================================
#	MAIN END
#==========================================================


#==========================================================
# SUBROUTINES
#==========================================================
#-------------------------------------------------------------
sub Build_JobInfoXML_file	{
#-------------------------------------------------------------
	my $me = whoami();
	my $sqlErr;
	my $myrtn;
	
	$dbh->{ReadOnly} = 1;
	$dbh->{RaiseError} = 1;
	
	# SELECTING MOST CURRENT JOBID 
	$query = 
		"select	top 1 t.jobId, (select jobName from tempSpeedJobNames where jobId = t.JobID) as jobname
		, max(t.start) maxstart 
		from	BIS_JOBS j, BIS_TRACKING t
		where	t.JobID = j.JobID
		and j.ProductID = " . $job . "
		and j.JobID in (select  parentJobId from BIS_EIJOBS) 
		and j.JobNameID in (1,2,3,4,5,12,13,15,16,17,18,19,20,28,30) 
		group by t.jobId
		order by maxstart desc";

	$sth = $dbh->prepare($query)
		 	or die "$DBI::errstr\n\nCan't prepare query: $query\n$!";
		 	
	$sqlErr = $sth->execute
	 	or die "$DBI::errstr\n\nCan't execute query:\n$query\n\n$!\n";
		
	#Bind the results to local variables...
	$sth->bind_columns(undef, \$LatestJobID, \$JobName, \$MaxStartDate)
		or die "$DBI::errstr\n\nCan't bind_columns\n$!\n";
					
	#retrieve values from the result set...
	$str1 =	$sth->fetch();	
	
	# Grab LatestAdoptionDate from bis_recordedjobs
	$query = "select convert(varchar,MAX(r.AdoptionDate),101) from BIS_RECORDEDJOBS rj , BIS_RECORDING r where rj.JobID =" . 
		$LatestJobID . " and r.RecordingID = rj.RecordingID";
	
	$sth = $dbh->prepare($query)
		 	or die "$DBI::errstr\n\nCan't prepare query: $query\n$!";
		 	
	$sqlErr = $sth->execute
	 	or die "$DBI::errstr\n\nCan't execute query:\n$query\n\n$!\n";
		
	#Bind the results to local variables...
	$sth->bind_columns(undef, \$LatestAdoptionDate)
		or die "$DBI::errstr\n\nCan't bind_columns\n$!\n";
	
	#retrieve values from the result set...
	$str1 =	$sth->fetch();	
	
	# grab Ship date of job	
	$query = "select convert(varchar,MAX(Finish),101) from BIS_TRACKING where JobID =$LatestJobID"; 
	$sth = $dbh->prepare($query) || die "$DBI::errstr\n\nCan't prepare query: $query\n$!";
	$sqlErr = $sth->execute;
	$sth->bind_columns(undef, \$maxTrackingDate);
	$str1 =	$sth->fetch();	
	
	#write "LatestJobID and JobName to ../HTML/JobInfo.xml
	open JOBINFO, ">:utf8", "$outpath\\jobInfo.xml"
		|| die "Cannot open file for output: $outpath//jobInfo.xml";
		
	if(!defined $LatestJobID) {$LatestJobID = "";}
	if(!defined $JobName) {$JobName = "";}
	if(!defined $MaxStartDate) {$MaxStartDate = "";}
	if(!defined $LatestAdoptionDate) {$LatestAdoptionDate = "";}
	if(!defined $LatestJobID) {$maxTrackingDate = "";}
	
	if($opt_d ne "") {
		print STDOUT "\nDEBUG:  query results:\n$dashes\n";
		print STDOUT "\t\$LatestJobID=$LatestJobID\n";
		print STDOUT "\t\$JobName=$JobName\n";
		print STDOUT "\t\$MaxStartDate=$MaxStartDate\n";
		print STDOUT "\t\$LatestAdoptionDate=$LatestAdoptionDate\n";
		print STDOUT "\t\$maxTrackingDate=$maxTrackingDate\n";
		print STDOUT "$dashes\n\n";
	}	
	
	print JOBINFO "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" .
			"<jobInfo ProductId=\"$opt_job\" JobId=\"$LatestJobID\" " .
			"PublishDate=\"$LatestAdoptionDate\" JobName=\"$JobName\" MaxTrackingDate=\"$maxTrackingDate\"/>";

	close JOBINFO;
		
	#Close the connection
	$sth->finish();
}


#-------------------------------------------------------------
sub dateTime {
#-------------------------------------------------------------
	my $me = whoami();
	my ($sec,$min,$hour,$mday,$monNr,$year);
	my $now = {};
	#get date and time
	($sec,$min,$hour,$mday,$monNr,$year) = localtime(time());
    	my ($date, $date_nr, $clock);
	my $month=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")[$monNr];
    	$monNr++;
	$monNr = sprintf("%02d",$monNr);
	$mdayNr = sprintf("%02d",$mday);
	$sec = sprintf("%02d",$sec);
	$min = sprintf("%02d",$min);
	$hour = sprintf("%02d",$hour);
	$year= 1900 + $year;
	$now->{'date'} = "$year-$month-$mday";
	$now->{'dateNr'} = "$year$monNr$mdayNr";
	$now->{'time'} = "$hour:$min:$sec";
	$now->{'HHMMSS'} = "$hour$min$sec";
	
	return($now);		
}


#-------------------------------------------------------------
sub DisplayParms	{
#-------------------------------------------------------------
	my $me=whoami();
	
	print STDOUT "\t\$opt_job=$opt_job\n";
	print STDOUT "\t\$opt_outpath=$outpath\n";
	print STDOUT "\t\$ToPause=$ToPause\n";
	print STDOUT "\t\$opt_d=$opt_d\n";
	print STDOUT "\n(Press any key to continue...\n\n";

	$myKey = &ReadKey;
}
	

#-------------------------------------------------------------
sub Housekeeping	{
#-------------------------------------------------------------
	&Clear;
	print STDOUT "$dashes\n";
	print STDOUT "$0\t\t$myVersion\n";
	print STDOUT "$dashes\n\n";

	if($opt_d ne "") { DisplayParms(); }
		
	if($opt_job eq "-empty-")	{
		print STDOUT "\n\n\t----------\n\t  ERROR!\n\t----------\n\n\tPlease enter an ACCOUNT (ProductID).\n\n";
		print STDOUT "\tExample:  $0 --job=12345 --outpath=\"V:\\AcctsR\\12345\\HTML\"\n\n";
		exit(1);	
	}
	
	if($opt_outpath eq "-empty-")	{
		print STDOUT "\n\n\t----------\n\t  ERROR!\n\t----------\n\n\tPlease enter an output location.\n\n";
		print STDOUT "\tExample:  $0 --job=12345 --outpath=\"V:\\AcctsR\\12345\\HTML\"\n\n";
		exit(1);	
	}
	
#	Connect to the data source and get a handle for that connection.
	$dbh = DBI->connect($dsn, $user, $pwd)  
		or die "Can't connect to $dsn: $DBI::errstr\n$!";			
}


#-------------------------------------------------------------
sub whoami {
#-------------------------------------------------------------
	(caller(1))[3];
}
