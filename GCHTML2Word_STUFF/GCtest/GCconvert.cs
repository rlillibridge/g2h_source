﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using Microsoft.Office.Interop.Word;

namespace GCconvert {
	class Program {
		static void Main(string[] args) {
			// Convert Input.html into Output.doc
			Convert(@"V:\_temp\RTF\abc.html", @"V:\_temp\RTF\abc.doc", WdSaveFormat.wdFormatDocument);
		}

		// Convert an HTML to Word .doc
		public static void Convert(string input, string output, WdSaveFormat format) {
			// Create an instance of Word.exe
			Word._Application oWord = new Word.Application();

			// Make this instance of word invisible (Can still see it in the taskmgr).
			//oWord.Visible = false;

			// Interop requires objects.
			object oMissing = System.Reflection.Missing.Value;
			object isVisible = true;
			object readOnly = false;
			object oInput = input;
			object oOutput = output;
			object oDocFormat = WdSaveFormat.wdFormatDocument;
			object oConfirmConv = true;

			// Load a document into our instance of word.exe
			Word._Document oDoc = oWord.Documents.Open(ref oInput, oConfirmConv, ref readOnly, ref oMissing, 
					ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, 
					WdOpenFormat.wdOpenFormatWebPages, 
					ref oMissing, ref isVisible, ref oMissing, ref oMissing, ref oMissing, ref oMissing);

			// Make this document the active document.
			oDoc.Activate();

			// Save this document in Word 2003 format.
			oDoc.SaveAs(ref oOutput, ref oDocFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, 
					ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, 
					ref oMissing, ref oMissing);

			// Always close Word.exe.
			oWord.Quit(ref oMissing, ref oMissing, ref oMissing);
		}
	}
}