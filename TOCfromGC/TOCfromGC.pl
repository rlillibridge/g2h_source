#!/usr/bin/perl
#Author: LaRae Chasteen

#=============================================	
# PROCESSING OUTLINE
#=============================================	
# Accept path to folder where gen-coded files are
#	If it doesn't already exist, create file ATOC.txt
# Loop through all *. files in the folder, in alphabetical order.
# Copy all ;oh0; through ;oh4; entries into ATOC.txt
# Add specs, tweaks.

#=============================================	
# PRAGMA
#=============================================	
use strict;
use warnings;
use Data::Dumper;
use Term::InKey;
# use Switch; # EXE gives switch error. Perlmonks say too many caveats with Switch.pm; use if / elsif instead.

#=============================================
# MAIN
#=============================================

my $dir = shift;
my $program_name = "TOCfromGC";
my $program_message = "\n=====================================\n\t$program_name\n=====================================\n\n";
my $qStart = "\n\n\n============ /// QUESTION \\\\\\ ============\n\n";
my $qEnd = "\n============ \\\\\\ QUESTION /// ============\n\n";
my $line = q{};
my $tocName;
my $kcPart = q{};
my $kcSubpart = q{};
my $kcTitle = q{};
my $kcChapter = q{};
my $kcArticle = q{};
my $kcDivision = q{};
my $kcSubdivision = q{};
my $kcAppendix = q{};
my $abrPart = q{};
my $abrSubpart = q{};
my $abrTitle = q{};
my $abrChapter = q{};
my $abrArticle = q{};
my $abrDivision = q{};
my $abrSubdivision = q{};
my $abrAppendix = q{};

&Clear;

my $rtn = MakeTOC();
# my $status = system ("Notepad $tocName");

#=============================================
sub MakeTOC {
#=============================================

# Does ATOC.txt already exist in the specified folder?
$tocName = $dir . "\\ATOC.TXT";
if (-e $tocName) {
	# print STDOUT $program_message;
	print STDOUT $qStart;
	print STDOUT $tocName . " already exists!\n\n";
	print STDOUT "Do you wish to overwrite $tocName?\n\n";
	print STDOUT "\tPress (Y)es or any other key to cancel processing. \n";
	
	my $x = &ReadKey;

	print STDOUT "\n";
		if($x =~ /[Yy]/) {
			print STDOUT "Deleting " . $tocName . ".\n";
			# Delete atoc
			unlink $tocName;
		} else {
			print STDOUT "\n\...Processing cancelled.\n";
			exit 0 ;
		}
	}
	print STDOUT "\nCreating " . $tocName . "\n";
	open TOC, ">", $tocName or die $!;
	UserInput();
	AddSpecs();
	GetFiles();
	close TOC;
	print STDOUT "\nFinished creating " . $tocName . "\n";
	return 0;
}

#=============================================
sub EvaluateInput  {
#=============================================

# arg 1 = which scalar am I setting & checking?
# arg 2 = what are the valid values? (string)

	my $uReply  = $_[0];
	my $legalChars = $_[1];
	
	while ($uReply !~ /[$legalChars]/) {
		print STDOUT "\n";
		$uReply = &ReadKey;
		print STDOUT "\n\tYou replied with '" . $uReply . "'.\n";
		print STDOUT "\n";
		if ($uReply !~ /[$legalChars]/) {
			print STDOUT "\n\t...Oops! Please respond with one of these choices: " ."$legalChars\n\n";
		}
	}

	return $uReply;

}
#=============================================
sub UserInput {
#=============================================

	print STDOUT "\n\n\n";	
	print STDOUT "\n   ========================================= ";
	print STDOUT "\n  | This is the default abbreviation and    |";
	print STDOUT "\n  |  capitalization for TOC entries.        |";
	print STDOUT "\n  |.........................................|";
	print STDOUT "\n  |                                         |";
	print STDOUT "\n  |    PART #. UPPERCASE                    |";
	print STDOUT "\n  |                                         |";
	print STDOUT "\n  |    SUBPART #. UPPERCASE                 |";
	print STDOUT "\n  |                                         |";
	print STDOUT "\n  |    TITLE #. UPPERCASE                   |";
	print STDOUT "\n  |                                         |";
	print STDOUT "\n  |    #. Key Capped (Chapter)              |";
	print STDOUT "\n  |                                         |";
	print STDOUT "\n  |    Art. #. Key Capped                   |";
	print STDOUT "\n  |                                         |";
	print STDOUT "\n  |    Div. #. Key Capped                   |";
	print STDOUT "\n  |                                         |";
	print STDOUT "\n  |    Subdiv. #. Key Capped                |";
	print STDOUT "\n  |                                         |";
	print STDOUT "\n  |    Apx. #. Key Capped                   |";
	print STDOUT "\n  |                                         |";
	print STDOUT "\n   ========================================= ";
	
	print STDOUT $qStart;
	print STDOUT "Do you wish to customize the default abbreviation and capitalization formats?\n";
	print STDOUT "\n\tPress (Y)es, (N)o or any other key to cancel processing.\n";
	my $customizeResponse = &ReadKey;
	print STDOUT "\n";

	if ($customizeResponse =~ m/[Nn]/) {
		#set defaults for abbreviations
		$abrPart = "l";
		$abrSubpart = "l";
		$abrTitle = "l";
		$abrChapter = "r";
		$abrArticle = "a";
		$abrDivision = "a";
		$abrSubdivision = "a";
		$abrAppendix = "a";
		# set defaults for key-caps
		$kcPart = "u";
		$kcSubpart = "k";
		$kcTitle = "k";
		$kcChapter = "k";
		$kcArticle = "k";
		$kcDivision = "k";
		$kcSubdivision = "k";
		$kcAppendix = "k";

		return 0;
	}
	elsif ($customizeResponse =~ m/[Yy]/) {
		print STDOUT "\n\...Processing cancelled.\n";
		exit 0;
	}
	else {
		# any other key... cancel processing 
		print STDOUT "\n\...Processing cancelled.\n";
		exit 0;
	}

	print STDOUT $qStart;
	print STDOUT "Do you wish to deviate any title ABBREVIATIONS from their default format?\n";
	print STDOUT "\n\tPress (Y)es, (N)o or any other key to cancel processing.\n";
	my $abrResponse = &ReadKey;
	print STDOUT "\n";
	
	print STDOUT "\nYou replied with $abrResponse\n";
	
	if ($abrResponse =~ m/[Nn]/) {
		#fall through to next question
	}
	elsif ($abrResponse =~ m/[Yy]/) {

		print STDOUT "\n";

		print STDOUT "\nPart: (l)eave as is, (r)emove, (d)efault choice or (a)bbreviate as 'Pt.'?\n";
		$abrPart = EvaluateInput ($abrPart, "LlRrDdAa");
		if ($abrPart eq "d") { $abrPart = "l"; } #default for PART is: leave as is.

		print STDOUT "\nSubpart: (l)eave as is, (r)emove, (d)efault choice or (a)bbreviate as 'Subpt.'?\n";
		$abrSubpart = EvaluateInput ($abrSubpart, "LlRrDdAa");
		if ($abrSubpart eq "d") { $abrSubpart = "l"; } #default for SUBPART is: leave as is.

		print STDOUT "\nTitle: (l)eave as is, (r)emove, (d)efault choice or (a)bbreviate as 'Tit.'?\n";
		$abrTitle = EvaluateInput ($abrTitle, "LlRrDdAa");
		if ($abrTitle eq "d") { $abrTitle = "l"; } #default for TITLE is: leave as is.

		print STDOUT "\nChapter: (l)eave as is, (r)emove, (d)efault choice or (a)bbreviate as 'Ch.'?\n";
		$abrChapter = EvaluateInput ($abrChapter, "LlRrDdAa");
		if ($abrChapter eq "d") { $abrChapter = "r"; } #default for Chapter is remove.

		print STDOUT "\nArticle: (l)eave as is, (r)emove, (d)efault choice or (a)bbreviate as 'Art.'?\n";
		$abrArticle = EvaluateInput ($abrArticle, "LlRrDdAa");
		if ($abrArticle eq "d") { $abrArticle = "a"; } #default for Article is: abbreviate.

		print STDOUT "\nDivision: (l)eave as is, (r)emove, (d)efault choice or (a)bbreviate as 'Div.'?\n";
		$abrDivision = EvaluateInput ($abrDivision, "LlRrDdAa");
		if ($abrDivision eq "d") { $abrDivision = "a"; } #default for Division is: abbreviate.

		print STDOUT "\nSubdivision: (l)eave as is, (r)emove, (d)efault choice or (a)bbreviate as 'Subdiv.'?\n";
		$abrSubdivision = EvaluateInput ($abrSubdivision, "LlRrDdAa");
		if ($abrSubdivision eq "d") { $abrSubdivision = "a"; } #default for Subdivision is abbreviate.

		print STDOUT "\nAppendix: (l)eave as is, (r)emove, (d)efault choice or (a)bbreviate as 'Apx.'?\n";
		$abrAppendix = EvaluateInput ($abrAppendix, "LlRrDdAa");
		if ($abrAppendix eq "d") { $abrAppendix = "a"; } #default for subdivision is abbreviate.
	}
	else {
		# any other key... cancel processing 
		print STDOUT "\n\...Processing cancelled.\n";
		exit 0;
	}

	print STDOUT "$qStart";
	print STDOUT "Do you wish to deviate any subtitles from their default CAPITALIZATION style?\n";
	print STDOUT "\n\tPress (Y)es, (N)o or any other key to cancel processing.\n";
	
	my $kcResponse = &ReadKey;
	print STDOUT "\n";
	
	if ($kcResponse =~ m/[Nn]/) {
		#fall through
	}
	elsif ($kcResponse =~ m/[Yy]/) {
		print STDOUT "\n";

		print STDOUT "\nPart: (l)eave as is, (k)ey-cap, (u)ppercase or (d)efault choice?\n";
		$kcPart = EvaluateInput $kcPart, "LlKkUuDd";
		if ($kcPart eq "d") { $kcPart = "u"; } #default for part is: leave as is.

		print STDOUT "\nSubpart: (l)eave as is, (k)ey-cap, (u)ppercase or (d)efault choice?\n";
		$kcSubpart = EvaluateInput $kcSubpart, "LlKkUuDd";
		if ($kcSubpart eq "d") { $kcSubpart = "k"; } #default for subpart is: leave as is.

		print STDOUT "\nTitle: (l)eave as is, (k)ey-cap, (u)ppercase or (d)efault choice?\n";
		$kcTitle = EvaluateInput $kcTitle, "LlKkUuDd";
		if ($kcTitle eq "d") { $kcTitle = "k"; } #default for Title is: leave as is.

		print STDOUT "\nChapter: (l)eave as is, (k)ey-cap, (u)ppercase or (d)efault choice?\n";
		$kcChapter = EvaluateInput $kcChapter, "LlKkUuDd";
		if ($kcChapter eq "d") { $kcChapter = "k"; } #default for chapter is: key-cap.
		
		print STDOUT "\nArticle: (l)eave as is, (k)ey-cap, (u)ppercase or (d)efault choice?\n";
		$kcArticle = EvaluateInput $kcArticle, "LlKkUuDd";
		if ($kcArticle eq "d") { $kcArticle = "k"; } #default for article is: key-cap.

		print STDOUT "\nDivision: (l)eave as is, (k)ey-cap, (u)ppercase or (d)efault choice?\n";
		$kcDivision = EvaluateInput $kcDivision, "LlKkUuDd";
		if ($kcDivision eq "d") { $kcDivision = "k"; } #default for division is key-cap.

		print STDOUT "\nSubdivision: (l)eave as is, (k)ey-cap, (u)ppercase or (d)efault choice?\n";
		$kcSubdivision = EvaluateInput $kcSubdivision, "LlKkUuDd";
		if ($kcSubdivision eq "d") { $kcSubdivision = "k"; } #default for subdivision is key-cap.

		print STDOUT "\nAppendix: (l)eave as is, (k)ey-cap, (u)ppercase or (d)efault choice?\n";
		$kcAppendix = EvaluateInput $kcAppendix, "LlKkUuDd";
		if ($kcAppendix eq "d") { $kcAppendix = "k"; } #default for appendix is key-cap.
	}
	else {
		# any other key... cancel processing 
		print STDOUT "\n\...Processing cancelled.\n";
		exit 0;
	}
}
#=============================================
sub GetFiles {
#=============================================

my $follows_head = 0;

print STDOUT "\nGetting files...\n";

opendir(DIR, $dir) or die $!;

while (my $file = readdir(DIR)) {

  # We only want files (not folders)
  next unless (-f "$dir/$file");

  # Use a regular expression to find files that don't have a period in their name AND are not named ATOC
  next if ($file =~ m/\./ | $file =~ m/[Aa][Tt][Oo][Cc]/);
  
  my $wholename = $dir . "\\" . $file;
	
	open FILE, "<", $wholename or die $!;
	
	# Clear at beginning of each new file
	my $prefolio = "";
	my $folio = "0";

	# Print blank line to TOC between each new file's entries
	print TOC "\n";

	my $activeHead = "0";

	while(<FILE>)	{
		$line = $_;
		
		# strip leading and trailing whitespace
		$line =~ s/(?:^ +)||(?: +$)//g;
		
		# get first instance of <prefolio> in file, if one exists
		if ($line =~ m/\<prefolio;(.*?)\>/) {
			$prefolio = $1;
		}

		# get first instance of <setpage> or <setllpg> in file, if one exists
		if ($line =~ m/\<setpage;(.*?)\>/) {
			$folio = $1;
		} elsif ($line =~ m/\<setllpg;([0-9]*);?([0-9]*);?([0-9]*)(.*?)\>/){
			$folio = $1;
			my $folio2 = $2;
			my $folio3 = $3;
			# If multi-part page number used with setllpg, then rebuild page number using periods as divider.
			if ($folio2 ne "" && $folio2 ne "0") {
				$folio = $folio . "." . $folio2;
				if ($folio3 ne "" && $folio3 ne "0") {
					$folio = $folio . "." . $folio3;
				}
			}
		}

		if ($follows_head == 1) {
			$line = RemoveTags($line);
			$line = ChangeLine($line);
			chomp($line);
			# ;oh0;'s do not get page numbers. All other heads do get page numbers.
			if ($activeHead ne "0") {
				$line = $line . "\\" . $prefolio . $folio . "\n";
			} else {
				$line = $line . "\n";
			}
			print TOC $line;
			$follows_head = 0;
		}
		if ($line =~ m/;oh([0-4]);/) {
			$activeHead = $1;
			# print STDOUT "\n\$activeHead: $activeHead\n";
			# remove ALL tags from ;oh#; lines
			$line =~ s|\<.*?\>||g;
			# print line to TOC file handle
			print TOC $line;
			$follows_head = 1;
		} else {
			$follows_head = 0;
		}
	}
}

closedir(DIR);
return 0;
}

#=============================================
sub RemoveTags {
#=============================================

	my $loc_Var = shift;
	
	$loc_Var =~ s|;anchor;||g;
	$loc_Var =~ s|;noanchor;||g;

	$loc_Var =~ s|;eol;| |g;
	$loc_Var =~ s|;q[clra];| |g;
	$loc_Var =~ s|\<q[clra]\>| |g;

	$loc_Var =~ s|;c;| |g;

	$loc_Var =~ s|;[\\]??[bi];||g;
	$loc_Var =~ s|;[\\]??ul;||g;
	$loc_Var =~ s|;[\\]??ul=[0-9];||g;
	$loc_Var =~ s|\<[\\]??[bi]\>||g;
	$loc_Var =~ s|\<\\]??ul\>||g;
	$loc_Var =~ s|\<[\\]??ul=[0-9]\>||g;

	$loc_Var =~ s|\<fv;[0-9]\>||g;
	$loc_Var =~ s|;fv=[0-9];||g;
	
	$loc_Var =~ s|\<le;[0-9]{1,2}[pq]\>||g;
	$loc_Var =~ s|;le=[0-9]{1,2}[pq];||g;

	$loc_Var =~ s|\<sz;[0-9]{1,2}[pq]\>||g;
	$loc_Var =~ s|;sz=[0-9]{1,2}[pq];||g;

	# sz xymacro can accept size and leading arguments
	$loc_Var =~ s|\<sz;[0-9]{1,2}[pq];[0-9]{1,2}[pq]\>||g;

	$loc_Var =~ s|\<lp;[0-9]{1,2}[pq]\>||g;
	$loc_Var =~ s|\;lp=[0-9]{1,2}[pq];||g;

	$loc_Var =~ s|\<adv;[0-9]{1,2}[pq]\>||g;
	$loc_Var =~ s|;adv=[0-9]{1,2}[pq];||g;

	$loc_Var =~ s|\<setgtext;.*?\>||g;
	$loc_Var =~ s|\<pregtext;.*?\>||g;
	
	$loc_Var =~ s|\<.*?\>||g; #any other tags


	return $loc_Var;

}
#=============================================
sub ChangeLine {
#=============================================

	my $str = shift;
	
	# String is supposed to have a backslash in it. But if it doesn't, add one at beginning of line so the TOC will work.
	if ($str !~ m/\\/) {
		$str = "\\" . $str;
	}

	# If portion before period is not blank, and ends with something other than a period, then add the period right before the backslash.
	# (example of when this is needed: Chapter number should be followed by a period.)
	$str =~ s|^(.*?[^\.])\\(.*?)$|$1\.\\$2|;
		
	# if user chose to key-cap a heading AND current line is that heading, then do it.
	if ($kcPart eq "k" && $str =~ m/^P[Aa][Rr][Tt]/) { $str = TitleCase($str); }
	if ($kcSubpart eq "k" && $str =~ m/^S[Uu][Bb][Pp]/) { $str = TitleCase($str); }
	if ($kcTitle eq "k" && $str =~ m/^T[Ii][Tt]/) { $str = TitleCase($str); }
	if ($kcChapter eq "k" && $str =~ m/^C[Hh]/) { $str = TitleCase($str); }
	if ($kcArticle eq "k" && $str =~ m/^A[Rr][Tt]/) { $str = TitleCase($str); }
	if ($kcDivision eq "k" && $str =~ m/^D[Ii][Vv]/) { $str = TitleCase($str); }
	if ($kcSubdivision eq "k" && $str =~ m/^S[Uu][Bb][Dd][Ii][Vv]/) { $str = TitleCase($str); }
	if ($kcAppendix eq "k" && $str =~ m/^A[Pp][Pp]/) { $str = TitleCase($str); }

	# if user chose to uppercase a heading AND current line is that heading, then do it.
	if ($kcPart eq "u" && $str =~ m/^P[Aa][Rr][Tt]/) { $str = uc($str); }
	if ($kcSubpart eq "u" && $str =~ m/^S[Uu][Bb][Pp]/) { $str = uc($str); }
	if ($kcTitle eq "u" && $str =~ m/^T[Ii][Tt]/) { $str = uc($str); }
	if ($kcChapter eq "u" && $str =~ m/^C[Hh]/) { $str = uc($str); }
	if ($kcArticle eq "u" && $str =~ m/^A[Rr][Tt]/) { $str = uc($str); }
	if ($kcDivision eq "u" && $str =~ m/^D[Ii][Vv]/) { $str = uc($str); }
	if ($kcSubdivision eq "u" && $str =~ m/^S[Uu][Bb][Dd][Ii][Vv]/) { $str = uc($str); }
	if ($kcAppendix eq "u" && $str =~ m/^A[Pp][Pp]/) { $str = uc($str); }

	# Abbreviate any headings as user indicated.
	if ($abrPart =~ m/[Rr]/) {
		$str =~ s|^P[Aa][Rr][Tt][Ss]||;	
		$str =~ s|^P[Aa][Rr][Tt]||;	
	}
	elsif ($abrPart =~ m/[Aa]/) {
		$str =~ s|^P[Aa][Rr][Tt][Ss]|Pts.|;
		$str =~ s|^P[Aa][Rr][Tt]|Pt.|;
	}

	if ($abrSubpart =~ m/[Rr]/) {
		$str =~ s|^S[Uu][Bb][Pp][Aa][Rr][Tt][Ss]||;
		$str =~ s|^S[Uu][Bb][Pp][Aa][Rr][Tt]||;
	}
	elsif ($abrSubpart =~ m/[Aa]/) {
		$str =~ s|^S[Uu][Bb][Pp][Aa][Rr][Tt][Ss]|Subpts.|;
		$str =~ s|^S[Uu][Bb][Pp][Aa][Rr][Tt]|Subpt.|;
	}

	if ($abrTitle =~ m/[Rr]/) {
		$str =~ s|^T[Ii][Tt][Ll][Ee][Ss]||;
		$str =~ s|^T[Ii][Tt][Ll][Ee]||;
	}
	elsif ($abrTitle =~ m/[Aa]/) {
		$str =~ s|^T[Ii][Tt][Ll][Ee][Ss]|Tits.|;
		$str =~ s|^T[Ii][Tt][Ll][Ee]|Tit.|;
	}		

	if ($abrChapter =~ m/[Rr]/) {
		$str =~ s|^C[Hh][Aa][Pp][Tt][Ee][Rr][Ss]||;
		$str =~ s|^C[Hh][Aa][Pp][Tt][Ee][Rr]||;
	}
	elsif ($abrChapter =~ m/[Aa]/) {
		$str =~ s|^C[Hh][Aa][Pp][Tt][Ee][Rr][Ss]|Chs.|;
		$str =~ s|^C[Hh][Aa][Pp][Tt][Ee][Rr]|Ch.|;
	}

	if ($abrArticle =~ m/[Rr]/) {
		$str =~ s|^A[Rr][Tt][Ii][Cc][Ll][Ee][Ss]||;
		$str =~ s|^A[Rr][Tt][Ii][Cc][Ll][Ee]||;
	}
	elsif ($abrArticle =~ m/[Aa]/) {
		$str =~ s|^A[Rr][Tt][Ii][Cc][Ll][Ee][Ss]|Art.|;
		$str =~ s|^A[Rr][Tt][Ii][Cc][Ll][Ee]|Art.|;
	}

	if ($abrDivision =~ m/[Rr]/) {
		$str =~ s|^D[Ii][Vv][Ii][Ss][Ii][Oo][Nn][Ss]||;
		$str =~ s|^D[Ii][Vv][Ii][Ss][Ii][Oo][Nn]||;
	}
	elsif ($abrDivision =~ m/[Aa]/) {
		$str =~ s|^D[Ii][Vv][Ii][Ss][Ii][Oo][Nn][Ss]|Div.|;
		$str =~ s|^D[Ii][Vv][Ii][Ss][Ii][Oo][Nn]|Div.|;
	}

	if ($abrSubdivision =~ m/[Rr]/) {
		$str =~ s|^S[Uu][Bb][Dd][Ii][Vv][Ii][Ss][Ii][Oo][Nn]||;
	}
	elsif ($abrSubdivision =~ m/[Aa]/) {
		$str =~ s|^S[Uu][Bb][Dd][Ii][Vv][Ii][Ss][Ii][Oo][Nn]|Subdiv.|;
	}

	if ($abrAppendix =~ m/[Rr]/) { $str =~ s|^A[Pp][Pp][Ee][Nn][Dd][Ii][Xx]||; }
	elsif ($abrAppendix =~ m/[Aa]/) { $str =~ s|^A[Pp][Pp][Ee][Nn][Dd][Ii][Xx]|Apx.|; }
	
	#Remove any spaces at beginning or end of line
	$str =~ s|^ ||;
	$str =~ s| $||;
	return $str;
}

#=============================================
sub TitleCase {
#=============================================

	my $title = q{};
	my $subtitle = q{};
	my $str = shift;
	
	$str =~ m|^(.*?)\\(.*?)$|;
	
	# text before backslash is title. Text after backslash is subtitle.
	$title = $1;
	$subtitle = $2;

	# lowercase all text in subtitle.
	$subtitle = lc($subtitle);

	# split words of subtitle into array elements
	my @subtitle = split(/ /, $subtitle);

 	# for each element of array (except specific small words), lowercase first letter 
 	foreach (@subtitle) {
 		if ($_ ne "of" && $_ ne "and" && $_ ne "for" && $_ ne "by" && $_ ne "the" && $_ ne "with") {
	 		$_ = ucfirst($_);
	 	}
 	}
 	# re-join the array elements into a single string
 	$subtitle = join(" ", @subtitle);
	
	# put title back together with subtitle
	$str = $title . "\\" . $subtitle;
	
	return $str;

}
#=============================================
sub AddSpecs {
#=============================================

	print TOC "<tochead>\n";
	print TOC "<setpage;13>\n";
	print TOC "<setrecto;TABLE OF CONTENTS--Cont'd.>\n";
	print TOC "<setverso;>\n";
	print TOC "<setrh1;Chapter;Page>\n";
	print TOC "\n";
	print TOC ";hc;\n";
	print TOC "TABLE OF CONTENTS\n\n";
	print TOC ";hr;\n";
	print TOC "Page\n";

return 0;
}


__END__


#============================
#POD Documentation
# To convert this documentation to an html file, type "C:\>pod2html TOCfromGC.pl > TOCfromGC.html" <enter>
#============================

=pod

=head1 TOCfromGC

=head3 This program creates a gen-coded table of contents file based on the headings in other gen-coded files.

=head2 User Information

=head3 One argument expected: valid path of folder containing gen-coded files to be used in creating the Table of Contents.

=head3 One output file will be created: ATOC.TXT at the same location as the input files.

=head3 Prompts

=over

=item If ATOC.TXT already exists, the user will be prompted to overwrite it or cancel processing.

=item An example of default heading abbreviation and capitalization style will be displayed. User will be asked if they want to customize TOC or use defaults.

=back

=head2 Developer Information

=over

=item Source: v:-source-TOCfromGC.pl

=item Compile pl to exe and place in u:-batch

=back

=cut

__END__