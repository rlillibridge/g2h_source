#!/usr/bin/perl
# ImageInfo.pl
# Programmer:  Raymond Lillibridge
#my $myVersion = "Version:  2013-11-07.1 (ImageInfo)";
my $myVersion = "Version:  2016-09-14.2";

################################################################ 
# Search through Book_Struct.xml file, calculates proper image width and height.
#	<img alt="2.2.1.tif" src="./images/2.2.1.tif" width="" height=""/>
################################################################ 

# WIP:
# 2016-09-14.2 - Raymond Lillibridge - Added 'nopause' conditional after the "tidy" section
# 2016-09-14.1 - Raymond Lillibridge - Use XML::LibXML::PrettyPrint BEFORE doing anything else.
# 2013-11-06.1 - Raymond Lillibridge - Allow multiple inline images (IGs).
# 2013-10-03.1 - Raymond Lillibridge - Allow passing in --nopause option

# History:
# 2013-09-04.1 - Raymond Lillibridge - Re-compile after Malware issues
# 2013-08-08.1 - Raymond Lillibridge - MERGE after rlillibridge-vm FAILURE & RESTORE (reinstall of Perl, Perl PDK, &c.)
#	GhostScript EPS->PDF problem - rotation of image in PDF output! (test account 15201)
# 2013-07-30.1 - Raymond Lillibridge - To use png rather than jpg image formats
# 2013-06-04.1 - Raymond Lillibridge - Removed many of DEBUG lines from screen display
# 2013-04-23.1 - Raymond Lillibridge - THIS APPLICATION REPLACES ImageMeta.exe in G2h_core.bat.
# 2013-04-18.1 - Raymond Lillibridge - Found Term::InKey for Perl 5.16
#     Using ImageMagick instead of Image-MetaData.

# 2013-04-09.1 - Raymond Lillibridge - <ig/> Issues Project
#	Term::InKey -> Term::ReadKey (for Perl 5.16.3)

# 2013-03-28.1 - Raymond Lillibridge - Perl Upgrade to 5.16.3; 
#	Image-MetaData not supported in 5.10 unless we pay ActiveState $1000. for Business Version of perllocal
#	Asked Phil to upgrade ActiveState PDK to latest version to support Perl 5.6 - 5.
#	Restructure use of Image-MetaData (not available for Perl 5.10) to use Image-Info instead
# 20101202.1 Raymond Lillibridge - Allow for -nopause processing

#==========================================================
# PRAGMA
#==========================================================
use warnings;
no warnings 'uninitialized';

use Term::InKey;
use File::Basename;
use XML::LibXML;
use XML::LibXML::PrettyPrint;

use Getopt::Long;
my $opt_d;			#-d	debug switch
my $opt_FrameWidth="800";	# --PixelWidth = 768  (default)  about 8"  (48p) screen 96dpi
my $opt_ScreenDPI="120";
my $opt_h;
my $opt_help;
my $opt_topause;

GetOptions(
	"d"				=>	\$opt_d,
	"FrameWidth=s" 	=>	\$opt_FrameWidth,
	"ScreenDPI=s"		=>	\$opt_ScreenDPI,
	"topause=s"		=>  \$opt_topause
);

my $ToPause="$opt_topause";

#==========================================================
# GLOBALS
#==========================================================
my $InputXML = "Book_Struct.xml";
my $OutputXML = "Book_StructIMG.xml";
my $dashes = "--------------------------------------------------------------------------------";
my $line;
my $imgline = "";
my $so_line = "";
my $lineTemp;
my $key = "";
my $idx = 0;
my $alineOriginal = ();
my @aline = ();
my @aErrors = ();

# Date and Time vars
my($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime;
$year += 1900;
my $now_string = localtime;
my $nowYYYYMMDD_HHMMSS= dateTime();
my $LogDateTime =	$nowYYYYMMDD_HHMMSS->{'dateNr'} .  "_" . 
					$nowYYYYMMDD_HHMMSS->{'HHMMSS'};
my $myDT=$LogDateTime;
$myDT =~ s|(\d\d\d\d)(\d\d)(\d\d)_(\d\d)(\d\d)(\d\d)|$1\/$2\/$3 $4:$5:$6|g;

BEGIN { $| = 1 }

#==========================================================
# MAIN MAIN MAIN MAIN MAIN MAIN MAIN 
#==========================================================
Housekeeping();

print STDOUT "\n(passed in) FrameWidth=$opt_FrameWidth\tScreenDPI=$opt_ScreenDPI\n\n";
	
## Process the file or quit?
print STDOUT "\n     continue?...\n$dashes\nY = YES                 --OR--     (blank\) or (!Y) = CANCEL\n\n";

if($ToPause ne "nopause")	{		
	$key = ReadKey;
} else {
	$key = "Y";
}

if(uc($key) =~ /[Y]/) {
	# do nothing...
	print STDOUT "\n";	#add a little space before processing the files
} else {
	print STDOUT "\n\...$0 Processing cancelled.\n";
	exit 0 ;
}

#--------------------------------------
# Clean up indents of input file... 
#(2016-09-13 Raymond Lillibridge)
#--------------------------------------
# Create $OutputXML file...
print "Fixing $InputXML structure before image processing..."; 

if(-e "_tempXML.xml") { unlink("_tempXML.xml"); }

my $document = XML::LibXML->new->parse_file($InputXML);
#my $pp = XML::LibXML::PrettyPrint->new(indent_string => "    ");
# my $pp = XML::LibXML::PrettyPrint->new(
	# element => {
          # inline   => [qw/span strong em b i a/],
          # block    => [qw/p div body html head/, $callback],
          # compact  => [qw/title caption li dd dt th td/],
          # preserves_whitespace => [qw/pre script style/],
         # }
# );	# default is to \t and utf8

my $pp = XML::LibXML::PrettyPrint->new(
	indent_string => "  ",
	element => {
          inline   => [qw/footnoteref span strong em b i a/],
          compact  => [qw/title subtitle productid expandlevel ClientID JobID JobName Client City State Zip PoprangeID ClassificationID BudgetMonth li dd dt th td bold bdit ital rule/],
          preserves_whitespace => [qw/banner pre script style/],
        }
);	# default is to \t and utf8
$pp->pretty_print($document); # modified in-place
my $state = $document->toFile("_tempXML.xml");

# print "DONE!\n\$state: $state\n\n";

if($ToPause ne "nopause")	{		
	print "(Press any key to continue...)\n\n";
		$key = ReadKey;
} else {
		$key = "Y";
}
 
 open FILE, '<:utf8', "_tempXML.xml"
	|| die "cannot open file for input:  _tempXML.xml";	
	
# Create $OutputXML file...
open OUT, ">:utf8", $OutputXML
	|| die "cannot open file for output:  $OutputXML";

print STDOUT "\n\$opt_FrameWidth=$opt_FrameWidth \n\n";
	
while(<FILE>) {
	 $line = $_;
	chomp($line);			# Strip the trailing newline from the line.

	if($line =~ /<img /)	{
		# Need to combine this line and all following lines until '/>' is found...
		$lineTemp = $line;	
		
		while($lineTemp !~ /<img (.+?)\/\>/)	{	# until $lineTemp contains the full <img ... /> element
			$line=readline(FILE);
			chomp($line);
			$lineTemp .= $line;
		}
		
		$line = $lineTemp;
		
		print STDOUT "$dashes\nINPUT:$dashes\n$line\n\n============\n";

		$imgline = substr($line, index($line, "<img "));		
		$imgline =~ s|(<img )|~~~$1|g;
		$imgline =~ s| (selectout=)|~~~$1|g;
		$imgline =~ s| (alt=)|~~~$1|g;
		$imgline =~ s| (src=)|~~~$1|g;
		$imgline =~ s| (width=)|~~~$1|g;
		$imgline =~ s| (height=)|~~~$1|g;
		
		# Save the 'selectout=' substring...
		if($imgline =~ /~~~selectout=/) {
			$so_line = $imgline;
			if($opt_d) { print STDOUT "\$so_line=$so_line\n"; }
			$so_line =~ s|^(.+?)~~~(selectout=\")(.+?)(\")~~~(.+?)$|$2$3$4|g;
			if($opt_d) { print STDOUT "\$so_line=$so_line\n"; }
		} else {
			$so_line = "";	
		}
		
		$imgline =~ s|~~~(selectout=)(.+?)~~~|~~~|g;		#REMOVE this one
				
		@alineOriginal = split('~~~', $imgline);		
		@aline = @alineOriginal;
		
		if($opt_d) {
			print STDOUT "\n\n+++ DEBUG:  \@aline +++\n";
			
			for(my $x=0; $x <= $#aline; $x++) {
				print STDOUT "($x) =$aline[$x]\n";
			}
			
			print STDOUT "+++ END DEBUG +++\n\n";	
		}	
		
		$line = IMG_SetWidthDepth($line);
		
		print STDOUT "============\n\nOUTPUT:\n$line\n\n";
	}
		
	print OUT "$line\n";	

} # END WHILE...

if($#aErrors > -1) {
	print STDOUT "\n$dashes\n\tIMPORTANT!!!\n$dashes\n";
	print STDOUT "WARNING! Editor's Use of Width and\/or Height Values are GREATER THAN 100\% For These Images:\n\n";
	
	foreach (@aErrors) {
		print STDOUT "$_";	
	}	
	
}

close FILE;
close OUT;
print STDOUT "\n\t$OutputXML created!\n\n\n";
print STDOUT "$0 FINISHED!\n\n";

1;
#==========================================================
#	MAIN END
#==========================================================


#==========================================================
# SUBROUTINES
#==========================================================
#-------------------------------------------------------------
sub IMG_SetWidthDepth	{
#-------------------------------------------------------------
#	<img alt="2.2.1.tif" src="./images/2.2.1.tif" width="" height=""/>
	my $myLine = shift;
	my $me = whoami();
	my $imgSrc="";
	my $imgWidthPercent="";
	my $dims = "";
	my @adimstr = ();
	my $image_actual_width = 0;
	my $image_actual_height = 0;
	my $image_actual_ppi = 0;
	my $awidth_inches = 0.0;	# used to simplify calculations
	my $aheight_inches = 0.0;	# used to simplify calculations	
	my $image_print_gencode_width = 0;
	my $image_print_gencode_height = 0;
	my $pwidth_inches = 0.0;	# used to simplify calculations
	my $pheight_inches = 0.0;	# used to simplify calculations
	my $image_print_gencode_scale_percent = 0;
	my $pwidth_ppi = 0;
	my $pheight_ppi = 0;
	my $mypercentage = 0.0;
	my $qmwidth = "";		# GenCode qualified measure:  i, p, or q
	my $qmheight = "";		# GenCode qualified measure:  i, p, or q
	my $web_pwidth_ppi = 0;
	my $web_pheight_ppi = 0;
	my $rtn = "";
		
	$imgSrc = $myLine;
	
	$imgSrc =~ s|^(.+?)alt=\"(.+?)\.(.+?)\" src(.+?)$|$2\.$3|g;
#	$imgSrc =~ s|^(.*)\.tif$|$1.jpg|ig;	###NOTE### For some reason this line will NOT compile!!!
	$imgSrc = substr($imgSrc, 0, -4) . ".png";
	
	#######################################################
	# ASSUMPTION:   ALL GRAPHICS ARE PNG in ./HTML/image/ subfolder
	#######################################################
	$imgSrc = "../HTML/images/" . $imgSrc;	
	print STDOUT "\n\$imgSrc=$imgSrc\n";
	# NOTE:  HTML5 DOES NOT ALLOW FOR WIDTH OR HEIGHT PERCENTAGES, ONLY PIXELS
	$dims = png_imagesize($imgSrc);
	
	print STDOUT "$dims\n";

	@adimstr=();
	@adimstr = split ' ', $dims;	

	$image_actual_width = $adimstr[0];
	$image_actual_height = $adimstr[1];
	$image_actual_ppi = $adimstr[2];
	
	print STDOUT "image_actual_width=\t$image_actual_width\n";
	print STDOUT "image_actual_height=\t$image_actual_height\n";
	print STDOUT "image_actual_ppi=\t$image_actual_ppi\n";
	
	# Magic Calculations...
	if( ($image_actual_ppi =~ /[^0-9]/) | ($image_actual_ppi < 1) ) {
		$image_actual_ppi = $opt_ScreenDPI;
	}
	
	# Image ACTUAL size in inches...
	$awidth_inches = $image_actual_width / $image_actual_ppi;
	$aheight_inches = $image_actual_height / $image_actual_ppi;
	
	print STDOUT "Actual Width (inches)  = $awidth_inches\n";
	print STDOUT "Actual Height (inches) = $aheight_inches\n";
	
	# ===========================================
	# WIDTH Calculations
	# ===========================================
	# Get Editor's GenCode qualified measure...
	if($aline[4] =~ /\"\"/) {
		$aline[4] = "";
		$image_print_gencode_width = 0;
		$qmwidth = "i";
	} else {
		$aline[4] =~ s|^width=\"(.+?)\"$|$1|g;
		$image_print_gencode_width = $aline[4];
		$qmwidth = substr($image_print_gencode_width, -1, 1);	# get last character of string
		$image_print_gencode_width = substr($image_print_gencode_width, 0, -1);  # remove qualifier character at end of string
	}
	
	if($opt_d) { print STDOUT "\n[debug] \$qmwidth=$qmwidth\t\$image_print_gencode_width=$image_print_gencode_width\n\n"; }

	# Convert Editor's GenCode qualified measure to inches...
	# WIDTH...
	if($qmwidth eq "i") {		# inches...
		$pwidth_inches =$image_print_gencode_width;
		
	} elsif($qmwidth eq "p") {	# picas...	(12 points = 1 pica; 6 picas = 1 inch)
		$pwidth_inches = $image_print_gencode_width / 6.0;
		$qmwidth = "i";
		
	} elsif ($qmwidth eq "q") {	# points...(12 points = 1 pica; 6 picas = 1 inch)
		$pwidth_inches = ($image_print_gencode_width / 12.0) / 6.0;
		$qmwidth = "i";
		
	} elsif($qmwidth eq "%") {	# percentage...
		$pwidth_inches = ($image_print_gencode_width / 100) * $awidth_inches;
		$qmwidth = "i";	
			
	} else {
		print STDOUT "\n--- ERROR! Qualified width is invalid:  $qmwidth\t($aline[4])\n\n";
		print STDOUT "Press CTRL-C = CANCEL; \t (any other key to continue...\n\n";
		$key = ReadKey;
	}
	
	if($opt_d) { print STDOUT "\n+++ [DEBUG pwidth_inches qmwidth]:  $pwidth_inches" . "$qmwidth\n\n"; }
	
	# ===========================================
	# END Width Calculations
	# ===========================================

	# ===========================================
	# HEIGHT Calculations
	# ===========================================
	# Get Editor's GenCode qualified measure...
	if($aline[5] =~ /\"\"/) {
		$aline[5] = "";
		$image_print_gencode_height = 0;
		$qmheight = "i";
	} else {
		$aline[5] =~ s|^height=\"(.+?)\"\/>|$1|g;
		$image_print_gencode_height = $aline[5];
		$qmheight = substr($image_print_gencode_height, -1, 1);	# get last character of string
		$image_print_gencode_height = substr($image_print_gencode_height, 0, -1);  # remove qualifier character  at end of string
	}

	# Convert Editor's GenCode qualified measure to inches...
	# height...
	if($qmheight eq "i") {		# inches...
		$pheight_inches =$image_print_gencode_height;
		
	} elsif($qmheight eq "p") {	# picas...	(12 points = 1 pica; 6 picas = 1 inch)
		$pheight_inches = $image_print_gencode_height / 6.0;
		$qmheight = "i";
		
	} elsif ($qmheight eq "q") {	# points...(12 points = 1 pica; 6 picas = 1 inch)
		$pheight_inches = ($image_print_gencode_height / 12.0) / 6.0;
		$qmheight = "i";

	} elsif($qmheight eq "%") {	# percentage...
		$pheight_inches = ($image_print_gencode_height / 100) * $aheight_inches;
		$qmheight = "i";	
		
	} else {
		print STDOUT "\n--- ERROR! Qualified height is invalid:  $qmheight\t($aline[5])\n\n";
		print STDOUT "Press CTRL-C = CANCEL; \t (any other key to continue...\n\n";
		$key = ReadKey;
	}
	
	#print STDOUT "+++ [DEBUG pheight_inches qmheight]:  $pheight_inches" . "$qmheight\n\n";

	# ===========================================
	# END Height Calculations
	# ===========================================

	# ===========================================
	# PERCENT Calculations
	# ===========================================
	if($pwidth_inches > 0) {
		$image_print_gencode_scale_percent = ($pwidth_inches / $awidth_inches);
	} elsif( $pheight_inches > 0) {
		$image_print_gencode_scale_percent = ($pheight_inches / $aheight_inches);
	} else {
		$image_print_gencode_scale_percent = "1.00";
	}
	
	if($opt_d) { print STDOUT "\n [DEBUG \$image_print_gencode_scale_percent = $image_print_gencode_scale_percent\n"; }

	if($image_print_gencode_scale_percent > 1.0) {
		push @aErrors, " \t $imgSrc (...about " . int($image_print_gencode_scale_percent * 100) . "\%)\n";	
	}

	$pwidth_ppi = int($image_actual_width * $image_print_gencode_scale_percent);
	$pheight_ppi = int($image_actual_height * $image_print_gencode_scale_percent);
	
	$web_pwidth_ppi =($pwidth_ppi / $image_actual_ppi) * $opt_ScreenDPI;
	$web_pheight_ppi = ($pheight_ppi / $image_actual_ppi) * $opt_ScreenDPI;
	
	# rounding...
	$web_pwidth_ppi = sprintf("%u", ($web_pwidth_ppi += 0.5));
	$web_pheight_ppi = sprintf("%u", ($web_pheight_ppi += 0.5));	
	
	if($opt_d) { 
		print STDOUT "[(debug A) \$web_pwidth_ppi=$web_pwidth_ppi\t(Frame width=$opt_FrameWidth\tScreen DPI=$opt_ScreenDPI)\n";
		print STDOUT "[(debug A) \$web_pheight_ppi=$web_pheight_ppi\n";
	}
	
	# Is width > $opt_FrameWidth?...
	if($web_pwidth_ppi > $opt_FrameWidth) {
		$mypercentage = $opt_FrameWidth / $web_pwidth_ppi;
		$web_pwidth_ppi = $opt_FrameWidth;
		$web_pheight_ppi = $web_pheight_ppi * $mypercentage;
	}
	
	# rounding...
	$web_pwidth_ppi = sprintf("%u", ($web_pwidth_ppi += 0.5));
	$web_pheight_ppi = sprintf("%u", ($web_pheight_ppi += 0.5));
	
	if($opt_d) {	
		print STDOUT "[(debug B) \$web_pwidth_ppi=$web_pwidth_ppi\n";
		print STDOUT "[(debug B) \$web_pheight_ppi=$web_pheight_ppi\n";
	}
	
	$aline[4] = "width=\"" . $web_pwidth_ppi . "\"";	
	$aline[5] = "height=\"" . $web_pheight_ppi . "\"";
	$rtn = "";
	
	foreach (@aline) {
		$rtn .= "$_ ";
	}
	
	$rtn .= "\/>";
	
	if($opt_d) { print STDOUT "\n\$so_line=$so_line\n\n"; }
	
	$rtn = substr($rtn,0, index($rtn, "alt=")) . $so_line . substr($rtn, index($rtn, " alt="));
	$rtn =~ s/^\s+//;	#trim leading whitespace
		
	if($opt_d) {
		print STDOUT "\n\n+++ DEBUG (NEW):  \@aline +++\n";
		
		for(my $x=0; $x <= $#aline; $x++) {
			print STDOUT "($x) =$aline[$x]\n";
		}
		print STDOUT "+++ END DEBUG +++\n\n";	
	}

	$myLine =~ s|<img id=(.+?)\/>|$rtn|g;
	
	# eps -> png...
	$myLine =~ s|\"(.+?)\.eps\"|\"$1\.png\"|g;
		
	return $myLine;
}


#--------------------------------------
sub png_imagesize {
#--------------------------------------
	my $image = shift;
	
	use Image::Magick;
	$ImageMagick = Image::Magick->new;
	$ImageMagick->Read($image);	
	
	#RETURNS: "1527x975 144 PixelsPerInch"...
	$rtn = `V:\\Apps_3rdparty\\ImageMagick\\identify -format \"%w %h %x\" $image`;
	
	$rtn =~ s|\n||g;
	return $rtn;
}


#-------------------------------------------------------------
sub dateTime {
#-------------------------------------------------------------
	my $me = whoami();
	my ($sec,$min,$hour,$mday,$monNr,$year);
	my $now = {};
	#get date and time
	($sec,$min,$hour,$mday,$monNr,$year) = localtime(time());
    	my ($date, $date_nr, $clock);
	my $month=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")[$monNr];
    	$monNr++;
	$monNr = sprintf("%02d",$monNr);
	$mdayNr = sprintf("%02d",$mday);
	$sec = sprintf("%02d",$sec);
	$min = sprintf("%02d",$min);
	$hour = sprintf("%02d",$hour);
	$year= 1900 + $year;
	$now->{'date'} = "$year-$month-$mday";
	$now->{'dateNr'} = "$year$monNr$mdayNr";
	$now->{'time'} = "$hour:$min:$sec";
	$now->{'HHMMSS'} = "$hour$min$sec";
	return($now);		
}


#-------------------------------------------------------------
sub Housekeeping	{
#-------------------------------------------------------------
	my $ctr = 0;
	
	if($ToPause ne "nopause")	{
		&Clear;
	}
	
	print STDOUT "\nRUNNING:  $0\t\t$myVersion\n";	
	
	# DELETE EXISTING $Book_StructIMG file...
	
	if(-e $OutputXML)	{	
		$ctr = unlink($OutputXML);
		if($ctr > 0) { print STDOUT "\n...DELETED $OutputXML\n"; }
	} else {
		print STDOUT "\n...$OutputXML NOT FOUND.\n";
	}
	
	if(not -e $InputXML) {
		print STDOUT "------------------------------\n\t XML INPUT FILE ($InputXML) NOT FOUND!\n------------------------------\n";
		exit 0;	
	}
	
	print STDOUT "\nINPUT FILE:  $InputXML\n";
}



#-------------------------------------------------------------
sub whoami {
#-------------------------------------------------------------
	(caller(1))[3];
}
