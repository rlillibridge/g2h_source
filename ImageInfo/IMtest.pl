#!/usr/bin/perl -w
#
# USE:  Reads image information and returns string: width="" height="".
# Written:  2013-04-18 - Raymond Lillibridge
# -----------------------------------------------------------------------------
use Term::InKey;
use File::Basename;
use Image::Magick;

my $parm = $ARGV[0];

my $ext = "";
my ($w, $h, $g);
my ($xres, $yres) = 0;
my $dashes = "------------------------------------------------------------";
my $AppPath = "";

my $rtn = "";

if(!defined $PerlApp::VERSION)	{
	$AppPath = $0;
} else {
	$AppPath = PerlApp::exe();	
}

Clear;	

if( length( $parm // '') ) {
	print STDOUT "$dashes\n";
	print STDOUT "$AppPath\\Info Image:  $parm\n";
	print STDOUT "$dashes\n";
} else {
	print STDOUT "$dashes\n";
	print STDOUT "$AppPath\\Info Image:  ???\n";
	print STDOUT "$dashes\n";	
	print STDOUT "\n\tERROR:  Please provide an image to process.\n\n";	
	exit;
}

$ext = lc(substr($parm, -4));
my $filename = basename($parm, $ext);

if($ext eq ".jpg") {
	#$rtn = nonEPS_imagesize($parm);
	print STDOUT "...TESTING WITH ImageMagick...\n";
	$rtn = jpg_imagesize($parm);

#} elsif($ext eq ".gif") {
#	$rtn = nonEPS_imagesize($parm);
#	
#} elsif($ext eq ".png") {
#	$rtn = nonEPS_imagesize($parm);
#
#} elsif($ext eq ".eps") {	
#	$rtn = eps2jpg($parm);	# convert to jpg and then runs nonEPS_imagesize...
#	#print STDOUT "\tProcessing JPG (Prior EPS)...\n";
#	#$rtn = nonEPS_imagesize($filename . ".jpg");
#	
} else {
	# Image not supported
	$rtn = "$parm is NOT SUPPORTED.\n";
}

print STDOUT "\n$dashes\n" . $rtn . "\n$dashes\n\n";

do_imageinfo();

1;


##--------------------------------------
#sub eps2jpg {
##--------------------------------------
#	my $image = shift;
#	use Image::Magick;
#
#	$ImageMagick = Image::Magick->new;
#	$ImageMagick->Read($image);
#	
#	my ($width, $height, $size, $format) = $ImageMagick->Ping($image); 
#	$rtn = "width=\"$width\" height=\"$height\"";
#
#	#$rtn = "(Ping): \nwidth: \t\t$width\nheight: \t$height\nsize: \t\t$size\nformat: \t$format\n\n";			
#	#system("V:\\Apps_3rdParty\\ImageMagick-6.8.4-10\\identify.exe -format \"%m:%f %wx%h\" $image");
#	
#	return $rtn;
#}


##--------------------------------------
#sub nonEPS_imagesize {
##--------------------------------------
#	# RETURNS:  HTML size:  'width="60" height="40"'
#	my $image = shift;	
#	my $htmlsize = "";
#	use Image::Size 'html_imgsize';
#	
#	if( -e $image) {
#		$htmlsize = html_imgsize("$image");
#		#varprint("\$htmlsize", $htmlsize);
#	} else {
#		$htmlsize = "\n FILE NOT FOUND!\n";	
#	}
#	
#	$rtn = $htmlsize;
#	
#	return $rtn;
#}


#--------------------------------------
sub jpg_imagesize {
#--------------------------------------
	my $image = shift;
	
	$ImageMagick = Image::Magick->new;
	$ImageMagick->Read($image);	
		
	#1527x975 144 PixelsPerInch:
	#$rtn = system("V:\\Apps_3rdparty\\ImageMagick\\identify -format \"%wx%h %x\" $image");
	$rtn = `V:\\Apps_3rdparty\\ImageMagick\\identify -format \"%wx%h %x\" $image`;
	
	#Adjust the DPI:
#	convert image.jpg -units "PixelsPerInch" -density 300 -resample "300x" image300.jpg
	#system("V:\\Apps_3rdparty\\ImageMagick\\convert $image -units \"PixelsPerInch\" -density 96 -resample \"96x\" $filename" . "_96.jpg");
	
	#Check the properties of the new file:
	#$rtn .= "\n" . system("V:\\Apps_3rdparty\\ImageMagick\\identify -format \"%wx%h %x\" $filename" . "_96.jpg");	
	
	$rtn =~ s|\n||g;
	return $rtn;
}


##--------------------------------------
#sub varprint {
##--------------------------------------
#	my $varname = shift;
#	my $varvalue = shift;
#	
#	print STDOUT "$varname: \t\t$varvalue\n";
#}


#--------------------------------------
sub do_imageinfo {
#--------------------------------------
	my %good = (
	    'ColorSpace' => 1,
	    'ComponentsConfiguration' => 1,
	    'DateTime' => 1,
	    'DateTimeDigitized' => 1,
	    'DateTimeOriginal' => 1,
	    'ExifImageLength' => 1,
	    'ExifImageWidth' => 1,
	    'ExifVersion' => 1,
	    'FileSource' => 1,
	    'Flash' => 1,
	    'FlashPixVersion' => 1,
	    'ISOSpeedRatings' => 1,
	    'ImageDescription' => 1,
	    'InteroperabilityIndex' => 1,
	    'InteroperabilityVersion' => 1,
	    'JPEG_Type' => 1,
	    'LightSource' => 1,
	    'Make' => 1,
	    'MeteringMode' => 1,
	    'Model' => 1,
	    'Orientation' => 1,
	    'SamplesPerPixel' => 1,
	    'Software' => 1,
	    'YCbCrPositioning' => 1,
	    'color_type' => 1,
	    'file_ext' => 1,
	    'file_media_type' => 1,
	    'height' => 1,
	    'resolution' => 1,
	    'width' => 1
	);
	
	use Image::Info qw(image_info);
	
	
	foreach my $cur_file (@ARGV) {
	    my $info = image_info($cur_file);
	
	    print "$cur_file ----------------------------------\n";
	    foreach my $key (sort keys %$info) {
	        if ($good{$key}) {
	            print "    $key -> $info->{$key}\n";
	        }
	    }
	}
}
