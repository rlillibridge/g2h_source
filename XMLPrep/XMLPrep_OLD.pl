#!/usr/bin/perl
# XMLPrep.pl (TESTING WITH ACCOUNT NO. "13783 s:14745, s:16780")
# Programmer:  Raymond Lillibridge
my $myVersion = "Version:  2013-09-03.1 (WIP)";

# WIP:
# 2013-10-01.1 - Raymond Lillibridge - Allow passing in --nopause option

#=============================================	
# CHANGE HISTORY:
# 2013-09-03.1 - Raymond Lillibridge - Problem with G2h_Dev running this and $CWDSAVE = R:\
#				ENLIGHTENMENT!!! SOMEBODY SET V:\APPS\CONFIG\XMLPREP_CONFIG.XML TO USE "live"
# 2013-05-29.1 - Raymond Lillibridge - For some reason DEV processing is accessing R:\ (LIVE) stuff and updating G:\Accts (it shouldn't)
#				ENLIGHTENMENT!!! ->  SEE:  R: or V: \Apps\Config\XMLPrep_config.xml
# 2011-08-26.1 Raymond Lillibridge - Saving Job No. of all split accounts in _XMLPrep_SPLITACCT.txt
# 2011-08-25.1 Raymond Lillibridge - Saving 'state' for split account processing by creating _XMLPrep_SPLITACCT.txt
# 2011-08-24.1 Raymond Lillibridge - [NOT WORKING] Added setting of ENVIRONMENTAL Variable %SPLITACCT%.  Used by G2H_XP.bat
#                                                Win32-AdminMisc module installed to set ENVIRONMENTAL Variable

#===============================================	
# PROCESSING OUTLINE
#===============================================	
#------------------------------------------------------------
# ● [DONE] File Combination
#		o Query DB to see if account needs to be combined into one working area
#		o Copy CONFIG in from PRIMARY account into G:\accts\~\CONFIG
#		o Copy CONFIG in from MASTER area into G:\accts\~\CONFIG
# ● [DONE] Config File Transport (makes g:\accts\...\ true source)
#		o Now that g:\ is the true source directory, any changes made to CONFIG files must be copied back to g:\
#		o If “R:\ACCTS\12345\gc\CONFIG” exists && “G:\ACCTS\12345\CONFIG” does not exist
# 			Initialize “G:\ACCTS\12345\CONFIG” with the contents of “R:\ACCTS\...\CONFIG”
#		o Else If neither R:\...\CONFIG or G:\...\CONFIG exist
# 			Initialize “G:\ACCTS\12345\CONFIG” with default pubmap and xref config files
# ● [DONE] Back-up Contents of G:\ to Y:\ (same logic as specstriage)
# ● [DONE] Clear-out Entire Working Folder Area
#		o Re-initialize folder with blank subdirectories
# ● [DONE] Copy CONFIGS and GENCODE From G:\ to R:\
#		o Filter-out unnecessary files (current FilterFile() logic)
#		o Allow for “.mcc” extensions (current FilterFile() logic)
#		o Copy in CONFIG files (NEW)
# 		o R:\ IF SPLIT-ACCOUNT then update _PubMap.xml file with "a_#####" prefix IF fn NOT begins with "a_"
#
#
#------------------------------------------------------------
# SEPERATE PROGRAMS REQUIRED:
#------------------------------------------------------------
# ● HTML_LocalSetup.exe to check the %ENV->SPLITACCT and copy images accordingly (report duplicate image names)
# ● GCspecsReturn.exe - Copy Back Gencode & Configs (G2H.bat - After Validation of IDs has been approved)
#		o Now that g:\ is the true source directory, any changes made to CONFIG files must be copied back to g:\ along with the cleaned gencode files
# ● GCdelspecs.exe - “Remove Specs” GUI application process selected (filtered) files in folder.
#=============================================	

#=============================================	
#	Config Name and Location...
#=============================================	
if (!defined $PerlApp::VERSION) {
		$APPpath = $0;
}	else {
		$APPpath = PerlApp::exe();
}

my $driveletter = substr($APPpath,0,2);
my $sXMLPrep_ConfigName = "XMLPrep_config.xml";
my $sAppPath= ($driveletter . "\\APPS\\CONFIG");		#LOCAL (location of file above)

#umask 0777;

#=============================================	
# PRAGMA
#=============================================	
use strict;
use warnings;
no warnings 'uninitialized';

use XML::Simple qw(:strict);
use Data::Dumper;
use DBI;
use File::Basename;
use File::chdir;
use File::Copy;
use File::Copy::Recursive qw(dircopy);
local $File::Copy::Recursive::SkipFlop = 1;

use File::Listing qw(parse_dir);
use File::Path;
use Win32;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );

#for development purposes...
use Term::InKey;
use Getopt::Long;

my $opt_d;				#-d			debug switch
my $opt_job="-none-";		# --job		job or account_number
my $opt_new=0;			# --new		new_process
my $opt_old=0;			# --old		old_process
my $opt_topause="";		# --topause	pause or nopause
my $opt_h;
my $opt_help;

GetOptions(
	"d"			=>	\$opt_d,
	"job=s" 		=>	\$opt_job,
	"new"		=>	\$opt_new,
	"old"			=>	\$opt_old,
	"topause=s"	=>	\$opt_topause,
	"h"			=>	\$opt_h,
	"help"		=>	\$opt_help
);

#=== Win32 ===
use Cwd;
my $CWDSAVED;
my $WinLoginName = getlogin || getpwuid($<) || "Win Login-UNKNOWN-";

# Date and Time vars
my($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime;
$year += 1900;
my $now_string = localtime;
my $nowYYYYMMDD_HHMMSS= dateTime();
my $LogDateTime =	$nowYYYYMMDD_HHMMSS->{'dateNr'} .  "_" . 
					$nowYYYYMMDD_HHMMSS->{'HHMMSS'};

#=============================================	
# GLOBALS
#=============================================
my $username=$WinLoginName;
my $UserPath="";
my $UserPathTemp="";
my $contents;
my $XMLPrepExists=0;
my $WFprefix=1;
my %WFAlpha = (
        1 => "a",
        2 => "b",
        3 => "c",
        4 => "d",
        5 => "e"
    );

my $Path;
my $BackupPath;
my $AccountPath_acctno;
my $WorkArea_acctno;
my $acct;
my $myFolder;
my $AcctConfigFile;
my $str1;
my $str2;
my $zip;
my $file_member;
my $dir_member;
my $FoundSpecsHeader="0";

my $XMLconfig;
my $XMLJobs;

# config values:
my $config_allusers="";
my $XMLPrepFolder="";
my $config_LogFile="";
my $config_MasterTemplatesFolder="";
my $config_MasterAccountFolder="";
my $config_WorkArea="";
my $config_BackupPath="";
my $config_OldProcessFolder="";
my $config_NewProcessFolder="";
my $config_CKListPath="";
my @config_ExcludeFilter=();

my $BadConfig=0;
my $FileFilterFlag="skip";

#my @account=();
my @newprocess=();
my @ErrorFile=();

my $JobCount;
my $job;
my $PrintKounter;

my $cwd="-empty-";

my @files=();
my $filename;
my $x;
my $rtn;
my $dashes = "----------------------------------------------------------------------";

# For Query
my @acctsToRun = ();
my $MainAccount="";
my $MainWorkAreaFolder="";
my $chgAccts = "false";
my $queryCount = 0;
my $warnSplsh = "WARNING! WARNING! WARNING! WARNING! WARNING! WARNING! WARNING!";
my ($PublishProductID, $AssociateProductID);


#=============================================	
# MAIN()
#=============================================	
	if(!Housekeeping())			{ ErrorExit("Housekeeping\(\)", "FAILED!"); }
	if(!Set_Gaccts_CONFIG()) 	{ ErrorExit("Set_Gaccts_CONFIG\(\)", "FAILED!"); }
#	if(!BackupJobs()) 			{ ErrorExit("BackupJobs\(\)", "FAILED!"); }
	if(!InitWorkAreaFolders())	{ ErrorExit("InitWorkAreaFolder\(\)", "FAILED!"); }

	if(-e "_XMLPrep_SPLITACCT.txt") { unlink "_XMLPrep_SPLITACCT.txt"; }		# DELETE previous

	if($#acctsToRun > 0) {
		system `echo SPLITACCT > _XMLPrep_SPLITACCT.txt`;	# Split account, so create file
		foreach (@acctsToRun) {
			system `echo $_ >> _XMLPrep_SPLITACCT.txt`;
		}
	}
			
	foreach (@acctsToRun) {
		printOut("\n\n$dashes\nPROCESSING:  $_\n$dashes\n");
		
		#if (length($_) eq 4)
		#{
		#	$_ = (0 . $_);
		#}	
		
		if(!ProcessAccount($_)) { ErrorExit("ProcessAccount\(\)", "FAILED!"); }
	}
	
	XMLPrepExit(0);	# success

#=============================================	
# MAIN END
#=============================================	


#=============================================================================	
# 					S U B R O U T I N E S	
#=============================================================================	
sub BackupJobs {
	my $me = whoami();
	
	if($opt_d) { printOut("+++[debug] ($me)\n\n"); }
	
	$CWD =  "$config_BackupPath";
	
	printOut("\$config_BackupPath=$config_BackupPath\n");	
	
	foreach my $BKjob (@acctsToRun)	{
		printOut("\n\tBacking up:  $BKjob...\n");	
		
			$AccountPath_acctno=$config_MasterAccountFolder . "\\" . $BKjob;
			
			#Backup Job...
			$BackupPath = "$config_BackupPath\\" . "$BKjob";
					
			if( -e ($BackupPath))	{
				if($opt_d) {printOut("\tBackup Path:\n\t$BackupPath\n");}
			} else {
				if($opt_d) {printOut("\tBackupPath NOT FOUND!\n\t$BackupPath\n\n");}
				&CreatePath("$BackupPath");
			}
		
			$CWD=$BackupPath;
		
			# Create backup subfolder name from date and time...
			&Update_LogDateTime;
			$myFolder=$LogDateTime;
			mkdir( "$myFolder", 0777) 	or die "Could NOT Make BACKUP DATE-TIME-FOLDER:  ~\\$myFolder\n$!";
						
			$CWD=$BackupPath . "\\" . $myFolder;
			
			if($opt_d) {
				printOut("\n\tCreated BACKUP FOLDER:\n");
				printOut("\t$CWD\n");
				printOut("\n\tCopying FROM ('G-Accts' Acct):\n\t$AccountPath_acctno\n");
			}
			
			$zip = Archive::Zip->new();
			
			# Copy $account file to backup folder...
			opendir(ACCTH, "$AccountPath_acctno")        or die "Couldn't open $AccountPath_acctno for reading: $!";
	
			# ADDING FILES TO TEMP FOLDER FOR BACKUP...	
			if($opt_d) {printOut("\t COPYING FILES TO FOLDER FOR BACKUP...\n");}
			
			while (defined(my $acctfile = readdir ACCTH)) {
				next if ($acctfile eq "." || $acctfile eq ".."); 
				
				$filename = "$AccountPath_acctno\\$acctfile";
				
				# Copy 'cklist'...
				if((-d $filename) and (uc($acctfile) eq "CKLIST")	)	{			
					if($opt_d) {printOut("\t\t$acctfile\n");}
					$dir_member = $zip->addDirectory( "$filename/" );
						
					opendir(CKLISTH, "$filename")	or die "Couldn't open ? for reading: $!";
						while (defined(my $ckfile = readdir CKLISTH)) {
							next if($ckfile eq "." || $ckfile eq "..");
							
							if(-d "$filename/$ckfile") {
								if($opt_d) {printOut("\t\t\t$ckfile \(DIRECTORY SKIPPED\)\n");}
								next;
							}
														
							$file_member = $zip->addFile("$filename/$ckfile");
							if($opt_d) {printOut("\t\t\t$ckfile \(ADDED TO ZIP\)\n");}
						}
					closedir CKLISTH	or die "Couldn't CLOSE CKLISTH file handle: $!";
				}
				
				# Copy 'CONFIG'...
				if((-d $filename) and (uc($acctfile) eq "CONFIG")	)	{			
					if($opt_d) {printOut("\t\t$acctfile\n");}
					$dir_member = $zip->addDirectory( "$filename/" );
						
					opendir(CONFIGH, "$filename")	or die "Couldn't open ? for reading: $!";
						while (defined(my $cfgfile = readdir CONFIGH)) {
							next if($cfgfile eq "." || $cfgfile eq "..");
							
							if(-d "$filename/$cfgfile") {
								if($opt_d) {printOut("\t\t\t$cfgfile \(DIRECTORY SKIPPED\)\n");}
								next;
							}
														
							$file_member = $zip->addFile("$filename/$cfgfile");
							if($opt_d) {printOut("\t\t\t$cfgfile \(ADDED TO ZIP\)\n");}
						}
					closedir CONFIGH	or die "Couldn't CLOSE CONFIGH file handle: $!";
				}	
				
				#Copy all files NOT folders...
				# NOTE:  $filename includes the full path, too!
				if( (!-d $filename))	{
					# $filename is NOT a DIRECTORY...
					$file_member = $zip->addFile( "$filename");
					copy($filename, "$UserPath/temp")
						or die "+++COPY Failed \n$filename  --\>  $UserPath/temp/\n\t$!";
					if($opt_d) {printOut("\t\t$acctfile\n");}
				}
			}
			
			# Save the Zip file
			unless ( $zip->writeToFileNamed("$CWD\\$BKjob.zip") == AZ_OK ) {
			   die "Write Zip Error:  $CWD\\$BKjob.zip";		
			}
			
	   		if($opt_d) {printOut("\n\tZIP FILE:\n\t$CWD\\$BKjob.zip\n");}
	
			closedir ACCTH	or die "Couldn't close ACCTH $AccountPath_acctno";
		}
	
	printOut("\n");
	
	return 1;	
}


#-------------------------------------------------------------
sub BadConfigData {
#-------------------------------------------------------------
	my ($parm1, $parm2) = @_;
	my $me = whoami();
	
	$BadConfig=1;
	
	if($opt_d) { printOut("\[$me\]\n"); }
	
	printOut("ERROR:  \<$parm1\>=$parm2\n");
}


#----------------------------------------------------------
sub CreateLog() {
#----------------------------------------------------------
	my $myDT=$LogDateTime;
	my $fn="--empty--";
	
	$myDT =~ s|(\d\d\d\d)(\d\d)(\d\d)_(\d\d)(\d\d)(\d\d)|$1\/$2\/$3 $4:$5:$6|g;
	
	open(LOG, ">:utf8",  "$CWD\\$config_LogFile")
		or die "Can't CREATE LOG file $config_LogFile: $!\n"; 
		
	print LOG "($config_LogFile)\n\nApplication:\tXMLPrep.exe\t\t$myVersion\n";
	print LOG "Date_Time:\t\t$myDT\n";
	print LOG "====================================================================================\n";

	if($opt_d) {
		print LOG " DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG \n";
		print LOG "*------------------------------------------------------------------------------*\n";
		
		print LOG "\n\n================================\n" .
			"+++[debug] EXCLUDE:\n================================\n";
		foreach my $exc (@config_ExcludeFilter)	{
			print LOG "$exc\n";	
		}
		print LOG "================================\n\n";
	} else {
		print LOG "\t(Process with '-d' switch to turn on debugging.)\n";
	}
}


#----------------------------------------------------------
sub CreatePath {
#----------------------------------------------------------
	my ($Path) = @_;
	my $me = whoami();
	my $UserShare="";
	my $mappedDrive="";
	my $folderString="";
	my @folders=();
	my $currPath="";
	
	printOut("[$me]...\n");
	
	printOut("CREATING DIRECTORY(S):  $Path...  \n");
		
	if($Path =~ /^\\\\/)	{
		$UserShare = $Path;
		$UserShare =~ s|^\\\\(.+?)\\(.+?)\\(.+?)$|\\\\$1\\$2\\|g;		#"\\server\share\"
		printOut("\t\$UserShare=$UserShare\n");
		$currPath=$UserShare;
		
		#Load @folders...
		$folderString = $Path;
		$folderString =~ s|^\\\\(.+?)\\(.+?)\\(.+?)\\(.+?)$|$3\\$4|g;	# ~\folders
		printOut("\t\$folderString=$folderString\n");
	} else {
		$mappedDrive = $Path;			
		$mappedDrive =~ s|^(.+?)(\:\\)(.+?)$|$1$2|g;
		printOut("\t\$mappedDrive=$mappedDrive\n");
		$currPath=$mappedDrive;
		
		#Load @folders...
		$folderString = $Path;
		$folderString =~ s|^(.+?)(\:\\)(.+?)$|$3|g;
		printOut("\t\$folderString=$folderString\n");
	}	
		
	@folders = split(/\\/, $folderString);
	foreach my $mysplit (@folders)	{
		printOut("\t\t$mysplit\n");
	}
	
	printOut("\n ok...Actually creating the missing directories...\n");

	if(-e $currPath)	{		# either "\\server\share\"  or  mapped drive ex. "C:\"
		local $CWD=$currPath;
		
		#create folders if they don't exist...
		foreach my $myFolder (@folders)	{
			
			printOut("\t===\> \$myFolder=$myFolder\n");
			
			if( -e $myFolder)	{
				$CWD = $CWD . "\\" . $myFolder;
				printOut("\t\tUpdated \$CWD to:  $CWD\n");
			} else {
				mkdir("$myFolder", 0777) or die "+++ Can't mkdir $myFolder\n$!\n";
				$CWD = $CWD . "\\" . $myFolder;
				printOut("\t\tCreated folder $myFolder and ...\n" . 
							"\t\tUpdated \$CWD to:  $CWD\n");
			}
		}	
		
		printOut("CREATED DIRECTORY(S)!\n\n");
		
	} else {
		ErrorExit(whoami(), "Dang!  Something is stinky in Denmark!");
	}
				
		
#	} else {
#		printOut("\n\...XMLPrep processing cancelled.\n");
#		exit 0 ;
#	}
}


#-------------------------------------------------------------
sub dateTime {
#-------------------------------------------------------------
	my ($sec,$min,$hour,$mday,$monNr,$year);
	my $now = {};
	#get date and time
	($sec,$min,$hour,$mday,$monNr,$year) = localtime(time());
    	my ($date, $date_nr, $clock);
	my $month=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")[$monNr];
    	$monNr++;
	$monNr = sprintf("%02d",$monNr);
	my $mdayNr = sprintf("%02d",$mday);
	$sec = sprintf("%02d",$sec);
	$min = sprintf("%02d",$min);
	$hour = sprintf("%02d",$hour);
	$year= 1900 + $year;
	$now->{'date'} = "$year-$month-$mday";
	$now->{'dateNr'} = "$year$monNr$mdayNr";
	$now->{'time'} = "$hour:$min:$sec";
	$now->{'HHMMSS'} = "$hour$min$sec";
	return($now);		
}


#-------------------------------------------------------------
sub Dump_Config	{
#-------------------------------------------------------------
	my $me = whoami();
	
	printOut("\n--------------------- Dump_Config: \$XMLconfig ----------------------\n");

	printOut("\$config_allusers=$config_allusers\n");
	printOut("\$username=$username\n");
	printOut("\$config_MasterTemplatesFolder=$config_MasterTemplatesFolder\n");	
	printOut("\$config_MasterAccountFolder=$config_MasterAccountFolder\n");	
	printOut("\$XMLPrepFolder=$XMLPrepFolder\n");
	printOut("\$config_LogFile=$config_LogFile\n");
	printOut("\$config_WorkArea=$config_WorkArea\n");
	printOut("\$config_BackupPath=$config_BackupPath\n");
	printOut("\$config_NewProcessFolder=$config_NewProcessFolder\n");	
	printOut("\$config_OldProcessFolder=$config_OldProcessFolder\n");	
	printOut("\$config_CKListPath=$config_CKListPath\n");
	
	printOut("\n-------------------END $me --------------------\n\n");
}


#-------------------------------------------------------------
sub Dump_Jobs	{
#-------------------------------------------------------------
	my $me = whoami();
	my $i;
		
	printOut("\n--------------------- $me: \$XMLJobs ----------------------\n");
	my $myDump = Dumper($XMLJobs);
	printOut("$myDump");
	printOut("\n-------------------END $me: \$XMLJobs --------------------\n\n");
}


#-------------------------------------------------------------
sub ErrorExit {
#-------------------------------------------------------------
	my ($mySub, $myMessage) = @_;
	
	printOut("ERROR:  [$mySub]\n\n\t$myMessage\n");
	die "Exiting application.";
}


#-------------------------------------------------------------
sub FilterFile {
#-------------------------------------------------------------
	my $parm = shift;
	my $me = whoami();
	my $FilteredParm=0;
	
#	#SELECT ONLY *. FILES (due to legacy processing on XPP)...
	if($parm !~ /\./)	{$FilteredParm = 1;}
	#$FilteredParm = grep { !/\./} $parm;
	if($parm =~ /(.+?)\.mcc$/i)	{$FilteredParm = 1;}
		
	if($opt_d) {
		printOut("\t\t$parm");
		if((length $parm) > 7)	{
			printOut("\t[\$FilteredParm=$FilteredParm]\t"); 
		} else {
			printOut("\t\t[\$FilteredParm=$FilteredParm]\t"); 
		}
	}
			
	if( $FilteredParm < 1)	{		
		if($opt_d) { printOut("--- EXCLUDED (via \$FilteredParm) ---\n"); }
		return "FILTERED";
	}
	
	#EXCLUDE FILTER ENTRIES TO TEST?...
	if($#config_ExcludeFilter > -1)	{	
		foreach my $ff (@config_ExcludeFilter)	{
			if($parm =~ /^$ff$/i)	{	#case insensitive
				if($opt_d) { printOut("--- EXCLUDED (via \@config_ExcludeFilter)---\n"); }
				return "FILTERED";
			}
		}
	}
	
	if($opt_d) { printOut("\(APPROVED\)\n"); }
	
	return "Approved";
}


#----------------------------------------------------------
sub Get_XMLPrep_config()	{
#----------------------------------------------------------
	my $me = whoami();
	my @config = ();
	
	if($opt_d) { printOut("+++[debug] ($me)\n\n"); }
		
	my $XMLPrepConfigFile = "$sAppPath" . "\\" . "$sXMLPrep_ConfigName";

	if(!-e ($XMLPrepConfigFile))	{
		ErrorExit($me, "The $XMLPrepConfigFile file does not exist!");
	}

	if($opt_d) {printOut("\$XMLPrepConfigFile=$XMLPrepConfigFile\n"); }
			
	my $xs = XML::Simple->new(
		forcearray => 1,
		KeepRoot => 0,
		KeyAttr => [ ]
	);
	
	$XMLconfig = $xs->XMLin($XMLPrepConfigFile);	
	
	$config_allusers = $XMLconfig->{allusers}->[0];
	$XMLPrepFolder = $XMLconfig->{XMLPrepFolder}->[0];
	$config_LogFile = $XMLconfig->{logfile}->[0];
	$config_MasterAccountFolder = $XMLconfig->{masteraccountfolder}->[0];
	$config_WorkArea = $XMLconfig->{workarea}->[0];
	$config_BackupPath = $XMLconfig->{backupfolder}->[0];
	$config_OldProcessFolder = $XMLconfig->{oldprocessfolder}->[0];
	$config_NewProcessFolder = $XMLconfig->{newprocessfolder}->[0];
	$config_MasterTemplatesFolder = $XMLconfig->{mastertemplatesfolder}->[0];
	
	my $REF_config_ExcludeFilter = $XMLconfig->{inputfilters}[0]->{exclude};
	@config_ExcludeFilter = @$REF_config_ExcludeFilter;

	#Verify that $config_* variables have data...
	$BadConfig=0;
	if( (substr($config_allusers, 0, 7) eq "HASH(0x") or ($config_allusers eq "") )
				{&BadConfigData("config_allusers", $config_allusers); }
	if( (substr($XMLPrepFolder, 0, 7) eq "HASH(0x") or ($XMLPrepFolder eq "") )
				{&BadConfigData("XMLPrepFolder", $XMLPrepFolder); }
	if( (substr($config_LogFile, 0, 7) eq "HASH(0x") or ($config_LogFile eq "") )
				{&BadConfigData("config_LogFile", $config_LogFile); }
	if( (substr($config_MasterAccountFolder, 0, 7) eq "HASH(0x") or ($config_MasterAccountFolder eq "") )
				{&BadConfigData("config_MasterAccountPath", $config_MasterAccountFolder); }
	if( (substr($config_WorkArea, 0, 7) eq "HASH(0x") or ($config_WorkArea eq "") )
				{&BadConfigData("config_WorkArea", $config_WorkArea); }
	if( (substr($config_BackupPath, 0, 7) eq "HASH(0x") or ($config_BackupPath eq "") )
				{&BadConfigData("config_BackupPath", $config_BackupPath); }
	if( (substr($config_OldProcessFolder, 0, 7) eq "HASH(0x") or ($config_OldProcessFolder eq "") )
				{&BadConfigData("config_OldProcessFolder", $config_OldProcessFolder); }
	if( (substr($config_NewProcessFolder, 0, 7) eq "HASH(0x") or ($config_NewProcessFolder eq "") )
				{&BadConfigData("config_NewProcessFolder", $config_NewProcessFolder); }
	if( (substr($config_MasterTemplatesFolder, 0, 7) eq "HASH(0x") or ($config_MasterTemplatesFolder eq "") )
				{&BadConfigData("config_MasterTemplatesFolder", $config_MasterTemplatesFolder); }
		
	if($BadConfig > 0)	{
		ErrorExit($me, "Config file:  $XMLPrepConfigFile has some missing content.\n");
	}
	
	return 1;
}


#----------------------------------------------------------
sub Housekeeping	{
#----------------------------------------------------------
	my $me = whoami();

	&Clear;
	
	# HELP?
	if( ($opt_h) or ($opt_help) )	{
		system `XMLPrep.html`;
		return 1;
	}
	
	printOut("*------------------------------------------------------------------------------*\n");
	printOut("RUNNING:  XMLPrep.exe\t\t$myVersion\n");
	printOut("*------------------------------------------------------------------------------*\n\n");
	printOut("\t(For Help run:  XMLPrep --help)\n");
	
	if($opt_d) {
		printOut("\tDEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG \n");
		printOut("*------------------------------------------------------------------------------*\n");
	} else {
		printOut("\t(Process with '-d' switch to turn on debugging.)\n\n");
	}
	
	if(!print_opts() ) { ErrorExit("print_opts", "Problem printing out 'input' options."); }
	
	printOut("*------------------------------------------------------------------------------*\n\n");
	
	if($opt_job ne "-none-") {
	 	if($opt_new or $opt_old)	{
	 		printOut("Ready to process the following Account(s):\n");
	 	} else {
			printOut("\tCOMMAND LINE: ERROR! ERROR! ERROR! ERROR! ERROR!\n");
			printOut("\tJOB = $opt_job\n\n");	
			printOut("ONE OF THE FOLLOWING REQUIRED:\n");
			printOut("\t'--new'\t(new WORKAREA process) = $opt_new\n");	
			printOut("\t'--old'\t(old WORKAREA process) = $opt_old\n");	
			printOut("\n*------------------------------------------------------------------------------*\n");
			ErrorExit($me, "Command line error.  Please check your parameters.");
		}
	} else {
		printOut("\tCOMMAND LINE: ERROR! ERROR! ERROR! ERROR! ERROR!\n");
		printOut("\t'--job=$opt_job is not valid.\n");
		printOut("\n*------------------------------------------------------------------------------*\n");
		return 0;
	}
		
	# QUERY THE DATABASE FOR NUMBER OF ASSOCATED JOBS
	$queryCount = QueryDb($opt_job);
	
	if($chgAccts eq "true")	{	
		printOut("\n$dashes\n$warnSplsh\n$dashes\n\n");
		printOut("\tThe PUBLISH PRODUCT ID you should be using is: $PublishProductID\n\n");
		printOut("\t(Account No. $AssociateProductID is an ASSOCIATE product ID)\n");
		printOut("\n$dashes\n$dashes\n\n");
		return 0;
	}
	
	if($#acctsToRun lt 0)	{
		push @acctsToRun, $opt_job;	
	}
	
	foreach (@acctsToRun) {
		printOut("\t$_\n");
	}
	
	
	# Process the files or quit?
	printOut("\n----------------------------------------------------------------\n" . 
		" Y = YES\t\t--OR--\t\t(blank\) or N = CANCEL\n" . 
		"\n----------------------------------------------------------------\n\n");
	
	if($opt_topause eq "pause") {	
		$x = &ReadKey;
	} else {
		$x = "Y";
	}
	
	if($x=~ /[Yy]/) {
		# do nothing...
		printOut("\n");	#add a little space before processing the files
	} else {
		ErrorExit(whoami(), "\...Processing cancelled.\n");
		exit 0 ;
	}

	if($opt_d and $opt_job)	{
		printOut("\nDumping GetOptions Data:\n");
		printOut("\$opt_d (debugging) = $opt_d\n");
		printOut("\$opt_job (job number) = $opt_job\n");	
		printOut("\$opt_new (new WORKAREA process) = $opt_new\n");	
		printOut("\$opt_old (old WORKAREA process) = $opt_old\n");	
	}
		
	if(!Get_XMLPrep_config()) { exit 0; }
	
	$MainAccount=$acctsToRun[0];
	$MainWorkAreaFolder = $config_WorkArea . "\\" . $MainAccount;	
	
	if($opt_d) { printOut("\n+++[debug] \$MainWorkAreaFolder=$MainWorkAreaFolder\n"); }
		
	#====================
	# Setup XMLPrepTemp folder...
	#====================
	# Set $CWD to $XMLPrepFolder location...
	$Path = "$config_allusers\\$username\\$XMLPrepFolder";
	
	if( -e "$Path")	{
		if($opt_d) { printOut("[$me]\n\tPath Exists!\n\t$Path\n\n"); }
	} else {
		printOut("[$me]\n\tNOT FOUND!\n\t$Path\n\n");
		&CreatePath("$Path");
	}
			
	$CWD=$Path;
	$UserPath=$Path;

	#====================
	# Setup LOGS folder...
	#====================
	$Path = $Path . "\\LOGS";
	
	if(!-e "$Path")	{
		mkdir("LOGS", 0777) or die "Could NOT Make Directory:  ~\\LOGS\n$!\n";
	}
	
	$CWD=$Path;
	if($opt_d) {printOut("[$me]\n\t\$CWD=$CWD\n\n");}

	&CreateLog();
	if($opt_d) {printOut("\$CWD=$CWD\n");	}

	#====================
	# Read XMLPrepfile ...
	#====================	
	$Path = substr($Path, 0, -5);
	$CWD=$Path;
	
	if($opt_d) {printOut("[$me]\n\t\$CWD=$CWD\n\n");}

	return 1;
}


#-------------------------------------------------------------
sub InitWorkAreaFolders {
#-------------------------------------------------------------
	my $me = whoami();

	if($opt_d)	{	
		printOut("\n\n$dashes\n\t$acct\n");
		printOut("\$CWDSAVED=$CWDSAVED\n");	
		printOut("\$CWD=$CWD\n");
		printOut("\$MainAccount=$MainAccount\n");
		printOut("\$MainWorkAreaFolder=$MainWorkAreaFolder\n");
		printOut("=====\n");
	}
										
	#CREATE ACCOUNT PATH IF NECESSARY...
	if(!-e $MainWorkAreaFolder)	{
		if($opt_d) {printOut("\n===>\tCREATING WORKAREA ACCOUNT PATH $MainWorkAreaFolder\n\n");}
		chdir $config_WorkArea;
		mkdir($MainAccount, 0777)	or die "+++ Can't mkdir $MainAccount\n$!";
	}   	

	$CWDSAVED = $CWD;
	$CWD = $MainWorkAreaFolder;		# WORKAREA + ACCOUNT#
	printOut("\n\$CWDSAVED=$CWDSAVED\n");	
	printOut("\$CWD=$CWD\n\n");	

	if(!InitWorkAreaFolder($MainWorkAreaFolder, "gc") )		{ ErrorExit("InitWorkAreaFolder", "Problem with:\n\tPATH=$CWD\n\tFOLDER=XML"); }	
	if(!InitWorkAreaFolder($MainWorkAreaFolder, "mag") )	{ ErrorExit("InitWorkAreaFolder", "Problem with:\n\tPATH=$CWD\n\tFOLDER=XML"); }	
	if(!InitWorkAreaFolder($MainWorkAreaFolder, "XML") )	{ ErrorExit("InitWorkAreaFolder", "Problem with:\n\tPATH=$CWD\n\tFOLDER=XML"); }
	if(!InitWorkAreaFolder($MainWorkAreaFolder, "RTF") )	{ ErrorExit("InitWorkAreaFolder", "Problem with:\n\tPATH=$CWD\n\tFOLDER=RTF"); }

	return 1;	
}


#-------------------------------------------------------------
sub InitWorkAreaFolder {
#-------------------------------------------------------------
	my $me = whoami();
	my $WAFolder = shift;
	my $folder = shift;
	
	my $WACWDSAVED=$CWD;
	$CWD=$WAFolder;
	
	if($opt_d){printOut("\$CWD=$CWD\n");}
	
	if(-e "$WAFolder/$folder")	{
		if($opt_d){printOut("\n[debug] ($me) \tWORKAREA ACCOUNT FOLDER EXISTS:  $folder\n");}
		printOut("Removing contents of $WAFolder\\$folder...");
		
		# Added logic here to clear out only appropriate folders based on --new/--old
		if ($opt_new gt 0 && $folder ne "mag"){
			rmtree("$WAFolder/$folder", {keep_root => 1} );	# keeps folder name...
			printOut("DONE!\n");
		}
		elsif ($opt_old gt 0 && $folder eq "mag"){
			rmtree("$WAFolder/$folder", {keep_root => 1} );	# keeps folder name...
			printOut("DONE!\n");
		}	
		else {
			printOut("SKIPPED!\n");
		}
	} else {
		# Create Folder...	
		if($opt_d){printOut("\n[debug] ($me) \tCREATING WORKAREA ACCOUNT FOLDER:  $folder\n");}
		printOut("Creating workarea folder $folder...");
		mkdir($folder, 0777)	or die "+++ Can't mkdir $MainAccount\\$folder\n$!";
		printOut("DONE!\n");
	}
	
	$CWD=$WACWDSAVED;
	
	return 1;
}


#-------------------------------------------------------------
sub listDir {
#-------------------------------------------------------------
#list the contents of a folder
	my ($folder) = @_;
	my $file;
	
	printOut("listing folder and content: $folder\n");
	
	opendir DIR, $folder or die "*** ERROR ***\nCouldn't open folder $folder\n$!";

	printOut("============================================================================\n");
	printOut("     FOLDER LISTING FOR:  $folder\n");
	printOut("============================================================================\n");

	while ($file = readdir DIR) {
		next if ($file =~m#^\.$#);
		next if ($file =~ m#^\.\.$#);
		printOut("$file\n");
	}
}


#-------------------------------------------------------------
sub LoadContentsVAR {
#-------------------------------------------------------------
	my ($infile) = @_;
	my $me = whoami();
	
	open INPUT, "<$infile" or die "[$me] Can't open $infile \n$!";
	undef $/;
	$contents = <INPUT>;
	close INPUT;	
	$/ = "\n";     #Restore for normal behaviour later in script
}


#-------------------------------------------------------------
sub print_opts ()	{
#-------------------------------------------------------------
	my $me = whoami();
	
	if($opt_job)		{ printOut(" --job=$opt_job"); }
	if($opt_new)		{ printOut(" --new"); }
	if($opt_old)		{ printOut(" --old"); }
	if($opt_topause)	{ printOut(" --topause=$opt_topause"); }
	
	printOut("\n\n");
	
	return 1;
}


#-------------------------------------------------------------
sub printOut {
#-------------------------------------------------------------
	my $parm = shift;
	my @FILEHANDLE = (*STDOUT, *LOG);
	
	foreach my $outfile (@FILEHANDLE) {
		if(fileno($outfile)) {
			print $outfile ("$parm");		
		}
	}
}


#-------------------------------------------------------------
sub ProcessAccount {
#-------------------------------------------------------------
	my $me = whoami();
	my $acct = shift;
	my $newFile="";
	my $splitAccounts=0;
	my($num_of_files_and_dirs, $num_of_dirs, $depth_traversed);
				
	if($#acctsToRun > 0) {
		$splitAccounts=1;
	} else {
		$splitAccounts=0;
	}		
		
	if($opt_new) {
		$str1=$MainWorkAreaFolder . "\\" . $config_NewProcessFolder;	
	} else {
		$str1=$MainWorkAreaFolder . "\\" . $config_OldProcessFolder;	
	}
	
	$AccountPath_acctno=$config_MasterAccountFolder . "\\" . $acct;
	$WorkArea_acctno=$str1;	
	
	if($opt_new) {	
		printOut("\n\n***\t\$WorkArea_acctno (NEW):\t$WorkArea_acctno\n\n");
	} else {
		printOut("\n\n***\t\$WorkArea_acctno (OLD):\t$WorkArea_acctno\n\n");
	}

	#COPY files to $str1  (WORKAREA ACCOUNT NEWPROCESS PATH...
	printOut("\tCOPY FROM:	$AccountPath_acctno\n");
	printOut("\tCOPY TO:	$str1\n");
	
	opendir(TEMPH, "$AccountPath_acctno")        or die "Couldn't open $AccountPath_acctno for reading: $!";

	while (defined($filename = readdir TEMPH)) {
		next if($filename eq "." || $filename eq ".."); 

		# Copy 'config folder'...(ONLY FROM PRIMARY ACCOUNT)
		if( (-d "$AccountPath_acctno/$filename") and (uc($filename) eq "CONFIG") )	{	

			if($acct eq $opt_job) {	# PRIMARY acct's CONFIG?...
				printOut("\tCopying PRIMARY ACCOUNT FOLDER:  $filename... ");

# my($num_of_files_and_dirs,$num_of_dirs,$depth_traversed) = dircopy($orig,$new);						
				($num_of_files_and_dirs,$num_of_dirs,$depth_traversed) = dircopy("$AccountPath_acctno\\$filename",  "$str1\\$filename")
					or die "Can not 'dircopy' folder:  $filename\n" . "\t$AccountPath_acctno\\$filename" . 
						"\tto $str1\$filename" .
						"\t=====================\n" .
						"FilesAndDIRs=$num_of_files_and_dirs\nDIRs=$num_of_dirs\nDepth_Traversed=$depth_traversed\n" .
						"\t=====================\n" . "\n $!\n"; 
				printOut("DONE.\n");
				
				# IF SPLIT-ACCOUNT then update the _PubMap.xml filenames with "a_#####"...
				if($splitAccounts)	{
					printOut("\t...SPLIT ACCOUNT UPDATE FILE:  \n\t\t$str1\\CONFIG\\ _PubMap.xml...\n");
				 	if(!Update_PubMap("$str1\\CONFIG\\_PubMap.xml", $acct)) { 
				 		ErrorExit("Update_PubMap\($str1\\CONFIG\\_PubMap.xml, $acct\)", "FAILED!"); 
				 	}
				 	printOut("\t\t...SPLIT ACCOUNT UPDATE FINISHED.\n\n");
				 }				
				
			} else {
				#do nothing since it's not a CONFIG from a PRIMARY account.	
			}
		}

		next if( -d ("$AccountPath_acctno\\$filename"));
		
		if( ! -d "$AccountPath_acctno/$filename" )	{	# $filename is NOT a DIRECTORY...

			$FileFilterFlag = FilterFile($filename);
			if( $FileFilterFlag ne "Approved")	{
				next;
			}

			if($splitAccounts) {
				$newFile = $str1 . "\\" . ($WFAlpha{$WFprefix}) . "_" . $acct . "_" . $filename;
			} else {
				$newFile = $str1 . "\\" . $filename;
			}
			
			copy("$AccountPath_acctno/$filename", $newFile)
				or die "\n\[WORKAREA NEW\] +++\n\tCOPY FAILED\n\t$AccountPath_acctno/$filename  --\>  $newFile\n$!";
		}
	}
	
	closedir TEMPH	or die "Couldn't close TEMPH ($AccountPath_acctno)"; 	

	$WFprefix++;

	return 1;
}


#-------------------------------------------------------------
sub ProcessAccount_SAVED {
#-------------------------------------------------------------
#	my $me = whoami();
#	my $acct = shift;
#	
#	printOut("\n\n$dashes\n\t$acct\n");
#printOut("\$CWDSAVED=$CWDSAVED\n");	
#printOut("\$CWD=$CWD\n");		
#	printOut("=====\n");
#			
#	$str1 = $config_WorkArea . "\\" . $acct;			
#							
#	#CREATE ACCOUNT PATH IF NECESSARY...
#	if(! -e $str1)	{
#		if($opt_d) {printOut("\n===>\tCREATING WORKAREA ACCOUNT PATH $str1\n\n");}
#		chdir $config_WorkArea;
#		mkdir($acct, 0777)	or die "+++ Can't mkdir $acct\n$!";
#	}   	
#
#	if($acct = $acctsToRun[0])	{
#		$CWDSAVED = $CWD;
#		$CWD = $MainWorkAreaFolder;		# WORKAREA + ACCOUNT#
#		printOut("\$CWDSAVED=$CWDSAVED\n");	
#		printOut("\$CWD=$CWD\n");	
#	}
#
#
#	if(! InitWorkAreaFolder("$CWD", "XML") )	{ ErrorExit("InitWorkAreaFolder", "Problem with:\n\tPATH=$CWD\n\tFOLDER=XML"); }
#	if(! InitWorkAreaFolder("$CWD", "RTF") )	{ ErrorExit("InitWorkAreaFolder", "Problem with:\n\tPATH=$CWD\n\tFOLDER=RTF"); }
#
#return 1;



#	if(! -e "XML")	{
#		if($opt_d){printOut("\n===>\tCREATING WORKAREA ACCOUNT FOLDER:  XML\n");}
#		chdir "XML";
#		mkdir("XML", 0777)	or die "+++ Can't mkdir $MainAccount\\XML\n$!";
#	} else {
#		$CWD=$str1 . "/XML";
#		if($opt_d){printOut("\n===>\tWORKAREA ACCOUNT FOLDER EXISTS:  XML\n");}
#		if($opt_d) {printOut("\n\tINITIALIZING:  $CWD\n\n");}
#		opendir( XMLH, "$CWD") or die "Can't OPEN XMLH --> $CWD";
#		
#		# Process Each File...
#		while (defined($filename = readdir XMLH)) {
#			next if ($filename eq "." || $filename eq ".."); 
#			unlink $filename;
#		}
#		closedir XMLH;
#	}
				
#				$CWD = $str1;
#	
#				if(! -e "RTF")	{
#					if($opt_d){printOut("\n===>\tCREATING WORKAREA ACCOUNT FOLDER:  RTF\n");}
#					chdir "RTF";
#					mkdir("RTF", 0777)	or die "+++ Can't mkdir $account[$job]\\RTF\n$!";
#				} else {
#					$CWD=$str1 . "/RTF";
#					if($opt_d){printOut("\n===>\tWORKAREA ACCOUNT FOLDER EXISTS:  RTF\n");}
#					if($opt_d) {printOut("\n\tINITIALIZING:  $CWD\n\n");}
#					opendir( RTFH, "$CWD") or die "Can't OPEN RTFH --> $CWD";
#					
#					# Process Each File...
#					while (defined($filename = readdir RTFH)) {
#						next if ($filename eq "." || $filename eq ".."); 
#						unlink $filename;
#					}
#					closedir RTFH;			}			
#	
#				$CWD = $str1;


				
#				# DELETE PRIOR CONTENTS OF 'old' and 'new' Process Folders...
#				$str1 = $CWD . "\\" . $config_NewProcessFolder;
#				
#				#NEW PROCESS FOLDER CLEANUP...
#				if(-e $str1)	{
#					$CWD=$str1;
#					if($opt_d) {printOut("\n\tNew Process Folder Cleanup...\n");}
#					opendir(TEMPNEW, $CWD)
#							or die "Couldn't open $CWD for reading: \n\t$!";
#							
#					#DELETE EACH FILE...
#					while (defined($filename = readdir TEMPNEW)) {
#						next if ($filename eq "." || $filename eq ".."); 
#						
#						if(! -d $filename)	{
#							unlink $filename	or die "NewProcess Cleanup - Can't unlink $CWD/$filename \n $!";
#							if($opt_d) {printOut("\t\t$filename has been DELETED\n");}
#						}
#					}
#					closedir TEMPNEW	or die "Couldn't close TEMPNEW ($CWD)";
#				}
#				
#	   		   	#OLD PROCESS FOLDER CLEANUP...
#				$str1 = $CWD . "\\" . $config_OldProcessFolder;
#				if(-e $str1)	{
#					$CWD=$str1;
#					if($opt_d) {printOut("\n\tOld Process Folder Cleanup...\n");}
#					opendir(TEMPOLD, $CWD)
#							or die "Couldn't open $CWD for reading: \n\t$!";
#							
#					#DELETE EACH FILE...
#					while (defined($filename = readdir TEMPOLD)) {
#						next if ($filename eq "." || $filename eq ".."); 
#	
#						if(! -d $filename)	{					
#							unlink $filename	or die "OldProcess Cleanup - Can't unlink $CWD/$filename \n $!";
#							if($opt_d) {printOut("\t\t$filename has been DELETED\n");}
#						}
#					}
#					closedir TEMPOLD	or die "Couldn't close TEMPOLD ($CWD)";
#				}
#									
#				$str1=$MainWorkAreaFolder . "\\" . $config_NewProcessFolder;	
#				
#				if($opt_new)	{	# NEW PROCESS...
#
#					$WorkArea_acctno=$str1;		
#					printOut("\n\n***\t\$WorkArea_acctno (NEW):\t$WorkArea_acctno\n\n");
#	
#					#Create WORKAREA account folder and 'gc' folder if necessary...
#					if(! -e $WorkArea_acctno)	{
#						if($opt_d){
#							printOut("===>\tCREATING WORKAREA ACCOUNT NEWPROCESS PATH - $config_NewProcessFolder\n\n");
#						}
#						chdir $MainWorkAreaFolder;
#						mkdir( $config_NewProcessFolder, 0777)	 or die "+++Can't mkdir $config_NewProcessFolder\n$!";
#					}
#					
#					$CWD=$UserPathTemp;
#					#COPY files to $str1  (WORKAREA ACCOUNT NEWPROCESS PATH...
#					printOut("\tCOPYING FILES TO WORKAREA FOLDER\n\t\t\($str1\)\n");
#					
#					opendir(TEMPH, "$UserPathTemp")        or die "Couldn't open $AccountPath_acctno for reading: $!";
#	
#					while (defined($filename = readdir TEMPH)) {
#						next if ($filename eq "." || $filename eq ".."); 
#						
#						if( ! -d $filename)	{	# $filename is NOT a DIRECTORY...
#	
#							$FileFilterFlag = FilterFile($filename);
#							if( $FileFilterFlag ne "Approved")	{
#								next;
#							}
#	
#							copy($filename, $str1 . "\\" . $filename)
#								or die "\n\[WORKAREA NEW\] +++\n\tCOPY FAILED\n" .
#									"\t$filename  --\>  $str1\\$filename\n$!";
#							if($opt_d eq '')	{		
#								printOut("\t\t$filename\n");
#							}
#						}
#					}
#					
#					closedir TEMPH	or die "Couldn't close TEMPH ($UserPathTemp)";
#					
#				} else {	# OLD PROCESS...
#					$str1=$MainWorkAreaFolder . "\\" . $config_OldProcessFolder;	
#					$WorkArea_acctno=$str1;
#					printOut("\n\n***\t\$WorkArea_acctno (OLD):\t$WorkArea_acctno\n\n");
#					
#					#Create WORKAREA account folder and 'mag' folder if necessary...
#					if(! -e $WorkArea_acctno)	{
#						if($opt_d){
#							printOut("===>\tCREATING WORKAREA ACCOUNT OLDPROCESS PATH - $config_OldProcessFolder\n\n");
#						}
#						chdir $MainWorkAreaFolder;
#						mkdir( $config_OldProcessFolder, 0777)	 or die "+++Can't mkdir $config_OldProcessFolder\n$!";
#					}
#					
#					$CWD=$str1;
#	
#					#COPY files to $CWD  (WORKAREA ACCOUNT OLDPROCESS PATH...
#					printOut("\tCOPYING FILES TO WORKAREA FOLDER\n\t\t\($str1\)\n");
#					
#					opendir(TEMPHO, "$UserPathTemp")        or die "Couldn't open $AccountPath_acctno for reading: $!";
#					
#					while (defined($filename = readdir TEMPHO)) {
#						next if ($filename eq "." || $filename eq ".."); 
#						
#						if( ! -d $filename)	{	# $filename is NOT a DIRECTORY...
#							
#							$FileFilterFlag = FilterFile($filename);
#							if( $FileFilterFlag ne "Approved")	{
#								next;
#							}
#							
#							copy($UserPathTemp ."\\" . $filename, $CWD . "\\" . $filename)
#								or die "\n\[WORKAREA OLD\] +++\n\tCOPY FAILED\n" .
#									"\t$filename  --\>  $CWD\\$filename\n$!";
#																	
#							if($opt_d eq '')	{		
#								printOut("\t\t$filename\n");
#							}
#						}
#					}
#					
#					closedir TEMPHO	or die "Couldn't close TEMPH ($UserPathTemp)";
#				}   	
#	
#	return 1;	
}


#----------------------------------------------------------
sub QueryDb {
#----------------------------------------------------------
	my $me = whoami();
	my $acctNum = shift;
	my $ct=0;
	
	# SQL INFO
	my $dsn=q/dbi:ODBC:BIS/;	#system DSN
	my $user=q/BIS_dbreader/;
	my $pwd=q/ReadOnly1/;
	my $dbh;	#database handle
	my $sth;	#statement handle
	my $query="";	
	
	# QUERY CONFIG VARIABLES
	$dbh->{ReadOnly} = 1;
	
	# Connect to the data source and get a handle for that connection.
	$dbh = DBI->connect($dsn, $user, $pwd)  
		or die "[$me] Can't connect to $dsn: $DBI::errstr\n$!";		

	# QUERY...
	$query = "SELECT (SELECT SplitWebPublishProductID FROM BIS_Products WHERE ProductID = $acctNum) AS PublishProductID, 
					[AssociateProductID]
			FROM [BIS].[dbo].[SplitWebPubAssociations]
			WHERE PrimaryProductID = $acctNum";

	$sth = $dbh->prepare($query) or die "[$me] Can't prepare query: $query\n$!";
	$sth->execute;
	
	# BIND TABLE COLUMNS TO VARIABLES
	$sth->bind_columns(undef, \$PublishProductID, \$AssociateProductID) or die "[$me] Could not bind_columns for SplitWebPublish stuff\n$!\n";

	# LOOP THROUGH RESULTS	
	while ($sth->fetch())	{
		#printOut("...PublishProductID:  $PublishProductID - AssociateProductID: $AssociateProductID\n");
		
		# Check to see if ID is Primary
		if ($PublishProductID eq $AssociateProductID) {
			# Index 0 should be primary
			unshift @acctsToRun, $AssociateProductID;
			}	
		else {
			push @acctsToRun, $AssociateProductID;
		}
	}	
		
	$ct++;
	$chgAccts = "true" if($PublishProductID ne $AssociateProductID && $acctNum eq $AssociateProductID);
	
	
	
	
	
	
	# Close the connection...
	$sth->finish();
	
	return $ct;
}


#----------------------------------------------------------
sub Set_Gaccts_CONFIG {
#----------------------------------------------------------
	# FIX LEGACY USE OF R:\ACCTS, AS CONFIG DATA LOCATION, TO G:\ACCTS....
	my $me = whoami();
	my $pathstr="";
	my $file="";
	my $ToPath="";
	my @copyArgs=();
	
	my $SaveCWD=$CWD;
	
	if($opt_d) { Dump_Config(); }

	if(!-e $config_MasterAccountFolder)	{ ErrorExit("$me", "Path Not FOUND:  $config_MasterAccountFolder"); }

	$CWD = ($config_MasterAccountFolder . "\\" . $opt_job);
	printOut("\$CWD=$CWD\n");
	
	# Make sure $config_MasterAccountFolder has a CONFIG folder...
	if(!-e "$CWD\\CONFIG")	{
		if($opt_d) { printOut("\nCONFIG NOT FOUND \(creating it\) in $CWD\n"); }
		mkdir "CONFIG";
	} else {
		if($opt_d) { printOut("\nCONFIG FOUND in $CWD\n"); }
	}
	
	# CD to CONFIG folder...
	$CWD = ($CWD . "\\CONFIG");
	
	if($opt_d) { printOut("\$CWD=$CWD\n"); }
	
	$ToPath=$CWD;

	# Set $pathstr if $config_WorkArea\(gc/mag)\CONFIG EXISTS, else (blank)...
	if($opt_new)	{ $pathstr = $MainWorkAreaFolder . "\\gc\\CONFIG"; }
	if($opt_old)	{ $pathstr = $MainWorkAreaFolder . "\\mag\\CONFIG"; }

#	if($opt_d) { printOut("+++[debug] A \$pathstr=$pathstr\n"); }
	
	if(-e $pathstr) {
		$CWD=$pathstr;
	} else {
		$pathstr="";
	}
	
#	if($opt_d) { printOut("+++[debug] B \$pathstr=$pathstr\n"); }
	
	if($pathstr ne "") {
		# R:\accts\#####\(gc/mag)\CONFIG folder EXISTS...
		$CWD=$pathstr;	# CD to R:\accts\#####\(gc/mag)\CONFIG folder...
				
		printOut("Updating Job CONFIG folder:  $opt_job\\CONFIG From $pathstr...\n");
		opendir(MYDIR, $pathstr) or die "Can't opendir $pathstr: $!";
		
		while(defined($file = readdir(MYDIR)))	{
			next if($file eq ".");
			next if($file eq "..");
			
			if(!-e ("$ToPath\\$file") )	{
				copy($file, $ToPath) or die "Can't COPY $file to $ToPath\n$!";		
				printOut("\t...$file\n");

# THIS SHOULD HAPPEN TO FILE AFTER IT HAS BEEN COPIED TO R:\ACCTS\#####\CONFIG:								
#				if( (uc("$file") eq "_PUBMAP.XML") and ($#acctsToRun gt 0) )	{	# SPLIT ACCOUNT...
#					printOut("\t\t...SPLIT ACCOUNT UPDATE FILE:   $file...\n");
#				 	if(!Update_PubMap("$ToPath\\$file")) { ErrorExit("Update_PubMap\($ToPath\\$file\)", "FAILED!"); }
#				 	printOut("\t\t...SPLIT ACCOUNT UPDATE FINISHED.\n\n");
#				 }
			} else {
				printOut("\t...$file \(EXISTS - not copied\)\n");
			}
		}
		close(MYDIR);
	}
	
	$CWD = $config_MasterTemplatesFolder;
	printOut("\n\$CWD=$CWD\n");
	printOut("Updating Job: " . $opt_job . "'s CONFIG folder From $config_MasterTemplatesFolder...\n");
	opendir(MYDIR, $config_MasterTemplatesFolder) or die "Can't opendir $config_MasterTemplatesFolder: $!";
	
	while(defined($file = readdir(MYDIR)))	{
		next if($file eq ".");
		next if($file eq "..");
		
		if(!-e "$ToPath\\$file" )	{
			copy($file, $ToPath) or die "Can't COPY $file to $ToPath\n$!";		
			printOut("\t...$file\n");
		} else {
			printOut("\t...$file \(EXISTS - not copied\)\n");
		}
	}
	
	close(MYDIR);
		
	printOut("\n");
	
	$CWD=$SaveCWD;
	
	if($opt_d) { printOut("+++[debug] CWD=$CWD\n"); }	
	
	return 1;
} 


#-------------------------------------------------------------
sub Update_LogDateTime{
#-------------------------------------------------------------
	$nowYYYYMMDD_HHMMSS= dateTime();
	$LogDateTime =	$nowYYYYMMDD_HHMMSS->{'dateNr'} .  "_" . $nowYYYYMMDD_HHMMSS->{'HHMMSS'};
}


#-------------------------------------------------------------
sub Update_PubMap {
#-------------------------------------------------------------
	my $me = whoami();
	my $PMfile = shift;
	my $PMacct = shift;
	my $line = "";
	my ($name,$path,$suffix) = fileparse($PMfile);
	my $PMnew = $path . "_PubMap_NEW.xml";
	my $prefix = "a_" . $PMacct . "_";
	
	open(FILE, "<:utf8",  "$PMfile") or die "Can't read file '$PMfile' [$!]\n";  
	open(OUT, ">:utf8",  "$PMnew") or die "Can't open file for output:  '$PMnew'\n[$!]\n";

	printOut("$dashes\n");
	printOut("FILE:  _PubMap - correcting for split-account processing...\n");
	
	while(<FILE>) {
		 my($line) = $_;
		chomp($line);			# Strip the trailing newline from the line.
		
		# Get header stuff...
		if(	($line =~ /^<\?xml/ )		|
			($line =~ /^<\!--/)
			) { 
			print(OUT "$line\n");
			printOut("$line\n");
			next;
		}
		
		if( 	( $line =~ /<PubMap>/ )	|
			( $line =~ /<\/PubMap>/ )	|
			($line =~ /<default>/ )		|
			( $line =~ /<\/default>/ )	|
			( $line =~ /<oh\d>/ )		|
			( $line =~ /<\/oh\d>/ )		|
			( $line =~ /<a_(.+?)>/ )			# already converted for split-account
			)	{
				
			print(OUT "$line\n");
			printOut("$line\n");	
			next;		
		}	
		
		# convert for split-account...
		if($line !~ /<\//)	{
			$line =~ s|<(.+?)>|<$prefix$1>|g;
		} else {
			$line =~ s|<(\/)(.+?)>|<$1$prefix$2>|g;
		}
		
		print(OUT "$line\n");
		printOut("$line\n");			
	}

	printOut("$dashes\n");
	
	close(OUT);
	close (FILE);  	
	
	if( (-e $PMfile) and (-e $PMnew) ) {	
		if(rename($PMfile, "$path\\_PubMap_ORIG.xml") ) {
			printOut("Renamed $PMfile to " . $path . "_PubMap_ORIG.xml\n");
			
			if(rename($PMnew, $path . "_PubMap.xml") ) {
				printOut("Renamed $PMnew to " . $path . "_PubMap.xml\n");
			} else {
				printOut("!!! Rename FAILED:  $PMnew to " . $path . "_PubMap.xml\n");
			}
		} else {
			printOut("!!! Rename FAILED:  $PMfile to " . $path . "_PubMap_ORIG.xml\n");
		}
	}
	
	printOut("FINISHED correcting for split-account processing.\n");
	printOut("$dashes\n");
	
	return 1;
}


#-------------------------------------------------------------
sub whoami {
#-------------------------------------------------------------
	(caller(1))[3];
}


#----------------------------------------------------------
sub XMLPrepExit  {
#----------------------------------------------------------
	 my $me = whoami();
	 my $status = shift;
	 
	 
	 if($status == 0) {
	 	printOut("\n\n============================================================\n");
	 	if($opt_d) {
			printOut("[$me]\n\t----- FINISHED!  [\$status=$status] -----\n");
		} else {
			printOut("\t----- FINISHED! -----\n");
		}
	 } else {
		do_Error(whoami(), "----- ERROR!  [\$status=$status]-----\n");
	}
	 
 	printOut("============================================================\n");
	close(LOG);

	if($opt_topause ne "nopause") {
		my $NotePadRTN = system("NotePad $Path\\LOGS\\$config_LogFile");
	}

	exit($status);
}


#====================================================================================
#====================================================================================
#POD Documentation
#
# To convert this documentation to an html file, type "C:\>pod2html XMLPrep.pl > XMLPrep.html" <enter>
# You will probably want to tweak the output a little.
#====================================================================================
#====================================================================================

=head1 XMLPrep.exe Documentation

=head2 The XMLPrep.exe is used to do the following:

=over 4

=item 1.
Read the XMLPrep_config.xml file for UNC paths and settings, \&c.

=item 2.
Zip all of the files for each Job specified, named as "JobNumber".zip and place it in the config files Backup Path/JobNumber/Date_Time/ folder.

=back

=head2 Command-line:

At your DOS prompt, enter one of the following examples, changing the Job number, of course:

C:\>XMLPrep --job=12345 --new

C:\>XMLPrep --job=12345 --old

(NOTE:  Remember that you can also add the -d switch to aid debugging.)

=back

=cut
