#!/usr/bin/perl
# XMLPrep.pl (TESTING WITH ACCOUNT NO. "13783 s:14745, s:16780")
# Programmer:  Raymond Lillibridge
my $myVersion = "Version:  2013-11-20.1 (WIP)";

# WIP:
# 2013-11-19.1 - Raymond Lillibridge - Lisa say that 'split accounts' are not processing
#				Using V:\AcctsG 19980 and 13907 (Tallahassee) for testing
# 2013-11-07.1 - Raymond Lillibridge - Setup to run using %APPSDRIVE% & 
#				new version of config file:  V:\Apps\config\XMLPrep2.xml (TEMPORARILY)
# .....
# NEW STUFF...
#	<oldprocessfolder>mag</oldprocessfolder>	Convert to global value
#	<newprocessfolder>gc</newprocessfolder>	Convert to global value

# LEGACY STUFF...
#<!-- LIVE -->
#<!--
# <mastertemplatesfolder>R:\Master\XMLPrepFiles</mastertemplatesfolder>	= %APPSDRIVE%\Master\XMLPrepFiles
# <GenCodePath>G:\Accts</GenCodePath>								= %GENCODEPATH%
# <WorkPath>R:\Accts</WorkPath>										= %WORKPATH%
# <accountfolder>\\mcc-file-01\accounts\accts</accountfolder> 				(not used)
# <backupfolder>\\mcc-archive-01\mcc_archive\Take_Backup</backupfolder>	(not used)
# <foliofolder>\\mcc-file-01\folio\accts</foliofolder>						(not used)
# <oldprocessfolder>mag</oldprocessfolder>								(make global variable)
# <newprocessfolder>gc</newprocessfolder>								(make global variable)
#-->	

# 2013-10-01.1 - Raymond Lillibridge - Allow passing in --nopause option

#=============================================	
# CHANGE HISTORY:
# 2013-09-03.1 - Raymond Lillibridge - Problem with G2h_Dev running this and $CWDSAVE = R:\
#				ENLIGHTENMENT!!! SOMEBODY SET V:\APPS\CONFIG\XMLPREP_CONFIG.XML TO USE "live"
# 2013-05-29.1 - Raymond Lillibridge - For some reason DEV processing is accessing R:\ (LIVE) stuff and updating G:\Accts (it shouldn't)
#				ENLIGHTENMENT!!! ->  SEE:  R: or V: \Apps\Config\XMLPrep_config.xml
# 2011-08-26.1 Raymond Lillibridge - Saving Job No. of all split accounts in _XMLPrep_SPLITACCT.txt
# 2011-08-25.1 Raymond Lillibridge - Saving 'state' for split account processing by creating _XMLPrep_SPLITACCT.txt
# 2011-08-24.1 Raymond Lillibridge - [NOT WORKING] Added setting of ENVIRONMENTAL Variable %SPLITACCT%.  Used by G2H_XP.bat
#                                                Win32-AdminMisc module installed to set ENVIRONMENTAL Variable

#===============================================	
# PROCESSING OUTLINE
#===============================================	
#------------------------------------------------------------
# ● [DONE] File Combination
#		o Query DB to see if account needs to be combined into one working area
#		o Copy CONFIG in from PRIMARY account into G:\accts\~\CONFIG
#		o Copy CONFIG in from MASTER area into G:\accts\~\CONFIG
# ● [DONE] Config File Transport (makes g:\accts\...\ true source)
#		o Now that g:\ is the true source directory, any changes made to CONFIG files must be copied back to g:\
#		o If “R:\ACCTS\12345\gc\CONFIG” exists && “G:\ACCTS\12345\CONFIG” does not exist
# 			Initialize “G:\ACCTS\12345\CONFIG” with the contents of “R:\ACCTS\...\CONFIG”
#		o Else If neither R:\...\CONFIG or G:\...\CONFIG exist
# 			Initialize “G:\ACCTS\12345\CONFIG” with default pubmap and xref config files
# ● [DONE] Back-up Contents of G:\ to Y:\ (same logic as specstriage)
# ● [DONE] Clear-out Entire Working Folder Area
#		o Re-initialize folder with blank subdirectories
# ● [DONE] Copy CONFIGS and GENCODE From G:\ to R:\
#		o Filter-out unnecessary files (current FilterFile() logic)
#		o Allow for “.mcc” extensions (current FilterFile() logic)
#		o Copy in CONFIG files (NEW)
# 		o R:\ IF SPLIT-ACCOUNT then update _PubMap.xml file with "a_#####" prefix IF fn NOT begins with "a_"
#
#
#------------------------------------------------------------
# SEPERATE PROGRAMS REQUIRED:
#------------------------------------------------------------
# ● HTML_LocalSetup.exe to check the %ENV->SPLITACCT and copy images accordingly (report duplicate image names)
# ● GCspecsReturn.exe - Copy Back Gencode & Configs (G2H.bat - After Validation of IDs has been approved)
#		o Now that g:\ is the true source directory, any changes made to CONFIG files must be copied back to g:\ along with the cleaned gencode files
# ● GCdelspecs.exe - “Remove Specs” GUI application process selected (filtered) files in folder.
#=============================================	

#=============================================	
#	Config Name and Location...
#=============================================	
if (!defined $PerlApp::VERSION) {
		$APPpath = $0;
}	else {
		$APPpath = PerlApp::exe();
}

my $AppsDrive = 		$ENV{APPSDRIVE};
my $MasterTemplatesFolder = "$AppsDrive\\Master\\XMLPrepFiles";
my $GenCodePath = 	$ENV{GENCODEPATH};
my $WorkPath = 		$ENV{WORKPATH};
my $XMLPrepConfigFile = "$AppsDrive\\APPS\\CONFIG\\XMLPrep_excludes.xml"; 	# Now it contains ONLY 'excludes'
my $oldprocessfolder = "mag";
my $newprocessfolder = "gc";

#umask 0777;

#=============================================	
# PRAGMA
#=============================================	
use strict;
use warnings;
no warnings 'uninitialized';

use XML::Simple qw(:strict);
use Data::Dumper;
use DBI;
use File::Basename;
use File::chdir;
use File::Copy;
use File::Copy::Recursive qw(dircopy);
local $File::Copy::Recursive::SkipFlop = 1;
use File::Glob;
use File::Listing qw(parse_dir);
use File::Path;
use Win32;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );

#for development purposes...
use Term::InKey;
use Getopt::Long;

my $opt_d;				#-d			debug switch
my $opt_job="-none-";	# --job		job or account_number
my $opt_new=0;			# --new		new_process
my $opt_old=0;			# --old		old_process
my $opt_topause="";		# --topause	pause or nopause
my $opt_h;
my $opt_help;

GetOptions(
	"d"			=>	\$opt_d,
	"job=s" 		=>	\$opt_job,
	"new"		=>	\$opt_new,
	"old"		=>	\$opt_old,
	"topause=s"	=>	\$opt_topause,
	"h"			=>	\$opt_h,
	"help"		=>	\$opt_help
);

#=== Win32 ===
use Cwd;
my $CWDSAVED;
my $WinLoginName = getlogin || getpwuid($<) || "Win Login-UNKNOWN-";

# Date and Time vars
my($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime;
$year += 1900;
my $now_string = localtime;
my $nowYYYYMMDD_HHMMSS= dateTime();
my $LogDateTime =	$nowYYYYMMDD_HHMMSS->{'dateNr'} .  "_" . 
					$nowYYYYMMDD_HHMMSS->{'HHMMSS'};

#=============================================	
# GLOBALS
#=============================================
#my $username=$WinLoginName;
my $UserPath="";
my $UserPathTemp="";
my $contents;
my $XMLPrepExists=0;
my $WFprefix=1;
my %WFAlpha = (
        1 => "a",
        2 => "b",
        3 => "c",
        4 => "d",
        5 => "e"
    );

my $Path;
my $BackupPath;
my $AccountPath_acctno;
my $WorkPath_acctno;
my $acct;
my $myFolder;
my $AcctConfigFile;
my $str1;
my $str2;
my $zip;
my $file_member;
my $dir_member;
my $FoundSpecsHeader="0";

my $XMLconfig;
my $XMLJobs;

# config values:
my $config_CKListPath="";
my @config_ExcludeFilter=();

my $BadConfig=0;
my $FileFilterFlag="skip";

#my @account=();
my @newprocess=();
my @ErrorFile=();

my $JobCount;
my $job;
my $PrintKounter;

my $cwd="-empty-";

my @files=();
my $filename;
my $x;
my $rtn;
my $dashes = "----------------------------------------------------------------------";

# For Query
my @acctsToRun = ();
my $MainAccount="";
my $MainWorkPathFolder="";
my $chgAccts = "false";
my $queryCount = 0;
my $warnSplsh = "WARNING! WARNING! WARNING! WARNING! WARNING! WARNING! WARNING!";
my ($PublishProductID, $AssociateProductID);


#=============================================	
# MAIN()
#=============================================
	if(($GenCodePath eq "") or ($WorkPath eq "") ) {
		print STDOUT "\n\n\n$dashes\n$dashes\n\tERROR!  Some Environment Variables are blank\n$dashes\n$dashes\n\n";
		print STDOUT "Env Values:\n--------------------\n";
		print STDOUT "\$AppsDrive = $AppsDrive\n";
		print STDOUT "\$MasterTemplatesFolder = $MasterTemplatesFolder\n";
		print STDOUT "\$GenCodePath = $GenCodePath\n";
		print STDOUT "\$WorkPath = $WorkPath\n";
		print STDOUT "\$XMLPrepConfigFile = $XMLPrepConfigFile\n";
		print STDOUT "--------------------\n\n";
		print STDOUT "(Press any key to exit)\n\n";
		$x = ReadKey();
		exit 0;
	} else {
		print STDOUT "\n--------------------\n";
		print STDOUT "Env Values:\n--------------------\n";
		print STDOUT "\$AppsDrive = $AppsDrive\n";
		print STDOUT "\$MasterTemplatesFolder = $MasterTemplatesFolder\n";
		print STDOUT "\$GenCodePath = $GenCodePath\n";
		print STDOUT "\$WorkPath = $WorkPath\n";
		print STDOUT "\$XMLPrepConfigFile = $XMLPrepConfigFile\n";
		print STDOUT "--------------------\n\n";	
	}
	
	if(!Housekeeping())		{ ErrorExit("Housekeeping\(\)", "FAILED!"); }
	if($opt_d) {print STDOUT "[debug] Finished Housekeeping...\n\n";	}
	
	if(!Set_Gaccts_CONFIG()) 	{ ErrorExit("Set_Gaccts_CONFIG\(\)", "FAILED!"); }
	if($opt_d) {print STDOUT "[debug] Finished Set_Gaccts_CONFIG...\n\n";}
	
	if(!InitWorkPathFolders())	{ ErrorExit("InitWorkPathFolder\(\)", "FAILED!"); }
	if($opt_d) {print STDOUT "[debug] Finished InitWorkPathFolders...\n\n";}

	if(-e "_XMLPrep_SPLITACCT.txt") {
		unlink "_XMLPrep_SPLITACCT.txt";	# DELETE previous
		if($opt_d) { print STDOUT "[debug] Finished DELETING _XMLPrep_SPLITACCT.txt...\n\n";	}
	}

	if($#acctsToRun > 0) {
		system `echo SPLITACCT > _XMLPrep_SPLITACCT.txt`;	# Split account, so create file
		foreach (@acctsToRun) {
			system `echo $_ >> _XMLPrep_SPLITACCT.txt`;
		}
	}
			
	foreach (@acctsToRun) {
		if($acctsToRun[0] eq $_) {
			printOut("\n\n$dashes\nPROCESSING (PRIMARY ACCOUNT):  $_\n$dashes\n");
			if(!ProcessAccount($_, 1)) { ErrorExit("ProcessAccount\(\)", "FAILED!"); }
			if($opt_d) { print STDOUT "[debug] ...Finished ProcessAccount($_)...\n"; }
		} else {
			printOut("\n\n$dashes\nPROCESSING (SECONDARY ACCOUNT):  $_\n$dashes\n");
			if(!ProcessAccount($_, 0)) { ErrorExit("ProcessAccount\(\)", "FAILED!"); }
			if($opt_d) { print STDOUT "[debug] ...Finished ProcessAccount($_)...\n"; }
		}
	}
	
	XMLPrepExit(1);	# success

#=============================================	
# MAIN END
#=============================================	


#=============================================================================	
# 					S U B R O U T I N E S	
#=============================================================================	
#-------------------------------------------------------------
sub BadConfigData {
#-------------------------------------------------------------
	my ($parm1, $parm2) = @_;
	my $me = whoami();
	
	$BadConfig=1;
	
	if($opt_d) { printOut("\[$me\]\n"); }
	
	printOut("ERROR:  \<$parm1\>=$parm2\n");
}


#----------------------------------------------------------
sub CreateLog() {
#----------------------------------------------------------
	my $myDT=$LogDateTime;
	my $fn="--empty--";
	
	$myDT =~ s|(\d\d\d\d)(\d\d)(\d\d)_(\d\d)(\d\d)(\d\d)|$1\/$2\/$3 $4:$5:$6|g;
	
	open(LOG, ">:utf8",  "$CWD\\XMLPrep_LOG.txt")
		or die "Can't CREATE LOG file $CWD\\XMLPrep_LOG.txt: $!\n"; 
		
	print LOG "($CWD\\XMLPrep_LOG.txt)\n\nApplication:\tXMLPrep.exe\t\t$myVersion\n";
	print LOG "Date_Time:\t\t$myDT\n";
	print LOG "====================================================================================\n";

	if($opt_d) {
		print LOG " DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG \n";
		print LOG "*------------------------------------------------------------------------------*\n";
		
		print LOG "\n\n================================\n" .
			"+++[debug] EXCLUDE:\n================================\n";
		foreach my $exc (@config_ExcludeFilter)	{
			print LOG "$exc\n";	
		}
		print LOG "================================\n\n";
	} else {
		print LOG "\t(Process with '-d' switch to turn on debugging.)\n";
	}
}


#----------------------------------------------------------
sub CreatePath {
#----------------------------------------------------------
	my ($Path) = @_;
	my $me = whoami();
	my $UserShare="";
	my $mappedDrive="";
	my $folderString="";
	my @folders=();
	my $currPath="";
	
	printOut("[$me]...\n");
	
	printOut("CREATING DIRECTORY(S):  $Path...  \n");
		
	if($Path =~ /^\\\\/)	{
		$UserShare = $Path;
		$UserShare =~ s|^\\\\(.+?)\\(.+?)\\(.+?)$|\\\\$1\\$2\\|g;		#"\\server\share\"
		printOut("\t\$UserShare=$UserShare\n");
		$currPath=$UserShare;
		
		#Load @folders...
		$folderString = $Path;
		$folderString =~ s|^\\\\(.+?)\\(.+?)\\(.+?)\\(.+?)$|$3\\$4|g;	# ~\folders
		printOut("\t\$folderString=$folderString\n");
	} else {
		$mappedDrive = $Path;			
		$mappedDrive =~ s|^(.+?)(\:\\)(.+?)$|$1$2|g;
		printOut("\t\$mappedDrive=$mappedDrive\n");
		$currPath=$mappedDrive;
		
		#Load @folders...
		$folderString = $Path;
		$folderString =~ s|^(.+?)(\:\\)(.+?)$|$3|g;
		printOut("\t\$folderString=$folderString\n");
	}	
		
	@folders = split(/\\/, $folderString);
	foreach my $mysplit (@folders)	{
		printOut("\t\t$mysplit\n");
	}
	
	printOut("\n ok...Actually creating the missing directories...\n");

	if(-e $currPath)	{		# either "\\server\share\"  or  mapped drive ex. "C:\"
		local $CWD=$currPath;
		
		#create folders if they don't exist...
		foreach my $myFolder (@folders)	{
			
			printOut("\t===\> \$myFolder=$myFolder\n");
			
			if( -e $myFolder)	{
				$CWD = $CWD . "\\" . $myFolder;
				printOut("\t\tUpdated \$CWD to:  $CWD\n");
			} else {
				mkdir("$myFolder", 0777) or die "+++ Can't mkdir $myFolder\n$!\n";
				$CWD = $CWD . "\\" . $myFolder;
				printOut("\t\tCreated folder $myFolder and ...\n" . 
							"\t\tUpdated \$CWD to:  $CWD\n");
			}
		}	
		
		printOut("CREATED DIRECTORY(S)!\n\n");
		
	} else {
		ErrorExit(whoami(), "Dang!  Something is stinky in Denmark!");
	}
}


#-------------------------------------------------------------
sub dateTime {
#-------------------------------------------------------------
	my ($sec,$min,$hour,$mday,$monNr,$year);
	my $now = {};
	#get date and time
	($sec,$min,$hour,$mday,$monNr,$year) = localtime(time());
    	my ($date, $date_nr, $clock);
	my $month=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")[$monNr];
    	$monNr++;
	$monNr = sprintf("%02d",$monNr);
	my $mdayNr = sprintf("%02d",$mday);
	$sec = sprintf("%02d",$sec);
	$min = sprintf("%02d",$min);
	$hour = sprintf("%02d",$hour);
	$year= 1900 + $year;
	$now->{'date'} = "$year-$month-$mday";
	$now->{'dateNr'} = "$year$monNr$mdayNr";
	$now->{'time'} = "$hour:$min:$sec";
	$now->{'HHMMSS'} = "$hour$min$sec";
	return($now);		
}


#-------------------------------------------------------------
sub Dump_Important_Globals	{
#-------------------------------------------------------------
	my $me = whoami();
	
	printOut("\n--------------------- Dump Important Globals: ----------------------\n");
	printOut("\$AppsDrive = $AppsDrive\n");
	printOut("\$GenCodePath = 	$GenCodePath\n");
	printOut("\$WorkPath = $WorkPath\n");
	printOut("\$XMLPrepConfigFile = $XMLPrepConfigFile\n");
	printOut("\$oldprocessfolder = $oldprocessfolder\n");
	printOut("\$newprocessfolder = $newprocessfolder\n");
	printOut("\$config_CKListPath=$config_CKListPath\n");
	
	printOut("\n-------------------END $me --------------------\n\n");
}


#-------------------------------------------------------------
sub Dump_Jobs	{
#-------------------------------------------------------------
	my $me = whoami();
	my $i;
		
	printOut("\n--------------------- $me: \$XMLJobs ----------------------\n");
	my $myDump = Dumper($XMLJobs);
	printOut("$myDump");
	printOut("\n-------------------END $me: \$XMLJobs --------------------\n\n");
}


#-------------------------------------------------------------
sub ErrorExit {
#-------------------------------------------------------------
	my ($mySub, $myMessage) = @_;
	
	printOut("ERROR:  [$mySub]\n\n\t$myMessage\n");
	printOut("\n(Press any key to continue...)\n\n");
	my $x = ReadKey();
	die "Exiting application.";
}


#-------------------------------------------------------------
sub FilterFile {
#-------------------------------------------------------------
	my $parm = shift;
	my $me = whoami();
	my $FilteredParm=0;
	
#	#SELECT ONLY *. FILES (due to legacy processing on XPP)...
	if($parm !~ /\./)	{$FilteredParm = 1;}
	#$FilteredParm = grep { !/\./} $parm;
	if($parm =~ /(.+?)\.mcc$/i)	{$FilteredParm = 1;}
		
	if($opt_d) {
		printOut("\t\t$parm");
		if((length $parm) > 7)	{
			printOut("\t[\$FilteredParm=$FilteredParm]\t"); 
		} else {
			printOut("\t\t[\$FilteredParm=$FilteredParm]\t"); 
		}
	}
			
	if( $FilteredParm < 1)	{		
		if($opt_d) { printOut("--- EXCLUDED (via \$FilteredParm) ---\n"); }
		return "FILTERED";
	}
	
	#EXCLUDE FILTER ENTRIES TO TEST?...
	if($#config_ExcludeFilter > -1)	{	
		foreach my $ff (@config_ExcludeFilter)	{
			if($parm =~ /^$ff$/i)	{	#case insensitive
				if($opt_d) { printOut("--- EXCLUDED (via \@config_ExcludeFilter)---\n"); }
				return "FILTERED";
			}
		}
	}
	
	if($opt_d) { printOut("\(APPROVED\)\n"); }
	
	return "Approved";
}


#----------------------------------------------------------
sub XMLPrep_excludes()	{
#----------------------------------------------------------
	my $me = whoami();
	my @config = ();
	
	if($opt_d) { printOut("+++[debug] ($me)\n\n"); }
		
	if(!-e ($XMLPrepConfigFile))	{
		ErrorExit($me, "The $XMLPrepConfigFile file does not exist!");
	}

	if($opt_d) {printOut("\$XMLPrepConfigFile=$XMLPrepConfigFile\n"); }
			
	my $xs = XML::Simple->new(
		forcearray => 1,
		KeepRoot => 0,
		KeyAttr => [ ]
	);
	
	$XMLconfig = $xs->XMLin($XMLPrepConfigFile);	
	my $REF_config_ExcludeFilter = $XMLconfig->{exclude};
	@config_ExcludeFilter = @$REF_config_ExcludeFilter;
	
	if($opt_d) { 
		printOut("+++[debug] (\@config_ExcludeFilter)\n\n");
		
		for(my $i=0; $i<$#config_ExcludeFilter; $i++) {
			print STDOUT "\t[$i] = $config_ExcludeFilter[$i]\n";	
		}
		
		print STDOUT "(Press any key to continue...)\n\n";
		$x = ReadKey();
	}
				
	return 1;
}


#----------------------------------------------------------
sub Housekeeping	{
#----------------------------------------------------------
	my $me = whoami();

	&Clear;
	
	# HELP?
	if( ($opt_h) or ($opt_help) )	{
		system `XMLPrep.html`;
		return 1;
	}
	
	printOut("*------------------------------------------------------------------------------*\n");
	printOut("RUNNING:  XMLPrep.exe\t\t$myVersion\n");
	printOut("*------------------------------------------------------------------------------*\n\n");
	printOut("\t(For Help run:  XMLPrep --help)\n");
	
	if($opt_d) {
		printOut("\tDEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG \n");
		printOut("*------------------------------------------------------------------------------*\n");
	} else {
		printOut("\t(Process with '-d' switch to turn on debugging.)\n\n");
	}
	
	if(!print_opts() ) { ErrorExit("print_opts", "Problem printing out 'input' options."); }
	
	printOut("*------------------------------------------------------------------------------*\n\n");
	
	if($opt_job ne "-none-") {
	 	if($opt_new or $opt_old)	{
	 		printOut("Ready to process the following Account(s):\n");
	 	} else {
			printOut("\tCOMMAND LINE: ERROR! ERROR! ERROR! ERROR! ERROR!\n");
			printOut("\tJOB = $opt_job\n\n");	
			printOut("ONE OF THE FOLLOWING REQUIRED:\n");
			printOut("\t'--new'\t(new WorkPath process) = $opt_new\n");	
			printOut("\t'--old'\t(old WorkPath process) = $opt_old\n");	
			printOut("\n*------------------------------------------------------------------------------*\n");
			ErrorExit($me, "Command line error.  Please check your parameters.");
		}
	} else {
		printOut("\tCOMMAND LINE: ERROR! ERROR! ERROR! ERROR! ERROR!\n");
		printOut("\t'--job=$opt_job is not valid.\n");
		printOut("\n*------------------------------------------------------------------------------*\n");
		return 0;
	}
		
	# QUERY THE DATABASE FOR NUMBER OF ASSOCATED JOBS
	$queryCount = QueryDb($opt_job);
	
	if($chgAccts eq "true")	{	
		printOut("\n$dashes\n$warnSplsh\n$dashes\n\n");
		printOut("\tThe PUBLISH PRODUCT ID you should be using is: $PublishProductID\n\n");
		printOut("\t(Account No. $AssociateProductID is an ASSOCIATE product ID)\n");
		printOut("\n$dashes\n$dashes\n\n");
		return 0;
	}
	
	if($#acctsToRun lt 0)	{
		push @acctsToRun, $opt_job;	
	}
	
	foreach (@acctsToRun) {
		printOut("\t$_\n");
	}
	
	
	# Process the files or quit?
	printOut("\n----------------------------------------------------------------\n" . 
		" Y = YES\t\t--OR--\t\t(blank\) or N = CANCEL\n" . 
		"\n----------------------------------------------------------------\n\n");
	
	if($opt_topause eq "pause") {	
		$x = &ReadKey;
	} else {
		$x = "Y";
	}
	
	if($x=~ /[Yy]/) {
		# do nothing...
		printOut("\n");	#add a little space before processing the files
	} else {
		ErrorExit(whoami(), "\...Processing cancelled.\n");
		exit 0 ;
	}

	if($opt_d and $opt_job)	{
		printOut("\nDumping GetOptions Data:\n");
		printOut("\$opt_d (debugging) = $opt_d\n");
		printOut("\$opt_job (job number) = $opt_job\n");	
		printOut("\$opt_new (new WorkPath process) = $opt_new\n");	
		printOut("\$opt_old (old WorkPath process) = $opt_old\n");	
	}
		
	if(!XMLPrep_excludes()) { exit 0; }
	
	$MainAccount=$acctsToRun[0];
	$MainWorkPathFolder = $WorkPath . "\\" . $MainAccount;	
	
	if($opt_d) { printOut("\n+++[debug] \$MainWorkPathFolder=$MainWorkPathFolder\n"); }
		
	#====================
	# Setup XMLPrepTemp folder...
	#====================
	# Set $CWD to $XMLPrepFolder location...
	$Path = "Z:\\XMLPrep";
	
	if( -e "$Path")	{
		if($opt_d) { printOut("[$me]\n\tPath Exists!\n\t$Path\n\n"); }
	} else {
		printOut("[$me]\n\tNOT FOUND!\n\t$Path\n\n");
		&CreatePath("$Path");
	}
			
	$CWD=$Path;
	$UserPath=$Path;

	#====================
	# Setup LOGS folder...
	#====================
	$Path = $Path . "\\LOGS";
	
	if(!-e "$Path")	{
		mkdir("LOGS", 0777) or die "Could NOT Make Directory:  ~\\LOGS\n$!\n";
	}
	
	$CWD=$Path;
	if($opt_d) {printOut("[$me]\n\t\$CWD=$CWD\n\n");}

	&CreateLog();
	if($opt_d) {printOut("\$CWD=$CWD\n");	}

	#====================
	# Read XMLPrepfile ...
	#====================	
	$Path = substr($Path, 0, -5);
	$CWD=$Path;
	
	if($opt_d) {printOut("[$me]\n\t\$CWD=$CWD\n\n");}

	return 1;
}


#-------------------------------------------------------------
sub InitWorkPathFolders {
#-------------------------------------------------------------
# Initializes these folders:  DocxTemp, gc, HTML, MunicodeDesktop, RTF, & XML
# AND deletes all *.zip files in the root g2h account folder.
	my $me = whoami();

	if($opt_d)	{	
		printOut("\n\n$dashes\n\t$acct\n");
		printOut("\$CWDSAVED=$CWDSAVED\n");	
		printOut("\$CWD=$CWD\n");
		printOut("\$MainAccount=$MainAccount\n");
		printOut("\$MainWorkPathFolder=$MainWorkPathFolder\n");
		printOut("=====\n");
	}
										
	#CREATE ACCOUNT PATH IF NECESSARY...
	if(!-e $MainWorkPathFolder)	{
		if($opt_d) {printOut("\n===>\tCREATING WorkPath ACCOUNT PATH $MainWorkPathFolder\n\n");}
		chdir $WorkPath;
		mkdir($MainAccount, 0777)	or die "+++ Can't mkdir $MainAccount\n$!";
	}   	

	$CWDSAVED = $CWD;
	$CWD = $MainWorkPathFolder;		# WorkPath + ACCOUNT#
	printOut("\n\$CWDSAVED=$CWDSAVED\n");	
	printOut("\$CWD=$CWD\n\n");	
	
	printOut("DELETING \*.zip files...\n");

	foreach my $filezip (glob "*.zip") {
		printOut("  $filezip...");
		
		if(unlink $filezip) {
			printOut("DONE!\n");
		} else {
		 	warn "\t+++ Unable to delete $filezip $!\n";
		}
	}
	
	printOut("\n");

	if(!InitWorkPathFolder($MainWorkPathFolder, "DocxTemp") )		{ 
		ErrorExit("InitWorkPathFolder", "Problem with:\n\tPATH=$CWD\n\tFOLDER=DocxTemp"); 
	}	
	
	if(!InitWorkPathFolder($MainWorkPathFolder, "gc") )		{ 
		ErrorExit("InitWorkPathFolder", "Problem with:\n\tPATH=$CWD\n\tFOLDER=gc"); 
	}	
	
	if(!InitWorkPathFolder($MainWorkPathFolder, "HTML") )	{ 
		ErrorExit("InitWorkPathFolder", "Problem with:\n\tPATH=$CWD\n\tFOLDER=HTML"); 
	}
	
#	if(!InitWorkPathFolder($MainWorkPathFolder, "mag") )	{ 
#		ErrorExit("InitWorkPathFolder", "Problem with:\n\tPATH=$CWD\n\tFOLDER=mag"); 
#	}	

	if(!InitWorkPathFolder($MainWorkPathFolder, "MunicodeDesktop") )	{ 
		ErrorExit("InitWorkPathFolder", "Problem with:\n\tPATH=$CWD\n\tFOLDER=MunicodeDesktop"); 
	}
	
	if(!InitWorkPathFolder($MainWorkPathFolder, "RTF") )	{ 
		ErrorExit("InitWorkPathFolder", "Problem with:\n\tPATH=$CWD\n\tFOLDER=RTF"); 
	}
	
	if(!InitWorkPathFolder($MainWorkPathFolder, "XML") )	{ 
		ErrorExit("InitWorkPathFolder", "Problem with:\n\tPATH=$CWD\n\tFOLDER=XML"); 
	}

	return 1;	
}


#-------------------------------------------------------------
sub InitWorkPathFolder {
#-------------------------------------------------------------
	my $me = whoami();
	my $WAFolder = shift;
	my $folder = shift;
	
	my $WACWDSAVED=$CWD;
	$CWD=$WAFolder;
	
	if($opt_d){printOut("[$me] \$CWD=$CWD\n");}
	
	if(-e "$WAFolder/$folder")	{
		if($opt_d){printOut("\n[debug] ($me) \tWorkPath ACCOUNT FOLDER EXISTS:  $folder\n");}
		printOut("Removing contents of $WAFolder\\$folder...");
		
		# Added logic here to clear out only appropriate folders based on --new/--old
		if ($opt_new gt 0 && $folder ne "mag"){
			rmtree("$WAFolder/$folder", {keep_root => 1} );	# keeps folder name...
			printOut("DONE!\n");
		}
		elsif ($opt_old gt 0 && $folder eq "mag"){
			rmtree("$WAFolder/$folder", {keep_root => 1} );	# keeps folder name...
			printOut("DONE!\n");
		}	
		else {
			printOut("SKIPPED!\n");
		}
	} else {
		# Create Folder...	
		if($opt_d){printOut("\n[debug] ($me) \tCREATING WorkPath ACCOUNT FOLDER:  $folder\n");}
		printOut("Creating WorkPath folder $folder...");
		mkdir($folder, 0777)	or die "+++ Can't mkdir $MainAccount\\$folder\n$!";
		printOut("DONE!\n");
	}
	
	$CWD=$WACWDSAVED;
	
	return 1;
}


#-------------------------------------------------------------
sub listDir {
#-------------------------------------------------------------
#list the contents of a folder
	my ($folder) = @_;
	my $file;
	
	printOut("listing folder and content: $folder\n");
	
	opendir DIR, $folder or die "*** ERROR ***\nCouldn't open folder $folder\n$!";

	printOut("============================================================================\n");
	printOut("     FOLDER LISTING FOR:  $folder\n");
	printOut("============================================================================\n");

	while ($file = readdir DIR) {
		next if ($file =~m#^\.$#);
		next if ($file =~ m#^\.\.$#);
		printOut("$file\n");
	}
}


#-------------------------------------------------------------
sub LoadContentsVAR {
#-------------------------------------------------------------
	my ($infile) = @_;
	my $me = whoami();
	
	open INPUT, "<$infile" or die "[$me] Can't open $infile \n$!";
	undef $/;
	$contents = <INPUT>;
	close INPUT;	
	$/ = "\n";     #Restore for normal behaviour later in script
}


#-------------------------------------------------------------
sub print_opts ()	{
#-------------------------------------------------------------
	my $me = whoami();
	
	if($opt_job)		{ printOut(" --job=$opt_job"); }
	if($opt_new)		{ printOut(" --new"); }
	if($opt_old)		{ printOut(" --old"); }
	if($opt_topause)	{ printOut(" --topause=$opt_topause"); }
	
	printOut("\n\n");
	
	return 1;
}


#-------------------------------------------------------------
sub printOut {
#-------------------------------------------------------------
	my $parm = shift;
	my @FILEHANDLE = (*STDOUT, *LOG);
	
	foreach my $outfile (@FILEHANDLE) {
		if(fileno($outfile)) {
			print $outfile ("$parm");		
		}
	}
}


#-------------------------------------------------------------
sub ProcessAccount {
#-------------------------------------------------------------
	my $me = whoami();
	my $acct = shift;
	my $primary = shift;
	my $newFile="";
	my $splitAccounts=0;
	my($num_of_files_and_dirs, $num_of_dirs, $depth_traversed);
				
	if($#acctsToRun > 0) {
		$splitAccounts=1;
	} else {
		$splitAccounts=0;
	}		

	if($primary > 0) {		
		if($opt_new) {
			$str1=$WorkPath .  "\\" . $acct . "\\" . $newprocessfolder;	
		} else {
			$str1=$WorkPath .  "\\" . $acct . "\\" . $oldprocessfolder;	
		}
	}
	
	$AccountPath_acctno=$GenCodePath . "\\" . $acct;
	
	
	$WorkPath_acctno=$str1;	
	
	if($opt_new) {	
		printOut("\n\n***\t\$WorkPath_acctno (NEW):\t$WorkPath_acctno\n\n");
	} else {
		printOut("\n\n***\t\$WorkPath_acctno (OLD):\t$WorkPath_acctno\n\n");
	}

	#COPY files to $str1  (WorkPath ACCOUNT NEWPROCESS PATH...
	printOut("\tCOPY FROM:	$AccountPath_acctno\n");
	printOut("\tCOPY TO:	$str1\n");
	
	opendir(TEMPH, "$AccountPath_acctno")        or die "Couldn't open $AccountPath_acctno for reading: $!";

	while (defined($filename = readdir TEMPH)) {
		next if($filename eq "." || $filename eq ".."); 

		# Copy 'config folder'...(ONLY FROM PRIMARY ACCOUNT)
		if( (-d "$AccountPath_acctno/$filename") and (uc($filename) eq "CONFIG") )	{	

			if($acct eq $opt_job) {	# PRIMARY acct's CONFIG?...
				printOut("\tCopying PRIMARY ACCOUNT FOLDER:  $filename... ");

# my($num_of_files_and_dirs,$num_of_dirs,$depth_traversed) = dircopy($orig,$new);						
				($num_of_files_and_dirs,$num_of_dirs,$depth_traversed) = dircopy("$AccountPath_acctno\\$filename",  "$str1\\$filename")
					or die "Can not 'dircopy' folder:  $filename\n" . "\t$AccountPath_acctno\\$filename" . 
						"\tto $str1\$filename" .
						"\t=====================\n" .
						"FilesAndDIRs=$num_of_files_and_dirs\nDIRs=$num_of_dirs\nDepth_Traversed=$depth_traversed\n" .
						"\t=====================\n" . "\n $!\n"; 
				printOut("DONE.\n");
				
				# IF SPLIT-ACCOUNT then update the _PubMap.xml filenames with "a_#####"...
				if($splitAccounts)	{
					printOut("\t...SPLIT ACCOUNT UPDATE FILE:  \n\t\t$str1\\CONFIG\\ _PubMap.xml...\n");
				 	if(!Update_PubMap("$str1\\CONFIG\\_PubMap.xml", $acct)) { 
				 		ErrorExit("Update_PubMap\($str1\\CONFIG\\_PubMap.xml, $acct\)", "FAILED!"); 
				 	}
				 	printOut("\t\t...SPLIT ACCOUNT UPDATE FINISHED.\n\n");
				 }				
				
			} else {
				#do nothing since it's not a CONFIG from a PRIMARY account.	
			}
		}

		next if( -d ("$AccountPath_acctno\\$filename"));
		
		if( ! -d "$AccountPath_acctno\\$filename" )	{	# $filename is NOT a DIRECTORY...

			$FileFilterFlag = FilterFile($filename);
			if( $FileFilterFlag ne "Approved")	{
				next;
			}

			if($splitAccounts) {
				$newFile = $str1 . "\\" . ($WFAlpha{$WFprefix}) . "_" . $acct . "_" . $filename;
			} else {
				$newFile = $str1 . "\\" . $filename;
			}
			
			copy("$AccountPath_acctno\\$filename", $newFile)
				or die "\n\[WorkPath NEW\] +++\n\tCOPY FAILED\n\t$AccountPath_acctno\\$filename  --\>  $newFile\n$!";
		}
	}
	
	closedir TEMPH	or die "Couldn't close TEMPH ($AccountPath_acctno)"; 	

	$WFprefix++;

	return 1;
}


#----------------------------------------------------------
sub QueryDb {
#----------------------------------------------------------
	my $me = whoami();
	my $acctNum = shift;
	my $ct=0;
	
	# SQL INFO
	my $dsn=q/dbi:ODBC:BIS/;	#system DSN
	my $user=q/BIS_dbreader/;
	my $pwd=q/ReadOnly1/;
	my $dbh;	#database handle
	my $sth;	#statement handle
	my $query="";	
	
	# QUERY CONFIG VARIABLES
	$dbh->{ReadOnly} = 1;
	
	# Connect to the data source and get a handle for that connection.
	$dbh = DBI->connect($dsn, $user, $pwd)  
		or die "[$me] Can't connect to $dsn: $DBI::errstr\n$!";		

	# QUERY...
	$query = "SELECT (SELECT SplitWebPublishProductID FROM BIS_Products WHERE ProductID = $acctNum) AS PublishProductID, 
					[AssociateProductID]
			FROM [BIS].[dbo].[SplitWebPubAssociations]
			WHERE PrimaryProductID = $acctNum";

	$sth = $dbh->prepare($query) or die "[$me] Can't prepare query: $query\n$!";
	$sth->execute;
	
	# BIND TABLE COLUMNS TO VARIABLES
	$sth->bind_columns(undef, \$PublishProductID, \$AssociateProductID) or die "[$me] Could not bind_columns for SplitWebPublish stuff\n$!\n";

	# LOOP THROUGH RESULTS	
	while ($sth->fetch())	{
		#printOut("...PublishProductID:  $PublishProductID - AssociateProductID: $AssociateProductID\n");
		
		# Check to see if ID is Primary
		if ($PublishProductID eq $AssociateProductID) {
			# Index 0 should be primary
			unshift @acctsToRun, $AssociateProductID;
			}	
		else {
			push @acctsToRun, $AssociateProductID;
		}
	}	
		
	$ct++;
	$chgAccts = "true" if($PublishProductID ne $AssociateProductID && $acctNum eq $AssociateProductID);
	
	# Close the connection...
	$sth->finish();
	
	return $ct;
}


#----------------------------------------------------------
sub Set_Gaccts_CONFIG {
#----------------------------------------------------------
	# FIX LEGACY USE OF R:\ACCTS, AS CONFIG DATA LOCATION, TO G:\ACCTS....
	my $me = whoami();
	my $pathstr="";
	my $file="";
	my $ToPath="";
	my @copyArgs=();
	my $SaveCWD=$CWD;
	
	if(!-e $GenCodePath)	{ ErrorExit("$me", "Path Not FOUND:  $GenCodePath"); }

	$CWD = ($GenCodePath . "\\" . $opt_job);
	printOut("\$CWD=$CWD\n");
	
	# Make sure $GenCodePath has a CONFIG folder...
	if(!-e "$CWD\\CONFIG")	{
		if($opt_d) { printOut("\nCONFIG NOT FOUND \(creating it\) in $CWD\n"); }
		mkdir "CONFIG";
	} else {
		if($opt_d) { printOut("\nCONFIG FOUND in $CWD\n"); }
	}
	
	# CD to CONFIG folder...
	$CWD = ($CWD . "\\CONFIG");
	
	if($opt_d) { printOut("\$CWD=$CWD\n"); }
	
	$ToPath=$CWD;

	# Set $pathstr if $WorkPath\(gc/mag)\CONFIG EXISTS, else (blank)...
	if($opt_new)	{ $pathstr = $MainWorkPathFolder . "\\gc\\CONFIG"; }
	if($opt_old)	{ $pathstr = $MainWorkPathFolder . "\\mag\\CONFIG"; }

#	if($opt_d) { printOut("+++[debug] A \$pathstr=$pathstr\n"); }
	
	if(-e $pathstr) {
		$CWD=$pathstr;
	} else {
		$pathstr="";
	}
	
	if($opt_d) { printOut("+++[debug] B \$pathstr=$pathstr\n"); }
print STDOUT "\n(Press any key to exit.)\n\n";
	
	if($pathstr ne "") {
		# R:\accts\#####\(gc/mag)\CONFIG folder EXISTS...
		$CWD=$pathstr;	# CD to R:\accts\#####\(gc/mag)\CONFIG folder...
				
		printOut("Updating Job CONFIG folder:  $opt_job\\CONFIG From $pathstr...\n");
		opendir(MYDIR, $pathstr) or die "Can't opendir $pathstr: $!";
		
		while(defined($file = readdir(MYDIR)))	{
			next if($file eq ".");
			next if($file eq "..");
			
			if(!-e ("$ToPath\\$file") )	{
				copy($file, $ToPath) or die "Can't COPY $file to $ToPath\n$!";		
				printOut("\t...$file\n");

# THIS SHOULD HAPPEN TO FILE AFTER IT HAS BEEN COPIED TO R:\ACCTS\#####\CONFIG:								
#				if( (uc("$file") eq "_PUBMAP.XML") and ($#acctsToRun gt 0) )	{	# SPLIT ACCOUNT...
#					printOut("\t\t...SPLIT ACCOUNT UPDATE FILE:   $file...\n");
#				 	if(!Update_PubMap("$ToPath\\$file")) { ErrorExit("Update_PubMap\($ToPath\\$file\)", "FAILED!"); }
#				 	printOut("\t\t...SPLIT ACCOUNT UPDATE FINISHED.\n\n");
#				 }
			} else {
				printOut("\t...$file \(EXISTS - not copied\)\n");
			}
		}
		close(MYDIR);
	}
	
	$CWD = $MasterTemplatesFolder;
	printOut("\n\$CWD=$CWD\n");
	printOut("Updating Job: " . $opt_job . "'s CONFIG folder From $MasterTemplatesFolder...\n");
	opendir(MYDIR, $MasterTemplatesFolder) or die "Can't opendir $MasterTemplatesFolder: $!";
	
	while(defined($file = readdir(MYDIR)))	{
		next if($file eq ".");
		next if($file eq "..");
		
		if(!-e "$ToPath\\$file" )	{
			copy($file, $ToPath) or die "Can't COPY $file to $ToPath\n$!";		
			printOut("\t...$file\n");
		} else {
			printOut("\t...$file \(EXISTS - not copied\)\n");
		}
	}
	
	close(MYDIR);
		
	printOut("\n");
	
	$CWD=$SaveCWD;
	
	if($opt_d) { printOut("+++[debug] CWD=$CWD\n"); }	
	
	return 1;
} 


#-------------------------------------------------------------
sub Update_LogDateTime{
#-------------------------------------------------------------
	$nowYYYYMMDD_HHMMSS= dateTime();
	$LogDateTime =	$nowYYYYMMDD_HHMMSS->{'dateNr'} .  "_" . $nowYYYYMMDD_HHMMSS->{'HHMMSS'};
}


#-------------------------------------------------------------
sub Update_PubMap {
#-------------------------------------------------------------
	my $me = whoami();
	my $PMfile = shift;
	my $PMacct = shift;
	my $line = "";
	my ($name,$path,$suffix) = fileparse($PMfile);
	my $PMnew = $path . "_PubMap_NEW.xml";
	my $prefix = "a_" . $PMacct . "_";
	
	open(FILE, "<:utf8",  "$PMfile") or die "Can't read file '$PMfile' [$!]\n";  
	open(OUT, ">:utf8",  "$PMnew") or die "Can't open file for output:  '$PMnew'\n[$!]\n";

	printOut("$dashes\n");
	printOut("FILE:  _PubMap - correcting for split-account processing...\n");
	
	while(<FILE>) {
		 my($line) = $_;
		chomp($line);			# Strip the trailing newline from the line.
		
		# Get header stuff...
		if(	($line =~ /^<\?xml/ )		|
			($line =~ /^<\!--/)
			) { 
			print(OUT "$line\n");
			printOut("$line\n");
			next;
		}
		
		if( 	( $line =~ /<PubMap>/ )	|
			( $line =~ /<\/PubMap>/ )	|
			($line =~ /<default>/ )		|
			( $line =~ /<\/default>/ )	|
			( $line =~ /<oh\d>/ )		|
			( $line =~ /<\/oh\d>/ )		|
			( $line =~ /<a_(.+?)>/ )			# already converted for split-account
			)	{
				
			print(OUT "$line\n");
			printOut("$line\n");	
			next;		
		}	
		
		# convert for split-account...
		if($line !~ /<\//)	{
			$line =~ s|<(.+?)>|<$prefix$1>|g;
		} else {
			$line =~ s|<(\/)(.+?)>|<$1$prefix$2>|g;
		}
		
		print(OUT "$line\n");
		printOut("$line\n");			
	}

	printOut("$dashes\n");
	
	close(OUT);
	close (FILE);  	
	
	if( (-e $PMfile) and (-e $PMnew) ) {	
		if(rename($PMfile, "$path\\_PubMap_ORIG.xml") ) {
			printOut("Renamed $PMfile to " . $path . "_PubMap_ORIG.xml\n");
			
			if(rename($PMnew, $path . "_PubMap.xml") ) {
				printOut("Renamed $PMnew to " . $path . "_PubMap.xml\n");
			} else {
				printOut("!!! Rename FAILED:  $PMnew to " . $path . "_PubMap.xml\n");
			}
		} else {
			printOut("!!! Rename FAILED:  $PMfile to " . $path . "_PubMap_ORIG.xml\n");
		}
	}
	
	printOut("FINISHED correcting for split-account processing.\n");
	printOut("$dashes\n");
	
	return 1;
}


#-------------------------------------------------------------
sub whoami {
#-------------------------------------------------------------
	(caller(1))[3];
}


#----------------------------------------------------------
sub XMLPrepExit  {
#----------------------------------------------------------
	 my $me = whoami();
	 my $status = shift;
	 
	 
	 if($status == 1) {
	 	printOut("\n\n============================================================\n");
	 	if($opt_d) {
			printOut("[$me]\n\t----- FINISHED!  [\$status=$status] -----\n");
		} else {
			printOut("\t----- FINISHED! -----\n");
		}
	 } else {
		do_Error(whoami(), "----- ERROR!  [\$status=$status]-----\n");
	}
	 
 	printOut("============================================================\n");
	close(LOG);

	if($opt_topause ne "nopause") {
		my $NotePadRTN = system("NotePad $Path\\LOGS\\XMLPrep_LOG.txt");
	}

	exit($status);
}


#====================================================================================
#====================================================================================
#POD Documentation
#
# To convert this documentation to an html file, type "C:\>pod2html XMLPrep.pl > XMLPrep.html" <enter>
# You will probably want to tweak the output a little.
#====================================================================================
#====================================================================================

=head1 XMLPrep.exe Documentation

=head2 The XMLPrep.exe is used to do the following:

=over 4

=item 1.
Read the XMLPrep_config.xml file for UNC paths and settings, \&c.

=item 2.
Zip all of the files for each Job specified, named as "JobNumber".zip and place it in the config files Backup Path/JobNumber/Date_Time/ folder.

=back

=head2 Command-line:

At your DOS prompt, enter one of the following examples, changing the Job number, of course:

C:\>XMLPrep --job=12345 --new

C:\>XMLPrep --job=12345 --old

(NOTE:  Remember that you can also add the -d switch to aid debugging.)

=back

=cut
