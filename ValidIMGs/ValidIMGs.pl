#!/usr/bin/perl
# ValidIMGs.pl

my $myVersion = "Version:  2019-07-30.1 (WIP)";

# WIP:
# 2019-07-30.1 - Brett Bonnet -  Changed so that it will check if filename case differs from src tag. Created IdentifyIncorrectCaseIMGs subroutine.
# 2019-07-24.1 - Andrea Gill -  Changed so that it validates filenames based on name, not based on alt tag.
# 2013-09-19.1 - Raymond Lillibridge - Just recompiled
# 2013-05-15.1 - Raymond Lillibridge - 	
	# using:  if( -e ..) IS CASE SENSITIVE, AS IT SHOULD BE!
	# Editors are naming files all lower case yet using CamelCase in the GenCode image tags.  Wheee!
	
#=============================================	
# CHANGE HISTORY:
# 2010-12-02.1 - Raymond Lillibridge - Allow -nopause processing

umask 0;
#=============================================	
# PROCESSING OUTLINE
#=============================================	
#	Reads "*.xml" files, collecting filenames and all related image file references.
#	Then reports any image references to files that do not exist.

#=============================================	
# PRAGMA
#=============================================	
use warnings;
# no warnings 'uninitialized';
use Data::Dumper;
use Encode;
use File::Basename;
use File::Listing qw(parse_dir);

#=== Win32 ===
use Cwd;
my $WinLoginName = getlogin || getpwuid($<) || "Win Login-UNKNOWN-";

# Date and Time vars
my($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime;
$year += 1900;
my $now_string = localtime;
my $nowYYYYMMDD_HHMMSS= dateTime();
my $LogDateTime =	$nowYYYYMMDD_HHMMSS->{'dateNr'} .  "_" . 

					$nowYYYYMMDD_HHMMSS->{'HHMMSS'};

#for development purposes...
use Term::InKey;


#=============================================	
# GLOBALS
#=============================================
my $NoPause=shift;
if(!defined $NoPause)	{
	$NoPause="";	
}

my $username=$WinLoginName;
my $Path=cwd();
my $str1;
my $str2;
my $ctr;
my @FILEHANDLE = (*STDOUT, *LOG);
my $logfile="_ValidIMGs_LOG.txt";
my @files=();
my $G_infilename = "";
my @G_images = ();
my @G_images_not_found = ();
my @G_ogcase_images = ();
my @G_ogcase_dont_match = ();
my $G_msg = "";
my $rtn;
my $dashes = "----------------------------------------------------------------------";

#=============================================	
# MAIN
#=============================================	
Housekeeping();
ProcessFiles();

if($#G_images_not_found > -1 or $#G_ogcase_dont_match > -1) {
	$G_msg = "The images (below) are referenced in the XML file and are either NOT FOUND in the ~HTML/images folder or the filename case doesn't match";
}

if($G_msg eq "")	{	
	ValidIMGsExit(0);	#no errors
} else {
	ValidIMGsExit(1);	#errors
	

}

#=============================================	
# MAIN END
#=============================================	


#=============================================================================	
# 					S U B R O U T I N E S	
#=============================================================================	
#----------------------------------------------------------
sub ProcessFiles {
#----------------------------------------------------------
	my $me = whoami();
	my $linetemp="";
	
	foreach my $fn (@files)	{	
		next if(uc(substr($fn , 0, 4)) eq "BOOK");		# skip files that begin with "BOOK"
		next if(uc(substr($fn , 0, 1)) eq "_");			# skip files that begin with underscore
		$linetemp="";

		printOut("\n$fn\n");			
		$G_infilename = $fn;
		
		open INFIL, "<:utf8", "$fn"	or die "Can't OPEN input file:  $fn  $!\n";

		while(<INFIL>)	{
			my($line) = $_;
			chomp($line);			# Strip the trailing newline from the line.
			
			if($line =~ /<img id=/)	{				
				IdentifyMissingIMGs($line);
				IdentifyIncorrectCaseIMGs($line);
			}
		} # END... while(<INFIL>)
			
		close(INFIL);
	}
}


#-------------------------------------------------------------
sub dateTime {
#-------------------------------------------------------------
	my ($sec,$min,$hour,$mday,$monNr,$year);
	my $now = {};
	#get date and time
	($sec,$min,$hour,$mday,$monNr,$year) = localtime(time());
    	my ($date, $date_nr, $clock);
	my $month=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")[$monNr];
    	$monNr++;
	$monNr = sprintf("%02d",$monNr);
	$mdayNr = sprintf("%02d",$mday);
	$sec = sprintf("%02d",$sec);
	$min = sprintf("%02d",$min);
	$hour = sprintf("%02d",$hour);
	$year= 1900 + $year;
	$now->{'date'} = "$year-$month-$mday";
	$now->{'dateNr'} = "$year$monNr$mdayNr";
	$now->{'time'} = "$hour:$min:$sec";
	$now->{'HHMMSS'} = "$hour$min$sec";
	return($now);		
}


#-------------------------------------------------------------
sub do_Error {
#-------------------------------------------------------------
	my ($mySub, $myMessage) = @_;
	
	printOut("ERROR:  [$mySub]\n\t$myMessage\n");
}


#----------------------------------------------------------
sub Housekeeping	{
#----------------------------------------------------------
	my $me = whoami();
	
	if($NoPause ne "-nopause")	{
		&Clear;		
	}
	
	$myDT=$LogDateTime;
	$myDT =~ s|(\d\d\d\d)(\d\d)(\d\d)_(\d\d)(\d\d)(\d\d)|$1\/$2\/$3 $4:$5:$6|g;
	
	open(LOG, ">:utf8",  "$logfile")
		or die "Can't CREATE LOG file $logfile: $!\n"; 

	printOut("\nRUNNING: \tValidIMGs.exe (pl)\t\t$myVersion\n");
	printOut("File (OUT):  \t$logfile\n");
	printOut("Date_Time: \t$myDT\n");
	printOut("USER:        \t$username\n");
	printOut("====================================================================================\n");
	printOut("Ready to process the following folder:\n");
	printOut("*------------------------------------------------------------------------------*\n");
	printOut("\n\$Path=$Path\n\n");


# create a list of all *.xml files in the current directory
	opendir(DIR, ".");
	@files = grep(/\.xml$/,readdir(DIR));
	closedir(DIR);
	
	# GLOB images into an array so that a ignore-case search/match will work with Windows
	# using:  if( -e $_) IS CASE SENSATIVE, AS IT SHOULD BE!
	# Editors are naming files all lower case yet using CamelCase in the GenCode image tags.  Wheee!
	opendir(IMGDIR, "../HTML/images");
	@G_images = readdir(IMGDIR);
	@G_ogcase_images = @G_images;  #Copying filename array to have original filename case to compare to - Brett;    
	closedir(IMGDIR);	
	
	foreach my $Gimg (@G_images) {
		$Gimg = lc($Gimg);	
	}
}
#-------------------------------------------------------------
sub IdentifyMissingIMGs {
#-------------------------------------------------------------
	my $me = whoami();
	my $imageLine = shift;  
	my @rtnimg = ();
	
	#<img id= " alt='apre-1.jpg' src='./images/apre-1.jpg' width='' />
	#img id="img_1" alt="xyzzy.jpg" src="./images/xyzzy.jpg" width="36p" height="" />
	   
	$imageLine =~ s|^(.+?)<img(.+?) alt=(.+?) src="\.\/images\/(.+?) (.+?)$|$4|g;
	$imageLine =~ s|\"||g;	

	
	if(not(lc($imageLine) ~~ @G_images) ){
		printOut("\t+++ NOT FOUND! +++\n");
		push @G_images_not_found, $imageLine;
	} else {
		printOut("\n");
	}
}
#-------------------------------------------------------------
sub IdentifyIncorrectCaseIMGs {
#-------------------------------------------------------------
  my $imageName = shift;
  
  $imageName =~ s|^(.+?)<img(.+?) alt=(.+?) src="\.\/images\/(.+?)" (.+?)$|$4|g;
  
  if (not(($imageName) ~~ @G_ogcase_images) ) {
    printOut("\t+++ FILENAME CASE DOES NOT MATCH! +++\n");
    push @G_ogcase_dont_match, $imageName;
  } else {
    printOut("\n"); 
  }
}
#-------------------------------------------------------------
sub printOut {
#-------------------------------------------------------------
	my $parm = shift;
	
	foreach my $outfile (@FILEHANDLE) {
		print $outfile "$parm";
	}
}


#-------------------------------------------------------------
sub Update_LogDateTime {
#-------------------------------------------------------------
	$nowYYYYMMDD_HHMMSS= dateTime();
	$LogDateTime =	$nowYYYYMMDD_HHMMSS->{'dateNr'} .  "_" . $nowYYYYMMDD_HHMMSS->{'HHMMSS'};
}


#----------------------------------------------------------
sub ValidIMGsExit  {
#----------------------------------------------------------
	 $status = shift;
	 my $me = whoami();
	 
	 if($status == 0) {
	 	printOut("\n\n============================================================\n");
		printOut("\t----- FINISHED WITH ALL IMAGES FOUND! -----\n");
	 } else {
	 	printOut("\n\n============================================================\n");
		do_Error(whoami(), "\n$G_msg\n");
			
		foreach my $missing_image (@G_images_not_found) {
			printOut("\t$missing_image");
			printOut("\t+++ NOT FOUND! +++\n");
		} 
		foreach my $wrong_case_image (@G_ogcase_dont_match) {
			printOut("\t$wrong_case_image");
			printOut("\t+++ FILENAME CASE DOESN'T MATCH! +++\n");
		}
		
		
		printOut("\n");	
	}
	 
 	printOut("============================================================\n");
	close(LOG);
	exit($status);
}


#-------------------------------------------------------------
sub whoami {
#-------------------------------------------------------------
	(caller(1))[3];
}
