#!/usr/bin/perl
# ValidIMGs.pl
# Programmer:  Raymond Lillibridge
my $myVersion = "Version:  2010-12-02.1 (STABLE)";

#=============================================	
# CHANGE HISTORY:
# 2010-12-02.1 - Raymond Lillibridge - Allow -nopause processing

umask 0;
#=============================================	
# PROCESSING OUTLINE
#=============================================	
#	Reads "*.xml" files, collecting filenames and all related image file references.
#	Then reports any image references to files that do not exist.

#=============================================	
# PRAGMA
#=============================================	
use warnings;
# no warnings 'uninitialized';
use Data::Dumper;
use Encode;
use File::Basename;
use File::Listing qw(parse_dir);

#=== Win32 ===
use Cwd;
my $WinLoginName = getlogin || getpwuid($<) || "Win Login-UNKNOWN-";

# Date and Time vars
my($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime;
$year += 1900;
my $now_string = localtime;
my $nowYYYYMMDD_HHMMSS= dateTime();
my $LogDateTime =	$nowYYYYMMDD_HHMMSS->{'dateNr'} .  "_" . 

					$nowYYYYMMDD_HHMMSS->{'HHMMSS'};

#for development purposes...
use Term::InKey;


#=============================================	
# GLOBALS
#=============================================
my $NoPause=shift;
if(!defined $NoPause)	{
	$NoPause="";	
}

my $username=$WinLoginName;
my $Path=cwd();
my $str1;
my $str2;
my $ctr;
my @FILEHANDLE = (*STDOUT, *LOG);
my $logfile="_ValidIMGs_LOG.txt";
my @files=();
my $G_infilename = "";
my $G_msg = "";
my $rtn;
my $dashes = "----------------------------------------------------------------------";

#=============================================	
# MAIN
#=============================================	
Housekeeping();
ProcessFiles();

if($G_msg eq "")	{	
	ValidIMGsExit(0);	#no errors
} else {
	ValidIMGsExit(1);	#errors	
}

#=============================================	
# MAIN END
#=============================================	


#=============================================================================	
# 					S U B R O U T I N E S	
#=============================================================================	
#----------------------------------------------------------
sub ProcessFiles {
#----------------------------------------------------------
	my $me = whoami();
	my $linetemp="";
	
	foreach my $fn (@files)	{	
		next if(uc(substr($fn , 0, 4)) eq "BOOK");		# skip files that begin with "BOOK"
		next if(uc(substr($fn , 0, 1)) eq "_");			# skip files that begin with underscore
		$linetemp="";

		printOut(" $fn\n");			
		$G_infilename = $fn;
		
		open INFIL, "<:utf8", "$fn"	or die "Can't OPEN input file:  $fn  $!\n";

		while(<INFIL>)	{
			my($line) = $_;
			chomp($line);			# Strip the trailing newline from the line.
			
			if($line =~ /<img alt=/)	{				
				IdentifyMissingIMGs($line);
			}
		} # END... while(<INFIL>)
			
		close(INFIL);
	}
	
}


#-------------------------------------------------------------
sub dateTime {
#-------------------------------------------------------------
	my ($sec,$min,$hour,$mday,$monNr,$year);
	my $now = {};
	#get date and time
	($sec,$min,$hour,$mday,$monNr,$year) = localtime(time());
    	my ($date, $date_nr, $clock);
	my $month=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")[$monNr];
    	$monNr++;
	$monNr = sprintf("%02d",$monNr);
	$mdayNr = sprintf("%02d",$mday);
	$sec = sprintf("%02d",$sec);
	$min = sprintf("%02d",$min);
	$hour = sprintf("%02d",$hour);
	$year= 1900 + $year;
	$now->{'date'} = "$year-$month-$mday";
	$now->{'dateNr'} = "$year$monNr$mdayNr";
	$now->{'time'} = "$hour:$min:$sec";
	$now->{'HHMMSS'} = "$hour$min$sec";
	return($now);		
}


#-------------------------------------------------------------
sub do_Error {
#-------------------------------------------------------------
	my ($mySub, $myMessage) = @_;
	
	printOut("ERROR:  [$mySub]\n\t$myMessage\n");
}


#----------------------------------------------------------
sub Housekeeping	{
#----------------------------------------------------------
	my $me = whoami();
	if($NoPause ne "-nopause")	{
		&Clear;		
	}
	$myDT=$LogDateTime;
	$myDT =~ s|(\d\d\d\d)(\d\d)(\d\d)_(\d\d)(\d\d)(\d\d)|$1\/$2\/$3 $4:$5:$6|g;
	
	open(LOG, ">:utf8",  "$logfile")
		or die "Can't CREATE LOG file $logfile: $!\n"; 

	printOut("\nRUNNING:  ValidIMGs.exe (pl)\t\t$myVersion\n");
	printOut("Date_Time:\t\t$myDT\n");
	printOut("USER:  $username\n");
	printOut("====================================================================================\n");
	printOut("Ready to process the following folder:\n");
	printOut("*------------------------------------------------------------------------------*\n");
	printOut("\n\$Path=$Path\n\n");

	# create a list of all *.xml files in the current directory
	opendir(DIR, ".");
	@files = grep(/\.xml$/,readdir(DIR));
	closedir(DIR);
}


#-------------------------------------------------------------
sub IdentifyMissingIMGs {
#-------------------------------------------------------------
	my $me = whoami();
	my $imageLine = shift;
	
	#<img alt='apre-1.jpg' src='./images/apre-1.jpg' width='' />
	#<img alt='827-1.eps' src='./images/827-1.eps' width='' />
				
	#$imageLine =~ s|^(.+?)<img alt=(.+?)$|<img alt=$2|g;	
	$imageLine =~ s|^(.+?)<img alt=(.+?)$|<img alt=$2|g;
	$imageLine =~ s|<img alt=\'(.+?)\'(.+?)\/>|$1|g;	
	
	$imageLine = "../HTML/images/$imageLine";
	
	unless ( -e $imageLine)	{
		printOut("\t...Image NOT FOUND: $imageLine \(ERROR!\)\n");	
		$G_msg .= "\tNOT FOUND - $imageLine\n";
	}
	
#	if( -e $imageLine)	{
#		printOut("\t...Image FOUND: $imageLine\n");
#	}
}


#-------------------------------------------------------------
sub printOut {
#-------------------------------------------------------------
	my $parm = shift;
	
	foreach my $outfile (@FILEHANDLE) {
		print $outfile "$parm";
	}
}


#-------------------------------------------------------------
sub Update_LogDateTime {
#-------------------------------------------------------------
	$nowYYYYMMDD_HHMMSS= dateTime();
	$LogDateTime =	$nowYYYYMMDD_HHMMSS->{'dateNr'} .  "_" . $nowYYYYMMDD_HHMMSS->{'HHMMSS'};
}


#----------------------------------------------------------
sub ValidIMGsExit  {
#----------------------------------------------------------
	 $status = shift;
	 my $me = whoami();
	 
	 if($status == 0) {
	 	printOut("\n\n============================================================\n");
		printOut("\t----- FINISHED WITH ALL IMAGES FOUND! -----\n");
	 } else {
	 	printOut("\n\n============================================================\n");
#		do_Error(whoami(), "\n----- ERROR!  [\$status=$status]-----\n$G_msg\n");
		do_Error(whoami(), "\n$G_msg\n");
		printOut("\t----- FINISHED WITH ERRORS! -----\n");
	}
	 
 	printOut("============================================================\n");
	close(LOG);
	exit($status);
}


#-------------------------------------------------------------
sub whoami {
#-------------------------------------------------------------
	(caller(1))[3];
}
