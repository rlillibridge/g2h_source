﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Drawing;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Security.Principal;

namespace DelSpecs
{
    public partial class frm_Main : Form
    {
		public static string myVersion = "2020-01-26.1";
		public static string WorkDrive = "";

        public frm_Main(string myDrive)
        {
            InitializeComponent();
            lbl_AcctName.Text = "";
			WorkDrive = myDrive;

			Properties.Settings.Default.defaultjobspecsXML = 
				myDrive + Properties.Settings.Default.defaultjobspecsXML.Substring(2);
			Properties.Settings.Default.path7z32 = myDrive + Properties.Settings.Default.path7z32.Substring(2);
			Properties.Settings.Default.path7z64 = myDrive + Properties.Settings.Default.path7z64.Substring(2);

            lbl_Note.Text = "This application is used to modify the GenCode files (selected below), " +
                "removing all <print_specs> ... </print_specs> tags and content contained therein.";
        }

        public class HourGlass : IDisposable
        {
            public HourGlass()
            {
                Enabled = true;
            }
            public void Dispose()
            {
                Enabled = false;
            }
            public static bool Enabled
            {
                get { return Application.UseWaitCursor; }
                set
                {
                    if (value == Application.UseWaitCursor) return;
                    Application.UseWaitCursor = value;
                    Form f = Form.ActiveForm;
                    if (f != null && f.Handle != null)   // Send WM_SETCURSOR
                        SendMessage(f.Handle, 0x20, f.Handle, (IntPtr)1);
                }
            }
            [System.Runtime.InteropServices.DllImport("user32.dll")]
            private static extern IntPtr SendMessage(IntPtr hWnd, int msg, IntPtr wp, IntPtr lp);
        }

        //You can use it either directly by assigning HourGlass.Enabled or like this:
        //private void button1_Click(object sender, EventArgs e) {
        //  using (new HourGlass()) {
        //    // Do something that takes time...
        //    System.Threading.Thread.Sleep(2000);
        //  }
        //}


        //! Use instances of this class instead of TextBox
        // in order to Redirect the process of the Enter Key,
        // before the Form does it for you
        public class CustomizedTextBox : System.Windows.Forms.TextBox
        {
            // This method intercepts the Enter Key
            // signal before the containing Form does

            protected override bool ProcessCmdKey(ref System.Windows.Forms.Message m,
                      System.Windows.Forms.Keys k)
            {
                // detect the pushing (Msg) of Enter Key (k)

                if (m.Msg == 256 && k ==
                       System.Windows.Forms.Keys.Enter)
                {
                    // Execute an alternative action: here we
                    // tabulate in order to focus
                    // on the next control in the formular

                    System.Windows.Forms.SendKeys.Send("\t");
                    // return true to stop any further
                    // interpretation of this key action

                    return true;
                }
                // if not pushing Enter Key,
                // then process the signal as usual

                return base.ProcessCmdKey(ref m, k);
            }
        }



        private void btn_browse_Click(object sender, EventArgs e)
        {
            clb_GCfiles.Items.Clear();
            btn_RemovePrintSpecs.Enabled = false;
            lbl_AcctName.Text = "";
            FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
            folderBrowser.Description = "Select GenCode Account Folder";
            //folderBrowser.RootFolder = Environment.SpecialFolder.ProgramFiles;
            folderBrowser.ShowNewFolderButton = false;

            this.folderBrowserDialog1.RootFolder = Environment.SpecialFolder.MyComputer;
            folderBrowserDialog1.SelectedPath = txt_Path.Text.ToString();

            if (folderBrowser.ShowDialog() == DialogResult.OK)
            {
                txt_Path.Text = folderBrowser.SelectedPath;
            }
        }


        private void folderBrowserDialog1_HelpRequest(object sender, EventArgs e)
        {

        }


        private void frm_Main_Load(object sender, EventArgs e)
        {
            this.txt_Path.Text = Properties.Settings.Default.defaultChasePath;
            this.folderBrowserDialog1.SelectedPath = Properties.Settings.Default.defaultChasePath;
			this.Text = this.Text + " (" + myVersion.ToString() + ")";
        }


        private void btn_SelectFiles_Click(object sender, EventArgs e)
        {
            string myFolder = txt_Path.Text.ToString() + "\\" + txt_Account.Text.ToString();
            List<string> myExcludes = Read_JobSpecsXMLexcludes();

            if (txt_Account.TextLength == 0)
            {
                MessageBox.Show("Please enter a valid Account Number.", "DelSpecs - Account Entry Message", MessageBoxButtons.OK);
            }
            else
            {
                if (!Directory.Exists(myFolder))
                {
                    MessageBox.Show(txt_Account.Text + " is NOT a valid Account Number." +
                        "\n\n" + myFolder,
                        "DelSpecs - Account Entry Message", MessageBoxButtons.OK);
                }
            }

            if (Directory.Exists(myFolder))
            {
                lbl_AcctName.Text = Get_Account_Name(txt_Account.Text.ToString());
                FillCheckListBox(myExcludes, clb_GCfiles, myFolder);
                if (clb_GCfiles.Items.Count > 0)
                {
                    btn_CheckAll.Enabled = true;
                    btn_UncheckAll.Enabled = true;
                    btn_RemovePrintSpecs.Enabled = true;
                }
                else
                {
                    btn_CheckAll.Enabled = false;
                    btn_UncheckAll.Enabled = false;
                    btn_RemovePrintSpecs.Enabled = false;
                    lbl_AcctName.Text = "";
                }
            }
        }


        private static void ProcessCheckedFiles(CheckedListBox clb, string path, string acct)
        {
            // *********************************************************
            // * BEGIN BACKUP PROCESSING
            // *********************************************************
            string PMbackupFolder = string.Empty;
            XmlReader myreader = XmlReader.Create(Properties.Settings.Default.defaultjobspecsXML.ToString());
            myreader.MoveToContent();

            while (myreader.Read())
            {
                if (myreader.NodeType == XmlNodeType.Element)
                {
                    if (myreader.Name == "backupfolder")
                    {
                        PMbackupFolder = myreader.ReadString();
                    }
                }
            }


            // Message if backupFolder doesn't exist...
            if (!Directory.Exists(PMbackupFolder))
            {
                MessageBox.Show("Backup Location:\r\n" + PMbackupFolder + "\r\ndoes NOT EXIST!",
                    "DelSpecs - No Backup Folder", MessageBoxButtons.RetryCancel);
            }
            else
            {

                // create BackFolder ~\#####  (account) subfolder...
                if (!File.Exists(PMbackupFolder + "\\" + acct))
                {
                    Directory.CreateDirectory(PMbackupFolder + "\\" + acct);
                }

                DateTime date1 = DateTime.Now;
                string YMDHMS = String.Format("{0:yyyyMMdd_HHmmss}", date1);
                string BackupDir = PMbackupFolder + "\\" + acct + "\\" + YMDHMS;
                Directory.CreateDirectory(BackupDir);

                // Method used to Log username/date/acct of user
                LogScreenName(acct, date1);

                //MessageBox.Show(PMbackupFolder + "\\" + acct + "\\" + YMDHMS, "DelSpecs - BackupFolder...");

                // Backup Files...
                HourGlass.Enabled = true;

                foreach (object item in clb.Items)
                {	// ALL ITEMS...
                    string myfile = item.ToString();
                    BackupAcctFile(BackupDir, path, acct, myfile);
                }

                // ZIP FILES...
                bool myRTN = ZipFiles(BackupDir, acct);
                if (myRTN)
                {
                    // DELETE INPUT FILES THAT WERE ZIPPED...
                    string[] fileEntries = Directory.GetFiles(BackupDir);

                    foreach (object bkfile in fileEntries)
                    {
                        if (Path.GetExtension((string)bkfile) != ".zip")
                        {
                            //MessageBox.Show("This file was added to zip (and can be deleted):  " + 
                            //	bkfile + "\r\n\r\nFOLDER:  " + BackupDir, "DelSpecs");
                            File.Delete((string)bkfile);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Zip Files FAILED!", "DelSpecs - Zip Failed");
                }

                HourGlass.Enabled = false;
                //MessageBox.Show("Backup Finished!", "DelSpecs");

                // *********************************************************
                // * END BACKUP PROCESSING
                // *********************************************************
                
                // Remove <print_specs/>...
                HourGlass.Enabled = true;

                foreach (object itemChecked in clb.CheckedItems)
                {
                    string myfile = itemChecked.ToString();
                    RemovePrintSpecs(path, acct, myfile);
                }

                HourGlass.Enabled = false;
                MessageBox.Show("Finished removing:\r\n\r\n<print_specs>...</print_specs>", "DelSpecs");
            }
        }


        private static void BackupAcctFile(string PMBackupFolder, string path, string acct, string fname)
        {
            string myFile = path + "\\" + acct + "\\" + fname;
            //MessageBox.Show("FROM:  " + myFile + "\r\n    TO:  " + PMBackupFolder, "DelSpecs - Backing up file");
            File.Copy(myFile, PMBackupFolder + "\\" + fname);
        }


        private static void RemovePrintSpecs(string path, string acct, string fname)
        {
            string myFile = path + "\\" + acct + "\\" + fname;

            // Finally!  Remove the print specs!

            // Read the file as one string.
            System.IO.StreamReader ReaderFile = new System.IO.StreamReader(myFile);
            string myFileString = ReaderFile.ReadToEnd();
            ReaderFile.Close();

            // Since myFileString contains the whole file...
            string regexPattern = @"<print_specs>(.*?)</print_specs>";

            myFileString = System.Text.RegularExpressions.Regex.Replace(
                myFileString,
                regexPattern,
                string.Empty,
                System.Text.RegularExpressions.RegexOptions.Singleline
                );

            System.IO.File.WriteAllText(myFile, myFileString);
        }
        

        private static void LogScreenName(string accountNumber, DateTime dateOfClick)
        {
            string logEntry;
            
            //log username
            string userNameText = WindowsIdentity.GetCurrent().Name.Substring(7);

            logEntry = "(" + dateOfClick + ")" + " | Account: " + accountNumber + " | " + "User: " + userNameText;

            WriteToFile(logEntry);
        }

        
        public static void WriteToFile(string entry)
        {
            // This is the path to the log file
			//string path = "\\\\mcc-file-01\\folio\\master\\DelSpecs\\logs\\UserLog.txt";
			string path = WorkDrive.ToString() + "\\master\\DelSpecs\\logs\\UserLog.txt";

            // Open I/O stream and initiate file
            StreamWriter sw = File.AppendText(path);

            // Create new file if one does not exist
            if (!File.Exists(path))
            {
                File.CreateText(path);
            }
            else
            {
                // Append file with new entry
                sw.WriteLine(entry);
            }

            // Close file/stream
            sw.Close();
        }


        private void FillCheckListBox(List<string> PMexcludes, CheckedListBox clb, string targetDirectory)
        {
            
            string[] fileEntries = Directory.GetFiles(targetDirectory);
            var notUnderscoreBeginningFileNames = fileEntries.ToList()
				.Where(x => x.ToCharArray()[x.LastIndexOf('\\') + 1] != ('_'));
            int kount = 0;
            clb.Items.Clear();
            int maxDataWidth = 0;
            int width = 0;

            clb.BeginUpdate();

            // Add Files...
            foreach (string fileName in notUnderscoreBeginningFileNames)
            {
				int backslash_idx = fileName.LastIndexOf('\\');
				string str_acct = fileName.Substring(backslash_idx + 1, fileName.Length - backslash_idx - 1);
				string UCstr_acct = str_acct.ToUpper();

				// Changed logic to search for file names that contain "spec" anywhere
				// WHAT ABOUT A FILE CALLED:  'special'???

				//if (UCstr_acct.Length >= 3 && !UCstr_acct.Contains("SPEC"))
				// SKIPPING Word 'temp' files, those that begin with ~$
				if (UCstr_acct.Length >= 3 
					&& !UCstr_acct.Contains("~$")
					&& !UCstr_acct.Contains("THUMBS.DB")
					) {
					clb.Items.Add(str_acct);
					kount = clb.Items.Count - 1;

					if (FileNameExcluded(PMexcludes, str_acct)) {
						clb.SetItemChecked(kount, false);//uncheck
					} else {
						clb.SetItemChecked(kount, true);//check
					}
					// Set width of columns
					width = (int)clb.CreateGraphics().MeasureString(clb.Items[kount].ToString(), clb.Font).Width;
					width += 30;
					if (maxDataWidth < width) { maxDataWidth = width; }
					// Set the column width based on the width of each item in the list.
					clb.ColumnWidth = maxDataWidth;

				}

            }
            clb.EndUpdate();

        }

        private void clb_GCfiles_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        //private void button1_Click(object sender, EventArgs e) {
        //    WhatIsChecked(clb_GCfiles);
        //}


        private void WhatIsChecked(CheckedListBox wckd)
        {
            // First show the index and check state of all selected items.
            foreach (int indexChecked in wckd.CheckedIndices)
            {
                // The indexChecked variable contains the index of the item.
                MessageBox.Show("Index#: " + indexChecked.ToString() + ", is checked. Checked state is:" +
                                wckd.GetItemCheckState(indexChecked).ToString() + ".", "DelSpecs");
            }

            // Next show the object title and check state for each item selected.
            foreach (object itemChecked in wckd.CheckedItems)
            {

                // Use the IndexOf method to get the index of an item.
                MessageBox.Show("Item with title: \"" + itemChecked.ToString() +
                                "\", is checked. Checked state is: " +
                                wckd.GetItemCheckState(wckd.Items.IndexOf(itemChecked)).ToString() + ".", "DelSpecs");
            }
        }


        private List<string> Read_JobSpecsXMLexcludes()
        {
            List<string> PMexcludes = new List<string>();
            XmlReader myreader = XmlReader.Create(Properties.Settings.Default.defaultjobspecsXML.ToString());
            myreader.MoveToContent();

            while (myreader.Read())
            {
                if (myreader.NodeType == XmlNodeType.Element)
                {
                    if (myreader.Name == "exclude")
                    {
                        string xxx = myreader.ReadString();
                        PMexcludes.Add(xxx);
                    }
                }
            }

            return PMexcludes;
        }


        private Boolean FileNameExcluded(List<string> PMexcludes, string acct)
        {
            Boolean acctExclueded = false;
            int foundDot = 0;
            int foundMCC = 0;

            foreach (string sPattern in PMexcludes)
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(
                        acct, sPattern, System.Text.RegularExpressions.RegexOptions.IgnoreCase))
                {
                    acctExclueded = true;
                    break;
                }

                // 'Check' *.mcc files
                if (System.Text.RegularExpressions.Regex.IsMatch(
                        acct, "(.+?)(.[.])(.+?)", System.Text.RegularExpressions.RegexOptions.IgnoreCase))
                {
                    foundDot = foundMCC = 0;
                    foundDot = acct.IndexOf(".");

                    if (foundDot > 0)
                    {
                        foundMCC = acct.IndexOf(".mcc");
                        if (foundMCC > 0)
                        {
                            acctExclueded = false;
                            break;
                        }
                        else
                        {
                            acctExclueded = true;
                            break;
                        }
                    }
                }
            }

            return acctExclueded;
        }

        private void btn_CheckAll_Click(object sender, EventArgs e)
        {
            btn_RemovePrintSpecs.Enabled = true;
            for (int i = 0; i < clb_GCfiles.Items.Count; i++)
            {
                clb_GCfiles.SetItemChecked(i, true);
            }
        }

        private void btn_UncheckAll_Click(object sender, EventArgs e)
        {
            btn_RemovePrintSpecs.Enabled = false;
            for (int i = 0; i < clb_GCfiles.Items.Count; i++)
            {
                clb_GCfiles.SetItemChecked(i, false);
            }
        }

        private void btn_RemovePrintSpecs_Click(object sender, EventArgs e)
        {
            DialogResult msgRtn = MessageBox.Show("ARE YOU SURE YOU WANT TO REMOVE SPECs for:\n\n" +
                lbl_AcctName.Text.ToString() + " (" + txt_Account.Text.ToString() + ")?",
                "Remove Specs", MessageBoxButtons.OKCancel);

            if (msgRtn == DialogResult.OK)
            {
                ProcessCheckedFiles(clb_GCfiles, txt_Path.Text.ToString(), txt_Account.Text.ToString());
            }
            else
            {
                MessageBox.Show("SPECS were NOT REMOVED from Checked Files.", "Processing Cancelled", MessageBoxButtons.OK);
            }
        }


        private void txt_Account_Leave(object sender, EventArgs e)
        {
            clb_GCfiles.Items.Clear();
            btn_RemovePrintSpecs.Enabled = false;
            lbl_AcctName.Text = "";
        }

        private void clb_GCfiles_MouseUp(object sender, MouseEventArgs e)
        {
            if (clb_GCfiles.CheckedItems.Count > 0)
            {
                btn_RemovePrintSpecs.Enabled = true;
            }
            else
            {
                btn_RemovePrintSpecs.Enabled = false;
            }
        }

        private void txt_Account_TextChanged(object sender, EventArgs e)
        {
            btn_RemovePrintSpecs.Enabled = false;
            clb_GCfiles.Items.Clear();
            btn_CheckAll.Enabled = false;
            btn_UncheckAll.Enabled = false;
            lbl_AcctName.Text = "";
        }



        private static void OpenSqlConnection()
        {
            string connectionString = GetConnectionString();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                Console.WriteLine("ServerVersion: {0}", connection.ServerVersion);
                Console.WriteLine("State: {0}", connection.State);
            }
        }


        private string Get_Account_Name(string acct)
        {
            string client = "";
            string state = "";
            string code = "";
            string connectionString = GetConnectionString();

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                //connection.Open();

                //MessageBox.Show("ServerVersion: " + connection.ServerVersion + "\r\n" +
                //	"State: " + connection.State, "DelSpecs - Server Version and State");

                using (SqlCommand cmd = new SqlCommand())
                {
                    //Int32 rowsAffected;

                    cmd.CommandText = "dbo.usp_ReadProductNameByProductID";
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Connection = connection;

                    // INPUT parm
                    SqlParameter myParm1 = cmd.Parameters.Add("@ProductID", System.Data.SqlDbType.Int);
                    myParm1.Value = acct;

                    connection.Open();
                    SqlDataReader myReader = cmd.ExecuteReader();

                    while (myReader.Read())
                    {
                        //MessageBox.Show(myReader.GetName(0) + ": " + myReader.GetValue(0) + "\r\n" +
                        //    myReader.GetName(1) + ": " + myReader.GetValue(1) + "\r\n" +
                        //    myReader.GetName(2) + ": " + myReader.GetValue(2),  "DelSpecs - myReader.GetString");

                        client = myReader.GetValue(0).ToString();
                        state = myReader.GetValue(1).ToString();
                        code = myReader.GetValue(2).ToString();
                    }

                    myReader.Close();
                    connection.Close();
                }
            }

            return client + ", " + state + "  -  " + code;
        }


        static private string GetConnectionString()
        {
            // To avoid storing the connection string in your code, 
            // you can retrieve it from a configuration file, using the 
            // System.Configuration.ConfigurationSettings.AppSettings property 
            return Properties.Settings.Default.BISconn;
        }


        static private Boolean ZipFiles(string BackupDir, string acct)
        {
            MessageBox.Show("Zipping files in " + BackupDir, "DelSpecs - Zip Files");

            // Use ProcessStartInfo class
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = true;

            if (IntPtr.Size.ToString() == "4")
            {
                startInfo.FileName = Properties.Settings.Default.path7z32.ToString() + @"\7z.exe";
            }
            else
            {
                startInfo.FileName = Properties.Settings.Default.path7z64.ToString() + @"\7z.exe";
            }

            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.Arguments = "a -tzip " + BackupDir + "\\" + acct + " " + BackupDir + "\\* -r-";

            try
            {
                // Start the process with the info we specified.
                // Call WaitForExit and then the using statement will close.
                using (Process exeProcess = Process.Start(startInfo))
                {
                    exeProcess.WaitForExit();
                }
            }
            catch
            {
                MessageBox.Show("7z.exe error", "DelSpecs - 7z.exe ERROR");
            }

            return true;
        }

        private void btn_Debug_Click(object sender, EventArgs e)
        {
            string procArch = System.Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE");
            string myMsg = "Processor Architecture: " + procArch;
            MessageBox.Show(myMsg, "DEBUG:  Processor Architecture", MessageBoxButtons.OK);
        }

    }
}
