﻿namespace DelSpecs
{
    partial class frm_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.txt_Path = new System.Windows.Forms.TextBox();
			this.lbl_path = new System.Windows.Forms.Label();
			this.btn_browse = new System.Windows.Forms.Button();
			this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
			this.lbl_account = new System.Windows.Forms.Label();
			this.txt_Account = new DelSpecs.frm_Main.CustomizedTextBox();
			this.btn_SelectFiles = new System.Windows.Forms.Button();
			this.clb_GCfiles = new System.Windows.Forms.CheckedListBox();
			this.btn_RemovePrintSpecs = new System.Windows.Forms.Button();
			this.btn_CheckAll = new System.Windows.Forms.Button();
			this.btn_UncheckAll = new System.Windows.Forms.Button();
			this.lbl_AcctName = new System.Windows.Forms.Label();
			this.lbl_Note = new System.Windows.Forms.Label();
			this.btn_Debug = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// txt_Path
			// 
			this.txt_Path.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_Path.Location = new System.Drawing.Point(70, 25);
			this.txt_Path.Name = "txt_Path";
			this.txt_Path.ReadOnly = true;
			this.txt_Path.Size = new System.Drawing.Size(386, 24);
			this.txt_Path.TabIndex = 0;
			this.txt_Path.TabStop = false;
			// 
			// lbl_path
			// 
			this.lbl_path.AutoSize = true;
			this.lbl_path.Location = new System.Drawing.Point(17, 32);
			this.lbl_path.Name = "lbl_path";
			this.lbl_path.Size = new System.Drawing.Size(32, 13);
			this.lbl_path.TabIndex = 3;
			this.lbl_path.Text = "Path:";
			// 
			// btn_browse
			// 
			this.btn_browse.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btn_browse.Location = new System.Drawing.Point(462, 24);
			this.btn_browse.Name = "btn_browse";
			this.btn_browse.Size = new System.Drawing.Size(37, 24);
			this.btn_browse.TabIndex = 0;
			this.btn_browse.TabStop = false;
			this.btn_browse.Text = "•••";
			this.btn_browse.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.btn_browse.UseVisualStyleBackColor = true;
			this.btn_browse.Click += new System.EventHandler(this.btn_browse_Click);
			// 
			// lbl_account
			// 
			this.lbl_account.AutoSize = true;
			this.lbl_account.Location = new System.Drawing.Point(17, 68);
			this.lbl_account.Name = "lbl_account";
			this.lbl_account.Size = new System.Drawing.Size(50, 13);
			this.lbl_account.TabIndex = 5;
			this.lbl_account.Text = "Account:";
			// 
			// txt_Account
			// 
			this.txt_Account.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.txt_Account.Location = new System.Drawing.Point(70, 62);
			this.txt_Account.Name = "txt_Account";
			this.txt_Account.Size = new System.Drawing.Size(100, 24);
			this.txt_Account.TabIndex = 1;
			this.txt_Account.TextChanged += new System.EventHandler(this.txt_Account_TextChanged);
			this.txt_Account.Leave += new System.EventHandler(this.txt_Account_Leave);
			// 
			// btn_SelectFiles
			// 
			this.btn_SelectFiles.Location = new System.Drawing.Point(186, 63);
			this.btn_SelectFiles.Name = "btn_SelectFiles";
			this.btn_SelectFiles.Size = new System.Drawing.Size(112, 23);
			this.btn_SelectFiles.TabIndex = 5;
			this.btn_SelectFiles.Text = "Select Files (filtered)";
			this.btn_SelectFiles.UseVisualStyleBackColor = true;
			this.btn_SelectFiles.Click += new System.EventHandler(this.btn_SelectFiles_Click);
			// 
			// clb_GCfiles
			// 
			this.clb_GCfiles.CheckOnClick = true;
			this.clb_GCfiles.ColumnWidth = 150;
			this.clb_GCfiles.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.clb_GCfiles.FormattingEnabled = true;
			this.clb_GCfiles.HorizontalScrollbar = true;
			this.clb_GCfiles.Location = new System.Drawing.Point(20, 134);
			this.clb_GCfiles.MultiColumn = true;
			this.clb_GCfiles.Name = "clb_GCfiles";
			this.clb_GCfiles.Size = new System.Drawing.Size(801, 446);
			this.clb_GCfiles.Sorted = true;
			this.clb_GCfiles.TabIndex = 10;
			this.clb_GCfiles.SelectedIndexChanged += new System.EventHandler(this.clb_GCfiles_SelectedIndexChanged);
			this.clb_GCfiles.MouseUp += new System.Windows.Forms.MouseEventHandler(this.clb_GCfiles_MouseUp);
			// 
			// btn_RemovePrintSpecs
			// 
			this.btn_RemovePrintSpecs.Enabled = false;
			this.btn_RemovePrintSpecs.Location = new System.Drawing.Point(70, 102);
			this.btn_RemovePrintSpecs.Name = "btn_RemovePrintSpecs";
			this.btn_RemovePrintSpecs.Size = new System.Drawing.Size(228, 23);
			this.btn_RemovePrintSpecs.TabIndex = 10;
			this.btn_RemovePrintSpecs.Text = "Remove \"Print_Specs\" (on checked files)";
			this.btn_RemovePrintSpecs.UseVisualStyleBackColor = true;
			this.btn_RemovePrintSpecs.Click += new System.EventHandler(this.btn_RemovePrintSpecs_Click);
			// 
			// btn_CheckAll
			// 
			this.btn_CheckAll.Enabled = false;
			this.btn_CheckAll.Location = new System.Drawing.Point(339, 63);
			this.btn_CheckAll.Name = "btn_CheckAll";
			this.btn_CheckAll.Size = new System.Drawing.Size(77, 23);
			this.btn_CheckAll.TabIndex = 0;
			this.btn_CheckAll.Text = "Check All";
			this.btn_CheckAll.UseVisualStyleBackColor = true;
			this.btn_CheckAll.Click += new System.EventHandler(this.btn_CheckAll_Click);
			// 
			// btn_UncheckAll
			// 
			this.btn_UncheckAll.Enabled = false;
			this.btn_UncheckAll.Location = new System.Drawing.Point(422, 63);
			this.btn_UncheckAll.Name = "btn_UncheckAll";
			this.btn_UncheckAll.Size = new System.Drawing.Size(77, 23);
			this.btn_UncheckAll.TabIndex = 0;
			this.btn_UncheckAll.Text = "Un-Check All";
			this.btn_UncheckAll.UseVisualStyleBackColor = true;
			this.btn_UncheckAll.Click += new System.EventHandler(this.btn_UncheckAll_Click);
			// 
			// lbl_AcctName
			// 
			this.lbl_AcctName.AutoSize = true;
			this.lbl_AcctName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lbl_AcctName.Location = new System.Drawing.Point(336, 103);
			this.lbl_AcctName.Name = "lbl_AcctName";
			this.lbl_AcctName.Size = new System.Drawing.Size(118, 18);
			this.lbl_AcctName.TabIndex = 11;
			this.lbl_AcctName.Text = "Account Name";
			// 
			// lbl_Note
			// 
			this.lbl_Note.BackColor = System.Drawing.Color.Transparent;
			this.lbl_Note.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.lbl_Note.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.lbl_Note.Location = new System.Drawing.Point(531, 25);
			this.lbl_Note.Name = "lbl_Note";
			this.lbl_Note.Size = new System.Drawing.Size(290, 61);
			this.lbl_Note.TabIndex = 12;
			this.lbl_Note.Text = "Note";
			this.lbl_Note.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// btn_Debug
			// 
			this.btn_Debug.Location = new System.Drawing.Point(533, 3);
			this.btn_Debug.Name = "btn_Debug";
			this.btn_Debug.Size = new System.Drawing.Size(74, 21);
			this.btn_Debug.TabIndex = 13;
			this.btn_Debug.Text = "DEBUG";
			this.btn_Debug.UseVisualStyleBackColor = true;
			this.btn_Debug.Visible = false;
			this.btn_Debug.Click += new System.EventHandler(this.btn_Debug_Click);
			// 
			// frm_Main
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.Control;
			this.ClientSize = new System.Drawing.Size(845, 604);
			this.Controls.Add(this.btn_Debug);
			this.Controls.Add(this.lbl_Note);
			this.Controls.Add(this.lbl_AcctName);
			this.Controls.Add(this.btn_UncheckAll);
			this.Controls.Add(this.btn_CheckAll);
			this.Controls.Add(this.btn_RemovePrintSpecs);
			this.Controls.Add(this.clb_GCfiles);
			this.Controls.Add(this.btn_SelectFiles);
			this.Controls.Add(this.txt_Account);
			this.Controls.Add(this.lbl_account);
			this.Controls.Add(this.btn_browse);
			this.Controls.Add(this.lbl_path);
			this.Controls.Add(this.txt_Path);
			this.Name = "frm_Main";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "GenCode Delete Print_Specs";
			this.Load += new System.EventHandler(this.frm_Main_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

		private System.Windows.Forms.Label lbl_path;
        private System.Windows.Forms.Button btn_browse;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
		private System.Windows.Forms.Label lbl_account;
        private System.Windows.Forms.Button btn_SelectFiles;
        private System.Windows.Forms.CheckedListBox clb_GCfiles;
        private System.Windows.Forms.Button btn_RemovePrintSpecs;
        private System.Windows.Forms.Button btn_CheckAll;
        private System.Windows.Forms.Button btn_UncheckAll;
		public System.Windows.Forms.TextBox txt_Path;
		private System.Windows.Forms.Label lbl_AcctName;
		private System.Windows.Forms.Label lbl_Note;
		private System.Windows.Forms.Button btn_Debug;
		public frm_Main.CustomizedTextBox txt_Account;
    }
}

