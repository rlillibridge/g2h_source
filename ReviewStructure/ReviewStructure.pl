#!/usr/bin/perl
# ReviewStructure.pl
# Reads all files in the current folder and lists out the "heading level" information for review
# prior to running "BuildPubMap.pl" and/or the structure of the "_PubMap.xml" file.
#
# Programmer:  Raymond Lillibridge
my $myVersion = "2019-01-10.1";

#=============================================	
# WIP:
# 2019-01-10.1 - Raymond Lillibridge - Make filenames uppercase in output file

#=============================================	
# HISTORY:
# 2014-10-31.1 - Raymond Lillibridge - For Editors use - create 'config' folder if it doesn't exist
# 2013-10-03.1 - Raymond Lillibridge - Allow passing in --nopause option
# 2013-09-19.1 - Raymond Lillibridge - Testing new exe in 'branch' processing testing
# 2013-01-14.1 - Raymond Lillibridge - Not picking up the <ig;...> image to write to imageDimension.txt
# 2012-08-23.1 - Raymond Lillibridge - Problem with duplicate images when building imageDimension.txt file:
#				SOLUTION:  Had to allow for files to end with uppercase ".MCC"
# 2012-07-16.2 - Raymond Lillibridge - Changed:  
#				$line =~ s|;oh3x;|;oh3;|g;  to   $line =~ s|;oh(\d)x;|;oh$1;|g;
#				$line =~ s|;oh3a;|;oh3;|g;  to   $line =~ s|;oh(\d)a;|;oh$1;|g;
# 2012-07-16.1 - Raymond Lillibridge - Minor cosmetic changes for Editors use
# 2012-05-24.1 - Raymond Lillibridge - Added '^' divider before last entry in imageDimension.txt
# 2012-05-16.1 - Raymond Lillibridge - Added pubSeq as first field of imageDimension.txt (seq of image in whole pub)
# 2012-05-14.1 - Raymond Lillibridge - Capture image dimensions to text file ~/config/imageDimension.txt
#     This file is used by MCCdocCleanup.exe when inserting images into Word documents.
# 2011-06-07.1 - Raymond Lillibridge - Allow for processing of GenCode files: / *.mcc$/ (ENDING with .mcc)
# 2011-06-06.1 - Raymond Lillibridge - Allow for processing of GenCode files:  *.mcc

#=============================================	
# PRAGMA
#=============================================	
use warnings;
no warnings 'uninitialized';
use Term::InKey;
use File::Basename;
use File::Path qw( make_path );
use Cwd;

#=============================================	
# GET OPTIONS
#=============================================
use Getopt::Long;
my $opt_topause="";	# --topause	pause or nopause

GetOptions(
	"topause=s"	=>	\$opt_topause
);

#=============================================	
# GLOBALS
#=============================================
my $dir = getcwd;
my $ReviewFile = "_ReviewFile.txt";
my $imageDimFile = "./config/imageDimension.txt";
my $imgStr = "";
my $go;
my $line = "";
my $dashLine = "";
my $tabLine = "";
my $tabCtrString = "";
my $tabCtr = 0;
my $mySpaces = 0;
my $ctr = 1;
my $lineCtr = 1;
my $MaxFileNameLength = 0;
my $BaseTabCount = 0;
my $mod = 0;
my $dashes = "=====================================================================";
my $x = "";

my $info =	" Use: \n\n This application creates two output files used for analysis\n" . 
			" of your GenCode data files.\n" .
			" 1)  $ReviewFile - lists the heading levels for this pub.\n" . 
		  	" 2)  $imageDimFile - lists images for this pub.\n";
my ($sec,$min,$hour,$day,$month,$year,$wday,$yday,$isdst) = localtime(time);	#localtime of your system
$year+=1900;
$month+=1;
$wday+=1;

#print "sec min hour day month year wday yday isdst\n";
#print "$sec, $min, $hour, $day, $month, $year, $wday, $yday, $isdst\n";

#=============================================	
# HOUSEKEEPING
#=============================================	
## Make empty argument list default to all files
@ARGV = glob("*") unless @ARGV;
# Filter out all but plain, text files.
# @ARGV = grep { !(/^.*\./) && -f && -T} @ARGV;						#HOW TO SELECT ONLY *. FILES?????
@ARGV = grep { (!(/^.*\./) or /^.+?\.mcc$/ or /^.+?.MCC$/) && -f && -T} @ARGV;		#HOW TO SELECT ONLY *. FILES?????

for(my $fn=0; $fn<= $#ARGV; $fn++)	{
#	print STDOUT "***** [DEBUG] [$fn] $ARGV[$fn]\n";	
	$MaxFileNameLength = int(length($ARGV[$fn])) unless length($ARGV[$fn]) <= $MaxFileNameLength;
}

$BaseTabCount = int($MaxFileNameLength / 8);
$mod = $MaxFileNameLength % 8;

if($mod >= 0) { $BaseTabCount++; }

if($#ARGV == -1) {
	print(STDOUT "------------------------------\n     FILE(S) NOT FOUND!\n------------------------------\n");
	exit 0;	
}

print STDOUT "$dashes\nRUNNING:  ReviewStructure.exe (pl)\tVersion:  $myVersion\n";
print STDOUT "\n$info\n$dashes\n";

print STDOUT "\nCURRENT PATH:  $dir\n";
print STDOUT "TOPAUSE: $opt_topause\n\n";

$ctr = 0;

print STDOUT "---------------------------------------------------------------------\n" . 
			"    Press any key to continue...\t\t(CTRL + C = Cancel)\n" . 
			"---------------------------------------------------------------------\n";

if($opt_topause ne "nopause") {$x = ReadKey(); }
	
# DELETE EXISTING $ReviewFile file...
$ctr = 0;

# Create $ReviewFile file...
open(OUT, ">", $ReviewFile)
	or die "Cannot create $ReviewFile\n$!";	

if ( !-d "config" ) {
    make_path "config" or die "Failed to create path: \"config\"";
}

# create $imageDimFile...
open(IMGDIM, ">", $imageDimFile) 
	or die "Cannot create $imageDimFile\n$!";	

if($#ARGV == -1) {
	print STDOUT "------------------------------\n\t GenCode INPUT FILE(S) NOT FOUND!\n------------------------------\n";
	exit 0;	
}

#=============================================	
# MAIN
#=============================================	

# Process each file in the @ARGV array...
#print STDOUT "======================================================================\n";
#print STDOUT "Publication folder:  $dir\n";
#print STDOUT "======================================================================\n\n";
print STDOUT "\nProcessing INPUT FILE(S):\n";


print OUT "======================================================================\n";
print OUT "Publication folder:  $dir\n";
print OUT "Date:\t" . $year . "-" . $month. "-" . $day . "\n";								#print date
print OUT "Time:\t".sprintf("%02d",$hour).":".sprintf("%02d",$min).":".sprintf("%02d",$sec)."\n";	#print current time
print OUT "Application:\t\tReviewStructure.exe (pl)\tVersion:  $myVersion\n";
print OUT "Printing Note:\t\tPaste content into MS-Word, format all margins to .3 inches, set font to 'Arial'\n";
print OUT "================================================================================\n\n";

foreach $infile (@ARGV) {
	if(	($infile !~ /^_(.+)\.xml/)	and
		($infile !~ /(.+?).txt$/)	)				{		# Skip *.xml and _* files...
		print STDOUT "\t$infile\n";
		
		open(FILE, '<:', $infile)
			|| die "Cannot open input file: $infile";		
		
		while(<FILE>) {
			 my($line) = $_;
			chomp($line);			# Strip the trailing newline from the line.
			
			$line =~ s|;oh(\d)x;|;oh$1;|g;
			$line =~ s|;oh(\d)a;|;oh$1;|g;
			
			if($line =~ /^;oh[01234];/)	{
				$mySpaces = ((8 * $BaseTabCount) - length($infile));
				
				$dashLine = "";
				for( my $i = 1; $i <= $mySpaces; $i++)	{
					$dashLine = $dashLine . "_";	
				}
				
				#init tabLine...
				$tabLine = "";
				
				if($line =~ /^;oh\d;/)	{
					$tabCtrString = $line;
					
					$tabCtrString =~ s|^;oh(\d);|$1|g;
					no warnings;
					$tabCtr = ($tabCtrString += 0);
					use warnings;
					
					for( my $i = 1; $i <= $tabCtr; $i++)	{
						$tabLine = $tabLine . "\t";	
					}	
									
				} elsif($line =~ /^;oxh5;/) {
					$tabCtr = 5;
				} else {
					$tabCtr = 0;	
				}				
				
#				printf OUT ("[%05s]\t%s %s %s %s", $lineCtr, $infile, $dashLine, $tabLine, $line);
				printf OUT ("%s %s %s %s", uc($infile), $dashLine, $tabLine, $line);
				
				$line=readline(FILE);
				chomp($line);
				
				$line = substr($line, 0, 60);
				
				if(length($line) > 57) 	{
					$line = $line . "...";
				}
				
				print OUT "$line\n";
				$lineCtr++;
			}
		}
		
		close FILE;
		
		print OUT "\n";
	}
}

close OUT;

# ============================================================
# Save image dimensions to $imageDimFile...
# ============================================================
print IMGDIM $dashes . "\n";
print IMGDIM "= FILE:  " . $dir . substr($imageDimFile,1) . "\n";
print IMGDIM "= Creation Date:\t" . $year . "-" . $month . "-" . $day . "\n";			#print date
print IMGDIM "= Creation Time:\t" . $hour . ":" . $min . "." . $sec . "\n";
print IMGDIM "= DATA DESCRIPTION:\n";
print IMGDIM "= Separator character:  ^\n";
print IMGDIM "= COLUMN NAMES:  pubSeq, fileName, seq, imageName, width, height, scale info, XPP-macro type\n";
print IMGDIM $dashes . "\n";

my $imgName = "";
my $imgWidth = "";
my $imgHeight = "";
my $imgScale = "";
my $imgSeq = 0;
my $pubSeq = 1;
my $str = "";
my @igStr = ();
my @igFields = ();
my $flagFileWithImages = 0;

foreach $infile (@ARGV) {
	if(	($infile !~ /^(.+)\.xml/)	and
		($infile !~ /^_(.+?)$/)	)				{		# Skip *.xml and _* files...
				
		open(GENCODE, '<:', $infile)
			|| die "Cannot open input file: $infile";		
			
		#print STDOUT "\t" . $infile . "\n";
		
		$imgSeq = 1;
		
		while(<GENCODE>) {
			 my($line) = $_;
			chomp($line);			# Strip the trailing newline from the line.
			
			#------------------
			# Pickup Processing
			#------------------		
			if($line =~ /^{\/GRAPH;/)	{
				$flagFileWithImages = 1;
				@igFields = split /;/, $line;
				$imgName = $igFields[1];
				$imgWidth = $igFields[2];
				$imgHeight = $igFields[3];						
				$imgScale = "";
				
				if(($imgWidth eq "full") && ($imgHeight eq "full")) {$imgScale = "best";}
				if(($imgWidth eq "full") && ($imgHeight eq "comptd")) {$imgScale = "width";}
				if(($imgWidth eq "comptd") && ($imgHeight eq "full")) {$imgScale = "height";}
				if(($imgWidth eq "comptd") && ($imgHeight eq "comptd")) {$imgScale = "best";}
				
				if((substr($imgWidth, 0, 1) =~ /^\d$/) && (substr($imgHeight,0,1) !~ /^\d$/)) {$imgScale = "width";}
				if((substr($imgWidth, 0, 1) !~ /^\d$/) && (substr($imgHeight,0,1) =~ /^\d$/)) {$imgScale = "height";}
				if((substr($imgWidth, 0, 1) =~ /^\d$/) && (substr($imgHeight,0,1) =~ /^\d$/)) {$imgScale = "both";}
				
				if($imgWidth eq "") {$imgWidth = "comptd";}
				if($imgHeight eq "") {$imgHeight = "comptd";}
				if($imgScale eq "") {$imgScale = "best";}
			}
			
			if($imgName ne "")	{							
				print IMGDIM $pubSeq . "^" . $infile . "^" . $imgSeq . "^" . $imgName . "^" . $imgWidth . "^" . $imgHeight . "^" . $imgScale . "^[pickup]\n";
				#print STDOUT  "\t\t" . $pubSeq . "^" . $imgSeq . "^" . $imgName . "^" . $imgWidth . "^" . $imgHeight . "^" . $imgScale . "^[pickup]\n";
				$imgName = "";
				$imgWidth = "";
				$imgHeight = "";
				$imgScale = "";
				$imgSeq++;
				$pubSeq++;
			}	
			
			#------------------
			# <ig[t].../> Processing
			#------------------
#			if($line =~ /<ig(.?[t]);/) {
			if( ($line =~ /<ig;(.+?)>/)  |  ($line =~ /<igt;(.+?)>/) ) {
				# Since $line may contain multiple <ig/> or <igt/> macros, we'll need to split and save to process each one.
				$flagFileWithImages = 1;
				$str = $line;
#				$str =~ s|<ig(.?[t])(.+?)>|\^<ig$1$2>\^\^|g;
				$str =~ s|<ig;(.+?)>|\^<ig;$1>\^\^|g;
				$str =~ s|<igt;(.+?)>|\^<ig;$1>\^\^|g;	#Note:  comverting igt to ig for ease of processing
				
				@igStr = split /\^/, $str;
				
				for($ctr = 0; $ctr <= $#igStr; $ctr++)	 {
					$igStr[$ctr] =~ s|^(.+?)(^^)(.*?)|$1|g;				

					if(substr($igStr[$ctr], 0, 4) eq "<ig;")	{	
						@igFields = split /;/, $igStr[$ctr];
						$imgName = $igFields[1];
						$imgWidth = $igFields[2];
						$imgHeight = $igFields[3];						
						$imgScale = $igFields[5];
						
						if($imgWidth eq "") {$imgWidth = "comptd";}
						if($imgHeight eq "") {$imgHeight = "comptd";}
						if($imgScale eq "") {$imgScale = "best";}												
						
						if($imgName ne "")	{							
							print IMGDIM  $pubSeq . "^" . $infile . "^" . $imgSeq . "^" . $imgName . "^" . $imgWidth . "^" . $imgHeight . "^" . $imgScale . "^[ig(t)]\n";
							$imgName = "";
							$imgWidth = "";
							$imgHeight = "";
							$imgScale = "";
							$imgSeq++;
							$pubSeq++;
						}					
					}
				} # END:  for($ctr...
			} # END:  if($line =~ /<ig...
			
		}  #  END:  while(GENCODE...

		if($flagFileWithImages) {
			#print IMGDIM "=-------\n"; 
			$flagFileWithImages = 0;
		}
		
		#print STDOUT "\t-------\n";
		close GENCODE;
	}
}

print IMGDIM "=== FINISHED ===\n";
close IMGDIM;

print STDOUT "\n$dashes\n\n Created Files:\n\t$ReviewFile\n\t$imageDimFile\n\n";
print STDOUT "\nReviewStructure.exe (pl) FINISHED\n";

1;
