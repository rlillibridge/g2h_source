#!/usr/bin/perl
# Html2wordRegex.pl
# Applies REGEX transformations to *.html files for HTML2WordDriver.exe (SP)
#
# Programmer:  Raymond Lillibridge
my $myVersion = "Version:  2013-01-30.3 (BETA)";

# WIP:

# HISTORY:
# 2013-01-30.1 - Raymond Lillibridge - Changed step (12 of 18) to simply remove the anchor since footnotes and title/subtitle are no longer nested.

#=============================================	
# PRAGMA
#=============================================	
use warnings;
no warnings 'once';
use Term::InKey;
use File::Basename;
use File::Copy;
use Data::Dumper;

#=============================================	
# GLOBALS
#=============================================
my $outfile = "";
my $output_buffer = "";
my $key;

#=============================================	
# HOUSEKEEPING
#=============================================	
# Clear screen
#	print "Press any key to clear the screen: ";
#	$x = &ReadKey;
#  or...
#	chomp($go=<STDIN>);
#	&Clear;
#	print "You pressed $x\n";

$num_args = $#ARGV + 1;
if ($num_args != 1) {
  print STDOUT "\nHTML2WordREGEX.exe\tVersion:  $myVersion\nUsage: html2wordRegex.exe(pl) full_path_to_input.html\n\n";
  print STDOUT "Example:  R:\\Accts\\10620\\level1\\CD_COMPARATIVE_TABLEOR.html\n\n";
  exit;
}

my $infile=$ARGV[0];
my($fname,$fpath) = fileparse($infile);
my $fsuffix = substr($fname, -5);

if(uc($fsuffix) ne ".HTML" || length($fname) < 6)
{
	print STDOUT "\nHTML2WordREGEX.exe\tVersion:  $myVersion\nUsage: html2wordRegex.exe(pl) full_path_to_input.html\n\n";
	print STDOUT "Example:  R:\\Accts\\10620\\level1\\CD_COMPARATIVE_TABLEOR.html\n\n";
	print STDOUT "(+++ INPUT MUST END WITH '.html' +++)\n\n";
	print STDOUT "INPUT PATH:\t$fpath\n";
	print STDOUT "INPUT FILE:\t$fname\n";
	exit;
 }

#print STDOUT "\nRUNNING:  html2wordRegex.exe (pl)\t\t$myVersion\n";
#print STDOUT "Ready to processing the following input file:\n";
#print STDOUT "*------------------------------------------------------------------------------*\n";
#print STDOUT "INPUT PATH:\t$fpath\n";
#print STDOUT "INPUT FILE:\t$fname\n";
#print STDOUT "*------------------------------------------------------------------------------*\n\n";
#
### Process the files or quit?
#print(STDOUT "\n     continue?...\n----------------------------------------------------------------\n" . 
#	" Y = YES                                      --OR--     \n\n K = YES    \(Keep Temp Files\)                 --OR--     \n\n\(blank\) or N = CANCEL\n" . 
#	"\n----------------------------------------------------------------\n");
#	
#$x = &ReadKey;
#
#if($x =~ /[YyKk]/) {
#	# do nothing...
#	print STDOUT "\n";	#add a little space before processing the files
#} else {
#	print(STDOUT "\n\...Processing cancelled.\n");
#	exit 0 ;
#}

#=============================================	
# HOUSKEEPING - END
#=============================================	


#=============================================	
# MAIN
#=============================================	
#print STDOUT "Reading $infile into a string...\n";
# Read $infile into a scalar...
open INFILE, "<:utf8", $infile or die "Couldn't open file: $!"; 

$filestr = "";
while (<INFILE>){
	$filestr .= $_;
}
close IN;	

$outfile = ($fpath . "\\" . $fname . ".tmp");

#print STDOUT "Creating output file:  $outfile...\n";
open(OUT, ">:utf8", $outfile)		# overwrite
	|| die "cannot open file for output:  $outfile";

my $tmpstr = regex($filestr);
print OUT "$tmpstr";
close OUT;

#print STDOUT "Moving $outfile to $infile...";
move($outfile, $infile)  or die(qq{failed to move $outfile -> $infile});;
#print STDOUT "DONE!\n\n";

#print STDOUT "\tFINISHED (HTML2WordRegex.exe(pl)\n\n";

exit;


#=============================================	
# SUBROUTINES
#=============================================	
#-------------------------------------------------------------
sub regex {
#-------------------------------------------------------------
	my $parm = shift;	#input string

	#print STDOUT "Start REGEX...\n--------------------------------------------------\n";

	#ConsoleDateTime("1 of 18");
	# Remove CSS reference (single dot)...
	$parm =~ s|<link type=\"text/css\" rel=\"stylesheet\" href=\"./MCC_style.css\">||g;
	
	#ConsoleDateTime("2 of 18");
	# Remove CSS reference (double dot)...
	$parm =~ s|<link type=\"text/css\" rel=\"stylesheet\" href=\"../MCC_style.css\">||smg;
	
	#ConsoleDateTime("3 of 18");
	# Remove XML IE comments...
	$parm =~ s|^\s<!--\[if IE\]>||smg;
	
	#ConsoleDateTime("4 of 18");
	# IE continued...
	$parm =~ s|<link type=\'text/css\' rel\=\'stylesheet\' href\=\'../MCC_style_IE.css\'></link>||smg;
	
	#ConsoleDateTime("5 of 18");
	# IE continued...
	$parm =~ s|^\s<!\[endif\]-->||smg;
	
	#ConsoleDateTime("6 of 18");
	# Remove XML comments...
	$parm =~ s|<!--(.+?)-->||smg;
	
	#ConsoleDateTime("7 of 18");
	# Add newline before </div>...
	$parm =~ s|(</div>)|\n$1\n|smg;
	
	#ConsoleDateTime("8 of 18");
	# <script src=" ... </script> 
	$parm =~ s|<script src=\"(.+?)</script>||smg;
	
	#ConsoleDateTime("9 of 18");
	# <script type="text/ ... </script>
	$parm =~ s|<script type=\"text/(.+?)</script>||smg;
	
	#ConsoleDateTime("10 of 18");
	# <link rel="stylesheet" href=" ... />
	$parm =~ s|<link rel=\"stylesheet\" href=\"(.+?)\/>||smg;
	
	#ConsoleDateTime("11 of 18");
	# INSERT \\n\t\t before:  <a class="crumb" ...
	$parm =~ s|(<a class=\"crumb\")|\n\t\t$1|smg;
	
	#ConsoleDateTime("12 of 18");
	# Remove wrapper:  <a class="showURLs ... >...</a>
#	$parm =~ s|<a class=\"showURL(.+?)>(.+?)</a>|$2|smg;
	# 2013-01-30...
	# Remove:  <a class="showURLs toplink" id="TOPTITLE" href="javascript:void(0)" ></a>...
	$parm =~ s|<a class=\"showURLs(.+?)<\/a>||smg;
	
	ConsoleDateTime("13 of 18");
	# INSERT \r\n before:  <p ...
	$parm =~ s|(<p class=\")|\n\t\t$1|smg;
	
	ConsoleDateTime("14 of 18");
	# <img id="img_1" src="../images/21-29-2.jpg" alt="21-29-2.jpg" width="75%">
	$parm =~ s|<img id=\"(.+?)\" src=\"(.+?)/images/(.+?)\" alt=(.+?)>|<img src=\"$2/images/$1^$3\" alt=$4 />|smg;
	
	# SAMPLE:
	# FROM:
	#<a href="../images/10-282.1-DT1.pdf" target="_blank"><img id="img_21" src="../images/_TN_10-282.1-DT1.jpg" alt="10-282.1-DT1.pdf"></a>
	
	# ORIGINAL:
	#ConsoleDateTime("03-Start (A) - DOUBLE QUOTE");
	#$parm =~ s|<a href=\"(.+?).pdf\" target=\"_blank\"><img id=(.[\'\"])(.+?)(.[\'\"]) src=\"../images/_TN_(.+?) alt=\"(.+?).pdf\"></a>";
	#replacement = "<a href=$1.jpg\" target=$2<img src=\"../images/$3_$4 alt=\"$5.jpg\" /></a>";
	#sHTML = Regex.Replace(sHTML, pattern, replacement, RegexOptions.Singleline);
	#ConsoleDateTime("03 (A) Finished");
	
	ConsoleDateTime("15 of 18");
	$parm =~ s|<a href=\"(.+?).pdf\"|<a href=\"$1.jpg\"|smg;
	
	ConsoleDateTime("16 of 18");
	$parm =~ s|<img id=\"(.+?)\" src=\"../images/_TN_| src=\"../images/_TN_$1\_|smg;
	
	ConsoleDateTime("17 of 18");
	$parm =~ s|src=\"../images/_TN_(.+?) alt=|src=\"../images/$1 alt=|smg;
	
	ConsoleDateTime("18 of 18");
	$parm =~ s| alt=\"(.+?).pdf\"></a>| alt=\"(.+?).jpg\"></a>|smg;
	
	#print STDOUT "\t...FINISHED REGEX...\n--------------------------------------------------\n";
	
	return $parm;
}


#-------------------------------------------------------------
sub ConsoleDateTime {
#-------------------------------------------------------------
	my $str = shift;
	
	my($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime;
	
	$mon = sprintf '%02d', $mon + 1;
	$mday   = sprintf '%02d', $mday;
	$year = sprintf '%4d', $year + 1900;
	$sec = sprintf '%02d', $sec;
	$min = sprintf '%02d', $min;
	$hour = sprintf '%02d', $hour;
						
  	#print STDOUT "\t$year-$mon-$mday ($hour:$min:$sec)\t$str\n";
}


#-------------------------------------------------------------
sub whoami {
#-------------------------------------------------------------
	(caller(1))[3];
}
