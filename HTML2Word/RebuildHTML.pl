#!/usr/bin/perl
#
# RebuildHTML.pl
#
# Author: LaRae Chasteen
#
# =======================================================================
# DESCRIPTION:
# This program "re-chunks" the small HTML files in HTML\level1 through HTML\section that were created by x2h.
# The new HTML files will be:
#  * given human-readable names (as they are listed in the MCC_TOC)
#  * placed in the RTF folder
# The HTML files created by this script are to be used as input for H2W.pl or HTML2RTF.pm.
#
# =======================================================================
# HISTORY:
# 2013-03-26.1 - Raymond Lillibridge - Added explicit conversion of &amp; to 'and'
# 2013-02-21.1 - Raymond Lillibridge - Corrected problem with long titles with newlines, &c.
# 2012-12-01.1 - Raymond Lillibridge - Corrected error in truncation of file names greater than 128 characters.
# 2012-12-07.1 - Raymond Lillibridge - Changed output filename by filtering out "special characters" for SharePoint
#
# =======================================================================
# ASSUMPTIONS:
# Accepts one argument:
# * Full path and name of the MCC_TOC.HTML that should be used as a reconstruction guide.
#
# =======================================================================
# DETAILS:
# * Uses MCC_TOC.html as a file listing to retrieve files and concatenate them into HTML files that are structured differently.
# * The ("input") files being concatenated are expected to be in subfolders relative to the MCC_TOC.html.
# * The new HTML files will be:
#		* "chunked" as specified in BOOK.HTML's "expandlevel".
# 	* named as specified in MCC_TOC's <a href> for chunked level.
#		* placed in RTF folder, at same location as the input MCC_TOC.html. If ..\RTF folder does not exist, it will be created.
#
# ================== PSEUDO-CODE ===================
# Looks for book.html\head\<meta name="expandlevel" content="1"> to determine how HTML chunks should be reconstructed.
# Read MCC_TOC one line at a time. 
# Create new file for each specified level.  
# Use contents of a href as filename for each output HTML.


use warnings;
use utf8;
use File::Slurp;
use File::Basename;
use Cwd;
use Term::InKey;
use File::Spec::Functions;
use utf8;
use open IN => ":encoding(utf8)", OUT => ":bytes";

&Clear;
my $pathRTF = "";
my $basename = "";
my $ext = "";
my $splitLevel = 0;
my $counter = 0;
my $bookFilename = "";
my $bookContents = "";
my $tocContents = "";
my $level=0;
my $filename = "";
my $title = "";
my $line = "";
my $xkey;
my $inputTOC = $ARGV[0]; # should include full path
local $/=undef;

	#binmode STDOUT, ":encoding(utf8)";
	
	#Parse $inputTOC
	($basename, $pathHTML, $ext) = fileparse($inputTOC, qr/\.[^.]*/);

	print STDOUT "\$inputTOC = $inputTOC\n";
	print STDOUT "\$pathHTML = $pathHTML \n";


	# Open the book.HTML input file (gather expandlevel from this file)
	$bookFilename = $pathHTML . "book.html";
	
	print STDOUT "\$bookFilename = $bookFilename\n";
	open BOOK, "<:encoding(UTF-8)", $bookFilename or die "cannot open file for input: $bookFilename";
	$bookContents = <BOOK>;
	close BOOK;
	
	#<meta name="expandlevel" content="2">
	if ($bookContents=~ m/\<meta name=\"expandlevel\" content=\"([1-9])\"\>/) {
		$splitLevel = $1;
	}
	print STDOUT "\$splitLevel = $splitLevel\n";
	
	my @Dir = split(/\\|\//, $pathHTML); # split pathHTML at slashes
	pop (@Dir); #remove last element, which would be empty (originally a slash)
	
	$pathRTF = join("\\",@Dir) . "\\RTF\\";

	print STDOUT "\$pathRTF = $pathRTF\n";

	# Is there a sibling RTF folder? If not, then create one.
	if ( ! -d $pathRTF) {
		mkdir($pathRTF, 0777) || print STDOUT $!;
	}

	# Open the MCC_TOC.HTML input file
	open FILE, "<:encoding(UTF-8)", $inputTOC or die "cannot open file for input: $inputTOC";

	$tocContents = <FILE>;
	close FILE;
	
	$tocContents =~ s|(\<a href=\"\.\/level)([0-9])\/(.*?)(.html\" target=)|~~~\[REBUILDHTML\]$1$2\/$3$4|mg;
	my @anchor = split(/~~~/, $tocContents);	
	
#	print STDOUT "\n \@anchor size = $#anchor\n\n";
#	
#	if(defined RAL) { close RAL; }
#	open RAL, ">:encoding(UTF-8)", "Anchors.txt" or die "cannot open file for output: Anchors.txt";
#	foreach $line (@anchor) {
#		if($line =~ /\[REBUILDHTML\]/) { 
#			#$line =~ s|\[REBUILDHTML\](.*?)|$1|img;
#			print RAL "$line\n";	 
#		}
#	}
#	close RAL;
#	exit 0;	
	
	
	# <a href="./level1/CDOFGRRAMI.html" target="display_frame">CODE - OF THE CITY OFGRAND RAPIDS, MICHIGAN</a></p>
	#while ($tocContents=~ m/\<a href=\"\.\/level([0-9])\/(.*?).html\" target=\"display_frame\"\>(.*?)\<\/a\>/g) {
	#	$tocContents =~ s|\<a href=\"\.\/level([0-9])\/(.*?).html\" target=\"display_frame\"\>(.*?)\<\/a\>/||g;

	foreach $line (@anchor) {
		if($line =~ /\[REBUILDHTML\]/) { 			
			$level = $line;
			$level =~ s|^(\[REBUILDHTML\])(\<a href=\"\.\/level)(\d)(.*)|$3|s;
#print STDOUT "\n\$level=$level\n\n";			
			$filename = $line;
			$filename =~ s|^(\[REBUILDHTML\])(\<a href=\"\.\/level)(\d)\/(.*?).html\" target=(.*)|$4|s;
			$filename = $filename . ".HTML";
#print STDOUT "\$filename=$filename\n\n";			
			$title = $line;	
			$title =~ s|^(\[REBUILDHTML\])(.+?)(display_frame\"\>)(.*?)\<\/a\>(.*)|$4|s;		
#print STDOUT "\$title=...\n$title\n\n";			
			$title = trim($title) . ".HTML";
			$title =~ s|\n||s;
	
#			if($title =~ /ADMINISTRATIVE POLICIES AND PROCEDURES/) {
#				print STDOUT "\n[DEBUG TITLE AFTER WHILE...:\n$title\n";		
#				$xkey = &ReadKey;
#				exit 0;
#			}
			
			$title =~ s|\x{2012}| - |g;
			$title =~ s|\x{2013}| - |g;
			$title =~ s|\x{2014}| - |g;
			$title =~ s|\*||g;
			
			#spaces
			$title =~ s|\x{00A0}| |g; # non-breaking space
			$title =~ s|[\x{2000}-\x{200B}]| |g; # non-breaking space
			$title =~ s|\x{202F}| |g; # non-breaking space
			$title =~ s|\x{205F}| |g; # non-breaking space
			$title =~ s|\x{3000}| |g; # non-breaking space
			$title =~ s|\x{FEFF}| |g; # non-breaking space
			
			#change multiple spaces to one spaces
			while ($title=~ m/  /) {
				$title =~ s|  | |g;
			}
	
			#change line breaks to spaces
			$title =~ s|\<br( ?)\/\>| |g;
	
			# Windows filenames Filter
			$title =~ s|[\<\>:\"\/\\\|\?]|_|g;
			
			$title = SharePointFilenameFilter($title);
			
			# if level == splitLevel then create new OUT file. Otherwise, write to the open OUT file.
			if ($level <= $splitLevel) {
								
				# if OUT was already defined from last match, then close it so it can be re-initiated	
				if (defined OUT) {
					close OUT;
				}
	
				$counter++;
				my $strCounter = sprintf("%03d",$counter);
	
				#set up filename
				my $newHTML = $pathRTF . $strCounter . "_" . $title;
	
				#check values
				print STDOUT "Level: $level\n";
				print STDOUT "Output File: $newHTML\n\n";
				
				
				# Create new HTML file in RTF folder
				# open OUT, ">:encoding(utf8)", $newHTML or die "cannot open file for output: $newHTML";
				open OUT, ">", $newHTML or die "cannot open file for output: $newHTML";
			}
	
			# my $infileContents = read_file($pathHTML . "level" . $level . "/" . $filename, binmode => ':utf8');
			my $infileContents = read_file($pathHTML . "level" . $level . "/" . $filename);
			print OUT $infileContents;
		}
	}
	
	if (defined OUT) {
		close OUT;
	}

	print STDOUT "FINISHED!\n\n";
	
	1;
	
# =======================================================================
# 	S U B R O U T I N E S
# =======================================================================


# =======================================================================
sub SharePointFilenameFilter {
# =======================================================================
	my $parm = shift;
	
#print STDOUT "\n[debug  IN]: $parm\n";	
	$parm =~ s|\&amp;|and|g;

	# Folder Names and File Names
	# Do not use: " # % & * : < > ? \ / { | } ~
	my $notSP = '\"%&*:<>\?\\\/\{\|\}\~\$\=\+\[\]\'!';
	$parm =~ s|(.?[$notSP])||g;
	$parm =~ s|#|_|g;
	
	# File names cannot be longer than 128 characters
	if(length($parm) > 128) { 
		$parm = substr($parm, 0, 123) . ".html";	
	}
	
	# Do not use the period character consecutively in the middle of a file name.  For example, "file..name.docx" is invalid.
	while($parm =~ /\.\./)	{ $parm =~ s|\.\.|\.|g; }
	
	# Remove duplicate spaces
	while($parm =~ /  /) { $parm =~ s|  | |g; }
	
	# You cannot use the period character at the end of a file name
	$parm =~ s|^(.*?)(.?[\.])$|$1|g;
	
	# You cannot start a file name with the period character
	if(substr($parm, 0,1) eq "\.") { $parm = substr($parm, 1); }
	
	# Many other symbols are not recommended such as $^()-_=+[]`! (other international currency symbols and international 
	# (selected some in $notSP (above)
	
	# symbols should be avoided in site names, but some are more acceptable in file names. Ascii is preferred when possible.
	# In addition, file names and folder names may not end with: (Many of these are international symbols)
	# .files, _files , -Dateien , _fichiers , _bestanden , _file ,_archivos ,-filer,_tiedostot ,_pliki ,_soubory ,_elemei ,
	#                    _arquivos ,_dosyalar ,_datoteke ,_fitxers,_failid 
	# 
	#                   ,_fails ,_bylos ,_fajlovi,_fitxategiak
	# N/A
	
#print STDOUT "[debug OUT]: $parm\n";	

	return $parm;
}


# =======================================================================
sub trim($) {
# =======================================================================
	my $string = shift;
	$string =~ s/^\s+//;
	$string =~ s/\s+$//;
	return $string;
}


#============================
#POD Documentation
# To convert this documentation to an html file, type "C:\>pod2html RebuildHTML.pl > RebuildHTML.html" <enter>
#============================

=head1 RebuildHTML[.pl .exe] Documentation

=head2 RebuildHTML is used to assemble HTML files which are to be used as input for H2W.pl or HTML2RTF.pm.

=over 4

=item * ~\HTML\MCC_TOC.HTML will be used as a reconstruction guide.

=item * HTML files (chunks) in the .\level* folders relative to the specified HTML\MCC_TOC.html will be used as input.

=item * New HTML files will be created for each level matching the "expandlevel" in ~\HTML\book.html.

=back

=head2 The new HTML files will be:

=over 4

=item * given human-readable names (as listed in MCC_TOC.HTML`s <a href>)

=item * prepended with a sequential number to force book-order listing of files when listed alpha by name.

=item  * placed in folder ..\RTF. (If ..\RTF does not exist, it will be created)

=back

=head2 EXPECTS ONE ARGUMENT:

=head3 Full path and name of the MCC_TOC.HTML which should be used as a reconstruction guide.

=cut
