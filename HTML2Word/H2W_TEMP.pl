#!/usr/bin/perl
#
# H2W.pl
#		(driver file for HTML2Word.pm)
#
# Authors: Raymond Lillibridge, LaRae Chasteen
#
# =======================================================================
# DESCRIPTION:
# * Used for in-house creation of Word (RTF) files. (NOT used by Contegra's "Save as Word (RTF)".)
# * Middle layer application used between x2h and HTML2Word.pm
#	*	Turns input HTML FILE (or all HTML files in specified folder) into a STRING and passes it to HTML2Word.pm.
#	*	Receives STRING (RTF file contents) back from HTML2Word.pm and turns it into a Word (RTF) FILE.
# 
# =======================================================================
# ASSUMPTIONS:
# Expects two arguments: 
#		INPUT1: HTML PATH (HTML files here should be output from g2x and x2h.)
#		INPUT3: IMAGE PATH
#
# Additional command line OPTION:
#		--logFile="path/filename_log.txt" - File where HTML2Word.pm logging info should be written. 
#		(This option gets passed through to HTML2Word.pm. If not provided, then run HTML2Word.pm silently.)
#		--singleFile="filename.HTML" - Single file to be processed. 
#		(This value does NOT get passed through to HTML2Word.pm. If not provided, then all HTML files at arg1 (inputPath) will be processed.)

# =======================================================================

# PRAGMA
#use strict;
use warnings;
use Term::InKey;
&Clear;
use File::Basename;
# use Data::Dumper;
use Getopt::Long qw(:config pass_through);
use Cwd;

my $inputPath = $ARGV[0];
my $imagePath = $ARGV[1];
my $fileString = "";
my $dashes = "============================================================\n";
my $rtnNew = "";
my $rtn = "";
local $/=undef;


	# From Getopt::Long doc: It is good practice to always specify the options first, and the other arguments last. 
	our $opt_singleFile;		# --filename.HTML of SINGLE file to be processed. If not provided, then process entire folder.
	
	GetOptions(
		"singleFile:s"	=>	\$opt_singleFile,
		);

	eval "use HTML2Word_TEMP";

	
	if ($opt_singleFile) {

		# if opt_singleFile was provided as filename only, then prepend the $inputPath given as argument 1.
		if (! -e $opt_singleFile) { 
			$opt_singleFile = $inputPath . "\\" . $opt_singleFile;
		}

		FileToString ($opt_singleFile);
		GetDocName ($inputFile);
		$rtnNew = HTML2Word_TEMP::new("$fileString", "$imagePath");
		$rtn = HTML2Word_TEMP::Translate();
		StringToFile ($outputFile);
	
	} else {
	
	# Does the provided inputPath really exist? Is it really a folder?
	if ( ! -d $inputPath) {
		print STDOUT "\$inputPath ($inputPath) is not a valid folder!";
		exit 1;
	} else {
		# print STDOUT "\$inputPath = $inputPath\n";
		my $orig_CWD = getcwd(); # save original Cwd. Change back here after read in all HTML files at new Cwd.
		chdir $inputPath; # change Cwd to $inputPath
		my @files = <*.HTML>; # "glob" all the HTML files 
		@files = map { $inputPath . '/' . $_ } @files; # replace the relative ".\" path with the literal path so location is obvious.
		foreach $file (@files) {
			$file =~ s|\/|\\|g;
		 	if (-f $file) {
				FileToString ($file);
				GetDocName ($inputFile);
				$rtnNew = HTML2Word_TEMP::new("$fileString", "$imagePath");
				$rtn = HTML2Word_TEMP::Translate();
				StringToFile ($outputFile);
			}
	 	}
		chdir $orig_CWD; # change back to original Cwd.
	}
	}

sub FileToString {
	$inputFile = shift;

	# Open the HTML input file - use utf-8 encoding I/O layer
	# Read the file into a string, because that is what's expected by HTML2Word.pm
	open FILE, "<:encoding(UTF-8)", $inputFile or die "Cannot open file for input:  $inputFile";
	$fileString = <FILE>;
	close FILE;	
	
}

sub GetDocName {

	$inputFile = shift;

	(my $basename, my $inputPath, my $ext) = fileparse($inputFile, qr/\.[^.]*/);
	$outputFile = $inputPath . $basename . ".DOC";	
	print STDOUT "$outputFile...";

}

sub StringToFile {

	open DOC, ">:encoding(utf8)", $outputFile or return "Cannot open file for OUTPUT: $outputFile";
	print DOC $rtn;
	close DOC;
	print STDOUT "...Done!\n";
	

}

1;

#============================
#POD Documentation
# To convert this documentation to an html file, type "C:\>pod2html H2W.pl > H2W.html" <enter>
#============================

=head1 H2W[.pl .exe] Documentation

=head2 H2W is used as a I<driver> file for HTML2Word.pm

=head2 DESCRIPTION:

=over 4 

=item * Used for in-house creation of Word (RTF) files. 

=item * Middle layer application used between x2h and HTML2Word.pm

=item * NOT used by Contegra`s "Save as Word (RTF)".

=item *	Reads all HTML files in specified input folder into STRINGS and passes them, one-by-one to HTML2Word.pm.

=item *	Writes each STRING (RTF file contents) returned from HTML2Word.pm to a *.DOC file (marked up in RTF.)

=back 

=head2 ASSUMPTIONS:

=head3 Expects two arguments.

=over 4

=item * INPUT1: HTML PATH (HTML files here should be output from g2x and x2h.)

=item * INPUT3: IMAGE PATH

=back

=head3 Additional command line OPTIONS (case sensitive!).

=over 4 

=item * --logFile="path/filename_log.txt" - File where HTML2Word.pm logging info should be written. 

This option gets passed through to HTML2Word.pm. 

If not provided, then HTML2Word.pm runs silently.

=item * --singleFile="filename.HTML" - Single file to be processed. The html input path provided in arg1 is assumed.

This value does NOT get passed through to HTML2Word.pm. 

If not provided, then all HTML files at arg1 (inputPath) will be processed.

=back

=cut