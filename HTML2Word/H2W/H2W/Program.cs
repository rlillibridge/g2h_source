﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

class main
{
    public static void Main(string[] args)
    {
        String HTML_Filename = "c:\\accts\\16213\\RTF\\Title_2.HTML";
        String Image_Path = "C:\\accts\\16213\\HTML\\images";

        // Open the file to read from.
        String HTML_String = File.ReadAllText(HTML_Filename);

        // Console.WriteLine(HTML_String);

        // Construct object...
        HTML2Word obj = new HTML2Word(HTML_String, Image_Path);
        String rtn = obj.Translate();

        //Console.WriteLine(rtn);

    }
}