package LookupBannerInfo;
# LookupBannerInfo.pm

use warnings;
use strict;

my $VERSION	= 1.0.0;

require Exporter;
use vars qw(@ISA @EXPORT @EXPORT_OK $VERSION);
@ISA		= qw(Exporter);
@EXPORT		= qw(lookupBIS);
# @EXPORT_OK	= qw(test_error);
use Carp;

# BIS conn stuff...
use DBI;

# SQL INFO
my $dsn=q/dbi:ODBC:BIS_x64/;	#system DSN
my $user=q/G2H/;
my $pwd=q/UK@UQ!c6Cz6V/;
my $dbh;	#database handle
my $sth;	#statement handle
my $query="";

# SQL Results...
my $ProductID="-empty-";	#result set ProductID			TEST WITH:  14139
my $DefaultExpandLevel="";
my $ProductName="";
my $ClientID="";
my $Client="";
my $City="";
my $State="";
my $Zip="";
my $PoprangeID="";
my $ClassificationID="";
my $BudgetMonth="";
my $ClientST="";
my $BannerText="";
my $bannerInfo="";
my $insideBookFlag=0;
my $insideTableFlag=0;
my $LatestJobID="";
my $JobName="";
my $LatestAdoptionDate = "";
my $maxTrackingDate = "";
my $maxstartdate = "";
my $productid = "XXX";
my $banner = "XXX";
my $expandlevel = "XXX";


# -------------------------------------------------------------------
sub lookupBIS {
# -------------------------------------------------------------------
	my $job = shift;
	my $rtn_lookupBIS = "";

	&FetchBIS($job);	

	$rtn_lookupBIS = "" 
        .	"ProductID=" . $ProductID . "|"
        .	"DefaultExpandLevel=" . $DefaultExpandLevel . "|"
        .	"ProductName=" . $ProductName . "|"
        .	"ClientID=" . $ClientID . "|"
        .	"Client=" . $Client . "|"
        .	"City=" . $City . "|"
        .	"State=" . $State . "|"
        .	"Zip=" . $Zip . "|"
        .	"PoprangeID=" . $PoprangeID . "|"
        .	"ClassificationID=" . $ClassificationID . "|"
        .	"BudgetMonth=" . $BudgetMonth . "|"
        .	"ClientST=" . $ClientST . "|"
        .	"BannerText=" . $BannerText . "|"
        .	"bannerInfo=" . $bannerInfo . "|"
        .	"insideBookFlag=" . $insideBookFlag . "|"
        .	"insideTableFlag=" . $insideTableFlag . "|"
        .	"LatestJobID=" . $LatestJobID . "|"
        .	"JobName=" . $JobName . "|"
        .	"LatestAdoptionDate=" . $LatestAdoptionDate . "|"
        .	"maxTrackingDate=" . $maxTrackingDate . "|"
        .	"maxstartdate=" . $maxstartdate . "|"
        .  "productid=" . $productid . "|"
        .	"banner=" . $banner . "|"
        .	"expandlevel=" . $expandlevel
	;
    
	return $rtn_lookupBIS;
}



#-------------------------------------------------------------
sub FetchBIS {
#-------------------------------------------------------------
	my $job = shift;
	
	my $sqlErr = "";
	my $myrtn = "";
	my $str1 = "";
	my $dashes = '-' x 80;
	
#	Connect to the data source and get a handle for that connection.
	$dbh = DBI->connect($dsn, $user, $pwd)  
		or die "Can't connect to $dsn: $DBI::errstr\n$!";			

	$dbh->{ReadOnly} = 1;
	$dbh->{RaiseError} = 1;

	# Fetch ProductID, DefaultExpandLevel, ProductName, Client, State, & State-abbreviation...
	$query=
		"SELECT P.ProductID, P.DefaultExpandLevel, N.ProductName, C.ClientID, C.Client, C.City, S.State, C.Zip, M.PoprangeID, C.ClassificationID, C.BudgetMonth, S.Abbreviation\n" .
		"FROM BIS_PRODUCTS AS P, BIS_PRODUCTNAMES AS N, BIS_CLIENTS AS C, _STATES AS S, BIS_MUNIPOPS as M\n" .
		"WHERE P.ProductID = '" . $job .  "' AND\n" . 
		"P.ProductNameID = N.ProductNameID AND\n" .
		"P.ClientID = C.ClientID AND C.StateID = S.StateID AND M.ClientID = C.ClientID";
		
	$sth = $dbh->prepare($query)
	 	or die "$DBI::errstr\n\nCan't prepare query: $query\n$!";
	 	
	$sqlErr = $sth->execute
	 	or die "$DBI::errstr\n\nCan't execute query:\n$query\n\n$!\n";
	
	#Bind the results to local variables...
	$sth->bind_columns(undef, \$ProductID, \$DefaultExpandLevel, \$ProductName, \$ClientID, \$Client, \$City, \$State, \$Zip, \$PoprangeID, \$ClassificationID, \$BudgetMonth, \$ClientST)
		or die "$DBI::errstr\n\nCan't bind_columns\n$!\n";
	
	#retrieve values from the result set...
	$str1 =	$sth->fetch();	
	
	#Check to see if previous query was valid:	
	if ($ProductID eq "-empty-") {
		#------------------------
		# Second try... pared-down verion of query for incomplete entries
		#------------------------				
	
		$query=
			"SELECT P.ProductID, P.DefaultExpandLevel, N.ProductName, C.ClientID, C.Client, C.City, S.State, S.Abbreviation\n" .
			"FROM BIS_PRODUCTS AS P, BIS_PRODUCTNAMES AS N, BIS_CLIENTS AS C, _STATES AS S\n" .
			"WHERE P.ProductID = '" . $job .  "' AND\n" . 
			"P.ProductNameID = N.ProductNameID AND\n" .
			"P.ClientID = C.ClientID AND C.StateID = S.StateID";
		
		# print STDOUT "$query\n";	
		
		$sth = $dbh->prepare($query)
	 	or die "$DBI::errstr\n\nCan't prepare query: $query\n$!";
	 	
		$sqlErr = $sth->execute
	 	or die "$DBI::errstr\n\nCan't execute query:\n$query\n\n$!\n";
	
		#Bind the results to local variables...
		$sth->bind_columns(undef, \$ProductID, \$DefaultExpandLevel, \$ProductName, \$ClientID, \$Client, \$City, \$State, \$ClientST)
			or die "$DBI::errstr\n\nCan't bind_columns\n$!\n";
		
		#retrieve values from the result set...
		$str1 =	$sth->fetch();
	}
	
	if($str1)	{	
		print STDOUT "\n\n\n$dashes\n[LookupBannerInfo::FetchBIS] SQL Query Results:\n" 
			.	"$dashes\n$ProductID\n"
			.	"$ProductName\n$Client, $State, $ClientST\n"
			.	"\(DefaultExpandLevel=$DefaultExpandLevel\)\n\n\n";
	
		#------------------------
		# Fetch BannerText...
		#------------------------				
		$query = "SELECT P.ProductID, T.bannertext\n"
			.	"FROM BIS_PRODUCTS AS P, vw_ProductBannerText AS T\n"
			.	"WHERE P.ProductID = '" . $job . "' AND\n"
			.	"P.ProductID = T.productid";
	
		$sth = $dbh->prepare($query)
		 	or die "$DBI::errstr\n\nCan't prepare query: $query\n$!";
		 	
		$sqlErr = $sth->execute
		 	or die "$DBI::errstr\n\nCan't execute query:\n$query\n\n$!\n";
		
		#Bind the results to local variables...
		# NOTE: BIS_PRODUCTS.ProductID is NUMERIC therefore, no leading zeros.  i.e.  job=02116
		$sth->bind_columns(undef, \$ProductID, \$BannerText)
			or die "$DBI::errstr\n\nCan't bind_columns\n$!\n";
		
		#retrieve values from the result set...
		$str1 =	$sth->fetch();

		if($str1 eq "")	{
			print STDOUT "\n!!! fetch() FAILED!\n\nNO BANNER TEXT FOUND!\n\n\$str1=$str1\n\n";
		}
	}
	
	print STDOUT "$dashes\n[LookupBannerInfo::FetchBIS] Banner Text:\n$dashes\n$BannerText\n\n\n";
	
	if($str1)	{	
		$productid =~ s|XXX|$job|g;
		
		#Cleanup SQL Data newline characters...
		$BannerText =~s/[^[:print:]]/_/g;
		$BannerText =~s/__/\n/g;
		
		$banner =~ s|XXX|$BannerText|g;
		$expandlevel =~ s|XXX|$DefaultExpandLevel|g;
	} else {
		$productid ="";	
		$banner ="";
		$expandlevel ="";
		$job="";
	}
}


1;
