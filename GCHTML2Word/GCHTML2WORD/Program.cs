﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Word = Microsoft.Office.Interop.Word;
using Microsoft.Office.Interop.Word;

namespace GCHTML2WORD {
	class Program {
		// Convert Input.html into Output.doc

		enum ExitCode : int {
			Success = 0,
			FileNotFound = 1,
			InvalidFilename = 2,
			FilenameEmpty = 3,
			ConversionFailed = 4,
			UnknownError = 10
		}

		static int Main(string[] args) {
			string infile = "";
			string outDoc = "";
			string outRTF = "";
			string myStrUC = "";
			Boolean myRtn = false;

			string CWD = System.IO.Directory.GetCurrentDirectory();
			//Console.WriteLine("CWD is '{0}'", CWD);

			//if (cwd.EndsWith("\\bin\\Debug")) {
			//    cwd = cwd.Replace("\\bin\\Debug", "");
			//    Console.WriteLine("updated cwd is '{0}'", cwd);
			//    return (int)ExitCode.UnknownError;
			//}

			// input file provided on the command line?
			if(args.Length < 1) {
				Console.WriteLine("\n+++ [DEBUG] Please provide an HTML file for input.\n\n");
				return(int)ExitCode.FilenameEmpty;
			}

			infile = args[0];
			infile = CWD + "\\" + infile;

			// prepend working directory giving a full path name  (V:\acctR\#####\RTF\abc.html)
			myStrUC = infile.ToUpper();

			// does the infile have the proper extension?
			if (infile.Length < 6) {
				Console.WriteLine("\n+++ [DEBUG] Input File is incorrect. It should match " +
					"pattern '*.html' but your input is:\n\t" +
					infile + "\n\n");
				return (int)ExitCode.InvalidFilename;
			}

			if (myStrUC.Substring((myStrUC.Length - 4), 4) != "HTML") {
				Console.WriteLine("\n+++ [DEBUG] Input File should end with 'html' but your input is:\n\t" +
					infile + "\n\n");
				return(int)ExitCode.InvalidFilename;
			}

			// does the infile (path + filename) exist?
			if (!File.Exists(infile)) {
				Console.WriteLine("\n+++ [DEBUG] Input File not found/does not exist:\n\t" + 
					infile + "\n\n");
				return (int)ExitCode.FileNotFound;
			}

			outDoc = infile.Substring(0, (infile.Length - 4)) + "doc";
			outRTF = infile.Substring(0, (infile.Length - 4)) + "rtf";

			// convert HTML to Word Document...
			myRtn = Convert(infile, outDoc, WdSaveFormat.wdFormatDocumentDefault);
			if (!myRtn) {
				Console.WriteLine("\n+++ [DEBUG] DANG! Converting \n\t" + infile + "\n\n Failed!");
				return (int)ExitCode.ConversionFailed;
			}

			// convert HTML to RTF...
			myRtn = Convert(infile, outRTF, WdSaveFormat.wdFormatRTF);
			if (!myRtn) {
				Console.WriteLine("\n+++ [DEBUG] DANG! Converting \n\t" + infile + "\n\n Failed!");
				return (int)ExitCode.ConversionFailed;
			}

			return (int)ExitCode.Success;
		}
		

		// Convert an HTML to Word .doc, or .rtf...
		public static Boolean Convert(string input, string output, WdSaveFormat myformat) {
			// Create an instance of Word.exe
			Word._Application oWord = new Word.Application();
			oWord.DisplayAlerts = WdAlertLevel.wdAlertsNone;
			// Make this instance of word invisible (Can still see it in the taskmgr).
			oWord.Visible = false;

			// Interop requires objects.
			object oMissing = System.Reflection.Missing.Value;
			object isVisible = false;
			object readOnly = true;
			object oInput = input;
			object oOutput = output;
			object oDocFormat = myformat;
			object oConfirmConv = true;

			// Load a document into our instance of word.exe
			Word._Document oDoc = oWord.Documents.Open(ref oInput, oConfirmConv, ref readOnly, ref oMissing,
					ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
					WdOpenFormat.wdOpenFormatWebPages,
					ref oMissing, ref isVisible, ref oMissing, ref oMissing, ref oMissing, ref oMissing);

			// Make this document the active document.
			oDoc.Activate();

			// Save this document in Word 2003 format.
			oDoc.SaveAs(ref oOutput, ref oDocFormat, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
					ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing, ref oMissing,
					ref oMissing, ref oMissing);

			// Always close Word.exe.
			oWord.Quit(ref oMissing, ref oMissing, ref oMissing);

			return true;
		}
	}
}