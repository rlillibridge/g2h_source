#!/usr/bin/perl
# XMLStruct2.pl
# Reads input file from an XSLT transformation (book.xml ->book.xsl  giving book_ALL.xml).
# Converts the sequentially oriented structure to a proper nested XML XPath structure 
# to allow proper "validation" of the xml data and to be used as input to generate unique id's.
#
# Programmer:  Raymond Lillibridge
my $myVersion = "Version:  2013-09-24.1 (Stable)";

################################################################ 
#    IMPORTANT NOTES:  		TEST WITH:  14139, 19980, 13695, 10376, 13811, 13857
################################################################ 
#	INPUT:  'Book_ALL.xml' which is created (the output) by transforming book.xml -> book.xsl

# WIP:

# CHANGE HISTORY:
# 2013-09-24.1 - Raymond Lillibridge - Removed David's extra queries for JobInfo.xml
#				Build_JobInfoXML.exe now takes care of this at STEPI in g2h
# 2013-01-16.1 - Raymond Lillibridge - <footnoteref... @mark...> rewrite
# 2012-05-30.1 - David Nichols - Added queries for extra meta data & jobInfo.xml
# 2011-04-21.1 - Raymond Lillibridge - Copied from XMLStruct.pl; Removing creation of Banner*.txt
#	Because Banner*.txt is created via GenBannerInfo.exe, accessed via G2H.bat (replacing zoom.bat)
# 2011-03-25.1 - Raymond Lillibidge - added .txt extension to BANNER_INFORMATION output file.
# 2011-03-21.1 - Raymond Lillibridge - Adding export of banner info to local text file for 
#        zipping purposes later in the process
# 20101202.1 - Raymond Lillibridge - Allow "-nopause" processing
# Process multiple (per line) <footnoteref/> and <footnote/> elements properly
# 20101104.1 - David Nichols - Added logic to detect where app is running as to avoid hard-coding paths.
# 20100902.1 - Raymond Lillibridge - <footnoteref linkend /> and <footnote id /> values to be sequential, book wide
# 20100811.1 - Raymond Lillibridge - Allowed leading zeros in <productid/> element.

# 2009-09-30 (10:21am) - Adding BIS lookup for:
#	- Client, ST (root 'crumb' for all breadcrumbs in html)
#	- Client . ", " . State  	->  <bookinfo><title>...</title>
#	- ProductName(?)    	->   <bookinfo><subtitle>...</subtitle>
#	- "banner text"         	->   <bookinfo><banner>...</banner>			(ADDED TO SCHEMA)
#	- ProductID                	->   <bookinfo><productid>...</productid>		(ADDED TO SCHEMA)
#	- ProductID			->	"<book id="n" . ProductID . " ...>
#	- DefaultExpandLevel	->	<tocNode expandlevel="" 		(ADDED TO SCHEMA)

#==========================================================
# PRAGMA
#==========================================================
use warnings;
no warnings 'uninitialized';
use DBI;
use Term::InKey;
use File::Basename;
use XML::Simple qw(:strict);
my $XMLPubMapdriver;
use Data::Dumper;
use Tie::File;

use Getopt::Long;
my $opt_d;				#-d			debug switch
my $opt_job="-empty-";	# --job		job or account_number
my $opt_h;
my $opt_help;
my $opt_nopause="-empty";

GetOptions(
	"d"			=>	\$opt_d,
	"job=s" 		=>	\$opt_job,
	"h"			=>	\$opt_h,
	"help"		=>	\$opt_help,
	"topause=s"	=>  \$opt_nopause
	);

my $NoPause="$opt_nopause";
my $job="$opt_job";

# SQL INFO
my $dsn=q/dbi:ODBC:BIS/;	#system DSN
my $user=q/BIS_dbreader/;
my $pwd=q/ReadOnly1/;
my $dbh;	#database handle
my $sth;	#statement handle
my $query="";

my $ProductID="-empty-";	#result set ProductID			TEST WITH:  14139
my $DefaultExpandLevel="";
my $ProductName="";
my $ClientID="";
my $Client="";
my $City="";
my $State="";
my $Zip="";
my $PoprangeID="";
my $ClassificationID="";
my $BudgetMonth="";
my $ClientST="";
my $BannerText="";
my $bannerInfo="";
my $insideBookFlag=0;
my $insideTableFlag=0;
my $LatestJobID="";
my $JobName="";
my $LatestAdoptionDate = "";
my $maxTrackingDate;
my $maxstartdate;
my $productid = "<productid>XXX<\/productid>";
my $banner = "<banner>XXX<\/banner>";
my $expandlevel = "<expandlevel>XXX<\/expandlevel>";

# Sequential footnoteref linkend's and footnote id's:
my $fnSeq=1;
my %H_fn=();
my $fnStrOriginal="";



#==========================================================
# GLOBALS
#==========================================================

my $APPpath = "";
if (!defined $PerlApp::VERSION) {
		$APPpath = $0;
}	else {
		$APPpath = PerlApp::exe();
}

my $driveletter = substr($APPpath,0,2);
my $InputXML = "Book_ALL.xml";
my $CleanedXML = "Book_All_Cleaned.xml";
my $OutputXML = "Book_Struct.xml";
my $dashes = "--------------------------------------------------------------------------------";
my $xmlns = 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="' . $driveletter . '/SCHEMA/MCC/XSD/CODE/code.xsd"';
my $xmlns_BeginString = "xmlns";
my $xmlns_EndString = "SCHEMA\\MCC\\XSD\\CODE\\code.xsd";

my $indexStart=0;
my $indexEnd=0;

#2010-09-27 - Allow Multiple footnoteref's per "line/para"...
my @Astr=();
my $fnSeq_MultiAnchors=0;
my $footnoteIDprefix="fn_";
my $myLinkend="";
my $myRLinkend="";
my $myMark="";
my $str="";
my $myL="";

my $aClose = (10);

use constant LEVEL0 => 0;
use constant LEVEL1 => 1;
use constant LEVEL2 => 2;
use constant LEVEL3 => 3;
use constant LEVEL4 => 4;
use constant LEVEL5 => 5;
use constant LEVEL6 => 6;


my $currLevel = "";
my $prevLevel = "";
my $lastPendingClose = "";

my $idx = -1;
my $ctr = 0;
my $str1 = "";
my $str2 = "";
my $line = "";
my $lineTemp = "";

# Date and Time vars
my($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime;
$year += 1900;
my $now_string = localtime;
my $nowYYYYMMDD_HHMMSS= dateTime();
my $LogDateTime =	$nowYYYYMMDD_HHMMSS->{'dateNr'} .  "_" . 
					$nowYYYYMMDD_HHMMSS->{'HHMMSS'};
my $myDT=$LogDateTime;
$myDT =~ s|(\d\d\d\d)(\d\d)(\d\d)_(\d\d)(\d\d)(\d\d)|$1\/$2\/$3 $4:$5:$6|g;


#==========================================================
# MAIN MAIN MAIN MAIN MAIN MAIN MAIN 
#==========================================================

	&Housekeeping();
	
	if($opt_job eq "-empty-")	{
		printOut("\n\n$dashes\n\nERROR!!!\n" .
			"ProductID entered:  \"$opt_job\" IS NOT going to be a valid account number, eh?\n\n$dashes\n\n");	
		exit (1);
	}
	
	&FetchBIS;	

	## Process the file or quit?
	print STDOUT "\n     continue?...\n$dashes\nY = YES                 --OR--     (blank\) or (!Y) = CANCEL\n\n";

	if($NoPause ne "nopause")	{		
		$x = &ReadKey;
	} else {
		$x = "Y";
	}
	
	if($x=~ /[Yy]/) {
		# do nothing...
		print STDOUT "\n";	#add a little space before processing the files
	} else {
		print STDOUT "\n\...$0 Processing cancelled.\n";
		exit 0 ;
	}

	open FILE, '<:utf8', $InputXML
		|| die "cannot open file for input:  $InputXML";
		
	# Create 'cleanup' file... (removing all newlines from first <footnoteref>...to <footnote />
	open OUT, ">:utf8", $CleanedXML
		|| die "cannot open file for output:  $CleanedXML";

	# Restructure Multiple footnoteref's into a single 'line'...
	$str = "";
	
	while(<FILE>) {
		 $line = $_;
		chomp($line);			# Strip the trailing newline from the line.
		
		if( ($line =~ /<footnoteref/) &  ($line !~ /<footnote id/) )	{	# Gather all lines until "<footnote id"
			
			while($line !~ /<footnote id/) {
				$str .= $line;	
				$line=readline(FILE);
				chomp($line);	
			}
			
			$str =~ s|\n||sg;
			print OUT "$str\n";
			$str = "";
		}
		
		print OUT "$line\n";
	}
		
	close(FILE);
	close(OUT);
	
	
	open FILE, '<:utf8', $CleanedXML
		|| die "cannot open file for input:  $CleanedXML";
		
#	open FILE, '<:utf8', $InputXML
#		|| die "cannot open file for input:  $InputXML";	
		
	# Create $OutputXML file...
	open OUT, ">:utf8", $OutputXML
		|| die "cannot open file for output:  $OutputXML";
		
	while(<FILE>) {
		 $line = $_;
		chomp($line);			# Strip the trailing newline from the line.
		
		# START xmlns stuff - Add proper xmlns to <book .../> element & remove all other xmlsns... entries on <level#.../> elements
		if($line =~ /^<book/)	{
			$line =~ s|^(<book)>|$1 $xmlns>|mg;
			$insideBookFlag=1;
		} else {		
			if($line =~ /xmlns/) {
				$xmlns_BeginString="xmlns:xsi";
				$xmlns_EndString='XMLSchema-instance"';
				$indexStart = index($line, $xmlns_BeginString);	
				$indexEnd = index($line, $xmlns_EndString);
				$str1 = substr($line, 0, ($indexStart -1) );
				$str2 = substr($line, ($indexEnd + length($xmlns_EndString)) );
				$line = $str1 . $str2;
			}
			if($line =~ /xsi:noNamespace/) {
				$xmlns_BeginString="xsi:noNamespaceSchemaLocation";
				$xmlns_EndString=':\\SCHEMA\\MCC\\XSD\\CODE\\code.xsd"';
				$indexStart = index($line, $xmlns_BeginString);	
				$indexEnd = index($line, $xmlns_EndString);
				$str1 = substr($line, 0, ($indexStart -1) );
				$str2 = substr($line, ($indexEnd + length($xmlns_EndString)) );
				$line = $str1 . $str2;
			}
		}
		# END xmlns stuff.
		
		while($line =~ /  /)	{
			$line =~ s|  | |g;	
		}
		
		if($line =~ /<table/)	{
			$insideTableFlag=1;
			$fnSeq_MultiAnchors = 0;
		}
		
		if($line =~ /<\/table/)	{
			$insideTableFlag=0;
			$fnSeq_MultiAnchors = 0;
		}
		

		# Allow for multiple <footnoteref/> per line and 'container tag...	
		#Example:  <footnoteref linkend="t001_1"/>
		if($line =~ /<footnoteref id/)	{
			if($insideTableFlag < 1) 	{	# NOT inside table...
				$fnSeq_MultiAnchors = 0;
			}
			
#			$line =~ s|(<footnoteref linkend\=\")(.+?)(\"\/>)|$1$2$3<!-- footnoteref -->|g;
			$line =~ s|(<footnoteref id)(.+?)(\/>)|$1$2$3<!-- footnoteref -->|g;
			@Astr=();
			@Astr = split /<\!-- footnoteref -->/, $line;	
			
#print OUT "\n<!-- BEFORE DUMPING \@Astr\(\)... -->\n";					
#for(my $x=0; $x <= $#Astr; $x++)	{
#	print OUT "<!--[$x] $Astr[$x] -->\n";	
#}
			
			foreach $str (@Astr)	{
				
#print OUT "\n<!-- \$insideTableFlag=$insideTableFlag -->\n";				
#print OUT "<!-- \$fnSeq=$fnSeq -->\n";
#print OUT "<!-- \$fnSeq_MultiAnchors=$fnSeq_MultiAnchors -->\n";

				if($str =~ /<footnoteref/)	{
					$myMark = $fnSeq + $fnSeq_MultiAnchors;
					$myLinkend = $footnoteIDprefix . ($myMark);
					$myRLinkend = "R" . $myLinkend;
					$fnSeq_MultiAnchors++;
				}
								
				$str =~ s|(<footnoteref )(.+?) linkend\=\"(.+?)\" (.+?)\/>|$1$2 linkend=\"$myLinkend\" $4 \/>|g;				
				$str =~ s|(<footnoteref) id\=\"(.+?)\" (.+?)\/>|$1 id=\"$myRLinkend\" $3 \/>|g;		
			}
			
#print OUT "\n<!-- AFTER DUMPING \@Astr\(\)... -->\n";					
#foreach $str (@Astr)	{
#	print OUT "<!-- $str -->\n";	
#}

			if($#Astr > -1)	{
				$line = "";
				foreach my $myL (@Astr)	{
					$line = $line . $myL;
				}
			}	
					
		}	# END: if($line =~ /<footnoteref linkend/)...
		
		# <footnote id="t001_1" ...
		if($line =~ /<footnote id/)	{			
			$line =~ s|(<footnote id\=\")(.+?)(\" )(.+?)>|$1$footnoteIDprefix$fnSeq$3$4>|g;
			
#			if($fnSeq_MultiAnchors > 1)	{
#				$fnSeq = $fnSeq + ($fnSeq_MultiAnchors - 1);
#print OUT "<!-- [a] \$fnSeq=$fnSeq -->\n";				
#			} else {
				$fnSeq++;				
#print OUT "<!-- [b] \$fnSeq=$fnSeq -->\n";				
#			}
		}


#		$line =~ s|(<footnote id=\")(.+?)(_)(\d+)(\")|$1$fnPrefix$4$5|g;
		
		if($insideBookFlag > 0)	{
			
			#Book Title...
			if( ($line =~ /<title\/>/) and ($Client ne "") and ($State ne "") )	{
				$line =~ s|^([ \t]*)<title\/>|$1<title>$Client, $State<\/title>|g;
			} else {
				$line =~ s|^([ \t]*)<title\/>|$1<title>__CLIENT, STATE__<\/title>|g;
			}
			
			#Book Subtitle...
			if( ($line =~ /<subtitle\/>/) and ($ProductName ne "") )	{
				$insideBookFlag=0;
				$line =~ s|^([ \t]*)<subtitle\/>|$1<subtitle>$ProductName<\/subtitle>|g;
			} else {
				$line =~ s|^([ \t]*)<subtitle\/>|$1<subtitle>__CODE_NAME__<\/subtitle>|g;
			}
		}
	
		if($line =~ /\/bookinfo/)	{
			if($productid eq "") {$productid = "<productid>$ProductID<\/productid>";}
			if($banner eq "") {$banner = "<banner\/>";}
			if($expandlevel eq "") {$expandlevel = "<expandlevel>1<\/expandlevel>";}
			$lineTemp = $line;
			$lineTemp =~ s|^([ \t]*)<\/bookinfo>|$1\t$productid\n$1\t$banner\n$1\t$expandlevel\n$1 
			<ClientID>$ClientID<\/ClientID>
			<JobID>$LatestJobID<\/JobID>
			<JobName>$JobName</JobName>
			<Client>$Client<\/Client>
			<City>$City<\/City>
			<State>$State<\/State>
			<Zip>$Zip<\/Zip>
			<PoprangeID>$PoprangeID<\/PoprangeID>
			<ClassificationID>$ClassificationID<\/ClassificationID>
			<BudgetMonth>$BudgetMonth<\/BudgetMonth>
			<\/bookinfo>\n|g;	
			$line = $lineTemp;			
		}		
			
		if($line =~ /^([ \t]*)<level/)	{		# found OPEN LEVEL element...
			$idx = index($line, "<level");
			$currLevel = substr($line, $idx + 6, 1);		# get digit portion of level...
			$prevLevel = $currLevel - 1;
			
			$lastPendingClose = GetDeepestNestedPendingClose();	
			
			if( ($currLevel le $lastPendingClose)	and	($lastPendingClose ne "0")	)	{
				
				while(($currLevel le $lastPendingClose) and	 ($lastPendingClose ne "0"))	{			
					print OUT pop(@aClose) . "\n";
					$lastPendingClose = GetDeepestNestedPendingClose();	
				}
			}
			
			print OUT "$line\n";	
	
		} elsif($line =~ /^([ \t]*)<\/level/)	{	#found CLOSING LEVEL element...
			$idx = index($line, "<\/level");
			$idx = $idx + 7;
			$aClose[substr($line, $idx, 1)] = "\n<\/level" . substr($line, $idx, 1) . ">";			
	
		} elsif($line =~ /^([\t]*)<\/book>/)	{	# found CLOSING BOOK element...
	
			while(@aClose)	{
				print OUT pop(@aClose) . "\n";
			}
			
			print OUT "<\/book>\n";
		
		} else {	
	
			print OUT "$line\n";	
		}
	}
	
	close(FILE);
	close(OUT);	
	print STDOUT "\n\t$OutputXML created!\n\n\n";
	print STDOUT "$0 FINISHED!\n\n";

1;
#==========================================================
#	MAIN END
#==========================================================


#==========================================================
# SUBROUTINES
#==========================================================
#-------------------------------------------------------------
sub FetchBIS()	{
#-------------------------------------------------------------
	my $me = whoami();
	my $sqlErr;
	my $myrtn;
	
	$dbh->{ReadOnly} = 1;
	$dbh->{RaiseError} = 1;

	#------------------------
	# Fetch ProductID, DefaultExpandLevel, ProductName, Client, State, & State-abbreviation...
	#------------------------				
	$query=
		"SELECT P.ProductID, P.DefaultExpandLevel, N.ProductName, C.ClientID, C.Client, C.City, S.State, C.Zip, M.PoprangeID, C.ClassificationID, C.BudgetMonth, S.Abbreviation\n" .
		"FROM BIS_PRODUCTS AS P, BIS_PRODUCTNAMES AS N, BIS_CLIENTS AS C, _STATES AS S, BIS_MUNIPOPS as M\n" .
		"WHERE P.ProductID = '" . $opt_job .  "' AND\n" . 
		"P.ProductNameID = N.ProductNameID AND\n" .
		"P.ClientID = C.ClientID AND C.StateID = S.StateID AND M.ClientID = C.ClientID";
		
	$sth = $dbh->prepare($query)
	 	or die "$DBI::errstr\n\nCan't prepare query: $query\n$!";
	 	
	$sqlErr = $sth->execute
	 	or die "$DBI::errstr\n\nCan't execute query:\n$query\n\n$!\n";
	
	#Bind the results to local variables...
	$sth->bind_columns(undef, \$ProductID, \$DefaultExpandLevel, \$ProductName, \$ClientID, \$Client, \$City, \$State, \$Zip, \$PoprangeID, \$ClassificationID, \$BudgetMonth, \$ClientST)
		or die "$DBI::errstr\n\nCan't bind_columns\n$!\n";
	
	#retrieve values from the result set...
	$str1 =	$sth->fetch();
	#		or die "fetch() FAILED!\n$!\nQuery=\n$query\n";
	
	
	#Check to see if previous query was valid:	
	if ($ProductID eq "-empty-") {
		#------------------------
		# Second try... pared-down verion of query for incomplete entries
		#------------------------				
	
		$query=
			"SELECT P.ProductID, P.DefaultExpandLevel, N.ProductName, C.ClientID, C.Client, C.City, S.State, S.Abbreviation\n" .
			"FROM BIS_PRODUCTS AS P, BIS_PRODUCTNAMES AS N, BIS_CLIENTS AS C, _STATES AS S\n" .
			"WHERE P.ProductID = '" . $opt_job .  "' AND\n" . 
			"P.ProductNameID = N.ProductNameID AND\n" .
			"P.ClientID = C.ClientID AND C.StateID = S.StateID";
		
		# print STDOUT "$query\n";	
		
		$sth = $dbh->prepare($query)
	 	or die "$DBI::errstr\n\nCan't prepare query: $query\n$!";
	 	
		$sqlErr = $sth->execute
	 	or die "$DBI::errstr\n\nCan't execute query:\n$query\n\n$!\n";
	
		#Bind the results to local variables...
		$sth->bind_columns(undef, \$ProductID, \$DefaultExpandLevel, \$ProductName, \$ClientID, \$Client, \$City, \$State, \$ClientST)
			or die "$DBI::errstr\n\nCan't bind_columns\n$!\n";
		
		#retrieve values from the result set...
		$str1 =	$sth->fetch();
		#		or die "fetch() FAILED!\n$!\nQuery=\n$query\n";
	}
	
	if($str1)	{	
		print STDOUT "\n\n\nSQL Query Results:\n" .
					"$dashes\n$ProductID\n" . 
					"$ProductName\n$Client, $State, $ClientST\n" . 
					"\(DefaultExpandLevel=$DefaultExpandLevel\)\n" . 
					"$dashes\n\n\n";
	
		#------------------------
		# Fetch BannerText...
		#------------------------				
		$query=
			"SELECT P.ProductID, T.bannertext\n" .
			"FROM BIS_PRODUCTS AS P, vw_ProductBannerText AS T\n" .
			"WHERE P.ProductID = '" . $opt_job . "' AND\n" . 
			"P.ProductID = T.productid";
	
		$sth = $dbh->prepare($query)
		 	or die "$DBI::errstr\n\nCan't prepare query: $query\n$!";
		 	
		$sqlErr = $sth->execute
		 	or die "$DBI::errstr\n\nCan't execute query:\n$query\n\n$!\n";
		
		#Bind the results to local variables...
		# 20100811 - BIS_PRODUCTS.ProductID is NUMERIC!!!  Therefore, no leading zeros!  i.e.  job=02116
		$sth->bind_columns(undef, \$ProductID, \$BannerText)
			or die "$DBI::errstr\n\nCan't bind_columns\n$!\n";
					
		#retrieve values from the result set...
		$str1 =	$sth->fetch();

		if($str1 eq "")	{
			print STDOUT "\n!!! fetch() FAILED!\n\nNO BANNER TEXT FOUND!\n\n\$str1=$str1\n\n";
		}
	}
	
	print STDOUT "Banner Text:\n$dashes\n$BannerText\n$dashes\n\n";
	
################################################################ 
	# NOTICE:  JobInfo.xml is now created via Build_JobInfoXML.exe (STEPI)
################################################################ 
	# SELECTING MOST CURRENT JOBID 
#	$query = 
#		"select	top 1 t.jobId, (select jobName from tempSpeedJobNames where jobId = t.JobID) as jobname
#		, max(t.start) maxstart 
#		from	BIS_JOBS j, BIS_TRACKING t
#		where	t.JobID = j.JobID
#		and j.ProductID = " . $opt_job . "
#		and j.JobID in (select  parentJobId from BIS_EIJOBS) 
#		and j.JobNameID in (1,2,3,4,5,12,13,15,16,17,18,19,20,28,30) 
#		group by t.jobId
#		order by maxstart desc";
#
#	$sth = $dbh->prepare($query)
#		 	or die "$DBI::errstr\n\nCan't prepare query: $query\n$!";
#		 	
#	$sqlErr = $sth->execute
#	 	or die "$DBI::errstr\n\nCan't execute query:\n$query\n\n$!\n";
#		
#	#Bind the results to local variables...
#	$sth->bind_columns(undef, \$LatestJobID, \$JobName, \$maxstartdate)
#		or die "$DBI::errstr\n\nCan't bind_columns\n$!\n";
#					
#	#retrieve values from the result set...
#	$str1 =	$sth->fetch();	
#	
#	# Grab LatestAdoptionDate from bis_recordedjobs
#	
#	$query = "select convert(varchar,MAX(r.AdoptionDate),101) from BIS_RECORDEDJOBS rj , BIS_RECORDING r where rj.JobID =" . $LatestJobID . "and r.RecordingID = rj.RecordingID";
#	
#	
#	$sth = $dbh->prepare($query)
#		 	or die "$DBI::errstr\n\nCan't prepare query: $query\n$!";
#		 	
#	$sqlErr = $sth->execute
#	 	or die "$DBI::errstr\n\nCan't execute query:\n$query\n\n$!\n";
#		
#	#Bind the results to local variables...
#	$sth->bind_columns(undef, \$LatestAdoptionDate)
#		or die "$DBI::errstr\n\nCan't bind_columns\n$!\n";
#					
#	#retrieve values from the result set...
#	$str1 =	$sth->fetch();	
#	
#	# grab Ship date of job	
#	$query = "select convert(varchar,MAX(Finish),101) from BIS_TRACKING where JobID =$LatestJobID"; 
#	$sth = $dbh->prepare($query) || die "$DBI::errstr\n\nCan't prepare query: $query\n$!";
#	$sqlErr = $sth->execute;
#	$sth->bind_columns(undef, \$maxTrackingDate);
#	$str1 =	$sth->fetch();	
#	
#	#write "LatestJobID and JobName to ../HTML/JobInfo.xml
#	open JOBINFO, ">:utf8", "jobInfo.xml"
#		|| die "cannot open file for output: jobInfo.xml";
#	
#	print JOBINFO "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n
#				   <jobInfo ProductId=\"$opt_job\" JobId=\"$LatestJobID\" PublishDate=\"$LatestAdoptionDate\" JobName=\"$JobName\" MaxTrackingDate=\"$maxTrackingDate\"/>";
#
#	close JOBINFO;
#		
#	#Close the connection
#	$sth->finish();
	
	if($str1)	{
#		$productid =~ s|XXX|$ProductID|g;	
		$productid =~ s|XXX|$job|g;			#20100811 $job needs to be character, not numeric, to allow for leading zeros
		
		#Cleanup SQL Data newline characters...
		$BannerText =~s/[^[:print:]]/_/g;
		$BannerText =~s/__/\n/g;
		
		$banner =~ s|XXX|$BannerText|g;
		$expandlevel =~ s|XXX|$DefaultExpandLevel|g;
	} else {
		$productid ="";	
		$banner ="";
		$expandlevel ="";
		$job="";
	}

#			
#	print STDOUT "\n=====\n$job\n$banner\n$expandlevel\n=====\n\n";
#	
#	# Save Banner Info for creation of a Readme.txt file that will be added to zip files later.
#	open(MYBANNER, ">BANNER_INFORMATION.txt")  or
#		die "Could not create BANNER_INFORMATION.txt\n$!";
#	$bannerInfo = $banner;
#	$bannerInfo =~ s|<banner>||g;
#	$bannerInfo =~ s|<\/banner>||g;
#	print MYBANNER "$job\n$bannerInfo\n";
#	close MYBANNER;
#	
#	if(-e "BANNER_INFORMATION.txt")	{
#		$myrtn = `MOVE BANNER_INFORMATION ..`;
#	}
}


#-------------------------------------------------------------------------------------------
sub GetDeepestNestedPendingClose	{
#-------------------------------------------------------------------------------------------
#	Loops through the @aClose array and returns the largest digit (index) of the items
#	That have the value of "yes". 	
	my $rtn = "0";

	for(my $index=0;		$index <= $#aClose;		$index++ ) {
		if($aClose[$index] ne "")	{
			$rtn =  $index;
		}
	}
	
	return $rtn;
}


#-------------------------------------------------------------------------------------------
sub DumpTagInfo {
#-------------------------------------------------------------------------------------------	
	print OUT "---------------------BEGIN:  DumpTagInfo ----------------------\n";
	print OUT "<!--\n";
	
	foreach my $myStuff (@_)	{
		print(OUT "\$myStuff=$myStuff\n");
	}

	print OUT "--------------------- END:   DumpTagInfo ------------------\n";
	print OUT "-->\n";
}

#----------------------------------------------------------------------
sub Dump_aClose {
#----------------------------------------------------------------------	
	for(my $index=0;		$index <= $#aClose;		$index++ ) {
		if($aClose[$index] ne "")	{
			print(OUT "<!-- \t\t\$aClose[$index] = |$aClose[$index]| -->\n");	
		}
	}
}

#-------------------------------------------------------------
sub dateTime {
#-------------------------------------------------------------
	my $me = whoami();
	my ($sec,$min,$hour,$mday,$monNr,$year);
	my $now = {};
	#get date and time
	($sec,$min,$hour,$mday,$monNr,$year) = localtime(time());
    	my ($date, $date_nr, $clock);
	my $month=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")[$monNr];
    	$monNr++;
	$monNr = sprintf("%02d",$monNr);
	$mdayNr = sprintf("%02d",$mday);
	$sec = sprintf("%02d",$sec);
	$min = sprintf("%02d",$min);
	$hour = sprintf("%02d",$hour);
	$year= 1900 + $year;
	$now->{'date'} = "$year-$month-$mday";
	$now->{'dateNr'} = "$year$monNr$mdayNr";
	$now->{'time'} = "$hour:$min:$sec";
	$now->{'HHMMSS'} = "$hour$min$sec";
	return($now);		
}


#-------------------------------------------------------------
sub DisplayParms()	{
#-------------------------------------------------------------
	my $me=whoami();
	
	&Clear;
	print STDOUT "\nRunning XMLStruct.exe with the following parameters:\n";
	
	print STDOUT "\$opt_d=$opt_d\n";
	print STDOUT "\$opt_job=$opt_job\n";
	print STDOUT "\$opt_h=$opt_h\n";
	print STDOUT "\$opt_help=$opt_help\n";
	print STDOUT "\$opt_nopause=$opt_nopause\n";
	print STDOUT "\$NoPause=$NoPause\n";
	print STDOUT "\n(Press any key to continue...\n\n";

	$x = &ReadKey;
}
	

#-------------------------------------------------------------
sub Housekeeping	{
#-------------------------------------------------------------
#	DisplayParms();
	
	if($NoPause ne "nopause")	{
		&Clear;
	}
	
	print STDOUT "\nRUNNING:  $0\t\t$myVersion\n";
	
	$ctr = 0;
	
	if($opt_job eq "-empty-")	{
		print STDOUT "\n\n...uh, how about entering an ACCOUNT Number for me, eh?\n\n";
		print STDOUT "\tExample:  C:\\\>XMLStruct --job=12345\n\n";
		print STDOUT "*** Some accounts to test with might be:  14139, 19980, 13695, 10376 or 13811\n\n";
		exit(1);	
	}
	
	# DELETE EXISTING $BookXML file...
	$ctr = 0;
	
	if(-e $OutputXML)	{	
		$ctr = unlink($OutputXML);
		if($ctr > 0) { print STDOUT "\n...DELETED $OutputXML\n"; }
	} else {
		print STDOUT "\n...$OutputXML NOT FOUND.\n";
	}
	
	if(not -e $InputXML) {
		print STDOUT "------------------------------\n\t XML INPUT FILE ($InputXML) NOT FOUND!\n------------------------------\n";
		exit 0;	
	}
	
	print STDOUT "\nINPUT FILE:  $InputXML\n";
	
#	# Connect to the data source and get a handle for that connection.
		$dbh = DBI->connect($dsn, $user, $pwd)  
			or die "Can't connect to $dsn: $DBI::errstr\n$!";			
			
}


#--
sub DumpHash {
	my(%hash) = %{$_[0]};
	my ($k, $v);
	
	print OUT "\n<!-- DUMPING HASH -->\n";
	while ( ($k,$v) = each %hash ) {
	    print OUT "\t\t<!-- key=|$k|\t\tvalue=|$v| -->\n";
	}	
	print OUT "<!-- END DUMPING HASH -->\n\n";
}

#-------------------------------------------------------------
sub whoami {
#-------------------------------------------------------------
	(caller(1))[3];
}
