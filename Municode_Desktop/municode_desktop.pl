#!/usr/bin/perl
# municode_desktop.pl
#
# USE:  Assemble Municode Deskop product
#
my $VERSION = "2011-09-02.1";

# NOTE:  This file is formatted with tabs set to 4 spaces.  Tabs do NOT become spaces.  Experiment with font size, too!

##########################
# VERSION HISTORY (Newest First)
##########################
# 2011-7-14.1 - David Nichols - Added "Product Name" to setup.ini 
# 2011-04-26.a - Raymond Lillibridge - Adding more progress messages to STDOUT
# 2011-03-17.1 - Raymond Lillibridge - REMOVED zipping and copying to web server since X2H handles this process
# 2011-03-04.1 - Raymond Lillibridge - -nopause processing added
# 2011-03-03.2 - Raymond Lillibridge - Zip file location issue
# 2011-03-02.1 - Raymond Lillibridge - Adding 3 parameters:  acctno, workDrive, & webDrive
# 2011-02-25.1 - Raymond Lillibridge - Removing hard-coded parameters, &c.
# 2011-02-10.1 - David Nichols - Will no longer upload zip
# Programmer: David Nichols
##########################

##########################
# PRAGMA
##########################
use warnings;
use File::Copy;
use File::Copy::Recursive qw(fcopy rcopy dircopy fmove rmove dirmove);
use File::Path;
use Term::InKey;
use Tie::File;
# use Archive::Zip qw( :ERROR_CODES :CONSTANTS );
use DBI;

##########################
# GLOBALS
##########################
my $acctnum = shift;		# 10123
my $workDrive = shift;		# %DRIVE%  (like R: or V:)
my $workPath = shift;		# Work Path from batch (V:\AcctsR\  or  G:\Accts)
my $webDrive = shift;		# L: (live web)  or O: (devlibrary)
my $noPause = shift;		# "-nopause" optional argument

#Folders...
#my $workAcctFolder			= "$workDrive\\ACCTS\\$acctnum";
my $workAcctFolder			= "$workPath\\$acctnum";
my $MD_MasterFolder			= "$workDrive\\master\\MunicodeDesktop";
my $workHTMLFolder			= "$workAcctFolder\\HTML";

# ...MunicodeDesktop...
my $workMDFolder				= "$workAcctFolder\\MunicodeDesktop";
my $workMDHTMLFolder			= "$workMDFolder\\HTML";
my $workMDHTMLAcctFolder		= "$workMDHTMLFolder\\$acctnum";
my $workMDHTMLAcctXMLfile	= "$workMDHTMLAcctFolder\\$acctnum.xml";
my $workMDIndexesAcctFolder	= "$workMDFolder\\Indexes\\$acctnum";
my $workMDStartupFolder		= "$workMDFolder\\Startup";
#my $workMDFolder_2BZipped		= "$workDrive\/Accts\/$acctnum\/MunicodeDesktop";
my $workMDFolder_2BZipped		= "$workAcctFolder\\MunicodeDesktop";

my $webIndexesAcctFolder		= "$webDrive\\Indexes\\$acctnum";
my $outfile					= $acctnum . "MunicodeDesktop.zip";

my $workMDUploadFile			= "$workAcctFolder\\$outfile";
my $webDownloadMDFolder		= "$webDrive\\download\\Municode_Desktop";

#Query stuff...
my $ProductID		= "_empty_";
my $ProductName		= "_empty_";
my $Client			= "_empty_"; 
my $State			= "_empty_"; 
my $Address			= "_empty_"; 
my $City				= "_empty_"; 
my $Zip				= "_empty_"; 
my $PhoneAreaCode	= "_empty_"; 
my $Phone			= "_empty_"; 
my $URL				= "_empty_"; 
my $StateID			= "_empty_"; 
my $ClientST			= "_empty_"; 
my $BannerInfo		= "_empty_"; 

my @args=();
my $myreturn;	


###############################################
# START Municode Desktop
###############################################
if($noPause ne "-nopause")	{
	&Clear;
}

ApplicationHeader();

# Validate parameters
if($acctnum eq "") {
	print STDOUT "ERROR!\n\n\tYou must pass this program an ACCOUNT NUMBER as the first parameter.\n\n";
	die "\n\n";
}

if($workDrive eq "") {
	print STDOUT "ERROR!\n\n\tYou must pass this program a valid MAPPED DRIVE (DATA) as the second parameter.\n";
	print STDOUT "Normally, this application is run from X2H.bat which will pass in the %DRIVE% value here\n\n";
	die "\n\n";
}

if($webDrive eq "") {
	print STDOUT "ERROR!\n\n\tYou must pass this program a valid MAPPED DRIVE (WEB) as the third parameter.\n\n";
	die "\n\n";
}

print STDOUT "\tAccount:\t$acctnum\n";
print STDOUT "\tDATA Drive:\t" . $workDrive . "\n";
print STDOUT "\tWEB Drive:\t" . $webDrive . "\n\n\n";

print STDOUT "\tProcessing...\n\n";

if (-d $workMDFolder) {
	print STDOUT "\tClearing old files... ($workMDFolder)...";
	$myreturn = rmtree($workMDFolder);
	print STDOUT "DONE!\n";
}

print STDOUT "\tCopying $MD_MasterFolder to $workMDFolder...";

if(! dircopy($MD_MasterFolder,$workMDFolder) ) {
	die "ERROR!  Unable to copy $MD_MasterFolder to $workMDFolder\n $!\n";
}

print STDOUT "DONE!\n\n";
print STDOUT "\tBuilding Municode Desktop for Account $acctnum!	(could take a few minutes)...\n";

if (-d "$workHTMLFolder") {
	if(-e "$workMDHTMLAcctFolder")	{
		print STDOUT "\t...Removing: $workMDHTMLAcctFolder...";
		$myreturn = rmtree($workMDHTMLAcctFolder);
		print STDOUT "DONE!\n";
	}
	
	print STDOUT "\t...Copying Folder: $workHTMLFolder...to: $workMDHTMLAcctFolder...";
	# THE NEXT THREE LINES:  flush STDOUT buffer...
	my $old_fh = select(STDOUT);
	$| = 1;
	select($old_fh);

	if(! dircopy($workHTMLFolder,$workMDHTMLAcctFolder) ) {
		die "ERROR!  Unable to copy $workHTMLFolder to $workMDHTMLAcctFolder\n $!\n";
	}
	print STDOUT "DONE!\n\n";
} else {
	print STDOUT "ERROR!\n\tOops... looks like there isn't an $workHTMLFolder folder for me to copy.";
	die "ERROR!  $workHTMLFolder does NOT Exist.\n\n";
}

print STDOUT "\tQuerying the database...";
$myreturn = DBQuery();
print STDOUT "DONE!\n\n";

print STDOUT "\tPassing data to setup files/landing page...";


# DUMP Query data to STDOUT...
#print STDOUT "\n\n----- Query Results -----\n";
#print STDOUT "\$ProductID=$ProductID\n";
#print STDOUT "\$ProductName=$ProductName\n";
#print STDOUT "\$Client=$Client\n"; 
#print STDOUT "\$State=$State\n"; 
#print STDOUT "\$Address=$Address\n"; 
#print STDOUT "\$City=$City\n"; 
#print STDOUT "\$Zip=$Zip\n"; 
#print STDOUT "\$PhoneAreaCode=$Address\n"; 
#print STDOUT "\$Phone=$Phone\n";
#print STDOUT "\$URL=$URL\n"; 
#print STDOUT "\$StateID=$StateID\n"; 
#print STDOUT "\$ClientST=$ClientST\n"; 
#print STDOUT "\$BannerInfo=$BannerInfo\n-------------------------\n\n"; 


###############################################
# Passing database info back into files
###############################################
# Inserting 'break rule' in place of newlines for HTML
$BannerInfo =~ s|\n|\n<br/>|g;

if(-e "$workMDHTMLFolder\\home.html")	{
	open (FILE, "$workMDHTMLFolder\\home.html")
		or die "\nERROR! Can NOT OPEN file:  $workMDHTMLFolder\\home.html\n $!\n\n";
	@home = <FILE>;
	close (FILE);
} else {
	die "\nERROR!  $workMDHTMLFolder\\home.html does not exist!\n $!\n\n";
}

#Insert Hyphen into phone number...
$Phone =~ s|(\d\d\d)(\d\d\d\d)|$1\-$2|g;

# Search & replace in home.html with new info from database...
foreach $line (@home) {
	$line =~ s|<div class="doctitle">\n|<div class="doctitle">\n$BannerInfo\n|;
	$line =~ s|<span id="ctl00_lbAddressClient"></span>|<span id="ctl00_lbAddressClient">$Client, $ClientST</span>|g;
	$line =~ s|<span id="ctl00_lbAddress"></span>|<span id="ctl00_lbAddress">$Address</span>|g;
	$line =~ s|<span id="ctl00_lbCity"></span>|<span id="ctl00_lbCity">$City, $ClientST $Zip</span>|g;
	$line =~ s|<span id="ctl00_lbPhone"></span>|<span id="ctl00_lbPhone">($PhoneAreaCode) $Phone</span>|g;
	$line =~ s|<a id="ctl00_hlWebSite" href=""></a>|<a id="ctl00_hlWebSite" href="http://$URL" target="_blank">$URL</a>|g;
}

open (HOME, ">$workMDHTMLFolder\\home.html");
print HOME "@home";
close (HOME);

print STDOUT "DONE!\n\n";
print STDOUT "\tEditing files...";

if (-e $workMDIndexesAcctFolder) {
	rmtree($workMDIndexesAcctFolder);
}

dircopy($webIndexesAcctFolder, $workMDIndexesAcctFolder)
	or die "ERROR!  Could NOT copy $webIndexesAcctFolder to $workMDIndexesAcctFolder\n(It probably does NOT exist.)\n $!\n";

###############################################
# Read and Edit "municode.exe.config"
###############################################
open (STARTUP, "$workMDStartupFolder\\municode.exe.config");
@startup = <STARTUP>;
close (STARTUP);
	
foreach $line (@startup) {
	$line =~ s|\<add key\=\"home\" value\=\"index.aspx\?clientID\=\&amp\;stateid\=\&amp\;statename\=\"\/>|<add key\=\"home\" value\=\"index.aspx\?clientID\=$acctnum\&amp\;stateid\=$StateID\&amp\;statename\=$State\"\/>|g;
}

open (CONFIG, ">$workMDStartupFolder\\municode.exe.config");
print CONFIG "@startup";	
close (CONFIG);


###############################################
# Read and Edit "setup.ini"
###############################################
open (SETUP, "$workMDStartupFolder\\setup.ini");
@setup = <SETUP>;
close (SETUP);
	
foreach $line (@setup) {
	$line =~ s|dirGroup = |dirGroup = $Client\, $ClientST $ProductName|g;
}

open (SETUPDONE, ">$workMDStartupFolder\\setup.ini");
print SETUPDONE "@setup";	
close (SETUPDONE);


###############################################
# Copying and pasting #####.xml
###############################################
move($workMDHTMLAcctXMLfile, $workMDHTMLFolder);


###############################################
# Zipping up files
###############################################
#print STDOUT "\tDONE!\n\n\tZipping files...\n";
#my $rtn = "";
#my $zipname = $outfile;
#
#chdir $workAcctFolder;
#
#my $zip_obj = Archive::Zip->new()
#	or die "ERROR! Can not create a new Archive\:\:Zip object.\n $!\n";
#	
#print STDOUT "\t...zip_obj created\n\t...addTree\($workMDFolder_2BZipped\)";	
#		
#$rtn = $zip_obj->addTree("$workMDFolder_2BZipped");
#die "ERROR! Called function addTree\($workMDFolder_2BZipped\) FAILED.\n\$rtn=$rtn\n $!\n" if $rtn != AZ_OK;
#	
#print STDOUT "\tDONE!\n";	
#
#print STDOUT "\t...writeToFileNamed\($zipname\)";	
#	
#$rtn = $zip_obj->writeToFileNamed("$zipname");
#die "ERROR!  Called function writeToFileNamed\($zipname\) FAILED.\n\$rtn=$rtn\n $!\n" if $rtn != AZ_OK;
#
#print STDOUT "\tDONE!\n\n";
#
#
################################################
## Copying to web server...
################################################
#print STDOUT "\tCopying files...\n";
#print STDOUT "\t    FROM: $workMDUploadFile\n\t      TO: $webDownloadMDFolder folder \(HTML server\)...\n\n";
#
#copy("$workMDUploadFile", "$webDownloadMDFolder")
#	or die "ERROR! Could NOT COPY...\n\tFROM:  $workMDUploadFile\n\t  TO:  $webDownloadMDFolder\n$!\n";

print STDOUT "\n\nCongrats!  Your Municode Desktop has finished building for $acctnum!\n";

exit;


###############################################
#=============================================
# SUBROUTINES
#=============================================
#----------------------------------------------------------
sub ApplicationHeader {
#----------------------------------------------------------
	my $dashes = "==================================================================================";
	my $dt=`echo %DATE% %TIME%`;
	print STDOUT "$dashes\n\n";
	print STDOUT "\tMUNICODE DESKTOP (V. $VERSION)\t$dt\n";
	
	if($noPause eq "-nopause")	{
		print STDOUT "\nNo Pause    No Pause    No Pause    No Pause    No Pause\n";
	}
	
	print STDOUT "$dashes\n";
}


#----------------------------------------------------------
sub DBQuery {
#----------------------------------------------------------
	my $dsn="dbi:ODBC:BIS";	#system DSN
	my $user="BIS_dbreader";
	my $pw="ReadOnly1";
	my $query="";
	my $myqueryresults="";
	
	# PERL DBI CONNECT
	$connect = DBI->connect($dsn, $user, $pw);
	$connect->{ReadOnly} = 1;
	$connect->{RaiseError} = 1;
	
	# PREPARE THE QUERY
	$query = 
			"SELECT P.ProductID, N.ProductName, C.Client, Address, City, Zip, PhoneAreaCode, Phone, URL, S.StateID, S.State, S.Abbreviation, T.bannertext\n" .
			"FROM BIS_PRODUCTS AS P, BIS_PRODUCTNAMES AS N, BIS_CLIENTS AS C, _STATES AS S, vw_ProductBannerText AS T\n" .
			"WHERE P.ProductID = '" . $acctnum . "' AND\n" . 
			"P.ProductNameID = N.ProductNameID AND\n" .
			"P.ClientID = C.ClientID AND C.StateID = S.StateID AND P.ProductID = T.productid";
	
	# print STDOUT "\$query\n$query\n\n";
			
	$myqueryresults = $connect->prepare($query)
		or die "$DBI::errstr\n\nCan't prepare query: $query\n$!";
	
	# EXECUTE THE QUERY
	$myqueryresults->execute()
		or die "$DBI::errstr\n\nCan't execute query: $query\n$!";
	
	# Binding the query results to scalars to be passed back into file contents later...
	$myqueryresults->bind_columns(undef, \$ProductID, \$ProductName, \$Client, \$Address, \$City, \$Zip, \$PhoneAreaCode, \$Phone, \$URL, \$StateID, \$State, \$ClientST, \$BannerInfo)
		or die "ERROR!  Can NOT 'bind_columns' see\: DBQuery\(\)\n $!\n";
	
	$myqueryresults->fetch();
	
	return 1;	
}
