#!/usr/bin/perl

# MUNICODE DESKTOP
#Version 1.0 [BETA]
#Programmer: David Nichols


##########################
########  PRAGMA  ########
##########################

use warnings;
use File::Copy::Recursive qw(fcopy rcopy dircopy fmove rmove dirmove);
use File::Path;
use Term::InKey;
use Tie::File;
use Archive::Zip qw( :ERROR_CODES :CONSTANTS );


use DBI;
# use DBD::mysql;

##########################
######## GLOBALS #########
##########################


$acctnum = $ARGV[0];

my $outfile = $acctnum . "MunicodeDesktop.zip";

$dirtobecopied = "R:\\master\\MunicodeDesktop";
$newdir = "R:\\ACCTS\\$acctnum\\MunicodeDesktop";
# $Indexes = "R:\\ACCTS\\$acctnum\\MunicodeDesktop\\Indexes\\$acctnum";
$html = "R:\\ACCTS\\$acctnum\\HTML";
$mcdt = "R:\\ACCTS\\$acctnum\\MunicodeDesktop\\HTML\\$acctnum";
$Webindex = "L:\\Indexes\\$acctnum";
$Newindex = "R:\\ACCTS\\$acctnum\\MunicodeDesktop\\Indexes\\$acctnum";
$Startup = "R:\\ACCTS\\$acctnum\\MunicodeDesktop\\Startup";
$filestobeziped = "R:\\ACCTS\\$acctnum\\MunicodeDesktop";
# $housekeeping = "R:\\ACCTS\\$acctnum\\$acctnumMunicodeDesktop";
$xml = "R:\\ACCTS\\$acctnum\\MunicodeDesktop\\HTML\\$acctnum\\$acctnum.xml";
$final = "R:\\ACCTS\\$acctnum\\MunicodeDesktop\\HTML";
# $upload = "R:\\ACCTS\\$acctnum\\$outfile";
$contegra = "L:\\download\\Municode_Desktop";

#Query stuff...
$ProductID = "_empty_";
$ProductName = "_empty_";
$Client = "_empty_"; 
$State = "_empty_"; 
# $Abbreviation = "_empty_";



#Error checking...
$errindex = "Sorry holmes, but there is no index for this job...";


###############################################
#####	START Municode Desktop	###############
###############################################

&Clear;

# print STDOUT "		Municode Desktop!!!! Yeehaw!!! $ARGV[0]\n";

if (-d $newdir) {
	
	print STDOUT "\n			MUNICODE DESKTOP (BETA)\n\n\n\n";
	print STDOUT "	Clearing old files... ($newdir)\n";
		
	my $myreturn;	
	$myreturn = rmtree($newdir);
	
	
	dircopy($dirtobecopied,$newdir) or die $!;
} else {
	dircopy($dirtobecopied,$newdir) or die $!;
}


print STDOUT "\n\n\n	Building Municode Desktop for Account #$acctnum!	(could take a few minutes...)\n";


if (-d "$html") {

	rmtree($mcdt);
	dircopy($html,$mcdt) or die $!;
} else {
	print STDOUT "Oops... looks like there isn't any content in L:\\HTML\\$acctnum for me to copy.";
}

print STDOUT "	...DONE!\n\n";
print STDOUT "	...Querying the database";


###############################################
#####	Query the database	###################
###############################################

my $dsn="dbi:ODBC:BIS";	#system DSN
my $user="sa";
my $pw="concasturbate";
my $query="";


# PERL DBI CONNECT
$connect = DBI->connect($dsn, $user, $pw);

	$connect->{ReadOnly} = 1;
	$connect->{RaiseError} = 1;


# PREPARE THE QUERY
$query = 
		"SELECT P.ProductID, N.ProductName, C.Client, Address, City, Zip, PhoneAreaCode, Phone, URL, S.StateID, S.State, S.Abbreviation, T.bannertext\n" .
		"FROM BIS_PRODUCTS AS P, BIS_PRODUCTNAMES AS N, BIS_CLIENTS AS C, _STATES AS S, vw_ProductBannerText AS T\n" .
		"WHERE P.ProductID = '" . $acctnum . "' AND\n" . 
		"P.ProductNameID = N.ProductNameID AND\n" .
		"P.ClientID = C.ClientID AND C.StateID = S.StateID AND P.ProductID = T.productid";

# print STDOUT "\$query\n$query\n\n";
		
$myqueryresults = $connect->prepare($query)
	or die "$DBI::errstr\n\nCan't prepare query: $query\n$!";

# EXECUTE THE QUERY
$myqueryresults->execute()
	or die "$DBI::errstr\n\nCan't execute query: $query\n$!";
	


# Binding the query results to scalars to be passed back into file contents later...
$myqueryresults->bind_columns(undef, \$ProductID, \$ProductName, \$Client, \$Address, \$City, \$Zip, \$PhoneAreaCode, \$Phone, \$URL, \$StateID, \$State, \$ClientST, \$BannerInfo)
	or die $!;

my $queryreturn = $myqueryresults->fetch();

print STDOUT "	...DONE!\n\n";
print STDOUT "	...Passing data to setup files/landing page";

###############################################
#####	Passing database info back into files #
###############################################

# Inserting 'break rule' in place of newlines for HTML
$BannerInfo =~ s|\n|\n<br/>|g;


open (FILE, "$newdir\\HTML\\home.html");
@home = <FILE>;
close (FILE);



	
#Insert Hyphen into phone number...
$Phone =~ s|(\d\d\d)(\d\d\d\d)|$1\-$2|g;

# Search & replace in home.html with new info from database...
foreach $line (@home) {
$line =~ s|<div class="doctitle">\n|<div class="doctitle">\n$BannerInfo\n|;

$line =~ s|<span id="ctl00_lbAddressClient"></span>|<span id="ctl00_lbAddressClient">$Client, $ClientST</span>|g;
$line =~ s|<span id="ctl00_lbAddress"></span>|<span id="ctl00_lbAddress">$Address</span>|g;
$line =~ s|<span id="ctl00_lbCity"></span>|<span id="ctl00_lbCity">$City, $ClientST $Zip</span>|g;
$line =~ s|<span id="ctl00_lbPhone"></span>|<span id="ctl00_lbPhone">($PhoneAreaCode) $Phone</span>|g;
$line =~ s|<a id="ctl00_hlWebSite" href=""></a>|<a id="ctl00_hlWebSite" href="http://$URL" target="_blank">$URL</a>|g;
}


open (HOME, ">$newdir\\HTML\\home.html");
print HOME "@home";
close (HOME);

print STDOUT "	...DONE!\n\n";
print STDOUT "	...Editing files";
print STDOUT "	...DONE!\n\n...Zipping files";


if ($Newindex) {
	rmtree($Newindex);
	dircopy($Webindex,$Newindex) or die $errindex;

} else {
	dircopy($Webindex,$Newindex) or die $errindex;
}



###############################################
#####	Read and Edit "municode.exe.config"	###
###############################################

open (STARTUP, "$Startup\\municode.exe.config");
@startup = <STARTUP>;
close (STARTUP);



	
foreach $line (@startup) {

$line =~ s|\<add key\=\"home\" value\=\"index.aspx\?clientID\=\&amp\;stateid\=\&amp\;statename\=\"\/>|<add key\=\"home\" value\=\"index.aspx\?clientID\=$acctnum\&amp\;stateid\=$StateID\&amp\;statename\=$State\"\/>|g;

}


open (CONFIG, ">$Startup\\municode.exe.config");
print CONFIG "@startup";	
close (CONFIG);






###############################################
#####	Read and Edit "setup.ini"	###########
###############################################

open (SETUP, "$Startup\\setup.ini");
@setup = <SETUP>;
close (SETUP);


	
foreach $line (@setup) {

$line =~ s|dirGroup = |dirGroup = $Client\, $ClientST|g;

}

open (SETUPDONE, ">$Startup\\setup.ini");
print SETUPDONE "@setup";	
close (SETUPDONE);





###############################################
#####	Copying and pasting #####.xml	#######
###############################################
use File::Copy;

move($xml,$final);




###############################################
#####	Zipping up files	###################
###############################################


my $zipname = "$outfile";

my $zip = Archive::Zip->new();
$zip->addTree( "$filestobeziped" );
$zip->writeToFileNamed("$zipname");



print STDOUT ".";
print STDOUT " .";
print STDOUT "  .";
print STDOUT "   .";
print STDOUT "    .";
print STDOUT "     .";
print STDOUT "DONE!";




###############################################
#####	Uploading...	###########################
###############################################


print STDOUT "	...Uploading $outfile to Contegra server...\n\n";

copy("$outfile",$contegra);

&Clear;

print STDOUT "\n\n\n		Congrats!  Your Municode Desktop has finished building. \n\n'$outfile' has been uploaded to $contegra.\n\n\n";

exit;