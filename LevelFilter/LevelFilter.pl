#!/usr/bin/perl
# 
# filter.pl [1.0]
# Programmer:  David Nichols
my $myversion = "10-27-2010.1 [STABLE]";

############################## PROCEDURE #
##########################################
#
#	Reads in XML config file and removes se-
#	lected levels from valid XML files in
#	R:\ACCTS\#####\XML.  Config file gives
#	options for adding levels to filter-out 
#	and also specific files for the program to 
# ignore (note: could be extended for
#	new tasks).  Once the lookup and levelsto-
# filter arrays are set up, program will 
# perform a series of substitutions on all
# non-ignored files in directory to change
# selected levels into para-like elements
# so they will not create hyperlinks in
#	web's TOC.
#
##########################################


################################# PRAGMA #
##########################################

use strict;
use File::Path;
use utf8;
#use open ":encoding(utf8)";
#use Unicode::UTF8 qw[decode_utf8 encode_utf8];
use warnings;
no warnings "uninitialized";

##########################################


######################### LOCALS/GLOBALS #
##########################################

my $acctnum = $ARGV[0];
my $user = getlogin();
my $filter = "R:\\ACCTS\\$acctnum\\gc\\config\\_LevelFilter.xml";
my $wd = "R:\\ACCTS\\$acctnum\\XML";
my $deltemp = "R:\\ACCTS\\$acctnum\\XML\\temp.xml";
my $deltemp2 = "R:\\ACCTS\\$acctnum\\XML\\temp2.xml";
my @addrlist;
my @filterfinal;
my @final;
my @lookup;
my @levelfilter;
my @levelstofilter;
my $rtn;
my @test;
my $level2;
my $level3;
my $level4;
my $level5;
my $temp = "R:\\ACCTS\\$acctnum\\XML\\temp.xml";
my $temp2 = "R:\\ACCTS\\$acctnum\\XML\\temp2.xml";
my $BackupXML = ($wd . "\\BackupXML");
my $ValidIDs = ($wd . "\\_ValidIDs\.xml");
my $ValidIDsLog = ($wd . "\\_ValidIDs_LOG\.txt");
my $ValidIDsNameIDs = ($wd . "\\_ValidIDs_Name_IDs\.xml");
my $Book = ($wd . "\\Book\.xml");
my $BookALL = ($wd . "\\Book_ALL\.xml");
my $BookStruct = ($wd . "\\Book_Struct\.xml");
my $BookStructIMG = ($wd . "\\Book_StructIMG\.xml");

##########################################


################################### MAIN #
##########################################

print STDOUT "		*****************************************		\n";
print STDOUT "			LEVEL FILTER(.pl) VERSION $myversion			\n";
print STDOUT "		*****************************************		\n\n\n";


#print STDOUT "Press any key to begin...";
#$x = &ReadKey;
#&Clear;

#######################################################
#	Part 1, Creating the "Do not touch" list for files	#
#######################################################


#### HOUSEKEEPING #####


if (-e "$BackupXML") {rmtree($BackupXML) or die "$!";}
if (-e "$ValidIDs") {unlink($ValidIDs);}
if (-e "$ValidIDsLog") {unlink($ValidIDsLog);}
if (-e "$ValidIDsNameIDs") {unlink($ValidIDsNameIDs);}
if (-e "$Book") {unlink($Book);}
if (-e "$BookALL") {unlink($BookALL);}
if (-e "$BookStruct") {unlink($BookStruct);}
if (-e "$BookStructIMG") {unlink($BookStructIMG);}


####	Reading DIR for potential files to open		####

opendir (DIR, $wd) or die "Can't open $wd... sorry.";
	
	while(($_ = readdir(DIR))) {
		push(@addrlist, $_);
		print STDOUT "Building lookup...	push(\@addrlist, $_);\n";
	}
	print STDOUT "\n\n";
close (DIR);

shift @addrlist;
shift @addrlist;

print STDOUT "DONE!\n\n";


####		If "LevelFilter.xml" exists in R:\accts\#####\gc\config...		####

if (-e "$filter") {

	open (FILTER, "$filter") or die "Can't open $filter... sad day.\n\n";
	my @filterlist = <FILTER>;
	close FILTER;


####	Grabbing pertinent info from LevelFilter.xml (type of filter, name of item to filter/ignore)	####
		
	foreach $_ (@filterlist) {

		$_ =~ m/(\s+\<skip type= \")(.+)(\"\>)(.+)(\<\/skip\>)/gi;
			
			my $type = $2;
			my $filter = ($4 . "\.xml");

####	If the type is a "file," push that name onto filter array		####

			if ($type eq "file") {
				push (@filterfinal, $filter);
			}
	}
}


####	Gathering potential files to filter and sending them one-by-one to "compare" subroutine		####

foreach $_ (@addrlist) {
	
	my $insert = compare($_, @filterfinal);  #passing parms to "compare" subroutine
	
	push(@final, "$insert");
	
}

####	Each potential file comes back with "AAAAAAAA_" prepended to it if it matches an item from the LevelFilter list to ignore		####

my @sortedfinal = sort(@final);

foreach $_ (@sortedfinal) {
	
	$_ =~ s|(AAAAAAAA_)(.+)(.xml)|nul|gi;
}



###################################################
#	Part 2, Swapping out level#/Title/Subtitles in 	#
#		selected files for para-like elements so that #
#		they won't become hyperlinks on the web TOC.	#
###################################################

@lookup = @sortedfinal;

open (LEVELS, "$filter") or die "Can't open $filter\n\n";
my @levellist = <LEVELS>;
close LEVELS;


####	Now that "ignore" files have been removed from lookup array...		####
	
foreach $_ (@levellist) {

	$_ =~ m/(\s+\<skip type= \")(.+)(\"\>)(.+)(\<\/skip\>)/gi;
		
####	If the type is a "level," push that name onto levelfilter array		####

		if ($2 eq "level") {
			push (@levelfilter, "$4");
		}
}

print STDOUT "\n\n";

@levelstofilter = sort(@levelfilter);  # Keeping things tidy

foreach $_ (@lookup) {
	my $goget = ($wd . "\\" . $_);
	
	if (-e "$goget") {
		
		print STDOUT "\"goget\" is: $goget\n";
		
		my $rtn = magic($goget, @levelstofilter);
		print STDOUT "Filtering selected levels from:	$rtn\n";	
		
		# print STDOUT "Here is goget: $goget and here is levelstofilter: $levelstofilter"

	} else {next;}
	
	
}

print STDOUT "\n\n		Level filter finshed!	(account #$acctnum)\n\n";
print STDOUT "		Thank you very much, $user.\n";
print STDOUT "		You are soooooo good lookin'!\n\n";

exit[1];




############################ SUBROUTINES #
##########################################

# SUBROUTINE NUMBER ONE - COMPARE

sub compare {
	
	my $parm1 = shift;
	
	my $test1 = shift;
		push(@test, $test1);
	my $test2 = shift;
		push(@test, $test2);
	my $test3 = shift;
		push(@test, $test3);
	my $test4 = shift;
		push(@test, $test4);
	my $test5 = shift;
		push(@test, $test5);
	my $test6 = shift;
		push(@test, $test6);
	my $test7 = shift;
		push(@test, $test7);
	my $test8 = shift;
		push(@test, $test8);
	my $test9 = shift;
		push(@test, $test9);
	my $test10 = shift;
		push(@test, $test10);
	my $test11 = shift;
		push(@test, $test11);
	my $test12 = shift;
		push(@test, $test12);
	my $test13 = shift;
		push(@test, $test13);
	my $test14 = shift;
		push(@test, $test14);
	my $test15 = shift;
		push(@test, $test15);
	my $test16 = shift;
		push(@test, $test16);
	my $test17 = shift;
		push(@test, $test17);
	my $test18 = shift;
		push(@test, $test18);
	my $test19 = shift;
		push(@test, $test19);
	my $test20 = shift;
		push(@test, $test20);
	my $test21 = shift;
		push(@test, $test21);
	my $test22 = shift;
		push(@test, $test22);
	my $test23 = shift;
		push(@test, $test23);
	my $test24 = shift;
		push(@test, $test24);
	my $test25 = shift;
		push(@test, $test25);
	
	
	foreach $_ (@test) {
		
####		If list item equals something on the "do not touch" list (now in @levelstofilter), then prepend it with "AAAAAAAA_"		####

		if ($_ eq $parm1) {
			
			print STDOUT "Ignoring...	 [FILE] $parm1\n";
			
			$parm1 = ("AAAAAAAA_" . $parm1);
			last;
		}
	}
	

return $parm1;
	
}
	


# SUBROUTINE NUMBER TWO - "Magic" aka, does all the fancy substition without causing the XML to become invalid	#

sub magic {
		
	my $addr = shift;
	my $in_a = shift;
	my $in_b = shift;
	my $in_c = shift;
	my $in_d = shift;
		
	if ($in_a eq "level2") {
		$level2 = $in_a;
#		print STDOUT "level to filter: $level2\n";
	} elsif ($in_a eq "level3") {
		$level3 = $in_a;
#		print STDOUT "level to filter: $level3\n";
	} elsif ($in_a eq "level4") {
		$level4 = $in_a;
#		print STDOUT "level to filter: $level4\n";
	} elsif ($in_a eq "level5") {
		$level5 = $in_a;
#		print STDOUT "level to filter: $level5\n";
	} else {$level2 = undef;}
	
	
	if ($in_b eq "level2") {
		$level2 = $in_b;
	#	print STDOUT "level to filter: $level2\n";
	} elsif ($in_b eq "level3") {
		$level3 = $in_b;
#		print STDOUT "level to filter: $level3\n";
	} elsif ($in_b eq "level4") {
		$level4 = $in_b;
#		print STDOUT "level to filter: $level4\n";
	} elsif ($in_b eq "level5") {
		$level5 = $in_b;
#		print STDOUT "level to filter: $level5\n";
	} else {$level3 = undef;}


	if ($in_c eq "level2") {
		$level2 = $in_c;
#		print STDOUT "level to filter: $level2\n";
	} elsif ($in_c eq "level3") {
		$level3 = $in_c;
#		print STDOUT "level to filter: $level3\n";
	} elsif ($in_c eq "level4") {
		$level4 = $in_c;
#		print STDOUT "level to filter: $level4\n";
	} elsif ($in_c eq "level5") {
		$level5 = $in_c;
#		print STDOUT "level to filter: $level5\n";
	} else {$level4 = undef;}

					
	if ($in_d eq "level2") {
		$level2 = $in_d;
#		print STDOUT "level to filter: $level2\n";
	} elsif ($in_d eq "level3") {
		$level3 = $in_d;
#		print STDOUT "level to filter: $level3\n";
	} elsif ($in_d eq "level4") {
		$level4 = $in_d;
#		print STDOUT "level to filter: $level4\n";
	} elsif ($in_d eq "level5") {
		$level5 = $in_d;
#		print STDOUT "level to filter: $level5\n";
	} else {$level5 = undef;}
	

# PASS NUMBER ONE (of two)...

	open FILE, "<:encoding(utf8)", $addr or die "Cannot open $addr!!!\n";
	open OUT, ">:encoding(utf8)", $temp or die "Cannot open $temp for file $addr\n\n";
 		
 		while(<FILE>) {
 			my($line) = $_;
 			
 			
			$line =~ s|(\<title\>)(\</title\>)|$1 $2|gi;
			$line =~ s|(\<subtitle\>)(\</subtitle\>)|$1 $2|gi;		
			$line =~ s|(\<title\>)(.+)(\</title\>)(\n)|$1$2$3|gi;
 		
			
			if ($level2 eq "level2") {
						
				$line =~ s|(\<level2.*\>)(\n)|\<MONKEY\><para  gclevel="0" justify='center' block_type='block'>|gi; 
#				$line =~ s|(\<MONKEY\>)(\<para  gclevel="0" justify='center' block_type='block'\>)(\<title\>)(.+)(\</title\>)|$1$2$3$4$5|gi;
				$line =~ s|(</)(level2)(>\n)||gi;
			
			}	
				
			if ($level3 eq "level3") {
		
				$line =~ s|(\<level3.*\>)(\n)|\<MONKEY\><para  gclevel="0" justify='center' block_type='block'>|gi; 
#				$line =~ s|(\<MONKEY\>)(\<para  gclevel="0" justify='center' block_type='block'\>)(\<title\>)(.+)(\</title\>)|$1$2$3$4$5>|gi;
				$line =~ s|(</)(level3)(>\n)||gi;
				
			}
			
			if ($level4 eq "$level4") {
					
				$line =~ s|(\<level4.*\>)(\n)|\<MONKEY\><para  gclevel="0" justify='center' block_type='block'>|gi; 
#				$line =~ s|(\<MONKEY\>)(\<para  gclevel="0" justify='center' block_type='block'\>)(\<title\>)(.+)(\</title\>)|$1$2$3$4$5|gi;
				$line =~ s|(</)(level4)(>\n)||gi;
			
			} 
			
			if ($level5 eq "$level5") {
				
				$line =~ s|(\<level5.*\>)(\n)|\<MONKEY\><para  gclevel="0" justify='center' block_type='block'>|gi; 
#				$line =~ s|(\<MONKEY\>)(\<para  gclevel="0" justify='center' block_type='block'\>)(\<title\>)(.+)(\</title\>)|$1$2$3$4$5|gi;
				$line =~ s|(</)(level5)(>\n)||gi;
					
			}

			print OUT "$line";
		}
	
	
	close OUT;
	close FILE;
		


# PASS NUMBER TWO....
	
#	$temp = "R:\\ACCTS\\$acctnum\\XML\\temp.xml";
	
	open FILE2, "<:encoding(UTF8)", $temp or die "cannot open $temp!!!\n";
	open OUT2, ">:encoding(utf8)", $temp2 or die "Cannot write to $temp2!!!\n\n\n";
		
	while (<FILE2>) {
		my($line) = $_;
		
			$line =~ s|(\<MONKEY\>)(\<para  gclevel="0" justify='center' block_type='block'\>)(\<title\>)(.+)(\</title\>)(\<subtitle\>)(.+)(\</subtitle\>)|$2\n\<bold\>$4 $7\</bold\>\</para\>|gi;
	
			$line =~ s|(\</title\>)(\<subtitle\>)|$1\n$2|gi; # This will replace non-messed-with titles/subtitles
		
		print OUT2 "$line";
	}		
	
	close OUT2;
	close FILE2;
	
	
	# PASS NUMBER THREE - HOUSEKEEPING #
	
	open FILE3, "<:encoding(UTF8)", $temp2 or die "cannot open $temp2!!!\n";
	open OUT3, ">:encoding(utf8)", $addr or die "Cannot write to $addr!!!\n(make sure you don't have it open...)\n\n";
		
	while (<FILE3>) {
		my($line) = $_;
	
			$line =~ s|(\<bold\>)(.+)(\<bold\>)(.+)(\</bold\>)|$1$2$4|gi;  #	Accounts for scenario in which editor adds their own in-text font markup at this level...
		
		print OUT3 "$line";
		
	}
	
	close OUT3;
	close FILE3;
	
	unlink($deltemp);	# Deleting temp.xml foreach file to filter...
	unlink($deltemp2);	# Deleting temp.xml foreach file to filter...
	
	
# (Do we need this?) Work in progress... this routine will close up any "gaps" in the levels post-filtration #

#	foreach $_ (@sortedfinal) {
#	
#	my $goget2 = ($wd . "\\" . $_);
#
#	if (-e "$goget2") {
#	
#	my $rtn2 = degappify($goget2);
#
#	}
#}
	
	
	
	return $addr;
		
}






# SUBROUTINE NUMBER THREE - DEGAPPIFY or "REMOVE POTENTIAL GAPS" in XMl levels #
# Work in Progress..... #



#sub degappify {
#	
#	my $parm1 = shift;
#	
#	open (FILE3, '<:encoding(UTF8)', $parm1) or die "Cannot open $parm1!!!\n";
#	open (OUT3, '>:UTF8', "$temp") or die "Cannot open $temp for file $parm1\n\n";
# 		
# 		while(<FILE>) {
# 			my($line) = $_;
# 			
# 			$line =~ m|(\<level)(\d)(.+)(\>)|gi;
# 				my $levelnumber = $2;
# 				
# 				print STDOUT "[FILE] $line		[LEVEL NUMBER] $levelnumber\n";
# 		
# 		}
#		
#	
#	close FILE3;
#	close OUT3;
#	
#	return $parm1;
#
#}