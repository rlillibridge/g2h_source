#!/usr/bin/perl
#!/usr/bin/perl
# GetBannerInfo_xp.pl
# 
#Using EIJobID, gets Job#, & Banner Text
#
# Programmer:  Raymond Lillibridge
my $myVersion = "Version:  2013-09-19.1 (WIP BRANCH)";

#==========================================================
# CHANGE HISTORY:
#==========================================================
# 2013-08-29.1 - Raymond Lillibridge - TESTING USE OF %BRANCH% via G2h_BRANCH.bat
# 2012-11-12.1 - Raymond Lillibridge - FetchBIS* - mkdir if it doesn't exist ($workpath\\$AcctNO) & list deletion of text files
# 2012-01-19.1 - David Nichols - Changed ODBC settings for BIS server changeover
# 2011-09-23.1 - Raymond Lillibridge - Added '&' to two ReadKey() calls.  OOPS!
# 2011-08-26.1 - Raymond Lillibridge - Added passing in %WORKPATH% for batch (see:  #331) replaces $opt_drive
# 2011-04-26.1 - Raymond Lillibridge - Replace spaces with '_' in JobName.txt content
# 2011-04-22.1 - Raymond Lillibridge - Removing display of BIS JobID from BANNER output file
# 2011-04-21.1 - Raymond Lillibridge - Added parm to receive 'AcctNO.tmp' file name.
# 2011-04-20.1 - Raymond Lillibridge - Added writing out:
#		EIJobID.txt with content equal EIJobID
#		JobName.txt with content equal JobName
#		AcctNO.txt file with content equal AccountNO
# 2011-04-20.2 - Raymond Lillibridge - Added "--drive=" as additional parameter
#		Output *.txt files will be written to "--drive" . \acct\<AcctNO>
# 2011-04-20.3 - Raymond Lillibridge - Delete previous *.txt files.
#==========================================================


#==========================================================
# PRAGMA
#==========================================================
use warnings;
no warnings 'uninitialized';
use DBI;
use Term::InKey;
use Data::Dumper;
use Tie::File;

use Getopt::Long;
my $opt_eijob="-empty-";	# --eijob		EIjobID
my $opt_job="-empty-";	# --job		Job or account_number
#my $opt_drive="-empty-";	# --drive		From calling batch file, usually, %DRIVE% value
my $opt_workpath="";		# --workpath	From calling batch file, %workpath% value
my $opt_acctnofn="";		# --acctnofn	'AcctNO.tmp' file name (full path for DOS access)
my $opt_nopause="";

GetOptions(
	"eijob=s" 	=>	\$opt_eijob,
	"job=s" 		=>	\$opt_job,
#	"drive=s"		=>  \$opt_drive,
	"workpath=s"  =>  \$opt_workpath,
	"acctnofn=s"	=>  \$opt_acctnofn,
	"nopause=s"	=>  \$opt_nopause
	);

my $eijob="$opt_eijob";
my $job="$opt_job";
#my $drive="$opt_drive";
my $workpath=$opt_workpath;
my $acctnofn="$opt_acctnofn";
my $NoPause="$opt_nopause";

# SQL INFO
my $dsn=q/dbi:ODBC:BIS/;	#system DSN
my $user=q/BIS_dbreader/;	# changed 1/19/2012 with BIS server changover
my $pwd=q/ReadOnly1/;		# changed 1/19/2012 with BIS server changover
my $dbh;	#database handle
my $sth;	#statement handle
my $query="";

my $EIJobID="-empty-";	#result set EIJobID			TEST WITH:  156968
my $JobID="-empty-";		#result set ProductID		TEST WITH:  10135
my $JobName="";			#result set JobName
my $AcctNO="";			# result set AcctNO
my $BannerText="";		# result set BannerText

my $productid = "<productid>XXX<\/productid>";
my $banner = "<banner>XXX<\/banner>";
my $expandlevel = "<expandlevel>XXX<\/expandlevel>";


#==========================================================
# GLOBALS
#==========================================================
my $dashes = "----------------------------------------------------------------------";

# Date and Time vars
my($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime;
$year += 1900;
my $now_string = localtime;
my $nowYYYYMMDD_HHMMSS= dateTime();
my $LogDateTime =	$nowYYYYMMDD_HHMMSS->{'dateNr'} .  "_" . 
					$nowYYYYMMDD_HHMMSS->{'HHMMSS'};
my $myDT=$LogDateTime;
$myDT =~ s|(\d\d\d\d)(\d\d)(\d\d)_(\d\d)(\d\d)(\d\d)|$1\/$2\/$3 $4:$5:$6|g;


#==========================================================
# MAIN MAIN MAIN MAIN MAIN MAIN MAIN 
#==========================================================
	if($NoPause ne "-nopause")	{
#		&Clear;	
	}

	&Housekeeping();	
	
	if($opt_eijob ne "-empty-") {&FetchBIS_eijob();}
	if($opt_job ne "-empty-") {&FetchBIS_job();}

	## Process the file or quit?
	print STDOUT "\n     continue?...\n$dashes\nY = YES                 --OR--     (blank\) or (!Y) = CANCEL\n\n";
	
	if($NoPause ne "-nopause")	{		
		$x = &ReadKey();
	} else {
		$x = "Y";
	}
		
	
	if($x=~ /[Yy]/) {
		# do nothing...
		print STDOUT "\n";	#add a little space before processing the files
	} else {
		print STDOUT "\n\...$0 Processing cancelled.\n";
		exit 0 ;
	}

	print STDOUT "$0 FINISHED!\n\n";

1;
#==========================================================
#	MAIN END
#==========================================================


#==========================================================
# SUBROUTINES
#==========================================================
#-------------------------------------------------------------
sub FetchBIS_eijob()	{
#-------------------------------------------------------------
	my $me = whoami();
	my $sqlErr;
	my $myrtn;
	my $myMkRtn;
	my $sText = "";
	
	$dbh->{ReadOnly} = 1;
	$dbh->{RaiseError} = 1;

	#------------------------
	# Fetch ProductID, DefaultExpandLevel, ProductName, Client, State, & State-abbreviation...
	#------------------------				
	$query=
	"SELECT  E.EIJobID, T.jobId, T.jobName, J.ProductID AS AcctNO, J.bannerText\n" . 
	"FROM     tempSpeedJobNames T, BIS_EIJOBS E, BIS_JOBS J\n" . 
	"WHERE  T.jobId = E.ParentJobID AND T.jobId = J.JobID AND (E.EIJobID = '" . $opt_eijob . "')";
			
	$sth = $dbh->prepare($query)
	 	or die "$DBI::errstr\n\nCan't prepare query: $query\n$!";
	 	
	$sqlErr = $sth->execute
	 	or die "$DBI::errstr\n\nCan't execute query:\n$query\n\n$!\n";
	
	#Bind the results to local variables...
	$sth->bind_columns(undef, \$EIJobID, \$JobID, \$JobName, \$AcctNO, \$BannerText)
		or die "$DBI::errstr\n\nCan't bind_columns\n$!\n";
	
	#retrieve values from the result set...
	$str1 =	$sth->fetch();
	
	if($str1)	{	
		print STDOUT "\n\n\nSQL Query Results:\n" .
					"$dashes\n" .
					"EIJobID=$EIJobID\n" . 
					"ActNO=$AcctNO\n" . 
#					"$JobID\n" . 
					"JobName=$JobName\n\n" . 
					"$BannerText\n" . 
					"$dashes\n\n\n";
	}
	
	print STDOUT "Banner Text:\n$dashes\n$BannerText\n$dashes\n\n";

	#Close the connection
	$sth->finish();
	
	if($str1)	{
		$productid =~ s|XXX|$AcctNO|g;
		#Cleanup SQL Data newline characters...
		$BannerText =~s/[^[:print:]]/_/g;
		$BannerText =~s/__/\n/g;
		$banner =~ s|XXX|$BannerText|g;
	} else {
		$EIJobID ="";	
		$JobID ="";
		$JobName ="";
		$AcctNO="";
		$BannerText="";
		$productid="";
	}

	$sText = "$workpath\\$AcctNO";
	if(-e $sText) {
		print STDOUT "\n\n==========\nDeleting Previous Text Workfiles...\n";
		DeletePreviousTextFiles($AcctNO);
		print STDOUT "==========\n\n";
	}
	else
	{
		print STDOUT "\nMaking Directory:  $sText... ";
		unless(mkdir($sText, 0755)) {
			die "Unable to create $sText\n";
		}
		print STDOUT "DONE.\n";
	}
	
	# Save Banner Info for creation of a Readme.txt file that will be added to zip files later.
#	open(MYBANNER, ">$drive\\Accts\\$AcctNO\\EIJobID_BANNER_INFORMATION.txt")  or
	open(MYBANNER, ">$workpath\\$AcctNO\\EIJobID_BANNER_INFORMATION.txt")  or
		die "Could not create $workpath\\$AcctNO\\EIJobID_BANNER_INFORMATION.txt\n$!";
	$bannerInfo = $banner;
	$bannerInfo =~ s|<banner>||g;
	$bannerInfo =~ s|<\/banner>||g;
	print MYBANNER "EIJobID=$EIJobID\n" . 
		"AcctNO=$AcctNO\n" . 
		#"JobID=$JobID\n" . 
		"JobName=$JobName\n" .
		"$productid\n\n" . 
		"$BannerText\n";
	close MYBANNER;
	
	# EIJobID.txt...
#	open(MYEIJOBID, ">$drive\\Accts\\$AcctNO\\EIJobID.txt") or
	open(MYEIJOBID, ">$workpath\\$AcctNO\\EIJobID.txt") or
		die "Could not create EIJobID.txt\n$!";
	print MYEIJOBID "$EIJobID";
	close MYEIJOBID;
	
	# JobName.txt...
#	open(MYJOBNAME, ">$drive\\Accts\\$AcctNO\\JobName.txt") or
	open(MYJOBNAME, ">$workpath\\$AcctNO\\JobName.txt") or
		die "Could not create JobName.txt\n$!";
		$JobName =~ s| |_|g;
	print MYJOBNAME "$JobName";
	close MYJOBNAME;
	
	# AcctNO.tmp...
	open(MYACCTNO, ">$acctnofn") or
		die "Could not create $acctnofn\n$!";
	print MYACCTNO "$AcctNO";
	close MYACCTNO;
}


#-------------------------------------------------------------
sub FetchBIS_job()	{
#-------------------------------------------------------------
	my $me = whoami();
	my $sqlErr;
	my $myrtn;
	my $sText = "";
		
	$dbh->{ReadOnly} = 1;
	$dbh->{RaiseError} = 1;

	#------------------------
	# Fetch ProductID, DefaultExpandLevel, ProductName, Client, State, & State-abbreviation...
	#------------------------				
	$query=
		"SELECT P.ProductID AS AcctNO, P.DefaultExpandLevel, N.ProductName, C.Client, S.State, S.Abbreviation\n" .
		"FROM BIS_PRODUCTS AS P, BIS_PRODUCTNAMES AS N, BIS_CLIENTS AS C, _STATES AS S\n" .
		"WHERE P.ProductID = '" . $opt_job . "' AND\n" . 
		"P.ProductNameID = N.ProductNameID AND\n" .
		"P.ClientID = C.ClientID AND C.StateID = S.StateID";

	#print STDOUT "\n[debug] Fetch query:\n$query\n\n";
		
	$sth = $dbh->prepare($query)
	 	or die "$DBI::errstr\n\nCan't prepare query: $query\n$!";
	 	
	$sqlErr = $sth->execute
	 	or die "$DBI::errstr\n\nCan't execute query:\n$query\n\n$!\n";
	
	#Bind the results to local variables...
	$sth->bind_columns(undef, \$AcctNO, \$DefaultExpandLevel, \$ProductName, \$Client, \$State, \$ClientST)
		or die "$DBI::errstr\n\nCan't bind_columns\n$!\n";
	
	#retrieve values from the result set...
	$str1 =	$sth->fetch();
	
	if($str1)	{	
		print STDOUT "\n\n\nSQL Query Results:\n" .
					"$dashes\n" .
					"AcctNO=$AcctNO\n" . 
					"$ProductName\n$Client, $State, $ClientST\n" . 
					"\(DefaultExpandLevel=$DefaultExpandLevel\)\n" . 
					"$dashes\n\n\n";
	
	
		#------------------------
		# Fetch BannerText...
		#------------------------				
		$query=
			"SELECT P.ProductID, T.bannertext\n" .
			"FROM BIS_PRODUCTS AS P, vw_ProductBannerText AS T\n" .
			"WHERE P.ProductID = '" . $opt_job . "' AND\n" . 
			"P.ProductID = T.productid";
			
			#print STDOUT "\n[debug] \$sQuery=$query\n\n";
	
		$sth = $dbh->prepare($query)
		 	or die "$DBI::errstr\n\nCan't prepare query: $query\n$!";
		 	
		$sqlErr = $sth->execute
		 	or die "$DBI::errstr\n\nCan't execute query:\n$query\n\n$!\n";
		
		#Bind the results to local variables...
		# 20100811 - BIS_PRODUCTS.ProductID is NUMERIC!!!  Therefore, no leading zeros!  i.e.  job=02116
		$sth->bind_columns(undef, \$ProductID, \$BannerText)
			or die "$DBI::errstr\n\nCan't bind_columns\n$!\n";
					
		#retrieve values from the result set...
		$str1 =	$sth->fetch();

		if($str1 eq "")	{
			print STDOUT "\n!!! fetch() FAILED!\n\nNO BANNER TEXT FOUND!\n\n\$str1=$str1\n\n";
		}
	}
	
	print STDOUT "Banner Text:\n$dashes\n$BannerText\n$dashes\n\n";

	#Close the connection
	$sth->finish();
	
	if($str1)	{
		$productid =~ s|XXX|$ProductID|g;	
		$productid =~ s|XXX|$job|g;			#20100811 $job needs to be character, not numeric, to allow for leading zeros
		
		#Cleanup SQL Data newline characters...
		$BannerText =~s/[^[:print:]]/_/g;
		$BannerText =~s/__/\n/g;
		
		$banner =~ s|XXX|$BannerText|g;
		$expandlevel =~ s|XXX|$DefaultExpandLevel|g;
	} else {
		$productid ="";	
		$banner ="";
		$expandlevel ="";
		$job="";
	}

	$AcctNO = $opt_job;
	#DeletePreviousTextFiles($AcctNO);
	
	$sText = "$workpath\\$AcctNO";
	if(-e $sText) {
		print STDOUT "\n\n==========\nDeleting Previous Text Workfiles...\n";
		DeletePreviousTextFiles($AcctNO);
		print STDOUT "==========\n\n";
	}
	else
	{
		print STDOUT "\nMaking Directory:  $sText... ";
		unless(mkdir($sText, 0755)) {
			die "Unable to create $sText\n";
		}
		print STDOUT "DONE.\n";
	}
			
#	print STDOUT "\n$dashes\nAcctNO=$job\n\n$banner\n$expandlevel\n$dashes\n\n";
	
	# Save Banner Info for creation of a Readme.txt file that will be added to zip files later.
#	open(MYBANNER, ">$drive\\Accts\\$AcctNO\\BANNER_INFORMATION.txt")  or
#		die "Could not create $drive\\Accts\\$AcctNO\\ BANNER_INFORMATION.txt\n$!";
	open(MYBANNER, ">$workpath\\$AcctNO\\BANNER_INFORMATION.txt")  or
		die "Could not create $workpath\\$AcctNO\\ BANNER_INFORMATION.txt\n$!";
	$bannerInfo = $banner;
	$bannerInfo =~ s|<banner>||g;
	$bannerInfo =~ s|<\/banner>||g;
	print MYBANNER "AcctNO=$job\n\n$bannerInfo\n";
	close MYBANNER;
}


#-------------------------------------------------------------
sub dateTime {
#-------------------------------------------------------------
	my $me = whoami();
	my ($sec,$min,$hour,$mday,$monNr,$year);
	my $now = {};
	#get date and time
	($sec,$min,$hour,$mday,$monNr,$year) = localtime(time());
    	my ($date, $date_nr, $clock);
	my $month=("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec")[$monNr];
    	$monNr++;
	$monNr = sprintf("%02d",$monNr);
	$mdayNr = sprintf("%02d",$mday);
	$sec = sprintf("%02d",$sec);
	$min = sprintf("%02d",$min);
	$hour = sprintf("%02d",$hour);
	$year= 1900 + $year;
	$now->{'date'} = "$year-$month-$mday";
	$now->{'dateNr'} = "$year$monNr$mdayNr";
	$now->{'time'} = "$hour:$min:$sec";
	$now->{'HHMMSS'} = "$hour$min$sec";
	return($now);		
}


#-------------------------------------------------------------
sub DeletePreviousTextFiles()	{
#-------------------------------------------------------------
	my $me=whoami();
	my $myacct=shift;
	my @aFiles=(
		"EIJobID_BANNER_INFORMATION.txt",
		"BANNER_INFORMATION.txt",
		"JobName.txt",
		"AcctNO.txt",
		"EIJobID.txt"
	);
	
	foreach (@aFiles)	{
#		$f = "$drive\\Accts\\$myacct\\$_";
		$f = "$workpath\\$myacct\\$_";
		if(-e $f)	{
			print STDOUT "\t$f...";
			unlink "$f";
			print STDOUT "DONE.\n";
		}
	}
}


#-------------------------------------------------------------
sub DisplayParms()	{
#-------------------------------------------------------------
	my $me=whoami();
	
	print STDOUT "\nRunning GetBannerInfo.exe with the following parameters:\n";
	
	print STDOUT "\$opt_eijob=$opt_eijob\n";
	print STDOUT "\$opt_job=$opt_job\n";
#	print STDOUT "\$opt_drive=$opt_drive\n";
	print STDOUT "\$opt_acctnofn=$opt_acctnofn\n";
	print STDOUT "\$opt_nopause=$opt_nopause\n";
	print STDOUT "\$NoPause=$NoPause\n";

	PauseRoutine();
}
	

#-------------------------------------------------------------
sub Housekeeping	{
#-------------------------------------------------------------
#	DisplayParms();
	
	print STDOUT "\nRUNNING:  $0\t\t$myVersion\n";

	if(($opt_eijob eq "-empty-")	and ($opt_job eq "-empty-") ) {
		print STDOUT "\n\n$dashes\n\nERROR!!!\n" .
			"Enter either EIJobID or Account Number: \n" . 
			"--eijob = $opt_eijob\n" .
			"--job = $opt_job\n" .
			"\n$dashes\n\n";	

		print STDOUT "\tExample:  C:\\\>GetBannerInfo --eijob=123456\n\n";
		
		exit (1);
	}

	if(($opt_eijob ne "-empty-")	and ($opt_job ne "-empty-") ) {
		print STDOUT "\n\n$dashes\n\nERROR!!!\n" .
			"Enter EITHER (but not both) EIJobID or Account Number: \n" . 
			"--eijob = $opt_eijob\n" .
			"--job = $opt_job\n" .
			"\n$dashes\n\n";	

		print STDOUT "\tExample:  C:\\\>GetBannerInfo --eijob=123456\n\n... OR ...\n\n";
		print STDOUT "\tExample:  C:\\\>GetBannerInfo --eijob=123456\n\n";
		
		exit (1);
	}
	
	if($opt_acctnofn eq "-empty-") {
		print STDOUT "\n\n$dashes\n\nERROR!!!\n" .
			"Enter a valid bath and file name for the 'AcctNO.tmp' file: \n" . 
			"--acctnofn = $opt_acctnofn\n" .
			"\n$dashes\n\n";	

		print STDOUT "\tExample:  C:\\\>GetBannerInfo --eijob=123456 --drive=V\: --acctnofn=" .
			"V\:\rlillibridge_20110421_10003405_AcctNO.tmp\n\n";
		
		exit (1);
	}	
	
	# Connect to the data source and get a handle for that connection.
	$dbh = DBI->connect($dsn, $user, $pwd)  
		or die "Can't connect to $dsn: $DBI::errstr\n$!";						
}


#-------------------------------------------------------------
sub PauseRoutine {
#-------------------------------------------------------------
	my $x="";
	
print STDOUT "\n\n...Press 'Y' or 'y' to continue...\(any other key to CANCEL\)\n\n";	
	
	if($NoPause ne "-nopause")	{		
		$x = &ReadKey();
	} else {
		$x = "Y";
	}
	
	if($x=~ /[Yy]/) {
		# do nothing...
		print STDOUT "\n";	#add a little space before processing the files
	} else {
		print STDOUT "\n\...$0 Processing cancelled.\n";
		exit 0 ;
	}	
}


#-------------------------------------------------------------
sub whoami {
#-------------------------------------------------------------
	(caller(1))[3];
}
