#!/usr/bin/perl
# slrt.pl
# GenCode to XML (schema:  code.xsd) Converter
#
# Programmer:  Raymond Lillibridge
my $myVersion = "Version:  2010-02-24.1 (beta)";

# UPDATES THIS VERSION:
# new application

# Xerces parse command:
my $xercesCMD = "C:\\xerces\\bin\\StdInParse -n -s";

#=============================================	
# PRAGMA
#=============================================	
use warnings;
no warnings 'uninitialized';
use Term::InKey;
use File::Basename;
use File::Copy;
use XML::Simple qw(:strict);
use Data::Dumper;
use Tie::File;

#=============================================	
# GLOBALS
#=============================================
my $XML_StatesInfo = "";
my $FN_olinkStatesInfo = "olinkStatesInfo.xml";

my $ext1 = "_TMP_01.txt";
my $ext2 = "_TMP_02.txt";
my $outfile = "";

my $ctr = 1;
my $seq=0;
my $tot = 0;
my $gtot = 0;

my $myStr1 = "";
my $myStr2 = "";
my $myInt1 = 0;
my $myInt2 = 0;
my %myHash1 = ();
my $myColumnCount;
my $index = 0;
my $go;

my $line = "";
my $lineTemp = "";
my $cnt;
my $parseCtr = 0;

my $str = "";
my $str2 = "";

%STATES =
                  (AL => 'Alabama', 		AK => 'Alaska', 			AZ => 'Arizona',
                   AR => 'Arkansas', 		CA => 'California', 			CO => 'Colorado',
                   CT => 'Connecticut', 	DE => 'Delaware', 			FL => 'Florida',
                   GA => 'Georgia', 		HI => 'Hawaii', 			ID => 'Idaho',
                   IL => 'Illinois', 		IN => 'Indiana', 			IA => 'Iowa',
                   KS => 'Kansas', 		KY => 'Kentucky',			LA => 'Louisiana',
                   ME => 'Maine', 		MD => 'Maryland',			MA => 'Massachusetts',
                   MI => 'Michigan', 		MN => 'Minnesota',			MS => 'Mississippi',
                   MO => 'Missouri', 		MT => 'Montana',			'NE' => 'Nebraska',
                   NJ => 'New Jersey', 	NH => 'New Hampshire',		NV => 'Nevada',
                   NM => 'New Mexico', 	NY => 'New York',			NC => 'North Carolina',
                   ND => 'North Dakota', 	OH => 'Ohio',				OK => 'Oklahoma',
                   OR => 'Oregon', 		PA => 'Pennsylvania',		RI => 'Rhode Island',
                   SC => 'South Carolina',	SD => 'South Dakota',		TN => 'Tennessee',
                   TX => 'Texas',		UT => 'Utah', 				VT => 'Vermont',
                   VA => 'Virginia',		WA => 'Washington',		WV => 'West Virginia',
                   WI => 'Wisconsin',		WY => 'Wyoming');

my $dashes="------------------------------------------------------------------------------";
my $equalDivider="============================================================";
# Cite holding array or hash...
my @aRegEx=();
my %HofA;
my %HofA_CA;


#=============================================	
#=============================================	
# MAIN
#=============================================
Housekeeping();
BuildRegExArray();
ProcessFiles();

print(STDOUT "\n$equalDivider\n");
print(STDOUT "\nGrand Total Lines Processed (All Files):  $gtot\n\n\n");
print(STDOUT "\nFINISHED:  olink.exe (pl)\n");
#=============================================	
# MAIN - END
#=============================================	
#=============================================	


##############################################
##############################################
#	SUBROUTINES
##############################################
#----------------------------------------------------------------------
sub BuildRegExArray {
#----------------------------------------------------------------------
	# Loads <cite/> elements and converts them to Perl RegEx strings...
	my $me = whoami();
	my $srch="";
	my $x=0;
	my $y=0;
	my $codeSec="";
	my $title="";
	my $cite="";

	print STDOUT "\$stateCode=$stateCode\n";
	print STDOUT "\nBUILDING Regex table:\n$dashes\n";
	
	if($XML_StatesInfo)	{
		$srch = $XML_StatesInfo->{StateLawCitations}->{$stateCode}->{search};
		print STDOUT "search=$srch\n";
	} else {
		print STDOUT "\t [$me]\t\$XML_StateInfo Doesn't exist!\n";
		exit(0);
	}
	
	
	if($stateCode eq "CA")	{	# CALIFORNIA...		
		while($XML_StatesInfo->{StateLawCitations}->{$stateCode}->{pub}->[$x])	{
			$codeSec=$XML_StatesInfo->{StateLawCitations}->{$stateCode}->{pub}->[$x]->{codesec};
			$title=$XML_StatesInfo->{StateLawCitations}->{$stateCode}->{pub}->[$x]->{title};
			print STDOUT "\n[$codeSec]\t$title\n";
	
			while($XML_StatesInfo->{StateLawCitations}->{$stateCode}->{pub}->[$x]->{cite}->[$y])	{
				$cite=$XML_StatesInfo->{StateLawCitations}->{$stateCode}->{pub}->[$x]->{cite}->[$y];
				print STDOUT "\t$cite\n";
					
				if( not(exists $HofA_CA{ $title})) {
					push @{ $HofA_CA{$title} }, $codeSec;	
					push @{ $HofA_CA{$title} }, $cite;	
				} else {
					push @{ $HofA_CA{$title} }, $cite;	
				}
				
				$y++;
			}
			
			$y=0;
			$x++;
		}
	} else {
		
		if(exists($XML_StatesInfo->{StateLawCitations}->{$stateCode}->{pub}->[$x]))	{
			
			while($XML_StatesInfo->{StateLawCitations}->{$stateCode}->{pub}->[$x])	{
				$title=$XML_StatesInfo->{StateLawCitations}->{$stateCode}->{pub}->[$x]->{title};
				print STDOUT "\n$title\n";
		
				while($XML_StatesInfo->{StateLawCitations}->{$stateCode}->{pub}->[$x]->{cite}->[$y])	{
					$cite=$XML_StatesInfo->{StateLawCitations}->{$stateCode}->{pub}->[$x]->{cite}->[$y];
					print STDOUT "\t$cite\n";
					push @{ $HofA{$title} }, $cite;	
					$y++;
				}
				
				$y=0;
				$x++;
			}			
		} else {
			print STDOUT "\n" . $XML_StatesInfo->{StateLawCitations}->{$stateCode}->{pub}->{title} . "\n";
	
			while($XML_StatesInfo->{StateLawCitations}->{$stateCode}->{pub}->{cite}->[$y])	{
				print STDOUT "\t" . $XML_StatesInfo->{StateLawCitations}->{$stateCode}->{pub}->{cite}->[$y] . "\n";
				push @aRegEx, $XML_StatesInfo->{StateLawCitations}->{$stateCode}->{pub}->{cite}->[$y];
				$y++;
			}
		}
	}

	$x=0;
	
	# DEBUG PURPOSES:  Dump of Cite holding array or hash...
	print STDOUT "\n$dashes\nDUMPING \@aRegEx,  \%HofA (std), OR \%HofA_CA\n$dashes\n";
	
	if($stateCode ne "CA")	{
		if(exists($XML_StatesInfo->{StateLawCitations}->{$stateCode}->{pub}->[$x])) {
			print STDOUT Dumper(%HofA);
		} else {
			print STDOUT Dumper(@aRegEx);
		}
	} elsif($stateCode eq "CA")	{
		print STDOUT Dumper(%HofA_CA);
	}
}


#----------------------------------------------------------------------
sub GetState {
#----------------------------------------------------------------------
	my $x;
	our $stateCode="";
	our $stateName="";

	while($x ne 'Y') {
		$x = DoStatePrompt();
		if((uc($x) ne 'N') and (uc($x) ne 'Y')) {
			exit(0);
		}
	}
	
	return $stateCode;
}


#----------------------------------------------------------------------
sub DoStatePrompt {
#----------------------------------------------------------------------
	my $key="";
	$stateCode="";
	$stateName="";
	
	print STDOUT "\nPlease type your state abbreviation \<ENTER\>...\n('A' = Alphabetic Listing of States and Abbreviations.)\n\n";
	$stateCode = <STDIN>;
	chomp($stateCode);
	$stateCode=uc($stateCode);
	
	if($stateCode eq "A")	{
		&DumpStates();
		$key="N";
		return($key);
	}

	while ( ($st, $stname) = each (%STATES)) {
       		if($st eq $stateCode)	{
       			$stateName = $stname;
       		}
	}
	
	print STDOUT "\nAbbreviation:  $stateCode\nState Name:    " . uc($stateName) . "\n\n  Correct?  (y/n)\t\(any other key = exit\)\n";
	$key = &ReadKey;
	return uc($key);
}

#----------------------------------------------------------------------
sub DumpStates {
#----------------------------------------------------------------------
       	print "\tState Name = State Abbr.\n$dashes\n";	
	foreach $key (sort (keys(%STATES))) {
		print "\t$STATES{$key} = $key\n";
	}
}


#----------------------------------------------------------------------
sub Get_olinkStatesInfoXML {
#----------------------------------------------------------------------
	my $me = whoami();
	my $appLocation = dirname($0);
		
#	my $appStatesInfo = $appLocation . "\\CONFIG\\" . $FN_olinkStatesInfo;
# TESTING ONLY:
	my $appStatesInfo = "C:\\apps\\CONFIG\\" . $FN_olinkStatesInfo;

	
	if(!-e $appStatesInfo)	{	# IF a {$FN_olinkStatesInfo} file does NOT exist in the current (Job) folder...	
		die("\n\n$dashes\nERROR!  The $FN_olinkStatesInfo file is not found!\n" .
		"Please contact your System Administrator for further assistance.\n$dashes\n$!");
	}
	
	our $xsStatesInfo = XML::Simple->new(
		ForceArray => 0,
		KeepRoot => 1,
		KeyAttr => [ ],
		NoAttr=>1
	);
		
	# Load $XMLdriver from PubMap.xml...
	$XML_StatesInfo = $xsStatesInfo->XMLin($appStatesInfo);	
}
	

#----------------------------------------------------------------------
sub hashValueAscending_StateName {
#----------------------------------------------------------------------
	no warnings;
	$STATES{$a} <=> $STATES{$b};
	use warnings;
}


#-------------------------------------------------------------
sub Housekeeping {
#-------------------------------------------------------------
	our $mystate="";
	$XML_StatesInfo="";

	## Make empty argument list default to all files
	@ARGV = glob("*") unless @ARGV;
	
	# Filter out all but plain, text files.
	@ARGV = grep { !(/^.*\./) && -f && -T} @ARGV;			#HOW TO SELECT ONLY *. FILES?????
	
	if($#ARGV == -1) {
		print(STDOUT "------------------------------\n     FILE(S) NOT FOUND!\n------------------------------\n");
		exit 0;	
	}
	
	&Clear;	
	
	print(STDOUT "\nRUNNING:  olink.exe (pl)\t\t$myVersion\n");
	print(STDOUT "*------------------------------------------------------------------------------*\n");
	
	$mystate=GetState();

	print(STDOUT "\n\n*$dashes*\n");	
	print(STDOUT "\nReady to processing the following input files:\n\n");
	
	if(not($mystate))	{
		print STDOUT "\n\nERROR!\n\nState not found:  $mystate\n$!\n\n";
		exit(1);
	}
	
	foreach $infile (@ARGV) {
			printf(STDOUT "%3s \t%s\n", $ctr, $infile);	#display to user what files are being processed
			$ctr++;
	}
	
	## Process the files or quit?
	print(STDOUT "\n     continue?...\n$dashes\n" . 
		" Y = YES                                      --OR--     \n\n K = YES    \(Keep Temp Files\)                 --OR--     \n\n\(blank\) or N = CANCEL\n" . 
		"\n$dashes\n");
		
	$x = &ReadKey;
	#$x = "y";
	
	if($x=~ /[YyKk]/) {
		# do nothing...
		print(STDOUT "\n");	#add a little space before processing the files
	} else {
		print(STDOUT "\n\...Processing cancelled.\n");
		exit 0 ;
	}
	
	# Get XML Data:  olinkStatesInfo.xml file...
	if(not exists $XML_StatesInfo->{'StateLawCitations'})	{	
		Get_olinkStatesInfoXML();
	}

	#DEBUG:
	#---------------------------------------------------------------------------
	open(STINFO, '>', "Dump_olinkStatesInfo.txt")
		|| die "cannot open (OUTPUT)  file:  Dump_olinkStatesInfo.txt\n";
		
	print STINFO Dumper($XML_StatesInfo);
	
	close STINFO;
	#---------------------------------------------------------------------------
	
}

#-------------------------------------------------------------
sub InsertStateStatutesRefs {
#-------------------------------------------------------------
	my $myline = shift;
	
	
	return ($myline);	
}


#-------------------------------------------------------------
sub PassOne {
#-------------------------------------------------------------
	$outfile = ($infile . $ext1);		# create PASS-ZERO output file...	
	
	open(OUT, ">", $outfile)
		|| die "cannot open file for output:  $outfile";


	while(<FILE>)	{
		my($line) = $_;
		chomp($line);			# Strip the trailing newline from the line.
		
		$line=InsertStateStatutesRefs($line);
		
		print(OUT "$line\n"); # echo line read
		$ctr++;
	}	# end while...
	
	$ctr++;
	$tot += $ctr;
	$gtot += $tot;
# 	print(STDOUT "\t--------------------------------------------------\n");
# 	print(STDOUT "\t\tTotal Lines Processed (This File):  $tot\n\n");
	$tot=0;
	
	close(FILE);
	close(OUT);
}


#-------------------------------------------------------------
sub PassTwo {
#-------------------------------------------------------------
#	my $infile2 = $outfile;		# PASS-ONE input file name...
#	
#	open(FILE, '<:encoding(utf8)', $infile2)
#		|| die "cannot open input file: $infile2";
#	
#	$ctr = 1;
#	$outfile = ($infile . $ext2);		# create PASS-TWO output file...
#	
#	open(OUT, ">:utf8", $outfile)
#		|| die "cannot open file for output:  $outfile";
#
#	while(<FILE>) {
#		 my($line) = $_;
#		chomp($line);			# Strip the trailing newline from the line.
#		
#	}	# end while...
#	
#	$ctr++;
#	$tot += $ctr;
#	$gtot += $tot;
#	$tot=0;
#	
#	close(FILE);
#	close(OUT);
#
}


#-------------------------------------------------------------
sub ProcessFiles {
#-------------------------------------------------------------
# At this point @ARGV should contain all of the files to process.

	$ctr=1;
	$seq=1;
	
	foreach $infile(@ARGV) {
			
		open(FILE, '<', $infile)
			|| die "cannot open file";
		
		print STDOUT "\n$dashes\n";
		print(STDOUT "$seq\t$infile\.txt\n");
		$seq++;
		$ctr = 1;
		
		PassOne();
			
		if($x =~/[Yy]/) {
			if(-e $infile1) {
				$cnt = unlink($infile1);
				#if($cnt > 0) { print(STDOUT "\n...DELETED $infile1\n"); }
			}		
			
			if(-e $infile2) {
				$cnt = unlink($infile2);
				#if($cnt > 0) { print(STDOUT "\n...DELETED $infile2\n"); }
			}
			
			if(-e $infile3) {
				$cnt = unlink($infile3);
				#if($cnt > 0) { print(STDOUT "...DELETED $infile3\n"); }
			}
			if(-e $infile4) {
				$cnt = unlink($infile4);
				#if($cnt > 0) { print(STDOUT "...DELETED $infile4\n"); }
			}
			
			if(-e $infile5) {
				$cnt = unlink($infile5);
				#if($cnt > 0) { print(STDOUT "...DELETED $infile5\n"); }
			}
	
		}
			
	}	#end foreach $infile...	
}


#-------------------------------------------------------------
sub whoami {
#-------------------------------------------------------------
	(caller(1))[3];
}